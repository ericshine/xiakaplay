//
//  AddPostListModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddPostListModel : NSObject
@property(nonatomic,strong) NSMutableArray *section1;
@property(nonatomic,strong) NSMutableArray *section2;
@end
