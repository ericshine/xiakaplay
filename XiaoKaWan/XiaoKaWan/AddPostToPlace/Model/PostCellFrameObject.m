//
//  PostCellFrameObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostCellFrameObject.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define itemsCount 4.0
@implementation PostCellFrameObject
+ (PostCellFrameObject *)cellFrame{
    PostCellFrameObject * frame = [[PostCellFrameObject alloc] init];
    return frame;
}
- (CGFloat)imageSpace{
    return 15.0;
}
- (CGFloat)imageSize{
    CGFloat itemsWidth = (kDeviceWidth -((itemsCount+1)*self.imageSpace))/itemsCount;
    return itemsWidth;
}
- (CGFloat)imageCellHeight{
    NSInteger row = ceil(self.totalCount/itemsCount);
    return row * self.imageSize + (row + 1) *self.imageSpace;
}
@end
