//
//  PostCellFrameObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PostCellFrameObject : NSObject
@property(nonatomic) NSInteger totalCount;
@property(nonatomic) CGFloat imageSpace;
@property(nonatomic) CGFloat imageSize;
@property(nonatomic) CGFloat imageCellHeight;

+ (PostCellFrameObject*)cellFrame;
@end
