//
//  AddPostParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddPostParameter : NSObject
/**
 *  动态文字内容
 */
@property(nonatomic,copy) NSString *content;
/**
 *  标签  逗号分割，如tag1,tag2
 */
@property(nonatomic,copy) NSString *tags;
#pragma mark - 第一个参数是百度自己的poi  的二个是我们的poi  第三个自己创建的poi  三个人选其一
#pragma mark parameter1
/**
 *  百度POI唯一ID
 */
@property(nonatomic,copy) NSString *baidu_uid;

#pragma mark parameter2
/**
 *  自有POI唯一ID
 */
@property(nonatomic,copy) NSString *place_id;


#pragma mark parameter3
/**
 *  用户创建POI的地点名
 */
@property(nonatomic,copy) NSString *place_name;
/**
 *  用户创建POI的经度
 */
@property(nonatomic,copy) NSString *lng;
/**
 *  用户创建POI的纬度
 */
@property(nonatomic,copy) NSString *lat;
/**
 *  用户创建POI的地址
 */
@property(nonatomic,copy) NSString *address;
/**
 *  用户创建POI的分类ID
 */
@property(nonatomic,copy) NSString *category_id;

/**
 *  用户创建POI的描述
 */
@property(nonatomic,copy) NSString *place_desc;

#pragma mark -------------
/**
 *  动态发生日期
 */
@property(nonatomic,copy) NSString *action_date;
/**
 *  榜单ID
 */
@property(nonatomic,copy) NSString *maps_id;
/**
 *  附件ID串
 */
@property(nonatomic,copy) NSString *aids;
/**
 *  token
 */
@property(nonatomic,copy) NSString *_t;
@end
