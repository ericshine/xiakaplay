//
//  CreatAddressModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatAddressModel.h"

@implementation CreatAddressModel
- (NSArray *)listArray{
    if(_listArray == nil){
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:5];
        NSArray *titles = @[@"地       区",@"地点名称",@"详细地址",@"所属类别",@"地点描述"];
        NSArray *placeHolders = @[@"所在地区",@"填写位置名称",@"街道门牌信息",@"选填",@"这里给你留下了什么回忆(选填)"];
        [titles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CreatAddressModel *address = [[CreatAddressModel alloc] init];
            address.title = obj;
            address.placeholder = placeHolders[idx];
            [array addObject:address];
        }];
        _listArray = [[NSArray alloc] initWithArray:[array copy]];
    }
    return _listArray;
}
@end
