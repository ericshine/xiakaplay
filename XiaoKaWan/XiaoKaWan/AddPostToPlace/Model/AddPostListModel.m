//
//  AddPostListModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostListModel.h"
#import "AddPostListObj.h"
#import "NSDate+BJDateFormat.h"
@implementation AddPostListModel
- (NSMutableArray *)section1{
    if(_section1 == nil){
    NSArray *icons = @[@"\U0000e617",@"\U0000e602"];
    NSArray *titles = @[@"添加标签",@"选择位置"];
    _section1  = [[NSMutableArray alloc] initWithCapacity:icons.count];
    [icons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AddPostListObj *object = [[AddPostListObj alloc] init];
        object.title = titles[idx];
        object.icon = icons[idx];
        [_section1 addObject:object];
    }];
    }
    return _section1;
}
- (NSMutableArray *)section2{
    if(_section2 == nil){
    NSArray *icons = @[@"\U0000e619"];
    NSArray *titles = @[@"时间"];
        NSString *date = [[NSDate date] dateToStringWithFormat:@"yyyy-MM-dd"];
        _section2  = [[NSMutableArray alloc] initWithCapacity:icons.count];
    [icons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AddPostListObj *object = [[AddPostListObj alloc] init];
        object.title = titles[idx];
        object.icon = icons[idx];
        object.detail = date;
        [_section2 addObject:object];
    }];
    }
    return _section2;
}
@end
