//
//  CreatLoactionObj.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreatLoactionObj : NSObject
/**
 *  用户创建POI的描述
 */
@property(nonatomic,copy) NSString *place_desc;
/**
 *  用户创建POI的分类ID
 */
@property(nonatomic,copy) NSString *category_id;
/**
 *  /用户创建POI的地址
 */
@property(nonatomic,copy) NSString *address;
/**
 *  用户创建POI的纬度
 */
@property(nonatomic,copy) NSString *lat;
/**
 *  用户创建POI的经度
 */
@property(nonatomic,copy) NSString *lng;
/**
 *  用户创建POI的地点名
 */
@property(nonatomic,copy) NSString *place_name;
/**
 *  Description
 */
@property(nonatomic,copy) NSString *place_id;
@end
