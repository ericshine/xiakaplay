//
//  CreatAddressModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface CreatAddressModel : NSObject
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *content;
@property(nonatomic,copy) NSString *placeholder;
@property(nonatomic,strong) NSArray *listArray;
@end
