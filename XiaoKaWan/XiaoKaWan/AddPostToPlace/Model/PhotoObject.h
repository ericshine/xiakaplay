//
//  PhotoObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/11/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PhotoObject : NSObject
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) UIImage *thumbImage;
@property(nonatomic,copy) NSString *aid;
@end
