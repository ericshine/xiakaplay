//
//  AddPostNetworkManage.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "ClientNetwork.h"
#import "APIConfig.h"
#import <MJExtension.h>
#import "UserConfig.h"
#import "BJNotificationConfig.h"
#import "QAlertView.h"
#import "AddPostParameter.h"
@interface AddPostNetworkManage : NSObject
SINGLETON_H
/**
 *  获取标签
 *
 *  @param complete complete description
 */
- (void)getPostTagsComplete:(requestComplete )complete;
/**
 *  加动态
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)addPostToPlaceWithParameter:(AddPostParameter *)parameter complete:(requestComplete )complete;
/**
 *  添加地点
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)addPlaceWithParameter:(AddPostParameter *)parameter complete:(requestComplete )complete;    
@end
