//
//  SelectMapsHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectMapsHeadView.h"

@implementation SelectMapsHeadView
- (void)updateConstraints{
    [super updateConstraints];
    [self.cancleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(39);
        make.right.equalTo(self.mas_right).offset(-39);
        make.height.equalTo(@30);
    }];
}
- (UIButton *)cancleBnt{
    if(_cancleBnt == nil){
        _cancleBnt = [[UIButton alloc] init];
        [_cancleBnt setBackgroundImage:[[UIImage imageNamed:@"dashedbox"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
         [_cancleBnt setBackgroundImage:[[UIImage imageNamed:@"dashedbox"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_cancleBnt setImage:[UIImage imageNamed:@"disSelectMap"] forState:UIControlStateNormal];
        _cancleBnt.titleLabel.font = [UIFont systemFontOfSize:14];
        [_cancleBnt setTitle:@"不添加到专辑中" forState:UIControlStateNormal];
        [_cancleBnt setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_cancleBnt setTitleColor:[UIColor p_colorWithHex:0x555555] forState:UIControlStateNormal];
        [_cancleBnt addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_cancleBnt];
    }
    return _cancleBnt;
}
- (void)cancleAction:(UIButton *)button{
    [self.delegate cancelSelect];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
