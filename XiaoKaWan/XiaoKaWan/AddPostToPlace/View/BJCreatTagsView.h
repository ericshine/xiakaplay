//
//  BJCreatTagsView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface BJCreatTagsView : BJView
@property(nonatomic,strong)NSMutableArray *tagsArray;
@property(nonatomic,copy) void(^viewHeight)(CGFloat height);// 动态刷新BJCreatTagsView 的高度
@property(nonatomic,strong) NSString *userTag;/// 传入一个tag显示
@property(nonatomic,strong) NSArray *selectedTags;
@end

