//
//  AnnotationIndexView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AnnotationIndexView.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@interface AnnotationIndexView ()
@property(nonatomic,strong)UILabel *indexLabel;
@end

@implementation AnnotationIndexView
- (instancetype)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
-(void)setIndex:(NSInteger)index{
    self.indexLabel.text = [NSString stringWithFormat:@"%li",(long)index];
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.indexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-3);
    }];
}
- (UILabel *)indexLabel{
    if(_indexLabel == nil){
        _indexLabel = [UILabel creatLabelWithFont:10 color:0x333333];
        [self addSubview:_indexLabel];
    }
    return _indexLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
