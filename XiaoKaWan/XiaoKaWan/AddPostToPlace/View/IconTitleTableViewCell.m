//
//  IconTitleTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "IconTitleTableViewCell.h"

@implementation IconTitleTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.iconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(15);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.iconLabel.mas_right).offset(9);
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(0);
    }];
}
- (UILabel*)iconLabel
{
    if(_iconLabel==nil){
        _iconLabel = [UILabel creatLabelWithFont:13 color:0x333333];
        _iconLabel.font = [UIFont fontWithName:@"iconfont" size:20];
        _iconLabel.textColor = [UIColor p_colorWithHex:0xffdc46];
        [self.contentView addSubview:_iconLabel];
    }
    return _iconLabel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:13 color:0x333333];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:13 color:0x999999];
        _detailLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
