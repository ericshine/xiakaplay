//
//  ShowIageTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ShowIageTableViewCell.h"
#import "ShowImageViewModel.h"
#import <Masonry.h>
//static NSInteger const itemSize = 85;
//static NSInteger const itemSpace = 15;
#import "PostCellFrameObject.h"
@interface ShowIageTableViewCell ()
@property(nonatomic,strong)ShowImageViewModel *viewModel;
@end
@implementation ShowIageTableViewCell{

    PostCellFrameObject *cellFrame;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        cellFrame = [PostCellFrameObject cellFrame];
        [self showImageCollectionView];
        [self setNeedsUpdateConstraints];
        
    }
    return self;
}
- (void)setImageArray:(NSArray *)imageArray{
    self.viewModel.selectImages = imageArray;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.showImageCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
- (UICollectionView *)showImageCollectionView{
    if(_showImageCollectionView == nil){
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(cellFrame.imageSize, cellFrame.imageSize);
        flowLayout.minimumInteritemSpacing = cellFrame.imageSpace;
        flowLayout.sectionInset = UIEdgeInsetsMake(cellFrame.imageSpace, cellFrame.imageSpace, cellFrame.imageSpace, cellFrame.imageSpace);
//        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _showImageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _showImageCollectionView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_showImageCollectionView];
        _showImageCollectionView.delegate = self.viewModel;
        _showImageCollectionView.dataSource = self.viewModel;
    }
    return _showImageCollectionView;
}
- (ShowImageViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[ShowImageViewModel alloc] initWithCollectionView:_showImageCollectionView];
    }
    _viewModel.selelctImageCallBack = self.selelctedImageCallBack;
    return _viewModel;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
