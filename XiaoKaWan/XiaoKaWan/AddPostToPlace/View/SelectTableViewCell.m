//
//  SelectTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectTableViewCell.h"

@implementation SelectTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setSelectCell:(BOOL)selectCell{
    if(selectCell){
        _selectLb.textColor =[UIColor mainColor];
    }else _selectLb.textColor = [UIColor colorWithWhite:0.8 alpha:0.5];
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-150);
    }];
    [self.selectLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self showLineWithLeft:0 rightOffset:0];
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:14 color:0x6c6c6c];
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}

- (UILabel *)selectLb {
    if(_selectLb == nil){
        _selectLb = [[UILabel alloc] init];
        _selectLb.font = [UIFont fontWithName:@"iconfont" size:17];
        _selectLb.textColor = [UIColor colorWithWhite:0.8 alpha:0.5];
        _selectLb.text = @"\U0000e60e";
        [self.contentView addSubview:_selectLb];
    }
    return _selectLb;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
