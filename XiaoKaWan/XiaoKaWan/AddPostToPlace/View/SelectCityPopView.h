//
//  SelectCityPopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface SelectCityPopView : BJView<UIPickerViewDelegate,UIPickerViewDataSource>
@property(nonatomic,strong)UIPickerView *pickerView;
@property(nonatomic,strong) UIButton *finish;
@property(nonatomic,strong) UIButton *cancle;
@end
