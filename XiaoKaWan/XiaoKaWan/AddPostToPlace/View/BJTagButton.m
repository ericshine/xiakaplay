//
//  BJTagButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTagButton.h"

@implementation BJTagButton
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.mas_left).offset(4);
        make.right.equalTo(self.mas_right).offset(-4);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        
    }];
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(-3);
        make.right.equalTo(self.mas_right).offset(3);
        make.width.equalTo(@15);
        make.height.equalTo(@15);
    }];
}
- (void)setTitle:(NSString *)title{
    [self.button setTitle:title forState:UIControlStateNormal];
}
- (UIButton *)button{
    if(_button == nil){
    _button = [[UIButton alloc] init];
    _button.titleLabel.font =[UIFont systemFontOfSize:13];
    [_button setTitleColor:[UIColor p_colorWithHex:0xffda44] forState:UIControlStateNormal];
    [self addSubview:_button];
    _button.layer.borderColor = [UIColor p_colorWithHex:0xffda44].CGColor;
    _button.layer.borderWidth = 1;
    _button.layer.cornerRadius = 10;
    }
    return _button;
}

- (UIButton*)deleteButton
{
    if(_deleteButton==nil){
        _deleteButton = [[UIButton alloc] init];
        [_deleteButton setTitle:@"\U0000e61b" forState:UIControlStateNormal];
        _deleteButton.titleLabel.font =[UIFont fontWithName:@"iconfont" size:16];
        [_deleteButton setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(deleteTag:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
    }
    return _deleteButton;
}
- (void)deleteTag:(UIButton *)button{
    [self removeFromSuperview];
    [self.delegate buttonDeleteSelf:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
