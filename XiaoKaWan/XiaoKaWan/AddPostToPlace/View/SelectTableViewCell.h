//
//  SelectTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"

@interface SelectTableViewCell : BJTableViewCell
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UILabel *selectLb;
@property(nonatomic) BOOL selectCell;
@end
