//
//  AddPostSuccessView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/29/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostSuccessView.h"
@interface AddPostSuccessView()
@property(nonatomic,strong)UILabel *textLab;
@property(nonatomic,strong)UIButton *sendAgainBtn;
@property(nonatomic,strong)UIButton *scanPostBtn;
@property(nonatomic,strong)UIView *leftLine;
@property(nonatomic,strong)UIView *rightLine;
@property(nonatomic,strong)UILabel *label;
@end
@implementation AddPostSuccessView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.textLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(80);
        make.centerX.equalTo(self.mas_centerX);
    }];
    [self.sendAgainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLab.mas_bottom).offset(62);
         make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@38);
        make.width.equalTo(@199);
    }];
    [self.scanPostBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sendAgainBtn.mas_bottom).offset(26);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@38);
        make.width.equalTo(@199);
    }];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scanPostBtn.mas_bottom).offset(100);
        make.centerX.equalTo(self.mas_centerX);
        
    }];
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(11);
        make.right.equalTo(self.label.mas_left).offset(-10);
        make.top.equalTo(self.scanPostBtn.mas_bottom).offset(110);
        make.height.equalTo(@0.5);
    }];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.left.equalTo(self.label.mas_right).offset(10);
        make.top.equalTo(self.scanPostBtn.mas_bottom).offset(110);
        make.height.equalTo(@0.5);
    }];
    [self.shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.label.mas_bottom).offset(35);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (UILabel *)textLab{
    if(_textLab == nil){
        _textLab = [UILabel creatLabelWithFont:18 color:0X3D3D3D];
        _textLab.text = @"发布成功";
        [self addSubview:_textLab];
    }
    return _textLab;
}
- (UIButton *)sendAgainBtn{
    if(_sendAgainBtn == nil){
        _sendAgainBtn = [self creatButton];
        [_sendAgainBtn setTitle:@"再发一条动态" forState:UIControlStateNormal];
        [_sendAgainBtn addTarget:self action:@selector(resendAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sendAgainBtn];
    }
   return  _sendAgainBtn;
}
- (UIButton *)scanPostBtn{
    if(_scanPostBtn == nil){
        _scanPostBtn = [self creatButton];
        [_scanPostBtn setTitle:@"查看动态" forState:UIControlStateNormal];
        [_scanPostBtn addTarget:self action:@selector(postDetail:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_scanPostBtn];
    }
    return  _scanPostBtn;
}
- (UIView *)leftLine{
    if(_leftLine == nil){
        _leftLine = [[UIView alloc] init];
        _leftLine.backgroundColor = [UIColor p_colorWithHex:0xc1c1c1];
        [self addSubview:_leftLine];
    }
    return _leftLine;
}
- (UIView *)rightLine{
    if(_rightLine == nil){
        _rightLine = [[UIView alloc] init];
        _rightLine.backgroundColor = [UIColor p_colorWithHex:0xc1c1c1];
        [self addSubview:_rightLine];
    }
    return _rightLine;
}
- (UILabel *)label{
    if(_label == nil){
        _label = [UILabel creatLabelWithFont:15 color:0x3d3d3d];
        _label.text = @"分享";
        [self addSubview:_label];
    }
    return _label;
}
- (ShareView *)shareView{
    if(_shareView == nil){
        _shareView = [[ShareView alloc] init];
        [self addSubview:_shareView];
    }
    return _shareView;
}
- (UIButton *)creatButton{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:[UIColor mainColor]];
    button.layer.cornerRadius = 10;
    button.layer.masksToBounds = YES;
    [button setTitleColor:[UIColor p_colorWithHex:0X3D3D3D] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    return button;
}
- (void)resendAction:(UIButton *)button{
    if(self.resendPost)self.resendPost();
}
- (void)postDetail:(UIButton *)button{
    if(self.postDetail)self.postDetail();
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

@implementation ShareView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        NSArray *items = @[@"wechatdiscover_share",@"weChat_share",@"weibo_share",@"qqzone_share",@"qq_share"];
        [self creatItems:items];
    }
    return self;
}
- (void)creatItems:(NSArray *)itmes{
    CGFloat width = 35.0;
    CGFloat heght = 35.0;
    CGFloat space = (kDeviceWidth-width*itmes.count)/(itmes.count+1);
    
    
    
    [itmes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(space + idx*(width +space), 20, width, heght)];
        [button setImage:[UIImage imageNamed:obj] forState:UIControlStateNormal];
        button.tag = idx+20;
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }];
}
- (void)buttonAction:(UIButton *)button{
    [self.delegate shareIndex:button.tag - 20];
}
@end