//
//  TagsTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"
@protocol TagsTableViewCellDelegate <NSObject>

- (void)selectTagWithString:(NSString *)text;

@end
@interface TagsTableViewCell : BJTableViewCell
@property(nonatomic,strong)NSArray *tags;
@property(nonatomic,assign) id<TagsTableViewCellDelegate>delegate;
@end

