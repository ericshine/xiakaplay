//
//  TagsTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "TagsTableViewCell.h"
#define itemSpace 23
#define itemHeight 25
#define itemFont [UIFont systemFontOfSize:13]
#define limitWidth 100
@implementation TagsTableViewCell{
    NSMutableArray *tagButtons;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        tagButtons = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}
- (void)setTags:(NSArray *)tags{
    [tagButtons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = obj;
        [button removeFromSuperview];
    }];
    CGFloat leftOffset = [self getLeftSpace:tags];
    [tags enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *lastButton = [tagButtons lastObject];
        NSString *text = obj;
        CGFloat width = 0;
        CGSize size = [text sizeWithFont:itemFont andSize:CGSizeMake(kDeviceWidth, 20)];
        UIButton *button = [self creatBUtton];
        [button setTitle:text forState:UIControlStateNormal];
        width += size.width+20;
        if(lastButton == nil){
        button.frame = CGRectMake(leftOffset, 10, width, itemHeight);
        }else{
        button.frame = CGRectMake(CGRectGetMaxX(lastButton.frame)+itemSpace, 10, width, itemHeight);
        }
        [tagButtons addObject:button];
    }];
}
- (CGFloat)getLeftSpace:(NSArray *)array{
    __block CGFloat width =0;
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *text = obj;
        CGSize size = [text sizeWithFont:itemFont andSize:CGSizeMake(kDeviceWidth, itemHeight)];
        width += size.width+20;
    }];
    width += (array.count-1)*itemSpace;
    CGFloat leftWidth = (kDeviceWidth-width)/2;
    return leftWidth;
}
- (UIButton *)creatBUtton{
    UIButton *button = [[UIButton alloc] init];
    button.titleLabel.font =itemFont;
    [button setTitleColor:[UIColor p_colorWithHex:0xffda44] forState:UIControlStateNormal];
    button.layer.borderColor = [UIColor p_colorWithHex:0xffda44].CGColor;
    button.layer.borderWidth = 1;
    button.layer.cornerRadius = 10;
    [self addSubview:button];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
- (void)buttonAction:(UIButton*)button{
    [self.delegate selectTagWithString:button.titleLabel.text];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
