//
//  AddPostunflodHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostunflodHeadView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@implementation AddPostunflodHeadView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.lineLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.unflodBUtton.mas_left).offset(-32);
        make.height.equalTo(@0.5);
    }];
    [self.unflodBUtton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);

        make.height.equalTo(@30);
    }];
    [self.lineRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.unflodBUtton.mas_right).offset(32);
        make.height.equalTo(@0.5);
        make.right.equalTo(self.mas_right);
    }];
}
- (UIView *)lineLeft{
    if(_lineLeft == nil){
        _lineLeft = [[UIView alloc] init];
        _lineLeft.backgroundColor = [UIColor p_colorWithHex:0xececec];
        [self addSubview:_lineLeft];
    }
    return _lineLeft;
}
- (UIView *)lineRight{
    if(_lineRight == nil){
        _lineRight = [[UIView alloc] init];
        _lineRight.backgroundColor = [UIColor p_colorWithHex:0xececec];
        [self addSubview:_lineRight];
    }
    return _lineRight;
}
- (UIButton *)unflodBUtton{
    if(_unflodBUtton == nil){
        _unflodBUtton = [[UIButton alloc] init];
        _unflodBUtton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_unflodBUtton setTitle:@"更多编辑" forState:UIControlStateNormal];
        [_unflodBUtton setTitleColor:[UIColor p_colorWithHex:0x999999] forState:UIControlStateNormal];
        [_unflodBUtton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_unflodBUtton];
    }
    return _unflodBUtton;
}
- (void)buttonAction:(UIButton *)button{
    button.selected = !button.selected;
    [self.delegate unflodAction:button.selected];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
