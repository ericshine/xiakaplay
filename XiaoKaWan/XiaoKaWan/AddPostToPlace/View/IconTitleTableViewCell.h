//
//  IconTitleTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+colorWithHex.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@interface IconTitleTableViewCell : UITableViewCell
@property(nonatomic,strong)UILabel *iconLabel;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *detailLabel;

@end
