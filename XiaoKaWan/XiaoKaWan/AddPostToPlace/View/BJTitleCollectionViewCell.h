//
//  BJTitleCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJTitleCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLb;
@end
