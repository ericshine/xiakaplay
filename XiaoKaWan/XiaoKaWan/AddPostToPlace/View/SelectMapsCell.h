//
//  SelectMapsCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"

@interface SelectMapsCell : BJTableViewCell
@property(nonatomic,strong) BJUIImageView *imageView1;
@property(nonatomic,strong)UILabel *titleLb;
@end
