//
//  LocationListHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListHeadView : UIView
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)UISwitch *searchSwitch;
@property(nonatomic,strong)UILabel *switchLb;
@property(nonatomic,copy) void(^searchTypeChange)(BOOL isOn);
@end
