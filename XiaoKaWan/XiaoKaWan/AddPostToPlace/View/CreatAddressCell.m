//
//  CreatAddressCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatAddressCell.h"

@implementation CreatAddressCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hiddenkeyboard) name:BJHiddenKeyboardNotification object:nil];
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewChanged) name:UITextViewTextDidChangeNotification object:nil];
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFlieldChanged) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)textViewChanged{
    self.model.content = self.textView.text;
}
- (void)textFlieldChanged{
    self.model.content = self.textField.text;

}
- (void)hiddenkeyboard{
    [self.textView resignFirstResponder];
    [self.textField resignFirstResponder];
}
- (void)setModel:(CreatAddressModel *)model{
    _model = model;
    self.titleLb.text = model.title;
     self.textField.text = model.content;
    self.textView.placeHolder = model.placeholder;//0x919191
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:model.placeholder];
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x919191],NSFontAttributeName:[UIFont systemFontOfSize:15]} range:NSMakeRange(0, model.placeholder.length)];
    self.textField.attributedPlaceholder = attStr;
//    self.textField.placeholder = model.placeholder;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(14);
        make.height.equalTo(@16);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.width.equalTo(@70);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-14);
        make.left.equalTo(self.titleLb.mas_right).offset(12);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView.mas_top).offset(14);
//        make.bottom.equalTo(self.contentView.mas_bottom).offset(-14);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.titleLb.mas_right).offset(21);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self showLineWithLeft:20 rightOffset:0];
}
- (BJPlaceholderTextView *)textView{
    if(_textView == nil){
        _textView = [[BJPlaceholderTextView alloc] init];
        _textView.delegate = self;
        _textView.font = [UIFont systemFontOfSize:15];
        _textView.textColor = [UIColor p_colorWithHex:0x111111];
        [self.contentView addSubview:_textView];
    }
    return _textView;
}
- (UITextField *)textField{
    if(_textField == nil){
        _textField = [[UITextField alloc]init];
        _textField.delegate = self;
        _textField.textColor = [UIColor p_colorWithHex:0x111111];
        _textView.font = [UIFont systemFontOfSize:15];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.contentView addSubview:_textField];
    }
    return _textField;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:16 color:0x333333];
        _titleLb.font = [UIFont fontWithName:@"STHeiti-Light" size:16];
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
