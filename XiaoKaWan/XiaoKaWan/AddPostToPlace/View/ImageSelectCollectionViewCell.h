//
//  ImageSelectCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSelectCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UIImageView *imageView;
- (void)showHud;
- (void)disMissHud;
@end
