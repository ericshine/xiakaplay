//
//  BJTimeSelectView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
@protocol BJTimeSelectViewDelegate <NSObject>

- (void)selectDate:(NSDate *)date;

@end
@interface BJTimeSelectView : BJView

@property(nonatomic,strong)UIDatePicker *datePicker;
@property(nonatomic,assign) id<BJTimeSelectViewDelegate>delegate;
- (void)showInView:(UIView*)view;
- (void)disMiss;
@end

