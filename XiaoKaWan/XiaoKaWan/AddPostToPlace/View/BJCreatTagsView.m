//
//  BJCreatTagsView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCreatTagsView.h"
#import "BJTextField.h"
#import "BJTagButton.h"
#define leftSpace  10
#define topSpace 10
#define itemSpace  10
#define tagFont [UIFont systemFontOfSize:13]
#define limitWidth 80
#define leftTextFeildLimitWidth  60 // 输入框在右边的最小宽度  小于就换行
#define itemHeight 35
@interface BJCreatTagsView ()<BJTextFieldDelegate,UITextFieldDelegate,BJTagButtonDelegate>
@property(nonatomic,strong)BJTextField *textField;
@end
@implementation BJCreatTagsView{
    NSMutableArray *tags;
    BOOL isRemove;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.tagsArray = [[NSMutableArray alloc] initWithCapacity:0];
        tags = [[NSMutableArray alloc] initWithCapacity:0];
        [self textField];
       
    }
    return self;
}
- (void)setUserTag:(NSString *)userTag{
    if(self.tagsArray.count>=20){
        [[QAlertView sharedInstance] showAlertText:@"你最多只能添加20个标签" fadeTime:2];
        return;
    }
    if(![self.tagsArray containsObject:userTag]){
        [self.tagsArray addObject:userTag];
        [self creatTag:userTag];
    }
    else [[QAlertView sharedInstance] showAlertText:@"此标签已经存在" fadeTime:2];
}
- (void)setSelectedTags:(NSArray *)selectedTags{
    self.tagsArray = [NSMutableArray arrayWithArray:selectedTags];
    [self.tagsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self creatTag:obj];
    }];
}
- (BJTextField *)textField{
    if(_textField == nil){
        _textField = [[BJTextField alloc] initWithFrame:CGRectMake(20, 10, kDeviceWidth-20, 20)];
        [self addSubview:_textField];
       // _textField.placeholder = @"添加你的标签";
        [_textField becomeFirstResponder];
        _textField.bjdelegate = self;
        _textField.delegate = self;
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.returnKeyType = UIReturnKeyDone;
    }
    return _textField;
}

- (void)creatTag:(NSString *)string{
    BJTagButton *lastButton = [tags lastObject];
    CGSize size = [string sizeWithFont:tagFont andSize:CGSizeMake(kDeviceWidth, 20)];
    CGFloat itemsWidth = size.width + 20;
    if(itemsWidth>limitWidth) itemsWidth = limitWidth;
     BJTagButton *button =[self creatButton:string];
    if(lastButton == nil){
        button.frame = CGRectMake(leftSpace, topSpace, itemsWidth, itemHeight);
        self.textField.frame = CGRectMake(CGRectGetMaxX(button.frame)+itemSpace, topSpace, kDeviceWidth-(CGRectGetMaxX(button.frame)+itemSpace), itemHeight);
    }else{
        CGFloat w = CGRectGetMaxX(lastButton.frame)+leftSpace+ itemSpace + itemsWidth;
        if(w>kDeviceWidth){
            button.frame = CGRectMake(leftSpace, CGRectGetMaxY(lastButton.frame)+topSpace, itemsWidth, itemHeight);
            self.textField.frame = CGRectMake(CGRectGetMaxX(button.frame)+itemSpace, button.frame.origin.y,  kDeviceWidth-(CGRectGetMaxX(button.frame)+itemSpace), itemHeight);
        }else{
            button.frame = CGRectMake(CGRectGetMaxX(lastButton.frame)+itemSpace, lastButton.frame.origin.y, itemsWidth, itemHeight);
            CGFloat t_w = CGRectGetMaxX(button.frame)+leftSpace+ itemSpace + leftTextFeildLimitWidth;
            if(t_w>kDeviceWidth){
                self.textField.frame = CGRectMake(leftSpace, CGRectGetMaxY(lastButton.frame)+topSpace, kDeviceWidth-leftSpace*2, itemHeight);
            }else{
                self.textField.frame =CGRectMake(CGRectGetMaxX(button.frame)+itemSpace, button.frame.origin.y,  kDeviceWidth-(CGRectGetMaxX(button.frame)+itemSpace), itemHeight);
            }
        }
    }
    [tags addObject:button];
    [self getSelfViewHeight];
}
- (BJTagButton *)creatButton:(NSString *)string{
    BJTagButton *button = [[BJTagButton alloc] init];
    button.delegate = self;
    button.title = string;
    [self addSubview:button];
    return button;
}
- (void)removeLastTag{
    BJTagButton *selectTag = [tags lastObject];
    [self.tagsArray removeLastObject];
    [tags removeLastObject];
    if(selectTag != nil){
        [selectTag removeFromSuperview];
        BJTagButton *lastButton = [tags lastObject];
        if(lastButton == nil){
            self.textField.frame = CGRectMake(leftSpace, topSpace, kDeviceWidth-leftSpace*2, itemHeight);
        }else{
         CGFloat t_w = CGRectGetMaxX(lastButton.frame)+leftSpace+ itemSpace + leftTextFeildLimitWidth;
            if(t_w>kDeviceWidth){
                self.textField.frame = CGRectMake(leftSpace, CGRectGetMaxY(lastButton.frame)+topSpace, kDeviceWidth-leftSpace*2, itemHeight);
            }else{
                self.textField.frame =CGRectMake(CGRectGetMaxX(lastButton.frame)+itemSpace, lastButton.frame.origin.y,  kDeviceWidth-(CGRectGetMaxX(lastButton.frame)+itemSpace), itemHeight);
            }

        }
    }
    [self getSelfViewHeight];
}
- (void)getSelfViewHeight{
    CGFloat viewHeight = CGRectGetMaxY(self.textField.frame)+topSpace;
    if(self.viewHeight)self.viewHeight(viewHeight);
}
#pragma mark - delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.text.length >12){
        [[QAlertView sharedInstance] showAlertText:@"标签长度不能超过12个字!" fadeTime:2];
        return NO;
    }
    if(textField.text.length<=0){
        [textField resignFirstResponder];
        return NO;
    }
//    [self.tagsArray addObject:textField.text];
//    [self creatTag:textField.text];
    [self setUserTag:textField.text];
    _textField.text = @"";
    return YES;
}
- (void)textDidDeleteText:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        if(isRemove) [self removeLastTag];
        isRemove = YES;
    }else isRemove = NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
}
#pragma mark - BJTagButtonDelegate
- (void)buttonDeleteSelf:(BJTagButton *)button{
    NSInteger index = [tags indexOfObject:button];
    if(index == tags.count-1) {
        [self removeLastTag];
        return;
    }
    [self.tagsArray removeObjectAtIndex:index];
    [tags removeObject:button];
    [tags enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BJTagButton *button = obj;
        [button removeFromSuperview];
    }];
    [tags removeAllObjects];
    [self.tagsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self creatTag:obj];
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
