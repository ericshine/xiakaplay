//
//  ShowIageTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowIageTableViewCell : UITableViewCell
@property(nonatomic,strong)UICollectionView *showImageCollectionView;
@property(nonatomic,strong)NSArray *imageArray;
@property(nonatomic,copy) void(^selelctedImageCallBack)(BOOL addImage,NSInteger index);
@end
