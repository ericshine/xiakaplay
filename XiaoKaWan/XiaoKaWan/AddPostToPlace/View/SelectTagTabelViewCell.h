//
//  SelectTagTabelViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "IconTitleTableViewCell.h"
#import "MapsPalceImageLayout.h"
@interface SelectTagTabelViewCell : IconTitleTableViewCell<MapsPalceImageLayoutDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSArray *tags;
@end
