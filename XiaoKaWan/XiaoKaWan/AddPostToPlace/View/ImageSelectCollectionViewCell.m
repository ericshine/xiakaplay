//
//  ImageSelectCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ImageSelectCollectionViewCell.h"
#import <Masonry.h>
@interface ImageSelectCollectionViewCell()
@property(nonatomic,strong)UIView *imageHud;
@property(nonatomic,strong)UIActivityIndicatorView *activityView;
@end
@implementation ImageSelectCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
        self.imageHud.hidden = YES;
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
    [self.imageHud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
    [self.activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.imageHud.mas_centerY);
        make.centerX.equalTo(self.imageHud.mas_centerX);
    }];
}
- (UIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}
- (UIView *)imageHud{
    if(_imageHud == nil){
        _imageHud = [[UIView alloc] init];
        _imageHud.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.8];
        _imageHud.layer.zPosition = 1;
        [self.contentView addSubview:_imageHud];
       
    }
    return _imageHud;
}
- (UIActivityIndicatorView *)activityView{
    if(_activityView == nil){
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self.imageHud addSubview:_activityView];
    }
    return _activityView;
}
- (void)showHud{
    [self.activityView startAnimating];
    self.imageHud.hidden = NO;
}
- (void)disMissHud{
    [self.activityView stopAnimating];
    self.imageHud.hidden = YES;
}
@end
