//
//  CreatTagTableCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatTagView.h"


@implementation CreatTagView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setTagStr:(NSString *)tagStr{
    self.creatTagView.userTag = tagStr;
}
- (void)setSelectedTags:(NSArray *)selectedTags{
    self.creatTagView.selectedTags = selectedTags;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.creatTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (BJCreatTagsView *)creatTagView{
    if(_creatTagView == nil){
        _creatTagView = [[BJCreatTagsView alloc] init];
        [self addSubview:_creatTagView];
        __weak typeof(self) weakSelf = self;
        _creatTagView.viewHeight = ^(CGFloat viewHeight){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate cellHeightChange:viewHeight];
        };
    }
    return _creatTagView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
