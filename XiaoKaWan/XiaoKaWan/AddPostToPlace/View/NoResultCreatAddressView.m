//
//  NoResultCreatAddressView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "NoResultCreatAddressView.h"
#import "UserConfig.h"

@implementation NoResultCreatAddressView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(30);
        make.centerX.equalTo(self.mas_centerX);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@(kDeviceWidth));
    }];
    [self.creatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@40);
        make.width.equalTo(@107);
    }];

}
- (UIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"creatAddress"];
        [self addSubview:_imageView];
    }
    return _imageView;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
    _titleLb = [UILabel creatLabelWithFont:13 color:0xA5A5A5];
        _titleLb.text =@"找不到就自己创建吧";
        _titleLb.numberOfLines = 0;
        _titleLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLb];
    }
    return _titleLb;
}
- (UIButton *)creatButton{
    if(_creatButton == nil){
        _creatButton = [[UIButton alloc] init];
        _creatButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_creatButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_creatButton setTitle:@"创建地址" forState:0];
        [_creatButton setBackgroundColor:[UIColor p_colorWithHex:0xFFDA44]];
        _creatButton.layer.cornerRadius = 5;
        _creatButton.layer.masksToBounds = YES;
        [_creatButton addTarget:self action:@selector(ceatAddressAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_creatButton];
    }
    return _creatButton;
}
- (void)ceatAddressAction:(UIButton*)button{
    if([UserConfig sharedInstance].user_token.length) {
        if(_creatAddress)_creatAddress();
    }else [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedUserLoginNotification object:nil];
}
@end
