//
//  SelectMapsHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
@protocol SelectMapsHeadViewDelegate <NSObject>

- (void)cancelSelect;

@end
@interface SelectMapsHeadView : BJView
@property(nonatomic,strong) UIButton *cancleBnt;
@property(nonatomic,assign) id<SelectMapsHeadViewDelegate>delegate;
@end

