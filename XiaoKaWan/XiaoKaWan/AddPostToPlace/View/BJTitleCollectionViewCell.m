//
//  BJTitleCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTitleCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation BJTitleCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@20);
    }];
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:13 color:0x3f3f3f];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.backgroundColor = [UIColor mainColor];
        _titleLb.layer.cornerRadius = 5;
        _titleLb.layer.masksToBounds = YES;
        _titleLb.layer.shouldRasterize = YES;
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}
@end
