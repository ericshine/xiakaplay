//
//  NoResultCreatAddressView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface NoResultCreatAddressView : BJView
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong) UIButton *creatButton;
@property(nonatomic,copy) void(^creatAddress)();
@end
