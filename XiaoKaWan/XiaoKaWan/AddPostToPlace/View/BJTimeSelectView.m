//
//  BJTimeSelectView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTimeSelectView.h"
#define backVieHeight 200
#define timeSelectheight 150
@implementation BJTimeSelectView{
    UIView *bgView;
    UIView *whiteView;
    UIButton *finishBnt;
    NSDate *selectDate;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        bgView = [[UIView alloc] initWithFrame:frame];
        [self addSubview:bgView];
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMiss)];
        [bgView addGestureRecognizer:gesture];
        whiteView = [[UIView alloc] init];
        whiteView.backgroundColor = [UIColor whiteColor];
        [self addSubview:whiteView];
        finishBnt = [[UIButton alloc] init];
        finishBnt.titleLabel.font = [UIFont systemFontOfSize:16];
        [finishBnt setTitle:@"完成" forState:UIControlStateNormal];
        [finishBnt setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [finishBnt addTarget:self action:@selector(finishAction:) forControlEvents:UIControlEventTouchUpInside];
        [whiteView addSubview:finishBnt];
        [self setNeedsUpdateConstraints];
        selectDate = [NSDate date];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];

    [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(backVieHeight);
        make.height.equalTo(@backVieHeight);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
    [self.datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(whiteView.mas_bottom);
        make.height.equalTo(@timeSelectheight);
        make.left.equalTo(whiteView.mas_left);
        make.right.equalTo(whiteView.mas_right);
    }];
    [finishBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteView.mas_top).offset(11);
        make.height.equalTo(@30);
        make.width.equalTo(@40);
        make.right.equalTo(whiteView.mas_right).offset(-16);
    }];
    
}
- (UIDatePicker *)datePicker{
    if(_datePicker == nil){
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 50, kDeviceWidth, 150)];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        [_datePicker addTarget:self action:@selector(timeChange:) forControlEvents:UIControlEventValueChanged];
        [whiteView addSubview:_datePicker];
    }
    return _datePicker;
}
- (void)timeChange:(UIDatePicker *)datePicker{
    selectDate = datePicker.date;
}
- (void)finishAction:(UIButton *)button{
    [self disMiss];
    [self.delegate selectDate:selectDate];
}
- (void)showInView:(UIView *)view{
    [view addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        bgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
    whiteView.transform = CGAffineTransformMakeTranslation(0, -backVieHeight);
    }];
    
}
- (void)disMiss{
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
