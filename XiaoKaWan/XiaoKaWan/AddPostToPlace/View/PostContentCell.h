//
//  PostContentCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJPlaceholderTextView.h"
@interface PostContentCell : UITableViewCell
@property(nonatomic,strong)BJPlaceholderTextView *textView;
@end
