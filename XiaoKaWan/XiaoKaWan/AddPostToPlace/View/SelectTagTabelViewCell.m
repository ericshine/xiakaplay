//
//  SelectTagTabelViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectTagTabelViewCell.h"
#import "NSString+Cagetory.h"
#import "BJTitleCollectionViewCell.h"
static NSString *const cellId = @"BJTitleCollectionViewCell" ;
@implementation SelectTagTabelViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self.collectionView registerClass:[BJTitleCollectionViewCell class] forCellWithReuseIdentifier:cellId];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(0);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
- (void)setTags:(NSArray *)tags{
    _tags = tags;
    if(_tags.count>0){
        self.collectionView.hidden = NO;
    }else self.collectionView.hidden = YES;
    [self.collectionView reloadData];
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        MapsPalceImageLayout *layout = [[MapsPalceImageLayout alloc] init];
        layout.delegate = self;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}
#pragma mark - MapsPalceImageLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPalceImageLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *text = _tags[indexPath.row];
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:13] andSize:CGSizeMake(kDeviceWidth, 20)];
    return size.width+40;
}
- (CGFloat)collectionViewHeight:(UICollectionView *)collectionView{
    return 30;
}
#pragma mark - MapsPlaceListHeadCollectionCellDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _tags.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BJTitleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    NSString *text = _tags[indexPath.row];
    cell.titleLb.text = text;
    cell.titleLb.layer.cornerRadius = 7;
    cell.titleLb.layer.borderColor = [UIColor p_colorWithHex:0xffda44].CGColor;
    cell.titleLb.layer.borderWidth = 1;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
