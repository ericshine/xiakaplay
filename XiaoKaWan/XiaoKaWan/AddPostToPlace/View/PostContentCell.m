//
//  PostContentCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostContentCell.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation PostContentCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(22.5);
        make.left.equalTo(self.contentView.mas_left).offset(14);
        make.right.equalTo(self.contentView.mas_right).offset(-14);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
- (BJPlaceholderTextView *)textView{
    if(_textView == nil){
        _textView = [[BJPlaceholderTextView alloc] init];
        _textView.font = [UIFont systemFontOfSize:13];
        _textView.placeHolder = @"分享当时的心情故事";
        _textView.layer.cornerRadius = 5;
        _textView.layer.masksToBounds = YES;
        _textView.layer.borderWidth = 0.5;
        _textView.layer.borderColor = [UIColor p_colorWithHex:0xcccccc].CGColor;
        [self.contentView addSubview:_textView];
    }
    return _textView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
