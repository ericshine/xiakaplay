//
//  AddPostSuccessView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/29/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
@class ShareView;
@protocol ShareViewDelegate <NSObject>

- (void)shareIndex:(NSInteger)index;

@end
@interface AddPostSuccessView : BJView
@property(nonatomic,strong)ShareView *shareView;
@property(nonatomic,copy) void(^resendPost)();
@property(nonatomic,copy) void(^postDetail)();
@end

@interface ShareView : BJView
@property(nonatomic,strong)id<ShareViewDelegate>delegate;
@end
