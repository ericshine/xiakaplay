//
//  SelectCityPopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectCityPopView.h"

@implementation SelectCityPopView{
    NSArray *cityList;
    NSInteger indexProvince;
    NSInteger indexCity;
    NSInteger indexDistrict;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        indexCity = 0;
        indexDistrict = 0;
        indexProvince = 0;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CityJson" ofType:@""];
        NSFileManager* fm = [NSFileManager defaultManager];
        NSData *data = [[NSData alloc] init];
        data =[fm contentsAtPath:path];
        cityList = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(30);
        make.left.right.bottom.equalTo(self);
    }];
    [self.cancle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.mas_left).offset(10);
    }];
    [self.finish mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
}
- (void)buttonCancleAction:(UIButton *)button{

}
- (void)buttonFinishAction:(UIButton *)button{
    
}
- (UIButton *)finish{
    if(_finish == nil){
    _finish = [[UIButton alloc] init];
    _finish.titleLabel.font = [UIFont systemFontOfSize:16];
    [_finish setTitle:@"完成" forState:UIControlStateNormal];
    [_finish setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
    [_finish addTarget:self action:@selector(buttonFinishAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_finish];
    }
    return _finish;
}
- (UIButton *)cancle{
    if(_cancle == nil){
        _cancle = [[UIButton alloc] init];
        _cancle.titleLabel.font = [UIFont systemFontOfSize:16];
        [_cancle setTitle:@"取消" forState:UIControlStateNormal];
        [_cancle setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [_cancle addTarget:self action:@selector(buttonCancleAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_cancle];
    }
    return _cancle;
}
- (UIPickerView *)pickerView{
    if(_pickerView == nil){
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        _pickerView.backgroundColor = [UIColor whiteColor];
        _pickerView.showsSelectionIndicator = YES;
        [self addSubview:_pickerView];
    }
    return _pickerView;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0)return cityList.count;
    else if (component == 1)return ((NSArray *)(cityList[indexProvince][@"city"])).count;
    return ((NSArray *)(((NSArray *)(cityList[indexProvince][@"city"]))[indexCity][@"area"])).count;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 44;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component ==0){
        NSString *province = cityList[row][@"name"];
        return province;
    }
    else if(component == 1){
    NSString *city = (((NSArray *)(cityList[indexProvince][@"city"]))[row])[@"name"];
        return city;
    }else{
        NSString *disc = ((NSArray *)(((NSArray *)(cityList[indexProvince][@"city"]))[indexCity][@"area"]))[row];
        return disc;
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == 0){
        indexProvince = row;
    }else if(component ==1){
        indexCity = row;
    
    }else{
        indexDistrict = row;
    }
    [self.pickerView reloadAllComponents];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
