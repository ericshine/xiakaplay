//
//  AddPostunflodHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AddPostunflodHeadViewDelegate <NSObject>

- (void)unflodAction:(BOOL)unflod;

@end
@interface AddPostunflodHeadView : UIView
@property(nonatomic,strong) UIView *lineLeft;
@property(nonatomic,strong) UIView *lineRight;
@property(nonatomic,strong) UIButton *unflodBUtton;
@property(nonatomic,assign) id<AddPostunflodHeadViewDelegate>delegate;
@end

