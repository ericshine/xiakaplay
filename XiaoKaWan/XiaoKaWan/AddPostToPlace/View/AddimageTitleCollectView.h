//
//  AddimageTitleCollectView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"

@interface AddimageTitleCollectView : BJCollectionViewCell
@property(nonatomic,strong)UILabel *titleLb;
@end
