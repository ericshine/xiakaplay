//
//  BJTagButton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
@class BJTagButton;
@protocol BJTagButtonDelegate <NSObject>

- (void)buttonDeleteSelf:(BJTagButton *)button;

@end
@interface BJTagButton : BJView
@property(nonatomic,assign) id<BJTagButtonDelegate>delegate;
@property(nonatomic,strong) UIButton *button;
@property(nonatomic,strong) UIButton *deleteButton;
@property(nonatomic,copy) NSString *title;
@end

