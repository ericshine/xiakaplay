//
//  SelectMapsCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectMapsCell.h"

@implementation SelectMapsCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(@23);
        make.width.equalTo(@55);
        make.height.equalTo(@55);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView1.mas_right).offset(17);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    [self showLineWithLeft:14 rightOffset:14];
}
- (BJUIImageView *)imageView1{
    if(_imageView1 == nil){
        _imageView1 = [[BJUIImageView alloc] init];
        _imageView1.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_imageView1];
    }
    return _imageView1;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb  = [UILabel creatLabelWithFont:15 color:0x555555];
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
