//
//  LocationCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LocationCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@implementation LocationCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(14);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    [self.detailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(6.5);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    
}
- (UILabel*)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:15 color:0x333333];
        [self.contentView addSubview:_titleLb];
    }
    return _titleLb;
}
- (UILabel*)detailLb{
    if(_detailLb == nil){
        _detailLb = [UILabel creatLabelWithFont:13 color:0x999999];
        [self.contentView addSubview:_detailLb];
    }
    return _detailLb;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
