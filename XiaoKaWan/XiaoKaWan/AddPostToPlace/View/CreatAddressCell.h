//
//  CreatAddressCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"
#import "BJPlaceholderTextView.h"
#import "CreatAddressModel.h"
@interface CreatAddressCell : BJTableViewCell<UITextFieldDelegate,UITextViewDelegate>
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)BJPlaceholderTextView *textView;
@property(nonatomic,strong)UITextField *textField;
@property(nonatomic,strong)CreatAddressModel *model;
@end
