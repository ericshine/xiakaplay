//
//  LocationCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"

@interface LocationCell : BJTableViewCell
@property(nonatomic,strong)UILabel *titleLb;
@property(nonatomic,strong)UILabel *detailLb;
@end
