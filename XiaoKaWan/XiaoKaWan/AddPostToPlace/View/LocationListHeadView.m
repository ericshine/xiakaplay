//
//  LocationListHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LocationListHeadView.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation LocationListHeadView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@1);
    }];
    [self.searchSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@30);
        make.width.equalTo(@50);
    }];
    [self.switchLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.searchSwitch.mas_left).offset(-10);
        
    }];
}
- (UISwitch *)searchSwitch{
    if(_searchSwitch == nil){
        _searchSwitch = [[UISwitch alloc] init];
        _searchSwitch.onTintColor = [UIColor mainColor];
        _searchSwitch.on = NO;
        [_searchSwitch addTarget:self action:@selector(switchValueChange:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_searchSwitch];
    }
    return _searchSwitch;
}
- (void)switchValueChange:(id)sender{
    UISwitch *switchButton = (UISwitch *)sender;
    BOOL isOn = [switchButton isOn];
    if(self.searchTypeChange)self.searchTypeChange(isOn);
}
- (UILabel*)switchLb{
    if(_switchLb == nil){
        _switchLb = [UILabel creatLabelWithFont:12 color:0x8c8c8c];
        _switchLb.text = @"按附近搜索";
        [self addSubview:_switchLb];
    }
    return _switchLb;
}
- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [UILabel creatLabelWithFont:12 color:0x8c8c8c];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
        [self addSubview:_lineView];
    }
    return _lineView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
