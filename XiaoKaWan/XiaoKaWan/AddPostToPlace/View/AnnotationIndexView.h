//
//  AnnotationIndexView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>
@interface AnnotationIndexView : BMKAnnotationView
@property(nonatomic)NSInteger index;
@end
