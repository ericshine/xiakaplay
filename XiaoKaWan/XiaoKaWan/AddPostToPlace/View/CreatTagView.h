//
//  CreatTagTableCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#import "BJCreatTagsView.h"
@protocol CreatTagViewDelegate <NSObject>

- (void)cellHeightChange:(CGFloat)cellHeight;

@end
@interface CreatTagView : BJView
@property(nonatomic,strong)BJCreatTagsView *creatTagView;
@property(nonatomic,assign) id<CreatTagViewDelegate>delegate;
@property(nonatomic,copy) NSString *tagStr;
@property(nonatomic,strong)NSArray *selectedTags;
@end

