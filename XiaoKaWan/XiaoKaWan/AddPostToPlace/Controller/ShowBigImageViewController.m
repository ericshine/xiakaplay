//
//  ShowBigImageViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ShowBigImageViewController.h"

@interface ShowBigImageViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollvew;
@property(nonatomic,strong)UIImageView *imageView;
@end

@implementation ShowBigImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self scrollvew];
    if(self.asset != nil){
        [self setImageWithPHAsset:self.asset];
    }else{
        [self setImageWithImage:self.image];
    }
    self.imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    tap.numberOfTapsRequired = 2;
    [_imageView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}
- (void)setImageWithPHAsset:(PHAsset *)asset{
    self.automaticallyAdjustsScrollViewInsets = NO;
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.synchronous = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block UIImage *image ;
        [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:CGSizeMake(asset.pixelWidth,asset.pixelHeight) contentMode:PHImageContentModeAspectFit options:options resultHandler:^(UIImage *result, NSDictionary *info) {
            image = result;
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = image;;
        });
    });
}
- (void)setImageWithImage:(UIImage *)image{
    self.imageView.image = image;
}
- (void)doubleTap:(UITapGestureRecognizer *)gesture{
    [_scrollvew setZoomScale:2 animated:YES];
}
- (UIScrollView *)scrollvew{
    if(_scrollvew == nil){
        _scrollvew = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollvew.delegate = self;
        _scrollvew.maximumZoomScale = 2;
        _scrollvew.minimumZoomScale = 1.0;
        _scrollvew.showsHorizontalScrollIndicator = NO;
        _scrollvew.showsVerticalScrollIndicator = NO;
        _scrollvew.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_scrollvew];
    }
    return _scrollvew;
}

- (UIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[UIImageView alloc] initWithFrame:_scrollvew.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.backgroundColor = [UIColor clearColor];
        [_scrollvew addSubview:_imageView];
        
    }
    return _imageView;
}
#pragma marl - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//    [_scrollvew setZoomScale:3 animated:YES];
    return _imageView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
