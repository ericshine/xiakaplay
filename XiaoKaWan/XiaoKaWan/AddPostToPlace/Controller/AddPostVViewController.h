//
//  AddPostVViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "AddPostViewModel.h"
@class CreatLoactionObj;
typedef NS_ENUM(NSInteger,FromeVc){
    FromeVc_map,
    FromeVc_place
};
@interface AddPostVViewController : BJSuperViewController
@property(nonatomic,strong)NSArray *photoObjs;
@property(nonatomic)PostType postType;
@property(nonatomic)FromeVc fromeVc;
@property(nonatomic,strong)MapSearchResult *location;
@end
