//
//  AddPostVViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostVViewController.h"

#import "PhotoAlbumViewController.h"
#import "BJNavigationController.h"
#import "PostImagePreviewViewController.h"
#import "SelectLocationViewController.h"
#import "SelectMapsViewController.h"
#import "SelectTagsViewController.h"
#import "PhotoObject.h"
#import "PostContentCell.h"
#import "AddPostParameter.h"
#import "AddPostNetworkManage.h"
#import "CreatLoactionObj.h"
#import "AddPostSuccessViewController.h"
@interface AddPostVViewController ()<AddPostViewModelDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)AddPostViewModel *viewModel;
@property(nonatomic,strong)BJTimeSelectView *timeSelectView;
@property(nonatomic,strong)NSMutableArray *selectedImages;
@property(nonatomic,strong)UIImagePickerController *imagePicker;
@property(nonatomic) BOOL canPublish;
@property(nonatomic,strong)CreatLoactionObj *createLocaion;
@end

@implementation AddPostVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"发布"];
    if(self.postType == POST_IMAGE) self.navigationItem.rightBarButtonItem.enabled = NO;
    _selectedImages = [[NSMutableArray alloc] initWithArray:self.photoObjs];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadAllImage) name:BJUploadAllImageNotification object:nil];
   
   

    self.viewModel.locationObj = self.location;
    // Do any additional setup after loading the view.
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setCanPublish:(BOOL)canPublish{
    if(canPublish){
    self.navigationItem.rightBarButtonItem.enabled = YES;
    }else self.navigationItem.rightBarButtonItem.enabled = NO;
}
- (void)uploadAllImage{
    self.canPublish = YES;
}
- (void)rightClick{
   
   NSArray *array =  [self.selectedImages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"aid = nil"]];
    if(array.count>0){
        return;
    }
    NSMutableArray *imageArray = [[NSMutableArray alloc] initWithCapacity:self.selectedImages.count];
    [self.selectedImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PhotoObject *photo = obj;
        [imageArray addObject:photo.aid];
    }];
    AddPostParameter *parameter = [[AddPostParameter alloc] init];
    NSIndexPath *path = [NSIndexPath indexPathForRow:1 inSection:0];
    PostContentCell *cell = [self.tableView cellForRowAtIndexPath:path];
    parameter.content = cell.textView.text;
    parameter.tags = [self.viewModel.tags componentsJoinedByString:@","];
    parameter.baidu_uid = self.viewModel.locationObj.uid;
    parameter.action_date = self.viewModel.postDate;
    parameter.place_id = self.viewModel.locationObj.place_id;
    if(self.viewModel.maps != nil){
    parameter.maps_id = [NSString stringWithFormat:@"%@",self.viewModel.maps.id];
    }
    parameter.aids = [imageArray componentsJoinedByString:@","];
    parameter.category_id = self.createLocaion.category_id;
    parameter.place_desc = self.createLocaion.place_desc;
    parameter.address = self.createLocaion.address;
    parameter.lat = self.createLocaion.lat;
    parameter.lng = self.createLocaion.lng;
    parameter.place_name = self.createLocaion.place_name;
    if(parameter.content.length>1200){
        [[QAlertView sharedInstance] showAlertText:@"内容长度不能超过1200字" fadeTime:2];
        return;
    }
    NSString *content = [parameter.content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    content = [content stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if(content.length<1 && parameter.aids.length<1){
        [[QAlertView sharedInstance] showAlertText:@"请添加动态内容" fadeTime:2];
        return;
    }
    if(!parameter.lat.length&&!parameter.baidu_uid.length&& !parameter.place_id.length){
        [[QAlertView sharedInstance] showAlertText:@"请添加地理位置" fadeTime:2];
        return;
    }
    [BJPregressHUD showWithStatus:@"发布中..."];
    parameter._t = [UserConfig sharedInstance].user_token;
    [[AddPostNetworkManage sharedInstance] addPostToPlaceWithParameter:parameter complete:^(BOOL succeed, id obj) {
        [BJPregressHUD dissMiss];
        if(succeed){
            [[NSNotificationCenter defaultCenter] postNotificationName:BJAddPostNotification object:nil];
            AddPostSuccessViewController *successVc = [[AddPostSuccessViewController alloc] init];
            successVc.postId = obj[@"post_id"];
            successVc.fromeVc = self.fromeVc;
            self.tableView = nil;
            self.viewModel = nil;
            [self.selectedImages removeAllObjects];
            __weak typeof(self)weakSelf = self;
            successVc.resendPost = ^(){
                __strong typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.tableView.delegate = strongSelf.viewModel;
                strongSelf.tableView.dataSource = strongSelf.viewModel;
            };
            [self push:successVc];
        }
    }];
    
}
-(CreatLoactionObj *)createLocaion{
    if(_createLocaion ==nil){
        _createLocaion = [[CreatLoactionObj alloc] init];
    }
    return _createLocaion;
}
- (AddPostViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[AddPostViewModel alloc] initWithTable:self.tableView postType:self.postType];
        _viewModel.delegate = self;
        _viewModel.images = _selectedImages;
        __weak __typeof(self) weakSelf = self;
        _viewModel.selelctedImageCallBack = ^(BOOL addImage,NSInteger index){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if(addImage) [strongSelf reSelectImage];
            else {
                PostImagePreviewViewController *previewController = [[PostImagePreviewViewController alloc] init];
                previewController.images = strongSelf.selectedImages;
                previewController.index = index;
                [strongSelf push: previewController];
                previewController.imageArrayCallBack = ^(NSMutableArray *images){
                strongSelf.selectedImages = images;
                strongSelf.viewModel.images = images;
                };
            }
        };
    }
    return _viewModel;
}

- (void)reSelectImage{
    UIAlertController *sheetVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"拍照" style:0 handler:^(UIAlertAction * _Nonnull action) {
        if(_imagePicker == nil){
            _imagePicker = [[UIImagePickerController alloc] init];
            _imagePicker.delegate = self;
            _imagePicker.allowsEditing = YES;
        }
        _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"从相册中选取" style:0 handler:^(UIAlertAction * _Nonnull action) {
        PhotoAlbumViewController *photoAlbum = [[PhotoAlbumViewController alloc] init];
        photoAlbum.limitCount = 9-_selectedImages.count;
        [self presentViewController:[[BJNavigationController alloc] initWithRootViewController:photoAlbum] animated:YES completion:nil];
        __weak typeof(self) weakSelf = self;
        photoAlbum.seleltImages = ^(NSArray *images){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.canPublish = NO;
            [strongSelf.selectedImages addObjectsFromArray:images];
            strongSelf.viewModel.images = strongSelf.selectedImages;
        };
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:sheetVc animated:YES completion:nil];
    

}
- (BJTimeSelectView *)timeSelectView{
    if(_timeSelectView == nil){
        _timeSelectView = [[BJTimeSelectView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        _timeSelectView.delegate = self.viewModel;
    }
    return _timeSelectView;
}
- (void)showTimeSelectView{
    [self.timeSelectView showInView:self.tabBarController.view];
}
#pragma mark - AddPostViewModelDelegate
- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self) weakSelf = self;
    if(indexPath.section == 0){
        if(indexPath.row == 2){
            SelectTagsViewController *selectTags = [[SelectTagsViewController alloc] init];
            selectTags.selectTags = ^(NSArray<NSString*> *tags){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.viewModel.tags = tags;
            };

            selectTags.tagsArr = self.viewModel.tags;
            [self push:selectTags];
        }else if(indexPath.row == 3){
            SelectLocationViewController *selelctLoaction = [[SelectLocationViewController alloc] init];
            selelctLoaction.loaction = ^(MapSearchResult *location){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.viewModel.locationObj = location;
            };
            selelctLoaction.creatLoaction = ^(CreatLoactionObj *location){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.createLocaion = location;
                MapSearchResult *resultLoc = [[MapSearchResult alloc] init];
                resultLoc.name = location.place_name;
                strongSelf.viewModel.locationObj = resultLoc;
            };
            [self push:selelctLoaction];
        }
    }else{
        if(indexPath.row == 0)[self showTimeSelectView];
        else{
            SelectMapsViewController *selectMapVc = [[SelectMapsViewController alloc] init];
            selectMapVc.selectMaps = ^(Maps *maps){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.viewModel.maps = maps;
            };
            [self push:selectMapVc];
        }
    }
}
#pragma mark - selectImage
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
//    [BJPregressHUD showWithStatus:@"上传中..."];
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerEditedImage];
    UIImage *image1 = [UIImage imageWithData:UIImageJPEGRepresentation(image, 0.5)];
     PhotoObject *photo = [[PhotoObject alloc] init];
    photo.image = image;
    photo.thumbImage = image1;
    [_selectedImages addObject:photo];
    self.canPublish = NO;
    self.viewModel.images = _selectedImages;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
