//
//  AddPostSuccessViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/29/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostSuccessViewController.h"
#import "AddPostSuccessView.h"
#import "PlacePostDetailViewController.h"
#import "BJShareTool.h"

@interface AddPostSuccessViewController ()<ShareViewDelegate>
@property(nonatomic,strong) AddPostSuccessView *successView;
@end

@implementation AddPostSuccessViewController
- (AddPostSuccessView *)successView{
    if(_successView == nil){
        _successView = [[AddPostSuccessView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64)];
        _successView.shareView.delegate = self;
        __weak typeof(self)weakSelf = self;
        _successView.resendPost = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if(strongSelf.resendPost)strongSelf.resendPost();
            [strongSelf popViewController];
        };
        _successView.postDetail = ^(){
             __strong typeof(weakSelf) strongSelf = weakSelf;
            PostParatemer *parameter = [PostParatemer parameter];
            parameter.post_id = [NSString stringWithFormat:@"%@",strongSelf.postId];
            PlacePostDetailViewController *controller = [[PlacePostDetailViewController alloc] init];
            controller.parameter = parameter;
            [strongSelf push:controller removeChildWithCount:2];
        };
    }
    return _successView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   // [self leftButtonWithIconfont:@"\U0000e613"];
    self.tableView.tableHeaderView = self.successView;
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
     [self leftButtonWithIconfont:@"\U0000e613"];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationItem.leftBarButtonItem = nil;
}
- (void)leftClick{
    if(self.fromeVc == FromeVc_map){
    [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        UIViewController *controller = [self.navigationController childViewControllerFromStr:@"MapsPlaceDetailViewController"];
        [self.navigationController popToViewController:controller animated:YES];
    }
    
}
#pragma mark - ShareViewDelegate
- (void)shareIndex:(NSInteger)index{
    switch (index) {
        case 0: {
            /**
             *  朋友圈
             */
            [BJShareTool shareDynamicDetailWithType:wxtimeline viewController:self postID:self.postId];
        } break;
        case 1: {
            /**
             *  微信
             */
            [BJShareTool shareDynamicDetailWithType:wxsession viewController:self postID:self.postId];
        } break;
        case 2: {
            /**
             *  微博
             */
            [BJShareTool shareDynamicDetailWithType:sina viewController:self postID:self.postId];
        } break;
        case 3: {
            /**
             *  qq空间
             */
            [BJShareTool shareDynamicDetailWithType:qzone viewController:self postID:self.postId];
        } break;
        case 4: {
            /**
             *  qq
             */
            [BJShareTool shareDynamicDetailWithType:qq viewController:self postID:self.postId];
        } break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
