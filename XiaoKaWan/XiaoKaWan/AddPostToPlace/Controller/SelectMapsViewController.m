//
//  SelectMapsViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectMapsViewController.h"
#import "SelectMapsViewModel.h"
#import "SelectMapsHeadView.h"
@interface SelectMapsViewController ()<SelectMapsViewModelDelegate,SelectMapsHeadViewDelegate>
@property(nonatomic,strong)SelectMapsViewModel *viewModel;
@property(nonatomic,strong)SelectMapsHeadView *headView;
@end

@implementation SelectMapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableHeaderView = self.headView;
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Do any additional setup after loading the view.
}
- (SelectMapsHeadView *)headView{
    if(_headView == nil){
    _headView = [[SelectMapsHeadView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 68)];
        _headView.delegate = self;
    }
    return _headView;
}
- (SelectMapsViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[SelectMapsViewModel alloc] init];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma mark - SelectMapsHeadViewDelegate
- (void)cancelSelect{
    if(self.selectMaps)self.selectMaps(nil);
    [self popViewController];
}
#pragma mark - SelectMapsViewModelDelegate
- (void)selectMaps:(Maps *)maps{
    if(self.selectMaps)self.selectMaps(maps);
    [self popViewController];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
