//
//  CreatAddressViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import <CoreLocation/CoreLocation.h>
typedef NS_ENUM(NSInteger,CreatPlaceFrom){
    TYPE_FROM,
    FROM_HOMEMAP,
    FROM_ADDPOST
};
@class CreatLoactionObj;
@interface CreatAddressViewController : BJSuperViewController
@property(nonatomic,copy) NSString *address;
@property(nonatomic) CLLocationCoordinate2D coordinate2d;
@property(nonatomic,copy) void(^creatLoaction)(CreatLoactionObj *locatin);
@property(nonatomic)CreatPlaceFrom fromType;
@end
