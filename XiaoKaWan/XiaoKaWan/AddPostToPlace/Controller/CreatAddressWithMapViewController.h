//
//  CreatAddressWithMapViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "CreatAddressViewController.h"
@class CreatLoactionObj;
@interface CreatAddressWithMapViewController : BJSuperViewController
@property(nonatomic,copy) void(^creatLoaction)(CreatLoactionObj *locatin);
@property(nonatomic)CreatPlaceFrom fromType;
@end
