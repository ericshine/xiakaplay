//
//  PhotoAlbumViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/11/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"

@interface PhotoAlbumViewController : BJSuperViewController
@property(nonatomic) NSInteger limitCount; // default 100
@property(nonatomic,copy) void(^seleltImages)(NSArray * images);
@end
