//
//  CreatAddressWithMapViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatAddressWithMapViewController.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

@interface CreatAddressWithMapViewController ()<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate>
@property(nonatomic,strong)BMKMapView *mapView;
@property(nonatomic,strong) BMKLocationService *locationService;
@property(nonatomic,strong)BMKGeoCodeSearch *geoSearch;
@property(nonatomic,strong) UIImageView *locationImageView;
@property(nonatomic) CLLocationCoordinate2D newCoordinate2d;
@property(nonatomic,strong) NSString *address;
@end

@implementation CreatAddressWithMapViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.mapView viewWillAppear];
    self.mapView.delegate = self;
    self.locationService.delegate = self;
    self.mapView.showsUserLocation = NO;
    self.mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    self.mapView.showsUserLocation = YES;//显示定位图层
    [self.locationService startUserLocationService];
    self.geoSearch.delegate = self;
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    self.locationService.delegate = nil;
    [self.locationService stopUserLocationService];
    self.geoSearch.delegate = nil;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self mapView];
    [self locationService];
    [self locationViewFrame];
    [self rightButtonWithTitle:@"下一步"];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    CreatAddressViewController *creatAddress = [[CreatAddressViewController alloc] init];
    creatAddress.coordinate2d = self.newCoordinate2d;
    creatAddress.address = self.address;
    creatAddress.creatLoaction = self.creatLoaction;
    creatAddress.fromType = self.fromType;
    [self push:creatAddress];
}
- (void)locationViewFrame{
    [self.locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mapView.mas_centerX);
        make.centerY.equalTo(self.mapView.mas_centerY);
    }];
}
- (UIImageView *)locationImageView{
    if(_locationImageView == nil){
        _locationImageView = [[UIImageView alloc] init];
        _locationImageView.image = [UIImage imageNamed:@"creatAddressAn"];
        [self.view addSubview:_locationImageView];
    }
    return _locationImageView;
}
- (BMKMapView *)mapView{
    if(_mapView == nil){
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64)];
        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        _mapView.showsUserLocation = YES;
        _mapView.showMapScaleBar = NO;
        _mapView.zoomLevel = 16;
        BMKLocationViewDisplayParam *parameter = [[BMKLocationViewDisplayParam alloc] init];
        parameter.isAccuracyCircleShow = NO;
        [_mapView updateLocationViewWithParam:parameter];
        [self.view addSubview:_mapView];
    }
    return _mapView;
}
- (BMKLocationService *)locationService{
    if(_locationService == nil){
        _locationService = [[BMKLocationService alloc] init];
    }
    return _locationService;
}
- (BMKGeoCodeSearch *)geoSearch{
    if(_geoSearch == nil){
        _geoSearch = [[BMKGeoCodeSearch alloc] init];
    }
    return _geoSearch;
}
- (void)replaceGeoSearchAtoordinate:(CLLocationCoordinate2D)coordinate{
    BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    annotation.title = @"新地址";
    [_mapView addAnnotation:annotation];
    [_mapView selectAnnotation:annotation animated:YES];
    
    BMKReverseGeoCodeOption *option = [[BMKReverseGeoCodeOption alloc] init];
    option.reverseGeoPoint = coordinate;
    BOOL flag = [self.geoSearch reverseGeoCode:option];
    if(flag){
        NSLog(@"反geo检索发送成功");
        [self.locationService stopUserLocationService];
    }else{
        NSLog(@"反geo检索发送失败");
    }

}
#pragma mark -BMKGeoCodeSearchDelegate
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    _address = [NSString stringWithFormat:@"%@%@",result.addressDetail.city,result.addressDetail.district];
}
#pragma mark - BMKMapViewDelegate
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    [mapView removeAnnotations:mapView.annotations];
    self.locationImageView.hidden = YES;
    CGPoint point = [mapView convertPoint:self.locationImageView.center fromView:self.view];
    CLLocationCoordinate2D coordinate = [mapView convertPoint:point toCoordinateFromView:mapView];
    _newCoordinate2d = coordinate;
    [self replaceGeoSearchAtoordinate:coordinate];
}
- (void)mapView:(BMKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    [mapView removeAnnotations:mapView.annotations];
    self.locationImageView.hidden = NO;
}
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    if([annotation isKindOfClass:[BMKPointAnnotation class]]){
        BMKAnnotationView *annotationView = [[BMKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiSearch"];
        annotationView.image = [UIImage imageNamed:@"creatAddressAn"];
        return annotationView;
    }
    return nil;
}
#pragma mark - BMKLocationServiceDelegate
- (void)didFailToLocateUserWithError:(NSError *)error{
    
}
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    [_mapView updateLocationData:userLocation];
    [self.locationService stopUserLocationService];
    [self.mapView setCenterCoordinate:self.locationService.userLocation.location.coordinate];
    [self replaceGeoSearchAtoordinate:self.locationService.userLocation.location.coordinate];
    self.navigationItem.rightBarButtonItem.enabled = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
