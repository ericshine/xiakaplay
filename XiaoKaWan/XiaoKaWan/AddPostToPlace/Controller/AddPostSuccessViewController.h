//
//  AddPostSuccessViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/29/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "AddPostVViewController.h"

@interface AddPostSuccessViewController : BJSuperViewController
@property(nonatomic,copy)void (^resendPost)();
@property(nonatomic,copy) NSString *postId;
@property(nonatomic)FromeVc fromeVc;
@end
