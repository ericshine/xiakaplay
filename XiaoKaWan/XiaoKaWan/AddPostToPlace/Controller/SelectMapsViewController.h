//
//  SelectMapsViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
@class Maps;
@interface SelectMapsViewController : BJSuperViewController
@property(nonatomic,copy) void(^selectMaps)(Maps *maps);
@end
