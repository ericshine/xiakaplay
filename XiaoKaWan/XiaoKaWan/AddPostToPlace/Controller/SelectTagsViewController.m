//
//  SelectTagsViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectTagsViewController.h"
#import "SelecctTagsViewModel.h"
#import "CreatTagView.h"
#define headHeight 40
@interface SelectTagsViewController ()<CreatTagViewDelegate,SelecctTagsViewModelDelegate>
@property(nonatomic,strong)SelecctTagsViewModel *viewModel;
@property(nonatomic,strong)CreatTagView *headView;
@end

@implementation SelectTagsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"确定"];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.tableHeaderView = self.headView;
    _headView.selectedTags = self.tagsArr;
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    if(self.selectTags)self.selectTags(self.headView.creatTagView.tagsArray);
    [self popViewController];
}
- (CreatTagView *)headView{
    if(_headView == nil){
        _headView = [[CreatTagView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, headHeight)];
        _headView.delegate = self;
    }
    return _headView;
}
- (SelecctTagsViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel  =[[SelecctTagsViewModel alloc] initWithTable:self.tableView];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma makr - CreatTagTableCellDelegate
- (void)cellHeightChange:(CGFloat)cellHeight{
    CGFloat headViewHeight;
    if(cellHeight <headHeight) headViewHeight = headHeight;
    else headViewHeight = cellHeight;
    self.headView.frame = CGRectMake(0, 0, kDeviceWidth, headViewHeight);
    self.tableView.tableHeaderView = self.headView;
}
#pragma mark - SelecctTagsViewModelDelegate
- (void)selectTag:(NSString *)tag{
    self.headView.tagStr = tag;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
