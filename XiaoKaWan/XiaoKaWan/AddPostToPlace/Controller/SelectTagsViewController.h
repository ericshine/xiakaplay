//
//  SelectTagsViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"

@interface SelectTagsViewController : BJSuperViewController
@property(nonatomic,copy)void(^selectTags)(NSArray<NSString*>*tags);
@property(nonatomic,strong) NSArray *tagsArr;
@end
