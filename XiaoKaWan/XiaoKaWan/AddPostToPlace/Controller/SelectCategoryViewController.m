//
//  SelectCategoryViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectCategoryViewController.h"

@interface SelectCategoryViewController ()
@property(nonatomic,strong)SelectCategoryViewModel *viewModel;
@end

@implementation SelectCategoryViewController
- (SelectCategoryViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[SelectCategoryViewModel alloc] initWithTabel:self.tableView];
    }
    return _viewModel;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"完成"];
    self.tableView.dataSource = self.viewModel;
    self.tableView.delegate = self.viewModel;
    self.tableView.separatorStyle = 0;
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    CategoryObject *category;
    if(self.viewModel.selectCagegorys.count>0) {
       category = self.viewModel.selectCagegorys[0];
    }
    if(self.selectCategory)self.selectCategory(category);
    [self popViewController];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
