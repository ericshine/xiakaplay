//
//  CreatAddressViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatAddressViewController.h"
#import "CreatAddressViewModel.h"
#import "SelectCityPopView.h"
#import "SelectCategoryViewController.h"
#import "CreatLoactionObj.h"
#import "AddPostVViewController.h"
#import "BJNavigationController.h"
#import "AddPostNetworkManage.h"
@interface CreatAddressViewController ()<CreatAddressViewModelDelegate>
@property(nonatomic,strong) CreatAddressViewModel *viewModel;
@end

@implementation CreatAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"创建地点";
    [self rightButtonWithTitle:@"完成"];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
       // Do any additional setup after loading the view.
}
- (void)rightClick{
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
    CreatAddressCell *cell1 = [self.tableView cellForRowAtIndexPath:indexPath1];
     CreatAddressCell *cell2 = [self.tableView cellForRowAtIndexPath:indexPath2];
     CreatAddressCell *cell4 = [self.tableView cellForRowAtIndexPath:indexPath4];
    CreatLoactionObj *locationObject = [[CreatLoactionObj alloc] init];
    locationObject.place_desc = cell4.textView.text;
    locationObject.category_id = self.viewModel.category.id;
    locationObject.address = cell2.textField.text;
    locationObject.place_name = cell1.textField.text;
    locationObject.lat = [NSString stringWithFormat:@"%f",self.coordinate2d.latitude];
    locationObject.lng = [NSString stringWithFormat:@"%f",self.coordinate2d.longitude];
    
    if(locationObject.place_name.length<1){
        [[QAlertView sharedInstance] showAlertText:@"请填写位置名称" fadeTime:2];
        return;
    }
    if(locationObject.place_desc.length>50){
        [[QAlertView sharedInstance] showAlertText:@"位置描述不能超过50个字" fadeTime:2];
        return;
    }
    if(self.fromType == FROM_HOMEMAP){
        AddPostParameter *parameter = [[AddPostParameter alloc] init];
        parameter.place_name = locationObject.place_name;
        parameter.lng = locationObject.lng;
        parameter.lat = locationObject.lat;
        parameter.address = locationObject.address;
        parameter.category_id = locationObject.category_id;
        parameter.place_desc = locationObject.place_desc;
        parameter._t = [UserConfig sharedInstance].user_token;
        [[AddPostNetworkManage sharedInstance] addPlaceWithParameter:parameter complete:^(BOOL succeed, id obj) {
            if(succeed){
                if(self.creatLoaction)self.creatLoaction(locationObject);
             UIViewController *controller = [self.navigationController childViewControllerFromStr:@"CityMapViewController"];
            [self.navigationController popToViewController:controller animated:YES];
            }
        }];
       
    }else{
        if(self.creatLoaction)self.creatLoaction(locationObject);
      UIViewController *controller = [self.navigationController childViewControllerFromStr:@"AddPostVViewController"];
        [self.navigationController popToViewController:controller animated:YES];
    }
    
}
- (CreatAddressViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[CreatAddressViewModel alloc] initWithTable:self.tableView andAddress:self.address];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma mark -CreatAddressViewModelDelegate
- (void)selectCagegory:(void (^)(CategoryObject *))category{
    SelectCategoryViewController *categoryVc = [[SelectCategoryViewController alloc] init];
    categoryVc.selectCategory = category;
    [self push:categoryVc];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
