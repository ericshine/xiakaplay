//
//  AddPostNetworkManage.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/26/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostNetworkManage.h"
#import "UserConfig.h"
typedef void(^requestComplete)(BOOL succeed,id obj);
@implementation AddPostNetworkManage
SINGLETON_M(AddPostNetworkManage)
- (void)getPostTagsComplete:(requestComplete)complete{
    NSDictionary *parameter = @{
                                @"_t":[UserConfig sharedInstance].user_token};
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].postTagApi parameter:parameter complete:complete];
}
- (void)addPostToPlaceWithParameter:(AddPostParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].addPostApi parameter:parameter.keyValues complete:complete];
}
- (void)addPlaceWithParameter:(AddPostParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].addPlaceApi parameter:parameter.keyValues complete:complete];
}
@end
