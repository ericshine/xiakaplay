
//
//  SelectCategoryViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectCategoryViewModel.h"
#import "PlaceCategory.h"
#import "CityMapNetworkManage.h"
@implementation SelectCategoryViewModel{
    NSMutableArray *categoryList;
    
}
- (instancetype)initWithTabel:(UITableView *)table{
    self = [super init];
    if(self){
        self.tableView = table;
        categoryList = [[NSMutableArray alloc] init];
        _selectCagegorys = [[NSMutableArray alloc] init];
        [self getData];
    }
    return self;
}
- (void)getData{
    [[CityMapNetworkManage sharedInstance] getPlaceCategoryComplete:^(BOOL succeed, id objs) {
        [objs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSArray *subArray = obj[@"sub"];
            NSArray *array = [CategoryObject objectArrayWithKeyValuesArray:subArray];
            [categoryList addObjectsFromArray:array];
        }];
        [self.tableView reloadData];
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return categoryList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *categoryId = @"categoryCell";
    SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:categoryId];
    if(!cell){
        cell = [[SelectTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:categoryId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    CategoryObject *category = categoryList[indexPath.row];
    cell.titleLb.text = category.title;
    if([_selectCagegorys containsObject:category]) cell.selectCell = YES;
    else cell.selectCell = NO;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     CategoryObject *category = categoryList[indexPath.row];
    [_selectCagegorys removeAllObjects];
    if([_selectCagegorys containsObject:category]){
        [_selectCagegorys removeObject:category];
    }else [_selectCagegorys addObject:category];
    [self.tableView reloadData];
}
@end
