//
//  AddPostViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJTimeSelectView.h"
#import "Maps.h"
#import "MapSearchResult.h"
typedef NS_ENUM(NSInteger,PostType){
    POST_TYPE,
    POST_IMAGE,
    POST_TEXT
};
@protocol AddPostViewModelDelegate <NSObject>

- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
@interface AddPostViewModel : NSObject<UITableViewDelegate,UITableViewDataSource,BJTimeSelectViewDelegate>
@property(nonatomic,strong)NSMutableArray *images;
@property(nonatomic,strong)MapSearchResult *locationObj;
@property(nonatomic,strong)Maps *maps;
@property(nonatomic,strong)NSArray *tags;
@property(nonatomic,copy) void(^selelctedImageCallBack)(BOOL addImage,NSInteger index);
@property(nonatomic,assign) id<AddPostViewModelDelegate>delegate;
@property(nonatomic,assign) NSString *postDate;
- (instancetype)initWithTable:(UITableView *)table postType:(PostType)type;
@end

