//
//  LocationListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LocationListViewModel.h"
#import "LocationListHeadView.h"
#import "LocationCell.h"
#import "NoResultCreatAddressView.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface LocationListViewModel ()
@property(nonatomic,strong)LocationListHeadView *headView;
@property(nonatomic,strong)NoResultCreatAddressView *noResultView;
@end
@implementation LocationListViewModel{
    NSArray *pois;
//    MapSearchResult *currentLocation;
    UITableView *_tableView;
}
- (instancetype)initWithTableView:(UITableView *)table{
    self = [super init];
    if(self){
        _tableView = table;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return self;
}
- (NoResultCreatAddressView *)noResultView{
    if(_noResultView == nil){
        _noResultView = [[NoResultCreatAddressView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, _tableView.frame.size.height)];
        __weak typeof(self)weakSelf = self;
        _noResultView.creatAddress = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegatel creatAddress];
        };
    }
    return _noResultView;
}
- (LocationListHeadView *)headView{
    if(_headView == nil){
        _headView = [[LocationListHeadView alloc] init];
        _headView.backgroundColor = [UIColor whiteColor];
        _headView.titleLabel.text = @"定位地周边";
        __weak typeof(self)weakSelf = self;
        _headView.searchTypeChange = ^(BOOL isOn){
            __strong typeof(weakSelf) strongSelf= weakSelf;
            [strongSelf.delegatel changeSearchType:isOn];
        };

    }
    return _headView;
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(pois.count>0)return 1;
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return pois.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 38;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 59;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
       return self.headView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        static NSString *cellId = @"loacationId";
    LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell){
        cell = [[LocationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell showLineWithLeft:10 rightOffset:0];
    }
     MapSearchResult *location = pois[indexPath.row];
        cell.titleLb.text = [NSString stringWithFormat:@"%li、%@",(long)(indexPath.row+1),location.name];
        cell.detailLb.text = location.address;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MapSearchResult *result;
    result = pois[indexPath.row];
    [self.delegatel selectLocation:result];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[NSNotificationCenter defaultCenter] postNotificationName:BJHiddenKeyboardNotification object:nil];
}
#pragma mark - SelectLoactionViewModelDelegate
- (void)searchResult:(NSArray *)results{
    pois = results;
    if(pois.count<=0){
        _tableView.tableHeaderView = self.noResultView;
    }else{
        _tableView.tableHeaderView = nil;
    }
    [_tableView reloadData];
}
//- (void)currentLocation:(MapSearchResult *)currentLocation1{
//    currentLocation = currentLocation1;
//    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
//}
@end
;