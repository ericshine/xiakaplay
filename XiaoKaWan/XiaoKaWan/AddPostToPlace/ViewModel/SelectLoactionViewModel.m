//
//  SelectLoactionViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectLoactionViewModel.h"
#import <MJExtension.h>
#import "ImageSizeUrl.h"
#import "CurrentUser.h"
#import <UIImageView+WebCache.h>
#import "AnnotationIndexView.h"
#import <MJExtension.h>
#import "BJPointIndexAnnotation.h"
#import "CityMapNetworkManage.h"
@interface SelectLoactionViewModel ()
@property(nonatomic,strong)BMKMapView *mapView;
@property(nonatomic,strong) CurrentUser *currentUser;
@property(nonatomic,strong)BMKGeoCodeSearch *geoSearch;
@property(nonatomic,strong) BMKLocationService *locationService;
@property(nonatomic,strong) NSMutableArray *userPoi;
@end
@implementation SelectLoactionViewModel{
    dispatch_group_t group;
    NSMutableArray *searchPoiArray;
    NSString *searchKeywords;
    NSString *city;
    BOOL searchNearby;
}
- (instancetype)initWithMapView:(BMKMapView *)mapView{
    self = [super init];
    if(self){
        searchPoiArray = [[NSMutableArray alloc] initWithCapacity:0];
        _userPoi = [[NSMutableArray alloc] initWithCapacity:0];
        
        _mapView = mapView;
        _currentUser= [CurrentUser getCurrentUser];
        [self locationService];
        city = @"杭州";

    }
    return self;
}
- (void)viewWillAppear{
    self.poisearch.delegate = self;
    self.geoSearch.delegate = self;
    [self.locationService startUserLocationService];
    self.locationService.delegate = self;
    
}
- (void)viewWillDisappear{
    self.poisearch.delegate = nil;
    self.geoSearch.delegate = nil;
    [self.locationService stopUserLocationService];
    self.locationService.delegate = nil;
}

- (BMKLocationService *)locationService{
    if(_locationService == nil){
        _locationService = [[BMKLocationService alloc] init];
        
    }
    return _locationService;
}
- (BMKPoiSearch *)poisearch{
    if(_poisearch == nil){
        _poisearch = [[BMKPoiSearch alloc] init];
    }
    return _poisearch;
}
- (BMKGeoCodeSearch *)geoSearch{
    if(_geoSearch == nil){
        _geoSearch = [[BMKGeoCodeSearch alloc] init];
    }
    return _geoSearch;
}
- (void)setIsNearbySearch:(BOOL)isNearbySearch{
    _isNearbySearch = isNearbySearch;
    searchNearby = isNearbySearch;
    if(searchKeywords == nil) return;
    if(searchNearby)[self poiNearbySearchWithKeywords:searchKeywords];
    else[self poiKeywordsSearch:searchKeywords];
    
}
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
 [[NSNotificationCenter defaultCenter] postNotificationName:BJHiddenKeyboardNotification object:nil];
}
#pragma mark -BMKMapViewDelegate
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    if([annotation isKindOfClass:[BJPointIndexAnnotation class]]){
        AnnotationIndexView *annotationView = [[AnnotationIndexView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiSearch"];
        BJPointIndexAnnotation *indexAnnotation = (BJPointIndexAnnotation *)annotation;
        annotationView.image = [UIImage imageNamed:@"dingwei"];
        annotationView.index = indexAnnotation.index;
        return annotationView;
    }
    return nil;
}
#pragma mark - BMKLocationServiceDelegate
- (void)didFailToLocateUserWithError:(NSError *)error{
    
}
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    [_mapView updateLocationData:userLocation];
    [self.locationService stopUserLocationService];
    [self.mapView setCenterCoordinate:self.locationService.userLocation.location.coordinate];
    BMKReverseGeoCodeOption *option = [[BMKReverseGeoCodeOption alloc] init];
    option.reverseGeoPoint = userLocation.location.coordinate;
     BOOL flag = [self.geoSearch reverseGeoCode:option];
    if(flag){
         NSLog(@"反geo检索发送成功");
        [self.locationService stopUserLocationService];
    }else{
     NSLog(@"反geo检索发送失败");
    }
}
#pragma mark -BMKGeoCodeSearchDelegate
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    city = result.addressDetail.city;
            NSArray *array = [NSMutableArray keyValuesArrayWithObjectArray:result.poiList];
        NSMutableArray *results =  [MapSearchResult objectArrayWithKeyValuesArray:array];
        [self.delegate searchResult:results];
    NSMutableArray *arrayAn = [NSMutableArray arrayWithArray:results];
    [self addAnnotation:arrayAn];
    
}
- (void)poiKeywordsSearch:(NSString *)keys{
    /**
     *  防止信号统计出错
     */
    group = nil;
    group = dispatch_group_create();
 [searchPoiArray removeAllObjects];
    BMKCitySearchOption *citySearchOption = [[BMKCitySearchOption alloc]init];
    citySearchOption.pageCapacity = 50;
    citySearchOption.city= city;
    citySearchOption.keyword = keys;
     dispatch_group_enter(group);
    [self.poisearch poiSearchInCity:citySearchOption];
    dispatch_group_enter(group);
    [self networkSearch:keys];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [searchPoiArray addObjectsFromArray:[self.userPoi copy]];
        [self.delegate searchResult:[searchPoiArray copy]];
        [self addAnnotation:[searchPoiArray copy]];
    });

}
- (void)poiNearbySearchWithKeywords:(NSString *)keys{
    group = nil;
    group = dispatch_group_create();
    [searchPoiArray removeAllObjects];
    BMKNearbySearchOption *option =[[BMKNearbySearchOption alloc] init];
    option.location = self.locationService.userLocation.location.coordinate;
    option.keyword = keys;
    option.sortType = BMK_POI_SORT_BY_DISTANCE;
    option.pageCapacity = 50;
    option.pageIndex = 0;
    option.radius = 10000;
    dispatch_group_enter(group);
    [self.poisearch poiSearchNearBy:option];
    dispatch_group_enter(group);
    [self networkSearch:keys];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [searchPoiArray addObjectsFromArray:[self.userPoi copy]];
            [self.delegate searchResult:[searchPoiArray copy]];
            [self addAnnotation:[searchPoiArray copy]];
    });
}
- (void)networkSearch:(NSString*)keys{
    [[CityMapNetworkManage sharedInstance] searchUserPlaceWithKeywords:keys complete:^(BOOL succeed, id obj) {
        if(succeed){
        [obj enumerateObjectsUsingBlock:^(id  _Nonnull objct, NSUInteger idx, BOOL * _Nonnull stop) {
            MapSearchResult *result = [[MapSearchResult alloc] init];
            [result setKeyValues:objct];
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = [objct[@"latitude"] floatValue];
            coordinate.longitude = [objct[@"longtitude"] floatValue];
            result.pt = coordinate;
            [self.userPoi addObject:result];
        }];
        }
        dispatch_group_leave(group);
    }];
}
#pragma mark - BMKPoiSearchDelegate
- (void)onGetPoiResult:(BMKPoiSearch*)searcher result:(BMKPoiResult*)poiResult errorCode:(BMKSearchErrorCode)errorCode{
    NSArray *array = [NSMutableArray keyValuesArrayWithObjectArray:poiResult.poiInfoList];
    searchPoiArray =  [MapSearchResult objectArrayWithKeyValuesArray:array];
    dispatch_group_leave(group);
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchKeywords = searchBar.text;
   if(searchNearby) [self poiNearbySearchWithKeywords:searchBar.text];
   else{
       [self poiKeywordsSearch:searchBar.text];
   }
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    searchKeywords = searchBar.text;
    return YES;
}

#pragma makr- addAnnotation

- (void)addAnnotation:(NSArray *)array{
    [self.mapView removeAnnotations:self.mapView.annotations];
    NSMutableArray *annotions = [[NSMutableArray alloc] init];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapSearchResult *poiObj = obj;
        BJPointIndexAnnotation *annotion = [[BJPointIndexAnnotation alloc] init];
        annotion.title = poiObj.name;
        annotion.coordinate = poiObj.pt;
        annotion.index = idx+1;
        [annotions addObject:annotion];
    }];
    [self.mapView addAnnotations:annotions];
    [_mapView showAnnotations:annotions animated:YES];
}
@end
