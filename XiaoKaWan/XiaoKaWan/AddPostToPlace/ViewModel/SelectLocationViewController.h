//
//  SelectLocationViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "LocationListViewModel.h"
#import "CreatLoactionObj.h"
@interface SelectLocationViewController : BJSuperViewController
@property(nonatomic,copy) void(^loaction)(MapSearchResult *result);
@property(nonatomic,copy) void(^creatLoaction)(CreatLoactionObj *locatin);
@end
