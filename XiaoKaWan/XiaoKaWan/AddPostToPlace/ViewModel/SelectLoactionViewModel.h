//
//  SelectLoactionViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import "MapSearchResult.h"
@protocol SelectLoactionViewModelDelegate <NSObject>
- (void)searchResult:(NSArray *)results;
//- (void)currentLocation:(MapSearchResult *)currentLocation;
@end
@interface SelectLoactionViewModel : NSObject<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKPoiSearchDelegate,BMKGeoCodeSearchDelegate,UISearchBarDelegate>
- (instancetype)initWithMapView:(BMKMapView *)mapView;
@property(nonatomic) BOOL isNearbySearch;
@property(nonatomic,strong)BMKPoiSearch* poisearch;
@property(nonatomic,assign) id<SelectLoactionViewModelDelegate>delegate;
- (void)viewWillAppear;
- (void)viewWillDisappear;
@end
