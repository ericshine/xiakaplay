//
//  SelectMapsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectMapsViewModel.h"

#import "SelectMapsCell.h"

@interface SelectMapsViewModel()
@property(nonatomic,strong)NSArray *mapsListArray;
@end
@implementation SelectMapsViewModel
- (instancetype)init{
    self = [super init];
    if(self){
        NSArray *myMaps = [Maps getMyMapsList];
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
        [myMaps enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Maps *maps = obj;
        NSInteger stast = [maps.status integerValue];
        if((stast&2)>0){
            [array addObject:maps];
                        }
        }];
        _mapsListArray = [array copy];
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mapsListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    SelectMapsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell){
        cell = [[SelectMapsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    Maps *maps = _mapsListArray[indexPath.row];
    [cell.imageView1 setImageWithUrlString:@""];
    cell.titleLb.text = maps.name;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     Maps *maps = _mapsListArray[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate selectMaps:maps];
}
@end
