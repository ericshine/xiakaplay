//
//  SelectLocationViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "CreatAddressWithMapViewController.h"
#import "UIImage+Category.h"
#define kTableHeight 310
@interface SelectLocationViewController ()<LocationListViewModelDelegate>
@property(nonatomic,strong)BMKMapView *mapView;
@property(nonatomic,strong)SelectLoactionViewModel *viewModel;
@property(nonatomic,strong)LocationListViewModel *locationViewModel;
@property(nonatomic,strong)UISearchBar *searchBar;
@end

@implementation SelectLocationViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self mapAppear];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self mapDisappear];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"创建地点"];
    [self mapView];
    self.tableView.frame = CGRectMake(0, CGRectGetMaxY(self.mapView.frame)-64, kDeviceWidth, kTableHeight);
    
    self.tableView.delegate = self.locationViewModel;
    self.tableView.dataSource = self.locationViewModel;
    [self searchBar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hiddenKeyboard) name:BJHiddenKeyboardNotification object:nil];
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    CreatAddressWithMapViewController *addressMap = [[CreatAddressWithMapViewController alloc] init];
    addressMap.creatLoaction = self.creatLoaction;
    addressMap.fromType = FROM_ADDPOST;
    [self push:addressMap];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)hiddenKeyboard{
    [self.searchBar resignFirstResponder];
}
- (void)mapDisappear{
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    [self.viewModel viewWillDisappear];
}
- (void)mapAppear{
    [self.mapView viewWillAppear];
    self.mapView.delegate = self.viewModel;
    self.mapView.showsUserLocation = NO;
    self.mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    self.mapView.showsUserLocation = YES;//显示定位图层
    [self.viewModel viewWillAppear];
}
- (LocationListViewModel *)locationViewModel{
    if(_locationViewModel == nil){
        _locationViewModel = [[LocationListViewModel alloc] initWithTableView:self.tableView];
        _locationViewModel.delegatel = self;
    }return _locationViewModel;
}
- (BMKMapView *)mapView{
    if(_mapView == nil){
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-kTableHeight)];
        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        _mapView.showsUserLocation = YES;
        _mapView.showMapScaleBar = NO;
        _mapView.zoomLevel = 10;
        BMKLocationViewDisplayParam *parameter = [[BMKLocationViewDisplayParam alloc] init];
        parameter.isAccuracyCircleShow = NO;
        [_mapView updateLocationViewWithParam:parameter];
        [self.view addSubview:_mapView];
    }
    return _mapView;
}

- (SelectLoactionViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[SelectLoactionViewModel alloc] initWithMapView:_mapView];
        _viewModel.delegate = self.locationViewModel;
    }
    return _viewModel;
}
- (UISearchBar *)searchBar{
    if(_searchBar == nil){
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(20, 20, kDeviceWidth-40, 30)];
        _searchBar.backgroundImage = [UIImage imageFromColor:[UIColor whiteColor]];
        _searchBar.layer.cornerRadius = 5;
        _searchBar.layer.masksToBounds = YES;
        _searchBar.placeholder = @"搜索更多地址";
        _searchBar.delegate = self.viewModel;
//        [_searchBar setContentMode:UIViewContentModeLeft];
        [self.view addSubview:_searchBar];
    }
    return _searchBar;
}
#pragma mark - LocationListViewModelDelegate
- (void)selectLocation:(MapSearchResult *)location{
    if(self.loaction)self.loaction(location);
    [self popViewController];
}
- (void)creatAddress{
    CreatAddressWithMapViewController *addressMap = [[CreatAddressWithMapViewController alloc] init];
    addressMap.creatLoaction = self.creatLoaction;
    addressMap.fromType = FROM_ADDPOST;
    [self push:addressMap];
}
- (void)changeSearchType:(BOOL)isNearbySearch{
    self.viewModel.isNearbySearch = isNearbySearch;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
