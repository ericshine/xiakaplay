//
//  AddPostViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostViewModel.h"
#import "ShowIageTableViewCell.h"
#import "PostContentCell.h"
//#import "IconTitleTableViewCell.h"
#import "PostCellFrameObject.h"
#import "AddPostunflodHeadView.h"
#import "AddPostListObj.h"
#import "AddPostListModel.h"
#import "SelectTagTabelViewCell.h"
typedef NS_ENUM(NSInteger, Unfold){
    UNFOLD,
    UNFOLD_YES,
    UNFOLD_NO
};
static NSInteger const kContentCellHeight = 90.5;
@interface AddPostViewModel ()<AddPostunflodHeadViewDelegate>;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic)Unfold unflod;
@property(nonatomic,strong)AddPostunflodHeadView *headView;
@property(nonatomic,strong)AddPostListModel *model;
@property(nonatomic)PostType postType;
@end
@implementation AddPostViewModel{
  PostCellFrameObject *cellFrame;
    NSArray *tagArray;
}
- (instancetype)initWithTable:(UITableView *)table postType:(PostType)type{
    self = [super init];
    if(self){
        _postType = type;
        _model = [[AddPostListModel alloc] init];
        _unflod = UNFOLD_NO;
        cellFrame = [PostCellFrameObject cellFrame];
        _tableView = table;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return self;
}
- (AddPostunflodHeadView *)headView{
    if(_headView == nil){
        _headView = [[AddPostunflodHeadView alloc] init];
        _headView.delegate = self;
    }
    return _headView;
}
- (void)setTags:(NSArray *)tags{
    _tags = tags;
    tagArray = tags;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)setLocationObj:(MapSearchResult *)locationObj{
    _locationObj = locationObj;
    AddPostListObj *object = _model.section1[1];
    object.detail = locationObj.name;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)setMaps:(Maps *)maps{
    _maps = maps;
        AddPostListObj *object = _model.section2[1];
    object.detail = maps.name;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)setImages:(NSMutableArray *)images{
    _images = images;
    if(images.count <9){
    cellFrame.totalCount = images.count + 1;
    }else cellFrame.totalCount = images.count;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return self.model.section1.count+2;
    }
    else {
//        if(_unflod == UNFOLD_YES){
//            return 2;
//        }else return 0;
        return self.model.section2.count;
    };
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(indexPath.row == 0) {
             return cellFrame.imageCellHeight;
        }
        else if(indexPath.row ==1) return kContentCellHeight;
        else if(indexPath.row == 2 && tagArray.count>0) {
            return 100;
        }
        else return 44;
    }else{
        return 44;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if(section == 1){
//        return 30;
//    }
    return 0.01;
}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if(section == 1){
//        return self.headView;
//    }
//    return nil;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if(indexPath.section == 0 && indexPath.row == 0){
    static NSString *imageCellIdentify = @"ShowIageTableViewCell";
    ShowIageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:imageCellIdentify];
    if(!cell){
        cell = [[ShowIageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:imageCellIdentify];
        cell.selectionStyle =  UITableViewCellSelectionStyleNone;
       
    }
         cell.selelctedImageCallBack = self.selelctedImageCallBack;
        cell.imageArray = _images;
    return cell;
        }else if(indexPath.section == 0 &&indexPath.row ==1){
        static NSString *contentId = @"PostContentCell";
            PostContentCell *cell = [tableView dequeueReusableCellWithIdentifier:contentId];
            if(!cell) {
                cell = [[PostContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:contentId];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return cell;
        }else{
        static NSString *postCellId = @"IconTitleTableViewCell";
        SelectTagTabelViewCell *cell = [tableView dequeueReusableCellWithIdentifier:postCellId];
        if(!cell){
            cell = [[SelectTagTabelViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:postCellId];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            AddPostListObj *object;
            if(indexPath.section == 0){
            object = _model.section1[indexPath.row-2];
            }else{
                object = _model.section2[indexPath.row];
            }
            if(indexPath.section == 0 && indexPath.row == 2) cell.tags = tagArray;
            else cell.tags = nil;
            cell.titleLabel.text = object.title;
            cell.iconLabel.text = object.icon;
            cell.detailLabel.text = object.detail;
            return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *path = [NSIndexPath indexPathForRow:1 inSection:0];
    PostContentCell *cell = [tableView cellForRowAtIndexPath:path];
    [cell.textView resignFirstResponder];
    if([self.delegate respondsToSelector:@selector(tableViewDidSelectRowAtIndexPath:)]){
        [self.delegate tableViewDidSelectRowAtIndexPath:indexPath];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSIndexPath *path = [NSIndexPath indexPathForRow:1 inSection:0];
    PostContentCell *cell = [self.tableView cellForRowAtIndexPath:path];
    [cell.textView resignFirstResponder];
}
#pragma mark - BJTimeSelectViewDelegate
- (void)selectDate:(NSDate *)date{
    AddPostListObj *object = _model.section2[0];
    object.detail = [date dateToStringWithFormat:@"yyyy-MM-dd"];
    _postDate = object.detail;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma makr - AddPostunflodHeadViewDelegate
- (void)unflodAction:(BOOL)unflod{
    if(unflod) self.unflod = UNFOLD_YES;
    else self.unflod = UNFOLD_NO;
    [self.tableView reloadData];
}
@end
