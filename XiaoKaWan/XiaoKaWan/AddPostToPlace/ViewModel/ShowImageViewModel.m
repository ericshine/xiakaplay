//
//  ShowImageViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ShowImageViewModel.h"
#import "ImageSelectCollectionViewCell.h"
#import "PhotoObject.h"
#import "AddimageTitleCollectView.h"
#import "UIColor+colorWithHex.h"
#import "ClientNetwork.h"
#import "BJNotificationConfig.h"
static NSString * const kImageCellIdentifier = @"imageCellIdentifier";
static NSString * const kAddImageIdentifier = @"addImageIdentifier";
@interface ShowImageViewModel()
@property(nonatomic,strong)UICollectionView *collectionView;
@end
@implementation ShowImageViewModel{
    dispatch_group_t group;
}
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        _collectionView = collectionView;
        [_collectionView registerClass:[ImageSelectCollectionViewCell class] forCellWithReuseIdentifier:kImageCellIdentifier];
        [_collectionView registerClass:[ImageSelectCollectionViewCell class] forCellWithReuseIdentifier:kAddImageIdentifier];
        group = dispatch_group_create();
        
    }
    return self;
}
- (void)setSelectImages:(NSArray *)selectImages{
    _selectImages = selectImages;
    [self.collectionView reloadData];
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(self.selectImages.count <9 )return self.selectImages.count + 1;
    return self.selectImages.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if((self.selectImages.count <9) && (indexPath.row == self.selectImages.count)){
        ImageSelectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAddImageIdentifier forIndexPath:indexPath];
//        cell.titleLb.font = [UIFont fontWithName:@"iconfont" size:40];
//        cell.titleLb.text =  @"\U0000e61c";
//        cell.titleLb.textColor = [UIColor p_colorWithHex:0xa3a6a6];
//        cell.contentView.backgroundColor =[UIColor p_colorWithHex:0xe3e6e6];
        cell.imageView.image = [UIImage imageNamed:@"addImage"];
        return cell;
    }
    ImageSelectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellIdentifier forIndexPath:indexPath];
    PhotoObject *photo = _selectImages[indexPath.row];
    cell.imageView.image = photo.thumbImage;
    if(photo.aid==nil){
        [cell showHud];
        dispatch_group_enter(group);
    [[ClientNetwork sharedInstance] uploadImageWithPath:[APIConfig sharedInstance].upLoadImageApi image:photo.image imageName:@"file" complete:^(BOOL succeed, id obj) {
        if(succeed){
            photo.aid = obj[@"aid"];
            cell.imageView.alpha = 1;
            [cell disMissHud];
             dispatch_group_leave(group);
        }
       
    } pregress:^(CGFloat pregress) {
        
    }];
    }
    if(indexPath.row == _selectImages.count-1){
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:BJUploadAllImageNotification object:nil];
        });
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if((self.selectImages.count <9) && (indexPath.row == self.selectImages.count)){
    // 添加照片
        if(self.selelctImageCallBack)self.selelctImageCallBack(YES,indexPath.row);
    }else{
        PhotoObject *photo = _selectImages[indexPath.row];
        if(photo.aid != nil){
        if(self.selelctImageCallBack)self.selelctImageCallBack(NO,indexPath.row);
        }
    }
    
}
@end
