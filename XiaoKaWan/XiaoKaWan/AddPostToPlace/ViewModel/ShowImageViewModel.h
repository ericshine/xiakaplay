//
//  ShowImageViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ShowImageViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;
@property(nonatomic,strong)NSArray *selectImages;
@property(nonatomic,copy) void(^selelctImageCallBack)(BOOL addImage,NSInteger index);
@end
