//
//  SelectMapsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Maps.h"
@protocol SelectMapsViewModelDelegate <NSObject>

- (void)selectMaps:(Maps *)maps;

@end
@interface SelectMapsViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)id<SelectMapsViewModelDelegate>delegate;

@end

