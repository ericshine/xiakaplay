//
//  SelectCategoryViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SelectTableViewCell.h"
#import "CategoryObject.h"
@interface SelectCategoryViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *selectCagegorys;
- (instancetype)initWithTabel:(UITableView *)table;
@end
