//
//  CreatAddressViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CreatAddressViewModel.h"
#import "CreatAddressModel.h"
#import "SelectCityPopView.h"
@interface CreatAddressViewModel ()
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)CreatAddressModel *model;
@property(nonatomic,strong)SelectCityPopView *selectCityView;
@property(nonatomic,copy) NSString *district;
@end
@implementation CreatAddressViewModel
- (instancetype)initWithTable:(UITableView *)table andAddress:(NSString *)address{
    self = [super init];
    if(self){
        _district = address;
        _tableView = table;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return self;
}
- (CreatAddressModel *)model{
    if(_model == nil){
        _model = [[CreatAddressModel alloc] init];
    }
    return _model;
}
- (SelectCityPopView *)selectCityView{
    if(_selectCityView == nil){
        _selectCityView = [[SelectCityPopView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 230)];
    }
    return _selectCityView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.model.listArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == self.model.listArray.count-1) return 120;
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *creatCellId = @"creatCell";
   CreatAddressModel *model = self.model.listArray[indexPath.row];
    CreatAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:creatCellId];
    if(!cell){
        cell = [[CreatAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:creatCellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if(indexPath.row == self.model.listArray.count-1) {
        cell.textView.hidden = NO;
        cell.textField.hidden = YES;
        cell.lineView.hidden = YES;
    }else{
        cell.textView.hidden = YES;
        cell.textField.hidden = NO;
    }
    if(indexPath.row == 0){
        cell.textField.enabled = NO;
        model.content = _district;
    }
    if(indexPath.row ==3){
        cell.textField.enabled = NO;
    }
    cell.model = model;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==3){
        [[NSNotificationCenter defaultCenter] postNotificationName:BJHiddenKeyboardNotification object:nil];
        CreatAddressModel *model = self.model.listArray[indexPath.row];
        [self.delegate selectCagegory:^(CategoryObject *category) {
            model.content = category.title;
            [self.tableView reloadData];
            self.category = category;
        }];
    }
    
}
@end
