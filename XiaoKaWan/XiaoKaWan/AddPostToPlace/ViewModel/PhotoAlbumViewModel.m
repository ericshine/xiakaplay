//
//  PhotoAlbumViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/11/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PhotoAlbumViewModel.h"
#import "QAlertView.h"
#import "UIImage+Category.h"

static NSString* const cellID = @"photoCollectionId";
#define kThumb 200
@implementation PhotoAlbumViewModel{
    PHFetchResult *assetsFetchResults;
    UICollectionView *_collectionView;
}
- (instancetype)initWihCollectionView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        _collectionView = collectionView;
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
        [collectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:cellID];
        _selectImages = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return assetsFetchResults.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
     PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
//    options.synchronous = YES;
    cell.indexPath = indexPath;
    cell.delegate = self;
    PHAsset *asset = assetsFetchResults[indexPath.row];
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
    [imageManager requestImageForAsset:asset targetSize:CGSizeMake(kThumb, kThumb) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        if (result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([_selectImages containsObject:asset]){
                    cell.selectButton.selected = YES;
                }else{
                    cell.selectButton.selected = NO;
                }
                cell.imageView.image=[UIImage cutImage:result WithSize:CGSizeMake(300, 300)];
            });
        }

    }];
});
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.previewDeleage selectImages:assetsFetchResults atIndex:indexPath.row andSelectImages:_selectImages];
}
#pragma mark - PhotoCollectionViewCellDelegate
- (void)selectItemAtIndex:(NSIndexPath *)index{
    
    PHAsset *asset = assetsFetchResults[index.row];
    if([_selectImages containsObject:asset]){
        [_selectImages removeObject:asset];
    }else{
        if(_selectImages.count>= self.limitCount){
            [[QAlertView sharedInstance] showAlertText:[NSString stringWithFormat:@"您最多只能选择%li张照片",(long)self.limitCount] fadeTime:2];
            return;
        }
        [_selectImages addObject:asset];
    }
    [_collectionView reloadItemsAtIndexPaths:@[index]];
    if([self.delegate respondsToSelector:@selector(selectImages:)]){
        [self.delegate selectImages:_selectImages];
    }
    

}
@end
