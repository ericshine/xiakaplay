//
//  LocationListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SelectLoactionViewModel.h"
@protocol LocationListViewModelDelegate <NSObject>

- (void)selectLocation:(MapSearchResult *)location;
- (void)creatAddress;
- (void)changeSearchType:(BOOL)isNearbySearch;
@end
@interface LocationListViewModel : NSObject<UITableViewDelegate,UITableViewDataSource,SelectLoactionViewModelDelegate>
@property(nonatomic,assign) id<LocationListViewModelDelegate>delegatel;
- (instancetype)initWithTableView:(UITableView *)table;
@end

