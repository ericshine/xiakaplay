//
//  CreatAddressViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CategoryObject.h"
#import "CreatAddressCell.h"
@protocol CreatAddressViewModelDelegate <NSObject>

- (void)selectCagegory:(void(^)(CategoryObject *category))category;

@end
@interface CreatAddressViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign) id<CreatAddressViewModelDelegate>delegate;
@property(nonatomic,strong) CategoryObject *category;
- (instancetype)initWithTable:(UITableView *)table andAddress:(NSString*)address;
@end

