//
//  SelecctTagsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SelecctTagsViewModel.h"
#import "TagsTableViewCell.h"
#import "AddPostNetworkManage.h"
@interface SelecctTagsViewModel()<TagsTableViewCellDelegate>
@property(nonatomic,strong) UITableView *tableView;
@end
@implementation SelecctTagsViewModel{
    CGFloat tagCellHeight;
    NSMutableArray *tagsArray;
}
- (instancetype)initWithTable:(UITableView *)table{
    self = [super init];
    if(self){
        self.tableView = table;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tagCellHeight = 100;
        tagsArray = [[NSMutableArray alloc] init];
        [[AddPostNetworkManage sharedInstance] getPostTagsComplete:^(BOOL succeed, id obj) {
            if(succeed){
                tagsArray = [NSMutableArray arrayWithArray:obj];
                [self.tableView reloadData];
            }
        }];
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger row = ceil(tagsArray.count/3.0);
    return row;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

        static NSString *tagId  = @"tagId";
        TagsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tagId];
        if(!cell){
            cell = [[TagsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tagId];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
    NSInteger arrayLength;
    if((tagsArray.count-indexPath.row*3)<3) arrayLength =  tagsArray.count-indexPath.row*3;
    else arrayLength= 3;
    cell.tags = [tagsArray subarrayWithRange:NSMakeRange(indexPath.row*3, arrayLength)];
        return cell;
}
#pragma mark - TagsTableViewCellDelegate
- (void)selectTagWithString:(NSString *)text{
    [self.delegate selectTag:text];
}
@end
