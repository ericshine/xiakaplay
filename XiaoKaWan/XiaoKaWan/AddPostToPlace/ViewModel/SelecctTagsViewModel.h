//
//  SelecctTagsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol SelecctTagsViewModelDelegate <NSObject>

- (void)selectTag:(NSString *)tag;

@end
@interface SelecctTagsViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)id<SelecctTagsViewModelDelegate>delegate;
- (instancetype)initWithTable:(UITableView *)table;
@end

