//
//  CityMapViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapViewController.h"
#import "CityMapViewModel.h"
#import "AddPostPopView.h"
#import "PhotoAlbumViewController.h"
#import "BJNavigationController.h"
#import "AddPostVViewController.h"
#import "PhotoObject.h"
#import "MapHeadViewModel.h"
#import "MapsPlaceDetailViewController.h"
#import "Place.h"
#import "BJMapPlacePopView.h"
#import "ThirdpartyNavigation.h"
#import "CreatAddressWithMapViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PlacePostDetailViewController.h"
#import "PostObjet.h"
@interface CityMapViewController ()<AddPostPopViewDelegate,CityMapViewModelDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UINavigationControllerDelegate,BJMapPlacePopViewDelegate>
@property(nonatomic,strong) CityMapHeadView *headView;
@property(nonatomic,strong) MapHeadViewModel *headViewModel;
@property(nonatomic,strong) CityMapsSearchResultView *searchResultView;
@property(nonatomic,strong) BMKMapView *mapView;

@property(nonatomic,strong) CityMapViewModel *viewModel;
@property(nonatomic,strong) UIButton *goBackButton;
@property(nonatomic,strong) UIButton *addPost;
@property(nonatomic,strong) AddPostPopView *addPostPopView;
@property(nonatomic,strong)BJMapPlacePopView *placePopView;
@end

@implementation CityMapViewController{
   
}
+ (void)initialize{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"mapCustom_config" ofType:@""];
    [BMKMapView customMapStyle:path];

}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
   self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.hiddenNav = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.hiddenNav = YES;
    [self.mapView viewWillAppear];
    self.mapView.delegate = self.viewModel;
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self mapAppear];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.hiddenNav = NO;
    [self mapDisappear];

}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /***  添加view *****/
    self.navigationController.delegate = self;
    [self headView];
    
    [self mapView];
    [self goBackButton];
    [self addPost];
    _headView.delegate = _viewModel;
    _headView.searchDataDelegate = self.searchResultView;
    _headView.dataSource = self.headViewModel;
    // Do any additional setup after loading the view.
}
- (void)mapDisappear{
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    
    [self.viewModel viewWillDisappear];
}
- (void)mapAppear{
    
    self.mapView.showsUserLocation = NO;
    self.mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    self.mapView.showsUserLocation = YES;//显示定位图层
    
    [self.viewModel viewWillAppear];
}
/**
 *  headVuew
 *
 *  @return return value description
 */
- (CityMapHeadView *)headView{
    if(_headView == nil){
        __weak typeof(self) weakSelf = self;
        _headView = [[CityMapHeadView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 95)];
        _headView.startSearch = ^(){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.searchResultView showAtView:strongSelf.tabBarController.view];
        };
        _headView.endSearch = ^(){
              __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.searchResultView disMiss];
            strongSelf.searchResultView.dataList = nil;
            [strongSelf.searchResultView.tableView reloadData];
        };
        [self.view addSubview:_headView];
    }
    return _headView;
}
- (MapHeadViewModel *)headViewModel{
    if(_headViewModel == nil){
        _headViewModel = [[MapHeadViewModel alloc] initWithView:_headView];
    }
    return _headViewModel;
}
/**
 *  添加地图
 *
 *  @return return value description
 */
- (BMKMapView *)mapView{
    if(_mapView == nil){
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_headView.frame), kDeviceWidth, kDeviceHeight-CGRectGetMaxY(_headView.frame))];
        [_mapView setMapType:BMKMapTypeStandard];
        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        _mapView.showsUserLocation = YES;
        _mapView.showMapScaleBar = NO;
        _mapView.zoomLevel = 18;
        _mapView.centerCoordinate = CLLocationCoordinate2DMake(30.257238, 120.151570);
//        _mapView.showMapPoi = NO;
//         _mapView.buildingsEnabled = NO;
        BMKLocationViewDisplayParam *parameter = [[BMKLocationViewDisplayParam alloc] init];
        parameter.isAccuracyCircleShow = NO;
        [_mapView updateLocationViewWithParam:parameter];
        [self.view addSubview:_mapView];
    }
    return _mapView;
}

- (CityMapViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[CityMapViewModel alloc] initWithMap:_mapView];
        _viewModel.delegate = self;
        __weak typeof(self) weakSelf = self;
        _viewModel.dissMissPlacePopView = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.placePopView disMissView];
        };
    }
    return _viewModel;
}
- (BJMapPlacePopView *)placePopView{
    if(_placePopView == nil){
        _placePopView = [[BJMapPlacePopView alloc] initWithFrame:CGRectMake(0, kDeviceHeight, kDeviceWidth, popViewHeight)];
        [self.view addSubview:_placePopView];
        _placePopView.delegate = self;
    }
    return _placePopView;
}
/**
 *  定位按钮
 */
- (UIButton *)goBackButton{
    if(_goBackButton == nil){
        _goBackButton = [[UIButton alloc] initWithFrame:CGRectMake(13, kDeviceHeight-90, 30, 30)];
        [_goBackButton setBackgroundImage:[UIImage imageNamed:@"userLoaction"] forState:UIControlStateNormal];
        [_goBackButton addTarget:self.viewModel action:@selector(userLocationAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_goBackButton];
    }
    return _goBackButton;
}
- (UIButton *)addPost{
    if(_addPost == nil){
        _addPost = [[UIButton alloc] initWithFrame:CGRectMake(kDeviceWidth-60, kDeviceHeight-108, 50, 50)];
        [_addPost setBackgroundImage:[UIImage imageNamed:@"post_button1"] forState:UIControlStateNormal];
        [self.view addSubview:_addPost];
        [_addPost addTarget:self action:@selector(addPostAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addPost;
}
- (AddPostPopView *)addPostPopView{
    if(_addPostPopView == nil){
        _addPostPopView = [[AddPostPopView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        [_addPostPopView addItems:@[@"paot_camera",@"post_photo",@"post_text"]];
        _addPostPopView.delegate = self;
    }
    return _addPostPopView;
}
- (void)addPostAction{
    if([UserConfig sharedInstance].user_token.length) [self.addPostPopView showInSuperView:self.tabBarController.view buttonPoint:_addPost.frame.origin buttonInView:self.view];
    else{
        [self toLoginVcClearData:NO];
    }
}
/**
 *  搜索结果view
 *
 *  @return return value description
 */
- (CityMapsSearchResultView *)searchResultView{
    if(_searchResultView == nil){
        _searchResultView = [[CityMapsSearchResultView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_headView.frame), kDeviceWidth, kDeviceHeight-CGRectGetMaxY(_headView.frame))];
        _searchResultView.backgroundColor = [UIColor whiteColor];
        _searchResultView.delegate = _viewModel;
        __weak typeof(self) weakSelf = self;
        _searchResultView.creatAddress = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            CreatAddressWithMapViewController *creatLocationVc = [[CreatAddressWithMapViewController alloc] init];
            creatLocationVc.creatLoaction = ^(CreatLoactionObj *location){
                strongSelf.viewModel.placeNewLocation = location;
            };
             creatLocationVc.fromType = FROM_HOMEMAP;
            [strongSelf push:creatLocationVc];
        };
    }
    return _searchResultView;
}
#pragma mark - CityMapViewModelDelegate
- (void)selectAnnotation:(Place *)place annotationPoint:(CGPoint)point{
//    CGPoint point1 = [self.placePopView convertPoint:point fromView:self.mapView];
//    [self.placePopView showInView:self.tabBarController.view fromPoint:point1];
    [self.placePopView showPopViewWithPlace:place];
}
- (void)selectAnnotationToPostDetailWithParameter:(Place *)place{
    PostParatemer *parameter = [PostParatemer parameter];
    parameter.post_id = [NSString stringWithFormat:@"%li",(long)place.last_post.id];
    PlacePostDetailViewController *controller = [[PlacePostDetailViewController alloc] init];
    controller.parameter = parameter;
    [self push:controller];
}
#pragma mark - AddPostPopViewDelegate
- (void)selectAtIndex:(NSInteger)index{
    [self.addPostPopView disMiss];
    switch (index) {
        case 0:
        {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if(status == AVAuthorizationStatusDenied){
                BJAlertController *alertVc = [[BJAlertController alloc] initWithTitle:@"小咖玩想要使用您的相机" message:nil preferredStyle:UIAlertControllerStyleAlert];
                [alertVc addActionWithTitle:@"设置" handler:^{
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }];
                [alertVc addCancleActionHandler:nil];
                
                [self presentViewController:alertVc animated:YES completion:nil];
            }else{
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.delegate = self;
            [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }break;
        case 1:
        {
            PhotoAlbumViewController *photoAlbum = [[PhotoAlbumViewController alloc] init];
            photoAlbum.limitCount = 9;
            [self presentViewController:[[BJNavigationController alloc] initWithRootViewController:photoAlbum] animated:YES completion:nil];
            __weak typeof(self) weakSelf = self;
            photoAlbum.seleltImages = ^(NSArray *images){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf addPostToPlace:images];
            };
        }break;
        case 2:
        {
            AddPostVViewController *addPost = [[AddPostVViewController alloc] init];
            addPost.postType = POST_TEXT;
            [self push:addPost];
            
        }break;
            
        default:
            break;
    }
}
#pragma mark - BJMapPlacePopViewDelegate
/**
 *  第三方APP地图导航
 *
 *  @param place place description
 */
- (void)placeNavigationWithPlace:(Place *)place{
   UIAlertController *alertVc = [[ThirdpartyNavigation sharedInstance] navigationAlertControllerFromPlace:self.viewModel.locationService.userLocation.location.coordinate toPlace:place];
    [self presentViewController:alertVc animated:YES completion:nil];
}

- (void)placeDetail:(Place *)place{
    place.placeId = [NSString stringWithFormat:@"%li",(long)place.id];
    MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
    placeDetail.place = place;
    [self push:placeDetail];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    PhotoObject *photo = [[PhotoObject alloc] init];
    photo.image = image;
    photo.thumbImage = image;
    [self addPostToPlace:@[photo]];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
     [picker dismissViewControllerAnimated:YES completion:^{
     }];
   
}
- (void)addPostToPlace:(NSArray *)images{
    AddPostVViewController *addPost = [[AddPostVViewController alloc] init];
    addPost.photoObjs = images;
    addPost.postType = POST_IMAGE;
    [self push:addPost];
}

#pragma mark - UINavigationController delegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
   [self.navigationController setNavigationBarHidden:self.hiddenNav animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
