//
//  ThirdpartyNavigation.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeadFileConfig.h"
#import "Place.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface ThirdpartyNavigation : NSObject
SINGLETON_H
- (UIAlertController *)navigationAlertControllerFromPlace:(CLLocationCoordinate2D) coordinate toPlace:(Place*)place;
@end
