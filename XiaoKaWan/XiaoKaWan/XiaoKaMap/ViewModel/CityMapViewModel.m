//
//  CityMapViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapViewModel.h"
#import "MapSearchResult.h"
#import <MJExtension.h>
#import "ImageSizeUrl.h"
#import "CurrentUser.h"
#import <UIImageView+WebCache.h>
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>
#import "BJPostAnnotationView.h"
#import "CityMapNetworkManage.h"
#import "Place.h"
#import "PostObjet.h"
#import "AnnatationContentPopView.h"
#import "PostBMKPointAnnotation.h"
#import "AnnatationImagePopView.h"
#import "PlaceCategory.h"
#import "CityMapNetworkManage.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
@interface CityMapViewModel ()<BJPostAnnotationViewDelegate,BMKGeoCodeSearchDelegate,AnnatationImagePopViewDelegate,AnnatationContentPopViewDelegate>
@property(nonatomic,strong)BMKMapView *mapView;
@property(nonatomic,strong)CategoryObject *currentCategory;
@property(nonatomic,copy) SearchResultArray callBackBlock;
@property(nonatomic,copy) void(^searchSuccess)(BOOL succcess);
@property(nonatomic,strong) CurrentUser *currentUser;
@property(nonatomic,strong) NSTimer *annotationTimer;
@property(nonatomic,strong)BMKCitySearchOption *citySearchOption;
@property(nonatomic,strong)BMKGeoCodeSearch *geoSearch;
@end
@implementation CityMapViewModel{
    NSArray *postList;
    NSMutableArray *postAnnotations;
    BMKPointAnnotation *searchAnnotion;
    BOOL newCreatLocation;// 新建地点回来在定位的方法里不拉去数据
    dispatch_group_t group;
    NSMutableArray *baiPoiArray;
    NSMutableArray *userPoiArray;
    NSMutableArray *allPoiArray;
    NSString *cityName;
}
- (instancetype)initWithMap:(BMKMapView *)mapView{
    self = [super init];
    if(self){
        group = dispatch_group_create();
        baiPoiArray = [[NSMutableArray alloc] initWithCapacity:0];
        userPoiArray = [[NSMutableArray alloc] initWithCapacity:0];
        postAnnotations = [[NSMutableArray alloc] initWithCapacity:0];
        allPoiArray = [[NSMutableArray alloc] initWithCapacity:0];
        _currentUser= [CurrentUser getCurrentUser];
        _mapView = mapView;
        cityName = @"杭州";///default
    }
    return self;
}
- (void)viewWillAppear{
    self.poisearch.delegate = self;
    self.geoSearch.delegate = self;
    [self locationService];
    self.locationService.delegate = self;
    [self getData];
}
- (void)viewWillDisappear{
    self.poisearch.delegate = nil;
    self.geoSearch.delegate = nil;
//    [self.locationService stopUserLocationService];
    self.locationService.delegate = nil;
}
- (BMKPoiSearch *)poisearch{
    if(_poisearch == nil){
        _poisearch = [[BMKPoiSearch alloc] init];
    }
    return _poisearch;
}
- (BMKGeoCodeSearch *)geoSearch{
    if(_geoSearch == nil){
        _geoSearch = [[BMKGeoCodeSearch alloc] init];
    }
    return _geoSearch;
}
- (BMKCitySearchOption *)citySearchOption{
    if(_citySearchOption == nil){
    _citySearchOption = [[BMKCitySearchOption alloc]init];
    _citySearchOption.pageCapacity = 20;
    _citySearchOption.city= cityName;
    }
    return _citySearchOption;
}
- (BMKLocationService *)locationService{
    if(_locationService == nil){
        _locationService = [[BMKLocationService alloc] init];
        [_locationService startUserLocationService];
    }
    return _locationService;
}
- (void)setPlaceNewLocation:(CreatLoactionObj *)placeNewLocation{
    newCreatLocation = YES;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [placeNewLocation.lat floatValue];
    coordinate.longitude = [placeNewLocation.lng floatValue];
    BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    annotation.title = placeNewLocation.place_name;
    searchAnnotion = annotation;
    [self.mapView setCenterCoordinate:coordinate animated:YES];
}
#pragma mark - netWork
- (void)getData{
    CGPoint point1 = CGPointMake(_mapView.frame.size.width, 20);
    CLLocationCoordinate2D coordinate1 = [_mapView convertPoint:point1 toCoordinateFromView:_mapView];
    CGPoint point2 = CGPointMake(20, _mapView.frame.size.height-64);
    CLLocationCoordinate2D coordinate2 = [_mapView convertPoint:point2 toCoordinateFromView:_mapView];
    CityMapPlaceNearbyParameter *parameter = [[CityMapPlaceNearbyParameter alloc] init];
    parameter.category_id = self.currentCategory.id;
    parameter._t = [UserConfig sharedInstance].user_token;
    parameter.location1 = [NSString stringWithFormat:@"%f,%f",coordinate1.longitude,coordinate1.latitude];
    parameter.location2 = [NSString stringWithFormat:@"%f,%f",coordinate2.longitude,coordinate2.latitude];
    [[CityMapNetworkManage sharedInstance] getPlaceNearbyWithParameter:parameter complete:^(BOOL succeed, id obj) {
        NSArray *dataArray = [Place objectArrayWithKeyValuesArray:obj];
        [self addAnnotations:dataArray];
    }];
}
- (void)addAnnotations:(NSArray *)array{
    postList = [self sortArrayByDistance:array];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [postAnnotations removeAllObjects];
    [_annotationTimer invalidate];
    _annotationTimer = nil;
    [postList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Place *place = obj;
        PostBMKPointAnnotation *annotation = [place placeToAnnotation];
        [self.mapView addAnnotation:annotation];
        if(place.last_post){
            [postAnnotations addObject:annotation];
        }
    }];
    [self showPost];
    if(searchAnnotion !=nil) {
        [self.mapView addAnnotation:searchAnnotion];
        [self.mapView showAnnotations:@[searchAnnotion] animated:YES];
    }

    searchAnnotion = nil;
    NSInteger timeCount = (postAnnotations.count-1) * 5;
    _annotationTimer = [NSTimer scheduledTimerWithTimeInterval:timeCount target:self selector:@selector(showPost) userInfo:nil repeats:YES];
}

- (void)showPost{
    [postAnnotations enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PostBMKPointAnnotation *annotation = obj;
        [self performSelector:@selector(autShowAnnatation:) withObject:annotation afterDelay:idx*5];
    }];
}
- (void)autShowAnnatation:(id)annatation{
    PostBMKPointAnnotation *annotationCur = annatation;
    [self.mapView selectAnnotation:annotationCur animated:YES];
}
#pragma mark - BMKLocationServiceDelegate
- (void)didFailToLocateUserWithError:(NSError *)error{

}
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
    //[_mapView updateLocationData:userLocation];
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
     [_mapView updateLocationData:userLocation];
    [self.locationService stopUserLocationService];
    [self showUserLocation];
    [self.mapView setCenterCoordinate:userLocation.location.coordinate];
    BMKReverseGeoCodeOption *option = [[BMKReverseGeoCodeOption alloc] init];
    option.reverseGeoPoint = userLocation.location.coordinate;
    BOOL flag = [self.geoSearch reverseGeoCode:option];
    if(flag){
        NSLog(@"反geo检索发送成功");
        [self.locationService stopUserLocationService];
    }else{
        NSLog(@"反geo检索发送失败");
    }
    if(!newCreatLocation) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       [self getData];
    });
     newCreatLocation = NO;
}
- (void)showUserLocation{
    _mapView.showsUserLocation = NO;
    _mapView.userTrackingMode = BMKUserTrackingModeNone;
    _mapView.showsUserLocation = YES;
    
}
- (void)userLocationAction{
    [self.locationService startUserLocationService];
    
}
#pragma mark -BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView{

}
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] postNotificationName:BJHiddenKeyboardNotification object:nil];
    if(self.currentCategory){
        [self getData];
    }
}


#pragma mark -BMKGeoCodeSearchDelegate
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    cityName = result.addressDetail.city;
     _citySearchOption.city= cityName;
}
#pragma mark - BMKPoiSearchDelegate
- (void)onGetPoiResult:(BMKPoiSearch *)searcher result:(BMKPoiResult *)poiResult errorCode:(BMKSearchErrorCode)errorCode{
    NSArray *array = [NSMutableArray keyValuesArrayWithObjectArray:poiResult.poiInfoList];
    NSMutableArray *array1 =  [MapSearchResult objectArrayWithKeyValuesArray:array];
    [baiPoiArray addObjectsFromArray:array1];
    dispatch_group_leave(group);
}

#pragma mark -BMKMapViewDelegate
- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate{
    [[NSNotificationCenter defaultCenter] postNotificationName:BJHiddenKeyboardNotification object:nil];
    if(self.dissMissPlacePopView)self.dissMissPlacePopView();
}
- (void)mapView:(BMKMapView *)mapView annotationViewForBubble:(BMKAnnotationView *)view {
    if([view.annotation isKindOfClass:[PostBMKPointAnnotation class]]){
    PostBMKPointAnnotation *annotation = (PostBMKPointAnnotation *)view.annotation;
   // CGPoint point = [self.mapView convertCoordinate:annotation.coordinate toPointToView:mapView];
//    [self.delegate selectAnnotation:annotation.place annotationPoint:view.frame.origin];
        [self.delegate selectAnnotationToPostDetailWithParameter:annotation.place];
    }
}
//- (void)mapView:(BMKMapView *)mapView onClickedMapPoi:(BMKMapPoi *)mapPoi{
//    
//}
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    if([annotation isKindOfClass:[PostBMKPointAnnotation class]]){
        BJPostAnnotationView *annotationView = [[BJPostAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiSearch"];
        annotationView.delegate = self;
         PostBMKPointAnnotation *annotation2 = (PostBMKPointAnnotation*)annotation;
        annotationView.image = [UIImage imageNamed:@"mapAnnotation"];
        if(annotation2.place.last_post)annotationView.canShowCallout = YES;
        else annotationView.canShowCallout = NO;
        NSString *iconConfig;
        if(annotation2.place.sub_category_id.length) iconConfig = annotation2.place.sub_category_id;
        else iconConfig = annotation2.place.category_id;
        [annotationView.imageView setImageWithUrlString:[NSString stringWithFormat:@"%@%@.png",[PlaceCategory iconUrlConfig],iconConfig]];
       
//        annotationView.iconLab.hidden = !annotation2.isHavePost;
        if(annotation2.imageUrl.length){
            AnnatationImagePopView *imageView = [[AnnatationImagePopView alloc] initWithFrame:CGRectMake(0, 0, popViewWidth, popViewWidth)];
            imageView.annotation = annotation2;
            imageView.delegate = self;
            [imageView.headImageView setAvatarImageWithUrlString:annotation2.headImageUrl];
            [imageView.imageView setImageWithUrlString:annotation2.imageUrl];
            annotationView.paopaoView  = [[BMKActionPaopaoView alloc] initWithCustomView:imageView];
        }else{
        AnnatationContentPopView *contentView = [[AnnatationContentPopView alloc] initWithFrame:CGRectMake(0, 0, [self contentWidth:annotation2.postContent], 30)];
            contentView.delegate = self;
            contentView.annotation = annotation2;
            [contentView.headImageView setAvatarImageWithUrlString:annotation2.headImageUrl];
        contentView.title = annotation2.postContent;
        annotationView.paopaoView  = [[BMKActionPaopaoView alloc] initWithCustomView:contentView];
    }
        return annotationView;
    }else if ([annotation isKindOfClass:[BMKPointAnnotation class]]){
         BMKAnnotationView *annotationView = [[BMKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiLocation"];
        annotationView.image = [UIImage imageNamed:@"creatAddressAn"];
        annotationView.canShowCallout = YES;
        return annotationView;
    }
    return nil;//
}
#pragma BJPostAnnotationViewDelegate
- (void)annotationViewTouch:(BJPostAnnotationView *)annotationView{
    PostBMKPointAnnotation *annotation = (PostBMKPointAnnotation *)annotationView.annotation;
    // CGPoint point = [self.mapView convertCoordinate:annotation.coordinate toPointToView:mapView];
    [self.mapView selectAnnotation:annotation animated:YES];
    [self.delegate selectAnnotation:annotation.place annotationPoint:annotationView.frame.origin];
}
#pragma mark - CityMapsSearchResultViewDelegate
- (void)footerRefresh:(void (^)(BOOL))success{
//    self.searchSuccess = success;
    [baiPoiArray removeAllObjects];
    [userPoiArray removeAllObjects];
    _citySearchOption.pageIndex +=1;
     dispatch_group_enter(group);
    [self.poisearch poiSearchInCity:_citySearchOption];
    /**
     *  自有poi 暂时不分页
     *
    
     */
//    dispatch_group_enter(group);
//     [[CityMapNetworkManage sharedInstance] searchUserPlaceWithKeywords:_citySearchOption.keyword complete:^(BOOL succeed, id obj) {
//         if(success)success(succeed);
//         [obj enumerateObjectsUsingBlock:^(id  _Nonnull objct, NSUInteger idx, BOOL * _Nonnull stop) {
//             MapSearchResult *result = [[MapSearchResult alloc] init];
//             [result setKeyValues:objct];
//             CLLocationCoordinate2D coordinate;
//             coordinate.latitude = [objct[@"latitude"] floatValue];
//             coordinate.longitude = [objct[@"longtitude"] floatValue];
//             result.pt = coordinate;
//             [userPoiArray addObject:result];
//         }];
//          dispatch_group_leave(group);
//     }];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
         if(success)success(YES);
        [allPoiArray addObjectsFromArray:[baiPoiArray copy]];
        if(_callBackBlock)_callBackBlock([allPoiArray copy]);
    });
}
- (void)selectResult:(MapSearchResult *)result{
    BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
    annotation.coordinate = result.pt;
    annotation.title = result.name;
    searchAnnotion = annotation;
    BMKCoordinateRegion region;
    BMKCoordinateSpan span;
    span.latitudeDelta = 0.4;
    span.longitudeDelta = 0.4;
    region.center = result.pt;
//    [self.mapView addAnnotation:searchAnnotion];
//    [self.mapView showAnnotations:@[searchAnnotion] animated:YES];
    [self.mapView setCenterCoordinate:result.pt animated:YES];
    
}
- (void)showView:(id)infor{
    BMKPointAnnotation *annotation = infor;
    [self.mapView selectAnnotation:annotation animated:YES];
}
#pragma CityMapHeadViewDelegate
- (void)searchWithSearchKey:(NSString *)serarchStr callBack:(SearchResultArray)results{
    [baiPoiArray removeAllObjects];
    [userPoiArray removeAllObjects];
    [allPoiArray removeAllObjects];
    _callBackBlock = results;
        self.citySearchOption.keyword = serarchStr;
    self.citySearchOption.pageIndex = 0;
    dispatch_group_enter(group);
    [self.poisearch poiSearchInCity:_citySearchOption];
    dispatch_group_enter(group);
    [[CityMapNetworkManage sharedInstance] searchUserPlaceWithKeywords:_citySearchOption.keyword complete:^(BOOL succeed, id obj) {
        [obj enumerateObjectsUsingBlock:^(id  _Nonnull objct, NSUInteger idx, BOOL * _Nonnull stop) {
           MapSearchResult *result = [[MapSearchResult alloc] init];
            [result setKeyValues:objct];
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = [objct[@"latitude"] floatValue];
            coordinate.longitude = [objct[@"longtitude"] floatValue];
            result.pt = coordinate;
            [userPoiArray addObject:result];
        }];
        dispatch_group_leave(group);
    }];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [allPoiArray addObjectsFromArray:baiPoiArray];
        [allPoiArray addObjectsFromArray:[userPoiArray copy]];
        if(results)results([allPoiArray copy]);
    });
   
}
- (void)selectCategory:(CategoryObject *)category{
    self.currentCategory = category;
    [self getData];
}

#pragma mark - AnnatationImagePopViewDelegate
- (void)selectAnnotationViewForBubble:(AnnatationImagePopView *)view{
    if([view.annotation isKindOfClass:[PostBMKPointAnnotation class]]){
        PostBMKPointAnnotation *annotation = (PostBMKPointAnnotation *)view.annotation;
        [self.delegate selectAnnotationToPostDetailWithParameter:annotation.place];
    }
}
#pragma mark - AnnatationContentPopViewDelegate
- (void)selectAnnotationViewForContentPopView:(AnnatationContentPopView *)view{
    if([view.annotation isKindOfClass:[PostBMKPointAnnotation class]]){
        PostBMKPointAnnotation *annotation = (PostBMKPointAnnotation *)view.annotation;
        [self.delegate selectAnnotationToPostDetailWithParameter:annotation.place];
    }
}
/**
 *  sort by distance
 *
 *  @param array array description
 *
 *  @return return value description
 */
- (NSArray *)sortArrayByDistance:(NSArray *)array{
    CLLocationCoordinate2D coordi = self.mapView.region.center;
    array =  [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        Place *poi1 = obj1;
        Place *poi2 = obj2;
        CLLocationCoordinate2D coordi1;
        coordi1.latitude = poi1.latitude;
        coordi1.longitude = poi1.longtitude;
        
        CLLocationCoordinate2D coordi2;
        coordi2.latitude = poi2.latitude;
        coordi2.longitude = poi2.longtitude;
      
        BMKMapPoint point = BMKMapPointForCoordinate(coordi);
        BMKMapPoint point1 = BMKMapPointForCoordinate(coordi1);
        BMKMapPoint point2 = BMKMapPointForCoordinate(coordi2);
        
        CLLocationDistance distance1 = BMKMetersBetweenMapPoints(point,point1);
        CLLocationDistance distance2 = BMKMetersBetweenMapPoints(point,point2);
        if(distance1>distance2)return NSOrderedDescending;
        else return NSOrderedAscending;
    }];
    return array;
}
- (CGFloat)contentWidth:(NSString *)content{
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:9] andSize:CGSizeMake(kDeviceWidth, 1000)];
    CGFloat width = 30;
    if(size.width>kDeviceWidth) width += kDeviceWidth-20;
    else if(size.width<40) width += 40;
    else width += size.width+10;
    if(width>200) return 200;
    return width;
}
@end
