//
//  MapHeadViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapHeadViewModel.h"
#import "CityMapNetworkManage.h"
#import "CategoryObject.h"
#import "PlaceCategory.h"
@implementation MapHeadViewModel{
    NSMutableArray *titlesArray;
}
- (instancetype)initWithView:(CityMapHeadView *)view{
    self = [super init];
    if(self){
        titlesArray = [[NSMutableArray alloc] init];
        [self getData:view];
    }
    return self;
}
- (void)getData:(CityMapHeadView *)titleView{
    [[CityMapNetworkManage sharedInstance] getPlaceCommendCategoryComplete:^(BOOL succeed, id obj) {
        NSArray *categorys;
        NSString *config;
        if(succeed){
        categorys = obj[@"categories"];
        config = obj[@"config"][@"prefix"];
        [PlaceCategory saveCategory:obj];
        }else{
         PlaceCategory *category = [PlaceCategory getCategory];
            NSDictionary *catDic;
            if(category)catDic = [NSJSONSerialization JSONObjectWithData:category.category options:NSJSONReadingAllowFragments error:nil];
            categorys = catDic[@"categories"];
             config = catDic[@"config"][@"prefix"];
        }
       
        [categorys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CategoryObject *category =  [CategoryObject objectWithKeyValues:obj];
            category.iconConfig = config;
            [titlesArray addObject:category];
            
        }];
        [titleView reloadData];
    }];
}
#pragma  mark- CityMapHeadViewDataSource
- (NSArray *)titlesForView{
    return [titlesArray copy];
}
@end
