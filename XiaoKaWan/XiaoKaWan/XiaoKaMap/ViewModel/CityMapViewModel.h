//
//  CityMapViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import "CityMapHeadView.h"
#import "CityMapsSearchResultView.h"
#import "BJNotificationConfig.h"
#import "CreatLoactionObj.h"
@class Place;
@protocol CityMapViewModelDelegate <NSObject>

- (void)selectAnnotation:(Place *)place annotationPoint:(CGPoint)point;
- (void)selectAnnotationToPostDetailWithParameter:(Place*)place;
@end
@interface CityMapViewModel : NSObject<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKPoiSearchDelegate,CityMapHeadViewDelegate,CityMapsSearchResultViewDelegate>
@property(nonatomic,strong)BMKPoiSearch* poisearch;
@property(nonatomic,assign) id<CityMapViewModelDelegate>delegate;
@property(nonatomic,strong) CreatLoactionObj *placeNewLocation;
@property(nonatomic,strong) BMKLocationService *locationService;
@property(nonatomic,copy) void(^dissMissPlacePopView)();
- (instancetype)initWithMap:(BMKMapView *)mapView;
- (void)userLocationAction;
- (void)viewWillAppear;
- (void)viewWillDisappear;
@end

