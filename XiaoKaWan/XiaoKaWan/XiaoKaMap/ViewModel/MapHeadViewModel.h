//
//  MapHeadViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityMapHeadView.h"
@interface MapHeadViewModel : NSObject<CityMapHeadViewDataSource>
- (instancetype)initWithView:(CityMapHeadView *)view;
@end
