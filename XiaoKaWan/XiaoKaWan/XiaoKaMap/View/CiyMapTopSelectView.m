//
//  CiyMapTopSelectView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CiyMapTopSelectView.h"
#import "UIColor+colorWithHex.h"
#import "NSString+Cagetory.h"

#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define topViewHeight 40
@implementation CiyMapTopSelectView
{
    UIScrollView *_topScrollView;
    UIView *bottomLine ;
    NSInteger selectTag;
    NSMutableArray *allItems;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        allItems =[[NSMutableArray alloc] initWithCapacity:0];
        _topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        _topScrollView.delegate = self;
        _topScrollView.backgroundColor = [UIColor clearColor];
        _topScrollView.pagingEnabled = YES;
        _topScrollView.scrollEnabled = YES;
        // _topScrollView.bounces = NO;
        _topScrollView.showsHorizontalScrollIndicator = NO;
        _topScrollView.showsVerticalScrollIndicator = NO;
//        _topScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:_topScrollView];
        
        UIImageView *imageLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_topScrollView.frame)+5, 0, 10, 50)];
        //imageLine.image = [UIImage imageNamed:@"bg_fengexian"];
        imageLine.backgroundColor = [UIColor p_colorWithHex:0xeac942];
        imageLine.alpha = 0.4;
        [self addSubview:imageLine];

        selectTag = 0;
    }
    return self;
}
- (void)setTitleArr:(NSArray *)titleArr
{
    _titleArr = [NSArray arrayWithArray:titleArr];
    [self createNameButtons];
}

- (void)createNameButtons
{
    [allItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    __weak CiyMapTopSelectView *weakSelf = self;
    [_titleArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CategoryObject *object = obj;
        CGFloat butonWidh = [weakSelf getCellHeight:object.title]+10;
        UIButton *titleButton = [[UIButton alloc]init];
        titleButton.tag = 70+idx;
        [titleButton setTitle:object.title forState:UIControlStateNormal];
        titleButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [titleButton setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [titleButton addTarget:self action:@selector(selectNameButton:) forControlEvents:UIControlEventTouchUpInside];
        [_topScrollView addSubview:titleButton];
        [allItems addObject:titleButton];
        if(idx>0){
            UIButton *beforeButton = (UIButton*)[_topScrollView viewWithTag:69+idx];
            titleButton.frame = CGRectMake(CGRectGetMaxX(beforeButton.frame)+20, 5, butonWidh, 30);
        }else{
            [titleButton setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
            titleButton.frame = CGRectMake(10, 5, butonWidh, 30);
            titleButton.transform = CGAffineTransformMakeScale(1.1, 1.1);
            bottomLine = [[UIView alloc]initWithFrame:CGRectMake(10, topViewHeight-3, CGRectGetWidth(titleButton.frame), 3)];
            bottomLine.backgroundColor = [UIColor p_colorWithHex:0x333333];
            [_topScrollView addSubview:bottomLine];
            [allItems addObject:bottomLine];
        }
//        
        if(idx == _titleArr.count-1)
        {
            [_topScrollView setContentSize:CGSizeMake(CGRectGetMaxX(titleButton.frame), 44)];
            
        }
        
    }];
    
}
- (void)startScroll:(UIButton*)button
{
    CGSize size = _topScrollView.contentSize;
    CGPoint offset = _topScrollView.contentOffset;
    offset.x+=kDeviceWidth;
    if(offset.x<size.width) {
        
        [_topScrollView setContentOffset:CGPointMake(offset.x-kDeviceWidth/2, offset.y) animated:YES];
    }
}
- (void)selectNameButton:(UIButton *)sender
{
    if(selectTag == sender.tag) return;
    selectTag = sender.tag;
    for(id button in _topScrollView.subviews){
        if([button isKindOfClass:[UIButton class]]){
            UIButton *buttonSelf = (UIButton*)button;
            [buttonSelf setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
            buttonSelf.transform = CGAffineTransformMakeScale(1, 1);
        }
    }
    [sender setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
    
    bottomLine.frame = CGRectMake(sender.frame.origin.x, topViewHeight-3, CGRectGetWidth(sender.frame), 3);
    
    [UIView animateWithDuration:0.2 animations:^{
        bottomLine.transform = CGAffineTransformMakeTranslation(CGRectGetWidth(sender.frame)/2, 0);
        bottomLine.transform = CGAffineTransformMakeScale(0.8, 0.8);
        sender.transform = CGAffineTransformMakeScale(0.9, 0.9);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            bottomLine.transform = CGAffineTransformMakeTranslation(0, 0);
            bottomLine.transform = CGAffineTransformMakeScale(1, 1);
            sender.transform = CGAffineTransformMakeScale(1.1, 1.1);
            
        }];
        
    }];
    [self adjustScrollViewContentX:bottomLine.frame];
    NSInteger index = (sender.tag-70);
    [self.delegate titleTouch:self.titleArr[index]];
}
- (void)setItemIndex:(NSInteger)itemIndex
{
    UIButton *selectButton = (UIButton*)[_topScrollView viewWithTag:70+itemIndex];
    if(selectTag == selectButton.tag) return;
    selectTag = selectButton.tag;
    for(id button in _topScrollView.subviews){
        if([button isKindOfClass:[UIButton class]]){
            UIButton *buttonSelf = (UIButton*)button;
            [buttonSelf setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
            buttonSelf.transform = CGAffineTransformMakeScale(1, 1);
        }
    }
    
    [selectButton setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
    bottomLine.frame = CGRectMake(selectButton.frame.origin.x, topViewHeight-3, CGRectGetWidth(selectButton.frame), 3);
    [self adjustScrollViewContentX:bottomLine.frame];
    [UIView animateWithDuration:0.2 animations:^{
        bottomLine.transform = CGAffineTransformMakeTranslation(CGRectGetWidth(selectButton.frame)/2, 0);
        bottomLine.transform = CGAffineTransformMakeScale(0.8, 0.8);
        selectButton.transform = CGAffineTransformMakeScale(0.9, 0.9);
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.2 animations:^{
            bottomLine.transform = CGAffineTransformMakeTranslation(0, 0);
            bottomLine.transform = CGAffineTransformMakeScale(1, 1);
            selectButton.transform = CGAffineTransformMakeScale(1.1, 1.1);
            
        }];
        
    }];
    
    //    [self.delegate titleTouch:itemIndex];
}
- (float)getCellHeight:(NSString*)string
{
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] andSize:CGSizeMake(kDeviceWidth-40, 9999)];
    return size.width;
}
- (void)adjustScrollViewContentX:(CGRect)rect
{
    CGSize size = _topScrollView.contentSize;
    if(rect.origin.x<150||rect.origin.x>size.width-150) {
        if(rect.origin.x<150) [_topScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        
        if(rect.origin.x>size.width-150)  [_topScrollView setContentOffset:CGPointMake(size.width-kDeviceWidth+60, 0) animated:YES];
        
    }
    else  [_topScrollView setContentOffset:CGPointMake(rect.origin.x-110, 0) animated:YES];
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
