//
//  BJMapPlacePopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/10/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJMapPlacePopView.h"
#import "Place.h"
#import "PlaceCategory.h"
#define buttonWidth 40
@interface BJMapPlacePopView()
@property(nonatomic,strong) BJUIImageView *imageView;
@property(nonatomic,strong) UILabel *titleLb;
@property(nonatomic,strong) UILabel *addressLb;
@property(nonatomic,strong) UILabel *categoryLb;
@property(nonatomic,strong) UILabel *describtionLb;
@property(nonatomic,strong) PlcaePopButton *buttonInf;
@property(nonatomic,strong) PlcaePopButton *buttonNav;
@property(nonatomic,strong)  Place *selectPlace;
@end
@implementation BJMapPlacePopView
- (instancetype)initWithFrame:(CGRect)frame{
   self =  [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(12);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.lessThanOrEqualTo(@(kDeviceWidth-110));
        make.height.equalTo(@18);
    }];
    [self.addressLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.lessThanOrEqualTo(@(kDeviceWidth-110));
       
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLb.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.equalTo(@13);
        make.height.equalTo(@13);
    }];
    [self.categoryLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLb.mas_bottom).offset(5);
        make.left.equalTo(self.imageView.mas_right).offset(10);
        
    }];
    [self.describtionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.lessThanOrEqualTo(@(kDeviceWidth-110));
    }];
    [self.buttonNav mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.width.equalTo(@buttonWidth);
        make.top.equalTo(@29);
        make.height.equalTo(@(buttonWidth +30));
    }];
    [self.buttonInf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.buttonNav.mas_left).offset(-20);
        make.width.equalTo(@buttonWidth);
        make.top.equalTo(@29);
        make.height.equalTo(@(buttonWidth +30));
    }];
}
- (BJUIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[BJUIImageView alloc] init];
        [self addSubview:_imageView];
    }
    return _imageView;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:20 color:0xffffff];
        [self addSubview:_titleLb];
    }
    return _titleLb;
}
- (UILabel *)describtionLb{
    if(_describtionLb == nil){
        _describtionLb = [UILabel creatLabelWithFont:11 color:0xffffff];
        [self addSubview:_describtionLb];
    }
    return _describtionLb;
}
- (UILabel *)addressLb{
    if(_addressLb == nil){
        _addressLb = [UILabel creatLabelWithFont:11 color:0xffffff];
        _addressLb.userInteractionEnabled = YES;
        [self addSubview:_addressLb];
    }
    return _addressLb;
}
- (UILabel *)categoryLb{
    if(_categoryLb == nil){
        _categoryLb = [UILabel creatLabelWithFont:11 color:0xffffff];
//        _categoryLb.font = [UIFont fontWithName:@"iconfont" size:20];
        _categoryLb.text = @"\U0000e62a";
        [self addSubview:_categoryLb];
    }
    return _categoryLb;
}
- (PlcaePopButton *)buttonInf{
    if(_buttonInf == nil){
        _buttonInf = [[PlcaePopButton alloc] init];
        _buttonInf.titleLb.text = @"详情";
        [_buttonInf.button setTitle:@"\U0000e634" forState:0];
        __weak typeof(self) weakSelf = self;
        _buttonInf.buttonAction = ^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate placeDetail:strongSelf.selectPlace];
        };
        [self addSubview:_buttonInf];
    }
    return _buttonInf;
}
- (PlcaePopButton *)buttonNav{
    if(_buttonNav == nil){
        _buttonNav = [[PlcaePopButton alloc] init];
        _buttonNav.titleLb.text = @"导航";
        [_buttonNav.button setTitle:@"\U0000e633" forState:0];
        __weak typeof(self) weakSelf = self;
        _buttonNav.buttonAction = ^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate placeNavigationWithPlace:strongSelf.selectPlace];
        };
        [self addSubview:_buttonNav];
    }
    return _buttonNav;
}
- (void)showPopViewWithPlace:(Place *)place{
    self.selectPlace = place;
    self.titleLb.text = place.name;
    self.addressLb.text = place.addressDetail;
    NSString *iconConfig;
    if(place.sub_category_id.length) iconConfig = place.sub_category_id;
    else iconConfig = place.category_id;
    [self.imageView setImageWithUrlString:[NSString stringWithFormat:@"%@%@.png",[PlaceCategory iconUrlConfig],iconConfig]];
    self.categoryLb.text = place.category_name;
    self.describtionLb.text = place.content;
    [UIView animateWithDuration:.3 animations:^{
        self.transform = CGAffineTransformMakeTranslation(0, -popViewHeight-44);
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    }];
   
}
- (void)disMissView{
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformIdentity;
    }];
    
}
@end

@implementation PlcaePopButton
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@buttonWidth);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.button.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:11 color:0xffffff];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLb];
    }
    return _titleLb;
}
- (UIButton *)button{
    if(_button == nil){
        _button = [[UIButton alloc] init];
        [_button setBackgroundColor:[UIColor whiteColor]];
        _button.layer.masksToBounds = YES;
        _button.layer.cornerRadius = 7;
        _button.titleLabel.font = [UIFont fontWithName:@"iconfont" size:25];
        [_button setTitleColor:[UIColor mainColor] forState:0];
        [_button addTarget:self action:@selector(buttonActin:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_button];
    }
    return _button;
}
- (void)buttonActin:(UIButton *)button{
    if(self.buttonAction)self.buttonAction();
}
@end
