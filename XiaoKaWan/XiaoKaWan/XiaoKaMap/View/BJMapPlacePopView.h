//
//  BJMapPlacePopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/10/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#define popViewHeight 100
@class Place;
@protocol BJMapPlacePopViewDelegate <NSObject>

- (void)placeDetail:(nullable Place *)place;
- (void)placeNavigationWithPlace:(nullable Place *)place;

@end
@interface BJMapPlacePopView : BJView
@property(nullable,nonatomic,assign)id<BJMapPlacePopViewDelegate>delegate;
- (void)showPopViewWithPlace:(nullable Place *)place;
- (void)disMissView;
@end


@interface PlcaePopButton : BJView
@property(nullable,nonatomic,strong) UIButton *button;
@property(nullable,nonatomic,strong)UILabel *titleLb;
@property(nonnull,copy) void(^buttonAction)();
@end
