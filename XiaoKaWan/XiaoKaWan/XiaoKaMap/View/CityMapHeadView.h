//
//  CityMapHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityMapSelectSegmentView.h"
#import "CiyMapTopSelectView.h"
typedef void (^SearchResultArray)(NSArray *dataArray);
@protocol CityMapHeadViewDelegate <NSObject>

- (void)searchWithSearchKey:(NSString *)serarchStr callBack:(SearchResultArray)results;
- (void)selectCategory:(CategoryObject*)category;
@end
@protocol CityMapSearchDataDelegate <NSObject>

- (void)searchResult:(NSArray *)array;

@end
@protocol CityMapHeadViewDataSource <NSObject>

- (NSArray *)titlesForView;

@end
@interface CityMapHeadView : UIView<UISearchBarDelegate,UISearchControllerDelegate,CiyMapTopSelectViewDelegae>
@property(nonatomic,strong)UISearchBar *searchBar;
@property(nonatomic,strong)CiyMapTopSelectView *selectView;
@property(nonatomic,strong)UISearchController *searchController;
@property(nonatomic,assign)id<CityMapHeadViewDelegate>delegate;
@property(nonatomic,assign)id<CityMapSearchDataDelegate>searchDataDelegate;
@property(nonatomic,assign)id<CityMapHeadViewDataSource>dataSource;
@property(nonatomic,copy) void(^startSearch)();
@property(nonatomic,copy) void(^endSearch)();
- (void)reloadData;
@end

