//
//  AddPostPopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/11/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddPostPopViewDelegate <NSObject>

- (void)selectAtIndex:(NSInteger)index;

@end
@interface AddPostPopView : UIView
@property(nonatomic,strong)UIButton *addPostButton;
@property(nonatomic,strong) id<AddPostPopViewDelegate>delegate;
- (void)showInSuperView:(UIView *)view buttonPoint:(CGPoint)origin buttonInView:(UIView *)view1;
- (void)disMiss;
- (void)addItems:(NSArray *)array;
@end

