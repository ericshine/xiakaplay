//
//  AnnatationContentPopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
@class PostBMKPointAnnotation;
@class AnnatationContentPopView;
@protocol AnnatationContentPopViewDelegate <NSObject>

- (void)selectAnnotationViewForContentPopView:(AnnatationContentPopView *)view;

@end

@interface AnnatationContentPopView : BJView
@property(nonatomic,strong)id<AnnatationContentPopViewDelegate>delegate;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,strong)UILabel *contentLb;
@property(nonatomic,copy) NSString *title;
@property(nonatomic,strong)PostBMKPointAnnotation *annotation;
@end
