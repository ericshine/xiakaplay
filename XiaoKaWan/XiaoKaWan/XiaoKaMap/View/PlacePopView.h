//
//  PlacePopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#import "Place.h"
@protocol PlacePopViewDelegate <NSObject>

- (void)toPlaceDetail:(Place *)place;
- (void)toMapNavigation:(Place *)place ;

@end
/**
 *
 */
__deprecated_msg("地图首页弹窗  暂时废弃")
@interface PlacePopView : BJView  
@property(nonatomic,strong)Place *place;
@property(nonatomic,assign) id<PlacePopViewDelegate>delegae;
- (void)showInView:(UIView*)view fromPoint:(CGPoint)point;
- (void)disMiss;
@end

