//
//  CityMapsSearchResultView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityMapHeadView.h"
#import "MapSearchResult.h"
@protocol CityMapsSearchResultViewDelegate <NSObject>

- (void)selectResult:(MapSearchResult *)result;
- (void)footerRefresh:(void(^)(BOOL success))success;
@end
@interface CityMapsSearchResultView : UIView<UITableViewDelegate,UITableViewDataSource,CityMapSearchDataDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSArray *dataList;
@property(nonatomic,strong) id<CityMapsSearchResultViewDelegate>delegate;
@property(nonatomic,copy) void(^creatAddress)();
- (void)showAtView:(UIView*)superView;
- (void)disMiss;
@end
