//
//  AnnatationContentPopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AnnatationContentPopView.h"
#define headWidth 14.5
@interface AnnatationContentPopView()
@property(nonatomic,strong)UIImageView *bgImageView;
@property(nonatomic,strong)UIButton *bgButton;
@end
@implementation AnnatationContentPopView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    self.contentLb.text = title;
    self.contentLb.layer.zPosition = 1;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.equalTo(self);
    }];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(5);
        make.width.equalTo(@headWidth);
        make.height.equalTo(@headWidth);
    }];
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.headImageView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    [self.bgButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self);
    }];
}
- (UIImageView *)bgImageView{
    if(_bgImageView == nil){
        _bgImageView = [[UIImageView alloc] init];
        _bgImageView.image = [[UIImage imageNamed:@"mapPopView"] resizableImageWithCapInsets:UIEdgeInsetsMake(10,10,50,10)resizingMode:UIImageResizingModeStretch];
        [self addSubview:_bgImageView];
    }
    return _bgImageView;
}
- (BJUIImageView *)headImageView{
    if(_headImageView == nil){
        _headImageView = [[BJUIImageView alloc] init];
        _headImageView.layer.cornerRadius =headWidth/2;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.layer.zPosition = 1;
        _headImageView.image = [UIImage imageNamed:@"defaultHeadImage"];
        [self addSubview:_headImageView];
    }
    return _headImageView;
}
- (UILabel *)contentLb{
    if(_contentLb == nil){
        _contentLb = [UILabel creatLabelWithFont:9 color:0x333333];
        [self addSubview:_contentLb];
    }
    return _contentLb;
}
- (UIButton *)bgButton{
    if(_bgButton == nil){
        _bgButton = [[UIButton alloc] init];
        [_bgButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_bgButton];
    }
    return _bgButton;
}
- (void)buttonAction:(UIButton*)button{
    [self.delegate selectAnnotationViewForContentPopView:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
