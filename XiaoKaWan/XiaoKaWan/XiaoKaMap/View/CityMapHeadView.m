//
//  CityMapHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapHeadView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#import "CategoryObject.h"
#import "UIImage+Category.h"
#import "BJNotificationConfig.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface CityMapHeadView ()
@property(nonatomic,strong)CityMapSelectSegmentView *segmentView;
@end
@implementation CityMapHeadView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor p_colorWithHex:0xffda44];
        [self setNeedsUpdateConstraints];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hiddenKeyboard) name:BJHiddenKeyboardNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollViewDidScroll) name:BJscrollViewDidScrollNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItems) name:BJDidSelectTableViewNotification object:nil];
    }
    return self;
}
- (void)didSelectItems{
    _searchBar.showsCancelButton = NO;
    [self.searchBar resignFirstResponder];
}
- (void)scrollViewDidScroll{
      [self.searchBar resignFirstResponder];
}
- (void)hiddenKeyboard{
    [self.searchBar resignFirstResponder];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)reloadData{
     self.selectView.titleArr =  [self.dataSource titlesForView];
    if(self.selectView.titleArr.count>0){
        CategoryObject *category = self.selectView.titleArr[0];
        if(category.subTtiles){
            [self.delegate selectCategory:category.subTtiles[0]];
        }else{
            [self.delegate selectCategory:category];
        }
    }
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(27);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@28);
    }];
    [self.selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBar.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (CityMapSelectSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView =[[CityMapSelectSegmentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.frame), kDeviceWidth, 32) andItmes:nil];
        __weak typeof(self) weakSelf = self;
        _segmentView.selectItem = ^(CategoryObject *category){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.searchBar resignFirstResponder];
            [strongSelf.delegate selectCategory:category];
        };
    }
    return _segmentView;
}
- (CiyMapTopSelectView *)selectView{
    if(_selectView == nil){
        _selectView = [[CiyMapTopSelectView alloc] init];
        _selectView.delegate = self;
        _selectView.itemIndex = 0;
        [self addSubview:_selectView];
    }
    return _selectView;
}
- (UISearchBar *)searchBar{
    if(_searchBar == nil){
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.layer.cornerRadius = 5;
        _searchBar.layer.masksToBounds = YES;
        _searchBar.placeholder = @"带你去寻找最流行的地方";
        _searchBar.tintColor = [UIColor blackColor];
        _searchBar.backgroundImage = [UIImage imageFromColor:[UIColor p_colorWithHex:0xffda44]];
        _searchBar.returnKeyType = UIReturnKeySearch;
        [self addSubview:_searchBar];
    }
    return _searchBar;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    _searchBar.showsCancelButton = NO;
    _searchBar.text = nil;
    if(self.endSearch)self.endSearch();
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    _searchBar.showsCancelButton = YES;
    if(self.startSearch)self.startSearch();
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    _searchBar.showsCancelButton = NO;
    if(self.startSearch)self.startSearch();
    [self.delegate searchWithSearchKey:searchBar.text callBack:^(NSArray *dataArray) {
        [self.searchDataDelegate searchResult:dataArray];
    }];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _searchBar.showsCancelButton = YES;
    if(searchText.length<=0){
     if(self.endSearch)self.endSearch();
    }else{
//        if(self.startSearch)self.startSearch();
//    [self.delegate searchWithSearchKey:searchText callBack:^(NSArray *dataArray) {
//        [self.searchDataDelegate searchResult:dataArray];
//    }];
    }
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{

}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar resignFirstResponder];
}
#pragma mark - CiyMapTopSelectViewDelegae
- (void)titleTouch:(CategoryObject *)category{
    [self.searchBar resignFirstResponder];
    if(category.subTtiles){
        [self.superview addSubview:self.segmentView];
        self.segmentView.titles = category.subTtiles;
        [self.delegate selectCategory:category.subTtiles[0]];
    }else{
        [self.segmentView removeFromSuperview];
        [self.delegate selectCategory:category];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
