//
//  BJPostAnnotationView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJPostAnnotationView.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@implementation BJPostAnnotationView{
    UIView *bgview;
}
- (instancetype)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if(self){
        bgview = [[UIView alloc] init];
        [self addSubview:bgview];
        bgview.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [bgview addGestureRecognizer:tap];

        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
//    [self.iconLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self.mas_top).offset(10);
//        make.right.equalTo(self.mas_right);
//    }];
    [bgview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self);
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-5);
        make.height.equalTo(@28);
        make.width.equalTo(@28);
//        make.left.equalTo(self.mas_left).offset(10);
//        make.right.equalTo(self.mas_right).offset(-10);
//        make.top.equalTo(self.mas_top).offset(10);
//        make.bottom.equalTo(self.mas_bottom).offset(-10);
    }];
}
- (UILabel *)iconLab{
    if(_iconLab ==nil){
        _iconLab = [UILabel creatLabelWithFont:14 color:0x4B96EF];
        _iconLab.font = [UIFont fontWithName:@"iconfont" size:15];
        _iconLab.text = @"\U0000e62f";
        _iconLab.layer.zPosition = 1;
//        [self addSubview:_iconLab];
    }
    return _iconLab;
}
- (BJUIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[BJUIImageView alloc] init];
//        _imageView.image = [UIImage imageNamed:@"qqLogo"];
        [self addSubview:_imageView];
    }
    return _imageView;
}
- (void)tapGesture:(UITapGestureRecognizer *)gesture{
    [self.delegate annotationViewTouch:self];
}
- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if(selected)_iconLab.hidden = YES;
    else _iconLab.hidden = NO;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
