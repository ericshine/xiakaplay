//
//  BJPostAnnotationView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import "BJUIImageView.h"
@class BJPostAnnotationView;
@protocol BJPostAnnotationViewDelegate <NSObject>

- (void)annotationViewTouch:(BJPostAnnotationView *)annotationView;

@end
@interface BJPostAnnotationView : BMKAnnotationView
@property(nonatomic,strong)UILabel *iconLab;
@property(nonatomic,strong)BJUIImageView*imageView;
@property(nonatomic,assign) id<BJPostAnnotationViewDelegate>delegate;
@end

