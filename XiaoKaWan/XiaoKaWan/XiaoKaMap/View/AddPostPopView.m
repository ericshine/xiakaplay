//
//  AddPostPopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/11/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPostPopView.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define itemWidth 75
#define radius 130
@implementation AddPostPopView{
    UIView *bgView;
    NSMutableArray *buttons;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        bgView =[[UIView alloc]initWithFrame:frame];
        
        [self addSubview:bgView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMiss)];
        [bgView addGestureRecognizer:tap];
        buttons = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)addItems:(NSArray *)array{
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [[UIButton alloc] init];
        [button setBackgroundImage:[UIImage imageNamed:obj] forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, itemWidth, itemWidth);
        button.tag = 100+idx;
        [self addSubview:button];
        [button addTarget:self action:@selector(selelctItem:) forControlEvents:UIControlEventTouchUpInside];
        [buttons addObject:button];
    }];
}
- (void)showInSuperView:(UIView *)view buttonPoint:(CGPoint)origin buttonInView:(UIView *)view1{
    [view addSubview:self];
    CGPoint point = [self convertPoint:origin fromView:view1];//
    self.addPostButton.frame = CGRectMake(point.x, point.y, 50, 50);
    [buttons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = obj;
        button.frame = CGRectMake(point.x, point.y, 0, 0);
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        button.frame = CGRectMake(0, 0, itemWidth, itemWidth);
        CGFloat radian = M_PI_4*(idx+1/buttons.count);
        CGFloat y = radius*sin(radian);
        CGFloat x = radius*cos(radian);
        CGPoint center = CGPointMake(kDeviceWidth-x-50, kDeviceHeight-94-y);
        button.center =center;
    } completion:^(BOOL finished) {
        
    }];
    }];
}
- (UIButton *)addPostButton{
    if(_addPostButton == nil){
        _addPostButton = [[UIButton alloc] init];
        [_addPostButton setBackgroundImage:[UIImage imageNamed:@"post_button"] forState:UIControlStateNormal];
        [self addSubview:_addPostButton];
        [_addPostButton addTarget:self action:@selector(disMiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addPostButton;
}
- (void)selelctItem:(UIButton*)button{
    NSInteger tag = button.tag - 100;
    [self.delegate selectAtIndex:tag];
}
- (void)disMiss{
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
