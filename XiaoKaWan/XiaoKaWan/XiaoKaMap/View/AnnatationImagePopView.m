//
//  AnnatationImagePopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AnnatationImagePopView.h"
#define headWidth 24
@interface AnnatationImagePopView()
@property(nonatomic,strong)UIImageView *bgImageView;
@property(nonatomic,strong)UIButton *bgButton;
@end
@implementation AnnatationImagePopView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self updateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.equalTo(self);
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.mas_centerY).offset(-3);
//        make.centerX.equalTo(self.mas_centerX);
//        make.height.equalTo(@(popViewWidth-20));
//        make.width.equalTo(@(popViewWidth-20));
        make.top.equalTo(self.mas_top).offset(8);
        make.left.equalTo(self.mas_left).offset(8);
        make.bottom.equalTo(self.mas_bottom).offset(-12);
        make.right.equalTo(self.mas_right).offset(-8);
    }];
    
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.height.equalTo(@headWidth);
        make.width.equalTo(@headWidth);
    }];
    [self.bgButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self);
    }];
}
- (UIButton *)bgButton{
    if(_bgButton == nil){
        _bgButton = [[UIButton alloc] init];
        [_bgButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_bgButton];
    }
    return _bgButton;
}
- (void)buttonAction:(UIButton*)button{
    [self.delegate selectAnnotationViewForBubble:self];
}
- (UIImageView *)bgImageView{
    if(_bgImageView == nil){
        _bgImageView = [[UIImageView alloc] init];
        _bgImageView.image = [[UIImage imageNamed:@"mapPopView"] resizableImageWithCapInsets:UIEdgeInsetsMake(10,10,50,10)resizingMode:UIImageResizingModeStretch];
        [self addSubview:_bgImageView];
    }
    return _bgImageView;
}
- (BJUIImageView *)headImageView{
    if(_headImageView == nil){
        _headImageView = [[BJUIImageView alloc] init];
        _headImageView.image = [UIImage imageNamed:@"defaultHeadImage"];
        _headImageView.layer.zPosition = 1;
        _headImageView.layer.cornerRadius = headWidth/2;
        _headImageView.layer.masksToBounds = YES;
        [self addSubview:_headImageView];
    }
    return _headImageView;
}
- (BJUIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[BJUIImageView alloc] init];
        [self addSubview:_imageView];
    }
    return _imageView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
