//
//  CityMapSelectSegmentView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapSelectSegmentView.h"
#import "UIImage+Category.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define itemsWidth 47
#define itemsDistance 23
@implementation CityMapSelectSegmentView{
    NSMutableArray *itemsArry;
}
- (instancetype)initWithFrame:(CGRect)frame andItmes:(NSArray *)itmes{
    self = [super initWithFrame:frame];
    if(self){
        itemsArry = [[NSMutableArray alloc] initWithCapacity:0];
        self.backgroundColor = [UIColor p_colorWithHex:0xffda44];
//        [self creatItems:itmes];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setTitles:(NSArray *)titles{
    _titles = titles;
    [self creatItems:titles];
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.height.equalTo(@1);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
}
- (void)creatItems:(NSArray *)items{
    [itemsArry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    CGFloat leftSpace = (kDeviceWidth - itemsWidth*items.count - itemsDistance*(items.count-1))/2.0;
    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CategoryObject *category = obj;
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(leftSpace+idx*(itemsWidth + itemsDistance), 6, itemsWidth, 20)];
        if(idx == 0) button.selected = YES;
        button.layer.cornerRadius = 8;
        button.layer.masksToBounds = YES;
        button.titleLabel.textColor = [UIColor p_colorWithHex:0x333333];
        button.titleLabel.font = [UIFont systemFontOfSize:12];
        [button setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xffcd00]] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xfed21f]] forState:UIControlStateHighlighted];
         [button setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xfed21f]] forState:UIControlStateNormal];
        button.tag = 40+idx;
        [self addSubview:button];
        [button setTitle:category.title forState:UIControlStateNormal];
        [button addTarget:self action:@selector(selectItems:) forControlEvents:UIControlEventTouchUpInside];
        [itemsArry addObject:button];
    }];
}
- (UIView*)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor =[UIColor p_colorWithHex:0xeac942];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (void)selectItems:(UIButton *)button{
    [itemsArry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = obj;
        button.selected = NO;
    }];
    button.selected = YES;
    CategoryObject *category = self.titles[button.tag-40];
    if(self.selectItem)self.selectItem(category);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
