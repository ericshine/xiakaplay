//
//  CityMapSelectSegmentView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryObject.h"

@interface CityMapSelectSegmentView : UIView
@property(nonatomic,strong) UIView *lineView;
@property(nonatomic,strong) NSArray *titles;
@property(nonnull,copy)void(^selectItem)(CategoryObject *category);
- (instancetype)initWithFrame:(CGRect)frame andItmes:(NSArray *)itmes;
@end
