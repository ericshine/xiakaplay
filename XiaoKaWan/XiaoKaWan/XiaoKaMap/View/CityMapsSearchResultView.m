//
//  CityMapsSearchResultView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapsSearchResultView.h"
#import "NoResultCreatAddressView.h"
#import <Masonry.h>
#import "BJTableViewCell.h"
#import "BJRefreshGitFooter.h"
@interface CityMapsSearchResultView()
@property(nonatomic,strong) NoResultCreatAddressView *noResultView;
@end;
@implementation CityMapsSearchResultView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (NoResultCreatAddressView *)noResultView{
    if(_noResultView == nil){
        _noResultView = [[NoResultCreatAddressView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, _tableView.frame.size.height)];
        _noResultView.titleLb.text = @"没有找到此地点";
        __weak typeof(self)weakSelf = self;
        _noResultView.creatAddress = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf disMiss];
            if(strongSelf.creatAddress)strongSelf.creatAddress();
        };
    }
    return _noResultView;
}
- (UITableView *)tableView{
    if(_tableView == nil){
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_tableView];
       BJRefreshGitFooter *footer =  [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
            [self.delegate footerRefresh:^(BOOL success) {
                [_tableView.footer endRefreshing];
            }];
        }];
        _tableView.footer = footer;
    }
    return _tableView;
}
- (void)showAtView:(UIView *)superView{
    [superView addSubview:self];
}
- (void)disMiss{
//    _dataList = nil;
//    [self.tableView reloadData];
    [self removeFromSuperview];
}
#pragma mark - CityMapSearchDataDelegate
- (void)searchResult:(NSArray *)array{
    self.dataList = array;
    if(array.count>0){
        self.tableView.tableHeaderView = nil;
        }
    else self.tableView.tableHeaderView = self.noResultView;
    [self.tableView reloadData];
}
#pragma mark - delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(_dataList.count)return 1;
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"searchResult";
    MapSearchResult *result= [self.dataList objectAtIndex:indexPath.row];
    BJTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell){
        cell = [[BJTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell showLineWithLeft:0 rightOffset:0];
    }
    cell.textLabel.text= result.name;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MapSearchResult *result= [self.dataList objectAtIndex:indexPath.row];
    [self.delegate selectResult:result];
    [self disMiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:BJDidSelectTableViewNotification object:nil];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[NSNotificationCenter defaultCenter] postNotificationName:BJscrollViewDidScrollNotification object:nil];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
