//
//  AnnatationImagePopView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#import "BJUIImageView.h"
#define popViewWidth 96
@class PostBMKPointAnnotation;
@class AnnatationImagePopView;
@protocol AnnatationImagePopViewDelegate <NSObject>

- (void)selectAnnotationViewForBubble:(AnnatationImagePopView *)view;

@end
@interface AnnatationImagePopView : BJView
@property(nonatomic,strong)id<AnnatationImagePopViewDelegate>delegate;
@property(nonatomic,strong)PostBMKPointAnnotation *annotation;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,strong)BJUIImageView *imageView;
@end

