//
//  PlacePopView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePopView.h"
#define imageWidth 20
#define backVieHeight 200
#define addressFont 12
#define titleFont 15
#import "BJUIImageView.h"
#import "PlaceCategory.h"
@interface PlacePopView()
@property(nonatomic,strong) UIView *lineView;
@property(nonatomic,strong) BJUIImageView *imageView;
@property(nonatomic,strong) UILabel *titleLb;
@property(nonatomic,strong) UILabel *addressLb;
@property(nonatomic,strong) UILabel *iconLb;
@property(nonatomic,strong) UIButton *button;
@end
@implementation PlacePopView{
    UIView *bgView;
    UIView *whiteView;
   
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        bgView =[[UIView alloc]initWithFrame:frame];
        [self addSubview:bgView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMiss)];
        [bgView addGestureRecognizer:tap];
        
        whiteView = [[UIView alloc] init];
        whiteView.backgroundColor = [UIColor whiteColor];
        whiteView.layer.cornerRadius = 10;
        whiteView.layer.masksToBounds = YES;
        [self addSubview:whiteView];

    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    self.titleLb.text = place.name;
    self.addressLb.text = place.address;
    NSString *iconConfig;
    if(place.sub_category_id.length) iconConfig = place.sub_category_id;
    else iconConfig = place.category_id;
    [self.imageView setImageWithUrlString:[NSString stringWithFormat:@"%@%@.png",[PlaceCategory iconUrlConfig],iconConfig]];
    [self setNeedsUpdateConstraints];
}
- (void)updateConstraints{
    [super updateConstraints];
    CGFloat width = kDeviceWidth-80;
    CGSize size1 = [_place.name sizeWithFont:[UIFont systemFontOfSize:titleFont] andSize:CGSizeMake(kDeviceWidth, 1000)];
    CGFloat titleWidth;
    if(size1.width> kDeviceWidth-120) titleWidth =  kDeviceWidth-120;
    else titleWidth = size1.width;
    CGFloat leftSpace1 = (width-20 - titleWidth)/2;
    
    CGSize size2 = [_place.address sizeWithFont:[UIFont systemFontOfSize:addressFont] andSize:CGSizeMake(kDeviceWidth, 1000)];
    CGFloat addressWidth;
    if(size2.width> kDeviceWidth-120) addressWidth =  kDeviceWidth-120;
     else addressWidth = size2.width;
    CGFloat leftSpace2 = (width-20 - addressWidth)/2;
    
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteView.mas_top).with.offset(20);
        make.left.equalTo(whiteView.mas_left).with.offset(leftSpace1);
        make.width.equalTo(@imageWidth);
        make.height.equalTo(@imageWidth);
    }];
   
    [self.titleLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteView.mas_top).offset(20);
        make.left.equalTo(self.imageView.mas_right);
    }];

    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(15);
        make.left.equalTo(whiteView.mas_left);
        make.right.equalTo(whiteView.mas_right);
        make.height.equalTo(@0.5);
    }];
    [self.addressLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(20);
        make.left.equalTo(whiteView.mas_left).offset(leftSpace2);
       make.width.lessThanOrEqualTo(@(width-60));
    }];
    [self.iconLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(15);
        make.left.equalTo(self.addressLb.mas_right).offset(10);
    }];
    [self.button mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLb.mas_bottom).offset(30);
        make.centerX.equalTo(whiteView.mas_centerX);
        make.height.equalTo(@30);
        make.width.equalTo(@180);
    }];
}
- (BJUIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[BJUIImageView alloc] init];
        [whiteView addSubview:_imageView];
    }
    return _imageView;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:titleFont color:0x434343];
        [whiteView addSubview:_titleLb];
    }
    return _titleLb;
}
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xD8D8D8];
        [whiteView addSubview:_lineView];
    }
    return _lineView;
}
- (UILabel *)addressLb{
    if(_addressLb == nil){
        _addressLb = [UILabel creatLabelWithFont:addressFont color:0xB5B5B5];
        [whiteView addSubview:_addressLb];
        _addressLb.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigation:)];
        [_addressLb addGestureRecognizer:tap];
    }
    return _addressLb;
}
- (UILabel *)iconLb{
    if(_iconLb == nil){
        _iconLb = [UILabel creatLabelWithFont:20 color:0xB5B5B5];
        _iconLb.font = [UIFont fontWithName:@"iconfont" size:20];
        _iconLb.text = @"\U0000e62a";
        [whiteView addSubview:_iconLb];
        _iconLb.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigation:)];
        [_iconLb addGestureRecognizer:tap];
    }
    return _iconLb;
}
- (UIButton *)button{
    if(_button == nil){
        _button = [[UIButton alloc] init];
        [_button setBackgroundColor:[UIColor mainColor]];
        [_button setTitle:@"地点主页" forState:0];
        _button.titleLabel.font = [UIFont systemFontOfSize:18];
        [_button setTitleColor:[UIColor p_colorWithHex:0x434343] forState:0];
        _button.layer.cornerRadius = 10;
        _button.layer.masksToBounds = YES;
        [_button addTarget:self action:@selector(toPlaceDetail:) forControlEvents:UIControlEventTouchUpInside];
        [whiteView addSubview:_button];
    }
    return _button;
}
- (void)toPlaceDetail:(UIButton *)button{
    [self disMiss];
    [self.delegae toPlaceDetail:_place];
}
- (void)navigation:(UITapGestureRecognizer *)gesture{
    [self disMiss];
    [self.delegae toMapNavigation:_place];
}
- (void)showInView:(UIView *)view fromPoint:(CGPoint)point{
    [view addSubview:self];
    whiteView.frame = CGRectMake(point.x, point.y, 40, 40);
    [UIView animateWithDuration:0.3 animations:^{
        bgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
        whiteView.frame = CGRectMake(40, 140, kDeviceWidth-80, 170);
    }];
    
}
- (void)disMiss{
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
