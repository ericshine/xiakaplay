//
//  CiyMapTopSelectView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryObject.h"
@protocol CiyMapTopSelectViewDelegae <NSObject>
- (void)titleTouch:(CategoryObject*)category;
@end
@interface CiyMapTopSelectView : UIView<UIScrollViewDelegate>
@property(nonatomic,strong)  NSArray *titleArr;
@property(nonatomic,assign) id<CiyMapTopSelectViewDelegae>delegate;
@property(nonatomic) NSInteger itemIndex;
@end
