//
//  ThirdpartyNavigation.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ThirdpartyNavigation.h"

@implementation ThirdpartyNavigation
SINGLETON_M(ThirdpartyNavigation)
- (UIAlertController *)navigationAlertControllerFromPlace:(CLLocationCoordinate2D)coordinate toPlace:(Place *)place{
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:nil message:@"地图导航" preferredStyle:UIAlertControllerStyleActionSheet];
    CLLocationCoordinate2D fromCoordinate = coordinate;
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"baidumap://map/"]]){
        [alertVc addAction:[UIAlertAction actionWithTitle:@"百度地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *url2 = [[NSString stringWithFormat:@"baidumap://map/direction?origin=%f,%f&destination=%f,%f&mode=driving",fromCoordinate.latitude,fromCoordinate.longitude,place.latitude,place.longtitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] ;
            
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:url2]];
        }]];
    }
    [alertVc addAction:[UIAlertAction actionWithTitle:@"苹果地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        CLLocationCoordinate2D coords1 = fromCoordinate;
        CLLocationCoordinate2D coords2 = CLLocationCoordinate2DMake(place.latitude,place.longtitude);
        MKMapItem *currentLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coords1 addressDictionary:nil]];
        currentLocation.name = @"当前";
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coords2 addressDictionary:nil]];
        toLocation.name = place.addressDetail;
        NSArray *items = [NSArray arrayWithObjects:toLocation, nil];
        
        NSDictionary *options = @{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES };
        [MKMapItem openMapsWithItems:items launchOptions:options];
        
    }]];
    
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"iosamap://navi"]]){
        [alertVc addAction:[UIAlertAction actionWithTitle:@"高德地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             NSString *gaodeUrl = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&poiname=%@&lat=%f&lon=%f&dev=2&style=1",@"xiakawan", @"com.xiaokaplay", @"终点", place.latitude, place.longtitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:gaodeUrl]];
        }]];
    }
    
    //
   
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"qqmap://map/"]]){
        [alertVc addAction:[UIAlertAction actionWithTitle:@"腾讯地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *url2 = [[NSString stringWithFormat:@"qqmap://map/routeplan?type=drive&fromcoord=%f,%f&tocoord=%f,%f&policy=1",fromCoordinate.latitude,fromCoordinate.longitude,place.latitude,place.longtitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] ;
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:url2]];
        }]];
    }
    [alertVc addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    return alertVc;
}
@end
