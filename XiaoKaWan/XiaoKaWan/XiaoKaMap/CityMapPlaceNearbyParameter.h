//
//  CityMapPlaceNearbyParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityMapPlaceNearbyParameter : NSObject
@property(nonatomic,copy) NSString *category_id;
@property(nonatomic,copy) NSString *location1;
@property(nonatomic,copy) NSString *location2;
@property(nonatomic,copy) NSString *count;
@property(nonatomic,copy) NSString *_t;
@end
