//
//  PlaceCategory+CoreDataProperties.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/1/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PlaceCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlaceCategory (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *category;
@property (nullable, nonatomic, retain) NSString *iconConfig;

@end

NS_ASSUME_NONNULL_END
