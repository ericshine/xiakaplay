//
//  CategoryObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CategoryObject.h"

@implementation CategoryObject
- (instancetype)init{
    self = [super init];
    if(self){
        [CategoryObject setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                      @"title":@"category_name",
                      @"subTtiles":@"sub"
                     };
        }];
        [CategoryObject setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"subTtiles":[CategoryObject class]
                     };
        }];
    }
    return self;
}
@end
