//
//  PlaceCategory.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceCategory.h"

@implementation PlaceCategory
+ (void)saveCategory:(NSDictionary *)object{
    [self clearAllData];
    PlaceCategory *category = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[CityMapModelManage sharedInstance].context];
    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:nil];
    category.category = data;
    [[CityMapModelManage sharedInstance] saveContext];
}
+ (PlaceCategory *)getCategory{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSArray *fetchArray =  [[CityMapModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!fetchArray.count){
        return nil;
    }
    return fetchArray[0];
}
+ (NSString *)iconUrlConfig{
   PlaceCategory *category =  [self getCategory];
    NSString *config;
    if(category){
     NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:category.category options:NSJSONReadingAllowFragments error:nil];
    config = dic[@"config"][@"prefix"];
    }
    return config;
}
+ (NSArray *)getAllData{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
   NSArray *fetchArray =  [[CityMapModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!fetchArray.count){
        return nil;
    }
    return fetchArray;
}
+ (void)clearAllData{
    for(NSManagedObject *obj in [self getAllData]){
        [[CityMapModelManage sharedInstance].context deleteObject:obj];
    }
}
// Insert code here to add functionality to your managed object subclass

@end
