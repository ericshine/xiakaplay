//
//  PlaceCategory.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CityMapModelManage.h"
NS_ASSUME_NONNULL_BEGIN

@interface PlaceCategory : NSManagedObject

+ (void)saveCategory:(NSDictionary *)object;
+ (PlaceCategory *)getCategory;
+ (NSString *)iconUrlConfig;
@end

NS_ASSUME_NONNULL_END

#import "PlaceCategory+CoreDataProperties.h"
