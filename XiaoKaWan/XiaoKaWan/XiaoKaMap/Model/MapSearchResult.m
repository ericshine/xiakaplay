//
//  MapSearchResult.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapSearchResult.h"
#import <MJExtension.h>
@implementation MapSearchResult
- (instancetype)init{
    self = [super init];
    if(self){
    [MapSearchResult setupReplacedKeyFromPropertyName:^NSDictionary *{
        return @{
                 @"place_id":@"id"
                 };
    }];
    }
    return self;
}
@end

