//
//  CategoryObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension.h>
@interface CategoryObject : NSObject
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *icon;
@property(nonatomic,strong) NSArray<CategoryObject*>*subTtiles;
/**
 *  图片配置  iconConfig + 类别id + .png
 */
@property(nonatomic,copy) NSString *iconConfig;
@end
