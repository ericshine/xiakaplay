//
//  PostBMKPointAnnotation.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostBMKPointAnnotation.h"
#import "ImageSizeUrl.h"
@implementation PostBMKPointAnnotation
- (void)setImageUrl:(NSString *)imageUrl{
    _imageUrl = [ImageSizeUrl image750_url:imageUrl];
}

@end
