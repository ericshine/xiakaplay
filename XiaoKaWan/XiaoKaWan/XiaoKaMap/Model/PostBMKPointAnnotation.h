//
//  PostBMKPointAnnotation.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/28/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>
@class Place;
@interface PostBMKPointAnnotation : BMKPointAnnotation
/**
 *  头像
 */
@property(nonatomic,copy) NSString *headImageUrl;
/**
 *  动态图片
 */
@property(nonatomic,copy) NSString *imageUrl;
/**
 *  地点是否有动态
 */
@property(nonatomic) BOOL isHavePost;
/**
 *  动态内容
 */
@property(nonatomic,copy) NSString *postContent;

@property(nonatomic,strong) Place *place;
@end
