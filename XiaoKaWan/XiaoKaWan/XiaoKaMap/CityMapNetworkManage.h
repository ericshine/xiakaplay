//
//  CityMapNetworkManage.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "ClientNetwork.h"
#import "APIConfig.h"
#import <MJExtension.h>
#import "UserConfig.h"
#import "BJNotificationConfig.h"
#import "QAlertView.h"
#import "CityMapPlaceNearbyParameter.h"
#define BJAPIDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)
@interface CityMapNetworkManage : NSObject
SINGLETON_H
/**
 *  获取类别
 *
 *  @param complete complete description
 */
- (void)getPlaceCategoryComplete:(requestComplete )complete;
/**
 *  获取类别
 *
 *  @param complete complete description
 */
- (void)getPlaceCommendCategoryComplete:(requestComplete )complete;
/**
 *  获取地点周边动态
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)getPlaceNearbyWithParameter:(CityMapPlaceNearbyParameter*)parameter complete:(requestComplete )complete;
/**
 *  搜索用户创建的附近点
 *
 *  @param parameter location1 , location2,count
 *  @param complete  complete description
 */
- (void)searchNearbyUserPlaceWithParameter:(CityMapPlaceNearbyParameter*)parameter complete:(requestComplete )complete;
/**
 *  搜索自有poi
 *
 *  @param parameter keyword
 *  @param complete  complete description
 */
- (void)searchUserPlaceWithKeywords:(NSString*)keyword complete:(requestComplete )complete;
@end
