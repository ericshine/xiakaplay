//
//  CityMapNetworkManage.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CityMapNetworkManage.h"

typedef void(^requestComplete)(BOOL succeed,id obj);
@implementation CityMapNetworkManage
SINGLETON_M(CityMapNetworkManage)
- (void)getPlaceCategoryComplete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].placeCategoryApi parameter:nil complete:complete];
}
- (void)getPlaceCommendCategoryComplete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].placeCommendCategoryApi parameter:nil complete:complete];
}
- (void)getPlaceNearbyWithParameter:(CityMapPlaceNearbyParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].placeNearbyApi parameter:parameter.keyValues complete:complete];
}
- (void)searchNearbyUserPlaceWithParameter:(CityMapPlaceNearbyParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].searchNearbyUserPlaceApi parameter:parameter.keyValues complete:complete];
}
- (void)searchUserPlaceWithKeywords:(NSString *)keyword complete:(requestComplete)complete{
    if(!keyword.length) {
        if(complete)complete(NO,@" ");
        return;
    };
    NSDictionary *parameter = @{@"keyword":keyword};
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].keyWordSearchUserPlaceApi parameter:parameter complete:complete];
}
@end
