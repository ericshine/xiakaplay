//
//  NSDate+BJDateFormat.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (BJDateFormat)
- (NSString*)dateToStringDefault;
- (NSString*)dateToStringWithFormat:(NSString*)formate;
- (NSDate *)stringToDateWithFormat:(NSString *)formate andString:(NSString*)time;
@end
