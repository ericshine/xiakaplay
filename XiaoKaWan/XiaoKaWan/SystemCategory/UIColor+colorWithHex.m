//
//  UIColor+colorWithHex.m
//  Share
//
//  Created by apple_Eric on 5/11/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "UIColor+colorWithHex.h"

@implementation UIColor (colorWithHex)
+ (UIColor *)mainColor{
    return [UIColor p_colorWithHex:0xffda44];
}
+(UIColor*)colorWithHex_4c4c4c
{
    return [UIColor p_colorWithHex:0x4c4c4c];
}
+ (UIColor *)p_colorWithHex:(unsigned int)rgbValue {
    return [UIColor p_colorWithHex:rgbValue alpha:1.0];
}
+ (UIColor *)p_colorWithHex:(unsigned int)rgbValue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:(CGFloat) (((rgbValue & 0xFF0000) >> 16) / 255.0) green:(CGFloat) (((rgbValue & 0xFF00) >> 8) / 255.0) blue:(CGFloat) ((rgbValue & 0xFF) / 255.0) alpha:alpha];
}

+ (UIColor *)RandomColor {
    NSInteger aRedValue = arc4random() % 255;
    NSInteger aGreenValue = arc4random() % 255;
    NSInteger aBlueValue = arc4random() % 255;
    UIColor *randColor = [UIColor colorWithRed:aRedValue / 255.0f green:aGreenValue / 255.0f blue:aBlueValue / 255.0f alpha:1.0f];
    return randColor;
}
@end
