//
//  NSDate+BJDateFormat.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "NSDate+BJDateFormat.h"

@implementation NSDate (BJDateFormat)
- (NSString *)dateToStringDefault{
   return [self dateToStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
}
- (NSString *)dateToStringWithFormat:(NSString *)formate{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:formate];
   NSString *dateStr = [dateFormate stringFromDate:self];
    return dateStr;
}
- (NSDate *)stringToDateWithFormat:(NSString *)formate andString:(NSString*)time{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:formate];
    NSDate *date = [dateFormate dateFromString:time];
    return date;

}
@end
