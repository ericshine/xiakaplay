//
//  UINavigationController+Category.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Category)
- (void)push:(UIViewController*)viewController;
- (void)popViewController;
- (UIViewController *)childViewControllerFromStr:(NSString*)controllerName;

@end
