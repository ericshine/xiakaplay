//
//  UINavigationController+Category.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UINavigationController+Category.h"

@implementation UINavigationController (Category)
- (void)push:(UIViewController*)viewController
{
    viewController.hidesBottomBarWhenPushed = YES;
    [self pushViewController:viewController animated:YES];
}
- (void)popViewController{
    [self popViewControllerAnimated:YES];
}
- (UIViewController *)childViewControllerFromStr:(NSString *)controllerName{
   __block UIViewController *controller;
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj isKindOfClass:NSClassFromString(controllerName)]){
            controller = obj;
        }
    }];
    return controller;
}

@end
