//
//  NSString+Cagetory.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
@interface NSString (Cagetory)
- (CGFloat)widthWithFont:(UIFont*)font;
- (CGSize)sizeWithFont:(UIFont *)font andSize:(CGSize)size;
- (NSMutableAttributedString *)getAttributedStringWithSpace:(CGFloat)space font:(UIFont *)font color:(UIColor *)color;
@end
