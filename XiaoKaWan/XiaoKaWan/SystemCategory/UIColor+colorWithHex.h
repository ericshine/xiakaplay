//
//  UIColor+colorWithHex.h
//  Share
//
//  Created by apple_Eric on 5/11/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

#define rgbColor(r,g,b,a)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define GrayColor(rgb)  [UIColor colorWithRed:rgb/255.0 green:rgb/255.0 blue:rgb/255.0 alpha:1.0]

@interface UIColor (colorWithHex)
/**
 *  Description 4c4c4c color
 *
 *  @return color
 */
+(UIColor *)mainColor;
+(UIColor*)colorWithHex_4c4c4c;
+ (UIColor *)p_colorWithHex:(unsigned int)rgbValue;
+ (UIColor *)p_colorWithHex:(unsigned int)rgbValue alpha:(CGFloat)alpha;

/**
 *  @brief  随机颜色
 *
 *  @return UIColor
 */
+ (UIColor *)RandomColor;
@end
