//
//  UIImage+Category.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+colorWithHex.h"
@interface UIImage (Category)
/**
 *  图片裁剪
 *
 *  @param image    图片
 *  @param viewsize 裁剪大小
 *
 *  @return 返回才将后的图片
 */
+ (UIImage *)cutImage: (UIImage *)image WithSize: (CGSize)viewsize;

/**
 *  图片设置圆角
 *
 *  @param image  image description
 *  @param size   size description
 *  @param radius radius description
 *
 *  @return return value description
 */
+ (id)createRoundedRectImage:(UIImage *)image size:(CGSize)size radius:(int)radius;
/**
 *  图片压缩
 *
 *  @param targetSize targetSize description
 *
 *  @return return value description
 */
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;
- (UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size;
/**
 *  通过颜色获得图片
 *
 *  @param color 生成图片的颜色
 *
 *  @return 返回一个通过颜色生成的图片
 */
+  (UIImage *)imageFromColor:(UIColor *)color;
/**
 *  模糊效果
 *
 *  @return return image
 */
- (UIImage *)applyLightEffect;
@end
