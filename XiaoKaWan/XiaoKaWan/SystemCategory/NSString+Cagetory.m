//
//  NSString+Cagetory.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "NSString+Cagetory.h"

@implementation NSString (Cagetory)
- (CGFloat)widthWithFont:(UIFont*)font{
    CGRect rect = [self boundingRectWithSize:CGSizeMake(kDeviceWidth, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    return rect.size.width;
}
- (CGSize)sizeWithFont:(UIFont *)font andSize:(CGSize)size{
    CGRect rect = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    return rect.size;
}
- (NSMutableAttributedString *)getAttributedStringWithSpace:(CGFloat)space font:(UIFont *)font color:(UIColor *)color{
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:self];

    [attStr setAttributes:@{NSKernAttributeName:@(space),NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:NSMakeRange(0, attStr.length)];
    return attStr;
}
@end
