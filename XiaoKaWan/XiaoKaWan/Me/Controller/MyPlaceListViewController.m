//
//  MyPlaceListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyPlaceListViewController.h"
#import "MyPlaceListViewModel.h"
@interface MyPlaceListViewController ()
@property(nonatomic,strong)MyPlaceListViewModel *viewModel;
@end

@implementation MyPlaceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}
- (MyPlaceListViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[MyPlaceListViewModel alloc] initWithModel:nil viewController:self andTabel:self.tableView];
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
