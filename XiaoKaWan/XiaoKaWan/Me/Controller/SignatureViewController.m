//
//  SignatureViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SignatureViewController.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#import "ChangeUserInformationViewModel.h"
#import "UserConfig.h"
@interface SignatureViewController ()
@property(nonatomic,strong) UITextView *textView;
@property(nonatomic,strong) UILabel *remindLabel;
@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"保存"];
    [self.view addSubview:[self textFieldView]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewChange:) name:UITextViewTextDidChangeNotification object:nil];
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    if(_textView.text.length>30){
        [[QAlertView sharedInstance] showAlertText:@"签名长度不能超过30个字" fadeTime:2];
        return;
    }
    [ChangeUserInformationViewModel changeInformation:@{@"sign":_textView.text,@"_t":[UserConfig sharedInstance].user_token} success:^(BOOL success) {
        if(success)[self.navigationController popViewControllerAnimated:YES];
    }];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)textViewChange:(NSNotification *)noti{
    NSInteger remindNumner = 30-_textView.text.length;
    
    if(remindNumner<10 && remindNumner>=0){
        _remindLabel.text = [NSString stringWithFormat:@"%li",(long)remindNumner];
        _remindLabel.textColor = [UIColor p_colorWithHex:0x6d6d6d];
    }else if(remindNumner<0){
        _remindLabel.text = [NSString stringWithFormat:@"%li",(long)remindNumner];
        _remindLabel.textColor = [UIColor redColor];
    }else{
    _remindLabel.text = @"";
    }
}
- (UIView *)textFieldView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 30, CGRectGetWidth(self.view.frame), 100)];
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 0, CGRectGetWidth(self.view.frame)-40, 100)];
    _textView.text = self.currentUser.sign;
    _textView.font = [UIFont systemFontOfSize:17];
    _textView.textColor = [UIColor p_colorWithHex:0x6d6d6d];
    [view addSubview:_textView];
    //_textView.scrollEnabled = NO;
    [_textView becomeFirstResponder];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_textView.frame)+16, CGRectGetWidth(self.view.frame), 0.5)];
    line.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
    [view addSubview:line];
    _remindLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)-75, CGRectGetMaxY(_textView.frame)+58, 50, 20)];
    _remindLabel.font = [UIFont systemFontOfSize:12];
    _remindLabel.textColor = [UIColor p_colorWithHex:0x6d6d6d];
    
    _remindLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:_remindLabel];
    
    return view;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
