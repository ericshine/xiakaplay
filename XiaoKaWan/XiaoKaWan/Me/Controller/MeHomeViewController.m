//
//  MeHomeViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHomeViewController.h"
#import "LoginViewController.h"
#import "BJNavigationController.h"
#import "SettingListViewController.h"
#import "UserMapsViewModel.h"
#import "UserPostViewModel.h"
#import "UserPlaceViewModel.h"
#import "MeHomeViewUnSiginViewModel.h"
#import "UserConfig.h"
#import "CurrentUser.h"
#import "BJNotificationConfig.h"
#import "MapObject.h"
#import "MapDetailListViewController.h"
#import "MapsPlaceDetailViewController.h"
#import "MapsUserListViewController.h"
#import "MyFansViewController.h"
#import "MeNetworkManage.H"
#import "PostParatemer.h"
#import "PlacePostDetailViewController.h"
#import "MyFollowerViewController.h"
#import "BJShareTool.h"
#import "ChangeAvatarViewController.h"
@interface MeHomeViewController ()<UserinforViewModelDelegate,UserMapsViewModelDelegate,UserPlaceViewModelDelegate,UserPostViewModelDelegate>
@property(nonatomic,strong) UserinforViewModel *viewModel;
@property(nonatomic,strong) UserMapsViewModel *mapsViewModel;
@property(nonatomic,strong) UserPostViewModel *postViewModel;
@property(nonatomic,strong) UserPlaceViewModel *placeBeenViewModel;
@property(nonatomic,strong) UserPlaceViewModel *placeWantViewModel;
@property(nonatomic,strong) MeHomeViewUnSiginViewModel *unSiginViewModel;
@property(nonatomic,strong) CurrentUser *user;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UICollectionView *unLoginCollectionView;
@end

@implementation MeHomeViewController{
    BOOL needReloadMaps;
    BOOL needReloadPost;
    BOOL needReloadBeenPlace;
    BOOL needReloadWantPlace;
    NSInteger currentIndex;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.userInforType != USERINFOR_OTHER) {
    [self.tabBarController.tabBar setHidden:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    [self initView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:BJLoginNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSuccess:) name:BJLogOutNotification object:nil];
    // Do any additional setup after loading the view.
}
- (void)initView{
    currentIndex = 0;
    if(self.userInforType == USERINFOR_OTHER){
        //        [self getUserInfor:^(bool success) {
        self.currentUser.isMyInformation = NO;
        [self collectionView];
        self.collectionView.dataSource = self.viewModel;
        self.collectionView.delegate = self.viewModel;
        if(self.currentUser.uid == [[UserConfig sharedInstance].userId integerValue]){
            [self rightButtonWithIconfont:@"\U0000e623"];
        }else [self rightButtonWithIconfont:@"\U0000e626" title1:@"\U0000e623"];
        //        }];
    }else{
        if([UserConfig sharedInstance].user_token.length){
            self.currentUser.isMyInformation = YES;
            [self collectionView];
            [self rightButtonWithIconfont:@"\U0000e605" title1:@"\U0000e623"];
            self.collectionView.dataSource = self.viewModel;
            self.collectionView.delegate = self.viewModel;
            
            [[NSNotificationCenter defaultCenter] removeObserver:self.viewModel name:BJChangeUserInformationNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self.viewModel name:BJNeedReloadUserInformationNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:BJDeletePostNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:BJAddPostNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:BJBeenActionNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:BJWantActionNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:BJMapsChangedNotification object:nil];
            
            [[NSNotificationCenter defaultCenter] addObserver:self.viewModel selector:@selector(userInformationChanged:) name:BJChangeUserInformationNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPost) name:BJDeletePostNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPost) name:BJAddPostNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPlaceBeen) name:BJBeenActionNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPlaceWant) name:BJWantActionNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self.viewModel selector:@selector(refreshUserInf) name:BJNeedReloadUserInformationNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMaps) name:BJMapsChangedNotification object:nil];
        }else{
            [self unLoginCollectionView];
            self.navigationItem.rightBarButtonItems = nil;
            self.unLoginCollectionView.dataSource = self.unSiginViewModel;
            self.unLoginCollectionView.delegate = self.unSiginViewModel;
        }
    }
    
}
- (void)rightClick{
    if(self.userInforType != USERINFOR_OTHER){
    SettingListViewController *settingVc = [[SettingListViewController alloc] initWithTableStyle:UITableViewStylePlain];
    settingVc.user = self.user;
    [self.tabBarController.tabBar setHidden:YES];
    [self push:settingVc];
    }else{
        if(self.currentUser.uid == [[UserConfig sharedInstance].userId integerValue]){
            [BJShareTool shareHomePageWithTypeViewController:self UID:[NSString stringWithFormat:@"%ld",(long)self.currentUser.uid]];
        }else{
            NSString *title;
            NSString *actionTitle;
            if(self.currentUser.followed){
                title = @"确定取消关注？";
                actionTitle = @"不再关注";
            }else{
                title = @"将要关注此人";
                actionTitle = @"关注";
            }
            BJAlertController  *alterVc = [[BJAlertController alloc] initWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            [alterVc addDestructiveActionWithTitle:actionTitle handler:^{
                UserParameter *parameter = [[UserParameter alloc] init];
                parameter._t = [UserConfig sharedInstance].user_token;
                parameter.uid = [NSString stringWithFormat:@"%li",(long)self.currentUser.uid];
                if(self.currentUser.followed){
                    [[MeNetworkManage sharedInstance] unFollowUserWithPararmeter:parameter complete:^(BOOL succeed, id obj) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                        self.currentUser.followed = NO;
                        [self rightButtonWithIconfont:@"\U0000e626" color1:0x000000 title1:@"\U0000e623" color2:0x000000];
                        
                    }];
                }else{
                    [[MeNetworkManage sharedInstance] followUserWithParameter:parameter complete:^(BOOL succeed, id obj) {
                        [self rightButtonWithIconfont:@"\U0000e626" color1:0xCB2929 title1:@"\U0000e623" color2:0x000000];
                        self.currentUser.followed = YES;
                        [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                    }];
                }
                
            }];
            [alterVc addCancleActionHandler:^{
                
            }];
            [self presentViewController:alterVc animated:YES completion:nil];
        }
    }
}
- (void)rightClick1{
    [BJShareTool shareHomePageWithTypeViewController:self UID:[NSString stringWithFormat:@"%ld",(long)self.currentUser.uid] ];
}
- (void)reloadMaps{
   
   [self.viewModel refreshAllData];
    if(currentIndex != 1) needReloadMaps = YES;
    else needReloadMaps = NO;
}
-(void)reloadPost{
    [self.viewModel refreshAllData];
   
    if(currentIndex != 0) needReloadPost = YES;
    else needReloadPost = NO;
}
- (void)reloadPlaceBeen{
     [self.viewModel refreshAllData];
    if(currentIndex != 2) needReloadBeenPlace = YES;
    else  needReloadBeenPlace = NO;
}
- (void)reloadPlaceWant{
    [self.viewModel refreshAllData];
    if(currentIndex != 2) needReloadWantPlace = YES;
    else  needReloadWantPlace = NO;
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self.view addSubview:_collectionView];
        [_collectionView setContentInset:UIEdgeInsetsMake(0, 0, 20, 0)];
    }
    return _collectionView;
}

- (UICollectionView *)unLoginCollectionView{
    if(_unLoginCollectionView == nil){
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _unLoginCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) collectionViewLayout:layout];
        
        _unLoginCollectionView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self.view addSubview:_unLoginCollectionView];
    }
    return _unLoginCollectionView;
}
- (void)loginSuccess:(NSNotification*)noti{
     [self.tabBarController.tabBar setHidden:NO];
    _unSiginViewModel = nil;
    [_unLoginCollectionView removeFromSuperview];
    _unLoginCollectionView = nil;
    [self initView];
}
- (void)logoutSuccess:(NSNotification *)notif{
    _currentUser = nil;
    _user = nil;
    _viewModel.layout = nil;
    _viewModel = nil;
    _mapsViewModel = nil;
    _placeBeenViewModel = nil;
    _placeWantViewModel = nil;
    _postViewModel = nil;
    [_collectionView removeFromSuperview];
    _collectionView = nil;
    [self initView];
}
- (User*)currentUser{
    if(_currentUser == nil){
        _currentUser = [[User alloc] init];
        _currentUser = [User coreDataUserToUser:self.user];
    }
    return _currentUser;
}
- (CurrentUser *)user{
    if(_user == nil){
        _user = [CurrentUser getCurrentUser];
    }
    return _user;
}

- (UserinforViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[UserinforViewModel alloc] initWithCollcetionview:_collectionView user:self.currentUser];
        _viewModel.delegate = self;
        _viewModel.dataSourceDelegete = self.postViewModel;// 默认
        __weak typeof(self) weakSelf = self;
        _viewModel.loadUserInf = ^(User *user){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.currentUser = user;
            if((strongSelf.userInforType == USERINFOR_OTHER) && (strongSelf.currentUser.uid != [[UserConfig sharedInstance].userId integerValue])){
                if(user.followed){
//                    [self rightButtonWithIconfont:@"\U0000e626" title1:@"\U0000e623"];
                    [strongSelf rightButtonWithIconfont:@"\U0000e626" color1:0xCB2929 title1:@"\U0000e623" color2:0x000000];
                }else{
                    [strongSelf rightButtonWithIconfont:@"\U0000e626" color1:0x000000 title1:@"\U0000e623" color2:0x000000];
                }
            }
        };
    }
    return _viewModel;
}
- (UserMapsViewModel *)mapsViewModel{
    if(_mapsViewModel == nil){
        _mapsViewModel = [[UserMapsViewModel alloc] init];
        _mapsViewModel.delegate = self;
    }
    return _mapsViewModel;
}
- (UserPostViewModel *)postViewModel{
    if(_postViewModel == nil){
        _postViewModel = [[UserPostViewModel alloc] init];
        _postViewModel.delegate = self;
    }
    return _postViewModel;
}
- (UserPlaceViewModel *)placeBeenViewModel{
    if(_placeBeenViewModel == nil){
        _placeBeenViewModel = [[UserPlaceViewModel alloc] init];
        _placeBeenViewModel.beenWantType = BEENWANTTYPE_BEEN;
        _placeBeenViewModel.delegate=  self;
    }
    return _placeBeenViewModel;
}
- (UserPlaceViewModel *)placeWantViewModel{
    if(_placeWantViewModel == nil){
        _placeWantViewModel = [[UserPlaceViewModel alloc] init];
        _placeWantViewModel.beenWantType = BEENWANTTYPE_WANT;
        _placeWantViewModel.delegate=  self;
    }
    return _placeWantViewModel;
}
- (MeHomeViewUnSiginViewModel*)unSiginViewModel{
    if(_unSiginViewModel ==nil){
        _unSiginViewModel = [[MeHomeViewUnSiginViewModel alloc] initWithController:self collectionView:_unLoginCollectionView];
    }
    return _unSiginViewModel;
}

#pragma mark - UserinforViewModelDelegate
- (void)toFans{
    MyFansViewController *fansVc = [[MyFansViewController alloc] init];
    fansVc.currentUser = self.currentUser;
    [self push:fansVc];
}
- (void)toFollows{
    MyFollowerViewController *follower = [[MyFollowerViewController alloc] init];
    follower.currentUser = self.currentUser;
     [self.tabBarController.tabBar setHidden:YES];
    [self push:follower];
}
- (void)toUserHeadImage{
    if(self.userInforType != USERINFOR_OTHER){
    ChangeAvatarViewController *headVieVc = [[ChangeAvatarViewController alloc] init];
    headVieVc.currentUser = self.user;
    [self.tabBarController.tabBar setHidden:YES];
    [self push:headVieVc];
    }
}
- (void)selectAtIndex:(NSUInteger)index{
    currentIndex = index;
    [self.collectionView.footer resetNoMoreData];
    if(index == 0){
        self.viewModel.dataSourceDelegete = nil;
        self.viewModel.dataSourceDelegete = self.postViewModel;
        if(needReloadPost) {
            needReloadPost = NO;
            [self.viewModel refreshAllData];
        }
       
    }else if(index == 1){
        self.viewModel.dataSourceDelegete = self.mapsViewModel;
        if(self.mapsViewModel.mapsList.count<=0|| needReloadMaps ){
            needReloadMaps = NO;
            [self.viewModel refreshAllData];
        }
    }else if(index == 2){
          self.viewModel.dataSourceDelegete = nil;
     self.viewModel.dataSourceDelegete = self.placeBeenViewModel;
        if(self.placeBeenViewModel.placeList.count<=0 || needReloadBeenPlace){
            needReloadBeenPlace = NO;
           [self.viewModel refreshAllData];
        }
    }else if(index == 3){
        self.viewModel.dataSourceDelegete = nil;
        self.viewModel.dataSourceDelegete = self.placeWantViewModel;
        if(self.placeWantViewModel.placeList.count<=0 || needReloadWantPlace){
            needReloadWantPlace = NO;
            [self.viewModel refreshAllData];
        }
    }
    [self.collectionView reloadData];
}
#pragma mark -UserPostViewModelDelegate
- (void)toPostDetail:(PostObjet *)postDetail{
    PostParatemer *parameter = [PostParatemer parameter];
    parameter.post_id = [NSString stringWithFormat:@"%li",(long)postDetail.id];
    PlacePostDetailViewController *postDetailVc = [[PlacePostDetailViewController alloc] init];
    postDetailVc.parameter = parameter;
     [self.tabBarController.tabBar setHidden:YES];
    [self push:postDetailVc];
}
#pragma mark - UserMapsViewModelDelegate
- (void)toAlbumDetail:(MapsDetail *)albumObj{
    MapObject *mapsObj = [[MapObject alloc] init];
    mapsObj.id = [NSString stringWithFormat:@"%li",(long)albumObj.id];
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    detailList.mapObject = mapsObj;
     [self.tabBarController.tabBar setHidden:YES];
    [self push:detailList];
}
#pragma mark -UserPlaceViewModelDelegate
- (void)toPlaceDetail:(Place *)place{
    MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
    placeDetail.place = place;
     [self.tabBarController.tabBar setHidden:YES];
    [self push:placeDetail];

}
- (void)toUserList:(Place *)place{
    MapObject *placeObj = [[MapObject alloc] init];
    placeObj.placeId = place.placeId;
    MapsUserListViewController *userListVc = [[MapsUserListViewController alloc] init];
    userListVc.model = placeObj;
     [self.tabBarController.tabBar setHidden:YES];
    [self push:userListVc];
}
//- (void)getUserInfor:(void(^)(bool success))success{
//    UserParameter *parameter = [[UserParameter alloc] init];
//    parameter.uid = [NSString stringWithFormat:@"%ld",(long)self.currentUser.uid];
//    [[MeNetworkManage sharedInstance] getUserInforWithParameter:parameter complete:^(BOOL succeed, id obj) {
//        self.currentUser = [User objectWithKeyValues:obj];
//        self.currentUser.isMyInformation = NO;
//        if(succeed){
//            if(success)success(succeed);
//        }else{
//            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
//        }
//    }];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
