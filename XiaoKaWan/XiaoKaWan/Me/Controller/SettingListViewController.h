//
//  SettingListViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "CurrentUser.h"
@interface SettingListViewController : BJSuperViewController
@property(nonatomic,strong) CurrentUser *user;
@end
