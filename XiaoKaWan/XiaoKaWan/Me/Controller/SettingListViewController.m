//
//  SettingListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SettingListViewController.h"
#import "SettingListViewModel.h"
#import "SettingLogoutView.h"
#import "LoginViewController.h"
#import "BJNavigationController.h"
#import "UMessage.h"
@interface SettingListViewController ()<SettingLogoutViewDelegate>
@property(nonatomic,strong) SettingListViewModel *viewModel;
@property(nonatomic,strong) SettingLogoutView *logoutView;
@end

@implementation SettingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [UIApplication sharedApplication].statusBarStyle =
    //self.edgesForExtendedLayout =  UIRectEdgeAll;
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.tableFooterView = self.logoutView;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self.viewModel selector:@selector(userInformationChange:) name:BJChangeUserInformationNotification object:nil];
//    [self disenableNavigationOpaque];
    // Do any additional setup after loading the view.
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self.viewModel];
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    [super preferredStatusBarStyle];
    return UIStatusBarStyleDefault;
}
- (SettingLogoutView *)logoutView{
    if(_logoutView == nil){
        _logoutView = [[SettingLogoutView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 145)];
        _logoutView.delegate = self;
    }
    return _logoutView;
}
- (SettingListViewModel*)viewModel{
    if(_viewModel == nil){
        _viewModel = [[SettingListViewModel alloc] initWithTabel:self.tableView andController:self model:self.user];
    }
    return _viewModel;
}
#pragma mark - SettingLogoutViewDelegate
- (void)userLogout{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"确定退出" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [UMessage removeAlias:[UserConfig sharedInstance].userId type:@"UID" response:^(id responseObject, NSError *error) {
        }];
       
        [self toLoginVcClearData:YES];
         [self popViewController];
        [[NSNotificationCenter defaultCenter] postNotificationName:BJLogOutNotification object:nil];
       
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
