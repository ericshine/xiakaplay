//
//  BJMyFollowViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "User.h"
@interface MyFollowerViewController : BJSuperViewController
@property(nonatomic,strong)User *currentUser;
@end
