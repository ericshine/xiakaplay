//
//  MyMapsListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyMapsListViewController.h"
#import "MyMapsListViewModel.h"
@interface MyMapsListViewController ()
@property(nonatomic,strong)MyMapsListViewModel *viewModel;
@end

@implementation MyMapsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}
- (MyMapsListViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[MyMapsListViewModel alloc] initWithModel:nil controller:self];
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
