//
//  ChangeAvatarViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangeAvatarViewController.h"
#import "AvatarView.h"
#import "ChangeAvatarViewModel.h"
#import "ImageSizeUrl.h"
#import <AVFoundation/AVFoundation.h>
#import<AssetsLibrary/AssetsLibrary.h>
//#import "ChangeUserInformationViewModel.h"
@interface ChangeAvatarViewController ()<ChangeAvatarViewModelDelegate>
@property(nonatomic,strong)AvatarView *avatarView;
@property(nonatomic,strong)ChangeAvatarViewModel *viewModel;
@property(nonatomic,strong)UIImagePickerController *imagePicker;
@end

@implementation ChangeAvatarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"更换"];
    self.tableView.tableHeaderView = self.avatarView;
    self.tableView.separatorStyle = 0;
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    UIAlertController *sheetVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"拍照" style:0 handler:^(UIAlertAction * _Nonnull action) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusDenied){
            BJAlertController *alertVc = [[BJAlertController alloc] initWithTitle:@"小咖玩想要使用您的相机" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alertVc addActionWithTitle:@"设置" handler:^{
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }];
            [alertVc addCancleActionHandler:nil];
            
            [self presentViewController:alertVc animated:YES completion:nil];
        }else [self selelctImageFromAlbumOrCamera:UIImagePickerControllerSourceTypeCamera];
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"从相册中选取" style:0 handler:^(UIAlertAction * _Nonnull action) {
        ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
        if(author == ALAuthorizationStatusDenied){
            BJAlertController *alertVc = [[BJAlertController alloc] initWithTitle:@"小咖玩想要访问您的相册" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alertVc addActionWithTitle:@"设置" handler:^{
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }];
            [alertVc addCancleActionHandler:nil];
            
            [self presentViewController:alertVc animated:YES completion:nil];
        }else [self selelctImageFromAlbumOrCamera:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:sheetVc animated:YES completion:nil];
}
- (void)selelctImageFromAlbumOrCamera:(UIImagePickerControllerSourceType)sourceType{
    if(_imagePicker == nil){
     _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.delegate = self.viewModel;
        _imagePicker.allowsEditing = YES;
    }
    _imagePicker.sourceType = sourceType;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (AvatarView*)avatarView{
    if(_avatarView == nil){
        _avatarView = [[AvatarView alloc] initWithFrame:CGRectMake(0, 15, CGRectGetWidth(self.view.frame), 333+15)];
        [_avatarView.aratarImageView setAvatarImageWithUrlString:[ImageSizeUrl image750_url:self.currentUser.avatar]];
    }
    return _avatarView;
}
- (ChangeAvatarViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[ChangeAvatarViewModel alloc] init];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma mark - ChangeAvatarViewModelDelegate
- (void)selectImage:(UIImage *)image{
    self.avatarView.aratarImageView.image = image;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
