//
//  ChangePassordViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangePassordViewController.h"
#import "ChangePasswordView.h"
#import "ClientNetwork.h"
#import "QAlertView.h"
#import "RegistAccountObject.h"
#import "UserConfig.h"
#import "CurrentUser.h"
@interface ChangePassordViewController ()<ChangePasswordViewDelegae>
@property(nonatomic,strong) ChangePasswordView *changPasswordView;
@property(nonatomic,strong) NSTimer *timer;
@end

@implementation ChangePassordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"保存"];
    [self.view addSubview:self.changPasswordView];
//    self.tableView.tableHeaderView = self.changPasswordView;
    // Do any additional setup after loading the view.
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}
- (void)rightClick{
    RegistAccountObject *countObject = [[RegistAccountObject alloc] init];
    countObject.phoneNumber = self.changPasswordView.phoneNumberTextField.text;
    countObject.verfyNumber = self.changPasswordView.verfyNumberTextField.text;
    countObject.passwordNumber = self.changPasswordView.passwordTextField.text;
    
    if(countObject.phoneNumber.length<11){
        [[QAlertView sharedInstance] showAlertText:@"请输入正确的手机号" fadeTime:2];
        return;
    }
    if(countObject.verfyNumber.length<3){
        [[QAlertView sharedInstance] showAlertText:@"请输入正确的验证码" fadeTime:2];
        return;
    }if(countObject.passwordNumber.length<5){
        [[QAlertView sharedInstance] showAlertText:@"密码格式不正确" fadeTime:2];
        return;
    }
    [self changePw:@{@"mobile":countObject.phoneNumber,@"captcha":countObject.verfyNumber,@"password":countObject.passwordNumber} error:^(NSString *errorString) {
        
    }];
}
- (NSTimer *)timer{
    if(_timer == nil){
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self.changPasswordView.verfyNumberTextField selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}
- (void)timerStop{
    [self.timer invalidate];
    self.timer = nil;
}
- (ChangePasswordView *)changPasswordView{
    if(_changPasswordView == nil){
        _changPasswordView = [[ChangePasswordView alloc] initWithFrame:CGRectMake(34, 60, CGRectGetWidth(self.view.frame)-68, 150)];
        _changPasswordView.delegate = self;
        [self.view addSubview:_changPasswordView];
    }
    return _changPasswordView;
}
#pragma mark - ChangePasswordViewDelegae
- (void)verfyButtonAction:(void (^)(BOOL))isStart{
    if(_changPasswordView.phoneNumberTextField.text.length<11){
    [[QAlertView sharedInstance] showAlertText:@"请输入正确的手机号" fadeTime:2];
        if(isStart)isStart(NO);
        return;
    }
    [self getVerfy:@{@"mobile":_changPasswordView.phoneNumberTextField.text,@"type":@"resetpassword"} error:^(NSString *errorString, bool start) {
        if(start){
            if(isStart)isStart(YES);
             [self timer];
        }
    }];
   
}
- (void)verfyStop{
    [self timerStop];
}
#pragma mark - netWork
- (void)getVerfy:(NSDictionary *)parameter error:(void(^)(NSString *errorString,bool isStart))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].registerVerfy parameter:parameter complete:^(BOOL succeed, id obj) {
        if(errorString)errorString(succeed==YES?@"":obj,succeed);
    }];
}
- (void)changePw:(NSDictionary *)parameter error:(void(^)(NSString *errorString))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].forgetPassworkApi parameter:parameter complete:^(BOOL succeed, id obj) {
        
        if(succeed){
            [[QAlertView sharedInstance] showAlertText:@"修改成功" fadeTime:2];
            [UserConfig sharedInstance].user_token = obj[@"token"];
            [UserConfig sharedInstance].userId = obj[@"userinfo"][@"uid"];
            [CurrentUser saveUserDataWithDic:obj[@"userinfo"]];
            [self popViewController];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
