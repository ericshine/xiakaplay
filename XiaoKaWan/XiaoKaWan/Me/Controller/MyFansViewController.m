//
//  MyFansViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyFansViewController.h"
#import "FansViewModel.h"
#import "MeHomeViewController.h"
@interface MyFansViewController ()<FansViewModelDelegate>
@property(nonatomic,strong)FansViewModel *viewModel;
@end

@implementation MyFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self.viewModel;
    self.tableView.delegate = self.viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
     [[NSNotificationCenter defaultCenter] addObserver:self.viewModel selector:@selector(reloadData) name:BJNeedReloadUserInformationNotification object:nil];
    // Do any additional setup after loading the view.
}
- (FansViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[FansViewModel alloc] initWithTable:self.tableView andUser:self.currentUser];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma mark - FansViewModelDelegate
- (void)toUserInfor:(User *)user{
    MeHomeViewController *userInfor = [[MeHomeViewController alloc] init];
    userInfor.currentUser = user;
    userInfor.userInforType = USERINFOR_OTHER;
    [self push:userInfor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
