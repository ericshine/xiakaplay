//
//  ChangeNameViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangeNameViewController.h"
#import "UIColor+colorWithHex.h"
#import "ChangeUserInformationViewModel.h"
#import "QAlertView.h"
#import "UserConfig.h"

@interface ChangeNameViewController ()
@property(nonatomic,strong)UITextField *textField;
@end

@implementation ChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButtonWithTitle:@"保存"];
    [self.view addSubview:[self textFieldView]];
    // Do any additional setup after loading the view.
}
- (UIView *)textFieldView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 30, CGRectGetWidth(self.view.frame), 60)];
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, 200, 20)];
    _textField.text = self.currentUser.nick;
    _textField.font = [UIFont systemFontOfSize:17];
    _textField.textColor = [UIColor p_colorWithHex:0x434343];
    [view addSubview:_textField];
    [_textField becomeFirstResponder];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_textField.frame)+16, CGRectGetWidth(self.view.frame), 0.5)];
    line.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
    [view addSubview:line];
    UILabel *remindLable=[[UILabel alloc]init];
    remindLable.text=@"昵称长度4-16个字符，仅支持中英文、数字、\" _ \"";
    remindLable.textColor=GrayColor(153);
    remindLable.frame=CGRectMake(24, 46,[UIScreen mainScreen].bounds.size.width-48, 13);
    remindLable.font=[UIFont systemFontOfSize:13];
    [view addSubview:remindLable];
    
    return view;
}
- (void)rightClick{
    if(_textField.text.length<2){
        [[QAlertView sharedInstance] showAlertText:@"无效的昵称" fadeTime:2];
        return;
    }
    [ChangeUserInformationViewModel changeInformation:@{@"nick":_textField.text,@"_t":[UserConfig sharedInstance].user_token} success:^(BOOL success) {
       if(success)[self.navigationController popViewControllerAnimated:YES];
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
