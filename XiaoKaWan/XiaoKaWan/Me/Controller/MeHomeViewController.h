//
//  MeHomeViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "User.h"
typedef NS_ENUM(NSInteger,UserInforType){
    USERINFOR_TYPE,
    USERINFOR_ME,
    USERINFOR_OTHER
};
@interface MeHomeViewController : BJSuperViewController
@property(nonatomic,strong)User *currentUser;
@property(nonatomic)UserInforType userInforType;
@end
