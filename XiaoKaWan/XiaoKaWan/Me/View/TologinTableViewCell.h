//
//  TologinTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  废弃的类  UnLoginCollectionViewCelll instead
 */
@interface TologinTableViewCell : UITableViewCell
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIButton *loginButton;
@property(nonatomic,strong) UIImageView *lineView;
@property(nonatomic,copy) void(^loginAction)();
@end
