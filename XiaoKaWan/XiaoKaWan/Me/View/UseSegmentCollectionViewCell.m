//
//  UseSegmentCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserSegmentCollectionViewCell.h"
@implementation UserSegmentCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setUser:(User *)user{
   BJTitleNumberButton *buttonMaps =  self.segmentView.buttons[1];
    BJTitleNumberButton *buttonPost =  self.segmentView.buttons[0];
    BJTitleNumberButton *buttonBeen =  self.segmentView.buttons[2];
    BJTitleNumberButton *buttonWant =  self.segmentView.buttons[3];
    NSInteger mapsNum = [user.maps_count integerValue] + [user.maps_create_count integerValue];
    buttonMaps.numberLab.text = [NSString stringWithFormat:@"%li",(long)mapsNum];
    buttonPost.numberLab.text = user.post_count;
    buttonBeen.numberLab.text = user.been_count;
    buttonWant.numberLab.text = user.want_count;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right);
    }];
}
- (NumberTiletlSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView = [[NumberTiletlSegmentView alloc] initWithFrame:CGRectZero withTitles:@[@"动态",@"专辑",@"去过",@"想去"]];
        _segmentView.defaultIndex = 0;
        _segmentView.delegate = self;
        [self.contentView addSubview:_segmentView];
    }
    return _segmentView;
}
#pragma mark - NumberTiletlSegmentViewDelegate
- (void)selectAtIndex:(NSInteger)index{
    [self.delegate selectAtIndex:index];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
