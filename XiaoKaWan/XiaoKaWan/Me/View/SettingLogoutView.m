//
//  SettingLogoutView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SettingLogoutView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@implementation SettingLogoutView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.logoutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(100);
        make.left.equalTo(self.mas_left).offset(35);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right).offset(-35);
    }];
}
- (UIButton*)logoutButton
{
    if(_logoutButton==nil){
        _logoutButton = [[UIButton alloc] init];
        [_logoutButton setBackgroundColor:[UIColor p_colorWithHex:0xffcb00]];
        _logoutButton.layer.cornerRadius = 5;
        _logoutButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_logoutButton setTitle:@"退出登陆" forState:UIControlStateNormal];
        [_logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_logoutButton addTarget:self action:@selector(logoutAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_logoutButton];
    }
    return _logoutButton;
}
- (void)logoutAction:(UIButton *)button{
    if([self.delegate respondsToSelector:@selector(userLogout)]){
        [self.delegate userLogout];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
