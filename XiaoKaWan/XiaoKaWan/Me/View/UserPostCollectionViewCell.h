//
//  UserPostCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"
#import "PostObjet.h"
#import "PostFrameObject.h"
@interface UserPostCollectionViewCell : BJCollectionViewCell
@property(nonatomic,strong)BJUIImageView *imageView;
@property(nonatomic,strong)UILabel *contentLb;
@property(nonatomic,strong)UILabel *addressLb;
@property(nonatomic,strong)UIButton *likeLabel __deprecated_msg("commentAndLikes 替换");
@property(nonatomic,strong)UIButton *commitLabel __deprecated_msg("commentAndLikes 替换");
@property(nonatomic,strong)UILabel *commentAndLikes;
@property(nonatomic,strong)PostObjet *postObject;
@property(nonatomic,strong)UILabel *multiImageLb;
@end
