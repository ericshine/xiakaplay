//
//  MyMapsListHeadTableCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyMapsListHeadTableCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#define headViewHeight 89
@implementation MyMapsListHeadTableCell
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.numberLabel.mas_bottom).offset(17);
        make.left.equalTo(self.contentView.mas_left).offset(20);
    }];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-20);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-15);
        make.height.equalTo(@(headViewHeight));
        make.width.equalTo(@(headViewHeight));
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@2);
    }];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:24 color:0xb5b5b5];
        _titleLabel.text = @"榜单";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)numberLabel
{
    if(_numberLabel==nil){
        _numberLabel = [UILabel creatLabelWithFont:53 color:0x555555];
        _numberLabel.text = @"666";
        [_numberLabel sizeToFit];
        [self.contentView addSubview:_numberLabel];
    }
    return _numberLabel;
}
- (BJUIImageView*)headImageView
{
    if(_headImageView==nil){
        _headImageView = [[BJUIImageView alloc] init];
       // [_headImageView setAvatarImageWithUrlString:@"http://vimg3.ws.126.net/image/snapshot/2016/5/5/S/VBLI6CF5S.jpg"];
        [self.contentView addSubview:_headImageView];
    }
    return _headImageView;
}
- (UIImageView*)lineView{
    if(_lineView == nil){
        _lineView = [[UIImageView alloc] init];
        _lineView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
