//
//  PlaceListTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "BeenWantView.h"
#import "MyPlaceObject.h"
@protocol PlaceListTableViewCellDelegate <NSObject>

- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)selectImageViewWithPlace:(Place*)place;
//- (void)showMoreReason:(BOOL)show atIndexPath:(NSIndexPath*)indexPath;

@end
@interface PlaceListTableViewCell : UITableViewCell<BeenWantViewDelegate>
@property(nonatomic,strong)UILabel *titleLabel;
//@property(nonatomic,strong)UICollectionView *collectionView;
//@property(nonatomic,strong)UIImageView *tagLabel;
@property(nonatomic,strong)BeenWantView *beenWantView;
@property(nonatomic,assign)id<PlaceListTableViewCellDelegate>delegate;
@property(nonatomic,strong)Place *place;
@property(nonatomic,strong)MyPlaceObject *placeObject;
@property(nonatomic,strong)NSIndexPath *indexPath;
@end
