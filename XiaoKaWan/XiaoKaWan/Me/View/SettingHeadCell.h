//
//  SettingHeadCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
@interface SettingHeadCell : UITableViewCell
@property(nonatomic,strong)BJUIImageView *rightImageView;
@property(nonatomic,strong)UIView *lineView;
@end
