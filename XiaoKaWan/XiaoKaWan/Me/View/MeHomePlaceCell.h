//
//  MeHomePlaceCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
@protocol MeHomePlaceCellDelegate <NSObject>

- (void)showUserListWithData:(NSArray *)array;

@end
@class Place;
@interface MeHomePlaceCell : UITableViewCell
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) BJGroupHeadImageView *groupBeenHeadView;
@property(nonatomic,strong) BJGroupHeadImageView *groupWantHeadView;
@property(nonatomic,strong) UIButton *beenButton;
@property(nonatomic,strong) UIButton *wantButton;
@property(nonatomic,strong) Place *place;
@property(nonatomic,strong) UILabel*flagLabelIcon;
@property(nonatomic,strong) UILabel *wantLabelIcon;
@property(nonatomic,assign) id<MeHomePlaceCellDelegate> delegate;

@end
