//
//  ChangePasswordView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftRightViewTextField.h"
@protocol ChangePasswordViewDelegae <NSObject>

- (void)verfyButtonAction:(void(^)(BOOL isStart))isStart;
- (void)verfyStop;
@end
@interface ChangePasswordView : UIView<LeftRightViewTextFieldDelegate,UITextFieldDelegate>
@property(nonatomic,strong)id<ChangePasswordViewDelegae>delegate;
@property(nonatomic,strong)LeftRightViewTextField *phoneNumberTextField;
@property(nonatomic,strong)LeftRightViewTextField *verfyNumberTextField;
@property(nonatomic,strong)LeftRightViewTextField *passwordTextField;
//@property(nonatomic,copy) void(^verfyStart)();
//@property(nonatomic,copy) void(^verfyStop)();
- (void)hiddenKeyboard;
@end
