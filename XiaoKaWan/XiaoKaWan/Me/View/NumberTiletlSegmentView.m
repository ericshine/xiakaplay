//
//  NumberTiletlSegmentView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "NumberTiletlSegmentView.h"

@implementation NumberTiletlSegmentView{
    NSInteger currentIndex;
}
- (instancetype)initWithFrame:(CGRect)frame withTitles:(NSArray *)titles{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        _buttons = [[NSMutableArray alloc] initWithCapacity:titles.count];
        [self creatItmsWithTitles:titles];
    }
    return self;
}
- (void)setDefaultIndex:(NSInteger)defaultIndex{
    currentIndex = defaultIndex;
    BJTitleNumberButton *button = _buttons[defaultIndex];
    button.titleLab.textColor = [UIColor p_colorWithHex:0xFFD946];
    button.numberLab.textColor = [UIColor p_colorWithHex:0xFFD946];
}
- (void)creatItmsWithTitles:(NSArray *)titles{
    CGFloat width = kDeviceWidth/titles.count;
    [titles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BJTitleNumberButton *button = [[BJTitleNumberButton alloc] initWithFrame:CGRectMake(0+width*idx, 10, width, kSegmentHeight-20)];
        [button.titleLab setText:obj];
//        button.numberLab.text = @"10";
        button.numberLab.font = [UIFont systemFontOfSize:10];
        button.titleLab.font = [UIFont systemFontOfSize:12];
        button.titleLab.textColor = [UIColor p_colorWithHex:0x747474];
        button.numberLab.textColor = [UIColor p_colorWithHex:0x949494];
        [button addTarget:self action:@selector(selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [_buttons addObject:button];
        [self addSubview:button];
    }];
}
- (void)selectButtonAction:(BJTitleNumberButton *)button{
    [_buttons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BJTitleNumberButton *button = obj;
        button.titleLab.textColor = [UIColor p_colorWithHex:0x747474];
        button.numberLab.textColor = [UIColor p_colorWithHex:0x949494];
    }];
    button.titleLab.textColor = [UIColor p_colorWithHex:0xFFD946];
    button.numberLab.textColor = [UIColor p_colorWithHex:0xFFD946];
    NSInteger index = [_buttons indexOfObject:button];
    if(index == currentIndex) return;
     currentIndex = index;
    [self.delegate selectAtIndex:index];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
