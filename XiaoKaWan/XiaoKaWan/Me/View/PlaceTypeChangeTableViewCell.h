//
//  PlaceTypeChangeTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,PlaceType){
    PLACE_NORMAL,
    PLACE_SIMPLE
};
@protocol PlaceTypeChangeTableViewCellDelegate <NSObject>

- (void)changeModel:(PlaceType)placeType;

@end
@interface PlaceTypeChangeTableViewCell : UITableViewCell
@property(nonatomic) PlaceType placeType;
@property(nonatomic,strong) UIButton *changeButton;
@property(nonatomic,assign)id<PlaceTypeChangeTableViewCellDelegate>delegate;
@end
