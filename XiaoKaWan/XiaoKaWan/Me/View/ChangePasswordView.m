//
//  ChangePasswordView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangePasswordView.h"
#import "UIColor+colorWithHex.h"
@implementation ChangePasswordView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
       self.phoneNumberTextField.leftTitle = @"手机号:";
      self.verfyNumberTextField.leftTitle = @"验证码:";
        self.verfyNumberTextField.rightTitle = @"获取验证码";
        self.passwordTextField.leftTitle = @"新密码:";
        self.passwordTextField.isShowSecureButton = YES;
    }
    return self;
}
- (LeftRightViewTextField *)phoneNumberTextField{
    if(_phoneNumberTextField == nil){
        _phoneNumberTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-10, 35)];
        [self addSubview:_phoneNumberTextField];
        _phoneNumberTextField.returnKeyType = UIReturnKeyNext;
        _phoneNumberTextField.keyboardType = UIKeyboardTypePhonePad;
        _phoneNumberTextField.delegate = self;
        _phoneNumberTextField.tag = 15;
        _phoneNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _phoneNumberTextField.attStrPlaceholder = @"请输入手机号";
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_phoneNumberTextField.frame)+7.5,  CGRectGetWidth(self.frame), 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _phoneNumberTextField;
}
- (LeftRightViewTextField *)verfyNumberTextField{
    if(_verfyNumberTextField == nil){
        _verfyNumberTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_phoneNumberTextField.frame)+15, self.frame.size.width-10, 35)];
        _verfyNumberTextField.tag = 16;
        _verfyNumberTextField.textDelegate = self;
        _verfyNumberTextField.delegate = self;
        _verfyNumberTextField.returnKeyType= UIReturnKeyNext;
        _verfyNumberTextField.keyboardType =UIKeyboardTypePhonePad;
        _verfyNumberTextField.attStrPlaceholder = @"4位验证码";
        [self addSubview:_verfyNumberTextField];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_verfyNumberTextField.frame)+7.5, CGRectGetWidth(self.frame), 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _verfyNumberTextField;
}
- (LeftRightViewTextField *)passwordTextField{
    if(_passwordTextField == nil){
        _passwordTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_verfyNumberTextField.frame)+15, self.frame.size.width-10, 35)];
        _passwordTextField.tag = 17;
        _passwordTextField.delegate  =self;
        _passwordTextField.returnKeyType = UIReturnKeyDone;
        _passwordTextField.attStrPlaceholder = @"由6-16位字母,数字组成";
        [self addSubview:_passwordTextField];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_passwordTextField.frame)+7.5, CGRectGetWidth(self.frame), 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _passwordTextField;
}
#pragma mark -LeftRightViewTextFieldDelegate
- (void)verfyButtonAction:(void (^)(BOOL))isStart{
    [self.delegate verfyButtonAction:isStart];
//    if(self.verfyStart)self;
}
- (void)verfyButtonActionStop{
    [self.delegate verfyStop];
}
- (void)hiddenKeyboard{
    [_phoneNumberTextField resignFirstResponder];
    [_verfyNumberTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}
#pragma mark - delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone){
        [_passwordTextField resignFirstResponder];
    }else{
        LeftRightViewTextField *nextTextField = (LeftRightViewTextField *)[self viewWithTag:textField.tag+1];
        [nextTextField becomeFirstResponder];
    }
    return YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
