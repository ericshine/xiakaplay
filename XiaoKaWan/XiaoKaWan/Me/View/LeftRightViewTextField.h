//
//  LeftRightViewTextField.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LeftRightViewTextFieldDelegate <NSObject>
/**
 *  获取验证码
 */
- (void)verfyButtonAction:(void(^)(BOOL isStart))isStart;
/**
 *  获取验证码时间限制结束
 */
- (void)verfyButtonActionStop;
@end
@interface LeftRightViewTextField : UITextField
@property(nonatomic,strong)UILabel *leftLabel;
@property(nonatomic,strong) UIView *rightButtonView;
@property(nonatomic,strong) UIButton *rightButton;
@property(nonatomic,strong) UIButton *secureButton;
@property(nonatomic,assign) id<LeftRightViewTextFieldDelegate>textDelegate;
@property(nonatomic,assign) NSString *attStrPlaceholder;
/**
 *  左边title
 */
@property(nonatomic,strong) NSString *leftTitle;
/**
 *  是否显示密码的开关
 */
@property(nonatomic)BOOL isShowSecureButton;
/**
 *  右边获取验证码的button
 */
@property(nonatomic,copy) NSString *rightTitle;

/**
 *  外部定时器代理的方法
 */
- (void)timerAction;
@end
