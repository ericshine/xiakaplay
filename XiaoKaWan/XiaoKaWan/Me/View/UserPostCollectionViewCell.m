//
//  UserPostCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserPostCollectionViewCell.h"
//static NSInteger const KheadImagecornerRadius = 15;
@interface UserPostCollectionViewCell ()
@property(nonatomic,strong)UIView *bgView;
@end
@implementation UserPostCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.layer.cornerRadius = 10.0f;
        self.layer.masksToBounds = YES;
        self.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self setNeedsUpdateConstraints];
        UIVisualEffectView *view = [UIVisualEffectView alloc]
//        UIBlurEffect
    }
    return self;
}
- (void)setPostObject:(PostObjet *)postObject{
    BJImageObject *imageObj = postObject.latestPostFristImage;
    [self.imageView setImageWithUrlString:[ImageSizeUrl imagef480_url:imageObj.url]];
    self.addressLb.text = postObject.placeInfo.name;
    self.contentLb.text = postObject.content;
    self.commentAndLikes.text = [NSString stringWithFormat:@"\U0000e636 %li \U0000e609 %li",(long)postObject.like_count,(long)postObject.comment_count];
   if(postObject.image_count>1) self.multiImageLb.text = @"多图 ";
    else self.multiImageLb.text = @"";

}
- (NSMutableAttributedString*)getAttString:(NSString*)string{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:string];
    [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:9],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(0, 1)];
    //    [attStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x94999a]} range:NSMakeRange(1, attStr.length)];
    return attStr;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
    [self.commentAndLikes mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bgView.mas_right).offset(-5.5);
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-8);
        make.height.equalTo(@20);
    }];
//    [self.likeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.commitLabel.mas_left).offset(-7);
//        make.bottom.equalTo(self.bgView.mas_bottom).offset(-8);
//        make.height.equalTo(@20);
//       
//    }];
    [self.addressLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.commentAndLikes.mas_left).offset(-10);
         make.bottom.equalTo(self.bgView.mas_bottom).offset(-8);
        make.height.equalTo(@20);
        make.left.equalTo(self.bgView.mas_left).offset(5.5);
    }];
    [self.contentLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.commentAndLikes.mas_top).offset(-8);
        make.left.equalTo(self.bgView.mas_left).offset(5.5);
        make.right.equalTo(self.bgView.mas_right).offset(-5.5);
    }];
    [self.imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentLb.mas_top).offset(-10);
        make.top.equalTo(self.bgView.mas_top);
        make.left.equalTo(self.bgView.mas_left);
        make.right.equalTo(self.bgView.mas_right);
        
    }];
    [self.multiImageLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.imageView.mas_right);
        make.bottom.equalTo(self.imageView.mas_bottom);
//        make.height.equalTo(@14);
//        make.width.equalTo(@31);
    }];
    
}
- (UIView*)bgView{
    if(_bgView == nil){
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
//        _bgView.layer.cornerRadius = 10;
//        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}
- (BJUIImageView*)imageView
{
    if(_imageView==nil){
        _imageView = [[BJUIImageView alloc] init];
        [self.bgView addSubview:_imageView];
    }
    return _imageView;
}

- (UILabel*)addressLb
{
    if(_addressLb==nil){
        _addressLb = [UILabel creatLabelWithFont:9 color:0x999999];
        [self.bgView addSubview:_addressLb];
    }
    return _addressLb;
}

- (UILabel *)commentAndLikes{
    if(_commentAndLikes == nil){
        _commentAndLikes = [UILabel creatLabelWithFont:9 color:0x94999a];
        _commentAndLikes.font = [UIFont fontWithName:@"iconfont" size:9];
        [self.bgView addSubview:_commentAndLikes];
    }
    return _commentAndLikes;
}
- (UILabel*)contentLb
{
    if(_contentLb==nil){
        _contentLb = [UILabel creatLabelWithFont:[PostFrameObject postContentFont] color:0x666666];
        _contentLb.numberOfLines = 2;
        [self.bgView addSubview:_contentLb];
    }
    return _contentLb;
}
- (UILabel *)multiImageLb{
    if(_multiImageLb == nil){
        _multiImageLb = [UILabel creatLabelWithFont:10 color:0xffffff];
        _multiImageLb.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _multiImageLb.textAlignment = NSTextAlignmentCenter;
        _multiImageLb.layer.cornerRadius = 3;
        _multiImageLb.layer.masksToBounds = YES;
        [self.imageView addSubview:_multiImageLb];
    }
    return _multiImageLb;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
