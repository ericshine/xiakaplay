//
//  TologinTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "TologinTableViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "UIImage+Category.h"
@implementation TologinTableViewCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@2);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(58);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_top).offset(73);
        make.left.equalTo(self.contentView.mas_left).offset(55);
        make.right.equalTo(self.contentView.mas_right).offset(-55);
        make.height.equalTo(@44);
    }];
}
- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [UILabel creatLabelWithFont:15 color:0xb5b5b5];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"登录后你的榜单信息会出现在这里";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIButton *)loginButton{
    if(_loginButton == nil){
        _loginButton = [[UIButton alloc] init];
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_loginButton setTitle:@"立 即 登 录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor p_colorWithHex:0xffffff] forState:UIControlStateNormal];
        _loginButton.layer.cornerRadius= 5;
        _loginButton.layer.masksToBounds = YES;
        [_loginButton setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xffc600]] forState:UIControlStateNormal];
        [_loginButton setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xedb800]] forState:UIControlStateHighlighted];
        [_loginButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_loginButton];
    }
    return _loginButton;
}
- (UIImageView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIImageView alloc] init];
        _lineView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
- (void)buttonAction:(UIButton*)button{
    if(self.loginAction)self.loginAction();
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
