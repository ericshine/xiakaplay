//
//  AvatarView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AvatarView.h"

#import <Masonry.h>
#import "UIImage+Category.h"
@implementation AvatarView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.aratarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(15);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@280);
        make.width.equalTo(@280);
    }];
}
- (UIImageView*)aratarImageView
{
    if(_aratarImageView==nil){
        _aratarImageView = [[BJUIImageView alloc] init];
        _aratarImageView.image = [UIImage imageFromColor:[UIColor darkGrayColor]];
        [self addSubview:_aratarImageView];
    }
    return _aratarImageView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
