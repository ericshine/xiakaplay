//
//  MeHeadCollectionviewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"
#import "BJTitleNumberButton.h"
#import "BJUIImageView.h"
#import "User.h"
@protocol MeHeadCollectionviewCellDelegate <NSObject>

- (void)toFans;
- (void)toFollows;
- (void)toUserHeadImage;
@end
@interface MeHeadCollectionviewCell : BJCollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong) UIButton *followBtn;
@property(nonatomic,strong) UIButton *fansBtn;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,assign) id<MeHeadCollectionviewCellDelegate>delegate;
@property(nonatomic,strong)User *user;
@property(nonatomic,strong)UIImageView *line;
@end
