//
//  UnLoginCollectionViewCelll.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UnLoginCollectionViewCelll.h"

@implementation UnLoginCollectionViewCelll
-(void)layoutSubviews{
    [super layoutSubviews];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(19);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.height.equalTo(@181);
        make.width.equalTo(@184);
    }];
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(28);
        make.left.equalTo(self.contentView.mas_left).offset(55);
        make.right.equalTo(self.contentView.mas_right).offset(-55);
        make.height.equalTo(@44);
    }];
}
- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [UILabel creatLabelWithFont:15 color:0xb5b5b5];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"登录后你的榜单信息会出现在这里";
       // [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIButton *)loginButton{
    if(_loginButton == nil){
        _loginButton = [[UIButton alloc] init];
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_loginButton setTitle:@"立 即 登 录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor p_colorWithHex:0xffffff] forState:UIControlStateNormal];
        _loginButton.layer.cornerRadius= 5;
        _loginButton.layer.masksToBounds = YES;
        [_loginButton setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xffc600]] forState:UIControlStateNormal];
        [_loginButton setBackgroundImage:[UIImage imageFromColor:[UIColor p_colorWithHex:0xedb800]] forState:UIControlStateHighlighted];
        [_loginButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_loginButton];
    }
    return _loginButton;
}
- (UIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[UIImageView alloc] init];
        _imageView.image =[UIImage imageNamed:@"unLoginImage"];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}
- (void)buttonAction:(UIButton*)button{
    if(self.loginAction)self.loginAction();
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
