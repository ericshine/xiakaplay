//
//  SettingLogoutView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SettingLogoutViewDelegate <NSObject>

- (void)userLogout;

@end
@interface SettingLogoutView : UIView
@property(nonatomic,strong) UIButton *logoutButton;
@property(nonatomic,assign) id<SettingLogoutViewDelegate>delegate;
@end
