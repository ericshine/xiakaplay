//
//  MyPlaceListDetailCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyPlaceListDetailCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import "MyPlaceCellViewModel.h"
#import <Masonry.h>
#define collectionViewHeight 145
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface MyPlaceListDetailCell()<MyPlaceCellViewModelDelegate>
@property(nonatomic,strong)MyPlaceCellViewModel *viewModel;
@end
@implementation MyPlaceListDetailCell
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(14);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(31);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@(collectionViewHeight));
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.left.equalTo(self.contentView.mas_left).offset(21);
        make.right.equalTo(self.contentView.mas_right).offset(-21);
    }];
    [self.noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-17);
    }];
}
- (MyPlaceCellViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[MyPlaceCellViewModel alloc] initWithView:_collectionView];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:18 color:0x545454];
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000e612未名湖湖畔"];
        [attStr setAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xffdc43],NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:20]} range:NSMakeRange(0, 1)];
        _titleLabel.attributedText = attStr;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(100, collectionViewHeight);
        layout.minimumInteritemSpacing = 6;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, collectionViewHeight) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self.viewModel;
        _collectionView.dataSource = self.viewModel;
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:11 color:0x5f5f5f];
        
        _detailLabel.numberOfLines = 3;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UILabel*)noteLabel
{
    if(_noteLabel==nil){
        _noteLabel = [UILabel creatLabelWithFont:10 color:0x323232];
    
        _noteLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_noteLabel];
    }
    return _noteLabel;
}
#pragma mark - MyPlaceCellViewModelDelegate
- (void)selelctItem{
    if([self.delegate respondsToSelector:@selector(selectImageView)]){
        [self.delegate selectImageView];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
