//
//  SettingHeadCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SettingHeadCell.h"

#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#define imageWidth 57
@implementation SettingHeadCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style   reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.textLabel.font = [UIFont systemFontOfSize:14];
        self.textLabel.textColor = [UIColor p_colorWithHex:0x444444];
        self.detailTextLabel.font = [UIFont systemFontOfSize:14];
        self.detailTextLabel.textColor = [UIColor p_colorWithHex:0x444444];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(2.5);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.width.equalTo(@(imageWidth));
        make.height.equalTo(@(imageWidth));
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-0.5);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(45);
        make.height.equalTo(@0.5);
    }];
}
- (UIImageView *)rightImageView{
    if(_rightImageView == nil){
        _rightImageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_rightImageView];
    }
    return _rightImageView;
}
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
