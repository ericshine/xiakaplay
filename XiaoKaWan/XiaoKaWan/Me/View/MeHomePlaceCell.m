//
//  MeHomePlaceCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHomePlaceCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#import "Place.h"
@implementation MeHomePlaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
        
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    [self setHeadViewData:place withGroupView:self.groupBeenHeadView type:0];
    [self setHeadViewData:place withGroupView:self.groupWantHeadView type:1];
    [self layoutIfNeeded];
}
- (void)setHeadViewData:(Place*)place withGroupView:(BJGroupHeadImageView*)view type:(NSInteger)type{
    NSMutableArray *imageUrl = [NSMutableArray arrayWithCapacity:0];
    NSArray *dataArray;
    if(type ==0) {
        dataArray = place.beenUsers;
        view.totalNumber = place.beenCount;
    }
    else {
        dataArray = place.wantUsers;
        view.totalNumber = place.wantCount;
    }
    view.countLimit = 3;
    view.headViewType = HEADVIEWTYPE_SHOWNUMBER;
    [dataArray enumerateObjectsUsingBlock:^(User * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [imageUrl addObject:obj.avatar];
    }];
    [view setImageUrlArray:imageUrl];
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(23);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(20);
    }];
    NSInteger beenImageCount =_place.beenCount; //userHeadUrl.count;
    if(beenImageCount>4) beenImageCount= 4;
    CGFloat beenWidth = beenImageCount*30+8*(beenImageCount-1);
    [self.groupBeenHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(32.5);
        make.left.equalTo(self.contentView.mas_left).offset(55);
        make.height.equalTo(@30);
        make.width.equalTo(@(beenWidth));
    }];
    NSInteger wantImageCount =_place.wantCount; //userHeadUrl.count;
    if(wantImageCount>4) beenImageCount= 4;
    CGFloat wantWidth = wantImageCount*30+8*(wantImageCount-1);
    
    [self.beenButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.groupBeenHeadView.mas_bottom).offset(6.5);
        make.left.equalTo(self.contentView.mas_left).offset(47);
        make.width.equalTo(@120.5);
        make.height.equalTo(@31);
    }];
    
    [self.groupWantHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(32.5);
        make.left.equalTo(self.beenButton.mas_right).offset(43);
        make.height.equalTo(@30);
        make.width.equalTo(@(wantWidth));
    }];
    [self.wantButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.groupWantHeadView.mas_bottom).offset(6.5);
        make.left.equalTo(self.beenButton.mas_right).offset(32);
        make.width.equalTo(@120.5);
        make.height.equalTo(@31);
    }];
    [self.wantLabelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.wantButton.mas_bottom).offset(-10);
        make.left.equalTo(self.wantButton.mas_left).offset(-10);
    }];
    [self.flagLabelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.beenButton.mas_bottom).offset(-10);
        make.left.equalTo(self.beenButton.mas_left).offset(-10);
    }];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:18 color:0x545454];
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000e612未名湖湖畔"];
        [attStr setAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xffdc43],NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:20]} range:NSMakeRange(0, 1)];
        _titleLabel.attributedText = attStr;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (BJGroupHeadImageView*)groupWantHeadView
{
    if(_groupWantHeadView==nil){
        _groupWantHeadView = [[BJGroupHeadImageView alloc] init];
        _groupWantHeadView.userInteractionEnabled = YES;
        [self addGestureToView:_groupWantHeadView andSelecto:@selector(wantViewAction:)];
        [self.contentView addSubview:_groupWantHeadView];
    }
    return _groupWantHeadView;
}
- (BJGroupHeadImageView*)groupBeenHeadView
{
    if(_groupBeenHeadView==nil){
        _groupBeenHeadView = [[BJGroupHeadImageView alloc] init];
        _groupBeenHeadView.userInteractionEnabled = YES;
        [self addGestureToView:_groupBeenHeadView andSelecto:@selector(beenViewAction:)];
        [self.contentView addSubview:_groupBeenHeadView];
    }
    return _groupBeenHeadView;
}

- (UILabel*)flagLabelIcon
{
    if(_flagLabelIcon==nil){
        _flagLabelIcon = [UILabel creatLabelWithFont:40 color:0xffffff];
        _flagLabelIcon.textColor = [UIColor redColor];
        _flagLabelIcon.font = [UIFont fontWithName:@"iconfont" size:45];
        _flagLabelIcon.userInteractionEnabled = YES;
        //        _flagLabelIcon.text = @"\U0000e604";
        [self addSubview:_flagLabelIcon];
    }
    return _flagLabelIcon;
}
- (UILabel*)wantLabelIcon
{
    if(_wantLabelIcon==nil){
        _wantLabelIcon = [UILabel creatLabelWithFont:40 color:0x66ccff];
        _wantLabelIcon.font = [UIFont fontWithName:@"iconfont" size:45];
        _wantLabelIcon.userInteractionEnabled = YES;
        //        _wantLabelIcon.text = @"\U0000e60a";
        [self addSubview:_wantLabelIcon];
    }
    return _wantLabelIcon;
}
- (UIButton*)beenButton
{
    if(_beenButton==nil){
        _beenButton = [self creatButton];
        _beenButton.tag = 40;
        [_beenButton setTitle:@"去过" forState:UIControlStateNormal];
        [self.contentView addSubview:_beenButton];
    }
    return _beenButton;
}
- (UIButton*)wantButton
{
    if(_wantButton==nil){
        _wantButton = [self creatButton];
        _wantButton.tag = 41;
        [_wantButton setTitle:@"想去" forState:UIControlStateNormal];
        [self.contentView addSubview:_wantButton];
    }
    return _wantButton;
}
- (UIButton *)creatButton{
    UIButton *button = [[UIButton alloc] init];
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor p_colorWithHex:0xbbbbbb].CGColor;
    [button setTitleColor:[UIColor p_colorWithHex:0x545454] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    button.layer.cornerRadius = 5;
    [button addTarget:self action:@selector(wantOrBeenAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
/**
 *  Description
 *
 *  @param tap 点击想去的头像
 */
- (void)wantViewAction:(UITapGestureRecognizer *)tap{
    if([self.delegate respondsToSelector:@selector(showUserListWithData:)]){
        [self.delegate showUserListWithData:self.place.wantUsers];
    }
}
/**
 *  Description
 *
 *  @param tap 点击去过的头像
 */
- (void)beenViewAction:(UITapGestureRecognizer *)tap{
    if([self.delegate respondsToSelector:@selector(showUserListWithData:)]){
        [self.delegate showUserListWithData:self.place.beenUsers];
    }
}
- (void)addGestureToView:(id)view andSelecto:(SEL)selector{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:selector];
    [view addGestureRecognizer:gesture];
}
- (void)wantOrBeenAction:(UIButton *)button{
    
    if(button.tag ==40){
        if(button.selected){
            [self disSelectAnimation:self.flagLabelIcon];
        }else{
            self.flagLabelIcon.text = @"\U0000e604";
            
            [self selectAnimation:self.flagLabelIcon];
        }
    }else{
        if(button.selected){
            [self disSelectAnimation:_wantLabelIcon];
        }else{
            [self selectAnimation:_wantLabelIcon];
        }
        _wantLabelIcon.text = @"\U0000e60a";
        
    }
    button.selected=!button.selected;
}
- (void)selectAnimation:(UILabel *)label{
    [UIView animateWithDuration:0.3 animations:^{
         label.hidden = NO;
        label.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
        label.transform = CGAffineTransformIdentity;
        }];
        
    }];
//    CGAffineTransform t = CGAffineTransformMakeTranslation(200, 200);
//    label.transform = CGAffineTransformScale(t, 10, 10);
//    [UIView animateWithDuration:0.6 animations:^{
//        CGAffineTransform t = CGAffineTransformMakeTranslation(150, -200);
//        label.transform = CGAffineTransformScale(t, 10, 10);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.4 animations:^{
//            label.transform = CGAffineTransformIdentity;
//        }];
//    }];
}
- (void)disSelectAnimation:(UILabel *)label{
    [UIView animateWithDuration:0.3 animations:^{
        label.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
             label.transform = CGAffineTransformMakeScale(0.2, 0.2);
            label.hidden = YES;
        }];
        
    }];
//    [UIView animateWithDuration:0.4 animations:^{
//        label.layer.transform = CATransform3DMakeRotation(600, 0, 0, 3);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:1 animations:^{
//            CGAffineTransform t = CGAffineTransformMakeTranslation(1000, 100);
//            label.transform = CGAffineTransformScale(t, 0.2, 0.2);
//        }];
//    }];
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
