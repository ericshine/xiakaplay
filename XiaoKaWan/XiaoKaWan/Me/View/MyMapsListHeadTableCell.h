//
//  MyMapsListHeadTableCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
@interface MyMapsListHeadTableCell : UITableViewCell
@property(nonatomic,strong)UILabel *numberLabel;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,strong) UIImageView *lineView;
@end
