//
//  PlaceListTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceListTableViewCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import "MapsPlaceListCellViewModel.h"
#import <Masonry.h>
#import "PlaceFrameObject.h"
#import "NSString+Cagetory.h"
#import <MJExtension.h>
#import "MapsUser.h"
#import "User.h"
#define collectionViewHeight 145
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface PlaceListTableViewCell()<MapsPlaceListCellViewModelDelegate>
@property(nonatomic,strong)MapsPlaceListCellViewModel *viewModel;
@property(nonatomic,strong)UIView *bgView;
@end
@implementation PlaceListTableViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self bgView];
    // [self collectionView];
        self.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setPlaceObject:(MyPlaceObject *)placeObject{
    self.titleLabel.text = placeObject.name;
//    _viewModel.place = [self toPlace:placeObject];
    self.beenWantView.place = [self toPlace:placeObject];
//    [self tagLabel];
    ;
}
- (Place *)toPlace:(MyPlaceObject*)placeObject{
    Place *place = [[Place alloc] init];
    place.id = [placeObject.id integerValue];
    place.placeId = [NSString stringWithFormat:@"%@",placeObject.id];
    place.isBeen =[placeObject.isBeen boolValue];
    place.isWant = [placeObject.isWant boolValue];
    place.wantCount = [placeObject.want_count integerValue];
    place.beenCount = [placeObject.been_count integerValue];
    place.id = [placeObject.id integerValue];
    
    NSArray *beenUser = [MapsUser beenUserPlaceId:[placeObject.id integerValue]];
    NSArray *wantUser = [MapsUser wantUserPlaceId:[placeObject.id integerValue]];
    NSMutableArray *beenUserArray = [[NSMutableArray alloc] initWithCapacity:beenUser.count];
    NSMutableArray *wantUserArray = [[NSMutableArray alloc] initWithCapacity:wantUser.count];
    [beenUser enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *userObj = obj;
//        NSDictionary *dic = userObj.keyValues;
        User *user = [[User alloc] init];
        user.avatar = userObj.avatar;
        user.nick = userObj.nick;
        user.uid = [userObj.uid integerValue];
        [beenUserArray addObject:user];
    }];
    [wantUser enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         MapsUser *userObj = obj;
//        NSDictionary *dic = userObj.keyValues;
//        User *user = [User objectWithKeyValues:dic];
        User *user = [[User alloc] init];
        user.avatar = userObj.avatar;
        user.nick = userObj.nick;
        user.uid = [userObj.uid integerValue];
        [wantUserArray addObject:user];
    }];
    place.beenUsers = [beenUserArray copy];
    place.wantUsers = [wantUserArray copy];
    return place;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(25);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
//    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView.mas_top).offset(5);
//        make.left.equalTo(self.contentView.mas_left).offset(5);
//        make.width.equalTo(@50);
//        make.height.equalTo(@50);
//    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(14);
        make.left.equalTo(self.bgView.mas_left).offset(15);
        make.right.equalTo(self.bgView.mas_right).offset(15);
    }];
//    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.titleLabel.mas_bottom).offset(31);
//        make.left.equalTo(self.bgView.mas_left).offset(10);
//        make.right.equalTo(self.bgView.mas_right).offset(-10);
//        make.height.equalTo(@(collectionViewHeight));
//    }];
        [self.beenWantView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-20);
        make.left.equalTo(self.bgView.mas_left).offset(5);
        make.right.equalTo(self.bgView.mas_right).offset(-5);
        make.height.equalTo(@30);
    }];
    
}

//- (MapsPlaceListCellViewModel *)viewModel{
//    if(_viewModel == nil){
//        _viewModel = [[MapsPlaceListCellViewModel alloc] initWithView:_collectionView];
//        _viewModel.delegate = self;
//    }
//    return _viewModel;
//}
- (UIView *)bgView{
    if(_bgView == nil){
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 5;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}
//- (UIImageView*)tagLabel
//{
//    if(_tagLabel==nil){
//        _tagLabel = [[UIImageView alloc] init];
//        _tagLabel.image = [UIImage imageNamed:@"bangdanIcon"];
//        [self.contentView addSubview:_tagLabel];
//    }
//    return _tagLabel;
//}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:18 color:0x545454];
        //        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000e612未名湖湖畔"];
        //        [attStr setAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xffdc43],NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:20]} range:NSMakeRange(0, 1)];
        //        _titleLabel.attributedText = attStr;
        [self.bgView addSubview:_titleLabel];
    }
    return _titleLabel;
}
//- (UICollectionView *)collectionView{
//    if(_collectionView == nil){
//        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//        layout.itemSize = CGSizeMake(100, collectionViewHeight);
//        layout.minimumInteritemSpacing = 6;
//        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, collectionViewHeight) collectionViewLayout:layout];
//        _collectionView.backgroundColor = [UIColor whiteColor];
//        _collectionView.delegate = self.viewModel;
//        _collectionView.dataSource = self.viewModel;
//        [self.bgView addSubview:_collectionView];
//    }
//    return _collectionView;
//}
//

- (BeenWantView*)beenWantView
{
    if(_beenWantView==nil){
        _beenWantView = [[BeenWantView alloc] init];
        _beenWantView.delegate = self;
        [self.bgView addSubview:_beenWantView];
    }
    return _beenWantView;
}

#pragma mark - MapsPlaceListCellViewModelDelegate
- (void)selelctItem:(Place *)place{
    if([self.delegate respondsToSelector:@selector(selectImageViewWithPlace:)]){
        [self.delegate selectImageViewWithPlace:place];
    }
}
#pragma mark - BeenWantViewDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate wantAction:place callBack:place1];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
