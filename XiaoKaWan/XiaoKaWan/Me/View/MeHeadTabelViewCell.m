//
//  MeHeadCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHeadTabelViewCell.h"
#import "UILabel+initLabel.h"

#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@interface MeHeadTabelViewCell ()
@property(nonatomic,strong)UIImageView *lineView;
@end

@implementation MeHeadTabelViewCell
- (void)setUser:(CurrentUser *)user{
    self.titleLabel.text = user.nick;
    self.detailLabel.text = user.sign;
    [self.headImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar280:user.avatar]];
    NSInteger placeCount = [user.been_count integerValue] + [user.want_count integerValue];
    self.segmentView.number1 = [NSString stringWithFormat:@"%ld",(long)placeCount];
    self.segmentView.number2 = [NSString stringWithFormat:@"%@",user.maps_count];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(34);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-165);
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(18);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-165);
    }];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(26);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@137);
        make.width.equalTo(@137);
    }];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.height.equalTo(@SEGMENTHEIGHT);
        make.width.equalTo(@SEGMENTWIDTH);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@2);
    }];
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:10 color:0x2c2c2c];
        _detailLabel.text = @"吃喝玩乐Follow me";
        _detailLabel.numberOfLines = 0;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:26 color:0x555555];
        _titleLabel.text = @"小咖玩";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (MapsPlaceSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView = [[MapsPlaceSegmentView alloc] initWithFrame:CGRectZero WithTitles:@[@"榜单",@"地点"]];
        __weak typeof(self) weakSelf=  self;
        _segmentView.segmentIndex = ^(NSInteger index){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if(index==0){
                [strongSelf.delegate mapsTouchAction];
            }
            else [strongSelf.delegate placeTouchAction];
        };
        [self.contentView addSubview:_segmentView];
    }
    return _segmentView;
}
//- (BJTitleNumberButton *)placeButton{
//    if(_placeButton == nil){
//        _placeButton = [[BJTitleNumberButton alloc] init];
//        _placeButton.tag = 30;
//        _placeButton.titleLab.text = @"地点";
//        _placeButton.titleLab.textColor = [UIColor p_colorWithHex:0x3d3d3d];
//        _placeButton.numberLab.textColor = [UIColor p_colorWithHex:0x585858];
//        [_placeButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:_placeButton];
//    }
//    return _placeButton;
//}
//- (BJTitleNumberButton *)mapsButton{
//    if(_mapsButton == nil){
//        _mapsButton = [[BJTitleNumberButton alloc] init];
//        _mapsButton.tag = 31;
//        _mapsButton.titleLab.text = @"榜单";
//        _mapsButton.titleLab.textColor = [UIColor p_colorWithHex:0x3d3d3d];
//        _mapsButton.numberLab.textColor = [UIColor p_colorWithHex:0x585858];
//        [_mapsButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:_mapsButton];
//    }
//    return _mapsButton;
//}
- (void)buttonAction:(BJTitleNumberButton*)button{
    NSInteger tag = button.tag ;
    switch (tag) {
        case 30:
            if([self.delegate respondsToSelector:@selector(placeTouchAction)]){
                [self.delegate placeTouchAction];
            }
            break;
        case 31:
            if([self.delegate respondsToSelector:@selector(mapsTouchAction)]){
                [self.delegate mapsTouchAction];
            }
            break;
            
        default:
            break;
    }
}
- (BJUIImageView *)headImageView{
    if(_headImageView == nil){
        _headImageView = [[BJUIImageView alloc] init];
//        [_headImageView setImageWithUrlString:@"http://vimg3.ws.126.net/image/snapshot/2016/5/5/S/VBLI6CF5.jpg"];
        [self.contentView addSubview:_headImageView];
    }
    return _headImageView;
}
- (UIImageView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIImageView alloc] init];
        _lineView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
@end
