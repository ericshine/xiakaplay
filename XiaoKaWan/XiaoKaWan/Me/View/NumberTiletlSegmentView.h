//
//  NumberTiletlSegmentView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#import "BJTitleNumberButton.h"
#define kSegmentHeight 50
@protocol NumberTiletlSegmentViewDelegate <NSObject>

- (void)selectAtIndex:(NSInteger)index;

@end
@interface NumberTiletlSegmentView : BJView
@property(nonatomic,copy) NSString *Album;
@property(nonatomic) NSInteger defaultIndex;
@property(nonatomic,strong) NSMutableArray *buttons;// 用来获取button 来修改 number 的值
@property(nonatomic,assign) id<NumberTiletlSegmentViewDelegate>delegate;
- (instancetype)initWithFrame:(CGRect)frame withTitles:(NSArray *)titles;
@end

