//
//  UseSegmentCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"
#import "NumberTiletlSegmentView.h"
#import "User.h"
@protocol UserSegmentCollectionViewCellDelegate <NSObject>

- (void)selectAtIndex:(NSInteger)index;

@end
@interface UserSegmentCollectionViewCell : BJCollectionViewCell<NumberTiletlSegmentViewDelegate>
@property(nonatomic,strong)NumberTiletlSegmentView *segmentView;
@property(nonatomic,strong)id<UserSegmentCollectionViewCellDelegate>delegate;
@property(nonatomic,strong)User *user;
@end

