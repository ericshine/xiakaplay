//
//  UserPlaceCollectioncell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserPlaceCollectioncell.h"
#define collectionViewHeight 145
@interface UserPlaceCollectioncell()
@property(nonatomic,strong)UIView *bgView;
@end
@implementation UserPlaceCollectioncell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self bgView];
        self.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    self.titleLabel.text = place.name;
    self.beenWantView.place = place;
    self.addressLb.text = place.addressDetail;
}

- (void)updateConstraints{
    [super updateConstraints];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(25);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(14);
        make.left.equalTo(self.bgView.mas_left).offset(15);
        make.right.equalTo(self.bgView.mas_right).offset(15);
    }];
    [self.addressLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(13);
        make.left.equalTo(self.bgView.mas_left).offset(15);
        make.right.equalTo(self.bgView.mas_right).offset(-15);
    }];
    
    [self.beenWantView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-20);
        make.left.equalTo(self.bgView.mas_left).offset(5);
        make.right.equalTo(self.bgView.mas_right).offset(-5);
        make.height.equalTo(@30);
    }];
    
}


- (UIView *)bgView{
    if(_bgView == nil){
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 5;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:18 color:0x545454];

        [self.bgView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)addressLb
{
    if(_addressLb==nil){
        _addressLb = [UILabel creatLabelWithFont:11 color:0x666666];
        
        [self.bgView addSubview:_addressLb];
    }
    return _addressLb;
}
- (BeenWantView*)beenWantView
{
    if(_beenWantView==nil){
        _beenWantView = [[BeenWantView alloc] init];
        _beenWantView.delegate = self;
        [self.bgView addSubview:_beenWantView];
        self.bgView.userInteractionEnabled = YES;
        self.contentView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userListtapGesture:)];
        [_beenWantView addGestureRecognizer:tap];
    }
    return _beenWantView;
}
- (void)userListtapGesture:(UITapGestureRecognizer*)gesture{
    [self.delegate showUserList:self.place];
}
#pragma mark - BeenWantViewDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate wantAction:place callBack:place1];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
