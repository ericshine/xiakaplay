//
//  UserlistHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "TabelHeadTitleView.h"

@implementation TabelHeadTitleView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@1);
    }];
    
}

- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [UILabel creatLabelWithFont:12 color:0x8c8c8c];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
        [self addSubview:_lineView];
    }
    return _lineView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
