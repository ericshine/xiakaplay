//
//  MeSegmentHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeSegmentHeadView : UIView
@property(nonatomic,strong)UISegmentedControl*segmentController;
@property(nonatomic,copy) void(^selectAtIndex)(NSInteger index);
@end
