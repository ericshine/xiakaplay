//
//  UserPlaceCollectioncell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"
#import "Place.h"
#import "BeenWantView.h"
#import "MyPlaceObject.h"
@protocol UserPlaceCollectioncellDelegate <NSObject>

- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)showUserList:(Place *)place;
@end
@interface UserPlaceCollectioncell : BJCollectionViewCell<BeenWantViewDelegate>
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *addressLb;
@property(nonatomic,strong)BeenWantView *beenWantView;
@property(nonatomic,assign)id<UserPlaceCollectioncellDelegate>delegate;
@property(nonatomic,strong)Place *place;
//@property(nonatomic,strong)MyPlaceObject *placeObject;
@property(nonatomic,strong)NSIndexPath *indexPath;
@end
