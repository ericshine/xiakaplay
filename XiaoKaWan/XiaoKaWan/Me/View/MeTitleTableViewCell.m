//
//  MeTitleTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeTitleTableViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"

@implementation MeTitleTableViewCell
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@3);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(20);
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(self.titleLabel.mas_height);
        make.width.equalTo(@100);
    }];
}
- (void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:15 color:0x3e3e3e];
        _titleLabel.text = @"";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIButton*)moreButton
{
    if(_moreButton==nil){
        _moreButton = [[UIButton alloc] init];
        _moreButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_moreButton setTitle:@"更多>" forState:UIControlStateNormal];
        [_moreButton setTitleColor:[UIColor p_colorWithHex:0x717171] forState:UIControlStateNormal];
        [_moreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [_moreButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_moreButton];
    }
    return _moreButton;
}
- (UIImageView*)lineView{
    if(_lineView == nil){
        _lineView = [[UIImageView alloc] init];
        _lineView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
- (void)buttonAction:(UIButton *)button{
    if([self.delegate respondsToSelector:@selector(moreAction:)]){
        [self.delegate moreAction:self.moreType];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
