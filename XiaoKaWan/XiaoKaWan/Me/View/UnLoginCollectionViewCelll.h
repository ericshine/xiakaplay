//
//  UnLoginCollectionViewCelll.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"

@interface UnLoginCollectionViewCelll : BJCollectionViewCell
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIButton *loginButton;
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,copy) void(^loginAction)();
@end
