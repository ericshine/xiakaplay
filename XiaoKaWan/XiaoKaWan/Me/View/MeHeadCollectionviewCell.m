//
//  MeHeadCollectionviewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHeadCollectionviewCell.h"
#define kHeadImageHeight 134
@implementation MeHeadCollectionviewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setUser:(User *)user{
    self.titleLabel.text = user.nick;
    self.detailLabel.text = user.sign;
    [_followBtn setTitle:[NSString stringWithFormat:@"关注:%@",user.following_count] forState:UIControlStateNormal];
    [_fansBtn setTitle:[NSString stringWithFormat:@"粉丝:%@",user.followers_count] forState:UIControlStateNormal];
    [self.headImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar280:user.avatar]];
}
- (void)updateConstraints{
    [super updateConstraints];

    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(36);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.width.equalTo(@(kDeviceWidth-kHeadImageHeight-40));
    }];
    [self.followBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        make.left.equalTo(self.contentView.mas_left).offset(20);
    }];
    [self.fansBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        make.left.equalTo(self.followBtn.mas_right).offset(20);
    }];
    [self.headImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-40);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
        make.height.equalTo(@kHeadImageHeight);
        make.width.equalTo(@kHeadImageHeight);
    }];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.followBtn.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.width.equalTo(@(kDeviceWidth-kHeadImageHeight-40));
    }];
    [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@1);
    }];
}
- (UIImageView*)line{
    if(_line == nil){
        _line = [[UIImageView alloc] init];
        _line.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_line];
    }
    return _line;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:12 color:0x2c2c2c];
        _detailLabel.text = @"吃喝玩乐Follow me";
        _detailLabel.numberOfLines = 0;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:24 color:0x555555];
        _titleLabel.text = @"小咖玩";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (BJUIImageView *)headImageView{
    if(_headImageView == nil){
        _headImageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userHeadImage)];
        [_headImageView addGestureRecognizer:tapGesture];
        _headImageView.userInteractionEnabled = YES;
        
    }
    return _headImageView;
}
- (void)userHeadImage{
    [self.delegate toUserHeadImage];
}
- (UIButton *)followBtn{
    if(_followBtn == nil){
        _followBtn = [self creatButton];
        [_followBtn setTitle:@"关注:0" forState:UIControlStateNormal];
        [_followBtn addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_followBtn];
    }
    return _followBtn;
}
- (UIButton *)fansBtn{
    if(_fansBtn == nil){
        _fansBtn = [self creatButton];
        [_fansBtn setTitle:@"粉丝:0" forState:UIControlStateNormal];
        [_fansBtn addTarget:self action:@selector(fansAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_fansBtn];
    }
    return _fansBtn;
}
- (void)followAction:(UIButton *)button{
    [self.delegate toFollows];
}
- (void)fansAction:(UIButton*)button{
    [self.delegate toFans];
}
- (UIButton *)creatButton{
    UIButton *button = [[UIButton alloc] init];
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    [button setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
    return button;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
