//
//  UserlistHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface TabelHeadTitleView : BJView
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIView *lineView;
@end
