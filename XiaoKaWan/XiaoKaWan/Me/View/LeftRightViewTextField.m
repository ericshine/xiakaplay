//
//  LeftRightViewTextField.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LeftRightViewTextField.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import "NSString+Cagetory.h"
#define limitTime  59
@implementation LeftRightViewTextField{
    NSInteger timeCount;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.font = [UIFont systemFontOfSize:16];
        self.textColor = [UIColor p_colorWithHex:0x444444];
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        timeCount = limitTime;
    }
    return self;
}
- (void)setLeftTitle:(NSString *)leftTitle{
    self.leftLabel.text = leftTitle;
    CGRect rect = self.leftLabel.frame;
    rect.size.width = [leftTitle widthWithFont:self.leftLabel.font]+20;
    self.leftLabel.frame = rect;
}
- (void)setAttStrPlaceholder:(NSString *)attStrPlaceholder{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:attStrPlaceholder];
    [attStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xb5b5b5]} range:NSMakeRange(0, attStr.length)];
    self.attributedPlaceholder = attStr;
    self.adjustsFontSizeToFitWidth = YES;
}
- (void)setIsShowSecureButton:(BOOL)isShowSecureButton{
    if(isShowSecureButton == YES)[self secureButton];
}
- (void)setRightTitle:(NSString *)rightTitle{
  [self.rightButton setTitle:rightTitle forState:0];
}
- (UIButton*)secureButton
{
    if(_secureButton==nil){
        self.secureTextEntry = YES;
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _secureButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _secureButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:18];
        [_secureButton setTitle:@"\U0000e60f" forState:UIControlStateNormal];
        [_secureButton setTitle:@"\U0000e610" forState:UIControlStateSelected];
        [_secureButton setTitleColor:[UIColor p_colorWithHex:0x444444] forState:UIControlStateNormal];
        [_secureButton setTitleColor:[UIColor p_colorWithHex:0x444444] forState:UIControlStateSelected];
        [_secureButton addTarget:self action:@selector(secureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        self.rightViewMode = UITextFieldViewModeAlways;
        [rightView addSubview:_secureButton];
        self.rightView = rightView;
    }
    return _secureButton;
}
- (UILabel*)leftLabel
{
    if(_leftLabel==nil){
        _leftLabel = [UILabel creatLabelWithFont:16 color:0x444444];
        _leftLabel.frame = CGRectMake(0, 0, 70, self.frame.size.height);
        self.leftViewMode = UITextFieldViewModeAlways;
        self.leftView = _leftLabel;
    }
    return _leftLabel;
}
- (UIView *)rightButtonView{
    if(_rightButtonView == nil){
        _rightButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, self.frame.size.height)];
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = _rightButtonView;
    }
    return _rightButtonView;
}
- (UIButton *)rightButton{
    if(_rightButton == nil){
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, self.frame.size.height)];
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_rightButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_rightButton setTitleColor:[UIColor p_colorWithHex:0x444444] forState:UIControlStateNormal];
        _rightButton.layer.cornerRadius = 5;
        _rightButton.layer.borderWidth = 1;
        _rightButton.layer.borderColor = [UIColor p_colorWithHex:0xb5b5b5].CGColor;
        [_rightButton addTarget:self action:@selector(verfyAciton) forControlEvents:UIControlEventTouchUpInside];
        [self.rightButtonView addSubview:_rightButton];
    }
    return _rightButton;
}
- (void)secureButtonAction:(UIButton *)button{
    button.selected = !button.selected;
    if(button.selected) self.secureTextEntry = NO;
    else self.secureTextEntry = YES;
}
- (void)verfyAciton{
    [_rightButton setTitle:@"获取中.." forState:UIControlStateNormal];
    if([self.textDelegate respondsToSelector:@selector(verfyButtonAction:)]){
        [self.textDelegate verfyButtonAction:^(BOOL isStart) {
            if(isStart){[_rightButton setTitle:[NSString stringWithFormat:@"%li秒",(long)timeCount] forState:UIControlStateNormal];
            _rightButton.enabled = NO;
            }else{
                [_rightButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            }
        }];
    }
    
}
- (void)timerAction{
    timeCount --;
    if(timeCount<=0){
        if([self.textDelegate respondsToSelector:@selector(verfyButtonActionStop)]){
            [self.textDelegate verfyButtonActionStop];
            _rightButton.enabled = YES;
        }
        timeCount = limitTime;
        [_rightButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        return;
    }
    [_rightButton setTitle:[NSString stringWithFormat:@"%li秒",(long)timeCount] forState:UIControlStateNormal];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
