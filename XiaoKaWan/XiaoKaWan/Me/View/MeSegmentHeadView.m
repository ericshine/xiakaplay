//
//  MeSegmentHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeSegmentHeadView.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation MeSegmentHeadView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.segmentController mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.right.equalTo(self.mas_right).offset(-20);
        make.width.equalTo(@120);
    }];
}
- (UISegmentedControl *)segmentController{
    if(_segmentController == nil){
        _segmentController = [[UISegmentedControl alloc] initWithItems:@[@"去过",@"想去"]];
        [_segmentController setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x2c2c2c]} forState:UIControlStateNormal];
        [_segmentController setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x2c2c2c]} forState:UIControlStateSelected];
        [_segmentController setTintColor:[UIColor p_colorWithHex:0xffe028]];
        _segmentController.selectedSegmentIndex = 0;
        [_segmentController addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_segmentController];
    }
    return _segmentController;
}
- (void)selectAction:(UISegmentedControl *)control{
    if(self.selectAtIndex)self.selectAtIndex(control.selectedSegmentIndex);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
