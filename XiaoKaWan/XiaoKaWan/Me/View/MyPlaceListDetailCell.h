//
//  MyPlaceListDetailCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyPlaceListDetailCellDelegate <NSObject>

- (void)selectImageView;

@end
@interface MyPlaceListDetailCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)UILabel *noteLabel;
@property(nonatomic,assign)id<MyPlaceListDetailCellDelegate>delegate;
@end
