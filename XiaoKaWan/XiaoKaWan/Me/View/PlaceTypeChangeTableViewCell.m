//
//  PlaceTypeChangeTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceTypeChangeTableViewCell.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation PlaceTypeChangeTableViewCell
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-17);
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.width.equalTo(@54);
        make.height.equalTo(@54);
    }];
    
}
- (UIButton *)changeButton{
    if(_changeButton == nil){
        _changeButton = [[UIButton alloc] init];
        _changeButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:30];
        [_changeButton setTitle:@"\U0000e608" forState:UIControlStateNormal];
        [_changeButton setTitle:@"\U0000e606" forState:UIControlStateSelected];
        [_changeButton setTitleColor:[UIColor p_colorWithHex:0xfbe47a] forState:UIControlStateNormal];
        [_changeButton setTitleColor:[UIColor p_colorWithHex:0xfbe47a] forState:UIControlStateSelected];
        [_changeButton addTarget:self action:@selector(changButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_changeButton];
    }
    return _changeButton;
}

- (void)changButtonAction:(UIButton *)button{
    button.selected = !button.selected;
    if(button.selected){
        [self.delegate changeModel:PLACE_NORMAL];
    }else [self.delegate changeModel:PLACE_SIMPLE];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
