//
//  MeHeadCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJTitleNumberButton.h"
#import "BJUIImageView.h"
#import "CurrentUser.h"
#import "MapsPlaceSegmentView.h"
@protocol MeHeadTabelViewCellDelegate <NSObject>

- (void)placeTouchAction;
- (void)mapsTouchAction;

@end
@interface MeHeadTabelViewCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)MapsPlaceSegmentView *segmentView;
//@property(nonatomic,strong)BJTitleNumberButton *placeButton;
//@property(nonatomic,strong)BJTitleNumberButton *mapsButton;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,assign) id<MeHeadTabelViewCellDelegate>delegate;
@property(nonatomic,strong)CurrentUser *user;
@end
