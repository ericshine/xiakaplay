//
//  SettingObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CurrentUser;
@interface SettingObject : NSObject
@property(nonatomic,strong)NSArray *listArray;
- (instancetype)initWith:(id)model;
- (void)setData:(CurrentUser*)user;
- (void)clearCache;
- (NSString *)getCacheSize;
@end
