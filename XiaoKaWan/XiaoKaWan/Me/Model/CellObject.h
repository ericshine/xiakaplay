//
//  CellObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CellObject : NSObject
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *detail;
@property(nonatomic,copy) NSString *urlString;
@end
