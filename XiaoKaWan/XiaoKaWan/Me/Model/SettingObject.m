//
//  SettingObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SettingObject.h"
#import "CellObject.h"
#import "CurrentUser.h"
#import "ImageSizeUrl.h"
#import <UIImageView+WebCache.h>
@interface SettingObject()
@property(nonatomic,strong)NSArray *titleArray;
@end
@implementation SettingObject
- (instancetype)initWith:(id)model{
    self = [super init];
    if(self){
        CurrentUser *user = model;
        [self setData:user];
    }
    return self;
}
- (void)setData:(CurrentUser*)user{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SettingList" ofType:@"plist"];
    self.titleArray = [NSArray arrayWithContentsOfFile:path];
    NSArray *details = @[user.avatar.length?user.avatar:@"",user.nick.length?user.nick:@"",user.sign.length?user.sign:@"",@"",@"",@"",[self getCacheSize]];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.titleArray.count];
    [self.titleArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CellObject *object = [[CellObject alloc] init];
        object.title = obj;
        object.detail = details[idx];
        [items addObject:object];
    }];
    self.listArray = [items copy];
}
- (NSString *)getCacheSize{
    long long sumSize = [[SDImageCache sharedImageCache] getSize];
//    NSString *cacheFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
//    NSFileManager *filemanager = [NSFileManager defaultManager];
//    NSArray *subPaths = [filemanager subpathsOfDirectoryAtPath:cacheFilePath error:nil];
//     NSLog(@"path1:%@",subPaths);
//    [self applicationDocumentsDirectory];
//    for (NSString *subPath in subPaths) {
//        NSString *filePath = [cacheFilePath stringByAppendingFormat:@"/%@",subPath];
//       
//        long long fileSize = [[filemanager attributesOfItemAtPath:filePath error:nil]fileSize];
//        sumSize += fileSize;
//    }
    float size_m = sumSize/(1024.0*1024.0);
    NSLog(@"size:%f",size_m);
    return [NSString stringWithFormat:@"%.2fM",size_m];
}
- (NSURL *)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSFileManager *filemanager = [NSFileManager defaultManager];
    NSArray *subPaths = [filemanager subpathsOfDirectoryAtPath:paths[0] error:nil];
    NSLog(@"path2:%@",subPaths);
    return paths.lastObject;
}
- (void)clearCache{
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] cleanDisk];
    [[SDImageCache sharedImageCache] clearDisk];
//  NSFileManager *filemanager = [NSFileManager defaultManager];
//    NSString *cacheFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
//    [filemanager removeItemAtPath:cacheFilePath error:nil];
    
}
@end
