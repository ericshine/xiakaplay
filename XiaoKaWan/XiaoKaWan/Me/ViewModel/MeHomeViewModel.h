//
//  MeHomeViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"

/**
 暂时弃用的类
 
 - returns: return value description
 */
@interface MeHomeViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
- (instancetype)initWithModel:(id)model tableView:(UITableView *)tableView viewController:(BJSuperViewController *)viewController;
- (void)userInformationChanged:(NSNotification *)noti;
@end
