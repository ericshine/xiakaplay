//
//  UserinforViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentUser.h"
#import "MeHeadCollectionviewCell.h"
#import "BJMapsListCollectionLayout.h"
#import "UserSegmentCollectionViewCell.h"
#import "MapsCollectionViewCell.h"
#import "UserPostCollectionViewCell.h"
#import "UserPlaceCollectioncell.h"
#import "BJRefreshGitFooter.h"
#import "BJRefreshGitHeader.h"
UIKIT_EXTERN NSString *const kMeHeadCollectionviewCell;
UIKIT_EXTERN NSString *const kUserSegmentCollectionViewCell;
UIKIT_EXTERN NSString *const kMapsCollectionViewCell;
UIKIT_EXTERN NSString *const kUserPostCollectionViewCell;
UIKIT_EXTERN NSString *const kUserPlaceCollectioncell;
@protocol UserinforViewModelDelegate <NSObject>
/**
 *  展示内容选择
 *
 *  @param index index description
 */
- (void)selectAtIndex:(NSUInteger)index;
- (void)toFans;
- (void)toFollows;
- (void)toUserHeadImage;
@end

/**
 *  用于不同模块的数据处理
 */
@protocol UserinforViewModelDataSource <NSObject>

@required

- (void)getDataFooter:(BOOL)footer withCollectionView:(UICollectionView*)collectionView WithUser:(User*)user;
- (CGFloat)widthForItemInCollectionView:(UICollectionView *)collectionView
                                 layout:(BJMapsListCollectionLayout *)collectionViewLayout;
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(BJMapsListCollectionLayout *)collectionViewLayout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfItemsInCollectionView:(UICollectionView *)collectionView;

- (UICollectionViewCell *)cellForItemAtCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexRow:(NSInteger)row;

@end

@interface UserinforViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource,BJMapsListCollectionDelegate,UserSegmentCollectionViewCellDelegate,MeHeadCollectionviewCellDelegate>
@property(nonatomic,assign) id<UserinforViewModelDelegate>delegate;
@property(nonatomic,assign) id<UserinforViewModelDataSource>dataSourceDelegete;
@property(nonatomic,strong) User *user;
@property(nonatomic,strong) UICollectionView *collcetionView;
@property(nonatomic,strong) BJMapsListCollectionLayout *layout;
@property(nonatomic,copy) void(^loadUserInf)(User *user);
//@property(nonatomic,strong)UerHeadCollectionReusableView *headView;
- (instancetype)initWithCollcetionview:(UICollectionView *)collectionView user:(User *)user;
- (void)userInformationChanged:(NSNotification *)noti;
- (void)refreshUserInf;
- (void)refreshAllData;
@end

