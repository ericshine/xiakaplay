//
//  ChangeUserInformationViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ChangeUserInformationViewModel : NSObject
+ (void)changeInformation:(NSDictionary*)parameter success:(void(^)(BOOL success))success;
@end
