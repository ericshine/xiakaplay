//
//  MeHomeViewUnSiginViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHomeViewUnSiginViewModel.h"
#import "MeHeadCollectionviewCell.h"
#import "UnLoginCollectionViewCelll.h"
#import "UserSegmentCollectionViewCell.h"
#define kHeadHeight 200
static NSString* const MeHeadCollectionViewCellIdentify = @"MeHeadCollectionViewCellIdentify";
static NSString *const kUserSegmentCollectionViewCell = @"UserSegmentCollectionViewCell";
static NSString* const kUnLoginCollectionViewCelll = @"UnLoginCollectionViewCelll";
@implementation MeHomeViewUnSiginViewModel
- (instancetype)initWithController:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView{
    self = [self init];
    if(self){
        self.controller = controller;
        [collectionView setCollectionViewLayout:self.layout];
        [collectionView registerClass:[MeHeadCollectionviewCell class] forCellWithReuseIdentifier:MeHeadCollectionViewCellIdentify];
        [collectionView registerClass:[UserSegmentCollectionViewCell class] forCellWithReuseIdentifier:kUserSegmentCollectionViewCell];
        [collectionView registerClass:[UnLoginCollectionViewCelll class] forCellWithReuseIdentifier:kUnLoginCollectionViewCelll];
    }
    return self;
}

- (BJMapsListCollectionLayout *)layout{
    if(_layout == nil){
        _layout = [[BJMapsListCollectionLayout alloc] init];
        _layout.delegate = self;
        _layout.isStickyHeader = YES;
    }
    return _layout;
}
#pragma mark - BJMapsListCollectionDelegate
- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
    widthForItemInSection:( NSInteger )section{
     return kDeviceWidth;
}
- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
 heightForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(indexPath.row == 0) return kHeadHeight;
    else if(indexPath.row == 1) return 50;
    return 280;
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        MeHeadCollectionviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MeHeadCollectionViewCellIdentify forIndexPath:indexPath];
        cell.headImageView.image = [UIImage imageNamed:@"defaultHeadImage"];
        return cell;
    }else if(indexPath.row == 1){
        UserSegmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserSegmentCollectionViewCell forIndexPath:indexPath];
        return cell;
    }
    UnLoginCollectionViewCelll *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUnLoginCollectionViewCelll forIndexPath:indexPath];
    __weak typeof(self) weakSelf = self;
    cell.loginAction = ^(){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf.controller toLoginVcClearData:NO];
            };
    return cell;
}

@end
