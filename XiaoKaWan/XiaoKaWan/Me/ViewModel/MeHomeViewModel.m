//
//  MeHomeViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeHomeViewModel.h"
#import "MeHeadTabelViewCell.h"
#import "MapsHomeCell.h"
#import "Maps.h"
#import "MeHomePlaceCell.h"
#import "MeTitleTableViewCell.h"
#import "Place.h"
#import "MapsUserListViewController.h"
#import "MyMapsListViewController.h"
#import "MyPlaceListViewController.h"
#import "MapsPlaceDetailViewController.h"
#import "MapDetailListViewController.h"
#import "CurrentUser.h"
#import "MeSegmentHeadView.h"
#import "BJRefreshGitFooter.h"
#import "BJRefreshGitHeader.h"
#import "MapsNetworkManage.h"
#import "MapsPostNetoworkParameter.h"
#import "QAlertView.h"
#import "MyPlaceObject.h"
#import "PlaceListTableViewCell.h"
#import "MapsNetworkManage.h"
#import "UIColor+colorWithHex.h"
typedef NS_ENUM(NSInteger,MapsPlaceTye) {
    MAPPLACETYPE,
    MAPPLACETYPE_MAP, // 我收藏的maps
    MAPPLACETYPE_PLACE // 我的地点
};
typedef NS_ENUM(NSInteger,BeenWantType){
    BEENWANTTYPE,
    BEENWANTTYPE_BEEN, //我去过的地方
    BEENWANTTYPE_WANT //我先去的地方
};
static NSString* const MeHeadCollectionViewCellIdentify = @"MeHeadCollectionViewCellIdentify";
static NSString* const MeHomePlaceCellIdentify = @"MeHomePlaceCellIdentify";
static NSString* const placeListCellIdentifier = @"placeListCellIdentifier";

@interface MeHomeViewModel ()<MeHeadTabelViewCellDelegate,MapsHomeCellDelegate,MeHomePlaceCellDelegate,PlaceListTableViewCellDelegate>
@property(nonatomic,assign)BJSuperViewController *controller;
@property(nonatomic,strong) CurrentUser *user;
@property(nonatomic,strong) UITableView *table;
@property(nonatomic,strong) MeSegmentHeadView *segmentHeadView;
@property(nonatomic)MapsPlaceTye mapsPlaceType;
@property(nonatomic)BeenWantType beenWantType;
@property(nonatomic,strong) NSMutableArray *mapsList;
@property(nonatomic,strong) NSMutableArray *placeBeenList;
@property(nonatomic,strong) NSMutableArray *placeWantList;
@property(nonatomic) BOOL noMoreMaps;
@property(nonatomic) BOOL noMoreBeenData;
@property(nonatomic) BOOL noMoreWantData;
@property(nonatomic,strong)MapsPostNetoworkParameter *mapsParameter;
@property(nonatomic,strong)PlaceParameter *placeBeenParameter;
@property(nonatomic,strong)PlaceParameter *placeWantParameter;
@end
@implementation MeHomeViewModel

- (instancetype)initWithModel:(id)model tableView:(UITableView *)collectionView viewController:(BJSuperViewController *)viewController{
    self = [super init];
    if(self){
        self.beenWantType = BEENWANTTYPE_BEEN;
        self.mapsPlaceType = MAPPLACETYPE_MAP;
        self.table = collectionView;
        self.user = model;
        _controller = viewController;
        [self setRefreshCotroller];
    }
    return self;
}
- (void)setRefreshCotroller{
    BJRefreshGitHeader *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self getDataFooter:NO];
    }];
    self.table.header = header;
    [self.table.header beginRefreshing];
  BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
      [self getDataFooter:YES];
    }];
    self.table.footer = footer;
}
- (MapsPostNetoworkParameter *)mapsParameter{
    if(_mapsParameter == nil){
        _mapsParameter = [[MapsPostNetoworkParameter alloc] init];
    }
    return _mapsParameter;
}
- (PlaceParameter *)placeBeenParameter{
    if(_placeBeenParameter == nil){
        _placeBeenParameter = [[PlaceParameter alloc] init];
    }
    return _placeBeenParameter;
}
- (PlaceParameter *)placeWantParameter{
    if(_placeWantParameter == nil){
        _placeWantParameter = [[PlaceParameter alloc] init];
    }
    return _placeWantParameter;
}
#pragma mark - netWork
- (void)getDataFooter:(BOOL)footer{
    if(self.mapsPlaceType == MAPPLACETYPE_MAP){
        [self getMapsDataFooter:footer];
    }else{
        [self getPlaceDataFooter:footer];
    }
}
- (void)getMapsDataFooter:(BOOL)footer{
    if(footer){
        Maps *maps = [_mapsList lastObject];
        self.mapsParameter.last_id = [NSString stringWithFormat:@"%@",maps.id];
    }else{
         self.mapsParameter.last_id = @"0";
    }
    [[MapsNetworkManage sharedInstance] getMyMapsListWithParameter:self.mapsParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
             NSMutableArray *array = [Maps saveMapsData:obj isMyMaps:YES];
            if(footer){
                [_mapsList insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_mapsList.count, array.count)]];
            }else{
               _mapsList = [NSMutableArray arrayWithArray:array];
            }
            if(footer)[self.table.footer endRefreshing];
            else [self.table.header endRefreshing];
            if(array.count<[self.mapsParameter.count integerValue]){
                [self.table.footer endRefreshingWithNoMoreData];
                self.noMoreMaps = YES;
            }
            [self.table reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(footer)[self.table.footer endRefreshing];
            else {
                _mapsList = [NSMutableArray arrayWithArray:[Maps getMyMapsList]];
                [self.table.header endRefreshing];
                [self.table reloadData];
            }
        }
    }];
}
- (void)getPlaceDataFooter:(BOOL)footer{
     PlaceParameter *placeParameter;
    if(footer){
        if(self.beenWantType == BEENWANTTYPE_BEEN){
            MyPlaceObject *place = [_placeBeenList lastObject];
            self.placeBeenParameter.last_id = [NSString stringWithFormat:@"%@",place.id];
            placeParameter = self.placeBeenParameter;
        }else{
            MyPlaceObject *place = [_placeWantList lastObject];
            self.placeWantParameter.last_id = [NSString stringWithFormat:@"%@",place.id];
            placeParameter = self.placeWantParameter;
        }
        
    }else{
        if(self.beenWantType == BEENWANTTYPE_BEEN){
            self.placeBeenParameter.place_type = @"been";
            self.placeBeenParameter.last_id = @"0";
            placeParameter = self.placeBeenParameter;
        }else{
            self.placeWantParameter.place_type = @"want";
             self.placeWantParameter.last_id = @"0";
            placeParameter = self.placeWantParameter;
        }
   
    }
   
    [[MapsNetworkManage sharedInstance] getMyPlceListWithParameter:placeParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            if(footer)[self.table.footer endRefreshing];
            else [self.table.header endRefreshing];
            NSInteger dataCount = 0;
            if(self.beenWantType == BEENWANTTYPE_BEEN){
             NSArray *array = [MyPlaceObject savePlaceBeenObjcets:obj];
                dataCount = array.count;
                if(footer){
                [_placeBeenList insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_placeBeenList.count, array.count)]];
                }else{
                 _placeBeenList = [NSMutableArray arrayWithArray:array];
                }
                if(dataCount<[placeParameter.count integerValue]){
                    [self.table.footer endRefreshingWithNoMoreData];
                    self.noMoreBeenData = YES;
                }
            }else{
                NSArray *array = [MyPlaceObject savePlaceWantObjcets:obj];
                dataCount = array.count;
                if(footer){
                [_placeWantList insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_placeWantList.count, array.count)]];
                }else{
                 _placeWantList = [NSMutableArray arrayWithArray:array];
                }
                if(dataCount<[placeParameter.count integerValue]){
                    [self.table.footer endRefreshingWithNoMoreData];
                    self.noMoreWantData = YES;
                }
            }
            [self.table reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(footer)[self.table.footer endRefreshing];
            else {
                if(self.beenWantType == BEENWANTTYPE_BEEN){
                _placeBeenList = [NSMutableArray arrayWithArray:[MyPlaceObject getPlaceBeen]];
                }else{
                    _placeWantList = [NSMutableArray arrayWithArray:[MyPlaceObject getPlaceWant]];
                }
                [self.table.header endRefreshing];
                [self.table reloadData];
            }
        }
    }];
}
- (MeSegmentHeadView *)segmentHeadView{
    if(_segmentHeadView == nil){
        _segmentHeadView = [[MeSegmentHeadView alloc] init];
        _segmentHeadView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        _segmentHeadView.selectAtIndex = ^(NSInteger index){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if(index == 0){
                strongSelf.beenWantType = BEENWANTTYPE_BEEN;
                [strongSelf.table reloadData];
            }else{
            strongSelf.beenWantType = BEENWANTTYPE_WANT;
                if(strongSelf.placeWantList.count<=0)[strongSelf getDataFooter:NO];
                else [strongSelf.table reloadData];
            }
        };
    }
    return _segmentHeadView;
}
#pragma  mark -tableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0)return 1;
    else{
        if(self.mapsPlaceType == MAPPLACETYPE_MAP)return _mapsList.count;
        else{
            if(self.beenWantType == BEENWANTTYPE_BEEN) return _placeBeenList.count;
            else return _placeWantList.count;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) return 220;
    else{
        if(self.mapsPlaceType == MAPPLACETYPE_MAP)return 312+73;
        else{
            return 150;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(self.mapsPlaceType ==MAPPLACETYPE_PLACE){
    if(section == 1){
        return 50;
    }else{
        return 0.01;
    }
    }else{
        return 0.01;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
     if(self.mapsPlaceType ==MAPPLACETYPE_PLACE){
    if(section ==1) return self.segmentHeadView;
     }
    return nil;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    MeHeadTabelViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MeHeadCollectionViewCellIdentify];
    if(!cell){
        cell = [[MeHeadTabelViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MeHeadCollectionViewCellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
        cell.user = self.user;
    return cell;
    }else {
        if(self.mapsPlaceType == MAPPLACETYPE_MAP){
            Maps *maps = _mapsList[indexPath.row];
            static NSString *cellId = @"myMapsHomeCellId";
            MapsHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if(!cell){
                cell = [[MapsHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                 cell.delegate = self;
            }
            cell.indexPath = indexPath;
            cell.maps = maps;
            return cell;
        }
        MyPlaceObject *placeObject;
        if(self.beenWantType == BEENWANTTYPE_BEEN){
            placeObject = _placeBeenList[indexPath.row];
        }else{
            placeObject = _placeWantList[indexPath.row];
        }
        PlaceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:placeListCellIdentifier];
        if(!cell){
            cell = [[PlaceListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:placeListCellIdentifier];
            cell.delegate =self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        }
        cell.placeObject = placeObject;
        cell.indexPath = indexPath;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        if(self.mapsPlaceType ==MAPPLACETYPE_MAP){
            Maps *maps = _mapsList[indexPath.row];
            MapObject *object = [MapObject mapObject];
            object.id = [NSString stringWithFormat:@"%@",maps.id];
            [self toMapsDetaliList:object];
        }else{
        
        }
    }
}
- (void)showUserListWithData:(NSArray *)array{
    MapsUserListViewController *userList = [[MapsUserListViewController alloc] init];
    [self.controller push:userList];
}
#pragma mark - MapsHomeCellDelegate
- (void)touchMapsWithModel:(id)model{
    MapObject *object = [MapObject mapObject];
    CollectionObject *collectionObject = model;
    object.id = collectionObject.id;
    [self toMapsDetaliList:object];
    self.table.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
}
- (void)toMapsDetaliList:(MapObject*)object{
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    detailList.mapObject = object;
    [self.controller push:detailList];
    self.table.backgroundColor = [UIColor whiteColor];
}
#pragma mark - PlaceListTableViewCellDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] wantGoAction:place callBack:place1];
}
- (void)selectImageViewWithPlace:(Place*)place{

}

#pragma mark - MeHeadCollectionViewCellDelegate
- (void)placeTouchAction{
    if(self.mapsPlaceType == MAPPLACETYPE_PLACE)return;
    self.mapsPlaceType = MAPPLACETYPE_PLACE;
    if(self.beenWantType ==BEENWANTTYPE_BEEN){
        if(_placeBeenList.count<=0)[self getDataFooter:NO];
        else [self.table reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationRight];
    }else{
     if(_placeWantList.count<=0)[self getDataFooter:NO];
     else [self.table reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationRight];
    }
    self.table.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
}
- (void)mapsTouchAction{
    if(self.mapsPlaceType == MAPPLACETYPE_MAP)return;
     self.mapsPlaceType = MAPPLACETYPE_MAP;
    [self.table reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationLeft];
    self.table.backgroundColor = [UIColor whiteColor];
}


#pragma mark - NSNotification
- (void)userInformationChanged:(NSNotification *)noti{
    CurrentUser *user = [CurrentUser getCurrentUser];
    self.user = user;
    [UIView animateWithDuration:0.3 animations:^{
        [self.table reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }];
    
}
@end
