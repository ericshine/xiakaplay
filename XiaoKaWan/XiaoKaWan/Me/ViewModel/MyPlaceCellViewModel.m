//
//  MyPlaceCellViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyPlaceCellViewModel.h"
#import "MapsPalceImageLayout.h"
#import "MapsPlaceImageCollectionCell.h"

static NSString* const imageCellIdentifer = @"imageCellIdentifer";
@interface MyPlaceCellViewModel()<MapsPalceImageLayoutDelegate>
@property(nonatomic,strong)MapsPalceImageLayout *layout;
@end

@implementation MyPlaceCellViewModel
- (instancetype)initWithView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        [collectionView setCollectionViewLayout:self.layout];
        [collectionView registerClass:[MapsPlaceImageCollectionCell class] forCellWithReuseIdentifier:imageCellIdentifer];
    }
    return self;
}
- (MapsPalceImageLayout *)layout{
    if(_layout == nil){
        _layout = [[MapsPalceImageLayout alloc] init];
        _layout.delegate = self;
    }
    return _layout;
}
#pragma mark - MapsPalceImageLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPalceImageLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath{
    return 20*indexPath.row;
}
#pragma mark - MapsPlaceListHeadCollectionCellDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 12;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MapsPlaceImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:imageCellIdentifer forIndexPath:indexPath];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if([self.delegate respondsToSelector:@selector(selelctItem)]){
        [self.delegate selelctItem];
    }
}

@end
