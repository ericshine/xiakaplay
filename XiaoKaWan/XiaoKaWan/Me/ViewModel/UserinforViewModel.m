//
//  UserinforViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserinforViewModel.h"
#import "UserParameter.h"
#import "MeNetworkManage.h"
#define kHeadHeight 200
NSString *const kMeHeadCollectionviewCell  = @"kMeHeadCollectionviewCell";
NSString *const kUserSegmentCollectionViewCell = @"UserSegmentCollectionViewCell";
NSString *const kMapsCollectionViewCell = @"MapsCollectionViewCell";
NSString *const kUserPostCollectionViewCell = @"UserPostCollectionViewCell";
NSString *const kUserPlaceCollectioncell = @"UserPlaceCollectioncell";
static NSString * const kheadViewBlankCellIdentifier = @"headViewBlankCellIdentifier";
@implementation UserinforViewModel{
    dispatch_group_t group;
    BOOL isRefreshUserInf;
}
- (instancetype)initWithCollcetionview:(UICollectionView *)collectionView user:(User *)user{
    self = [super init];
    if(self){
       
        self.user = user;
        self.collcetionView = collectionView;
        [self.collcetionView setCollectionViewLayout:self.layout];
        [self.collcetionView registerClass:[MeHeadCollectionviewCell class] forCellWithReuseIdentifier:kMeHeadCollectionviewCell];
        [self.collcetionView registerClass:[MapsCollectionViewCell class] forCellWithReuseIdentifier:kMapsCollectionViewCell];
        [self.collcetionView registerClass:[UserPostCollectionViewCell class] forCellWithReuseIdentifier:kUserPostCollectionViewCell];
        [self.collcetionView registerClass:[UserPlaceCollectioncell class] forCellWithReuseIdentifier:kUserPlaceCollectioncell];
        [self.collcetionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kheadViewBlankCellIdentifier];
        [self.collcetionView registerClass:[UserSegmentCollectionViewCell class] forCellWithReuseIdentifier:kUserSegmentCollectionViewCell];
        [self setRefreshCotroller];
       
    }
    return self;
}
- (void)refreshUserInf{
    isRefreshUserInf = YES;
    [self getUserInf];
}
- (void)refreshAllData{
    [self startReloadData];
}
- (void)setRefreshCotroller{
    BJRefreshGitHeader *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self startReloadData];
        
    }];
    self.collcetionView.header = header;
    [self.collcetionView.header beginRefreshing];
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self.dataSourceDelegete getDataFooter:YES withCollectionView:self.collcetionView WithUser:self.user];
    }];
    self.collcetionView.footer = footer;
}
- (void)startReloadData{
    group = nil;
    group = dispatch_group_create();
    [self getUserInf];
    // 防止数据安全
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.dataSourceDelegete getDataFooter:NO withCollectionView:self.collcetionView WithUser:self.user];
    });
}
- (void)dealloc{
    NSLog(@"dealloc UserinforViewModel");
}

- (void)getUserInf{
     dispatch_group_enter(group);
    UserParameter *parameter = [[UserParameter alloc] init];
    parameter.uid = [NSString stringWithFormat:@"%ld",(long)self.user.uid];
    parameter._t = [UserConfig sharedInstance].user_token;
    [[MeNetworkManage sharedInstance] getUserInforWithParameter:parameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            if(self.user.isMyInformation){
                [CurrentUser saveUserDataWithDic:obj[@"userinfo"]];
            }
            self.user = [User objectWithKeyValues:obj];
            self.user.isMyInformation = YES;
            if(isRefreshUserInf) {
               [self.collcetionView.collectionViewLayout invalidateLayout];
                [self.collcetionView reloadData];
            }
            isRefreshUserInf = NO;
            if(self.loadUserInf)self.loadUserInf( self.user);
        }
        dispatch_group_leave(group);
    }];
    
}
- (BJMapsListCollectionLayout *)layout{
    if(_layout == nil){
        _layout = [[BJMapsListCollectionLayout alloc] init];
        _layout.delegate = self;
        _layout.isStickyHeader = YES;
        
    }
    return _layout;
}
#pragma mark - BJMapsListCollectionDelegate
- (CGFloat)collectionView:( UICollectionView *)collectionView
                   layout:( BJMapsListCollectionLayout *)collectionViewLayout
    widthForItemInSection:( NSInteger )section{
    if(section == 2) {
        NSInteger width = [self.dataSourceDelegete widthForItemInCollectionView:collectionView layout:collectionViewLayout];
        return width;
    }else return kDeviceWidth;
}
- (CGFloat)collectionView:( UICollectionView *)collectionView
                   layout:( BJMapsListCollectionLayout *)collectionViewLayout
 heightForItemAtIndexPath:( NSIndexPath *)indexPath{
    if(indexPath.section == 0) return kHeadHeight;
     else if(indexPath.section == 1) return 50;
    return [self.dataSourceDelegete collectionView:collectionView layout:collectionViewLayout heightForItemAtIndexPath:indexPath];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 2)return [self.dataSourceDelegete numberOfItemsInCollectionView:collectionView];
    else return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    MeHeadCollectionviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kMeHeadCollectionviewCell forIndexPath:indexPath];
        cell.delegate = self;
    cell.user = self.user;
    return cell;
    }else if(indexPath.section == 1){
        UserSegmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserSegmentCollectionViewCell forIndexPath:indexPath];
        cell.delegate = self;
        cell.user = self.user;
        return cell;
    }
    return [self.dataSourceDelegete cellForItemAtCollectionView:collectionView cellForItemAtIndexPath:indexPath];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 2){
        [self.dataSourceDelegete collectionView:collectionView didSelectItemAtIndexRow:indexPath.row];
    }
}
//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGRect bounds = CGRectInset(cell.bounds, 10, 0);
//    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, bounds.size.width, bounds.size.height)];
//    
//    // MARK: circlePath
//    //    [path appendPath:[UIBezierPath bezierPathWithArcCenter:CGPointMake(SCREEN_WIDTH / 2, 200) radius:100 startAngle:0 endAngle:2*M_PI clockwise:NO]];
//    
//    // MARK: roundRectanglePath
//    [path appendPath:[[UIBezierPath bezierPathWithRoundedRect:CGRectMake(20, 400,200, 100) cornerRadius:15] bezierPathByReversingPath]];
//    
//    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//    
//    shapeLayer.path = path.CGPath;
//    
//    [cell.layer setMask:shapeLayer];
//}
#pragma mark - MeHeadCollectionviewCellDelegate
- (void)toFans{
    [self.delegate toFans];
}
- (void)toFollows{
    [self.delegate toFollows];
}
- (void)toUserHeadImage{
    [self.delegate toUserHeadImage];
}
#pragma mark - UserSegmentCollectionViewCellDelegate
- (void)selectAtIndex:(NSInteger)index{
    [self.delegate selectAtIndex:index];
}
#pragma mark - NSNotification
- (void)userInformationChanged:(NSNotification *)noti{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    self.user = [User coreDataUserToUser:currentUser];
//    [UIView animateWithDuration:0.3 animations:^{
        [self.collcetionView.collectionViewLayout invalidateLayout];
       [self.collcetionView reloadData];
//    }];
    
}


@end
