//
//  UserPostViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "UserinforViewModel.h"
@protocol UserPostViewModelDelegate <NSObject>

- (void)toPostDetail:(PostObjet *)postDetail;

@end
@interface UserPostViewModel : NSObject<UserinforViewModelDataSource>
@property(nonatomic,strong) NSMutableArray *postList;
@property(nonatomic,assign)id<UserPostViewModelDelegate>delegate;
@end
