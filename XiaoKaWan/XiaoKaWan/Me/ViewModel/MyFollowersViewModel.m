//
//  MyFollowersViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyFollowersViewModel.h"
#import "BJRefreshGitFooter.h"
#import "BJRefreshGitHeader.h"
#import "MeNetworkManage.h"
#import "TabelHeadTitleView.h"
#import "UIColor+colorWithHex.h"
#import "MeNetworkManage.h"
static CGFloat const rowHeight = 72;
static NSString *const cellIdentify = @"userCellId";
@interface MyFollowersViewModel ()
@property(nonatomic,strong)UserParameter *parameter;
@property(nonatomic,strong) User *currentUser;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)TabelHeadTitleView *headView;

@end
@implementation MyFollowersViewModel
- (instancetype)initWithTable:(UITableView *)table andUser:(User *)user{
    self = [super init];
    if(self){
        _parameter = [[UserParameter alloc] init];
        _parameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
        _parameter._t = [UserConfig sharedInstance].user_token;
        _tableView = table;
        _currentUser = user;
       
        BJRefreshGitHeader *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
             [self getDataFooter:NO];
        }];
        _tableView.header = header;
        [_tableView.header beginRefreshing];
        BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
            [self getDataFooter:YES];
        }];
        _tableView.footer = footer;
    }
    return self;
}
- (void)reloadData{
    [self getDataFooter:NO];
}
- (void)getDataFooter:(BOOL)footer{
    if(footer){
        User *user = [_userLists lastObject];
        _parameter.last_id = [NSString stringWithFormat:@"%li",(long)user.uid];
    }else{
        _parameter.last_id = @"0";
    }
    [[MeNetworkManage sharedInstance] getFollowersWithParameter:_parameter complete:^(BOOL succeed, id obj) {
        if(!succeed){
            if(!footer){
            [_tableView.header endRefreshing];
            }else[_tableView.footer endRefreshing];
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            return;
        }else{
            NSMutableArray *users = [[NSMutableArray alloc] initWithCapacity:0];
            [obj enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                User *user = [User objectWithKeyValues:obj];
                user.userType = UserType_MyFollows;
                [users addObject:user];
            }];
            
            if(!footer){
                [_tableView.header endRefreshing];
                _userLists = [NSMutableArray arrayWithArray:[users mutableCopy]];
            }else{
                [_tableView.footer endRefreshing];
                [_userLists insertObjects:users atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_userLists.count, users.count)]];
            }
            if(users.count< [_parameter.count integerValue]){
                [_tableView.footer endRefreshingWithNoMoreData];
            }
            [_tableView reloadData];
        }
    }];

}
- (TabelHeadTitleView *)headView{
    if(_headView == nil){
        _headView = [[TabelHeadTitleView alloc] init];
        _headView.lineView.hidden = YES;
        _headView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
    }
    return _headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.headView.titleLabel.text= [NSString stringWithFormat:@"全部关注(%lu)",(unsigned long)self.userLists.count];
    return self.headView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userLists.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    User *user = self.userLists[indexPath.row];
    UserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell = [[UserListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.delegate = self;
    }
    cell.user = user;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    User *user = self.userLists[indexPath.row];
    [self.delegate toUserInfor:user];
}
#pragma mark - UserListTableViewCellDelegate
- (void)followUser:(User *)user success:(void (^)(User *))user1{
    
    UIViewController *controller =[self findViewController:self.tableView];

    NSString *title;
    NSString *actionTitle;
    if(user.followed){
        title = @"确定取消关注？";
        actionTitle = @"不再关注";
    }else{
        title = @"将要关注此人";
        actionTitle = @"关注";
    }
    BJAlertController  *alterVc = [[BJAlertController alloc] initWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alterVc addDestructiveActionWithTitle:actionTitle handler:^{
        UserParameter *parameter = [[UserParameter alloc] init];
        parameter._t = [UserConfig sharedInstance].user_token;
        parameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
        if(user.followed){
            [[MeNetworkManage sharedInstance] unFollowUserWithPararmeter:parameter complete:^(BOOL succeed, id obj) {
                if(succeed){
                user.followed = [obj[@"followed"] boolValue];
                user.bi_followed = [obj[@"bi_followed"] boolValue];
                if(user1)user1(user);
                [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                }else [[QAlertView sharedInstance]showAlertText:obj fadeTime:2];
            }];
        }else{
            [[MeNetworkManage sharedInstance] followUserWithParameter:parameter complete:^(BOOL succeed, id obj) {
                if(succeed){
                user.followed = [obj[@"followed"] boolValue];
                user.bi_followed = [obj[@"bi_followed"] boolValue];
                if(user1)user1(user);
                [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                }else [[QAlertView sharedInstance]showAlertText:obj fadeTime:2];
            }];
        }
        
    }];
    [alterVc addCancleActionHandler:^{
        
    }];
    [controller presentViewController:alterVc animated:YES completion:nil];
}
- (UIViewController *)findViewController:(UIView *)sourceView
{
    id target=sourceView;
    while (target) {
        target = ((UIResponder *)target).nextResponder;
        if ([target isKindOfClass:[UIViewController class]]) {
            break;
        }
    }
    return target;
}
@end
