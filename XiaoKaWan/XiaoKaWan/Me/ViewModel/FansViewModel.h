//
//  FansViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperObject.h"
#import "UserListTableViewCell.h"
#import "User.h"
@protocol FansViewModelDelegate <NSObject>

- (void)toUserInfor:(User *)user;

@end
@interface FansViewModel : BJSuperObject<UITableViewDelegate,UITableViewDataSource,UserListTableViewCellDelegate>
@property(nonatomic,strong)NSMutableArray *userLists;
@property(nonatomic,assign)id<FansViewModelDelegate>delegate;
- (instancetype)initWithTable:(UITableView *)table andUser:(User*)user;
- (void)reloadData;
@end

