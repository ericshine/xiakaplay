//
//  UserPlaceViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserinforViewModel.h"
typedef NS_ENUM(NSInteger,BeenWantType){
    BEENWANTTYPE,
    BEENWANTTYPE_BEEN, //我去过的地方
    BEENWANTTYPE_WANT //我先去的地方
};
@protocol UserPlaceViewModelDelegate <NSObject>

- (void)toPlaceDetail:(Place *)place;
- (void)toUserList:(Place*)place;
@end
@interface UserPlaceViewModel : NSObject<UserinforViewModelDataSource>
@property(nonatomic)BeenWantType beenWantType;
@property(nonatomic,strong)NSMutableArray *placeList;
@property(nonatomic,assign)id<UserPlaceViewModelDelegate>delegate;
@end

