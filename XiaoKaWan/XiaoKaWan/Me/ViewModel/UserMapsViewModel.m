//
//  UserMapsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserMapsViewModel.h"
#import "MapsNetworkManage.h"
#import "MapsPostNetoworkParameter.h"
#import <MJExtension.h>
#import "MapsUser.h"
@interface UserMapsViewModel ()<MapsCollectionViewCellDelegate>

@property(nonatomic,strong)MapsPostNetoworkParameter *mapsParameter;
@property(nonatomic,strong)User *user;
@end
@implementation UserMapsViewModel
- (MapsPostNetoworkParameter *)mapsParameter{
    if(_mapsParameter == nil){
        _mapsParameter = [[MapsPostNetoworkParameter alloc] init];
    }
    return _mapsParameter;
}
#pragma mark -UserinforViewModelDataSource
- (void)getDataFooter:(BOOL)footer withCollectionView:(UICollectionView *)collectionView WithUser:(User *)user{
    self.user = user;
    self.mapsParameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
    if(footer){
        MapsDetail *maps = [_mapsList lastObject];
        self.mapsParameter.last_id = [NSString stringWithFormat:@"%ld",(long)maps.id];
    }else{
         [collectionView.footer resetNoMoreData];
       self.mapsParameter.last_id = @"0";
    }
    [[MapsNetworkManage sharedInstance] getMyMapsListWithParameter:self.mapsParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            NSMutableArray *array;
            if(self.user.isMyInformation){
                [Maps clearMyMapsCache];
                NSMutableArray *mapsArray = [[NSMutableArray alloc] init];
               NSArray *list = [Maps saveMapsData:obj isMyMaps:YES];
                [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    MapsDetail *detail = [self mapsToMapsDetail:obj];
                    [mapsArray addObject:detail];
                }];
                array = [mapsArray mutableCopy];
            }else{
                array = [MapsDetail dicToObject:obj];
            }
            if(!array.count) {
                if(footer)[collectionView.footer endRefreshing];
                else [collectionView.header endRefreshing];
                return;
            }
            if(footer){
                [_mapsList insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_mapsList.count, array.count)]];
            }else{
                _mapsList = [NSMutableArray arrayWithArray:array];
            }
            
            if(footer)[collectionView.footer endRefreshing];
            else [collectionView.header endRefreshing];
            if(array.count<[self.mapsParameter.count integerValue]){
                [collectionView.footer endRefreshingWithNoMoreData];
            }
            [collectionView.collectionViewLayout invalidateLayout];
            [collectionView reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(footer)[collectionView.footer endRefreshing];
            else {
                if(self.user.isMyInformation){
                _mapsList = [NSMutableArray arrayWithArray:[Maps getMyMapsList]];
                }
                [collectionView.header endRefreshing];
                [collectionView.collectionViewLayout invalidateLayout];
                [collectionView reloadData];
            }
        }
    }];
    
}

- (CGFloat)widthForItemInCollectionView:(nonnull UICollectionView *)collectionView
                                 layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout{
    return kDeviceWidth;//
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return 25;
    return 312+73;
}
- (NSInteger)numberOfItemsInCollectionView:(UICollectionView *)collectionView{
    return _mapsList.count+1;
}

- (UICollectionViewCell *)cellForItemAtCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"blankCell"];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"blankCell" forIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        return cell;
    }
    MapsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kMapsCollectionViewCell forIndexPath:indexPath];
    cell.delegate = self;
    
    MapsDetail *detail = [_mapsList objectAtIndex:indexPath.row-1];
    cell.mapsObj = detail;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexRow:(NSInteger)row{
    if(row == 0)return;
    MapsDetail *detail = [_mapsList objectAtIndex:row-1];
    [self.delegate toAlbumDetail:detail];
}
#pragma mark - MapsCollectionViewCell
- (void)touchMapsWithModel:(MapsDetail *)model{
    [self.delegate toAlbumDetail:model];
}
- (MapsDetail *)mapsToMapsDetail:(Maps *)maps{
    MapsDetail *detail = [[MapsDetail alloc] init];
    detail.id = [maps.id integerValue];
    detail.name = maps.name;
    detail.place_count = [maps.place_count integerValue];
    detail.post_count = [maps.post_count integerValue];
    detail.follow_count = [maps.follow_count integerValue];
     NSMutableArray *images = [[NSMutableArray alloc] init];
    if(maps.photos!=nil){
    NSArray *photos = [NSJSONSerialization JSONObjectWithData:maps.photos options:NSJSONReadingAllowFragments error:nil];
  [photos enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
      BJImageObject *image = [[BJImageObject alloc] init];
      image.url = obj[@"src"];
      image.height = [obj[@"h"] floatValue];
      image.width = [obj[@"w"] floatValue];
      [images addObject:image];
  }];
    }
   NSArray *array = [MapsUser mapsUserMapsId:maps.idValue];
    NSMutableArray *userArray = [[NSMutableArray alloc] init];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapUser = obj;
        User *user = [[User alloc] init];
        user.avatar = mapUser.avatar;
        user.uid = mapUser.uidValue;
        [userArray addObject:user];
    }];
    detail.latest_users = [userArray copy];
    detail.latest_photos =  [images copy];
    return detail;
}

@end
