//
//  ChangeUserInformationViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangeUserInformationViewModel.h"
#import "ClientNetwork.h"
#import "QAlertView.h"
#import "CurrentUser.h"
#import "BJNotificationConfig.h"
@implementation ChangeUserInformationViewModel

+ (void)changeInformation:(NSDictionary *)parameter success:(void (^)(BOOL))success{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].changeUserInformationApi parameter:parameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            [CurrentUser saveUserDataWithDic:obj[@"userInfo"]];
            [[QAlertView sharedInstance] showAlertText:@"修改成功" fadeTime:2];
            if(success)success(YES);
            [[NSNotificationCenter defaultCenter] postNotificationName:BJChangeUserInformationNotification object:nil];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            
        }
    }];
}
@end
