//
//  MeHomeViewUnSiginViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
#import "BJMapsListCollectionLayout.h"
@interface MeHomeViewUnSiginViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource,BJMapsListCollectionDelegate>
@property(nonatomic,assign)BJSuperViewController *controller;
@property(nonatomic,strong) BJMapsListCollectionLayout *layout;
- (instancetype)initWithController:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView;
@end
