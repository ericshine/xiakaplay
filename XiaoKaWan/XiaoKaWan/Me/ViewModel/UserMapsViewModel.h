//
//  UserMapsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "UserinforViewModel.h"
#import "Maps.h"
@protocol UserMapsViewModelDelegate <NSObject>

- (void)toAlbumDetail:(MapsDetail *)albumObj;

@end
@interface UserMapsViewModel : NSObject<UserinforViewModelDataSource>
@property(nonatomic,strong) id<UserMapsViewModelDelegate>delegate;
@property(nonatomic,strong) NSMutableArray *mapsList;
@end
