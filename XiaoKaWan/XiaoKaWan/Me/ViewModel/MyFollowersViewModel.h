//
//  MyFollowersViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperObject.h"
#import "User.h"
#import "UserListTableViewCell.h"
@protocol MyFollowersViewModelDelegate <NSObject>

- (void)toUserInfor:(User *)user;

@end
@interface MyFollowersViewModel : BJSuperObject<UITableViewDelegate,UITableViewDataSource,UserListTableViewCellDelegate>
- (instancetype)initWithTable:(UITableView *)table andUser:(User*)user;
@property(nonatomic,strong)NSMutableArray *userLists;
@property(nonatomic,assign)id<MyFollowersViewModelDelegate>delegate;
- (void)reloadData;
@end
