//
//  MyPlaceListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyPlaceListViewModel.h"
#import "MyMapsListHeadTableCell.h"
#import "PlaceTypeChangeTableViewCell.h"
#import "MeHomePlaceCell.h"
#import "Place.h"
#import "MapsUserListViewController.h"
#import "MyPlaceListDetailCell.h"
#import "MapsPlaceDetailViewController.h"
@interface MyPlaceListViewModel()<MeHomePlaceCellDelegate,PlaceTypeChangeTableViewCellDelegate,MyPlaceListDetailCellDelegate>
@property(nonatomic,assign) BJSuperViewController *controller;
@property(nonatomic)PlaceType placeType;
@property(nonatomic,strong)UITableView *tableView;
@end
@implementation MyPlaceListViewModel
- (instancetype)initWithModel:(id)model viewController:(BJSuperViewController *)controller andTabel:(UITableView *)tablel{
    self = [super init];
    if(self){
        self.controller = controller;
        self.tableView = tablel;
        self.placeType = PLACE_SIMPLE;
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 2) return 3;
    else return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0)return 222;
    else if(indexPath.section ==1)return 70;
    else {
        if(self.placeType == PLACE_NORMAL){
            return 300;
        }else{
            return 150;
    }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    static NSString *headIdentify = @"headIdentify";
    MyMapsListHeadTableCell *cell = [tableView dequeueReusableCellWithIdentifier:headIdentify];
    if(!cell){
        cell = [[MyMapsListHeadTableCell alloc] initWithStyle:0 reuseIdentifier:headIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleLabel.text = @"地点";
    }
    return cell;
    }else if (indexPath.section == 1){
    static NSString *changeCellIdentify = @"changeCellIdentify";
        PlaceTypeChangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:changeCellIdentify];
        if(!cell){
            cell = [[PlaceTypeChangeTableViewCell alloc] initWithStyle:0 reuseIdentifier:changeCellIdentify];
            cell.selectionStyle = 0;
            cell.delegate = self;
        }
        return cell;
    }
    if(self.placeType == PLACE_NORMAL){
        static NSString *cellPlaceIdentify = @"cellPlaceDetailIdentify";
        MyPlaceListDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellPlaceIdentify];
        if(!cell){
            cell = [[MyPlaceListDetailCell alloc] initWithStyle:0 reuseIdentifier:cellPlaceIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
//        cell.place = [Place testPlace];
//        cell.delegate = self;
        return cell;
    }else{
    static NSString *cellPlaceIdentify = @"cellPlaceIdentify";
    MeHomePlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellPlaceIdentify];
    if(!cell){
        cell = [[MeHomePlaceCell alloc] initWithStyle:0 reuseIdentifier:cellPlaceIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 2){
    [self placeDetail];
    }

}
- (void)placeDetail{
    MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
    [self.controller push:placeDetail];
}
#pragma mark - showUserListWithData
- (void)showUserListWithData:(NSArray *)array{
    MapsUserListViewController *userList = [[MapsUserListViewController alloc] init];
    [self.controller push:userList];
}
#pragma mark -MyPlaceListDetailCellDelegate
- (void)selectImageView{
    [self placeDetail];
}
#pragma  mark - PlaceTypeChangeTableViewCellDelegate
- (void)changeModel:(PlaceType)placeType
{
    self.placeType = placeType;
    [self.tableView reloadData];
}
@end
