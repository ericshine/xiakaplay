//
//  SettingListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SettingListViewModel.h"
#import "SettingObject.h"
#import "CellObject.h"
#import "SettingHeadCell.h"
#import <UIImageView+WebCache.h>
#import "ChangeAvatarViewController.h"
#import "BJSuperViewController.h"
#import "ChangeNameViewController.h"
#import "SignatureViewController.h"
#import "ChangePassordViewController.h"
#import "CurrentUser.h"
#import "ImageSizeUrl.h"
#import "CommunityController.h"
@interface SettingListViewModel()
@property(nonatomic,strong)SettingObject *settingObject;
@property(nonatomic,strong)UITableView *tabelView;
@property(nonatomic,assign)BJSuperViewController *viewController;
@property(nonatomic,strong)CurrentUser *user;
@end
@implementation SettingListViewModel
- (instancetype)initWithTabel:(UITableView *)tabel andController:(BJSuperViewController *)controller model:(id)model{
    self  =[super init];
    if(self){
        self.user = model;
        _tabelView = tabel;
        self.viewController = controller;
        self.settingObject = [[SettingObject alloc] initWith:model];
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.settingObject.listArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return 62;
    return 47;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellObject *cellObj = self.settingObject.listArray[indexPath.row];
    static NSString *cellIdentify = @"cellIdentify";
    SettingHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell = [[SettingHeadCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentify];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = cellObj.title;
    if(indexPath.row == 0){
        [cell.rightImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar120:cellObj.detail]];
    }else if (indexPath.row == 6){
        cell.detailTextLabel.text = [self.settingObject getCacheSize];
    }
    else{
        cell.detailTextLabel.text = cellObj.detail;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self selectRowAction:indexPath];
}
- (void)selectRowAction:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        ChangeAvatarViewController *avatar = [[ChangeAvatarViewController alloc] init];
        avatar.currentUser = self.user;
        [self.viewController push:avatar];
    }else if (indexPath.row == 1){
        ChangeNameViewController *changeName = [[ChangeNameViewController alloc] init];
        changeName.currentUser = self.user;
        [self.viewController push:changeName];
    }else if (indexPath.row == 2){
        SignatureViewController *signature = [[SignatureViewController alloc] init];
        signature.currentUser = self.user;
        [self.viewController push:signature];
    }else if (indexPath.row == 3){
        ChangePassordViewController *changPassword = [[ChangePassordViewController alloc] init];
        [self.viewController push:changPassword];
    }else if(indexPath.row == 4){
        CommunityController *community=  [[CommunityController alloc] init];
        [self.viewController push:community];
    }else if (indexPath.row == 5){
        CommunityController *community=  [[CommunityController alloc] init];
        [self.viewController push:community];
    }
    if(indexPath.row ==6){
        UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"清除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self.settingObject clearCache];
            [self.tabelView reloadData];
        }];
        [alertVc addAction:action1];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"算了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVc addAction:action2];
        [self.viewController presentViewController:alertVc animated:YES completion:nil];
    }
}
#pragma mark - notificaiont
- (void)userInformationChange:(NSNotification*)note{
    [self.settingObject setData:[CurrentUser getCurrentUser]];
    [self.tabelView reloadData];
}
@end
