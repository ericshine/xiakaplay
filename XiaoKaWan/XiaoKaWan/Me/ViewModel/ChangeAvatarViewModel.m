//
//  ChangeAvatarViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ChangeAvatarViewModel.h"
#import "BJPregressHUD.h"
#import "ClientNetwork.h"
#import "CurrentUser.h"
#import "QAlertView.h"
#import "BJNotificationConfig.h"
@implementation ChangeAvatarViewModel
- (instancetype)init{
    self = [super init];
    if(self){
    
    }
    return self;
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [BJPregressHUD showWithStatus:@"上传中..."];
     [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerEditedImage];
       UIImage *image1 = [UIImage imageWithData:UIImageJPEGRepresentation(image, 0.5)];
//    NSLog(@"%f",(UIImagePNGRepresentation(image2).length)/(1024.0*1024.0));
    [[ClientNetwork sharedInstance] uploadImageWithPath:[APIConfig sharedInstance].changeUserInformationApi image:image1 imageName:@"avatar" complete:^(BOOL succeed, id obj) {
        [BJPregressHUD dissMiss];
        if(succeed){
            [CurrentUser saveUserDataWithDic:obj[@"userInfo"]];
            [[QAlertView sharedInstance] showAlertText:@"修改成功" fadeTime:2];
            [[NSNotificationCenter defaultCenter] postNotificationName:BJChangeUserInformationNotification object:nil];
            if([self.delegate respondsToSelector:@selector(selectImage:)]){
                [self.delegate selectImage:image];
            }
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
    
    
}
- (NSData *)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return UIImageJPEGRepresentation(newImage, 0.8);
}
@end
