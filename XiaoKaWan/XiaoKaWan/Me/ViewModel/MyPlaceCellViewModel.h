//
//  MyPlaceCellViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol MyPlaceCellViewModelDelegate <NSObject>

- (void)selelctItem;

@end

@interface MyPlaceCellViewModel : NSObject<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,assign) id<MyPlaceCellViewModelDelegate>delegate;
- (instancetype)initWithView:(UICollectionView *)collectionView;
@end
