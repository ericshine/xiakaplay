//
//  ChangeAvatarViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol ChangeAvatarViewModelDelegate <NSObject>

- (void)selectImage:(UIImage *)image;

@end
@interface ChangeAvatarViewModel : NSObject<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,assign) id<ChangeAvatarViewModelDelegate>delegate;
@end

