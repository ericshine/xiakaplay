//
//  MyPlaceListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
@interface MyPlaceListViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
- (instancetype)initWithModel:(id)model viewController:(BJSuperViewController *)controller andTabel:(UITableView *)tablel;
@end
