//
//  SettingListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/3/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SettingListViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
- (instancetype)initWithTabel:(UITableView *)tabel andController:(UIViewController*)controller model:(id)model;
- (void)userInformationChange:(NSNotification*)note;
@end
