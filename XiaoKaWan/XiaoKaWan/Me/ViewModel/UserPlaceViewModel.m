//
//  UserPlaceViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserPlaceViewModel.h"
#import "PlaceParameter.h"
#import "MapsNetworkManage.h"
#import <MJExtension.h>
#import "MapsUser.h"
@interface UserPlaceViewModel ()<UserPlaceCollectioncellDelegate>
@property(nonatomic,strong)PlaceParameter *placeParameter;
@property(nonatomic,strong)User *user;
@end
@implementation UserPlaceViewModel
- (NSMutableArray *)placeList{
    if(_placeList == nil){
        _placeList = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return _placeList;
}
- (PlaceParameter *)placeParameter{
    if(_placeParameter == nil){
        _placeParameter = [[PlaceParameter alloc] init];
    }
    return _placeParameter;
}
#pragma mark -UserinforViewModelDataSource
- (void)getDataFooter:(BOOL)footer withCollectionView:(UICollectionView *)collectionView WithUser:(User *)user{
    self.user = user;
    self.placeParameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
    if(self.beenWantType == BEENWANTTYPE_WANT){
        self.placeParameter.place_type = @"want";
    }else{
        self.placeParameter.place_type = @"been";
    }
    if(footer){
            Place *place = [_placeList lastObject];
            self.placeParameter.last_id = [NSString stringWithFormat:@"%ld",(long)place.id];
    }else{
        [collectionView.footer resetNoMoreData];
        self.placeParameter.last_id = @"0";
    }
    [[MapsNetworkManage sharedInstance] getMyPlceListWithParameter:_placeParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            if(footer)[collectionView.footer endRefreshing];
            else [collectionView.header endRefreshing];
            NSInteger dataCount = 0;
            NSMutableArray *array;
            if(self.user.isMyInformation){
                if(self.beenWantType == BEENWANTTYPE_WANT){
                 [MyPlaceObject clearMyWantCache];
                    array = [MyPlaceObject savePlaceWantObjcets:obj];
                }else{
                    [MyPlaceObject clearMyBeenCache];
                    array = [MyPlaceObject savePlaceBeenObjcets:obj];
                }
                NSMutableArray *listArray = [[NSMutableArray alloc] initWithCapacity:array.count];
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    Place *place = [Place myPlaceObjectToPlace:obj];
                    [listArray addObject:place];
                }];
                array = [listArray copy];
                
            }else{
                array = [Place dicToObject:obj];
            }
                dataCount = array.count;
                if(footer){
                    [self.placeList insertObjects:array atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(self.placeList.count, array.count)]];
                }else{
                    self.placeList = [NSMutableArray arrayWithArray:array];
                }
                if(dataCount<[_placeParameter.count integerValue]){
                    [collectionView.footer endRefreshingWithNoMoreData];
                }
            [collectionView.collectionViewLayout invalidateLayout];
            [collectionView reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(footer)[collectionView.footer endRefreshing];
            else {
                if(self.user.isMyInformation){
                    NSMutableArray *array;
                    if(self.beenWantType == BEENWANTTYPE_WANT){
                    array = [NSMutableArray arrayWithArray:[MyPlaceObject getPlaceWant]];
                    }else{
                    array = [NSMutableArray arrayWithArray:[MyPlaceObject getPlaceBeen]];
                    }
                    NSMutableArray *listArray = [[NSMutableArray alloc] initWithCapacity:array.count];
                    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        Place *place = [Place myPlaceObjectToPlace:obj];
                        [listArray addObject:place];
                    }];
                    self.placeList = [listArray mutableCopy];
            }
                [collectionView.header endRefreshing];
                [collectionView.collectionViewLayout invalidateLayout];
                [collectionView reloadData];
            }
        }
    }];
}

- (CGFloat)widthForItemInCollectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout{
    return kDeviceWidth;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (NSInteger)numberOfItemsInCollectionView:(UICollectionView *)collectionView{
    return self.placeList.count;
}
- (UICollectionViewCell *)cellForItemAtCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     Place *placeObject = [_placeList objectAtIndex:indexPath.row];
    UserPlaceCollectioncell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserPlaceCollectioncell forIndexPath:indexPath];
    cell.delegate = self;
//    if(self.user.isMyInformation){
//    cell.place = [self toPlace:placeObject];
//    }else{
    cell.place = placeObject;
//    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexRow:(NSInteger)row{
    Place *placeObject = [_placeList objectAtIndex:row];
    if(self.user.isMyInformation){
        [self.delegate toPlaceDetail:placeObject];
    }else{
        [self.delegate toPlaceDetail:[_placeList objectAtIndex:row]];
//        cell.place = [_placeList objectAtIndex:indexPath.row];
    }
}
#pragma mark -UserPlaceCollectioncellDelegate
- (void)showUserList:(Place *)place{
    [self.delegate toUserList:place];
}
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
     
   [[MapsNetworkManage sharedInstance] beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    
    [[MapsNetworkManage sharedInstance] wantGoAction:place callBack:place1];
}

@end
