//
//  UserPostViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserPostViewModel.h"
#import "MeNetworkManage.h"
#import "UserPost.h"
#import "QAlertView.h"
#import "PostFrameObject.h"
@interface UserPostViewModel ()
@property(nonatomic,strong)UserParameter *postParameter;
@property(nonatomic,strong)User *user;
@property(nonatomic,strong)PostFrameObject *postFrame;
@end
@implementation UserPostViewModel
- (UserParameter *)postParameter{
    if(_postParameter == nil){
        _postParameter = [[UserParameter alloc] init];
    }
    return _postParameter;
}
- (void)dealloc{
    NSLog(@"UserPostViewModel");
}
#pragma mark -UserinforViewModelDataSource
- (void)getDataFooter:(BOOL)footer withCollectionView:(UICollectionView *)collectionView WithUser:(User *)user{
    self.user = user;
    self.postParameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
    if(footer){
        PostObjet *post = [_postList lastObject];
        self.postParameter.last_id = [NSString stringWithFormat:@"%li",(long)post.id];
    }else{
        [collectionView.footer resetNoMoreData];
        self.postParameter.last_id = @"0";
    }
    [[MeNetworkManage sharedInstance] getMyPostListWithParameter:self.postParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
                  NSMutableArray *array;
                if(self.user.isMyInformation){
                    [UserPost clearCache];
                    array = [UserPost savePostData:obj];
                }else{
                    array = [PostObjet dicToObjectWithArray:obj];
                }
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             NSMutableArray *postsArr = [[NSMutableArray alloc] initWithCapacity:array.count];
            if(self.user.isMyInformation){
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    PostObjet *postObject =  [PostObjet coreDataToObject:obj];
                    [postsArr addObject:postObject];
                }];
            }else postsArr = [NSMutableArray arrayWithArray:array];
                if(footer){
                    [_postList insertObjects:postsArr atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_postList.count, postsArr.count)]];
                }else{
                    _postList = [NSMutableArray arrayWithArray:postsArr];
                }
                 _postFrame = [[PostFrameObject alloc] initWithPostList:_postList];
        
//                dispatch_async(dispatch_get_main_queue(), ^{
                    if(footer)[collectionView.footer endRefreshing];
                    else [collectionView.header endRefreshing];
                    if(array.count<[self.postParameter.count integerValue]){
                        [collectionView.footer endRefreshingWithNoMoreData];
                    }
                [collectionView.collectionViewLayout invalidateLayout];
                    [collectionView reloadData];
//                });
//            });
            
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(footer)[collectionView.footer endRefreshing];
            else {
                if(self.user.isMyInformation){
                    _postList = [NSMutableArray arrayWithArray:[UserPost getAllPost]];
                        NSMutableArray *postsArr = [[NSMutableArray alloc] initWithCapacity:_postList.count];
                        [_postList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            PostObjet *postObject =  [PostObjet coreDataToObject:obj];
                            [postsArr addObject:postObject];
                        }];
                    _postList = postsArr;
                    _postFrame = [[PostFrameObject alloc] initWithPostList:_postList];
                        [collectionView.header endRefreshing];
                     [collectionView.collectionViewLayout invalidateLayout];
                        [collectionView reloadData];
                }else{
                [collectionView.header endRefreshing];
                [collectionView.collectionViewLayout invalidateLayout];
                [collectionView reloadData];
                }
            }
        }
    }];
}

- (CGFloat)widthForItemInCollectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout{
    return (kDeviceWidth-30)/2;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row<=1) return 5;
    CGFloat height =[_postFrame.myPostItemHeights[indexPath.row-2] floatValue];
    return height;
}
- (NSInteger)numberOfItemsInCollectionView:(UICollectionView *)collectionView{
    return _postList.count+2;
}
- (UICollectionViewCell *)cellForItemAtCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row<=1){
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"blankCell"];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"blankCell" forIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        return cell;
    }
    UserPostCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserPostCollectionViewCell forIndexPath:indexPath];
    PostObjet *postObjb = _postList[indexPath.row-2];
    cell.postObject = postObjb;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexRow:(NSInteger)row{
    PostObjet *post = _postList[row-2];
    [self.delegate toPostDetail:post];
}

@end
