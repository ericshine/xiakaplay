//
//  MyMapsListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/6/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MyMapsListViewModel.h"
#import "MyMapsListHeadTableCell.h"
#import "MapsHomeCell.h"
#import "Maps.h"
#import "MapDetailListViewController.h"
static NSString* const KmyMapsListHeadTableCellIdentify = @"myMapsListHeadTableCellIdentify";
@interface MyMapsListViewModel()<MapsHomeCellDelegate>
@property(nonatomic,assign) BJSuperViewController *controller;
@end
@implementation MyMapsListViewModel{
    NSArray *mapsList;
}
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller{
    self = [super init];
    if(self){
        self.controller = controller;
        mapsList = [Maps getMapsList];
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return mapsList.count +1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 200;
    }
    return 312+73;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        MyMapsListHeadTableCell *cell = [tableView dequeueReusableCellWithIdentifier:KmyMapsListHeadTableCellIdentify];
        if(!cell){
            cell = [[MyMapsListHeadTableCell alloc] initWithStyle:0 reuseIdentifier:KmyMapsListHeadTableCellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleLabel.text = @"榜单";
        }
        return cell;
    }else{
        Maps *maps = mapsList[indexPath.section];
        MapsHomeCell *cell = [MapsHomeCell initCellWithTabel:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        cell.indexPath = indexPath;
        cell.maps = maps;
        return cell;
    }
}
#pragma mark - MapsHomeCellDelegate
- (void)touchMapsWithModel:(id)model{
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    [self.controller push:detailList];
}
@end
