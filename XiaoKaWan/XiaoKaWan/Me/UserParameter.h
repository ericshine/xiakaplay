//
//  UserParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserParameter : NSObject
@property(nonatomic,copy) NSString *uid;
@property(nonatomic,copy) NSString *count;
@property(nonatomic,copy) NSString *get_type;
@property(nonatomic,copy) NSString *last_id;
@property(nonatomic,copy) NSString *_t;
@end
