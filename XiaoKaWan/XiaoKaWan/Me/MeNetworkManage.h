//
//  MeNetworkManage.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "ClientNetwork.h"
#import "APIConfig.h"
#import <MJExtension.h>
#import "UserConfig.h"
#import "BJNotificationConfig.h"
#import "QAlertView.h"
#import "CurrentUser.h"
#import "UserParameter.h"
#define BJAPIDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)
@interface MeNetworkManage : NSObject
SINGLETON_H
/**
 *  获取用户信息
 *
 *  @param parameter uid
 *  @param complete  complete description
 */
- (void)getUserInforWithParameter:(UserParameter *)parameter complete:(requestComplete )complete;

/**
 *  获取粉丝列表
 *
 *  @param parameter uid,
 *  @param complete  complete description
 */
- (void)getFansListWithParameter:(UserParameter *)parameter complete:(requestComplete )complete;
/**
 *  获取关注的列表
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)getFollowersWithParameter:(UserParameter *)parameter complete:(requestComplete )complete;//followFollowingsApi
/**
 *  关注用户
 *
 *  @param parameter uid
 *  @param complete  complete description
 */
- (void)followUserWithParameter:(UserParameter *)parameter complete:(requestComplete )complete;
/**
 *  取消关注
 *
 *  @param parameter  uid ，token
 *  @param complete  complete description
 */
- (void)unFollowUserWithPararmeter:(UserParameter *)parameter complete:(requestComplete)complete;
/**
 *  获取我的动态
 *
 *  @param parameter uid
 *  @param complete  complete description
 */
- (void)getMyPostListWithParameter:(UserParameter *)parameter complete:(requestComplete)complete;
@end
