//
//  MeNetworkManage.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MeNetworkManage.h"

typedef void(^requestComplete)(BOOL succeed,id obj);
@implementation MeNetworkManage
SINGLETON_M(MeNetworkManage)


- (void)getUserInforWithParameter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].userInforApi parameter:parameter.keyValues complete:complete];
}
- (void)getFansListWithParameter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].myFansApi parameter:parameter.keyValues complete:complete];
}
//followFollowingsApi
- (void)getFollowersWithParameter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].followFollowingsApi parameter:parameter.keyValues complete:complete];
}
- (void)followUserWithParameter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].followAdddApi parameter:parameter.keyValues complete:complete];
}
- (void)unFollowUserWithPararmeter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].followDeleteApi parameter:parameter.keyValues complete:complete];
}
- (void)getMyPostListWithParameter:(UserParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].myPostListApi parameter:parameter.keyValues complete:complete];
}
@end
