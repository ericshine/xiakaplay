//
//  CommunityController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommunityController.h"
#import "UserConfig.h"
@interface CommunityController ()<UIWebViewDelegate>
@property(nonatomic,strong) UIWebView *webiew;
@end

@implementation CommunityController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self rightButton];
    _webiew=  [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64)];
    NSURL *url =[[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://192.168.3.23/sf/?access-token=%@",[UserConfig sharedInstance].user_token]];
    [_webiew loadRequest:[[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10]];
    _webiew.delegate  = self;
    [self.view addSubview:_webiew];
    // Do any additional setup after loading the view.
}
- (void)rightButton{
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(action1)];
     UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(action2)];
   
    self.navigationItem.leftBarButtonItems = @[item1];
    self.navigationItem.rightBarButtonItems=@[item2];
//    [self.navigationItem setLeftBarButtonItems:@[item1,item2]];
    
}
- (void)action1{
    if([_webiew canGoBack]) [_webiew goBack];
   
}
- (void)action2{
    [self popViewController];
}
#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
   NSURL *url =  [request URL];
    NSString *strUrl = url.absoluteString;
    if([strUrl containsString:@"access-token"])return YES;
    
    NSString *lastString = @"";
    if([strUrl containsString:@"#"]){
        NSArray *urlArray = [strUrl componentsSeparatedByString:@"#"];
        strUrl = urlArray[0];
        lastString = urlArray[1];
        lastString = [NSString stringWithFormat:@"#%@",lastString];
    }
    if(![strUrl containsString:@"?"]){
        strUrl = [NSString stringWithFormat:@"%@?random_2d=%f&access-token=%@%@",strUrl,[[NSDate date] timeIntervalSince1970],[UserConfig sharedInstance].user_token,lastString];
        NSMutableURLRequest *requestM = (NSMutableURLRequest*)request;
        [requestM setURL:[NSURL URLWithString:strUrl]];
        [webView loadRequest:requestM];
        return NO;
    }else{
        if(![strUrl containsString:@"access-token"]){
        strUrl = [NSString stringWithFormat:@"%@&random_2d=%f&access-token=%@%@",strUrl,[[NSDate date] timeIntervalSince1970],[UserConfig sharedInstance].user_token,lastString];
            NSMutableURLRequest *requestM = (NSMutableURLRequest*)request;
            [requestM setURL:[NSURL URLWithString:strUrl]];
            [webView loadRequest:requestM];
            return NO;
        }
    }
    return YES;
}
- (void)modifyRequest:(NSString *)requestStr{
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
