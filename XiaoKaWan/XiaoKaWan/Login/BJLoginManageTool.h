//
//  BJLoginManageTool.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "Singleton.h"
#import <UIKit/UIKit.h>
@class AppDelegate;
@interface BJLoginManageTool : NSObject
SINGLETON_H
- (void)loginManage;
- (void)logoutManage;
- (void)clearCahce;
- (AppDelegate*)appDeleage;
@end
