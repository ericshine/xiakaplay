//
//  RegistAccountObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegistAccountObject : NSObject
@property(nonatomic,copy) NSString *nick;
@property(nonatomic,copy) NSString *phoneNumber;
@property(nonatomic,copy) NSString *verfyNumber;
@property(nonatomic,copy) NSString *passwordNumber;
@property(nonatomic,copy) NSString *account;
@end
