#import "CurrentUser.h"
#import <MJExtension.h>
#import "UserConfig.h"
@interface CurrentUser ()

// Private interface goes here.

@end

@implementation CurrentUser

+(void)saveUserDataWithDic:(NSDictionary *)dic{
    CurrentUser *user = [CurrentUser getUser:dic[@"uid"]];
    if(user == nil){
        user = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[UserDataManage sharedInstance].context];
    }
    user = [user setKeyValues:dic];
//    user = [CurrentUser objectWithKeyValues:dic context:[UserDataManage sharedInstance].context];
    [[UserDataManage sharedInstance] saveContext];
}
+(CurrentUser*)getCurrentUser{
    if(![UserConfig sharedInstance].user_token.length) return nil;
    return [CurrentUser getUser:nil];
}
+ (CurrentUser *)getUser:(NSString *)userId{
    if(userId == nil){
        userId = [UserConfig sharedInstance].userId;
    }
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetch.predicate = [NSPredicate predicateWithFormat:@"uid = %@",userId];
    NSArray *results = [[UserDataManage sharedInstance].context executeFetchRequest:fetch error:nil];
    if(!results.count){
        return nil;
    }
    return results[0];
}
@end
