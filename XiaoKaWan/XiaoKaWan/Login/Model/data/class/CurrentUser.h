#import "_CurrentUser.h"
#import "UserDataManage.h"
@interface CurrentUser : _CurrentUser {}
+(void)saveUserDataWithDic:(NSDictionary*)dic;
+(CurrentUser*)getCurrentUser;

@end
