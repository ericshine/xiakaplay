// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CurrentUser.h instead.

#import <CoreData/CoreData.h>

extern const struct CurrentUserAttributes {
	__unsafe_unretained NSString *avatar;
	__unsafe_unretained NSString *been_count;
	__unsafe_unretained NSString *followers_count;
	__unsafe_unretained NSString *following_count;
	__unsafe_unretained NSString *maps_count;
	__unsafe_unretained NSString *maps_create_count;
	__unsafe_unretained NSString *mobile;
	__unsafe_unretained NSString *nick;
	__unsafe_unretained NSString *post_count;
	__unsafe_unretained NSString *sign;
	__unsafe_unretained NSString *uid;
	__unsafe_unretained NSString *want_count;
} CurrentUserAttributes;

@interface CurrentUserID : NSManagedObjectID {}
@end

@interface _CurrentUser : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CurrentUserID* objectID;

@property (nonatomic, strong) NSString* avatar;

//- (BOOL)validateAvatar:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* been_count;

@property (atomic) int32_t been_countValue;
- (int32_t)been_countValue;
- (void)setBeen_countValue:(int32_t)value_;

//- (BOOL)validateBeen_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* followers_count;

@property (atomic) int32_t followers_countValue;
- (int32_t)followers_countValue;
- (void)setFollowers_countValue:(int32_t)value_;

//- (BOOL)validateFollowers_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* following_count;

@property (atomic) int32_t following_countValue;
- (int32_t)following_countValue;
- (void)setFollowing_countValue:(int32_t)value_;

//- (BOOL)validateFollowing_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maps_count;

@property (atomic) int32_t maps_countValue;
- (int32_t)maps_countValue;
- (void)setMaps_countValue:(int32_t)value_;

//- (BOOL)validateMaps_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maps_create_count;

@property (atomic) int32_t maps_create_countValue;
- (int32_t)maps_create_countValue;
- (void)setMaps_create_countValue:(int32_t)value_;

//- (BOOL)validateMaps_create_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* mobile;

//- (BOOL)validateMobile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nick;

//- (BOOL)validateNick:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* post_count;

@property (atomic) int32_t post_countValue;
- (int32_t)post_countValue;
- (void)setPost_countValue:(int32_t)value_;

//- (BOOL)validatePost_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sign;

//- (BOOL)validateSign:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* uid;

@property (atomic) int32_t uidValue;
- (int32_t)uidValue;
- (void)setUidValue:(int32_t)value_;

//- (BOOL)validateUid:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* want_count;

@property (atomic) int32_t want_countValue;
- (int32_t)want_countValue;
- (void)setWant_countValue:(int32_t)value_;

//- (BOOL)validateWant_count:(id*)value_ error:(NSError**)error_;

@end

@interface _CurrentUser (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAvatar;
- (void)setPrimitiveAvatar:(NSString*)value;

- (NSNumber*)primitiveBeen_count;
- (void)setPrimitiveBeen_count:(NSNumber*)value;

- (int32_t)primitiveBeen_countValue;
- (void)setPrimitiveBeen_countValue:(int32_t)value_;

- (NSNumber*)primitiveFollowers_count;
- (void)setPrimitiveFollowers_count:(NSNumber*)value;

- (int32_t)primitiveFollowers_countValue;
- (void)setPrimitiveFollowers_countValue:(int32_t)value_;

- (NSNumber*)primitiveFollowing_count;
- (void)setPrimitiveFollowing_count:(NSNumber*)value;

- (int32_t)primitiveFollowing_countValue;
- (void)setPrimitiveFollowing_countValue:(int32_t)value_;

- (NSNumber*)primitiveMaps_count;
- (void)setPrimitiveMaps_count:(NSNumber*)value;

- (int32_t)primitiveMaps_countValue;
- (void)setPrimitiveMaps_countValue:(int32_t)value_;

- (NSNumber*)primitiveMaps_create_count;
- (void)setPrimitiveMaps_create_count:(NSNumber*)value;

- (int32_t)primitiveMaps_create_countValue;
- (void)setPrimitiveMaps_create_countValue:(int32_t)value_;

- (NSString*)primitiveMobile;
- (void)setPrimitiveMobile:(NSString*)value;

- (NSString*)primitiveNick;
- (void)setPrimitiveNick:(NSString*)value;

- (NSNumber*)primitivePost_count;
- (void)setPrimitivePost_count:(NSNumber*)value;

- (int32_t)primitivePost_countValue;
- (void)setPrimitivePost_countValue:(int32_t)value_;

- (NSString*)primitiveSign;
- (void)setPrimitiveSign:(NSString*)value;

- (NSNumber*)primitiveUid;
- (void)setPrimitiveUid:(NSNumber*)value;

- (int32_t)primitiveUidValue;
- (void)setPrimitiveUidValue:(int32_t)value_;

- (NSNumber*)primitiveWant_count;
- (void)setPrimitiveWant_count:(NSNumber*)value;

- (int32_t)primitiveWant_countValue;
- (void)setPrimitiveWant_countValue:(int32_t)value_;

@end
