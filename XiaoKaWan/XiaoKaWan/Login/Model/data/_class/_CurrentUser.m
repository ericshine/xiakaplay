// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CurrentUser.m instead.

#import "_CurrentUser.h"

const struct CurrentUserAttributes CurrentUserAttributes = {
	.avatar = @"avatar",
	.been_count = @"been_count",
	.followers_count = @"followers_count",
	.following_count = @"following_count",
	.maps_count = @"maps_count",
	.maps_create_count = @"maps_create_count",
	.mobile = @"mobile",
	.nick = @"nick",
	.post_count = @"post_count",
	.sign = @"sign",
	.uid = @"uid",
	.want_count = @"want_count",
};

@implementation CurrentUserID
@end

@implementation _CurrentUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CurrentUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CurrentUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CurrentUser" inManagedObjectContext:moc_];
}

- (CurrentUserID*)objectID {
	return (CurrentUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"been_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"been_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"followers_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"followers_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"following_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"following_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maps_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maps_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maps_create_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maps_create_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"post_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"post_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"uidValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"uid"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"want_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"want_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatar;

@dynamic been_count;

- (int32_t)been_countValue {
	NSNumber *result = [self been_count];
	return [result intValue];
}

- (void)setBeen_countValue:(int32_t)value_ {
	[self setBeen_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBeen_countValue {
	NSNumber *result = [self primitiveBeen_count];
	return [result intValue];
}

- (void)setPrimitiveBeen_countValue:(int32_t)value_ {
	[self setPrimitiveBeen_count:[NSNumber numberWithInt:value_]];
}

@dynamic followers_count;

- (int32_t)followers_countValue {
	NSNumber *result = [self followers_count];
	return [result intValue];
}

- (void)setFollowers_countValue:(int32_t)value_ {
	[self setFollowers_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFollowers_countValue {
	NSNumber *result = [self primitiveFollowers_count];
	return [result intValue];
}

- (void)setPrimitiveFollowers_countValue:(int32_t)value_ {
	[self setPrimitiveFollowers_count:[NSNumber numberWithInt:value_]];
}

@dynamic following_count;

- (int32_t)following_countValue {
	NSNumber *result = [self following_count];
	return [result intValue];
}

- (void)setFollowing_countValue:(int32_t)value_ {
	[self setFollowing_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFollowing_countValue {
	NSNumber *result = [self primitiveFollowing_count];
	return [result intValue];
}

- (void)setPrimitiveFollowing_countValue:(int32_t)value_ {
	[self setPrimitiveFollowing_count:[NSNumber numberWithInt:value_]];
}

@dynamic maps_count;

- (int32_t)maps_countValue {
	NSNumber *result = [self maps_count];
	return [result intValue];
}

- (void)setMaps_countValue:(int32_t)value_ {
	[self setMaps_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMaps_countValue {
	NSNumber *result = [self primitiveMaps_count];
	return [result intValue];
}

- (void)setPrimitiveMaps_countValue:(int32_t)value_ {
	[self setPrimitiveMaps_count:[NSNumber numberWithInt:value_]];
}

@dynamic maps_create_count;

- (int32_t)maps_create_countValue {
	NSNumber *result = [self maps_create_count];
	return [result intValue];
}

- (void)setMaps_create_countValue:(int32_t)value_ {
	[self setMaps_create_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMaps_create_countValue {
	NSNumber *result = [self primitiveMaps_create_count];
	return [result intValue];
}

- (void)setPrimitiveMaps_create_countValue:(int32_t)value_ {
	[self setPrimitiveMaps_create_count:[NSNumber numberWithInt:value_]];
}

@dynamic mobile;

@dynamic nick;

@dynamic post_count;

- (int32_t)post_countValue {
	NSNumber *result = [self post_count];
	return [result intValue];
}

- (void)setPost_countValue:(int32_t)value_ {
	[self setPost_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePost_countValue {
	NSNumber *result = [self primitivePost_count];
	return [result intValue];
}

- (void)setPrimitivePost_countValue:(int32_t)value_ {
	[self setPrimitivePost_count:[NSNumber numberWithInt:value_]];
}

@dynamic sign;

@dynamic uid;

- (int32_t)uidValue {
	NSNumber *result = [self uid];
	return [result intValue];
}

- (void)setUidValue:(int32_t)value_ {
	[self setUid:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUidValue {
	NSNumber *result = [self primitiveUid];
	return [result intValue];
}

- (void)setPrimitiveUidValue:(int32_t)value_ {
	[self setPrimitiveUid:[NSNumber numberWithInt:value_]];
}

@dynamic want_count;

- (int32_t)want_countValue {
	NSNumber *result = [self want_count];
	return [result intValue];
}

- (void)setWant_countValue:(int32_t)value_ {
	[self setWant_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveWant_countValue {
	NSNumber *result = [self primitiveWant_count];
	return [result intValue];
}

- (void)setPrimitiveWant_countValue:(int32_t)value_ {
	[self setPrimitiveWant_count:[NSNumber numberWithInt:value_]];
}

@end

