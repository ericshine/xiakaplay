//
//  CommitUserInforModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommitUserInforModel.h"
#import "CellObject.h"
@implementation CommitUserInforModel
- (instancetype)init{
    self = [super init];
    if(self){
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:3];
        NSArray *titles = @[@"头像",@"昵称",@"个人签名"];
        NSArray *detail = @[@"http://vimg3.ws.126.net/image/snapshot/2016/5/5/S/VBLI6CF5S.jpg",@"取个昵称呗",@"来个签名吧"];
        [titles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CellObject *object = [[CellObject alloc] init];
            object.title = obj;
            object.detail= detail[idx];
            [items addObject:object];
        }];
        self.listArray = [items copy];
    }
    return self;
}
@end
