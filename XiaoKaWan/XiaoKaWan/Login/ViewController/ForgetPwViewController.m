//
//  ForgetPwViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ForgetPwViewController.h"
#import "ForgetPWView.h"
#import "ForgetPasswordViewModel.h"
@interface ForgetPwViewController ()<ForgetPWViewDelegate>
@property(nonatomic,strong)ForgetPWView *forgetPasswordView;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,strong)ForgetPasswordViewModel *viewModel;
@end

@implementation ForgetPwViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableHeaderView = self.forgetPasswordView;
    self.tableView.delegate = self.forgetPasswordView;
    // Do any additional setup after loading the view.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self timerStop];
}
- (ForgetPWView *)forgetPasswordView{
    if(_forgetPasswordView == nil){
        _forgetPasswordView =[[ForgetPWView alloc] initWithFrame:self.view.frame];
        _forgetPasswordView.delegate = self;
        _forgetPasswordView.dataSource = self.viewModel;
    }
    return _forgetPasswordView;
}
- (ForgetPasswordViewModel*)viewModel{
    if(_viewModel == nil){
        _viewModel = [[ForgetPasswordViewModel alloc] initWithController:self];
    }
    return _viewModel;
}
- (NSTimer *)timer{
    if(_timer == nil){
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:_forgetPasswordView.mainView.verfyNumberTextField selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}

#pragma mark - ForgetPWViewDelegate
- (void)verfyStart{
    [self timer];
}
- (void)verfyStop{
    [self timerStop];
}
- (void)timerStop{
    [self.timer invalidate];
    self.timer = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
