//
//  SignUpAccountViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SignUpAccountViewController.h"
#import "SignUpAccountView.h"
#import "CommitUserInformationController.h"
#import "SiginUpViewModel.h"
@interface SignUpAccountViewController ()<SignUpAccountViewDelegate>
@property(nonatomic,strong) SignUpAccountView *signUpView;
@property(nonatomic,strong) SiginUpViewModel *viewModel;
@property(nonatomic,strong) NSTimer *timer;
@end

@implementation SignUpAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableHeaderView = self.signUpView;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = _signUpView;
    // Do any additional setup after loading the view.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopTimer];
}
- (SignUpAccountView*)signUpView
{
    if(_signUpView==nil){
        _signUpView = [[SignUpAccountView alloc] initWithFrame:self.view.frame];
        _signUpView.delegate = self;
        _signUpView.datadSource = self.viewModel;
    }
    return _signUpView;
}
- (SiginUpViewModel*)viewModel{
    if(_viewModel == nil){
        _viewModel = [[SiginUpViewModel alloc] initWithController:self];
    }
    return _viewModel;
}
- (NSTimer *)timer{
    if(_timer == nil){
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self.signUpView.mainView.verfyNumberTextField selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}
#pragma mark -SignUpAccountViewDelegate
- (void)signUpSuccess{
    CommitUserInformationController *commitUserVc = [CommitUserInformationController alloc];
    [self push:commitUserVc];
}
- (void)verfyStart{
    [self timer];
}
- (void)verfyStop{
    [self stopTimer];
}
- (void)stopTimer{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
