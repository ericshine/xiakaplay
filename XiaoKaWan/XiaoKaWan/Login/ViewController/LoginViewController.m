//
//  LoginViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginView.h"
#import "LoginVewModel.h"
#import "ServiceUrl.h"
@interface LoginViewController ()
@property(nonatomic,strong)LoginView *loginView;
@property(nonatomic,strong)LoginVewModel *viewModel;
@property(nonatomic,strong) NSTimer *timer;
@end

@implementation LoginViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self enableNavigationOpaque];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [self disenableNavigationOpaque];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [_timer invalidate];
    _timer = nil;
}
- (void)serviceChange{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"线上环境",@"测试环境"]];
    segment.selectedSegmentIndex = 1;
    [[ServiceUrl sharedInstance] changeServiceWithIndex:DevelopmentService];

    segment.frame = CGRectMake(200, kDeviceHeight-200, 150, 30);
    [self.view addSubview:segment];
    [segment addTarget:self action:@selector(changeService:) forControlEvents:UIControlEventValueChanged];
    self.view.userInteractionEnabled= YES;
    
}
- (void)changeService:(UISegmentedControl *)segment{
    if(segment.selectedSegmentIndex == 0){
        [[ServiceUrl sharedInstance] changeServiceWithIndex:ProductionService];
    }else{
        [[ServiceUrl sharedInstance] changeServiceWithIndex:DevelopmentService];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self leftButtonWithTitle:NSLocalizedString(@"skip", nil)];
    [self loginView];
    //[self serviceChange];
//    [self timer];
    // Do any additional setup after loading the view.
}


- (LoginVewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[LoginVewModel alloc] initWithController:self];
    }
    return _viewModel;
}
- (LoginView *)loginView{
    if(_loginView ==nil){
    _loginView = [[LoginView alloc] initWithFrame:self.view.frame];
        _loginView.delegate = self.viewModel;
        [self.view addSubview:_loginView];
    }
    return _loginView;
}

#pragma mark - LoginVewModelDelegate
- (void)changeColors:(NSArray *)colors{
//    self.loginView.gradientLayer.colors = colors;
}

//- (BOOL)prefersStatusBarHidden{
//    return YES;
//}

- (void)leftClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
