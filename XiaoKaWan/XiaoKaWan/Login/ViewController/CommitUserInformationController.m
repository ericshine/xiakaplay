//
//  CommitUserInformationController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommitUserInformationController.h"
#import "CommitUserInforViewModel.h"
#import "CommitUserInforModel.h"
@interface CommitUserInformationController ()
@property(nonatomic,strong)CommitUserInforViewModel *viewModel;
@property(nonatomic,strong)CommitUserInforModel *model;
@end

@implementation CommitUserInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}
- (CommitUserInforModel*)model{
    if(_model == nil){
        _model = [[CommitUserInforModel alloc] init];
    }
    return _model;
}
- (CommitUserInforViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[CommitUserInforViewModel alloc] initWithModel:self.model viewController:self];
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
