//
//  SignUpAccountView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SignUpAccountView.h"

#import "UIColor+colorWithHex.h"
#import "UILabel+initLabel.h"
#import "ProtocolButton.h"
#import <Masonry.h>
#import "QAlertView.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define leftSpace 35
@interface SignUpAccountView()<ChangePasswordViewDelegae>

@property(nonatomic,strong) UIButton *commitButton;
//@property(nonatomic,strong) UILabel *pwNoteLabel;
@property(nonatomic,strong) ProtocolButton *protocolButton;
@property(nonatomic,strong) UILabel *errorLabel;
@property(nonatomic,strong) RegistAccountObject *countObject;
@end
@implementation SignUpAccountView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        _countObject = [[RegistAccountObject alloc] init];
        self.nickTextField.leftTitle = @"昵称:";
        [self mainView];
//        [self pwNoteLabel];
        [self protocolButton];
        [self commitButton];
        [self errorLabel];
    }
    return self;
}
//- (void)layoutSubviews{
//    [super layoutSubviews];
//    [self.errorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.commitButton.mas_bottom).offset(56);
//        make.left.equalTo(self.mas_left).offset(leftSpace);
//        make.right.equalTo(self.mas_right).offset(-leftSpace);
//    }];
//}
- (LeftRightViewTextField *)nickTextField{
    if(_nickTextField == nil){
        _nickTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(leftSpace+10, 56, CGRectGetWidth(self.frame)-leftSpace*2, 35)];
        [self addSubview:_nickTextField];
        _nickTextField.returnKeyType = UIReturnKeyNext;
        _nickTextField.delegate = self;
        _nickTextField.tag = 15;
        _nickTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _nickTextField.attStrPlaceholder = @"4-16字符,可由中英文,数字,“_”组成";
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_nickTextField.frame)+7.5,  CGRectGetWidth(self.frame)-leftSpace*2, 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _nickTextField;
}
- (ChangePasswordView *)mainView{
    if(_mainView == nil){
        _mainView =[[ChangePasswordView alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_nickTextField.frame)+15, CGRectGetWidth(self.frame)-leftSpace*2, 150)];
        _mainView.delegate = self;
        [self addSubview:_mainView];
    }
    return _mainView;
}
//- (UILabel*)pwNoteLabel
//{
//    if(_pwNoteLabel==nil){
//        _pwNoteLabel = [UILabel creatLabelWithFont:10 color:0x7e7e7e];
//        _pwNoteLabel.frame = CGRectMake(leftSpace+10, CGRectGetMaxY(_mainView.frame), 200, 15);
//        _pwNoteLabel.text = @"密码包含字母、数字且长度在6-16位数";
//        [self addSubview:_pwNoteLabel];
//    }
//    return _pwNoteLabel;
//}
- (ProtocolButton *)protocolButton{
    if(_protocolButton == nil){
        _protocolButton = [[ProtocolButton alloc] initWithFrame: CGRectMake(leftSpace+10, CGRectGetMaxY(_mainView.frame)+30, 150, 30)];
        [self addSubview:_protocolButton];
//        _protocolButton.backgroundColor = [UIColor redColor];
    }
    return _protocolButton;
}
- (UIButton*)commitButton
{
    if(_commitButton==nil){
        _commitButton = [[UIButton alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_protocolButton.frame)+100, CGRectGetWidth(self.frame)-leftSpace*2, 44)];
        _commitButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_commitButton setTitle:@"注 册 并 登 录" forState:0];
        _commitButton.layer.cornerRadius = 5;
        [_commitButton setBackgroundColor:[UIColor p_colorWithHex:0xffda44]];
        [_commitButton addTarget:self action:@selector(commitAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_commitButton];
    }
    return _commitButton;
}
- (UILabel*)errorLabel
{
    if(_errorLabel==nil){
        _errorLabel = [UILabel creatLabelWithFont:16 color:0xfe2624];
        _errorLabel.frame = CGRectMake(leftSpace, CGRectGetMaxY(_commitButton.frame)+56, CGRectGetWidth(self.frame)-leftSpace*2, 20);
        _errorLabel.textAlignment = NSTextAlignmentCenter;
        _errorLabel.numberOfLines = 0;
//        [self addSubview:_errorLabel];
    }
    return _errorLabel;
}
- (void)commitAction:(UIButton *)button{
    [self hiddenKeyboard];
    if(!_protocolButton.isAgree){
        [[QAlertView sharedInstance] showAlertText:@"请先阅读协议" fadeTime:3];
        return;
    }
    _countObject.nick = self.nickTextField.text;
    _countObject.phoneNumber = self.mainView.phoneNumberTextField.text;
    _countObject.verfyNumber = self.mainView.verfyNumberTextField.text;
    _countObject.passwordNumber = self.mainView.passwordTextField.text;
    [self.datadSource signUpAction:_countObject error:^(NSString *error) {
        if(error == nil) return;
        [[QAlertView sharedInstance] showAlertText:error fadeTime:2];
    }];
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hiddenKeyboard];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self hiddenKeyboard];
}
- (void)hiddenKeyboard{
    [self.nickTextField resignFirstResponder];
    [self.mainView hiddenKeyboard];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.mainView.phoneNumberTextField becomeFirstResponder];
    return YES;
}
#pragma mark - ChangePasswordViewDelegae
- (void)verfyButtonAction:(void (^)(BOOL))isStart{
    [self hiddenKeyboard];
    self.countObject.phoneNumber = self.mainView.phoneNumberTextField.text;
    if([self.datadSource respondsToSelector:@selector(getVerfyAction:error:)]){
        [self.datadSource getVerfyAction:self.countObject error:^(NSString *error,BOOL success) {
            if(isStart)isStart(success);
            if(success){
                if([self.delegate respondsToSelector:@selector(verfyStart)]){
                    [self.delegate verfyStart];
                }
            }else{
                [[QAlertView sharedInstance] showAlertText:error fadeTime:2];
            }
           
        }];
    }
}
- (void)verfyStop{
    if([self.delegate respondsToSelector:@selector(verfyStop)]){
        [self.delegate verfyStop];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
