//
//  ForgetPWView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordView.h"
#import "RegistAccountObject.h"
@protocol ForgetPWViewDelegate <NSObject>

- (void)verfyStart;
- (void)verfyStop;

@end
@protocol ForgetPWViewDataSource <NSObject>
- (void)changePwAction:(RegistAccountObject*)countObject error:(void(^)(NSString *error))error;
- (void)getVerfyAction:(RegistAccountObject*)countObject error:(void(^)(NSString *error,BOOL success))error;

@end
@interface ForgetPWView : UIView<UITableViewDelegate>
@property(nonatomic,strong) ChangePasswordView *mainView;
@property(nonatomic,assign) id<ForgetPWViewDelegate>delegate;
@property(nonatomic,assign) id<ForgetPWViewDataSource>dataSource;
@end

