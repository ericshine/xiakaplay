//
//  ProtocolButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ProtocolButton.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@interface ProtocolButton()
@property(nonatomic,strong) UIButton *selectIcon;
@property(nonatomic,strong) UIButton *buttonSelect;
@property(nonatomic,strong) UIButton *buttonCheck;
@end
@implementation ProtocolButton
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.isAgree = YES;
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.selectIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
    }];
    [self.buttonSelect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.selectIcon.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
    [self.buttonCheck mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.buttonSelect.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (UIButton*)selectIcon
{
    if(_selectIcon==nil){
        _selectIcon = [self creatButtonWithFont:12 color:[UIColor p_colorWithHex:0x7e7e7e]];
        [_selectIcon setTitleColor:[UIColor p_colorWithHex:0xfdc700] forState:UIControlStateSelected];
        _selectIcon.titleLabel.font = [UIFont fontWithName:@"iconfont" size:18];
        [_selectIcon setTitle:@"\U0000e60e" forState:0];
        _selectIcon.tag = 40;
        _selectIcon.selected = YES;
        [self addSubview:_selectIcon];
    }
    return _selectIcon;
}
- (UIButton*)buttonSelect
{
    if(_buttonSelect==nil){
        _buttonSelect = [self creatButtonWithFont:12 color:[UIColor p_colorWithHex:0x7e7e7e]];
        [_buttonSelect setTitle:@"同意" forState:0];
        _buttonSelect.tag = 41;
        [self addSubview:_buttonSelect];
    }
    return _buttonSelect;
}
- (UIButton*)buttonCheck
{
    if(_buttonCheck==nil){
        _buttonCheck = [self creatButtonWithFont:12 color:[UIColor p_colorWithHex:0x0050dd]];
        [_buttonCheck setTitle:@"小咖玩用户注册协议" forState:0];
        _buttonCheck.tag = 42;
        _buttonCheck.selected = YES;
        [self addSubview:_buttonCheck];
    }
    return _buttonCheck;
}
- (UIButton*)creatButtonWithFont:(CGFloat)font color:(UIColor*)color{
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:color forState:0];
    button.titleLabel.font = [UIFont systemFontOfSize:font];
    [button addTarget:self action:@selector(buttonSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
- (void)buttonSelectAction:(UIButton*)button{
    if(button.tag !=42){
    button.selected = !button.selected;
    _selectIcon.selected = button.selected;
        self.isAgree = _selectIcon.selected;
    }else{
    
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
