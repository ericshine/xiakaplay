//
//  SignUpAccountView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordView.h"
#import "RegistAccountObject.h"
@protocol SignUpAccountViewDelegate <NSObject>
- (void)verfyStart;
- (void)verfyStop;
- (void)signUpSuccess;

@end
@protocol SignUpAccountViewDataSource <NSObject>

- (void)signUpAction:(RegistAccountObject*)countObject error:(void(^)(NSString *error))error;
- (void)getVerfyAction:(RegistAccountObject*)countObject error:(void(^)(NSString *error,BOOL success))error;
@end
@interface SignUpAccountView : UIView<UITextFieldDelegate,UITableViewDelegate>
@property(nonatomic,strong) ChangePasswordView *mainView;
@property(nonatomic,strong)LeftRightViewTextField *nickTextField;
@property(nonatomic,assign)id<SignUpAccountViewDelegate>delegate;
@property(nonatomic,assign)id<SignUpAccountViewDataSource>datadSource;
@end
