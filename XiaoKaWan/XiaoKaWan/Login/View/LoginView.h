//
//  LoginView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftRightViewTextField.h"
#import "RegistAccountObject.h"
@protocol LoginViewDelegate <NSObject>
/**
 *  不登陆
 */
- (void)skipSign;
/**
 *  登陆
 */
- (void)signInAction:(nullable RegistAccountObject*)accountObject;
/**
 *  注册
 */
- (void)signUpAction;
/**
 *  找回密码
 */
- (void)forgetAction;

@end
@interface LoginView : UIView<UITextFieldDelegate>

//@property(nonatomic,strong) CAGradientLayer *gradientLayer;
//@property(nonatomic,strong) UILabel *<#name#>;
@property(nullable,nonatomic,strong)LeftRightViewTextField *phoneNumberTextField;
@property(nullable,nonatomic,strong)LeftRightViewTextField *passwordTextField;
@property(nullable,nonatomic,strong) UIButton *signInButton;
@property(nullable,nonatomic,strong) UIButton *signUpButton;
@property(nullable,nonatomic,strong) UIButton *skipButton;
@property(nullable,nonatomic,strong) UIButton *forgetButton;
@property(nullable,nonatomic,assign) id<LoginViewDelegate>delegate;
@end
