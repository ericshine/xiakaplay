//
//  ForgetPWView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ForgetPWView.h"
#import "UIColor+colorWithHex.h"
#import "UILabel+initLabel.h"
#import "QAlertView.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define leftSpace 35
@interface ForgetPWView()<ChangePasswordViewDelegae>
@property(nonatomic,strong) UIButton *commitButton;
@property(nonatomic,strong) RegistAccountObject *account;
@property(nonatomic,strong) UILabel *errorLabel;
@end
@implementation ForgetPWView{
    
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.account = [[RegistAccountObject alloc] init];
        [self mainView];
        [self commitButton];
    }
    return self;
}
- (ChangePasswordView *)mainView{
    if(_mainView == nil){
        _mainView =[[ChangePasswordView alloc] initWithFrame:CGRectMake(leftSpace, 56, CGRectGetWidth(self.frame)-leftSpace*2, 150)];
        _mainView.delegate = self;
        [self addSubview:_mainView];
    }
    return _mainView;
}
- (UIButton*)commitButton
{
    if(_commitButton==nil){
        _commitButton = [[UIButton alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_mainView.frame)+110, CGRectGetWidth(self.frame)-leftSpace*2, 44)];
        _commitButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_commitButton setTitle:@"完成" forState:0];
        _commitButton.layer.cornerRadius = 5;
        [_commitButton setBackgroundColor:[UIColor p_colorWithHex:0xffda44]];
        [_commitButton addTarget:self action:@selector(finishAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_commitButton];
    }
    return _commitButton;
}
- (UILabel*)errorLabel
{
    if(_errorLabel==nil){
        _errorLabel = [UILabel creatLabelWithFont:16 color:0xfe2624];
        _errorLabel.frame = CGRectMake(leftSpace, CGRectGetMaxY(_commitButton.frame)+56, CGRectGetWidth(self.frame)-leftSpace*2, 20);
        _errorLabel.textAlignment = NSTextAlignmentCenter;
        _errorLabel.numberOfLines = 0;
//        [self addSubview:_errorLabel];
    }
    return _errorLabel;
}

- (void)finishAction:(UIButton *)button{
    self.account.phoneNumber = self.mainView.phoneNumberTextField.text;
    self.account.verfyNumber = self.mainView.verfyNumberTextField.text;
    self.account.passwordNumber = self.mainView.passwordTextField.text;
    
    [self.dataSource changePwAction:self.account error:^(NSString *error) {
//        self.errorLabel.text = error;
        [[QAlertView sharedInstance] showAlertText:error fadeTime:2];
    }];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.mainView hiddenKeyboard];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.mainView hiddenKeyboard];
}
#pragma mark -ChangePasswordViewDelegae
- (void)verfyButtonAction:(void (^)(BOOL))isStart{
    self.account.phoneNumber = self.mainView.phoneNumberTextField.text;
    [self.dataSource getVerfyAction:self.account error:^(NSString *error, BOOL success) {
//        self.errorLabel.text = error;
        if(isStart)isStart(success);
        if(success){
        if([self.delegate respondsToSelector:@selector(verfyStart)]){
            [self.delegate  verfyStart];
        }
        }else{
        [[QAlertView sharedInstance] showAlertText:error fadeTime:2];
        }
    }];
    
}
- (void)verfyStop{
    if([self.delegate respondsToSelector:@selector(verfyStop)]){
        [self.delegate  verfyStop];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
