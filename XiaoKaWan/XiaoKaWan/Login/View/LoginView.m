//
//  LoginView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LoginView.h"
#import "BJKenburnsView.h"
#import "UIImage+Category.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "NSString+Cagetory.h"
#import "UserConfig.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define leftSpace 35
static CGFloat const signButtonHeight = 44;
@interface LoginView()
@property(nonatomic,strong) BJKenburnsView *animationView;
@property(nonatomic,strong) RegistAccountObject *account;
@end
@implementation LoginView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.account = [[RegistAccountObject alloc] init];
        self.phoneNumberTextField.leftTitle = @"账号:";
        self.passwordTextField.leftTitle = @"密码:";
        self.phoneNumberTextField.text =  [UserConfig sharedInstance].account;
        [self signInButton];
        [self signUpButton];
        [self forgetButton];
//        [self animationView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.skipButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(30);
        make.left.equalTo(self.mas_left).offset(0);
    }];
//    [self.signInButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top).offset(300);
//        make.left.equalTo(self.mas_left).offset(20);
//        make.right.equalTo(self.mas_right).offset(-20);
//        make.height.equalTo(@(signButtonHeight));
//    }];
}
- (LeftRightViewTextField *)phoneNumberTextField{
    if(_phoneNumberTextField == nil){
        _phoneNumberTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(leftSpace, 120, self.frame.size.width-leftSpace*2-10, leftSpace)];
        _phoneNumberTextField.returnKeyType= UIReturnKeyNext;
        _phoneNumberTextField.delegate = self;
        _phoneNumberTextField.placeholder = @"手机号";
        _phoneNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_phoneNumberTextField];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_phoneNumberTextField.frame)+7.5,  CGRectGetWidth(self.frame)-leftSpace*2, 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _phoneNumberTextField;
}
- (LeftRightViewTextField *)passwordTextField{
    if(_passwordTextField == nil){
        _passwordTextField = [[LeftRightViewTextField alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_phoneNumberTextField.frame)+15, self.frame.size.width-leftSpace*2-10, leftSpace)];
        _passwordTextField.returnKeyType = UIReturnKeyGo;
        _passwordTextField.delegate = self;
        _passwordTextField.placeholder = @"6～16位密码";
        [self addSubview:_passwordTextField];
        _passwordTextField.isShowSecureButton = YES;
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, CGRectGetMaxY(_passwordTextField.frame)+7.5, CGRectGetWidth(self.frame)-leftSpace*2, 0.5)];
        line.backgroundColor = [UIColor p_colorWithHex:0xb6b6b6];
        [self addSubview:line];
        
    }
    return _passwordTextField;
}
- (UIButton*)signInButton
{
    if(_signInButton==nil){
        _signInButton = [self creatButtonWithTitle:NSLocalizedString(@"signIn", nil) font:20 andColor:[UIColor whiteColor]];
//        NSMutableAttributedString *attStr = [NSLocalizedString(@"signIn", nil) getAttributedStringWithSpace:20 font:[UIFont systemFontOfSize:20] color:[UIColor whiteColor]];
//        [_signInButton setAttributedTitle:attStr forState:UIControlStateNormal];
        
        _signInButton.frame = CGRectMake(leftSpace, CGRectGetMaxY(_passwordTextField.frame)+110, CGRectGetWidth(self.frame)-leftSpace*2, signButtonHeight);
        _signInButton.tag = 20;
        _signInButton.layer.cornerRadius = 5;
        [_signInButton setBackgroundColor:[UIColor p_colorWithHex:0xffda44]];
        [self addSubview:_signInButton];
    }
    return _signInButton;
}
- (UIButton*)signUpButton
{
    if(_signUpButton==nil){
        _signUpButton = [self creatButtonWithTitle:NSLocalizedString(@"signUp", nil) font:13 andColor:[UIColor p_colorWithHex:0x979797]];
        _signUpButton.tag = 21;
        _signUpButton.frame = CGRectMake(leftSpace+5, CGRectGetMaxY(_signInButton.frame)+22, 80, 20);
        [self addSubview:_signUpButton];
    }
    return _signUpButton;
}
- (UIButton*)forgetButton
{
    if(_forgetButton==nil){
        _forgetButton = [self creatButtonWithTitle:@"忘记密码" font:13 andColor:[UIColor p_colorWithHex:0x979797]];
        _forgetButton.frame = CGRectMake(CGRectGetMaxX(_signInButton.frame)-65, CGRectGetMaxY(_signInButton.frame)+22, 60, 20);
        _forgetButton.tag = 22;
        [self addSubview:_forgetButton];
    }
    return _forgetButton;
}
- (UIButton*)skipButton
{
    if(_skipButton==nil){
        _skipButton = [self creatButtonWithTitle:NSLocalizedString(@"\U0000e613", nil) font:20 andColor:[UIColor blackColor]];
        _skipButton.tag = 23;
        _skipButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
     [self addSubview:_skipButton];
        _skipButton.transform = CGAffineTransformMakeScale(0.2, 0.2);
        [UIView animateWithDuration:.7 animations:^{
            _skipButton.transform = CGAffineTransformMakeScale(1, 1);
        }];
    }
    return _skipButton;
}
- (UIButton *)creatButtonWithTitle:(NSString *)title font:(NSInteger)font andColor:(UIColor*)color{
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:font];
     [button addTarget:self action:@selector(buttonTouchDownAction:) forControlEvents:UIControlEventTouchDown];
     [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchCancel];
     [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchDragExit];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
- (void)buttonTouchDownAction:(UIButton *)button{
    [UIView animateKeyframesWithDuration:0.2 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        button.transform = CGAffineTransformMakeScale(0.9, 0.8);
    } completion:^(BOOL finished) {
        
    }];
}
- (void)buttonAction:(UIButton *)button{
    [self hiddenKeybord];
    [UIView animateWithDuration:0.2 animations:^{
        button.transform =CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if(button.tag ==20){
            [self loginAction];
        }else if (button.tag ==21){
            [self.delegate signUpAction];
        }
        else if (button.tag ==22){
            [self.delegate forgetAction];
        }
        else if (button.tag== 23){
            [self.delegate skipSign];
        }
       
    }];
   
}
- (void)loginAction{
    self.account.account = self.phoneNumberTextField.text;
    self.account.passwordNumber = self.passwordTextField.text;
    [self.delegate signInAction:self.account];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hiddenKeybord];
}
- (void)hiddenKeybord{
    [self.phoneNumberTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}
- (BJKenburnsView*)animationView{
    if(_animationView == nil){
        _animationView = [[BJKenburnsView alloc] initWithFrame:self.frame];
//        _animationView.image = [UIImage imageNamed:@"IMG_1275.jpg"];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                UIImage *image = [[UIImage imageNamed:@"backImage"] applyLightEffect];
            dispatch_async(dispatch_get_main_queue(), ^{
                 _animationView.image = image;
            });
            });
        [self addSubview:_animationView];
        UIToolbar *toobar = [[UIToolbar alloc] initWithFrame:self.frame];
        toobar.barStyle = UIBarStyleBlackTranslucent;
        [_animationView addSubview:toobar];
    }
    return _animationView;
}
#pragma mark - delegate textField
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyNext){
        [self.passwordTextField becomeFirstResponder];
    }else{
        [self loginAction];
    }
    return YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
