//
//  CommitUserInforViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CommitUserInforModel.h"
@interface CommitUserInforViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
- (instancetype)initWithModel:(CommitUserInforModel*)model viewController:(id)viewController;
@end
