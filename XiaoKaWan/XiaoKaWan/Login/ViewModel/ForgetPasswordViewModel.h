//
//  ForgetPasswordViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
#import "ForgetPWView.h"
@interface ForgetPasswordViewModel : NSObject<ForgetPWViewDataSource>
@property(nonatomic,assign) BJSuperViewController*controller;
- (instancetype)initWithController:(BJSuperViewController*)controller;
@end
