//
//  ForgetPasswordViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ForgetPasswordViewModel.h"
#import "ClientNetwork.h"
#import "QAlertView.h"
@implementation ForgetPasswordViewModel
- (instancetype)initWithController:(BJSuperViewController *)controller{
    self = [super init];
    if(self){
        self.controller = controller;
    }
    return self;
}
//- (void)getVerfy:()
#pragma mark - ForgetPWViewDataSource
- (void)changePwAction:(RegistAccountObject *)countObject error:(void (^)(NSString *))error{
    if(countObject.phoneNumber.length<11){
        if(error)error(@"请输入正确的手机号");
            return;
    }
    if(countObject.verfyNumber.length<3){
        if(error)error(@"请输入正确的验证码");
        return;
    }if(countObject.passwordNumber.length<5){
        if(error)error(@"密码格式不正确");
        return;
    }
    // network
     NSDictionary *parameter = @{@"mobile":countObject.phoneNumber,@"captcha":countObject.verfyNumber,@"password":countObject.passwordNumber};
    [self changePw:parameter error:^(NSString *errorString) {
        if(error)error(errorString);
    }];
    
}
- (void)getVerfyAction:(RegistAccountObject *)countObject error:(void (^)(NSString *, BOOL))error{
    if(countObject.phoneNumber.length<11){
        if(error)error(@"请输入正确的手机号",NO);
        return;
    }
    [self getVerfy:@{@"mobile":countObject.phoneNumber,@"type":@"resetpassword"}  error:^(NSString *errorString, bool isStart) {
        if(error)error(errorString,isStart);
    }];
    
    
}
#pragma mark - network
- (void)getVerfy:(NSDictionary *)parameter error:(void(^)(NSString *errorString,bool isStart))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].registerVerfy parameter:parameter complete:^(BOOL succeed, id obj) {
        if(errorString)errorString(succeed==YES?@"":obj,succeed);
    }];
}
- (void)changePw:(NSDictionary *)parameter error:(void(^)(NSString *errorString))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].forgetPassworkApi parameter:parameter complete:^(BOOL succeed, id obj) {
        
        if(succeed){
            [[QAlertView sharedInstance] showAlertText:@"修改成功" fadeTime:2];
            [self.controller popViewController];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
}
@end
