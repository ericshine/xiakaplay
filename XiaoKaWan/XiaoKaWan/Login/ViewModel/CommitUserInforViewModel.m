//
//  CommitUserInforViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommitUserInforViewModel.h"
#import "CellObject.h"
#import "SettingHeadCell.h"
#import <UIImageView+WebCache.h>
#import "ChangeNameViewController.h"
#import "SignatureViewController.h"
#import "BJSuperViewController.h"
@interface CommitUserInforViewModel()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property(nonatomic,strong)CommitUserInforModel *model;
@property(nonatomic,assign)BJSuperViewController *viewController;
@property(nonatomic,strong)UIImagePickerController *imagePicker;
@end
@implementation CommitUserInforViewModel
- (instancetype)initWithModel:(CommitUserInforModel *)model viewController:(id)viewController{
    self = [super init];
    if(self){
        self.model = model;
        self.viewController = viewController;
    }
    return self;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.model.listArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return 62;
    return 47;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellObject *cellObj = self.model.listArray[indexPath.row];
    static NSString *cellIdentify = @"cellIdentify";
    SettingHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell = [[SettingHeadCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentify];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = cellObj.title;
    if(indexPath.row == 0){
        [cell.rightImageView sd_setImageWithURL:[NSURL URLWithString:cellObj.detail] placeholderImage:nil];
    } else{
        cell.detailTextLabel.text = cellObj.detail;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0){
        [self selectPhoto];
    }else if (indexPath.row == 1){
        ChangeNameViewController *changeName = [[ChangeNameViewController alloc] init];
        [self.viewController push:changeName];
    }else if (indexPath.row == 2){
        SignatureViewController *signature = [[SignatureViewController alloc] init];
        [self.viewController push:signature];
    }else if (indexPath.row == 3){

    }

}
- (void)selectPhoto{
    UIAlertController *sheetVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"拍照" style:0 handler:^(UIAlertAction * _Nonnull action) {
        [self selelctImageFromAlbumOrCamera:UIImagePickerControllerSourceTypeCamera];
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"从相册中选取" style:0 handler:^(UIAlertAction * _Nonnull action) {
        [self selelctImageFromAlbumOrCamera:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [sheetVc addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self.viewController presentViewController:sheetVc animated:YES completion:nil];
}
- (void)selelctImageFromAlbumOrCamera:(UIImagePickerControllerSourceType)sourceType{
    if(_imagePicker == nil){
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    _imagePicker.sourceType = sourceType;
    [self.viewController presentViewController:_imagePicker animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
  //  UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
}
@end
