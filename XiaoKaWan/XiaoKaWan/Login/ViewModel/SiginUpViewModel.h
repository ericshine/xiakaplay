//
//  SiginUpViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
#import "SignUpAccountView.h"
@interface SiginUpViewModel : NSObject<SignUpAccountViewDataSource>
- (instancetype)initWithController:(BJSuperViewController*)controller;
@end
