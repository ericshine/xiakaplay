//
//  LoginVewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJSuperViewController.h"
#import "LoginView.h"
@protocol LoginVewModelDelegate <NSObject>



@end
@interface LoginVewModel : NSObject<LoginViewDelegate>
@property(nonatomic,assign)BJSuperViewController *controller;
@property(nonatomic,assign) id<LoginVewModelDelegate>delegate;
- (instancetype)initWithController:(BJSuperViewController*)controller;
@end
