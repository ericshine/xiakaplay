//
//  SiginUpViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "SiginUpViewModel.h"
#import "QAlertView.h"
#import "ClientNetwork.h"
#import "BJNotificationConfig.h"
#import "UserConfig.h"
#import "CurrentUser.h"
#import "UMessage.h"
#import "BJNotificationDBTool.h"
#import "BJDynamicDBTool.h"
#import "BJContactTool.h"
@interface SiginUpViewModel()
@property(nonatomic,assign)BJSuperViewController *controller;
@end
@implementation SiginUpViewModel
- (instancetype)initWithController:(BJSuperViewController *)controller{
    self = [super init];
    if(self){
        self.controller = controller;
    }
    return self;
}
#pragma mark - SignUpAccountViewDataSource
- (void)signUpAction:(RegistAccountObject *)countObject error:(void (^)(NSString *))error{
    if(countObject.nick.length <1){
        if(error)error(@"昵称格式不正确");
        return;
    }
    if(countObject.phoneNumber.length <1){
        if(error)error(@"请输入正确手机号");
        return;
    }
    if(countObject.verfyNumber.length <1){
        if(error)error(@"请输入正确验证码");
        return;
    }
    if(countObject.passwordNumber.length <6){
        if(error)error(@"请输入正确密码");
        return;
    }
    if(error)error(nil);
    NSDictionary *parameter = @{@"mobile":countObject.phoneNumber,@"captcha":countObject.verfyNumber,@"nick":countObject.nick,@"password":countObject.passwordNumber};
    [self regisetAndLogin:parameter error:^(NSString *errorString) {
        if(error)error(errorString);
    }];
}
- (void)getVerfyAction:(RegistAccountObject *)countObject error:(void (^)(NSString *, BOOL))error{
    if(countObject.phoneNumber.length <1){
        if(error)error(@"请输入正确手机号",NO);
        return;
    }
    
    [self registerVerfy:@{@"mobile":countObject.phoneNumber,@"type":@"register"} error:^(NSString *errorString,BOOL isStart) {
        if(error)error(errorString,isStart);
    }];
}
//- (void)getVerfyAction:(RegistAccountObject *)countObject error:(void (^)(NSString *))error{
//    
//}
- (void)registerSuccessLogin{
    [[NSNotificationCenter defaultCenter] postNotificationName:BJLoginNotification object:nil];
    [self.controller dismissViewControllerAnimated:YES completion:nil];
   
}
#pragma mark - network
- (void)registerVerfy:(NSDictionary *)parameter error:(void(^)(NSString *errorString,BOOL isStart))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].registerVerfy parameter:parameter complete:^(BOOL succeed, id obj) {
        if(errorString)errorString(succeed==YES?@"":obj,succeed);
    }];
}
- (void)regisetAndLogin:(NSDictionary *)parameter error:(void(^)(NSString *errorString))errorString{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].registerAndLogin parameter:parameter complete:^(BOOL succeed, id obj) {
        if(succeed) {
            [UserConfig sharedInstance].user_token = obj[@"token"];
            [UserConfig sharedInstance].userId = obj[@"userinfo"][@"uid"];
            if([UserConfig sharedInstance].userId == nil ||[UserConfig sharedInstance].userId.length<=0)return ;
            [CurrentUser saveUserDataWithDic:obj[@"userinfo"]];
            [self registerSuccessLogin];
            //友盟推送，绑定别名
            [UMessage setAlias:obj[@"userinfo"][@"uid"] type:@"UID" response:^(id responseObject, NSError *error) {
            }];
            [BJNotificationDBTool initialize];
            [BJDynamicDBTool initialize];
            [BJContactTool initialize];
        }else{
            if(errorString)errorString(obj);
        }
    }];
}
@end
