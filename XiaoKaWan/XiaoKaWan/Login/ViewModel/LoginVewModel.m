//
//  LoginVewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/25/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "LoginVewModel.h"
#import "ForgetPwViewController.h"
#import "SignUpAccountViewController.h"
#import "ClientNetwork.h"
#import "QAlertView.h"
#import "CurrentUser.h"
#import "UserConfig.h"
#import "BJNotificationConfig.h"
#import "UMessage.h"
#import "BJNotificationDBTool.h"
#import "BJDynamicDBTool.h"
#import "BJContactTool.h"
@implementation LoginVewModel
- (instancetype)initWithController:(BJSuperViewController *)controller{
    self = [super init];
    if(self){
        self.controller = controller;
    }
    return self;
}
#pragma mark - network
- (void)loginAction:(RegistAccountObject*)account{
    if(account.account.length<1){
        [[QAlertView sharedInstance] showAlertText:@"请输入正确的账号" fadeTime:2];
        return;
    }
    if(account.passwordNumber.length<6){
        [[QAlertView sharedInstance] showAlertText:@"请输入正确的密码" fadeTime:2];
        return;
    }
    NSDictionary *parameter = @{@"mobile":account.account,@"password":account.passwordNumber};
   // @synchronized (self) {
        [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].loginApi parameter:parameter complete:^(BOOL succeed, id obj) {
            if(succeed) {
                [UserConfig sharedInstance].account = account.account;
                [UserConfig sharedInstance].user_token = obj[@"token"];
                [UserConfig sharedInstance].userId = obj[@"userinfo"][@"uid"];
//                NSLog(@"userId:%@",[UserConfig sharedInstance].userId);
                if([UserConfig sharedInstance].userId == nil ||[UserConfig sharedInstance].userId.length<=0)return ;
                [CurrentUser saveUserDataWithDic:obj[@"userinfo"]];
                [self.controller dismissViewControllerAnimated:YES completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:BJLoginNotification object:nil];
                //友盟推送，绑定别名
                [UMessage setAlias:obj[@"userinfo"][@"uid"] type:@"UID" response:^(id responseObject, NSError *error) {
                }];
                [BJNotificationDBTool initialize];
                [BJDynamicDBTool initialize];
                [BJContactTool initialize];
            }else{
                [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            }
        }];
  //  }
}
#pragma mark - LoginViewDelegate
-(void)skipSign{
    [self.controller dismissViewControllerAnimated:YES completion:nil];
}
- (void)signUpAction{
    SignUpAccountViewController *signUpVc = [[SignUpAccountViewController alloc] init];
    [self.controller push:signUpVc];
}
- (void)signInAction:(RegistAccountObject *)accountObject{
    [self loginAction:accountObject];
}

- (void)forgetAction{
    ForgetPwViewController *forgetPWVc = [[ForgetPwViewController alloc] init];
    [self.controller push:forgetPWVc];
}

@end
