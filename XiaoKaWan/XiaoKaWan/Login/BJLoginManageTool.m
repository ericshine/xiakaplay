//
//  BJLoginManageTool.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJLoginManageTool.h"
#import "BJTabBarViewController.h"
#import "LoginViewController.h"
#import "BJNavigationController.h"
#import "UserConfig.h"
#import "MapsModelManage.h"
#import "UserDataManage.h"
#import "AppDelegate.h"

@implementation BJLoginManageTool
SINGLETON_M(BJLoginManageTool)
- (void)loginManage{
    [self.appDeleage mainTaBarController];
}
- (void)logoutManage{
    [self clearCahce];
    [self.appDeleage loginViewController];
    
}
- (void)clearCahce{

    [UserConfig sharedInstance].user_token = nil;
    [UserConfig sharedInstance].userId = nil;
    
    [[MapsModelManage sharedInstance] clearContext];
    [[UserDataManage sharedInstance] clearContext];
    
    
}
- (AppDelegate*)appDeleage{
    return (AppDelegate*)([UIApplication sharedApplication].delegate);
}
@end
