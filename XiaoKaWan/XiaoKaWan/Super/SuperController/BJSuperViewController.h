//
//  BJSuperViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeadFileConfig.h"
@interface BJSuperViewController : UIViewController
- (instancetype)initWithTableStyle:(UITableViewStyle)style;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic) BOOL hiddenNav;
/**
 *  添加导航栏 右按钮
 *
 *  @param title 按钮title
 */
- (void)rightButtonWithTitle:(NSString *)title;
- (void)rightButtonWithIconfont:(NSString *)title;
- (void)rightButtonWithIconfont:(NSString *)title title1:(NSString *)title1;
- (void)rightButtonWithIconfont:(NSString *)title1 color1:(unsigned int)color1 title1:(NSString *)title2 color2:(unsigned int)color2;
/**
 *  点击右按钮调用方法
 */
- (void)rightClick;
- (void)leftButtonWithTitle:(NSString *)title;
- (void)leftButtonWithIconfont:(NSString *)title;
- (void)leftClick;
- (void)rightClick1;
/**
 *  重写 push
 *
 *  @param viewController viewController description
 */
- (void)push:(UIViewController *)viewController;
- (void)popViewController;
/**
 *  点击返回按钮 返回到倒数第几级
 *
 *  @param viewController viewController which for pushing
 *  @param count          return to which controller
 */
- (void)push:(UIViewController *)viewController removeChildWithCount:(NSInteger)count;
/**
 *  导航栏透明
 */
- (void)enableNavigationOpaque;
/**
 *  导航栏不透明
 */
/**
 *  跳转登录页面到
 *
 *  @param clearData bool 是否要清除缓存数据
 */
- (void)toLoginVcClearData:(BOOL)clearData;
- (void)disenableNavigationOpaque;
- (void)rightButtonWithColor:(unsigned int)color title:(NSString *)title;
@end
