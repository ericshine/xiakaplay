//
//  BJSuperViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "UINavigationController+Category.h"
#import "UIImage+Category.h"
#import "BJNavigationController.h"
#import "LoginViewController.h"
#import "BJLoginManageTool.h"

@interface BJSuperViewController ()<UINavigationControllerDelegate>
@property(nonatomic) UITableViewStyle tableStyle;
@end

@implementation BJSuperViewController

- (instancetype)initWithTableStyle:(UITableViewStyle)style
{
    self = [super init];
    _tableStyle = style;
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BJNeedUserLoginNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(neetLoginNoti:) name:BJNeedUserLoginNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BJNeedUserLoginNotification object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"";
//    self.navigationController.delegate = self;
    // Do any additional setup after loading the view.
}
- (void)neetLoginNoti:(NSNotification *)noti{
    [self toLoginVcClearData:NO];
}
- (void)rightButtonWithTitle:(NSString *)title{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(rightClick)];
}
- (void)rightButtonWithIconfont:(NSString *)title{
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   // [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    [button addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}
- (void)rightButtonWithIconfont:(NSString *)title title1:(NSString *)title1{
    [self rightButtonWithIconfont:title color1:0x000000 title1:title1 color2:0x000000];
}
- (void)rightButtonWithIconfont:(NSString *)title1 color1:(unsigned int)color1 title1:(NSString *)title2 color2:(unsigned int)color2{
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
    [button setTitle:title1 forState:UIControlStateNormal];
    [button setTitleColor:[UIColor p_colorWithHex:color1] forState:UIControlStateNormal];
    // [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    [button addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * button1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button1.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
    [button1 setTitle:title2 forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor p_colorWithHex:color2] forState:UIControlStateNormal];
    // [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    [button1 addTarget:self action:@selector(rightClick1) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *item1 =  [[UIBarButtonItem alloc] initWithCustomView:button];
    UIBarButtonItem *item2 =  [[UIBarButtonItem alloc] initWithCustomView:button1];
    self.navigationItem.rightBarButtonItems = @[item2,item1];
}
- (void)rightClick{
    
}
- (void)rightClick1{
    
}
- (void)leftButtonWithTitle:(NSString *)title{
 self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(leftClick)];
}
- (void)leftButtonWithIconfont:(NSString *)title{
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    // [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    [button addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}
- (void)rightButtonWithColor:(unsigned int)color title:(NSString *)title{
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:title];
    NSShadow *shadow = [[NSShadow alloc]init];
    shadow.shadowOffset = CGSizeMake(2, 2);
    shadow.shadowBlurRadius = 3;
    shadow.shadowColor = [UIColor p_colorWithHex:0x810810];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:14],NSFontAttributeName,shadow,NSShadowAttributeName,nil];
    [attrStr setAttributes:dic range:NSMakeRange(0, attrStr.length)];
//    textlabel.attributedText = attrStr;
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    button.backgroundColor = [UIColor p_colorWithHex:color];
    button.layer.cornerRadius = 5;
    [button setAttributedTitle:attrStr forState:UIControlStateNormal];
    [button addTarget:self action:@selector(rightClickAnimation:) forControlEvents:UIControlEventTouchUpInside];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    [rightView addSubview:button];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightView];
}
- (void)rightClickAnimation:(UIButton*)button{
   
    [UIView animateWithDuration:0.2 animations:^{
        button.transform = CGAffineTransformMakeScale(0.9, 0.9);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
           button.transform = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                button.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                [self rightClick];
            }];
            
        }];
        
    }];
}
- (void)leftClick{

}
- (void)push:(UIViewController *)viewController{
    [self.navigationController push:viewController];
}
- (void)push:(UIViewController *)viewController removeChildWithCount:(NSInteger)count{
    [self.navigationController push:viewController];
   NSInteger lastIndex = [self.navigationController.childViewControllers indexOfObject:self];
    NSMutableArray *removings = [[NSMutableArray alloc] initWithCapacity:count];
    for(int i=0;i<count;i++){
       
        UIViewController *controller = [self.navigationController.childViewControllers objectAtIndex:lastIndex-i];
        [removings addObject:controller];
    }
    [removings enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         [obj removeFromParentViewController];
    }];
}
- (void)popViewController{
    [self.navigationController popViewController];
}
- (void)enableNavigationOpaque{
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
}
- (void)disenableNavigationOpaque
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
}
- (void)toLoginVcClearData:(BOOL)clearData{
   if(clearData) [[BJLoginManageTool sharedInstance] clearCahce];
    LoginViewController *loginVc = [[LoginViewController alloc] init];
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform t = CGAffineTransformMakeTranslation(0, 100);
        self.tabBarController.view.transform = CGAffineTransformScale(t, 0.9, 0.9);
        [UIView animateWithDuration:0.1 animations:^{
            
            [self presentViewController:[[BJNavigationController alloc] initWithRootViewController:loginVc] animated:YES completion:nil];
        } completion:^(BOOL finished) {
            
        }];
    } completion:^(BOOL finished) {
        self.tabBarController.view.transform = CGAffineTransformIdentity;
    }];
}
#pragma mark - add Table
- (UITableView*)tableView{
    if(_tableView == nil){
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) style:_tableStyle];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (void)dealloc{
    NSLog(@"dealloc:%@",NSStringFromClass([self class]));
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -UINavigationControllerDelegate
//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
//    BJSuperViewController *controller = (BJSuperViewController *)viewController;
//     [self.navigationController setNavigationBarHidden:controller.hiddenNav animated:YES];
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
