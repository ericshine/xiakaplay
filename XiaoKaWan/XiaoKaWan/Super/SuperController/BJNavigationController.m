//
//  BJNavigationController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJNavigationController.h"
#import "UIImage+Category.h"
@interface BJNavigationController ()

@end

@implementation BJNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage imageFromColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
}
//- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    [super pushViewController:viewController animated:animated];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
