//
//  BJSuperObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperObject.h"

@implementation BJSuperObject
- (UIViewController *)getControllerAtTabIndex:(NSInteger)index vcName:(NSString *)controllerName{
  BJNavigationController *nav = [UIApplication sharedApplication].keyWindow.rootViewController.childViewControllers[index];
  UIViewController *controller = [nav childViewControllerFromStr:controllerName];
    return controller;
}
@end
