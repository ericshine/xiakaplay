//
//  BJSuperObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HeadFileConfig.h"
#import "BJNavigationController.h"
@interface BJSuperObject : NSObject
- (UIViewController *)getControllerAtTabIndex:(NSInteger)index vcName:(NSString *)controllerName;
@end
