//
//  BJTextField.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTextField.h"
#import <objc/runtime.h>
 static NSString * const BJFieldDidDeleteBackwardNotification = @"BJFieldDidDeleteBackwardNotification";
@implementation BJTextField
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        Method method1 = class_getInstanceMethod([self class], NSSelectorFromString(@"deleteBackward"));
        Method method2 = class_getInstanceMethod([self class], @selector(wj_deleteBackward));
        method_exchangeImplementations(method1, method2);
    }
    return self;
}
- (void)wj_deleteBackward {
    [self wj_deleteBackward];
    [self.bjdelegate textDidDeleteText:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
