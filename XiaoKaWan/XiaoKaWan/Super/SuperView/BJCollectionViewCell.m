//
//  BJCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJCollectionViewCell.h"

@implementation BJCollectionViewCell
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xc9c9c9];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)showLineWithLeft:(CGFloat)leftOffset rightOffset:(CGFloat)rightOffset{
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left).offset(leftOffset);
        make.right.equalTo(self.mas_right).offset(-rightOffset);
        make.height.equalTo(@1);
    }];
}
@end
