//
//  BJCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeadFileConfig.h"
@interface BJCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UIView *lineView;
- (void)showLineWithLeft:(CGFloat)leftOffset rightOffset:(CGFloat)rightOffset;
@end
