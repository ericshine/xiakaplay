//
//  BJTextField.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeadFileConfig.h"
@protocol BJTextFieldDelegate <NSObject>
@optional
- (void)textDidDeleteText:(UITextField*)textField;

@end
@interface BJTextField : UITextField
@property(nonatomic,assign) id<BJTextFieldDelegate>bjdelegate;
@end

