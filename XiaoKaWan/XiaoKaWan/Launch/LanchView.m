//
//  LanchView.m
//  LaunchScreenAnimation-Storyboard
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 Cave. All rights reserved.
//

#import "LanchView.h"
//#import "uidt"o
@interface LanchView ()<UIDynamicAnimatorDelegate,UICollisionBehaviorDelegate>

@property(nonatomic,strong) UIDynamicAnimator *dynamicAnimator;
@property(nonatomic,strong) UIGravityBehavior *gravity;
@property(nonatomic,strong) UIPushBehavior *push;
@property(nonatomic,strong)UICollisionBehavior *collision;
@property(nonatomic,strong) UIDynamicItemBehavior *itemBehavior;
@property(nonatomic,strong)UILabel *testLabel;
@end
@implementation LanchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor blackColor];
        _testLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(frame)/2, 200, 200, 20)];
        _testLabel.textColor = [UIColor redColor];
        _testLabel.text = @"eric";
        [self addSubview:_testLabel];
        [self dynamicAnimator];
        [self gravity];
        [self collision];
        [self itemBehavior];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [UIView animateWithDuration:1 animations:^{
//                
//            } completion:^(BOOL finished) {
//                [UIView animateWithDuration:1 animations:^{
//                    _testLabel.transform = CGAffineTransformMakeTranslation(100, 0);
//                } completion:^(BOOL finished) {
//                    [UIView animateWithDuration:1 animations:^{
//                        _testLabel.transform = CGAffineTransformMakeTranslation(0, 100);
//                    } completion:^(BOOL finished) {
//                        [UIView animateWithDuration:2.0f delay:0.5f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//                            self.alpha = 0.0f;
//                            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 2.0f, 2.0f, 1.0f);
//                        } completion:^(BOOL finished) {
//                            [self removeFromSuperview];
//                        }];
//                    }];
//                }];
//
//            }];
//            
//        });
    }
    return self;
}
- (UIDynamicAnimator *)dynamicAnimator
{
    if(_dynamicAnimator == nil){
        _dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
        _dynamicAnimator.delegate = self;
    }
    return _dynamicAnimator;
}
- (UIPushBehavior *)push
{
    if(_push == nil){
        _push = [[UIPushBehavior alloc] initWithItems:@[_testLabel] mode:UIPushBehaviorModeContinuous];
//        [_push setAngle:0 magnitude:10];
        [_push setMagnitude:0.98];
//        [_push setPushDirection:CGVectorMake(10, 0)];
        [self.dynamicAnimator addBehavior:_push];
    }
    return _push;
}
- (UIGravityBehavior*)gravity
{
    if(_gravity==nil){
        _gravity = [[UIGravityBehavior alloc] initWithItems:@[_testLabel]];
        [_gravity setMagnitude:0.98];
//        [_gravity setAngle:0.5];
        [self.dynamicAnimator addBehavior:_gravity];
    }
    return _gravity;
}
- (UICollisionBehavior*)collision
{
    if(_collision==nil){
        _collision = [[UICollisionBehavior alloc] initWithItems:@[_testLabel]];
        [_collision setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(0, 0, 200, 0)];
        _collision.collisionDelegate = self;
//        [_collision setTranslatesReferenceBoundsIntoBoundary:YES];
        [self.dynamicAnimator addBehavior:_collision];
    }
    return _collision;
}
- (UIDynamicItemBehavior*)itemBehavior
{
    if(_itemBehavior == nil){
        _itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[_testLabel]];
        [_itemBehavior setElasticity:0.5];
        [self.dynamicAnimator addBehavior:_itemBehavior];
    }
    return _itemBehavior;
}
- (void)dynamicAnimatorWillResume:(UIDynamicAnimator *)animator
{

}
- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator
{
    [self removeFromSuperview];
}
- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item1 withItem:(id<UIDynamicItem>)item2 atPoint:(CGPoint)p
{

}
- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p
{
//   [self push];
}
- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier
{
//    [_push removeItem:_testLabel];
}
- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id<UIDynamicItem>)item1 withItem:(id<UIDynamicItem>)item2

{

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
