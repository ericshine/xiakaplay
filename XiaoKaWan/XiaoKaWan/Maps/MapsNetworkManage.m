//
//  MapsNetworkManage.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsNetworkManage.h"
#import "APIConfig.h"
#import <MJExtension.h>
#import "UserConfig.h"
#import "BJNotificationConfig.h"
#import "QAlertView.h"
#import "CurrentUser.h"
@implementation MapsNetworkManage
SINGLETON_M(MapsNetworkManage)

- (void)getMapsDetailWithParameter:(MapDetailNetworkParameter *)parametar complete:(requestComplete)complete{
    if([self loginState]) parametar._t = [UserConfig sharedInstance].user_token;
    else parametar._t = nil;
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].mapsDetail parameter:parametar.keyValues complete:complete];
}
- (void)getMapsPlaceListWithParameter:(MapDetailNetworkParameter *)parametar complete:(requestComplete)complete{
    if([self loginState]) parametar._t = [UserConfig sharedInstance].user_token;
    else parametar._t = nil;
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].mapsPlacelist parameter:parametar.keyValues complete:complete];
}
- (void)readMapsWithParameter:(MapObject *)parametar complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].readMapsDetailApi parameter:@{@"maps_id":parametar.id} complete:complete];
}
- (void)followWithParameter:(MapObject *)parametar complete:(requestComplete)complete{
     if(![self isLogin])return;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].mapsFollowApi parameter:@{@"maps_id":parametar.id,@"is_remove":[NSNumber numberWithBool:parametar.isFollow],@"_t":[UserConfig sharedInstance].user_token} complete:complete];
}
- (void)wantGoWithParameter:(Place *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    NSDictionary *paraDic =@{
                             @"place_id":parameter.placeId,
                             @"is_remove":[NSNumber numberWithBool:parameter.isWant],
                             @"_t":[UserConfig sharedInstance].user_token
                             };
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].mapsPlaceWantGoApi parameter:paraDic complete:complete];
}
- (void)wantGoAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] wantGoWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJWantActionNotification object:nil];
            place.isWant = !place.isWant;
            NSInteger offNumber;
            if(place.isWant){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                [users addObject:[self getCurrentUser]];
                place.wantUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
                NSArray *userswant = [users filteredArrayUsingPredicate:predicate];
                place.wantUsers = userswant;
            }
            place.wantCount = place.wantCount+offNumber;
            
            if(place1)place1(place);
        };
    }];
}
#pragma mark currentUser
- (User *)getCurrentUser{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    User *user  = [[User alloc] init];
    user.nick = currentUser.nick;
    user.avatar = currentUser.avatar;
    user.uid = [currentUser.uid integerValue];
    return user;
}
- (void)beenWithParameter:(Place *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    NSDictionary *paraDic =@{
                             @"place_id":parameter.placeId,
                             @"is_remove":[NSNumber numberWithBool:parameter.isBeen],
                             @"_t":[UserConfig sharedInstance].user_token
                             };
     [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].mapsPlaceBeenApi parameter:paraDic complete:complete];
}
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] beenWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJBeenActionNotification object:nil];
            place.isBeen = !place.isBeen;
            NSInteger offNumber;
            if(place.isBeen){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                [users addObject:[self getCurrentUser]];
                place.beenUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
                NSArray *usersBeen = [users filteredArrayUsingPredicate:predicate];
                place.beenUsers = usersBeen;
            }
            place.beenCount = place.beenCount+offNumber;
            
            if(place1)place1(place);
        };
    }];
}

- (void)getMapsPostWithParameter:(MapsPostNetoworkParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].mapsPostListApi parameter:parameter.keyValues complete:complete];
}
- (void)getWantUserListWithParameter:(MapsPostNetoworkParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].wantUserListApi parameter:parameter.keyValues complete:complete];
}
- (void)getbeenUserListWithParameter:(MapsPostNetoworkParameter *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].beenUserListApi parameter:parameter.keyValues complete:complete];
}
- (void)getMapPostDetailWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete{
    if([self loginState]) parameter._t = [UserConfig sharedInstance].user_token;
    else parameter._t = nil;
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].mapsPostDetailApi parameter:parameter.keyValues complete:complete];
}
- (void)commentPostWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].commentPostApi parameter:parameter.keyValues complete:complete];
}

- (void)postLikeWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].postLikeApi parameter:parameter.keyValues complete:complete];

}
- (void)commentLikeWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].commentLikeApi parameter:parameter.keyValues complete:complete];
    
}
- (void)getPlaceDetailWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete{
    if([self loginState]) parameter._t = [UserConfig sharedInstance].user_token;
    else parameter._t = nil;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].placeDetailApi parameter:parameter.keyValues complete:complete];
}
- (void)getPlaceMapsListWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete{
    if([self loginState]) parameter._t = [UserConfig sharedInstance].user_token;
    else parameter._t = nil;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].placeMapsListApi parameter:parameter.keyValues complete:complete];
}
- (void)getPlacePostListWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete{
    if([self loginState]) parameter._t = [UserConfig sharedInstance].user_token;
    else parameter._t = nil;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].placePostListApi parameter:parameter.keyValues complete:complete];
}
- (void)deletePostWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete{
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].deletePostApi parameter:parameter.keyValues complete:complete];
}
- (void)getMyMapsListWithParameter:(MapsPostNetoworkParameter *)parameter complete:(requestComplete)complete{
    
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].myMapsListApi parameter:parameter.keyValues complete:complete];
}
- (void)getMyPlceListWithParameter:(PlaceParameter *)parameter complete:(requestComplete)complete{
    
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].myPlacelistApi parameter:parameter.keyValues complete:complete];
}
- (void)addPlaceToMapsWithParameter:(PlaceParameter *)parameter complete:(requestComplete)complete{
    if(![self isLogin])return;
    parameter._t = [UserConfig sharedInstance].user_token;
    [[ClientNetwork sharedInstance] postWithPath:[APIConfig sharedInstance].placeAddToMapsApi parameter:parameter.keyValues complete:complete];
}
#pragma  mark -  -------
- (BOOL)isLogin{
    if([UserConfig sharedInstance].user_token == nil ||[UserConfig sharedInstance].user_token.length<1){
        [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedUserLoginNotification object:nil];
        [[QAlertView sharedInstance] showAlertText:@"请先登录" fadeTime:2];
        return NO;
    }
    return YES;
}
- (BOOL)loginState{
    if([UserConfig sharedInstance].user_token == nil ||[UserConfig sharedInstance].user_token.length<1){
        return NO;
    }
    return YES;
}
@end
