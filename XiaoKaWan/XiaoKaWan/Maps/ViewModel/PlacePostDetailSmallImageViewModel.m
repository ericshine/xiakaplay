//
//  PlacePostDetailSmallImageViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailSmallImageViewModel.h"
#import "ImageCollectionViewCell.h"
#import "BJImageObject.h"
#import "UIColor+colorWithHex.h"
static NSString *const smallImageCollectin = @"smallImageCollectin";
@interface PlacePostDetailSmallImageViewModel ()
@property(nonatomic,strong)UICollectionView *collectionView;

@end
@implementation PlacePostDetailSmallImageViewModel
- (instancetype)initWithModel:(id)model collectionView:(UICollectionView *)colletionView{
    self = [self init];
    if(self){
        self.collectionView = colletionView;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake(50, 50);
        layout.minimumLineSpacing = 5;
        [colletionView setCollectionViewLayout:layout];
        [colletionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:smallImageCollectin];
        self.selectIndex = 0;
        
    }
    return self;
}
- (void)setImageList:(NSArray *)imageList{
    _imageList = imageList;
    if(imageList.count>0)
    [self.collectionView reloadData];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _imageList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BJImageObject *imageObject = _imageList[indexPath.row];
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:smallImageCollectin forIndexPath:indexPath];
    [cell.imageView setImageWithUrlString:[ImageSizeUrl image90_url:imageObject.url]];
    if(indexPath.row == _selectIndex){
        cell.layer.borderColor = [UIColor mainColor].CGColor;
        cell.layer.borderWidth = 2;
    }else cell.layer.borderWidth = 0;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == self.selectIndex)return;
    self.selectIndex = indexPath.row;
    [self.delegate selectItemtAtIndex:indexPath.row];
    [self.collectionView reloadData];
}
#pragma mark - PlacePostDetailBigImageViewModelDelegate
-(void)selectAtIndex:(NSInteger)index{
    self.selectIndex = index;
//    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    [self.collectionView reloadData];
    
}
@end
