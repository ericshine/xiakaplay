//
//  PlaceMapsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceMapsViewModel.h"
#import "PlaceMapsHeadTabCell.h"
#import "MapsHomeCell.h"
#import "Maps.h"
#import "MapDetailListViewController.h"
@interface PlaceMapsViewModel()<MapsHomeCellDelegate>
@property(nonatomic,strong) BJSuperViewController *controller;
@property(nonatomic,strong) NSArray *listArray;
@end
@implementation PlaceMapsViewModel
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller{
    self = [super init];
    if(self){
        self.controller = controller;
        _listArray = [Maps getMapsList];
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _listArray.count+1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section ==0)return 1;
    else return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) return 130;
    return 312+73;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    static NSString * headCelleId = @"PlaceMapsHeadTabCell";
    PlaceMapsHeadTabCell *cell = [tableView dequeueReusableCellWithIdentifier:headCelleId];
    if(!cell){
        cell = [[PlaceMapsHeadTabCell alloc] initWithStyle:0 reuseIdentifier:headCelleId];
        cell.selectionStyle  =0;
    }
    return cell;
    }else{
        MapsHomeCell *cell = [MapsHomeCell initCellWithTabel:tableView];
        cell.delegate = self;
        cell.indexPath = indexPath;
        cell.maps = _listArray[indexPath.section -1];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section != 0){
        [self toMapsDetail];
    }
}
-(void)toMapsDetail{
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    [self.controller push:detailList];
}
#pragma mark -MapsHomeCellDelegate
- (void)touchMapsWithModel:(id)model{
   [self toMapsDetail];
}
@end
