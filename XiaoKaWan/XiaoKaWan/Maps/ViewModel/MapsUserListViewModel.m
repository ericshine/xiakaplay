//
//  MapsUserListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsUserListViewModel.h"
#import "User.h"
#import <UIImageView+WebCache.h>
#import "MapsNetworkManage.h"
#import "MapsPostNetoworkParameter.h"
#import "BJRefreshGitFooter.h"
#import "QAlertView.h"
#import <MJExtension.h>
#import "UserListTableViewCell.h"
#import "MeNetworkManage.h"
#import "BJAlertController.h"
static CGFloat const rowHeight = 72;
static NSString *const cellIdentify = @"userCellId";
@interface MapsUserListViewModel()<UserListTableViewCellDelegate>
@property(nonatomic,strong) MapsPostNetoworkParameter *parameter;

@property(nonatomic,strong)NSMutableArray *userLists;


@end
@implementation MapsUserListViewModel
- (instancetype)initWithModel:(MapObject*)model listType:(UserListType)type table:(UITableView*)tableview {
    self = [super init];
    if(self){
        _table = tableview;
        _listType = type;
        _parameter = [[MapsPostNetoworkParameter alloc] init];
        _parameter.place_id = model.placeId;
        _parameter.count = @"20";
        _parameter._t = [UserConfig sharedInstance].user_token;
        [self getDataFooter:NO];
       BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
           [self getDataFooter:YES];
       }];
        tableview.footer = footer;
    }
    return self;
}
- (void)reloadData{
    [self getDataFooter:NO];
}
- (void)getDataFooter:(BOOL)footer{
    if(footer){
        User *user = [_userLists lastObject];
        _parameter.last_id = [NSString stringWithFormat:@"%li",(long)user.uid];
    }else{
        _parameter.last_id = @"0";
    }
    if(_listType == USERLISTTYPE_WANT){
        [[MapsNetworkManage sharedInstance] getWantUserListWithParameter:_parameter complete:^(BOOL succeed, id obj) {
            [self getData:obj isFooter:footer success:succeed];
        }];
    }
    else if (_listType == USERLISTTYPE_BEEN){
        [[MapsNetworkManage sharedInstance] getbeenUserListWithParameter:_parameter complete:^(BOOL succeed, id obj) {
            [self getData:obj isFooter:footer success:succeed];
        }];
    }
    
}

- (void)getData:(id)obj isFooter:(BOOL)footer success:(BOOL)success{
    if(!success){
        [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        return;
    }
    NSMutableArray *users = [[NSMutableArray alloc] initWithCapacity:0];
    [obj enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        User *user = [User objectWithKeyValues:obj];
        [users addObject:user];
    }];
    if(users.count< [_parameter.count integerValue]){
    [self.table.footer endRefreshingWithNoMoreData];
    }
    if(!footer){
        _userLists = [NSMutableArray arrayWithArray:[users mutableCopy]];
    }else{
        [_userLists insertObjects:users atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_userLists.count, users.count)]];
    }
    
    [self.table reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userLists.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    User *user = self.userLists[indexPath.row];
    UserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell = [[UserListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.delegate = self;
    }
    cell.user = user;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     User *user = self.userLists[indexPath.row];
    [self.delegate toUserInfor:user];
}
#pragma mark - UserListTableViewCellDelegate
- (void)followUser:(User *)user success:(void (^)(User *))user1{
    UIViewController * controller = [self findViewController:self.table];
    NSString *title;
    NSString *actionTitle;
    if(user.followed){
        title = @"确定取消关注？";
        actionTitle = @"不再关注";
    }else{
        title = @"将要关注此人";
        actionTitle = @"关注";
    }
    BJAlertController  *alterVc = [[BJAlertController alloc] initWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alterVc addDestructiveActionWithTitle:actionTitle handler:^{
        UserParameter *parameter = [[UserParameter alloc] init];
        parameter._t = [UserConfig sharedInstance].user_token;
        parameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
        if(user.followed){
            [[MeNetworkManage sharedInstance] unFollowUserWithPararmeter:parameter complete:^(BOOL succeed, id obj) {
                if(succeed){
                user.followed = [obj[@"followed"] boolValue];
                user.bi_followed = [obj[@"bi_followed"] boolValue];
                if(user1)user1(user);
                [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                }else [[QAlertView sharedInstance]showAlertText:obj fadeTime:2];
            }];
        }else{
            [[MeNetworkManage sharedInstance] followUserWithParameter:parameter complete:^(BOOL succeed, id obj) {
                if(succeed){
                user.followed = [obj[@"followed"] boolValue];
                user.bi_followed = [obj[@"bi_followed"] boolValue];
                if(user1)user1(user);
                [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
                }else [[QAlertView sharedInstance]showAlertText:obj fadeTime:2];
            }];
        }
        
    }];
    [alterVc addCancleActionHandler:^{
        
    }];
    [controller presentViewController:alterVc animated:YES completion:nil];
    
    

//    UserParameter *parameter = [[UserParameter alloc] init];
//    parameter._t = [UserConfig sharedInstance].user_token;
//    parameter.uid = [NSString stringWithFormat:@"%li",(long)user.uid];
//    
//    if(user.followed){
//        [[MeNetworkManage sharedInstance] unFollowUserWithPararmeter:parameter complete:^(BOOL succeed, id obj) {
//            user.bi_followed = NO;
//            user.followed = NO;
//            if(user1)user1(user);
//            [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
//        }];
//    }else{
//        [[MeNetworkManage sharedInstance] followUserWithParameter:parameter complete:^(BOOL succeed, id obj) {
//            user.followed = YES;
//            if(user1)user1(user);
//            [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedReloadUserInformationNotification object:nil];
//        }];
//    }
    
    
}
- (UIViewController *)findViewController:(UIView *)sourceView
{
    id target=sourceView;
    while (target) {
        target = ((UIResponder *)target).nextResponder;
        if ([target isKindOfClass:[UIViewController class]]) {
            break;
        }
    }
    return target;
}
@end
