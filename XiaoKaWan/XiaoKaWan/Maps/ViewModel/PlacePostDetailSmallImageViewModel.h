//
//  PlacePostDetailSmallImageViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PlacePostDetailBigImageViewModel.h"
@protocol PlacePostDetailSmallImageViewModelDelegate <NSObject>

- (void)selectItemtAtIndex:(NSInteger)index;

@end
@interface PlacePostDetailSmallImageViewModel :  NSObject<UICollectionViewDelegate,UICollectionViewDataSource,PlacePostDetailBigImageViewModelDelegate>
@property(nonatomic) NSInteger selectIndex;
@property(nonatomic,strong)NSArray *imageList;
@property(nonatomic,assign)id<PlacePostDetailSmallImageViewModelDelegate>delegate;
- (instancetype)initWithModel:(id)model collectionView:(UICollectionView*)colletionView;
@end

