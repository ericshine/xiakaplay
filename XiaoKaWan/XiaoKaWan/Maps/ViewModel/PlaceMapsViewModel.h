//
//  PlaceMapsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
@interface PlaceMapsViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller;

@end
