//
//  PlacePostViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostViewModel.h"
#import "BJMapsListCollectionLayout.h"
#import "PlacePostHeadCollectionCell.h"
#import "MapsDetailDynamicCollectionViewCell.h"
#import "BJRefreshGitFooter.h"
#import "BJRefreshGitHeader.h"
#import "PlacePostDetailViewController.h"
#import "MapsNetworkManage.h"
#import "MapsPostNetoworkParameter.h"
#import "MapObject.h"
#import "PostListObj.h"
#import "PostFrameObject.h"
static NSString* const headCellIdentify = @"PlacePostHeadCollectionCell";
static NSString* const postCellIdentify = @"MapsDetailDynamicCollectionViewCell";
@interface PlacePostViewModel()<BJMapsListCollectionDelegate>
@property(nonatomic,assign)BJSuperViewController *controller;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) BJMapsListCollectionLayout *layout;
@property(nonatomic,strong) MapsPostNetoworkParameter *parameter;
@property(nonatomic,strong) MapObject *mapsObject;
@property(nonatomic,strong) PostListObj *postListObject;
@property(nonatomic,strong) PostFrameObject *postFrameObj;
@property(nonatomic,strong) NSMutableArray *postList;
@end
@implementation PlacePostViewModel
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView{
    self= [super init];
    if(self){
        
        _mapsObject = model;
        _parameter = [[MapsPostNetoworkParameter alloc] init];
        _parameter.maps_id = _mapsObject.id;
        self.controller = controller;
        self.collectionView = collectionView;
        [self.collectionView setCollectionViewLayout:self.layout];
        [self.collectionView registerClass:[PlacePostHeadCollectionCell class] forCellWithReuseIdentifier:headCellIdentify];
        [self.collectionView registerClass:[MapsDetailDynamicCollectionViewCell class] forCellWithReuseIdentifier:postCellIdentify];
        
        [self refreshData];
    }
    return self;
}
- (void)refreshData{
  
    BJRefreshGitHeader *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self getPostListHeader:YES];
        
    }];
    self.collectionView.header = header;
    [self.collectionView.header beginRefreshing];
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self getPostListHeader:NO];
    }];
    self.collectionView.footer = footer;
}
- (void)getPostListHeader:(BOOL)header{
    if(!header){
        PostObjet *psotOj = [_postList lastObject];
        _parameter.last_id = [NSString stringWithFormat:@"%ld",(long)psotOj.id];
    }
    [[MapsNetworkManage sharedInstance] getMapsPostWithParameter:_parameter complete:^(BOOL succeed, id obj) {
        if(succeed){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             _postListObject = [PostListObj objectWithKeyValues:obj];
            if(header){
                _postList = [NSMutableArray arrayWithArray:_postListObject.postList];
            }else{
                [_postList insertObjects:_postListObject.postList atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_postList.count, _postListObject.postList.count)]];
            }
            _postFrameObj = [[PostFrameObject alloc] initWithPostList:_postList];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
            });
        });
        }
         if(header) [self.collectionView.header endRefreshing];
          else  [self.collectionView.footer endRefreshing];
    
    }];
}
- (BJMapsListCollectionLayout *)layout{
    if(_layout == nil){
        _layout = [[BJMapsListCollectionLayout alloc] init];
        _layout.delegate = self;
    }
    return _layout;
}
#pragma mark - BJMapsListCollectionDelegate
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) return 130;
    NSInteger height = [_postFrameObj.postItemHeights[indexPath.row] floatValue];
    return height;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout widthForItemInSection:(NSInteger)section
{
    if(section == 1) return (kDeviceWidth-15)/2;
    else return [PostFrameObject widthPostCell];
}
//- (CGFloat)collectionView:(UICollectionView *)collectionView
//                   layout:(BJMapsListCollectionLayout *)collectionViewLayout
//heightForHeaderAtIndexPath:(NSIndexPath *)indexPath {
//    return 0.0f;
//}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 0)return 1;
    return _postList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        PlacePostHeadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:headCellIdentify forIndexPath:indexPath];
        cell.typeString = @"动态";
        cell.postListObject = _postListObject;
        return cell;
    }
    MapsDetailDynamicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:postCellIdentify forIndexPath:indexPath];
    cell.postObject = _postList[indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PostObjet *postObject = _postList[indexPath.row];
    PostParatemer *parameter = [PostParatemer parameter];
    parameter.post_id = [NSString stringWithFormat:@"%li",(long)postObject.id];
    PlacePostDetailViewController *postDetailVc = [[PlacePostDetailViewController alloc] init];
    postDetailVc.parameter = parameter;
    [self.controller push:postDetailVc];
}
@end
