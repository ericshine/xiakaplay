//
//  PlaceMapsMultipleImageViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJMapsCollectionLayout.h"
#import "MapsHomeCollectionViewCell.h"
#import "MapsDetail.h"
@protocol PlaceMapsMultipleImageViewModelDelegate <NSObject>

- (void)touchCollectionWithModel:(MapsDetail*)mapsObject;

@end
@interface PlaceMapsMultipleImageViewModel : NSObject<MapsHomeCollectionViewCellDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)BJMapsCollectionLayout *collectionLayout;
@property(nonatomic,strong) NSArray *imageArray;
@property(nonatomic,strong) MapsDetail *mapsObject;
@property(nonatomic,assign) id<PlaceMapsMultipleImageViewModelDelegate>delegate;
- (instancetype)initWithModel:(id)model andCollcetionView:(UICollectionView*)collectionView;
@end

