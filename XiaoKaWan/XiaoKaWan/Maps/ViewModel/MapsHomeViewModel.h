//
//  MapsHomeViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MapsHomeCell.h"
@protocol MapsHomeViewModelDelegate <NSObject>

- (void)touchOneMapsWithModel:(id)model;

@end
@interface MapsHomeViewModel : NSObject<UITableViewDelegate,UITableViewDataSource,MapsHomeCellDelegate>
@property(nonatomic,assign)id<MapsHomeViewModelDelegate>delegate;
- (instancetype)initWithModel:(id)model andTabel:(UITableView *)table;
//- (void)getMapsList;
@end
