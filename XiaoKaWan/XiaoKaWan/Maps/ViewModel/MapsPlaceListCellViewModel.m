//
//  MapsPlaceListCellViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceListCellViewModel.h"
#import "MapsPalceImageLayout.h"
#import "MapsPlaceImageCollectionCell.h"
#import "PlaceFrameObject.h"
static NSString* const imageCellIdentiferId = @"imageCellIdentiferId";
@interface MapsPlaceListCellViewModel()<MapsPalceImageLayoutDelegate>
@property(nonatomic,strong)MapsPalceImageLayout *layout;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)PlaceFrameObject *frameObject;
@end

@implementation MapsPlaceListCellViewModel
- (instancetype)initWithView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        self.collectionView = collectionView;
        [collectionView setCollectionViewLayout:self.layout];
        [self.collectionView registerClass:[MapsPlaceImageCollectionCell class] forCellWithReuseIdentifier:imageCellIdentiferId];
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    [self.collectionView reloadData];
}
- (MapsPalceImageLayout *)layout{
    if(_layout == nil){
        _layout = [[MapsPalceImageLayout alloc] init];
        _layout.delegate = self;
    }
    return _layout;
}
#pragma mark - MapsPalceImageLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPalceImageLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [_place.placeImageFrames[indexPath.row] integerValue];
}
- (CGFloat)collectionViewHeight:(UICollectionView *)collectionView{
    return [PlaceFrameObject heightimageCollectionView];
}
#pragma mark - MapsPlaceListHeadCollectionCellDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _place.latestImages.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BJImageObject *imageObj = _place.latestImages[indexPath.row];
    MapsPlaceImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:imageCellIdentiferId forIndexPath:indexPath];
    [cell.imageView setImageWithUrlString:[ImageSizeUrl imageFH300_url:imageObj.url]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if([self.delegate respondsToSelector:@selector(selelctItem:)]){
        [self.delegate selelctItem:self.place];
    }
}
@end
