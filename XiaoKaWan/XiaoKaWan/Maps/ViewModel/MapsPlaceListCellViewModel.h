//
//  MapsPlaceListCellViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJImageObject.h"
#import "Place.h"
@protocol MapsPlaceListCellViewModelDelegate <NSObject>

- (void)selelctItem:(Place*)place;

@end
@interface MapsPlaceListCellViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,assign) id<MapsPlaceListCellViewModelDelegate>delegate;
@property(nonatomic,strong)Place *place;
- (instancetype)initWithView:(UICollectionView *)collectionView;

@end

