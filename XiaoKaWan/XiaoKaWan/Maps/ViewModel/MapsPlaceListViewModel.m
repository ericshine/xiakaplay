//
//  MapsPlaceListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceListViewModel.h"
#import "MapsPlaceLayout.h"
#import "MapsPlaceSimpleCollectionViewCell.h"
#import "MapsPlaceListHeadCollectionCell.h"
#import "MapsPlaceNormalCollectionCell.h"
static NSString* const placeHeadViewCellIdentier = @"placeHeadViewCellIdentier";
static NSString* const placeSimpleCellIdentifier = @"placeSimpleCell";
static NSString* const placeNormalCellIdentifier = @"placeNormalCellIdentifier";
#define kPlaceViewHeight 150
#define kPlaceHeadView 190
@interface MapsPlaceListViewModel()<MapsPlaceLayoutDelegate,MapsPlaceListHeadCollectionCellDelegate,MapsPlaceNormalCollectionCellDelegate>
@property(nonatomic,strong)MapsPlaceLayout *palceLayout;
@property(nonatomic) PlaceType type;
@property(nonatomic,strong)UICollectionView *collectView;
@end
@implementation MapsPlaceListViewModel
- (instancetype)initWithView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        [collectionView setCollectionViewLayout:self.palceLayout];
        [collectionView registerClass:[MapsPlaceListHeadCollectionCell class] forCellWithReuseIdentifier:placeHeadViewCellIdentier];
        [collectionView registerClass:[MapsPlaceSimpleCollectionViewCell class] forCellWithReuseIdentifier:placeSimpleCellIdentifier];
        [collectionView registerClass:[MapsPlaceNormalCollectionCell class] forCellWithReuseIdentifier:placeNormalCellIdentifier];
        _collectView = collectionView;
    }
    return self;
}
- (MapsPlaceLayout *)palceLayout{
    if(_palceLayout == nil){
    _palceLayout = [[MapsPlaceLayout alloc] init];
        _palceLayout.delegate = self;
    }
    return _palceLayout;
}
#pragma mark -MapsPlaceNormalCollectionCellDelegate
- (void)selectImageView{
    if([self.delegate respondsToSelector:@selector(didSelectItem)]){
        [self.delegate didSelectItem];
    }
}
#pragma mark - MapsPlaceListHeadCollectionCellDelegate
- (void)changeModel:(PlaceType)placeType{
    self.type = placeType;
    [self.collectView reloadData];
}
#pragma mark - MapsPlaceLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPlaceLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return kPlaceHeadView;
    if(self.type == PLACETYPE_SIMPLE) return kPlaceViewHeight;
    else return 300;
}
#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
    MapsPlaceListHeadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:placeHeadViewCellIdentier forIndexPath:indexPath];
        cell.delegate = self;
    return cell;
    }
    if(self.type == PLACETYPE_SIMPLE){
    MapsPlaceSimpleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:placeSimpleCellIdentifier forIndexPath:indexPath];
    return cell;
    }else{
        MapsPlaceNormalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:placeNormalCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if([self.delegate respondsToSelector:@selector(didSelectItem)]){
        [self.delegate didSelectItem];
    }
}
@end
