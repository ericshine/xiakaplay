//
//  MapsDetailListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJMapsListCollectionLayout.h"
#import "MapsDetailPlaceHeadCollectionViewCell.h"
//#import "TitleCollectionViewCell.h"
@protocol MapsDetailListViewModelDelegate <NSObject>

//- (void)gotoWantUserListWithData:(Place *)place;
//- (void)gotoBeenUserListWithData:(Place *)place;
- (void)isFollow:(MapsDetail *)mapDetail;
- (void)toPostDetail:(PostObjet*)postObject;
- (void)toPlaceDetail:(Place *)place;
- (void)toUserList:(Place *)place;
@end
@interface MapsDetailListViewModel : NSObject<BJMapsListCollectionDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MapsDetailPlaceHeadCollectionViewCellDelegate>
@property(nonatomic,assign) id<MapsDetailListViewModelDelegate>delegate;
- (instancetype)initWithModel:(id)model andCollectionView:(UICollectionView*)collectionView;
- (void)reloadData;
@end


