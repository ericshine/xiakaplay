//
//  MapsPlaceListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol MapsPlaceListViewModelDelegate <NSObject>

- (void)didSelectItem;

@end
@interface MapsPlaceListViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) id<MapsPlaceListViewModelDelegate>delegate;
- (instancetype)initWithView:(UICollectionView *)collectionView;
@end
