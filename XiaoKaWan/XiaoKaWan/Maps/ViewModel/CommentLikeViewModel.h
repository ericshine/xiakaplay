//
//  CommentLikeViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommentAndLikeView.h"
#import "PlacePostDetailViewModel.h"
#import "PostParatemer.h"
#import "CommetObject.h"
#define commentViewHeight 44
typedef NS_ENUM(NSInteger,ReplyType){
    REPLYTYPE_POST,// 回复动态
    REPLYTYPE_USER  //回复用户： @某某
};

@protocol CommentLikeViewModelDelegate <NSObject>
- (void)commentSuccess:(CommetObject*)commentObj;
- (void)likePostCuccess:(NSString *)likeState;
@end
@interface CommentLikeViewModel : NSObject<PlacePostDetailViewModelDelegate,CommentAndLikeViewDelegate>
@property(nonatomic,copy) void(^keyboardShow)(CGFloat height);
@property(nonatomic,strong)PostParatemer *paratemer;
@property(nonatomic) ReplyType relpyType;
@property(nonatomic,assign) id<CommentLikeViewModelDelegate>delegate;
- (instancetype)initWithView:(CommentAndLikeView*)view andSuperView:(UIView *)superView;
- (void)keyBoardViewShow:(NSNotification *)aNotification;
- (void)keyBoardHidden:(NSNotification *)aNotification;
- (void)textChanged:(NSNotification *)aNotification;

@end
