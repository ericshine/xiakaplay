//
//  PostTagsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostTagsViewModel.h"
#import "MapsPalceImageLayout.h"
#import "BJTitleCollectionViewCell.h"
static NSString *const cellId = @"BJTitleCollectionViewCell" ;
@interface PostTagsViewModel ()<MapsPalceImageLayoutDelegate>
@property(nonatomic,strong)MapsPalceImageLayout *layout;
@property(nonatomic,strong)UICollectionView *collectionView;
@end
@implementation PostTagsViewModel
- (instancetype)initWithTags:(NSArray *)tags collectView:(UICollectionView *)collectionView1{
    self = [super init];
    if(self){
        _tagList = tags;
        self.collectionView = collectionView1;
        [self.collectionView setCollectionViewLayout:self.layout];
        [_collectionView registerClass:[BJTitleCollectionViewCell class] forCellWithReuseIdentifier:cellId];
    }
    return self;
}
- (void)setTagList:(NSArray *)tagList{
    _tagList = tagList;
    [self.collectionView reloadData];
}
- (MapsPalceImageLayout *)layout{
    if(_layout == nil){
    _layout = [[MapsPalceImageLayout alloc] init];
    _layout.delegate = self;
    }
    return _layout;
}
#pragma mark - MapsPalceImageLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPalceImageLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *text = _tagList[indexPath.row];
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:13] andSize:CGSizeMake(kDeviceWidth, 20)];
    return size.width+40;
}
- (CGFloat)collectionViewHeight:(UICollectionView *)collectionView{
    return 30;
}
#pragma mark - MapsPlaceListHeadCollectionCellDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _tagList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BJTitleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    NSString *text = _tagList[indexPath.row];
    cell.titleLb.text = text;
    cell.titleLb.layer.cornerRadius = 7;
    cell.titleLb.layer.borderColor = [UIColor p_colorWithHex:0xffda44].CGColor;
    cell.titleLb.layer.borderWidth = 1;
    return cell;
}
@end
