//
//  PlacePostDetailViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailViewModel.h"
#import "PlacePostDetailHeadCell.h"
#import "PlacePostDetailHeadFootView.h"
#import "PlacePostDetailTitleCell.h"
#import "PlacePostDetaiCommentCell.h"
#import "PostParatemer.h"
#import "BJRefreshGitHeader.h"
#import "BJRefreshGitFooter.h"
#import "MapsNetworkManage.h"
#import "QAlertView.h"
#import "CurrentUser.h"
#import "UserConfig.h"

@interface PlacePostDetailViewModel()<PlacePostDetailHeadFootViewDelegate,PlacePostDetaiCommentCellDelegate,PlacePostDetailHeadCellDelegate>
@property(nonatomic,assign) BJSuperViewController *controller;
@property(nonatomic)CommentLikeType commentLikeType;
@property(nonatomic,strong)PostParatemer *parameter;
@property(nonatomic,strong)UITableView *table;
@property(nonatomic,strong)NSMutableArray *commentList;
@property(nonatomic,strong)PostDetailObject *postObject;
@property(nonatomic,strong)NSMutableArray *hotComments;
@property(nonatomic,strong)NSMutableArray *lastComment;
@property(nonatomic,strong)PlacePostDetailHeadFootView *headView;
@end
@implementation PlacePostDetailViewModel
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller table:(UITableView *)tableView{
    self = [super init];
    if(self){
        self.hotComments = [[NSMutableArray alloc] init];
        self.lastComment = [[NSMutableArray alloc] init];
        self.table = tableView;
        self.parameter = model;
        self.controller = controller;
        [self initRefreshView];
    }
    return self;
}
- (void)initRefreshView{
   BJRefreshGitHeader *Header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
       [self getPostDetailHeader:YES];
    }];
    self.table.header =Header;
    [self.table.header beginRefreshing];
    
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self getPostDetailHeader:NO];
    }];
    self.table.footer = footer;
}
- (void)getPostDetailHeader:(BOOL)header{
    if(header){
    self.parameter.last_id = @"0";
    }
    else{
        CommetObject *lastObj = [_lastComment lastObject];
        self.parameter.last_id = lastObj.id;
    }
    [[MapsNetworkManage sharedInstance] getMapPostDetailWithParameter:self.parameter complete:^(BOOL succeed, id obj) {
        if(succeed){
        _postObject = [PostDetailObject objectWithKeyValues:obj];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kLoadedPostData" object:_postObject];
            [self.delegate getPostData:_postObject];
        if(header){
            self.hotComments = [NSMutableArray arrayWithArray:_postObject.hotComments];
            self.lastComment = [NSMutableArray arrayWithArray:_postObject.lastComments];
        [self.table.header endRefreshing];
        }else{
            [self.lastComment insertObjects:_postObject.lastComments atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(self.lastComment.count, _postObject.lastComments.count)]];
            [self.table.footer endRefreshing];
        }
        [self.table reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            if(header)[self.table.header endRefreshing];
            else[self.table.footer endRefreshing];
        }
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.postObject == nil)return 0;
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section ==0)return 1;
    NSInteger number = self.hotComments.count +self.lastComment.count;
    if(number<=0)return 0;
    else if(number>0 && self.lastComment.count<=0) return number + 1;
    else return number + 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section ==0) return _postObject.cellHeight;
    else {
        if(indexPath.row == 0 || indexPath.row ==self.hotComments.count+1) return 20;
        CommetObject *commentObject;
        if(indexPath.row<self.hotComments.count+2){
            commentObject = self.hotComments[indexPath.row-1];
        }else{
            commentObject = self.lastComment[indexPath.row - (self.hotComments.count+2)];
        }
        return commentObject.commentHeight;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1)return 44;
    return 0.1;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section ==1){
        if(_headView==nil){
        _headView = [[PlacePostDetailHeadFootView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 44)];
        _headView.backgroundColor = [UIColor whiteColor];
        _headView.delegate = self;
            
        }
        _headView.postDetail = self.postObject;
        return _headView;
    };
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    static NSString *cellIdentify = @"placePostDetailHead";
    PlacePostDetailHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell =[[PlacePostDetailHeadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
        cell.postObject = self.postObject;
    return cell;
    }else{
        if(indexPath.row == 0 || indexPath.row == self.hotComments.count+1){
            static NSString *cellIdentify = @"PlacePostDetailTitleCell";
            PlacePostDetailTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
            if(!cell){
                cell =[[PlacePostDetailTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.title = indexPath.row == 0?@"热门评论":@"最新评论";
            return cell;
        }
        static NSString *cellIdentify = @"commentCell";
        PlacePostDetaiCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if(!cell){
            cell =[[PlacePostDetaiCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.delelgate = self;
        }
        CommetObject *commentObject;
        if(indexPath.row<self.hotComments.count+2){
            commentObject = self.hotComments[indexPath.row-1];
        }else{
            commentObject = self.lastComment[indexPath.row - (self.hotComments.count+2)];
        }
        cell.commentObject = commentObject;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if((indexPath.section == 1) && indexPath.row !=0 && (indexPath.row != self.hotComments.count+1)){
        CommetObject *commentObject;
        if(indexPath.row<self.hotComments.count+2){
            commentObject = self.hotComments[indexPath.row-1];
        }else{
            commentObject = self.lastComment[indexPath.row - (self.hotComments.count+2)];
        }
        [self.delegate selectUserToReply:commentObject.user];
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
 if(self.scrollDidScroll)self.scrollDidScroll();
    [self.delegate selectUserToReply:nil];
}

#pragma mark - PlacePostDetailHeadFootViewDelegate
- (void)selelctSegmentType:(CommentLikeType)type{
    self.commentLikeType = type;
}
#pragma mark - PlacePostDetaiCommentCellDelegate
- (void)likeComment:(CommetObject *)commentObj commentObj:(void (^)(CommetObject *))commentObject{
    PostParatemer *parameter = [PostParatemer parameterComment];
    parameter.comment_id = commentObj.id;
    parameter.is_remove = commentObj.loginUserlLike;
    [[MapsNetworkManage sharedInstance] commentLikeWithParameter:parameter complete:^(BOOL succeed, id obj) {
        if(!succeed){
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }else{
            BOOL isLike = [commentObj.loginUserlLike boolValue];
            NSInteger likes = [commentObj.likes integerValue];
            if(!isLike) likes = likes+1;
            else likes = likes -1;
            commentObj.likes = [NSString stringWithFormat:@"%li",(long)likes];
            commentObj.loginUserlLike = [NSString stringWithFormat:@"%i",!isLike];
            if(commentObject)commentObject(commentObj);
        }
    }];
}
#pragma mark - comment success
- (void)commentSuccess:(CommetObject *)commentObj{
    commentObj.likes = @"0";
    if(self.hotComments.count<10){
        [self.hotComments insertObject:commentObj atIndex:0];
    }else [self.lastComment insertObject:commentObj atIndex:0];
    NSInteger count = [self.postObject.commentCount integerValue] + 1;
    self.postObject.commentCount = [NSString stringWithFormat:@"%li",(long)count];
    _headView.postDetail = self.postObject;
    [self.table reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
}
-(void)likePostSuccess:(NSString *)likeState{
    self.postObject.loginUserLike = likeState;
    BOOL like = [likeState boolValue];
    NSInteger count = [self.postObject.likeCount integerValue];
    if(like) count =count +1;
    else count = count -1;
    self.postObject.likeCount = [NSString stringWithFormat:@"%li",(long)count];
    _headView.postDetail = self.postObject;
}
#pragma mark- PlacePostDetailHeadCellDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] beenAction:place callBack:place1];
}
- (void)wantGoAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] wantGoAction:place callBack:place1];
}
- (void)toUserList:(PostDetailObject *)object{
    if(self.toUserList)self.toUserList(object);
}
- (void)touserDetail:(User *)user{
    if(self.toUserDetail)self.toUserDetail(user);
}
- (void)toPlaceDetail:(Place *)place{
    if(self.toPlaceDetail)self.toPlaceDetail(place);
}
#pragma mark currentUser
- (User *)getCurrentUser{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    User *user  = [[User alloc] init];
    user.nick = currentUser.nick;
    user.avatar = currentUser.avatar;
    user.uid = [currentUser.uid integerValue];
    return user;
}
@end

