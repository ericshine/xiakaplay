//
//  MapsHomeCellViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJMapsCollectionLayout.h"
#import "MapsHomeCollectionViewCell.h"
@interface CollectionObject : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,strong) NSData *photoData;
@end
@protocol MapsHomeCellViewModelDelegate <NSObject>
- (void)touchCollectionWithModel:(id)model;
@end
@interface MapsHomeCellViewModel : NSObject<UICollectionViewDataSource,UICollectionViewDelegate,MapsHomeCollectionViewCellDelegate>
@property(nonatomic,strong)BJMapsCollectionLayout *collectionLayout;
@property(nonatomic,strong) NSIndexPath *indexPath;
@property(nonatomic,assign) id<MapsHomeCellViewModelDelegate>delegate;
@property(nonatomic,strong) CollectionObject *collectionObject;
- (instancetype)initWithModel:(id)model andCollcetionView:(UICollectionView*)collectionView;
@end

