//
//  MapsHomeViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsHomeViewModel.h"
//#import "MapsHomeCell.h"
#import "ClientNetwork.h"
#import "Maps.h"
#import "BJRefreshGitHeader.h"
#import "BJRefreshGitFooter.h"
#import "MapsListNetworkParameter.h"
#import <MJExtension.h>
#import "MapObject.h"
@implementation MapsHomeViewModel{
    NSMutableArray *mapsList;
    UITableView *_tableView;
    MapsListNetworkParameter *networkParameter;
}
- (instancetype)initWithModel:(id)model andTabel:(UITableView *)table
{
    self = [super init];
    if(self){
        networkParameter = [[MapsListNetworkParameter alloc] init];
        _tableView = table;
        mapsList = [NSMutableArray arrayWithCapacity:0];
        [self initRefresh];
        
    }
    return self;
}
- (void)initRefresh{
    BJRefreshGitHeader  *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self getMapsList:YES];
    }];
    _tableView.header  = header;
    [_tableView.header beginRefreshing];
    
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self getMapsList:NO];
    }];
    _tableView.footer = footer;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return mapsList.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 312+73;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Maps *maps = mapsList[indexPath.section];
    MapsHomeCell *cell = [MapsHomeCell initCellWithTabel:tableView];
    cell.delegate = self;
    cell.indexPath = indexPath;
    cell.maps = maps;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Maps *maps = mapsList[indexPath.section];
    MapObject *mapObj = [MapObject mapObject];
    mapObj.id = [NSString stringWithFormat:@"%@",maps.id];
    [self selectItems:mapObj];
}
- (void)selectItems:(MapObject *)mapObj{
    [self.delegate touchOneMapsWithModel:mapObj];
}
#pragma mark - MapsHomeViewModelDelegate
- (void)touchMapsWithModel:(id)model
{
    CollectionObject *collectionObj = model;
     MapObject *mapObj = [MapObject mapObject];
    mapObj.id = collectionObj.id;
    [self selectItems:mapObj];
}
#pragma mark - netWork
-(void)getMapsList:(BOOL)isLatest{
    if(isLatest){
    networkParameter.last_id = 0;
//        networkParameter.get_type = @"";
    }else{
        Maps *lastMap = [mapsList lastObject];
    networkParameter.last_id = [lastMap.id integerValue];
    }
    [[ClientNetwork sharedInstance] getWithPath:[APIConfig sharedInstance].mapsList parameter:networkParameter.keyValues complete:^(BOOL succeed, id obj) {
        if(isLatest)[self getLatestData:obj success:succeed];
        else [self getOldData:obj success:succeed];
        [_tableView reloadData];
    }];
}
- (void)getLatestData:(NSArray *)obj success:(BOOL)success{
    NSArray *mapsArray = (NSArray *)obj;
    if(success){
        if(mapsArray.count){
            [Maps clearCache];
            mapsList = [Maps saveMapsData:obj isMyMaps:NO];
        }else{
            mapsList = [[NSMutableArray alloc] initWithArray:[Maps getMapsList]];
        }
    }else{
     mapsList = [[NSMutableArray alloc] initWithArray:[Maps getMapsList]];
    }
    [_tableView.header endRefreshing];
}
- (void)getOldData:(NSArray*)obj success:(BOOL)success{
    if(success){
        if(obj.count){
            NSArray * dataArray = [NSArray arrayWithArray:[Maps saveMapsData:obj isMyMaps:NO]];
            [mapsList insertObjects:dataArray atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(mapsList.count, obj.count)]];
        }
    }else{
    
    }
    
    [_tableView.footer endRefreshing];
}
@end
