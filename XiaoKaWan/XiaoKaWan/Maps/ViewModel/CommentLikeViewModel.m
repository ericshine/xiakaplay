//
//  CommentLikeViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommentLikeViewModel.h"
#import <Masonry.h>
#import "NSString+Cagetory.h"
#import "MapsNetworkManage.h"
#import "CurrentUser.h"
#import <MJExtension.h>
#import "QAlertView.h"
@interface CommentLikeViewModel()
@property(nonatomic,strong)CommentAndLikeView *commentView;
@property(nonatomic,strong)NSLayoutConstraint *constraintY;
@property(nonatomic,strong)NSLayoutConstraint *constraintH;
@property(nonatomic,strong)UIView *superView;
@property(nonatomic,strong)PostDetailObject *postDetail;
@end
@implementation CommentLikeViewModel
- (instancetype)initWithView:(CommentAndLikeView *)view andSuperView:(UIView *)superView{
    self =[super init];
    if(self){
        self.commentView = view;
        self.superView = superView;
        [self layoutView:view withSuperView:superView];
        
    }
    return self;
}
- (void)keyBoardHidden:(NSNotification *)aNotification{
    NSDictionary* info = [aNotification userInfo];
    NSTimeInterval durationTime = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] CGRectValue].origin.x;
    [UIView animateWithDuration:durationTime animations:^{
        self.keyboardShow(44);
        self.constraintY.constant=0;
        [self.superView layoutIfNeeded];
    }];
}
- (void)keyBoardViewShow:(NSNotification *)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
   NSTimeInterval durationTime = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] CGRectValue].origin.x;
    [UIView animateWithDuration:durationTime animations:^{
        self.keyboardShow(kbSize.height+44);
        self.constraintY.constant = -kbSize.height;
        [self.superView layoutIfNeeded];
    }];
   
}
- (void)textChanged:(NSNotification *)aNotification{
    CGSize size = [self.commentView.commentTextView.text sizeWithFont:[UIFont systemFontOfSize:12] andSize:CGSizeMake(self.commentView.commentTextView.frame.size.width, 99999)];
    CGFloat height = size.height+20;
    if(height<=40) height = 40;
    else if (height>80) height = 80;
    self.constraintH.constant = height;
    [self.superView layoutIfNeeded];
}
- (void)layoutView:(CommentAndLikeView*)view withSuperView:(UIView*)superView{
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.left.equalTo(superView);
        make.right.equalTo(superView);
//        make.height.equalTo(@44);
    }];
    _constraintY = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [superView addConstraint:_constraintY];
    _constraintH = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:commentViewHeight];
    [view addConstraint:_constraintH];
}
#pragma mark - PlacePostDetailViewModelDelegate
- (void)selectUserToReply:(User *)user{
    
    if(user == nil){
        self.relpyType = REPLYTYPE_POST;
     self.commentView.commentTextView.placeHolder = @"写评论:";
        self.commentView.commentTextView.text = @"";
    }else{
        self.relpyType = REPLYTYPE_USER;
        [self.commentView.commentTextView becomeFirstResponder];
        self.commentView.commentTextView.placeHolder = @"";
       self.commentView.commentTextView.text = [NSString stringWithFormat:@"@%@ ",user.nick];
//        self.commentView.commentTextView.attributedText = 
    }
}
- (void)getPostData:(PostDetailObject *)postData{
    _postDetail = postData;
    self.commentView.likeState = [postData.loginUserLike boolValue];
    
}
#pragma mark - CommentAndLikeViewDelegate
- (void)commentWithContent:(NSString *)content{
    self.paratemer.content = content;
    if(self.relpyType == REPLYTYPE_POST){
    
    }else{
    
    }
    [[MapsNetworkManage sharedInstance] commentPostWithParameter:self.paratemer complete:^(BOOL succeed, id obj) {
        if(succeed){
            CurrentUser *currentUser = [CurrentUser getCurrentUser];
            CommetObject *commentObj = [[CommetObject alloc] init];
            commentObj = [CommetObject objectWithKeyValues:obj];
            User *user = [[User alloc] init];
            user.nick = currentUser.nick;
            user.avatar = currentUser.avatar;
            commentObj.user = user;
            [self.delegate commentSuccess:commentObj];
        self.relpyType = REPLYTYPE_POST;
        self.commentView.commentTextView.text = @"";
        }
        else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
}
- (void)likeAction:(void (^)(BOOL))success{
    PostParatemer *paratemer = [PostParatemer parameterComment];
    paratemer.post_id = self.paratemer.post_id;
    paratemer.is_remove = _postDetail.loginUserLike;
    [[MapsNetworkManage sharedInstance] postLikeWithParameter:paratemer complete:^(BOOL succeed, id obj) {
        if(success)success(succeed);
        if(succeed){
            BOOL isLike = [_postDetail.loginUserLike boolValue];
            [self.delegate likePostCuccess:[NSString stringWithFormat:@"%i",!isLike]];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
}
@end
