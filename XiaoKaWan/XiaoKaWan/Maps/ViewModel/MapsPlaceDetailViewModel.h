//
//  MapsPlaceDetailViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
@class Place;
@protocol MapsPlaceDetailViewModelDelegate <NSObject>

- (void)toUserList:(Place *)place;
- (void)thirdpartyNavigation;
@end
@interface MapsPlaceDetailViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) id<MapsPlaceDetailViewModelDelegate>delegate;
@property(nonatomic,strong) Place *place;
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView;
- (void)getPlaceData:(BOOL)header;
@end

