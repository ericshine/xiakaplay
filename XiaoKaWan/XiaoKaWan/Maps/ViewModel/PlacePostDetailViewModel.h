//
//  PlacePostDetailViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
#import "User.h"
#import "CommetObject.h"
#import "PostDetailObject.h"

@protocol PlacePostDetailViewModelDelegate <NSObject>

- (void)selectUserToReply:(User*)user;
- (void)getPostData:(PostDetailObject*)postData;
@end
@interface PlacePostDetailViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)id<PlacePostDetailViewModelDelegate>delegate;
@property(nonatomic,copy) void(^scrollDidScroll)();
@property(nonatomic,copy) void(^toUserList)(PostDetailObject *object);
@property(nonatomic,copy) void(^toUserDetail)(User *user);
@property(nonatomic,copy) void(^toPlaceDetail)(Place *place);
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController*)controller table:(UITableView *)tableView;
- (void)commentSuccess:(CommetObject*)commentObj;
- (void)likePostSuccess:(NSString*)likeState;
@end
