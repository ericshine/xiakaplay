//
//  MapsUserListViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapObject.h"
#import <UIKit/UIKit.h>
#import "User.h"
typedef NS_ENUM(NSInteger,UserListType){
    USERLISTTYPE_WANT,
    USERLISTTYPE_BEEN
};
@protocol MapsUserListViewModelDelegate <NSObject>

- (void)toUserInfor:(User *)user;

@end
@interface MapsUserListViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *table;
@property(nonatomic)UserListType listType;
@property(nonatomic,assign) id<MapsUserListViewModelDelegate>delegate;
- (instancetype)initWithModel:(MapObject*)model listType:(UserListType)type table:(UITableView*)tableview;
- (void)getDataFooter:(BOOL)footer;
- (void)reloadData;
@end

