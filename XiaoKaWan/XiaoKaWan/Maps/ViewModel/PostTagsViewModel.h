//
//  PostTagsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeadFileConfig.h"
@interface PostTagsViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
- (instancetype)initWithTags:(NSArray *)tags collectView:(UICollectionView *)collectionView;
@property(nonatomic,strong)NSArray *tagList;
@end
