//
//  AddPlaceToMapsViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPlaceToMapsViewModel.h"
#import "MapsSelectCell.h"
#import "UIImage+Category.h"
#import <Masonry.h>
#import "MapsPostNetoworkParameter.h"
#import "UserConfig.h"
#import "MapsNetworkManage.h"
@interface AddPlaceToMapsViewModel ()
@property(nonatomic,strong)UITableView *table;
@property(nonatomic,strong)NSArray *mapsListArray;
@property(nonatomic,strong)Maps *selelctMap;
@property(nonatomic,strong)UILabel *selectLabTag;
@property(nonatomic,strong)MapsPostNetoworkParameter *mapsParameter;
@end
@implementation AddPlaceToMapsViewModel
- (instancetype)initWithTable:(UITableView *)table{
    self = [super init];
    if(self){
        _mapsParameter = [[MapsPostNetoworkParameter alloc] init];
        _table = table;
        NSArray *myMaps = [Maps getMyMapsList];
        if(!myMaps.count){
            [self getMaps];
        }else{
        [self getMapList:myMaps];
        }
    }
    return self;
}
- (void)getMaps{
    self.mapsParameter.uid = [UserConfig sharedInstance].userId;
    self.mapsParameter.last_id = @"0";
    [[MapsNetworkManage sharedInstance] getMyMapsListWithParameter:self.mapsParameter complete:^(BOOL succeed, id obj) {
        if(succeed){
            NSMutableArray *array;
           [Maps clearMyMapsCache];
            array = [Maps saveMapsData:obj isMyMaps:YES];
          [self getMapList:array];
            [self.table reloadData];
        }
    }];
    
}
- (void)getMapList:(NSArray *)array{
    NSMutableArray *mapsArray = [[NSMutableArray alloc] initWithCapacity:0];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Maps *maps = obj;
        NSInteger stast = [maps.status integerValue];
        NSInteger userId = maps.createUseridValue;
        NSLog(@"%li    %li",(long)userId,(long)[[UserConfig sharedInstance].userId integerValue]);
        if((stast&2)>0 && (userId == [[UserConfig sharedInstance].userId integerValue])){
            [mapsArray addObject:maps];
        }
    }];
    _mapsListArray = [mapsArray copy];
}
//- (UILabel *)selectLabTag{
//    if(_selectLabTag == nil){
//        _selectLabTag = [[UILabel alloc] init];
//        _selectLabTag.text = @"\U0000e60b";
//        _selectLabTag.font = [UIFont fontWithName:@"iconfont" size:20];
//    }
//    return _selectLabTag;
//}
- (void)getMapsList{
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mapsListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Maps *map = _mapsListArray[indexPath.row];
    static NSString *cellIdentifyer = @"cellIdentify";
    MapsSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyer];
    if(!cell){
        cell = [[MapsSelectCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifyer];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.titleLabel.text = map.name;
    cell.headImageView.image = [UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:0.8]];
    if(map == _selelctMap){
    cell.selectLb.text = @"\U0000e60e";
    }else{
    cell.selectLb.text = @"";
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selelctMap = _mapsListArray[indexPath.row];
    [self.table reloadData];
    [self.delegate selectMaps:_selelctMap];
}
@end
