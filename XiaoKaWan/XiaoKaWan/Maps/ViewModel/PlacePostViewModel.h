//
//  PlacePostViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJSuperViewController.h"
@interface PlacePostViewModel : NSObject<UICollectionViewDelegate,UICollectionViewDataSource>
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView;
@end
