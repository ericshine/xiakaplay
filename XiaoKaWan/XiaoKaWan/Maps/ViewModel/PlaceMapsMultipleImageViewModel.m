//
//  PlaceMapsMultipleImageViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceMapsMultipleImageViewModel.h"
#import "UIImage+Category.h"
#import "ImageSizeUrl.h"
#import <UIImageView+WebCache.h>
#import "BJImageObject.h"
#import <MJExtension.h>
#define itmesNumber 6
static NSString* const collectionIdentify = @"mapsHomeCollcetion";
@implementation PlaceMapsMultipleImageViewModel{
    UICollectionView *_collectionView;
}
- (instancetype)initWithModel:(id)model andCollcetionView:(UICollectionView *)collectionView
{
    self = [super init];
    if(self){
        collectionView.collectionViewLayout = self.collectionLayout;
        [collectionView registerClass:[MapsHomeCollectionViewCell class] forCellWithReuseIdentifier:collectionIdentify];
        _collectionView = collectionView;
    }
    return self;
}
- (void)setImageArray:(NSArray *)imageArray{
    _imageArray = imageArray;
    [_collectionView reloadData];
}
- (BJMapsCollectionLayout*)collectionLayout
{
    if(_collectionLayout==nil){
        _collectionLayout = [[BJMapsCollectionLayout alloc] init];
        
        [_collectionLayout calculateItemSizeWithWidthBlock:^CGSize(NSIndexPath *indexPath) {
            CGFloat width = _collectionLayout.collectionView.frame.size.width;
            CGFloat height = _collectionLayout.collectionView.frame.size.height;
            NSInteger row = (_imageArray.count+1)%3;
            if (indexPath.row == row) {
                return CGSizeMake(width/3, (height*2)/3);
            }
            else if(indexPath.row ==5){
                return CGSizeMake(width/3, (height)/3);
            }
            else{
                return CGSizeMake(width/3, height/2);
            }
        }];
    }
    return _collectionLayout;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return itmesNumber;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    //else return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BJImageObject *imageObj;
    if(indexPath.row>=_imageArray.count){
        imageObj = [[BJImageObject alloc] init];
    }else{
        imageObj = _imageArray[indexPath.row];
    }
    MapsHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionIdentify forIndexPath:indexPath];
    cell.delegate = self;
    cell.backgroundColor = [UIColor p_colorWithHex:0xefefef];
    cell.imageObj = imageObj;
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.2 animations:^{
        cell.transform = CGAffineTransformMakeScale(0.95, 0.95);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            cell.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            [self.delegate touchCollectionWithModel:self.mapsObject];;
        }];
    }];
    //     [self.delegate touchCollectionWithModel:nil];
}
- (void)touchItemWithModel:(id)model
{
    //[self.delegate touchCollectionWithModel:self.collectionObject];
}
@end
