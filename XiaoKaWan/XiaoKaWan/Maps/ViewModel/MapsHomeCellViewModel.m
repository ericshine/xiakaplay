//
//  MapsHomeCellViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsHomeCellViewModel.h"
#import "UIImage+Category.h"
#import "ImageSizeUrl.h"
#import <UIImageView+WebCache.h>
#import "BJImageObject.h"
#import <MJExtension.h>

#define itmesNumber 6
static NSString* const collectionIdentify = @"mapsHomeCollcetion";
@implementation CollectionObject

@end
@implementation MapsHomeCellViewModel{
    UICollectionView *_collectionView;
    NSMutableArray *imageArray;
}
- (instancetype)initWithModel:(id)model andCollcetionView:(UICollectionView *)collectionView
{
    self = [super init];
    if(self){
        imageArray = [NSMutableArray arrayWithCapacity:0];
        collectionView.collectionViewLayout = self.collectionLayout;
        [collectionView registerClass:[MapsHomeCollectionViewCell class] forCellWithReuseIdentifier:collectionIdentify];
        _collectionView = collectionView;
    }
    return self;
}
//- (void)setIndexPath:(NSIndexPath *)indexPath
//{
//    _indexPath = indexPath;
////    [self.collectionLayout prepareLayout];
//}
- (void)setCollectionObject:(CollectionObject *)collectionObject{
    _collectionObject = collectionObject;
    @autoreleasepool {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *photoJson;
            NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
            if(collectionObject.photoData != nil){
            photoJson = [NSJSONSerialization JSONObjectWithData:collectionObject.photoData options:NSJSONReadingMutableContainers error:nil];
            }
            for (int i=0; i<itmesNumber; i++) {
                BJImageObject *imageObj = [[BJImageObject alloc] init];
                if(i<photoJson.count){
                imageObj = [BJImageObject objectWithKeyValues:photoJson[i]];
                }
                [array addObject:imageObj];
            }
            imageArray = [array copy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
            });
        });
    }
}
- (BJMapsCollectionLayout*)collectionLayout
{
    if(_collectionLayout==nil){
        _collectionLayout = [[BJMapsCollectionLayout alloc] init];
        
        [_collectionLayout calculateItemSizeWithWidthBlock:^CGSize(NSIndexPath *indexPath) {
             CGFloat width = _collectionLayout.collectionView.frame.size.width;
            CGFloat height = _collectionLayout.collectionView.frame.size.height;
            NSInteger row = (imageArray.count+2)%3;
            if (indexPath.row == row) {
                return CGSizeMake(width/3, (height*2)/3);
            }
            else if(indexPath.row ==5){
                return CGSizeMake(width/3, (height)/3);
            }
            else{
                return CGSizeMake(width/3, height/2);
            }
        }];
    }
    return _collectionLayout;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return itmesNumber;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(imageArray.count>0)return 1;
    else return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BJImageObject *imageObj = imageArray[indexPath.row];
    MapsHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionIdentify forIndexPath:indexPath];
    cell.delegate = self;
    cell.backgroundColor = [UIColor p_colorWithHex:0xefefef];
    cell.imageObj = imageObj;
    return cell;

}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.2 animations:^{
        cell.transform = CGAffineTransformMakeScale(0.95, 0.95);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            cell.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            [self.delegate touchCollectionWithModel:self.collectionObject];;
        }];
    }];
//     [self.delegate touchCollectionWithModel:nil];
}
- (void)touchItemWithModel:(id)model
{
    [self.delegate touchCollectionWithModel:self.collectionObject];
}
@end

