//
//  MapsDetailListViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetailListViewModel.h"
#import "BJRefreshGitHeader.h"
#import "BJRefreshGitFooter.h"
//#import "MapsPlaceSimpleCollectionViewCell.h"
#import "MapsDetailDynamicCollectionViewCell.h"
#import "ClientNetwork.h"
#import "MapsDetail.h"
#import <MJExtension.h>
#import "MapObject.h"
#import "MapDetailNetworkParameter.h"
#import "MapsNetworkManage.h"
#import "QAlertView.h"
#import "MapsDetailFrame.h"
#import "PostFrameObject.h"
#import "BJNotificationConfig.h"
#import "MapsPlaceNormalCollectionCell.h"
#import "PlaceFrameObject.h"
#import "CurrentUser.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define kHeadViewHeight 230
#define kTitleViewHeight 40
#define kPlaceViewHeight 150
static NSString * const mapsDetailHeadCollectionIdentifier = @"mapsDetailHeadCollectionId";
static NSString * const mapsPlaceNormalCollectionCellIdenfier = @"mapsPlaceNormalCollectionCellIdenfier";
static NSString * const mapsDetailListDynamicCollectionIdentifier = @"mapsDetailListDynamicCollectionIdentifier";

typedef enum {
    MAPS= 0,
    MAPS_PLAE, // 基于地理位置展示
    MAPS_POST // 基于动态展示
}ShowType;
@interface MapsDetailListViewModel ()<MapsPlaceNormalCollectionCellDelegate>
@property(nonatomic)ShowType showType;
@property(nonatomic,strong)UICollectionView *collectView;
@property(nonatomic,strong)MapsDetail *mapsDetail;
@property(nonatomic,strong)MapObject *mapObject;
@property(nonatomic,strong)MapDetailNetworkParameter *placeParameter;
@property(nonatomic,strong)MapDetailNetworkParameter *postParameter;
@property(nonatomic,strong)MapsDetailFrame *frameObject;
@property(nonatomic,strong)PostFrameObject *postFrameObj;
@property(nonatomic,strong)PlaceFrameObject *placeFrameObj;
@property(nonatomic,strong)NSMutableArray *postList;
@property(nonatomic,strong)NSMutableArray *placeList;
@property(nonatomic) BOOL noMorePost;
@property(nonatomic) BOOL noMorePlace;
@property(nonatomic,strong)NSMutableDictionary *showMoreDic;
@end
@implementation MapsDetailListViewModel
- (instancetype)initWithModel:(id)model andCollectionView:(UICollectionView *)collectionView
{
    self = [super init];
    if(self){
        self.mapObject = model;
        _placeParameter = [[MapDetailNetworkParameter alloc] init];
        _postParameter = [[MapDetailNetworkParameter alloc] init];
        _showMoreDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [collectionView registerClass:[MapsDetailPlaceHeadCollectionViewCell class] forCellWithReuseIdentifier:mapsDetailHeadCollectionIdentifier];
        [collectionView registerClass:[MapsPlaceNormalCollectionCell class] forCellWithReuseIdentifier:mapsPlaceNormalCollectionCellIdenfier];
        [collectionView registerClass:[MapsDetailDynamicCollectionViewCell class] forCellWithReuseIdentifier:mapsDetailListDynamicCollectionIdentifier];
        [self initRefresh:collectionView];
        self.collectView = collectionView;
        [self readMapsDetail];
        self.showType = MAPS_PLAE;
    }
    return self;
}
- (void)initRefresh:(UICollectionView *)collectionView{
    BJRefreshGitHeader  *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self getMapsDetailHeaderRefresh:YES];
    }];
    
    collectionView.header  = header;
    [collectionView.header beginRefreshing];
    
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self getMapsDetailHeaderRefresh:NO];
    }];
    collectionView.footer = footer;
}

#pragma mark - BJMapsListCollectionDelegate
- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
    widthForItemInSection:( NSInteger )section{
    if(self.showType == MAPS_PLAE){
    return kDeviceWidth;
    }else{
        if(section ==0){
            return kDeviceWidth;
        }else return [PostFrameObject widthPostCell];
    }
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section ==0)return _frameObject.headCellHeight;
    if(self.showType == MAPS_PLAE){
        Place *place = _placeList[indexPath.row];
        BOOL show = [[_showMoreDic objectForKey:[NSNumber numberWithInteger:indexPath.row]] boolValue];
        return [PlaceFrameObject heightCellShowMore:show content:place.addReason];
    }else{
    CGFloat cellHeitht = [self.postFrameObj.postItemHeights[indexPath.row] floatValue];
        return cellHeitht;
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForHeaderAtIndexPath:(NSIndexPath *)indexPath
{
    return 10; /// 最好直接修改layout里边的高度
}
#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(self.mapsDetail == nil) return 0;
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(section == 0)return 1;
    else{
        if(self.showType == MAPS_PLAE){
            return _placeList.count;
        }else{
         return _postList.count;
        }
    }
   
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        MapsDetailPlaceHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsDetailHeadCollectionIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        cell.mapsObject = self.mapsDetail;
        return cell;
    }
    else{
        if(self.showType == MAPS_PLAE){
            Place *place = _placeList[indexPath.row];
            place.placeImageFrames = _placeFrameObj.placeImageWidths[indexPath.row];
            MapsPlaceNormalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsPlaceNormalCollectionCellIdenfier forIndexPath:indexPath];
            cell.delegate = self;
            cell.place = place;
            cell.indexPath = indexPath;
            if(indexPath.row>3) cell.tagLabel.image = nil;
            else cell.tagLabel.image = [UIImage imageNamed:@"bangdanIcon"];
             BOOL show = [[_showMoreDic objectForKey:[NSNumber numberWithInteger:indexPath.row]] boolValue];
            cell.isShowMore = show;
            return cell;
        }else{
            MapsDetailDynamicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsDetailListDynamicCollectionIdentifier forIndexPath:indexPath];
            cell.postObject = _postList[indexPath.row];
            return cell;
        }
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        if(self.showType == MAPS_PLAE){
            [self.delegate toPlaceDetail:_placeList[indexPath.row]];
        }else{
        [self.delegate toPostDetail:_postList[indexPath.row]];
        }
    }
}
#pragma mark -MapsPlaceNormalCollectionCellDelegate
- (void)toUserList:(Place *)place{
    [self.delegate toUserList:place];
}
- (void)selectImageViewWithPlace:(Place *)place{
    [self.delegate toPlaceDetail:place];
}
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] beenWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJBeenActionNotification object:nil];
            place.isBeen = !place.isBeen;
            NSInteger offNumber;
            if(place.isBeen){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                [users addObject:[self getCurrentUser]];
                place.beenUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
               NSArray *usersBeen = [users filteredArrayUsingPredicate:predicate];
                place.beenUsers = usersBeen;
            }
            place.beenCount = place.beenCount+offNumber;
           
            if(place1)place1(place);
        };
    }];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] wantGoWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJWantActionNotification object:nil];
            place.isWant = !place.isWant;
            NSInteger offNumber;
            if(place.isWant){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                [users addObject:[self getCurrentUser]];
                place.wantUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
                NSArray *userswant = [users filteredArrayUsingPredicate:predicate];
                place.wantUsers = userswant;
            }
            place.wantCount = place.wantCount+offNumber;
            
            if(place1)place1(place);
        };
    }];
}
- (void)showMoreReason:(BOOL)show atIndexPath:(NSIndexPath *)indexPath{
    [_showMoreDic setObject:[NSNumber numberWithBool:show] forKey:[NSNumber numberWithInteger:indexPath.row]];
    [self.collectView reloadData];
}
- (User *)getCurrentUser{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    User *user  = [[User alloc] init];
    user.nick = currentUser.nick;
    user.avatar = currentUser.avatar;
    user.uid = [currentUser.uid integerValue];
    return user;
}
#pragma mark - MapsDetailPlaceHeadCollectionViewCellDelegate
- (void)postTouchAction{
    self.showType = MAPS_POST;
    if(!_noMorePost)[self.collectView.footer resetNoMoreData];
    if(_postList==nil ||_postList.count<=0){
        [self getMapsDetailHeaderRefresh:YES];
    }
    [self.collectView reloadData];
}
- (void)placeTouchAction{
    self.showType = MAPS_PLAE;
    if(!_noMorePlace)[self.collectView.footer resetNoMoreData];
    [self.collectView reloadData];
}

//- (void)beenButtonActionWithPlace:(Place *)place {
//    [[MapsNetworkManage sharedInstance] beenWithParameter:place complete:^(BOOL succeed, id obj) {
//        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
//        else [[NSNotificationCenter defaultCenter] postNotificationName:BJMapsDetailNeedReloadDataNotification object:nil];
//    }];
//}
//- (void)wangButtonActionWithPlace:(Place *)place {
//    [[MapsNetworkManage sharedInstance] wantGoWithParameter:place complete:^(BOOL succeed, id obj) {
//        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
//        else [[NSNotificationCenter defaultCenter] postNotificationName:BJMapsDetailNeedReloadDataNotification object:nil];
//    }];
//}
#pragma mark - netWork
/**
 *  获取详情
 */
-(void)getMapsDetailHeaderRefresh:(BOOL)header{

    _postParameter.maps_id = [NSString stringWithFormat:@"%@",self.mapObject.id];
    _placeParameter.maps_id =[NSString stringWithFormat:@"%@",self.mapObject.id];
    if(self.showType == MAPS_PLAE){
        if(!header){
            Place *place = [_placeList lastObject];
            _placeParameter.last_id = [NSString stringWithFormat:@"%ld",(long)place.id];
        }else{
        _noMorePlace = NO;
        _placeParameter.last_id = @"0";
        }
        [[MapsNetworkManage sharedInstance] getMapsPlaceListWithParameter:_placeParameter complete:^(BOOL succeed, id obj) {
            if(succeed) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    _mapsDetail= [MapsDetail objectWithKeyValues:obj];
                    _frameObject = [[MapsDetailFrame alloc] initWithMapsDetail:_mapsDetail];
                   
                    if(header){
                        _placeList = [NSMutableArray arrayWithArray:_mapsDetail.place_list];
                    }else{
                        [_placeList insertObjects:_mapsDetail.place_list atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_placeList.count, _mapsDetail.place_list.count)]];
                    }
                    _placeFrameObj = [[PlaceFrameObject alloc] initWithPlaceList:_placeList];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectView reloadData];
                        if(_mapsDetail.place_list.count <[_placeParameter.count integerValue]){
                            self.noMorePlace = YES;
                            [self.collectView.footer endRefreshingWithNoMoreData];
                        }
                        if(header)[self.delegate isFollow:_mapsDetail];
                    });
                });
                
            }else{
                [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            }
            if(header) [self.collectView.header endRefreshing];
            else [self.collectView.footer endRefreshing];
        }];
    }else{
        if(!header){
            PostObjet *post = [_postList lastObject];
            _postParameter.last_id = [NSString stringWithFormat:@"%ld",(long)post.id];
        }else{
            _noMorePost = NO;
            _postParameter.last_id = @"0";
        }
    [[MapsNetworkManage sharedInstance] getMapsDetailWithParameter:_postParameter complete:^(BOOL succeed, id obj) {
        if(succeed) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                _mapsDetail= [MapsDetail objectWithKeyValues:obj];
                _frameObject = [[MapsDetailFrame alloc] initWithMapsDetail:_mapsDetail];
               
                if(header){
                    _postList = [NSMutableArray arrayWithArray:_mapsDetail.latest_post];
                }else{
                    
                    [_postList insertObjects:_mapsDetail.latest_post atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_postList.count, _mapsDetail.latest_post.count)]];
                }
                 _postFrameObj = [[PostFrameObject alloc] initWithPostList:_postList];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectView reloadData];
                    if(_mapsDetail.latest_post.count<[_postParameter.count integerValue]){
                        self.noMorePost = YES;
                        [self.collectView.footer endRefreshingWithNoMoreData];
                    }
                    //[self.delegate isFollow:_mapsDetail.isFollowed];
                });
            });
           
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
       if(header) [self.collectView.header endRefreshing];
       else [self.collectView.footer endRefreshing];
    }];
    }
    
}
/**
 * 点击阅读
 */
- (void)readMapsDetail{
    [[MapsNetworkManage sharedInstance]readMapsWithParameter:self.mapObject complete:^(BOOL succeed, id obj) {
        
    }];
}
/**
 *  need  reloadData
 */
- (void)reloadData{
    [self getMapsDetailHeaderRefresh:YES];
}
@end
