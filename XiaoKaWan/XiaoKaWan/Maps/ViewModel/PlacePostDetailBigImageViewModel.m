//
//  PlacePostDetailBigImageViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailBigImageViewModel.h"
#import "ShowImageViewController.h"

@interface PlacePostDetailBigImageViewModel ()
@end
@implementation PlacePostDetailBigImageViewModel{
    UIPageViewController *pageControler;
    NSInteger selectIndex;
}
- (instancetype)initWithPageController:(UIPageViewController *)pageController{
    self = [super init];
    if(self){
        pageControler = pageController;
        ShowImageViewController *controller = [self getControllerAtIndex:0];
        if(controller != nil) [pageController setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    return self;
}
- (void)setImageArray:(NSArray *)imageArray{
    _imageArray = imageArray;
    ShowImageViewController *controller = [self getControllerAtIndex:0];
    if(controller) [pageControler setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}
- (void)setSelectAtIndex:(NSInteger)selectAtIndex{
    ShowImageViewController *controller = [self selectControllerAtIndex:selectAtIndex];
    UIPageViewControllerNavigationDirection derection;
    if(selectAtIndex<selectIndex) derection = UIPageViewControllerNavigationDirectionReverse;
    else derection = UIPageViewControllerNavigationDirectionForward;
    [pageControler setViewControllers:@[controller] direction:derection animated:YES completion:nil];
    selectIndex = selectAtIndex;
}
- (ShowImageViewController *)selectControllerAtIndex:(NSInteger)index{
    if(_imageArray.count<=0){
        return nil;
    }
    ShowImageViewController * controller = [[ShowImageViewController alloc] init];
    controller.imageObject = _imageArray[index];
    controller.pageIndex = index;
    return controller;

}
- (ShowImageViewController*)getControllerAtIndex:(NSInteger)index{
    [self.delegate selectAtIndex:index];
    selectIndex = index;

    return [self selectControllerAtIndex:index];
}
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    ShowImageViewController *controller = (ShowImageViewController*)viewController;
   NSInteger index = controller.pageIndex;
    if(index>=_imageArray.count-1)return nil;
    index++;
    return [self getControllerAtIndex:index];
}
- (nullable UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    ShowImageViewController *controller = (ShowImageViewController*)viewController;
    NSInteger index = controller.pageIndex;
    if(index<=0)return nil;
    index--;
    return [self getControllerAtIndex:index];
}
#pragma mark - PlacePostDetailSmallImageViewModelDelegate
- (void)selectItemtAtIndex:(NSInteger)index{

}
@end
