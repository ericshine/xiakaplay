//
//  MapsPlaceDetailViewModel.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceDetailViewModel.h"
#import "BJMapsListCollectionLayout.h"
#import "MapsPlaceDetailCollectionViewCell.h"
#import "MapsCollectionViewCell.h"
#import "MapsDetailDynamicCollectionViewCell.h"
#import "TitleCollectionViewCell.h"
#import "PlaceMapsListViewController.h"
#import "PlacePostViewController.h"
#import "BJRefreshGitFooter.h"
#import "BJRefreshGitHeader.h"
#import "ShowMoreDetailView.h"
#import "MapDetailListViewController.h"
#import "MapsNetworkManage.h"
#import "Place.h"
#import "HeadSegmentCollectionView.h"
#import "PostListObj.h"
#import "PostObjet.h"
#import <MJExtension.h>
#import "QAlertView.h"
#import "PostFrameObject.h"
#import "PlaceMapsFrameObj.h"
#import "MapsNetworkManage.h"
#import "CurrentUser.h"
#import "PlacePostDetailViewController.h"
#import "UIColor+colorWithHex.h"
typedef enum {
    PLACE= 0,
    PLACE_MAPS, // 地点maps
    PLACE_POST // 地点动态
}PlaceShowType;
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
static NSString* const mapsPlaceDetailCell = @"placeDetailCell";
static NSString* const mapsCollcetionViewCell = @"mapsCollcetionViewCell";
static NSString* const mapsDetailDynamicCollectionViewCell = @"MapsDetailDynamicCollectionViewCell";
static NSString* const titleCollectionViewCell = @"TitleCollectionViewCell";
static NSString* const headViewCellIdentifier = @"headViewCellIdentifier";
static NSString* const headViewBlankCellIdentifier = @"headViewBlankCellIdentifier";
@interface MapsPlaceDetailViewModel()<BJMapsListCollectionDelegate,MapsCollectionViewCellDelegate,MapsPlaceDetailCollectionViewCellDelegate,HeadSegmentCollectionViewDelegate>
@property(nonatomic,assign) BJSuperViewController *viewController;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) BJMapsListCollectionLayout *layout;
@property(nonatomic,strong) NSMutableArray *mapsList;
@property(nonatomic,strong) NSMutableArray *postList;
@property(nonatomic) PlaceShowType showType;
@property(nonatomic,strong)PostListObj *postListObject;
@property(nonatomic,strong)PostFrameObject *postFrameObj;
@property(nonatomic,strong)PlaceMapsFrameObj *mapsFrameObj;
@property(nonatomic) BOOL isMorePost;
@property(nonatomic) BOOL isMoreMaps;
@end
@implementation MapsPlaceDetailViewModel
- (instancetype)initWithModel:(id)model controller:(BJSuperViewController *)controller collectionView:(UICollectionView *)collectionView{
    self = [super init];
    if(self){
        self.place = model;
        self.viewController = controller;
        self.collectionView = collectionView;
        self.showType = PLACE_POST;
        [self.collectionView setCollectionViewLayout:self.layout];
        /**
         *  headCell
         */
        [self.collectionView registerClass:[MapsPlaceDetailCollectionViewCell class] forCellWithReuseIdentifier:mapsPlaceDetailCell];
        /**
         *  mapsCell
         */
        [self.collectionView registerClass:[MapsCollectionViewCell class] forCellWithReuseIdentifier:mapsCollcetionViewCell];
        /**
         *  postCell
         */
        [self.collectionView registerClass:[MapsDetailDynamicCollectionViewCell class] forCellWithReuseIdentifier:mapsDetailDynamicCollectionViewCell];
       
        /**
         *  headView
         */
        [self.collectionView registerClass:[HeadSegmentCollectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headViewCellIdentifier];
        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headViewBlankCellIdentifier];
        [self getHeadData];
        [self refreshData];
    }
    return self;
}
- (void)refreshData{
    BJRefreshGitFooter *footer = [BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        [self getPlaceData:NO];
    }];
    BJRefreshGitHeader *header = [BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        [self getPlaceData:YES];
    }];
    self.collectionView.header = header;
    self.collectionView.footer = footer;
    [self.collectionView.header  beginRefreshing];
}
#pragma mark - network
- (void)getHeadData{
    PlaceParameter *parameter = [[PlaceParameter alloc] init];
    parameter.place_id = self.place.placeId;
[[MapsNetworkManage sharedInstance] getPlaceDetailWithParameter:parameter complete:^(BOOL succeed, id obj) {
    self.place = [Place objectWithKeyValues:obj];
     _postFrameObj.place = self.place;
    self.place.placeId = [NSString stringWithFormat:@"%li",(long)self.place.id];
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self.collectionView reloadData];
}];
}
- (void)getPlaceData:(BOOL)header{
    PlaceParameter *parameter = [[PlaceParameter alloc] init];
    parameter.place_id = self.place.placeId;
    if(self.showType == PLACE_POST){
    if(header==YES){
    parameter.last_id = @"0";
        _isMorePost = YES;
//        [_postList removeAllObjects];
//        self.layout = nil;
//        [self.collectionView setCollectionViewLayout:self.layout];
    }else{
        PostObjet *lasPost = [_postList lastObject];
        parameter.last_id = [NSString stringWithFormat:@"%li",(long)lasPost.id];
    }
    [[MapsNetworkManage sharedInstance] getPlacePostListWithParameter:parameter complete:^(BOOL succeed, id obj) {
        if(succeed){
        _postListObject = [PostListObj objectWithKeyValues:obj];
            if(header){
                _postList = [[NSMutableArray alloc] initWithArray:_postListObject.postList];
            }else{
                [_postList insertObjects:_postListObject.postList atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_postList.count, _postListObject.postList.count)]];
            }
             _postFrameObj = [[PostFrameObject alloc] initWithPostList:_postList];
            [self.collectionView.collectionViewLayout invalidateLayout];
            [self.collectionView reloadData];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
        if(header)[self.collectionView.header endRefreshing];
        else [self.collectionView.footer endRefreshing];
        if(_postListObject.postList.count<[parameter.count integerValue]){
            _isMorePost = NO;
            [self.collectionView.footer endRefreshingWithNoMoreData];
        }
         _postFrameObj.place = self.place;
    }];
    }else{
        if(header){
            parameter.last_id = @"0";
            _isMoreMaps = YES;
            [_mapsList removeAllObjects];
        }else{
            MapsDetail *mapsDetail = [_mapsList lastObject];
            parameter.last_id = [NSString stringWithFormat:@"%li",(long)mapsDetail.id];
        }
        [[MapsNetworkManage sharedInstance] getPlaceMapsListWithParameter:parameter complete:^(BOOL succeed, id obj) {
            if(succeed){
            _postListObject = [PostListObj objectWithKeyValues:obj];
                if(header){
                    _mapsList = [[NSMutableArray alloc] initWithArray:_postListObject.maps];
                }else{
                    [_mapsList insertObjects:_postListObject.maps atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_mapsList.count, _postListObject.maps.count)]];
                }
                _mapsFrameObj = [[PlaceMapsFrameObj alloc] initWithMapsArray:_mapsList];
                [self.collectionView.collectionViewLayout invalidateLayout];
                 [self.collectionView reloadData];
            }else{
                [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            }
            if(header)[self.collectionView.header endRefreshing];
            else [self.collectionView.footer endRefreshing];
            if(_postListObject.maps.count<[parameter.count integerValue]){
                _isMoreMaps = NO;
                [self.collectionView.footer endRefreshingWithNoMoreData];
            }
        }];
    }
}
#pragma mark - layout
- (BJMapsListCollectionLayout *)layout{
    if(_layout == nil){
        _layout = [[BJMapsListCollectionLayout alloc] init];
        _layout.isStickyHeader = YES;
        _layout.delegate = self;
    }
    return _layout;
}
#pragma mark - BJMapsListCollectionDelegate
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        return self.postFrameObj==nil?265:self.postFrameObj.headViewHeight;
    }
    if(self.showType == PLACE_POST){
       
        CGFloat cellHeitht = [self.postFrameObj.postItemHeights[indexPath.row] floatValue];
        return cellHeitht;
    }else{
        CGFloat cellHeitht = [self.mapsFrameObj.mapsCellHeight[indexPath.row] floatValue];
        return cellHeitht;
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(BJMapsListCollectionLayout *)collectionViewLayout widthForItemInSection:(NSInteger)section
{
    if(section == 1 && self.showType == PLACE_POST){
      return (kDeviceWidth-30)/2;
    }
    else return kDeviceWidth;
}

#pragma mark - collectionView delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    if(section ==0)return 1;
    else{
        if(self.showType == PLACE_POST) return self.postList.count;
        else return self.mapsList.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
    MapsPlaceDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsPlaceDetailCell forIndexPath:indexPath];
        cell.delegate = self;
        cell.place = self.place;
        cell.titles = @[@"动态",@"榜单"];
//        cell.object = _postListObject;
    return cell;
    }else{
        if(self.showType == PLACE_POST){
            MapsDetailDynamicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsDetailDynamicCollectionViewCell forIndexPath:indexPath];
            cell.postObject = self.postList[indexPath.row];
            return cell;
        }else{
            MapsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:mapsCollcetionViewCell forIndexPath:indexPath];
            cell.mapsObj = _mapsList[indexPath.row];
            cell.delegate = self;
            return cell;
        }
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        if(self.showType == PLACE_POST){
            PostObjet *postObect = self.postList[indexPath.row];
            [self toPostDetail:postObect];
        }else{
        MapsDetail *mapsDetail = self.mapsList[indexPath.row];
        [self touchMapsWithModel:mapsDetail];
        }
    }
}
- (void)toPostDetail:(PostObjet*)postObject{
    PostParatemer *parameter = [PostParatemer parameter];
    parameter.post_id = [NSString stringWithFormat:@"%li",(long)postObject.id];
    PlacePostDetailViewController *postDetailVc = [[PlacePostDetailViewController alloc] init];
    postDetailVc.parameter = parameter;
    [self.viewController push:postDetailVc];
}



#pragma mark - MapsCollectionViewCellDelegate
- (void)touchMapsWithModel:(id)model{
    MapsDetail *mapsDetail = model;
    MapObject *mapsObj = [[MapObject alloc] init];
    mapsObj.id = [NSString stringWithFormat:@"%li",(long)mapsDetail.id];
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    detailList.mapObject = mapsObj;
    [self.viewController push:detailList];

}
#pragma mark -MapsPlaceDetailCollectionViewCellDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] beenWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJBeenActionNotification object:nil];
            place.isBeen = !place.isBeen;
            NSInteger offNumber;
            if(place.isBeen){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                [users addObject:[self getCurrentUser]];
                place.beenUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.beenUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
                NSArray *usersBeen = [users filteredArrayUsingPredicate:predicate];
                place.beenUsers = usersBeen;
            }
            place.beenCount = place.beenCount+offNumber;
            
            if(place1)place1(place);
        };
    }];
}

- (void)wantGoAction:(Place *)place callBack:(void (^)(Place *))place1{
    [[MapsNetworkManage sharedInstance] wantGoWithParameter:place complete:^(BOOL succeed, id obj) {
        if(!succeed)[[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:BJWantActionNotification object:nil];
            place.isWant = !place.isWant;
            NSInteger offNumber;
            if(place.isWant){
                offNumber = +1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                [users addObject:[self getCurrentUser]];
                place.wantUsers = [users copy];
            }
            else {
                offNumber = -1;
                NSMutableArray *users = [[NSMutableArray alloc] initWithArray:place.wantUsers];
                User *user = [self getCurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid != %i",user.uid];
                NSArray *userswant = [users filteredArrayUsingPredicate:predicate];
                place.wantUsers = userswant;
            }
            place.wantCount = place.wantCount+offNumber;
            
            if(place1)place1(place);
        };
    }];
}
- (void)userList:(Place *)place{
    [self.delegate toUserList:place];
}
- (void)moreDetailContent:(NSString *)content{
    if(content.length<=0){
        [[QAlertView sharedInstance] showAlertText:@"暂无简介" fadeTime:2];
        return;
    }
    ShowMoreDetailView *showView = [ShowMoreDetailView showViewInView:self.viewController.tabBarController.view point:CGRectMake(0, 0, 200, 200)];
    showView.contentString = content;
}
- (void)selectAtIndex:(NSInteger)index{
    if(index == 0){
        self.showType = PLACE_POST;
        self.collectionView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        if(_isMorePost) [self.collectionView.footer resetNoMoreData];
        [self.collectionView.collectionViewLayout invalidateLayout];
        [self.collectionView reloadData];
    }else{
        self.showType = PLACE_MAPS;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        if(_isMoreMaps) [self.collectionView.footer resetNoMoreData];
        if(self.mapsList.count == 0 || self.mapsList == nil){
            [self getPlaceData:YES];
        }else{
            [self.collectionView.collectionViewLayout invalidateLayout];
            [self.collectionView reloadData];
        }
    }
}
- (void)mapNavigation{
    [self.delegate thirdpartyNavigation];
}
#pragma mark currentUser
- (User *)getCurrentUser{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    User *user  = [[User alloc] init];
    user.nick = currentUser.nick;
    user.avatar = currentUser.avatar;
    user.uid = [currentUser.uid integerValue];
    return user;
}

@end
