//
//  AddPlaceToMapsViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Maps.h"
@protocol AddPlaceToMapsViewModelDelegate <NSObject>

- (void)selectMaps:(Maps *)maps;

@end
@interface AddPlaceToMapsViewModel : NSObject<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)id<AddPlaceToMapsViewModelDelegate>delegate;
- (instancetype)initWithTable:(UITableView*)table;
@end

