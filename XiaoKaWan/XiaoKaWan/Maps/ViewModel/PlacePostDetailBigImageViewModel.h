//
//  PlacePostDetailBigImageViewModel.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol PlacePostDetailBigImageViewModelDelegate <NSObject>

- (void)selectAtIndex:(NSInteger)index;

@end

@interface PlacePostDetailBigImageViewModel : NSObject<UIPageViewControllerDelegate,UIPageViewControllerDataSource>
@property(nonatomic,assign) id<PlacePostDetailBigImageViewModelDelegate>delegate;
- (instancetype)initWithPageController:(UIPageViewController *)pageController;
@property(nonatomic,strong)NSArray *imageArray;
@property(nonatomic) NSInteger selectAtIndex;
@end
