//
//  MapsNetworkManage.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "ClientNetwork.h"
#import "MapDetailNetworkParameter.h"
#import "MapObject.h"
#import "Place.h"
#import "MapsPostNetoworkParameter.h"
#import "PostParatemer.h"
#import "PlaceParameter.h"
#define BJAPIDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)
@interface MapsNetworkManage : NSObject
SINGLETON_H
/**
 *  获取maps 详情
 *
 *  @param parametar parametar description
 *  @param complete  complete description
 */
- (void)getMapsDetailWithParameter:(MapDetailNetworkParameter*)parametar complete:(requestComplete)complete;
/**
 *  获取maps 地点列表
 *
 *  @param parametar parametar description
 *  @param complete  complete description
 */
- (void)getMapsPlaceListWithParameter:(MapDetailNetworkParameter*)parametar complete:(requestComplete)complete;
/**
 *  统计阅读数
 *
 *  @param parametar parametar description
 *  @param complete  complete description
 */
- (void)readMapsWithParameter:(MapObject *)parametar complete:(requestComplete)complete;
/**
 *  follow maps
 *
 *  @param parametar parametar description
 *  @param complete  complete description
 */
- (void)followWithParameter:(MapObject *)parametar complete:(requestComplete)complete;
/**
 *  place 想去
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)wantGoWithParameter:(Place*)parameter complete:(requestComplete)complete ;
- (void)wantGoAction:(Place *)place callBack:(void (^)(Place *))place1;
/**
 *  place 去过
 *
 *  @param parameter
 *  @param complete
 */
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)beenWithParameter:(Place*)parameter complete:(requestComplete)complete ;
/**
 *  获取maps的动态
 *
 *  @param parameter parameter description
 *  @param complete  complete description
 */
- (void)getMapsPostWithParameter:(MapsPostNetoworkParameter*)parameter complete:(requestComplete)complete;
/**
 *  想去的人列表
 *
 *  @param parameter place_id	地点ID	必填; last_id 数据自增ID(id)上一个数据的id ; count 单次获取数量 可选, 默认10;get_type获取顺序方向 可选, 默认earlier

 *  @param complete  complete description
 */
- (void)getWantUserListWithParameter:(MapsPostNetoworkParameter*)parameter complete:(requestComplete)complete;
/**
 *  去过的人列表
 *
 *  @param parameter  parameter place_id	地点ID	必填; last_id 数据自增ID(id)上一个数据的id ; count 单次获取数量 可选, 默认10;get_type获取顺序方向 可选, 默认earlier
 *  @param complete  complete description
 */
- (void)getbeenUserListWithParameter:(MapsPostNetoworkParameter*)parameter complete:(requestComplete)complete;
/**
 *  动态详情
 *
 *  @param parameter post_id 动态ID;last_id 评论ID 上一个评论数据的id;count单次获取数量可选, 默认10;get_type获取顺序方向 可选, 默认earlier;hot_coun热门 可选，默认10
 *  @param complete  complete description
 */
- (void)getMapPostDetailWithParameter:(PostParatemer*)parameter complete:(requestComplete)complete;
/**
 *  评论动态
 *
 *  @param parameter post_id 动态ID;content 内容
 *  @param complete  complete description
 */
- (void)commentPostWithParameter:(PostParatemer*)parameter complete:(requestComplete)complete;
/**
 *  动态点赞
 *
 *  @param parameter post_id 动态id；is_remove 0为去过，1为取消去过；_t token
 *  @param complete  complete description
 */
- (void)postLikeWithParameter:(PostParatemer*)parameter complete:(requestComplete)complete;
/**
 *  评论点赞
 *
 *  @param parameter comment_id 评论id；is_remove 0为去过，1为取消去过；_t token
 *  @param complete  complete description
 */
- (void)commentLikeWithParameter:(PostParatemer*)parameter complete:(requestComplete)complete;
/**
 *  获取地点详情
 *
 *  @param parameter place_id 、last_id 、count、 get_type
 *  @param complete  complete description、
 */
- (void)getPlaceDetailWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete /*BJAPIDeprecated("使用getPlaceMapsListWithParameter 和 getPlacePostListWithParameter 代替")*/;
/**
 *  获取地点maps 列表
 *
 *  @param parameter place_id 、last_id 、count、 get_type
 *  @param complete
 */
- (void)getPlaceMapsListWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete;
/**
 *  获取地点的动态列表
 *
 *  @param parameter place_id 、last_id 、count、 get_type
 *  @param complete  complete description
 */
- (void)getPlacePostListWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete;
/**
 *  删除动态
 *
 *  @param parameter post_id,_t
 *  @param complete  
 */
- (void)deletePostWithParameter:(PostParatemer *)parameter complete:(requestComplete)complete;
#pragma mark - 我的列表
/**
 *  获取我的maps
 *
 *  @param parameter last_id;count;get_type
 *  @param complete  complete description
 */
- (void)getMyMapsListWithParameter:(MapsPostNetoworkParameter*)parameter complete:(requestComplete)complete;

/**
 *  个人主页-地点
 *
 *  @param parameter last_id;count;get_type;place_type(want / been)
 *  @param complete  complete description
 */
- (void)getMyPlceListWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete;
/**
 *  地点加入榜单
 *
 *  @param parameter maps_id;place_id;add_reason;
 *  @param complete  complete description
 */
- (void)addPlaceToMapsWithParameter:(PlaceParameter*)parameter complete:(requestComplete)complete;
@end
