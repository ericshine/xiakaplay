//
//  PlaceParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/22/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaceParameter : NSObject
@property(nonatomic,copy) NSString *place_id;
@property(nonatomic,copy) NSString *last_id;
@property(nonatomic,copy) NSString *count;
@property(nonatomic,copy) NSString *get_type;
@property(nonatomic,copy) NSString *_t;
@property(nonatomic,copy) NSString *add_reason;
@property(nonatomic,copy) NSString *place_type; //want / been
@property(nonatomic,copy) NSString *maps_id;
@property(nonatomic,copy) NSString *uid;
@end
