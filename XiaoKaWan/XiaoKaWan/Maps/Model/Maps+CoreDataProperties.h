//
//  Maps+CoreDataProperties.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Maps.h"

NS_ASSUME_NONNULL_BEGIN

@interface Maps (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *descriptions;
@property (nullable, nonatomic, retain) NSNumber *placeCount;
@property (nullable, nonatomic, retain) NSNumber *followCount;
@property (nullable, nonatomic, retain) NSNumber *postCount;
@property (nullable, nonatomic, retain) NSNumber *userCount;
@property (nullable, nonatomic, retain) NSData *photos;
@property (nullable, nonatomic, retain) NSSet<MapsUser *> *mapsUser;

@end

@interface Maps (CoreDataGeneratedAccessors)

- (void)addMapsUserObject:(MapsUser *)value;
- (void)removeMapsUserObject:(MapsUser *)value;
- (void)addMapsUser:(NSSet<MapsUser *> *)values;
- (void)removeMapsUser:(NSSet<MapsUser *> *)values;

@end

NS_ASSUME_NONNULL_END
