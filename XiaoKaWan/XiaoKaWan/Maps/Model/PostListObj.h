//
//  PostListObj.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostObjet.h"
#import "MapsDetail.h"
@interface PostListObj : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *id;
@property(nonatomic,strong) NSArray<PostObjet*> *postList;
@property(nonatomic,strong) NSArray<MapsDetail*>*maps;
@property(nonatomic,copy) NSString *address;
@property(nonatomic,strong) NSArray<User*> *beenUsers;//去过的人
@property(nonatomic,strong) NSArray<User*> *wantUsers;
@property(nonatomic) NSInteger beenCount;// 多少人去过
@property(nonatomic) NSInteger wantCount;// 多少人想去
@property(nonatomic,copy) NSString *postCount;
@property(nonatomic,copy) NSString *maspCount;
@end
