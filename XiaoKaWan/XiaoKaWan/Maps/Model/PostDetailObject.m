//
//  PostDetailObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostDetailObject.h"
#import <MJExtension.h>
#import "NSString+Cagetory.h"
@implementation PostDetailObject
- (instancetype)init{
    self = [super init];
    if(self){
        [PostDetailObject setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"mapsId":@"maps_id",
                     @"placeId":@"place_id",
                     @"userId":@"user_id",
                     @"commentCount":@"comment_count",
                     @"likeCount":@"like_count",
                     @"actionDate":@"action_date",
                     @"gmtCreated":@"gmt_created",
                     @"gmtModified":@"gmt_modified",
                     @"placeInfo":@"place_info",
                     @"user":@"post_user",
                     @"hotComments":@"hot_comments",
                     @"lastComments":@"new_comments",
                     @"loginUserLike":@"login_user_like"
                     };
        }];
        [PostDetailObject setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"placeInfo":[Place class],
                     @"photos":[BJImageObject class],
                     @"user":[User class],
                     @"hotComments":[CommetObject class],
                      @"lastComments":[CommetObject class],
                     @"tags":[NSString class]
                     };
        }];
    }
    return self;
}
- (CGFloat)cellHeight{
    CGFloat height = 250;
    if(self.photos.count>0) height += 385;
    if(self.tags.count>0) height +=30;
    CGSize size = [self.content sizeWithFont:[UIFont systemFontOfSize:15] andSize:CGSizeMake(kDeviceWidth-30, 9999)];
    height += size.height;
    return height;
}
@end
