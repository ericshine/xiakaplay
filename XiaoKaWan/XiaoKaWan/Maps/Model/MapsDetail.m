//
//  MapsDetail.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetail.h"
#import <MJExtension.h>
@implementation MapsDetail
- (instancetype)init{
    self =[super init];
    if(self){
        [MapsDetail setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"latest_photos":[BJImageObject class],
                     @"latest_post":[PostObjet class],
                     @"place_list":[Place class],
                     @"latest_users":[User class]
                     };
        }];
        [MapsDetail setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{@"content":@"description",
                     @"isFollowed":@"login_user_followed",
                     @"addReason":@"add_reason",
                     @"createUserId":@"create_user_id"
                     
                     };
        }];
    }
    return self;
}
- (void)setValue:(id)value forKey:(NSString *)key{
    [super setValue:value forKey:key];
}
+ (NSMutableArray *)dicToObject:(NSArray *)array{
    NSMutableArray *objects = [[NSMutableArray alloc] initWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsDetail *maps = [MapsDetail objectWithKeyValues:obj];
        [objects addObject:maps];
    }];
    return objects;
}
@end
