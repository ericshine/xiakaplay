//
//  PlaceMapsFrameObj.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceMapsFrameObj.h"
#import "MapsDetail.h"
#import "NSString+Cagetory.h"
@implementation PlaceMapsFrameObj{
    NSArray *mapsList;
}
- (instancetype)initWithMapsArray:(NSArray *)mapsArray{
    self = [super init];
    if(self){
        mapsList = mapsArray;
    }
    return self;
}
- (NSArray *)mapsCellHeight{
    if(_mapsCellHeight == nil){
        NSMutableArray *heights = [[NSMutableArray alloc] initWithCapacity:mapsList.count];
    [mapsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsDetail *mapsObj = obj;
        CGFloat height = 312+73 +24;
        CGSize size = [mapsObj.addReason sizeWithFont:[UIFont systemFontOfSize:[PlaceMapsFrameObj reasonLabelFont]] andSize:CGSizeMake(kDeviceWidth-30, 999)];
        if(size.height>40){
            height += 40;
        }else{
            height += size.height;
        }
        [heights addObject:[NSNumber numberWithFloat:height]];
    }];
        _mapsCellHeight =  [heights copy];
    }
    return _mapsCellHeight;
}
+ (CGFloat)reasonLabelFont{
    return 12;
}
@end
