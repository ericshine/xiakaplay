//
//  MapsDetailFrame.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MapsDetail.h"
@interface MapsDetailFrame : NSObject
- (instancetype)initWithMapsDetail:(MapsDetail*)mapsDetail;
@property(nonatomic) CGFloat headCellHeight;
@end
