//
//  User.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "User.h"
#import <MJExtension.h>
@implementation User
- (instancetype)init{
    self = [super init];
    if(self){
    self.avatar = @"";
    [User setupReplacedKeyFromPropertyName:^NSDictionary *{
        return @{
                 @"activeTime":@"active_time_h"
                 };
    }];
    }
    return self;
}
+ (User *)coreDataUserToUser:(CurrentUser *)currentUser{
   
    User *user = [[User alloc] init];
    user.isMyInformation = YES;
    user.uid = currentUser.uidValue;
    user.nick = currentUser.nick;
    user.avatar = currentUser.avatar;
    user.sign = currentUser.sign;
    user.followers_count = [NSString stringWithFormat:@"%@",currentUser.followers_count];
    user.following_count = [NSString stringWithFormat:@"%@",currentUser.following_count];
    user.maps_count = [NSString stringWithFormat:@"%@",currentUser.maps_count];
    user.maps_create_count = [NSString stringWithFormat:@"%@",currentUser.maps_create_count];
    user.post_count = [NSString stringWithFormat:@"%@",currentUser.post_count];
    user.been_count = [NSString stringWithFormat:@"%@",currentUser.been_count];
    user.want_count = [NSString stringWithFormat:@"%@",currentUser.want_count];
    return user;
}
@end
