//
//  Maps+CoreDataProperties.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Maps+CoreDataProperties.h"

@implementation Maps (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic descriptions;
@dynamic placeCount;
@dynamic followCount;
@dynamic postCount;
@dynamic userCount;
@dynamic photos;
@dynamic mapsUser;

@end
