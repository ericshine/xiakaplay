#import "MapsUser.h"
#import "MapsModelManage.h"
#import <MJExtension.h>

@interface MapsUser ()



@end

@implementation MapsUser
+ (MapsUser *)saveUserWithDic:(NSDictionary *)dic{
    MapsUser *mapsUser = [MapsUser getuserWithId:[dic[@"uid"] integerValue]];
    if(mapsUser == nil){
        mapsUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
    }
    [mapsUser setKeyValues:dic];
    mapsUser = [MapsUser objectWithKeyValues:dic context:[MapsModelManage sharedInstance].context];
    return mapsUser;
}
+ (void)saveMapUser:(NSArray *)array withMaps:(Maps *)maps{
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapsUser = [MapsUser getMapsUserWithId:[obj[@"uid"] integerValue] andMapsId:maps.idValue];//[MapsUser getMapsWithId:[obj[@"uid"] integerValue]];
        if(mapsUser == nil){
            mapsUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
//        mapsUser.userMaps = maps;
        [maps addMapsUserObject:mapsUser];
        mapsUser.mapsId = maps.id;
//        mapsUser = [MapsUser objectWithKeyValues:obj context:[MapsModelManage sharedInstance].context];
        [mapsUser setKeyValues:obj];
    }];
}
+ (void)savebeenUser:(NSArray *)array withPlace:(MyPlaceObject *)place{
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapsUser = [MapsUser getBeenUserWithId:[obj[@"uid"] integerValue] andMapsId:place.idValue];//[MapsUser getMapsWithId:[obj[@"uid"] integerValue]];
        if(mapsUser == nil){
            mapsUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        mapsUser.placeBeen = place;
        //mapsUser.myPlaceBeen = place;
        mapsUser.beenPlaceId = place.id;
//        mapsUser =  [MapsUser objectWithKeyValues:obj context:[MapsModelManage sharedInstance].context];
        [mapsUser setKeyValues:obj];
    }];
}
+ (void)saveWantUser:(NSArray *)array withPlace:(MyPlaceObject *)place{
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapsUser = [MapsUser getWantUserWithId:[obj[@"uid"] integerValue] andMapsId:place.idValue];//[MapsUser getMapsWithId:[obj[@"uid"] integerValue]];
        if(mapsUser == nil){
            mapsUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        mapsUser.wantPlaceId = place.id;
        mapsUser.placeWant = place;
      //  mapsUser.myPlaceWant = place;
        [mapsUser setKeyValues:obj];
//        mapsUser = [MapsUser objectWithKeyValues:obj context:[MapsModelManage sharedInstance].context];
        
    }];
}
+ (MapsUser *)getuserWithId:(NSInteger)uid {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"uid = %i ",uid];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (MapsUser *)getMapsUserWithId:(NSInteger)uid andMapsId:(NSInteger)mapsId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"uid = %i and mapsId = %i ",uid,mapsId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (MapsUser *)getWantUserWithId:(NSInteger)uid andMapsId:(NSInteger)wantPlaceId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"uid = %i and wantPlaceId = %i ",uid,wantPlaceId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (MapsUser *)getBeenUserWithId:(NSInteger)uid andMapsId:(NSInteger)beenPlaceId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"uid = %i and beenPlaceId = %i ",uid,beenPlaceId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (NSArray *)mapsUserMapsId:(NSInteger)mapsId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"mapsId = %i",mapsId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;
}
+ (NSArray *)beenUserPlaceId:(NSInteger)placeId{

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"beenPlaceId = %i",placeId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;

}
+ (NSArray *)wantUserPlaceId:(NSInteger)placeId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"wantPlaceId = %i",placeId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;

}
+ (NSArray *)getAllUser{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    return result;
}

@end
