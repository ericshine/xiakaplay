#import "MyPlaceObject.h"
#import "MapsModelManage.h"
#import <MJExtension.h>
#import "MapsUser.h"
@interface MyPlaceObject ()

// Private interface goes here.

@end

@implementation MyPlaceObject
+ (void)objectMaping{
    [MyPlaceObject setupReplacedKeyFromPropertyName:^NSDictionary *{
        return @{
                 @"descriptions":@"description",
                 @"isWant":@"login_user_want",
                 @"isBeen":@"login_user_been",
                 @"placeId":@"place_id"
                 };
    }];
}
+ (MyPlaceObject *)savePlaceWithDic:(NSDictionary *)dic{
    MyPlaceObject *place = [MyPlaceObject getPlaceObjcetWithId:[dic[@"id"] integerValue]];
    if(place == nil){
        place = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
    }
    [place setKeyValues:dic];
    place = [MyPlaceObject objectWithKeyValues:dic context:[MapsModelManage sharedInstance].context];
    return place;
}
+ (NSMutableArray *)savePlaceBeenObjcets:(NSArray *)obects{
     NSMutableArray *placeListArray = [[NSMutableArray alloc] initWithCapacity:obects.count];
    [self objectMaping];
    [obects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MyPlaceObject *place = [self savePlaceWithDic:obj];
        place.isBeenValue = YES;
        [MapsUser savebeenUser:obj[@"been_users"] withPlace:place];
        [MapsUser saveWantUser:obj[@"want_users"] withPlace:place];
        [placeListArray addObject:place];
    }];
    [[MapsModelManage sharedInstance] saveContext];
    return placeListArray;
}

+ (NSMutableArray *)savePlaceWantObjcets:(NSArray *)obects{
    NSMutableArray *placeListArray = [[NSMutableArray alloc] initWithCapacity:obects.count];
    [self objectMaping];
    [obects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MyPlaceObject *place = [self savePlaceWithDic:obj];
        place.isWantValue = YES;
        [MapsUser savebeenUser:obj[@"been_users"] withPlace:place];
        [MapsUser saveWantUser:obj[@"want_users"] withPlace:place];
        [placeListArray addObject:place];
    }];
    [[MapsModelManage sharedInstance] saveContext];
    return placeListArray;
}
+ (MyPlaceObject *)getPlaceObjcetWithId:(NSInteger)idNum{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    request.predicate = [NSPredicate predicateWithFormat:@"id = %i",idNum];
    NSArray *resultArray = [[MapsModelManage sharedInstance].context executeFetchRequest:request error:nil];
    if(!resultArray.count){
        return nil;
    }
    return resultArray[0];
}
+ (NSArray<MyPlaceObject*> *)getPlaceBeen{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    request.predicate = [NSPredicate predicateWithFormat:@"isBeen = %@",[NSNumber numberWithBool:YES]];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"gmt_created" ascending:NO];
    [request setSortDescriptors:@[sort]];
    NSArray *resultArray = [[MapsModelManage sharedInstance].context executeFetchRequest:request error:nil];
    if(!resultArray.count){
        return nil;
    }
    return resultArray;
}
+ (NSArray<MyPlaceObject*> *)getPlaceWant{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    request.predicate = [NSPredicate predicateWithFormat:@"isWant = %@",[NSNumber numberWithBool:YES]];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"gmt_created" ascending:NO];
    [request setSortDescriptors:@[sort]];
    NSArray *resultArray = [[MapsModelManage sharedInstance].context executeFetchRequest:request error:nil];
    if(!resultArray.count){
        return nil;
    }
    return resultArray;
}
+ (void)clearMyBeenCache{
    for(NSManagedObject *obj in [self getPlaceBeen]){
        [[MapsModelManage sharedInstance].context deleteObject:obj];
    }
}
+ (void)clearMyWantCache{
    for(NSManagedObject *obj in [self getPlaceWant]){
        [[MapsModelManage sharedInstance].context deleteObject:obj];
    }
}
// Custom logic goes here.

@end
