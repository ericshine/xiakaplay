#import "_MapsUser.h"
#import "Maps.h"
#import "MyPlaceObject.h"
@interface MapsUser : _MapsUser {}
+(MapsUser *)saveUserWithDic:(NSDictionary *)dic;
+ (void)saveMapUser:(NSArray*)array withMaps:(Maps*)maps;
+ (void)savebeenUser:(NSArray *)array withPlace:(MyPlaceObject *)place;
+ (void)saveWantUser:(NSArray *)array withPlace:(MyPlaceObject *)place;
+ (NSArray *)mapsUserMapsId:(NSInteger)mapsId;
+ (NSArray *)beenUserPlaceId:(NSInteger)placeId;
+ (NSArray *)wantUserPlaceId:(NSInteger)placeId;
+ (NSArray *)getAllUser;
@end
