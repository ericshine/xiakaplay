#import "UserPost.h"
#import <MJExtension.h>
#import "MapsUser.h"
#import "MyPlaceObject.h"
@interface UserPost ()

// Private interface goes here.

@end

@implementation UserPost
+ (NSMutableArray *)savePostData:(NSArray *)postList{
    NSMutableArray *posts = [[NSMutableArray alloc] initWithCapacity:postList.count];
    [postList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UserPost *post = [self getPostWithId:[obj[@"id"] integerValue]];
        if(post == nil){
            post = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        [post setKeyValues:obj];
//        post = [UserPost objectWithKeyValues:obj context:[MapsModelManage sharedInstance].context];
        if(obj[@"latest_post_frist_image"] != nil) post.images = [NSJSONSerialization dataWithJSONObject:obj[@"latest_post_frist_image"] options:NSJSONWritingPrettyPrinted error:nil];
        [post setPostUser:[MapsUser saveUserWithDic:obj[@"post_user"]]];
        [post setPlace:[MyPlaceObject savePlaceWithDic:obj[@"place_info"]]];
        [posts addObject:post];
    }];
    [[MapsModelManage sharedInstance] saveContext];
    return posts;
}
+ (UserPost *)getPostWithId:(NSUInteger)postId{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    request.predicate = [NSPredicate predicateWithFormat:@"id = %i",postId];
    NSArray *results = [[MapsModelManage sharedInstance].context executeFetchRequest:request error:nil];
    if(!results.count){
        return nil;
    }
    return results[0];
}
+ (NSArray *)getAllPost{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"gmt_created" ascending:NO];
    [request setSortDescriptors:@[sort]];
    NSArray *results = [[MapsModelManage sharedInstance].context executeFetchRequest:request error:nil];
    if(!results.count){
        return nil;
    }
    return results;
}
// Custom logic goes here.
+ (void)clearCache{
    for(NSManagedObject *obj in [self getAllPost]){
        [[MapsModelManage sharedInstance].context deleteObject:obj];
    }
}
@end
