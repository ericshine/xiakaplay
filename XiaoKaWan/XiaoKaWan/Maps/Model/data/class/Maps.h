#import "_Maps.h"

@interface Maps : _Maps {}
+ (NSMutableArray *)saveMapsData:(NSArray *)mapsList isMyMaps:(BOOL)isMyMaps;
// Insert code here to declare functionality of your managed object subclass
+ (NSArray<Maps*>*)getMapsList;
+ (NSArray<Maps*>*)getMyMapsList;
+ (Maps *)getMapsWithId:(NSInteger)mapsId;
+ (void)clearCache;
+ (void)clearMyMapsCache;
@end
