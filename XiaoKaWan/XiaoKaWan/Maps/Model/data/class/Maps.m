#import "Maps.h"
#import "MapsUser.h"
#import "MapsModelManage.h"
#import <MJExtension.h>
@interface Maps ()


// Private interface goes here.

@end

@implementation Maps
+ (void)objectMaping{
        [Maps setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"descriptions":@"description",
                     @"createUserid":@"create_user_id"
                     };
        }];
}
+ (NSMutableArray *)saveMapsData:(NSArray *)mapsList isMyMaps:(BOOL)isMyMaps{
    [self objectMaping];
    NSMutableArray *mapsListArray = [[NSMutableArray alloc] initWithCapacity:0];

    for(NSDictionary *obj in mapsList){
        Maps *maps =[Maps getMapsWithId:[obj[@"id"] integerValue]];
        if(maps == nil){
            maps = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        [maps setKeyValues:obj];
        NSArray *imageUrls = obj[@"latest_photos"];
        if(![obj[@"latest_photos"] isKindOfClass:[NSString class]]){
            if(imageUrls != nil&&imageUrls.count){
                NSData *photoData = [NSJSONSerialization dataWithJSONObject:obj[@"latest_photos"] options:NSJSONWritingPrettyPrinted error:nil];
                maps.photos = photoData;
            }
        }
        maps.isMyMaps =  [NSNumber numberWithBool:isMyMaps];
//        maps = [Maps objectWithKeyValues:obj context:[MapsModelManage sharedInstance].context];
        if(!(obj[@"latest_users"] == nil || [obj[@"latest_users"] isKindOfClass:[NSString class]])){
            [MapsUser saveMapUser:obj[@"latest_users"] withMaps:maps];
        }
        [mapsListArray addObject:maps];
    }
    [[MapsModelManage sharedInstance] saveContext];
    return mapsListArray;
}
+ (Maps *)getMapsWithId:(NSInteger)mapsId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"id = %i",mapsId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (NSArray<Maps*>*)getMapsList{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate=  [NSPredicate predicateWithFormat:@"isMyMaps = %@",[NSNumber numberWithBool:NO]];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort]];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;
}
+ (NSArray<Maps*>*)getMyMapsList{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate=  [NSPredicate predicateWithFormat:@"isMyMaps = %@",[NSNumber numberWithBool:YES]];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort]];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;
}
+ (void)clearCache{
    for(NSManagedObject *obj in [self getMapsList]){
        [[MapsModelManage sharedInstance].context deleteObject:obj];
    }
}
+ (void)clearMyMapsCache{
    for(NSManagedObject *obj in [self getMyMapsList]){
        [[MapsModelManage sharedInstance].context deleteObject:obj];
    }
}
// Custom logic goes here.

@end
