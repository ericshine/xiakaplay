#import "_UserPost.h"
#import "MapsModelManage.h"
@interface UserPost : _UserPost {}
// Custom logic goes here.
+ (NSMutableArray *)savePostData:(NSArray *)postList;
+ (NSArray *)getAllPost;
+ (void)clearCache;
@end
