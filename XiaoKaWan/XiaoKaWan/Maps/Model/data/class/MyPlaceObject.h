#import "_MyPlaceObject.h"

@interface MyPlaceObject : _MyPlaceObject {}
+ (MyPlaceObject *)savePlaceWithDic:(NSDictionary *)dic;
+ (NSMutableArray *)savePlaceWantObjcets:(NSArray *)obects;
+ (NSMutableArray *)savePlaceBeenObjcets:(NSArray *)obects;
+ (NSArray<MyPlaceObject*> *)getPlaceBeen;
+ (NSArray<MyPlaceObject*> *)getPlaceWant;
+ (void)clearMyBeenCache;
+ (void)clearMyWantCache;
@end
