// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Maps.h instead.

#import <CoreData/CoreData.h>

extern const struct MapsAttributes {
	__unsafe_unretained NSString *createUserid;
	__unsafe_unretained NSString *descriptions;
	__unsafe_unretained NSString *follow_count;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *isMyMaps;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *photos;
	__unsafe_unretained NSString *place_count;
	__unsafe_unretained NSString *post_count;
	__unsafe_unretained NSString *read_count;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *user_count;
} MapsAttributes;

extern const struct MapsRelationships {
	__unsafe_unretained NSString *mapsUser;
} MapsRelationships;

@class MapsUser;

@interface MapsID : NSManagedObjectID {}
@end

@interface _Maps : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MapsID* objectID;

@property (nonatomic, strong) NSNumber* createUserid;

@property (atomic) int32_t createUseridValue;
- (int32_t)createUseridValue;
- (void)setCreateUseridValue:(int32_t)value_;

//- (BOOL)validateCreateUserid:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* descriptions;

//- (BOOL)validateDescriptions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* follow_count;

@property (atomic) int32_t follow_countValue;
- (int32_t)follow_countValue;
- (void)setFollow_countValue:(int32_t)value_;

//- (BOOL)validateFollow_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isMyMaps;

@property (atomic) BOOL isMyMapsValue;
- (BOOL)isMyMapsValue;
- (void)setIsMyMapsValue:(BOOL)value_;

//- (BOOL)validateIsMyMaps:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* photos;

//- (BOOL)validatePhotos:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* place_count;

@property (atomic) int32_t place_countValue;
- (int32_t)place_countValue;
- (void)setPlace_countValue:(int32_t)value_;

//- (BOOL)validatePlace_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* post_count;

@property (atomic) int32_t post_countValue;
- (int32_t)post_countValue;
- (void)setPost_countValue:(int32_t)value_;

//- (BOOL)validatePost_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* read_count;

@property (atomic) int32_t read_countValue;
- (int32_t)read_countValue;
- (void)setRead_countValue:(int32_t)value_;

//- (BOOL)validateRead_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) int32_t statusValue;
- (int32_t)statusValue;
- (void)setStatusValue:(int32_t)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* user_count;

@property (atomic) int32_t user_countValue;
- (int32_t)user_countValue;
- (void)setUser_countValue:(int32_t)value_;

//- (BOOL)validateUser_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *mapsUser;

- (NSMutableSet*)mapsUserSet;

@end

@interface _Maps (MapsUserCoreDataGeneratedAccessors)
- (void)addMapsUser:(NSSet*)value_;
- (void)removeMapsUser:(NSSet*)value_;
- (void)addMapsUserObject:(MapsUser*)value_;
- (void)removeMapsUserObject:(MapsUser*)value_;

@end

@interface _Maps (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCreateUserid;
- (void)setPrimitiveCreateUserid:(NSNumber*)value;

- (int32_t)primitiveCreateUseridValue;
- (void)setPrimitiveCreateUseridValue:(int32_t)value_;

- (NSString*)primitiveDescriptions;
- (void)setPrimitiveDescriptions:(NSString*)value;

- (NSNumber*)primitiveFollow_count;
- (void)setPrimitiveFollow_count:(NSNumber*)value;

- (int32_t)primitiveFollow_countValue;
- (void)setPrimitiveFollow_countValue:(int32_t)value_;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsMyMaps;
- (void)setPrimitiveIsMyMaps:(NSNumber*)value;

- (BOOL)primitiveIsMyMapsValue;
- (void)setPrimitiveIsMyMapsValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSData*)primitivePhotos;
- (void)setPrimitivePhotos:(NSData*)value;

- (NSNumber*)primitivePlace_count;
- (void)setPrimitivePlace_count:(NSNumber*)value;

- (int32_t)primitivePlace_countValue;
- (void)setPrimitivePlace_countValue:(int32_t)value_;

- (NSNumber*)primitivePost_count;
- (void)setPrimitivePost_count:(NSNumber*)value;

- (int32_t)primitivePost_countValue;
- (void)setPrimitivePost_countValue:(int32_t)value_;

- (NSNumber*)primitiveRead_count;
- (void)setPrimitiveRead_count:(NSNumber*)value;

- (int32_t)primitiveRead_countValue;
- (void)setPrimitiveRead_countValue:(int32_t)value_;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (int32_t)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(int32_t)value_;

- (NSNumber*)primitiveUser_count;
- (void)setPrimitiveUser_count:(NSNumber*)value;

- (int32_t)primitiveUser_countValue;
- (void)setPrimitiveUser_countValue:(int32_t)value_;

- (NSMutableSet*)primitiveMapsUser;
- (void)setPrimitiveMapsUser:(NSMutableSet*)value;

@end
