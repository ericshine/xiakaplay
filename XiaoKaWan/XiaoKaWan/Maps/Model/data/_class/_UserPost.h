// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserPost.h instead.

#import <CoreData/CoreData.h>

extern const struct UserPostAttributes {
	__unsafe_unretained NSString *action_date;
	__unsafe_unretained NSString *comment_count;
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *gmt_created;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *image_count;
	__unsafe_unretained NSString *images;
	__unsafe_unretained NSString *like_count;
	__unsafe_unretained NSString *maps_id;
	__unsafe_unretained NSString *place_id;
	__unsafe_unretained NSString *user_id;
} UserPostAttributes;

extern const struct UserPostRelationships {
	__unsafe_unretained NSString *place;
	__unsafe_unretained NSString *postUser;
} UserPostRelationships;

@class MyPlaceObject;
@class MapsUser;

@interface UserPostID : NSManagedObjectID {}
@end

@interface _UserPost : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserPostID* objectID;

@property (nonatomic, strong) NSString* action_date;

//- (BOOL)validateAction_date:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* comment_count;

@property (atomic) int32_t comment_countValue;
- (int32_t)comment_countValue;
- (void)setComment_countValue:(int32_t)value_;

//- (BOOL)validateComment_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* content;

//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* gmt_created;

//- (BOOL)validateGmt_created:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* image_count;

@property (atomic) int32_t image_countValue;
- (int32_t)image_countValue;
- (void)setImage_countValue:(int32_t)value_;

//- (BOOL)validateImage_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* images;

//- (BOOL)validateImages:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* like_count;

@property (atomic) int32_t like_countValue;
- (int32_t)like_countValue;
- (void)setLike_countValue:(int32_t)value_;

//- (BOOL)validateLike_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maps_id;

@property (atomic) int32_t maps_idValue;
- (int32_t)maps_idValue;
- (void)setMaps_idValue:(int32_t)value_;

//- (BOOL)validateMaps_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* place_id;

@property (atomic) int32_t place_idValue;
- (int32_t)place_idValue;
- (void)setPlace_idValue:(int32_t)value_;

//- (BOOL)validatePlace_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* user_id;

@property (atomic) int32_t user_idValue;
- (int32_t)user_idValue;
- (void)setUser_idValue:(int32_t)value_;

//- (BOOL)validateUser_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MyPlaceObject *place;

//- (BOOL)validatePlace:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MapsUser *postUser;

//- (BOOL)validatePostUser:(id*)value_ error:(NSError**)error_;

@end

@interface _UserPost (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAction_date;
- (void)setPrimitiveAction_date:(NSString*)value;

- (NSNumber*)primitiveComment_count;
- (void)setPrimitiveComment_count:(NSNumber*)value;

- (int32_t)primitiveComment_countValue;
- (void)setPrimitiveComment_countValue:(int32_t)value_;

- (NSString*)primitiveContent;
- (void)setPrimitiveContent:(NSString*)value;

- (NSString*)primitiveGmt_created;
- (void)setPrimitiveGmt_created:(NSString*)value;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveImage_count;
- (void)setPrimitiveImage_count:(NSNumber*)value;

- (int32_t)primitiveImage_countValue;
- (void)setPrimitiveImage_countValue:(int32_t)value_;

- (NSData*)primitiveImages;
- (void)setPrimitiveImages:(NSData*)value;

- (NSNumber*)primitiveLike_count;
- (void)setPrimitiveLike_count:(NSNumber*)value;

- (int32_t)primitiveLike_countValue;
- (void)setPrimitiveLike_countValue:(int32_t)value_;

- (NSNumber*)primitiveMaps_id;
- (void)setPrimitiveMaps_id:(NSNumber*)value;

- (int32_t)primitiveMaps_idValue;
- (void)setPrimitiveMaps_idValue:(int32_t)value_;

- (NSNumber*)primitivePlace_id;
- (void)setPrimitivePlace_id:(NSNumber*)value;

- (int32_t)primitivePlace_idValue;
- (void)setPrimitivePlace_idValue:(int32_t)value_;

- (NSNumber*)primitiveUser_id;
- (void)setPrimitiveUser_id:(NSNumber*)value;

- (int32_t)primitiveUser_idValue;
- (void)setPrimitiveUser_idValue:(int32_t)value_;

- (MyPlaceObject*)primitivePlace;
- (void)setPrimitivePlace:(MyPlaceObject*)value;

- (MapsUser*)primitivePostUser;
- (void)setPrimitivePostUser:(MapsUser*)value;

@end
