// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserPost.m instead.

#import "_UserPost.h"

const struct UserPostAttributes UserPostAttributes = {
	.action_date = @"action_date",
	.comment_count = @"comment_count",
	.content = @"content",
	.gmt_created = @"gmt_created",
	.id = @"id",
	.image_count = @"image_count",
	.images = @"images",
	.like_count = @"like_count",
	.maps_id = @"maps_id",
	.place_id = @"place_id",
	.user_id = @"user_id",
};

const struct UserPostRelationships UserPostRelationships = {
	.place = @"place",
	.postUser = @"postUser",
};

@implementation UserPostID
@end

@implementation _UserPost

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserPost" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserPost";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserPost" inManagedObjectContext:moc_];
}

- (UserPostID*)objectID {
	return (UserPostID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"comment_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"comment_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"image_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"image_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"like_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"like_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maps_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maps_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"place_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"place_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"user_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"user_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic action_date;

@dynamic comment_count;

- (int32_t)comment_countValue {
	NSNumber *result = [self comment_count];
	return [result intValue];
}

- (void)setComment_countValue:(int32_t)value_ {
	[self setComment_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveComment_countValue {
	NSNumber *result = [self primitiveComment_count];
	return [result intValue];
}

- (void)setPrimitiveComment_countValue:(int32_t)value_ {
	[self setPrimitiveComment_count:[NSNumber numberWithInt:value_]];
}

@dynamic content;

@dynamic gmt_created;

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic image_count;

- (int32_t)image_countValue {
	NSNumber *result = [self image_count];
	return [result intValue];
}

- (void)setImage_countValue:(int32_t)value_ {
	[self setImage_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImage_countValue {
	NSNumber *result = [self primitiveImage_count];
	return [result intValue];
}

- (void)setPrimitiveImage_countValue:(int32_t)value_ {
	[self setPrimitiveImage_count:[NSNumber numberWithInt:value_]];
}

@dynamic images;

@dynamic like_count;

- (int32_t)like_countValue {
	NSNumber *result = [self like_count];
	return [result intValue];
}

- (void)setLike_countValue:(int32_t)value_ {
	[self setLike_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLike_countValue {
	NSNumber *result = [self primitiveLike_count];
	return [result intValue];
}

- (void)setPrimitiveLike_countValue:(int32_t)value_ {
	[self setPrimitiveLike_count:[NSNumber numberWithInt:value_]];
}

@dynamic maps_id;

- (int32_t)maps_idValue {
	NSNumber *result = [self maps_id];
	return [result intValue];
}

- (void)setMaps_idValue:(int32_t)value_ {
	[self setMaps_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMaps_idValue {
	NSNumber *result = [self primitiveMaps_id];
	return [result intValue];
}

- (void)setPrimitiveMaps_idValue:(int32_t)value_ {
	[self setPrimitiveMaps_id:[NSNumber numberWithInt:value_]];
}

@dynamic place_id;

- (int32_t)place_idValue {
	NSNumber *result = [self place_id];
	return [result intValue];
}

- (void)setPlace_idValue:(int32_t)value_ {
	[self setPlace_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePlace_idValue {
	NSNumber *result = [self primitivePlace_id];
	return [result intValue];
}

- (void)setPrimitivePlace_idValue:(int32_t)value_ {
	[self setPrimitivePlace_id:[NSNumber numberWithInt:value_]];
}

@dynamic user_id;

- (int32_t)user_idValue {
	NSNumber *result = [self user_id];
	return [result intValue];
}

- (void)setUser_idValue:(int32_t)value_ {
	[self setUser_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUser_idValue {
	NSNumber *result = [self primitiveUser_id];
	return [result intValue];
}

- (void)setPrimitiveUser_idValue:(int32_t)value_ {
	[self setPrimitiveUser_id:[NSNumber numberWithInt:value_]];
}

@dynamic place;

@dynamic postUser;

@end

