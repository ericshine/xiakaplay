// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MapsUser.m instead.

#import "_MapsUser.h"

const struct MapsUserAttributes MapsUserAttributes = {
	.avatar = @"avatar",
	.beenPlaceId = @"beenPlaceId",
	.mapsId = @"mapsId",
	.nick = @"nick",
	.uid = @"uid",
	.wantPlaceId = @"wantPlaceId",
};

const struct MapsUserRelationships MapsUserRelationships = {
	.placeBeen = @"placeBeen",
	.placeWant = @"placeWant",
	.userMaps = @"userMaps",
	.userPost = @"userPost",
};

@implementation MapsUserID
@end

@implementation _MapsUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MapsUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MapsUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MapsUser" inManagedObjectContext:moc_];
}

- (MapsUserID*)objectID {
	return (MapsUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"beenPlaceIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"beenPlaceId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mapsIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mapsId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"uidValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"uid"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wantPlaceIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wantPlaceId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatar;

@dynamic beenPlaceId;

- (int32_t)beenPlaceIdValue {
	NSNumber *result = [self beenPlaceId];
	return [result intValue];
}

- (void)setBeenPlaceIdValue:(int32_t)value_ {
	[self setBeenPlaceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBeenPlaceIdValue {
	NSNumber *result = [self primitiveBeenPlaceId];
	return [result intValue];
}

- (void)setPrimitiveBeenPlaceIdValue:(int32_t)value_ {
	[self setPrimitiveBeenPlaceId:[NSNumber numberWithInt:value_]];
}

@dynamic mapsId;

- (int32_t)mapsIdValue {
	NSNumber *result = [self mapsId];
	return [result intValue];
}

- (void)setMapsIdValue:(int32_t)value_ {
	[self setMapsId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMapsIdValue {
	NSNumber *result = [self primitiveMapsId];
	return [result intValue];
}

- (void)setPrimitiveMapsIdValue:(int32_t)value_ {
	[self setPrimitiveMapsId:[NSNumber numberWithInt:value_]];
}

@dynamic nick;

@dynamic uid;

- (int32_t)uidValue {
	NSNumber *result = [self uid];
	return [result intValue];
}

- (void)setUidValue:(int32_t)value_ {
	[self setUid:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUidValue {
	NSNumber *result = [self primitiveUid];
	return [result intValue];
}

- (void)setPrimitiveUidValue:(int32_t)value_ {
	[self setPrimitiveUid:[NSNumber numberWithInt:value_]];
}

@dynamic wantPlaceId;

- (int32_t)wantPlaceIdValue {
	NSNumber *result = [self wantPlaceId];
	return [result intValue];
}

- (void)setWantPlaceIdValue:(int32_t)value_ {
	[self setWantPlaceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveWantPlaceIdValue {
	NSNumber *result = [self primitiveWantPlaceId];
	return [result intValue];
}

- (void)setPrimitiveWantPlaceIdValue:(int32_t)value_ {
	[self setPrimitiveWantPlaceId:[NSNumber numberWithInt:value_]];
}

@dynamic placeBeen;

@dynamic placeWant;

@dynamic userMaps;

@dynamic userPost;

@end

