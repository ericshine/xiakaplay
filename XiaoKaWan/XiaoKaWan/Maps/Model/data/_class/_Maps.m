// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Maps.m instead.

#import "_Maps.h"

const struct MapsAttributes MapsAttributes = {
	.createUserid = @"createUserid",
	.descriptions = @"descriptions",
	.follow_count = @"follow_count",
	.id = @"id",
	.isMyMaps = @"isMyMaps",
	.name = @"name",
	.photos = @"photos",
	.place_count = @"place_count",
	.post_count = @"post_count",
	.read_count = @"read_count",
	.status = @"status",
	.user_count = @"user_count",
};

const struct MapsRelationships MapsRelationships = {
	.mapsUser = @"mapsUser",
};

@implementation MapsID
@end

@implementation _Maps

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Maps" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Maps";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Maps" inManagedObjectContext:moc_];
}

- (MapsID*)objectID {
	return (MapsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"createUseridValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createUserid"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"follow_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"follow_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isMyMapsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isMyMaps"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"place_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"place_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"post_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"post_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"read_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"read_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"user_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"user_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createUserid;

- (int32_t)createUseridValue {
	NSNumber *result = [self createUserid];
	return [result intValue];
}

- (void)setCreateUseridValue:(int32_t)value_ {
	[self setCreateUserid:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreateUseridValue {
	NSNumber *result = [self primitiveCreateUserid];
	return [result intValue];
}

- (void)setPrimitiveCreateUseridValue:(int32_t)value_ {
	[self setPrimitiveCreateUserid:[NSNumber numberWithInt:value_]];
}

@dynamic descriptions;

@dynamic follow_count;

- (int32_t)follow_countValue {
	NSNumber *result = [self follow_count];
	return [result intValue];
}

- (void)setFollow_countValue:(int32_t)value_ {
	[self setFollow_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFollow_countValue {
	NSNumber *result = [self primitiveFollow_count];
	return [result intValue];
}

- (void)setPrimitiveFollow_countValue:(int32_t)value_ {
	[self setPrimitiveFollow_count:[NSNumber numberWithInt:value_]];
}

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic isMyMaps;

- (BOOL)isMyMapsValue {
	NSNumber *result = [self isMyMaps];
	return [result boolValue];
}

- (void)setIsMyMapsValue:(BOOL)value_ {
	[self setIsMyMaps:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsMyMapsValue {
	NSNumber *result = [self primitiveIsMyMaps];
	return [result boolValue];
}

- (void)setPrimitiveIsMyMapsValue:(BOOL)value_ {
	[self setPrimitiveIsMyMaps:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic photos;

@dynamic place_count;

- (int32_t)place_countValue {
	NSNumber *result = [self place_count];
	return [result intValue];
}

- (void)setPlace_countValue:(int32_t)value_ {
	[self setPlace_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePlace_countValue {
	NSNumber *result = [self primitivePlace_count];
	return [result intValue];
}

- (void)setPrimitivePlace_countValue:(int32_t)value_ {
	[self setPrimitivePlace_count:[NSNumber numberWithInt:value_]];
}

@dynamic post_count;

- (int32_t)post_countValue {
	NSNumber *result = [self post_count];
	return [result intValue];
}

- (void)setPost_countValue:(int32_t)value_ {
	[self setPost_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePost_countValue {
	NSNumber *result = [self primitivePost_count];
	return [result intValue];
}

- (void)setPrimitivePost_countValue:(int32_t)value_ {
	[self setPrimitivePost_count:[NSNumber numberWithInt:value_]];
}

@dynamic read_count;

- (int32_t)read_countValue {
	NSNumber *result = [self read_count];
	return [result intValue];
}

- (void)setRead_countValue:(int32_t)value_ {
	[self setRead_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRead_countValue {
	NSNumber *result = [self primitiveRead_count];
	return [result intValue];
}

- (void)setPrimitiveRead_countValue:(int32_t)value_ {
	[self setPrimitiveRead_count:[NSNumber numberWithInt:value_]];
}

@dynamic status;

- (int32_t)statusValue {
	NSNumber *result = [self status];
	return [result intValue];
}

- (void)setStatusValue:(int32_t)value_ {
	[self setStatus:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result intValue];
}

- (void)setPrimitiveStatusValue:(int32_t)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithInt:value_]];
}

@dynamic user_count;

- (int32_t)user_countValue {
	NSNumber *result = [self user_count];
	return [result intValue];
}

- (void)setUser_countValue:(int32_t)value_ {
	[self setUser_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUser_countValue {
	NSNumber *result = [self primitiveUser_count];
	return [result intValue];
}

- (void)setPrimitiveUser_countValue:(int32_t)value_ {
	[self setPrimitiveUser_count:[NSNumber numberWithInt:value_]];
}

@dynamic mapsUser;

- (NSMutableSet*)mapsUserSet {
	[self willAccessValueForKey:@"mapsUser"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mapsUser"];

	[self didAccessValueForKey:@"mapsUser"];
	return result;
}

@end

