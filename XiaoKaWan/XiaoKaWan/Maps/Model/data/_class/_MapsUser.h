// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MapsUser.h instead.

#import <CoreData/CoreData.h>

extern const struct MapsUserAttributes {
	__unsafe_unretained NSString *avatar;
	__unsafe_unretained NSString *beenPlaceId;
	__unsafe_unretained NSString *mapsId;
	__unsafe_unretained NSString *nick;
	__unsafe_unretained NSString *uid;
	__unsafe_unretained NSString *wantPlaceId;
} MapsUserAttributes;

extern const struct MapsUserRelationships {
	__unsafe_unretained NSString *placeBeen;
	__unsafe_unretained NSString *placeWant;
	__unsafe_unretained NSString *userMaps;
	__unsafe_unretained NSString *userPost;
} MapsUserRelationships;

@class MyPlaceObject;
@class MyPlaceObject;
@class Maps;
@class UserPost;

@interface MapsUserID : NSManagedObjectID {}
@end

@interface _MapsUser : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MapsUserID* objectID;

@property (nonatomic, strong) NSString* avatar;

//- (BOOL)validateAvatar:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* beenPlaceId;

@property (atomic) int32_t beenPlaceIdValue;
- (int32_t)beenPlaceIdValue;
- (void)setBeenPlaceIdValue:(int32_t)value_;

//- (BOOL)validateBeenPlaceId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mapsId;

@property (atomic) int32_t mapsIdValue;
- (int32_t)mapsIdValue;
- (void)setMapsIdValue:(int32_t)value_;

//- (BOOL)validateMapsId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nick;

//- (BOOL)validateNick:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* uid;

@property (atomic) int32_t uidValue;
- (int32_t)uidValue;
- (void)setUidValue:(int32_t)value_;

//- (BOOL)validateUid:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wantPlaceId;

@property (atomic) int32_t wantPlaceIdValue;
- (int32_t)wantPlaceIdValue;
- (void)setWantPlaceIdValue:(int32_t)value_;

//- (BOOL)validateWantPlaceId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MyPlaceObject *placeBeen;

//- (BOOL)validatePlaceBeen:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MyPlaceObject *placeWant;

//- (BOOL)validatePlaceWant:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Maps *userMaps;

//- (BOOL)validateUserMaps:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserPost *userPost;

//- (BOOL)validateUserPost:(id*)value_ error:(NSError**)error_;

@end

@interface _MapsUser (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAvatar;
- (void)setPrimitiveAvatar:(NSString*)value;

- (NSNumber*)primitiveBeenPlaceId;
- (void)setPrimitiveBeenPlaceId:(NSNumber*)value;

- (int32_t)primitiveBeenPlaceIdValue;
- (void)setPrimitiveBeenPlaceIdValue:(int32_t)value_;

- (NSNumber*)primitiveMapsId;
- (void)setPrimitiveMapsId:(NSNumber*)value;

- (int32_t)primitiveMapsIdValue;
- (void)setPrimitiveMapsIdValue:(int32_t)value_;

- (NSString*)primitiveNick;
- (void)setPrimitiveNick:(NSString*)value;

- (NSNumber*)primitiveUid;
- (void)setPrimitiveUid:(NSNumber*)value;

- (int32_t)primitiveUidValue;
- (void)setPrimitiveUidValue:(int32_t)value_;

- (NSNumber*)primitiveWantPlaceId;
- (void)setPrimitiveWantPlaceId:(NSNumber*)value;

- (int32_t)primitiveWantPlaceIdValue;
- (void)setPrimitiveWantPlaceIdValue:(int32_t)value_;

- (MyPlaceObject*)primitivePlaceBeen;
- (void)setPrimitivePlaceBeen:(MyPlaceObject*)value;

- (MyPlaceObject*)primitivePlaceWant;
- (void)setPrimitivePlaceWant:(MyPlaceObject*)value;

- (Maps*)primitiveUserMaps;
- (void)setPrimitiveUserMaps:(Maps*)value;

- (UserPost*)primitiveUserPost;
- (void)setPrimitiveUserPost:(UserPost*)value;

@end
