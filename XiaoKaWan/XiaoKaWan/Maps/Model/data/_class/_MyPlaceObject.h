// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MyPlaceObject.h instead.

#import <CoreData/CoreData.h>

extern const struct MyPlaceObjectAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *been_count;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *descriptions;
	__unsafe_unretained NSString *district;
	__unsafe_unretained NSString *gmt_created;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *isBeen;
	__unsafe_unretained NSString *isWant;
	__unsafe_unretained NSString *latest_image;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longtitude;
	__unsafe_unretained NSString *maps_count;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *placeId;
	__unsafe_unretained NSString *province;
	__unsafe_unretained NSString *want_count;
} MyPlaceObjectAttributes;

extern const struct MyPlaceObjectRelationships {
	__unsafe_unretained NSString *beenUser;
	__unsafe_unretained NSString *post;
	__unsafe_unretained NSString *wantUser;
} MyPlaceObjectRelationships;

@class MapsUser;
@class UserPost;
@class MapsUser;

@interface MyPlaceObjectID : NSManagedObjectID {}
@end

@interface _MyPlaceObject : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MyPlaceObjectID* objectID;

@property (nonatomic, strong) NSString* address;

//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* been_count;

@property (atomic) int32_t been_countValue;
- (int32_t)been_countValue;
- (void)setBeen_countValue:(int32_t)value_;

//- (BOOL)validateBeen_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* descriptions;

//- (BOOL)validateDescriptions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* district;

//- (BOOL)validateDistrict:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* gmt_created;

//- (BOOL)validateGmt_created:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isBeen;

@property (atomic) BOOL isBeenValue;
- (BOOL)isBeenValue;
- (void)setIsBeenValue:(BOOL)value_;

//- (BOOL)validateIsBeen:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isWant;

@property (atomic) BOOL isWantValue;
- (BOOL)isWantValue;
- (void)setIsWantValue:(BOOL)value_;

//- (BOOL)validateIsWant:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* latest_image;

//- (BOOL)validateLatest_image:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* latitude;

@property (atomic) int32_t latitudeValue;
- (int32_t)latitudeValue;
- (void)setLatitudeValue:(int32_t)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longtitude;

@property (atomic) int32_t longtitudeValue;
- (int32_t)longtitudeValue;
- (void)setLongtitudeValue:(int32_t)value_;

//- (BOOL)validateLongtitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maps_count;

@property (atomic) int32_t maps_countValue;
- (int32_t)maps_countValue;
- (void)setMaps_countValue:(int32_t)value_;

//- (BOOL)validateMaps_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* placeId;

@property (atomic) int32_t placeIdValue;
- (int32_t)placeIdValue;
- (void)setPlaceIdValue:(int32_t)value_;

//- (BOOL)validatePlaceId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* province;

//- (BOOL)validateProvince:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* want_count;

@property (atomic) int32_t want_countValue;
- (int32_t)want_countValue;
- (void)setWant_countValue:(int32_t)value_;

//- (BOOL)validateWant_count:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *beenUser;

- (NSMutableSet*)beenUserSet;

@property (nonatomic, strong) UserPost *post;

//- (BOOL)validatePost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *wantUser;

- (NSMutableSet*)wantUserSet;

@end

@interface _MyPlaceObject (BeenUserCoreDataGeneratedAccessors)
- (void)addBeenUser:(NSSet*)value_;
- (void)removeBeenUser:(NSSet*)value_;
- (void)addBeenUserObject:(MapsUser*)value_;
- (void)removeBeenUserObject:(MapsUser*)value_;

@end

@interface _MyPlaceObject (WantUserCoreDataGeneratedAccessors)
- (void)addWantUser:(NSSet*)value_;
- (void)removeWantUser:(NSSet*)value_;
- (void)addWantUserObject:(MapsUser*)value_;
- (void)removeWantUserObject:(MapsUser*)value_;

@end

@interface _MyPlaceObject (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSNumber*)primitiveBeen_count;
- (void)setPrimitiveBeen_count:(NSNumber*)value;

- (int32_t)primitiveBeen_countValue;
- (void)setPrimitiveBeen_countValue:(int32_t)value_;

- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;

- (NSString*)primitiveDescriptions;
- (void)setPrimitiveDescriptions:(NSString*)value;

- (NSString*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSString*)value;

- (NSString*)primitiveGmt_created;
- (void)setPrimitiveGmt_created:(NSString*)value;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsBeen;
- (void)setPrimitiveIsBeen:(NSNumber*)value;

- (BOOL)primitiveIsBeenValue;
- (void)setPrimitiveIsBeenValue:(BOOL)value_;

- (NSNumber*)primitiveIsWant;
- (void)setPrimitiveIsWant:(NSNumber*)value;

- (BOOL)primitiveIsWantValue;
- (void)setPrimitiveIsWantValue:(BOOL)value_;

- (NSData*)primitiveLatest_image;
- (void)setPrimitiveLatest_image:(NSData*)value;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (int32_t)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(int32_t)value_;

- (NSNumber*)primitiveLongtitude;
- (void)setPrimitiveLongtitude:(NSNumber*)value;

- (int32_t)primitiveLongtitudeValue;
- (void)setPrimitiveLongtitudeValue:(int32_t)value_;

- (NSNumber*)primitiveMaps_count;
- (void)setPrimitiveMaps_count:(NSNumber*)value;

- (int32_t)primitiveMaps_countValue;
- (void)setPrimitiveMaps_countValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePlaceId;
- (void)setPrimitivePlaceId:(NSNumber*)value;

- (int32_t)primitivePlaceIdValue;
- (void)setPrimitivePlaceIdValue:(int32_t)value_;

- (NSString*)primitiveProvince;
- (void)setPrimitiveProvince:(NSString*)value;

- (NSNumber*)primitiveWant_count;
- (void)setPrimitiveWant_count:(NSNumber*)value;

- (int32_t)primitiveWant_countValue;
- (void)setPrimitiveWant_countValue:(int32_t)value_;

- (NSMutableSet*)primitiveBeenUser;
- (void)setPrimitiveBeenUser:(NSMutableSet*)value;

- (UserPost*)primitivePost;
- (void)setPrimitivePost:(UserPost*)value;

- (NSMutableSet*)primitiveWantUser;
- (void)setPrimitiveWantUser:(NSMutableSet*)value;

@end
