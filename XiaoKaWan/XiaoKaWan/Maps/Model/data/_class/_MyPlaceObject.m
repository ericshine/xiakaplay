// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MyPlaceObject.m instead.

#import "_MyPlaceObject.h"

const struct MyPlaceObjectAttributes MyPlaceObjectAttributes = {
	.address = @"address",
	.been_count = @"been_count",
	.city = @"city",
	.descriptions = @"descriptions",
	.district = @"district",
	.gmt_created = @"gmt_created",
	.id = @"id",
	.isBeen = @"isBeen",
	.isWant = @"isWant",
	.latest_image = @"latest_image",
	.latitude = @"latitude",
	.longtitude = @"longtitude",
	.maps_count = @"maps_count",
	.name = @"name",
	.placeId = @"placeId",
	.province = @"province",
	.want_count = @"want_count",
};

const struct MyPlaceObjectRelationships MyPlaceObjectRelationships = {
	.beenUser = @"beenUser",
	.post = @"post",
	.wantUser = @"wantUser",
};

@implementation MyPlaceObjectID
@end

@implementation _MyPlaceObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MyPlaceObject" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MyPlaceObject";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MyPlaceObject" inManagedObjectContext:moc_];
}

- (MyPlaceObjectID*)objectID {
	return (MyPlaceObjectID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"been_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"been_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isBeenValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isBeen"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isWantValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isWant"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longtitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longtitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maps_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maps_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"placeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"placeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"want_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"want_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic been_count;

- (int32_t)been_countValue {
	NSNumber *result = [self been_count];
	return [result intValue];
}

- (void)setBeen_countValue:(int32_t)value_ {
	[self setBeen_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBeen_countValue {
	NSNumber *result = [self primitiveBeen_count];
	return [result intValue];
}

- (void)setPrimitiveBeen_countValue:(int32_t)value_ {
	[self setPrimitiveBeen_count:[NSNumber numberWithInt:value_]];
}

@dynamic city;

@dynamic descriptions;

@dynamic district;

@dynamic gmt_created;

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic isBeen;

- (BOOL)isBeenValue {
	NSNumber *result = [self isBeen];
	return [result boolValue];
}

- (void)setIsBeenValue:(BOOL)value_ {
	[self setIsBeen:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsBeenValue {
	NSNumber *result = [self primitiveIsBeen];
	return [result boolValue];
}

- (void)setPrimitiveIsBeenValue:(BOOL)value_ {
	[self setPrimitiveIsBeen:[NSNumber numberWithBool:value_]];
}

@dynamic isWant;

- (BOOL)isWantValue {
	NSNumber *result = [self isWant];
	return [result boolValue];
}

- (void)setIsWantValue:(BOOL)value_ {
	[self setIsWant:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsWantValue {
	NSNumber *result = [self primitiveIsWant];
	return [result boolValue];
}

- (void)setPrimitiveIsWantValue:(BOOL)value_ {
	[self setPrimitiveIsWant:[NSNumber numberWithBool:value_]];
}

@dynamic latest_image;

@dynamic latitude;

- (int32_t)latitudeValue {
	NSNumber *result = [self latitude];
	return [result intValue];
}

- (void)setLatitudeValue:(int32_t)value_ {
	[self setLatitude:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result intValue];
}

- (void)setPrimitiveLatitudeValue:(int32_t)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithInt:value_]];
}

@dynamic longtitude;

- (int32_t)longtitudeValue {
	NSNumber *result = [self longtitude];
	return [result intValue];
}

- (void)setLongtitudeValue:(int32_t)value_ {
	[self setLongtitude:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLongtitudeValue {
	NSNumber *result = [self primitiveLongtitude];
	return [result intValue];
}

- (void)setPrimitiveLongtitudeValue:(int32_t)value_ {
	[self setPrimitiveLongtitude:[NSNumber numberWithInt:value_]];
}

@dynamic maps_count;

- (int32_t)maps_countValue {
	NSNumber *result = [self maps_count];
	return [result intValue];
}

- (void)setMaps_countValue:(int32_t)value_ {
	[self setMaps_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMaps_countValue {
	NSNumber *result = [self primitiveMaps_count];
	return [result intValue];
}

- (void)setPrimitiveMaps_countValue:(int32_t)value_ {
	[self setPrimitiveMaps_count:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic placeId;

- (int32_t)placeIdValue {
	NSNumber *result = [self placeId];
	return [result intValue];
}

- (void)setPlaceIdValue:(int32_t)value_ {
	[self setPlaceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePlaceIdValue {
	NSNumber *result = [self primitivePlaceId];
	return [result intValue];
}

- (void)setPrimitivePlaceIdValue:(int32_t)value_ {
	[self setPrimitivePlaceId:[NSNumber numberWithInt:value_]];
}

@dynamic province;

@dynamic want_count;

- (int32_t)want_countValue {
	NSNumber *result = [self want_count];
	return [result intValue];
}

- (void)setWant_countValue:(int32_t)value_ {
	[self setWant_count:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveWant_countValue {
	NSNumber *result = [self primitiveWant_count];
	return [result intValue];
}

- (void)setPrimitiveWant_countValue:(int32_t)value_ {
	[self setPrimitiveWant_count:[NSNumber numberWithInt:value_]];
}

@dynamic beenUser;

- (NSMutableSet*)beenUserSet {
	[self willAccessValueForKey:@"beenUser"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"beenUser"];

	[self didAccessValueForKey:@"beenUser"];
	return result;
}

@dynamic post;

@dynamic wantUser;

- (NSMutableSet*)wantUserSet {
	[self willAccessValueForKey:@"wantUser"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"wantUser"];

	[self didAccessValueForKey:@"wantUser"];
	return result;
}

@end

