//
//  Place.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "Place.h"
#import "User.h"
#import <MJExtension.h>
#import "PostBMKPointAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "PostObjet.h"
#import "MapsUser.h"
@implementation Place
- (instancetype)init{
    self = [super init];
    if(self){
        [Place setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"beenUsers":[User class],
                     @"wantUsers":[User class],
                     @"latestImages":[BJImageObject class]
                     };
        }];
        [Place setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"isWant":@"login_user_want",
                     @"isBeen":@"login_user_been",
                     @"placeId":@"place_id",
                     @"beenUsers":@"been_users",
                     @"wantUsers":@"want_users",
                     @"beenCount":@"been_count",
                     @"wantCount":@"want_count",
                     @"addReason":@"add_reason",
                     @"addUserId":@"add_user_id",
                     @"latestImages":@"latest_image",
                     @"postCount":@"post_count",
                     @"mapsCount":@"maps_count",
                     @"content":@"description"
                     };
        }];
    }
    return self;
}
- (NSString *)addressDetail{
    if(self.address.length) return self.address;
   else return [NSString stringWithFormat:@"%@%@%@",self.province,self.city,self.district];
}
+ (NSMutableArray *)dicToObject:(NSArray *)array{
    NSMutableArray *objects = [[NSMutableArray alloc] initWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Place *place = [Place objectWithKeyValues:obj];
        if(place.placeId == nil) place.placeId = [NSString stringWithFormat:@"%li",(long)place.id];
        [objects addObject:place];
    }];
    return objects;
}
- (PostBMKPointAnnotation*)placeToAnnotation{
    CLLocationCoordinate2D coorinate;
    coorinate.latitude = self.latitude;
    coorinate.longitude = self.longtitude;
    PostBMKPointAnnotation *annotation = [[PostBMKPointAnnotation alloc] init];
    annotation.coordinate = coorinate;
    annotation.postContent = self.last_post.content;
    BJImageObject *imageObj = self.last_post.latestPostFristImage;
    annotation.imageUrl = imageObj.url;
    annotation.headImageUrl = self.last_post.post_user.avatar;
    annotation.place = self;
    if(self.last_post) annotation.isHavePost = YES;
    else annotation.isHavePost = NO;
    return annotation;
}
+ (Place *)myPlaceObjectToPlace:(MyPlaceObject *)placeObject{
    Place *place = [[Place alloc] init];
    place.name = placeObject.name;
    place.address = placeObject.address;
    place.id = [placeObject.id integerValue];
    place.placeId = [NSString stringWithFormat:@"%@",placeObject.placeId];
    place.isBeen =[placeObject.isBeen boolValue];
    place.isWant = [placeObject.isWant boolValue];
    place.wantCount = [placeObject.want_count integerValue];
    place.beenCount = [placeObject.been_count integerValue];
    place.content = placeObject.descriptions;
    place.province = placeObject.province;
    place.city = placeObject.city;
    place.district =placeObject.district;
    NSArray *beeUsers = [placeObject.beenUser sortedArrayUsingDescriptors:@[]];
    NSArray *wantUsers = [placeObject.wantUser sortedArrayUsingDescriptors:@[]];
    NSMutableArray *beenArr = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *wantArr = [[NSMutableArray alloc] initWithCapacity:0];
    [beeUsers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapUser = obj;
        User *user = [[User alloc] init];
        user.avatar = mapUser.avatar;
        user.uid = [mapUser.uid integerValue];
        [beenArr addObject:user];
    }];
    [wantUsers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapUser = obj;
        User *user = [[User alloc] init];
        user.avatar = mapUser.avatar;
        user.uid = [mapUser.uid integerValue];
        [wantArr addObject:user];
    }];
    place.beenUsers = [beenArr copy];
    place.wantUsers = [wantArr copy];
    return place;
};
@end
