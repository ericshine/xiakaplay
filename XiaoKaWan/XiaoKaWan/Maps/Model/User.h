//
//  User.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentUser.h"
typedef NS_ENUM(NSInteger,UserTye){
    UserType,
    UserType_MyFans, // 我的粉丝
    UserType_MyFollows, // 我关注的
    Uormal_user  // 默认
};
@interface User : NSObject
@property(nonatomic) UserTye userType;

/**
 *  头像
 */
@property(nonatomic,copy) NSString *avatar;
/**
 *  昵称
 */
@property(nonatomic,copy) NSString *nick;

/**
 *  关注数
 */
@property(nonatomic,copy) NSString *following_count;
/**
 *  粉丝数
 */
@property(nonatomic,copy) NSString *followers_count;
/**
 *  去过的地点数
 */
@property(nonatomic,copy) NSString *been_count;
/**
 *  想去的地点数
 */
@property(nonatomic,copy) NSString *want_count;
/**
 *  用户maps数
 */
@property(nonatomic,copy) NSString *maps_count;
/**
 *  创建maps
 */
@property(nonatomic,copy) NSString *maps_create_count;
/**
 *  用户动态数
 */
@property(nonatomic,copy) NSString *post_count;
/**
 *  是否已关注
 */
@property(nonatomic,assign) BOOL followed;
/**
 *  是否双向关注
 */
@property(nonatomic,assign) BOOL bi_followed;

/**
 *  用户id
 */
@property(nonatomic) NSInteger uid;
/**
 *  密码
 */
@property(nonatomic,copy) NSString *password;
/**
 *  签名
 */
@property(nonatomic,copy) NSString *sign;
/**
 *  活跃时间
 */
@property(nonatomic,copy) NSString *activeTime;

/**
 *  我的信息还是他人的信息
 */
@property(nonatomic) BOOL isMyInformation;
/**
 *  是否是我的粉丝 还是我关注的用户
 */
/**
 *  coreData 对象转换
 *
 *  @param currentUser currentUser description
 *
 *  @return return value description
 */
+ (User *)coreDataUserToUser:(CurrentUser *)currentUser;
@end
