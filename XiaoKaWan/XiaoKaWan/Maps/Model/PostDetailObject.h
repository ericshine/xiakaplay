//
//  PostDetailObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Place.h"
#import "BJImageObject.h"
#import "User.h"
#import "CommetObject.h"
@interface PostDetailObject : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *mapsId;
@property(nonatomic,copy) NSString *placeId;
@property(nonatomic,copy) NSString *userId;
@property(nonatomic,copy) NSString *content;
@property(nonatomic,copy) NSString *commentCount;
@property(nonatomic,copy) NSString *likeCount;
@property(nonatomic,copy) NSString *actionDate;
@property(nonatomic,copy) NSString *gmtCreated;
@property(nonatomic,copy) NSString *gmtModified;
@property(nonatomic,copy) NSString *loginUserLike;
@property(nonatomic,strong) Place *placeInfo;
@property(nonatomic,strong) NSArray<BJImageObject*>*photos;
@property(nonatomic,strong) User *user;
@property(nonatomic,strong) NSArray<CommetObject*>*hotComments;
@property(nonatomic,strong) NSArray<CommetObject*>*lastComments;
@property(nonatomic,strong) NSArray<NSString *> *tags;
- (CGFloat)cellHeight;
@end
