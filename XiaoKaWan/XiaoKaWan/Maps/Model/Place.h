//
//  Place.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "BJImageObject.h"
#import "MyPlaceObject.h"
#import <UIKit/UIKit.h>
@class PostObjet;
@class PostBMKPointAnnotation;
@interface Place : NSObject
@property(nonatomic) NSInteger id;
@property(nonatomic,copy) NSString *placeId;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *content;
@property(nonatomic,copy) NSString *address;
@property(nonatomic,copy) NSString *province;
@property(nonatomic,copy) NSString *city;
@property(nonatomic,copy) NSString *district;
/**
 *  具体使用 addressDetail  不使用 address
 */
@property(nonatomic,copy) NSString *addressDetail;
@property(nonatomic,copy) NSString *addReason;
@property(nonatomic,copy) NSString *addUserId;
@property(nonatomic) NSInteger beenCount;// 多少人去过
@property(nonatomic) NSInteger wantCount;// 多少人想去
@property(nonatomic,copy) NSString *mapsCount; 
@property(nonatomic,copy) NSString *postCount;
@property(nonatomic) BOOL isWant;
@property(nonatomic) BOOL isBeen;
@property(nonatomic) CGFloat latitude;
@property(nonatomic) CGFloat longtitude;
@property(nonatomic,strong) NSArray<User*> *beenUsers;//去过的人
@property(nonatomic,strong) NSArray<User*> *wantUsers; // 想去的人
@property(nonatomic,strong) NSArray<BJImageObject*> *latestImages;
@property(nonatomic,strong) NSArray *placeImageFrames;

/*自加 */

@property(nonatomic,copy) NSString *mapId;
/**
 *   last_post 用与地图上显示动态使用
 */
@property(nonatomic,strong) PostObjet *last_post;
@property(nonatomic,copy) NSString *category_id;
@property(nonatomic,copy) NSString *sub_category_id;
@property(nonatomic,copy) NSString *category_name;
+ (NSMutableArray *)dicToObject:(NSArray*)array;
- (PostBMKPointAnnotation *)placeToAnnotation;
+ (Place *)myPlaceObjectToPlace:(MyPlaceObject *)object;
@end
