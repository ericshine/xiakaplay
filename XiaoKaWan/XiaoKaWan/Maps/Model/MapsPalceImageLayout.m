//
//  MapsPalceImageLayout.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPalceImageLayout.h"
//#define collectionViewHeight 145
@interface MapsPalceImageLayout()
@property (nonatomic, strong) NSArray *layoutInfoArr;

@property(nonatomic,strong) NSMutableArray *rowHeightArray;
@property (nonatomic, assign) CGSize contentSize;
@end
@implementation MapsPalceImageLayout
- (NSMutableArray*)rowHeightArray{
    if(_rowHeightArray == nil){
        _rowHeightArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _rowHeightArray;
}
- (void)prepareLayout{
    [super prepareLayout];
    @autoreleasepool {
        NSMutableArray *layoutInfoArr = [NSMutableArray array];
        NSInteger maxNumberOfItems = 0;
        //获取布局信息
        [self.rowHeightArray removeAllObjects];
        NSInteger numberOfSections = [self.collectionView numberOfSections];
        for (NSInteger section = 0; section < numberOfSections; section++){
            NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:section];
            NSMutableArray *subArr = [NSMutableArray arrayWithCapacity:numberOfItems];
            for (NSInteger item = 0; item < numberOfItems; item++){
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
                UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
                CGFloat rowWidth = [self.delegate collectionView:self.collectionView layout:self widthForItemAtIndexPath:indexPath];
                [self.rowHeightArray addObject:[NSNumber numberWithFloat:rowWidth]];
                CGFloat width =0;
                for (int i = 0; i<indexPath.row; i++) {
                    width += [self.rowHeightArray[i] floatValue];
                }
                attributes.frame = CGRectMake(width, 0, rowWidth,[self.delegate collectionViewHeight:self.collectionView]);
                //            attributes.frame =
                [subArr addObject:attributes];
            }
            
            if(maxNumberOfItems < numberOfItems){
                maxNumberOfItems = numberOfItems;
            }
            //添加到二维数组
            [layoutInfoArr addObject:[subArr copy]];
        }
        __block CGFloat heidth = 0;
        [self.rowHeightArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            heidth += [obj floatValue];
        }];
        self.contentSize = CGSizeMake(heidth, [self.delegate collectionViewHeight:self.collectionView]);
        self.layoutInfoArr = [layoutInfoArr copy];
    }

}
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    NSMutableArray *layoutAttributesArr = [NSMutableArray array];
    [self.layoutInfoArr enumerateObjectsUsingBlock:^(NSArray *array, NSUInteger i, BOOL * _Nonnull stop) {
        [array enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if(CGRectIntersectsRect(obj.frame, rect)) {
                [layoutAttributesArr addObject:obj];
            }
        }];
    }];
    return layoutAttributesArr;
}
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    return attributes;
}

- (CGSize)collectionViewContentSize{
    return self.contentSize;
}
@end
