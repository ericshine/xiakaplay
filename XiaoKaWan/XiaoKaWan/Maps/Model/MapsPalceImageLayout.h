//
//  MapsPalceImageLayout.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MapsPalceImageLayout;
@protocol MapsPalceImageLayoutDelegate <NSObject>
@required
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPalceImageLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath;
@optional
- (CGFloat)collectionViewHeight:(UICollectionView *)collectionView;

@end
@interface MapsPalceImageLayout : UICollectionViewLayout
@property(nonatomic,assign) id<MapsPalceImageLayoutDelegate>delegate;
@end
