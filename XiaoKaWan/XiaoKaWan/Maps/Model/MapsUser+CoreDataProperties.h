//
//  MapsUser+CoreDataProperties.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MapsUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface MapsUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) NSString *nick;
@property (nullable, nonatomic, retain) NSString *avatar;
@property (nullable, nonatomic, retain) Maps *maps;

@end

NS_ASSUME_NONNULL_END
