//
//  MapsDetailFrame.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetailFrame.h"
#import "NSString+Cagetory.h"
@implementation MapsDetailFrame{
    MapsDetail *mapsDetailObj;
}
- (instancetype)initWithMapsDetail:(MapsDetail *)mapsDetail{
    self = [super init];
    if(self){
        mapsDetailObj = mapsDetail;
    }
    return self;
}
- (CGFloat)headCellHeight{
    CGSize size =  [mapsDetailObj.content sizeWithFont:[UIFont systemFontOfSize:17] andSize:CGSizeMake(kDeviceWidth-154, 9999)];
    CGFloat height = 210 + size.height;
    return height;
}
@end
