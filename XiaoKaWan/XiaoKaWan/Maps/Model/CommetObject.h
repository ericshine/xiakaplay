//
//  CommetObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <UIKit/UIKit.h>
@interface CommetObject : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *postId;
@property(nonatomic,copy) NSString *userId;
@property(nonatomic,copy) NSString *likes;
@property(nonatomic,copy) NSString *content;
@property(nonatomic,copy) NSString *gmtCreated;
@property(nonatomic,copy) NSString *gmtCreatedH;
@property(nonatomic,copy) NSString *loginUserlLike;
@property(nonatomic,strong) User *user;
-(CGFloat)commentHeight;
@end
