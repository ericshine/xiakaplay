//
//  PostFrameObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostFrameObject.h"
#import "NSString+Cagetory.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
@implementation PostFrameObject{
    NSArray *_postLists;
}
- (instancetype)initWithPostList:(NSArray *)postist{
    self = [super init];
    if(self){
        _postLists = postist;
        [self postItemHeights];
        [self myPostItemHeights];
    }
    return self;
}
- (CGFloat)headViewHeight{
    CGFloat height = 290;
    if(self.place.content.length>0){
        NSString *content;
        if(self.place.content.length>50) content = [self.place.content substringToIndex:50];
        else content = self.place.content;
     CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:[PostFrameObject postContentFont]] andSize:CGSizeMake(kDeviceWidth-145, 9999)];
    height += size.height;
    }else{
        height -= 25;
    }
    return height;
}
- (NSArray *)postItemHeights{
    if(_postItemHeights == nil ||_postItemHeights.count<=0){
    NSMutableArray *heights = [[NSMutableArray alloc] initWithCapacity:_postLists.count];
    [_postLists enumerateObjectsUsingBlock:^(PostObjet * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGFloat h = obj.latest_image.height;
        CGFloat w = obj.latest_image.width;
        CGFloat height = 78;
        if(w !=0 && h != 0){
        CGFloat cellW = [PostFrameObject widthPostCell];
        height += (cellW/w)*h;
        }
        CGSize size = [obj.content sizeWithFont:[UIFont systemFontOfSize:[PostFrameObject postContentFont]] andSize:CGSizeMake(([PostFrameObject widthPostCell] -30), 999)];
        if(size.height>25) height = height+25;
        else height = height +size.height;
        [heights addObject:[NSNumber numberWithDouble:height]];
    }];
    _postItemHeights = [heights copy];
    }
        return _postItemHeights;
}
- (NSArray *)myPostItemHeights{
    if(_myPostItemHeights == nil ||_myPostItemHeights.count<=0){
        NSMutableArray *heights = [[NSMutableArray alloc] initWithCapacity:_postLists.count];
        [_postLists enumerateObjectsUsingBlock:^(PostObjet * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            BJImageObject *imageObj = obj.latestPostFristImage;
            CGFloat h = imageObj.height;
            CGFloat w = imageObj.width;
            CGFloat height = 50;
            if(w != 0 && h != 0 && imageObj){
            CGFloat cellW = [PostFrameObject widthPostCell];
            height += (cellW/w)*h;
            }
            CGSize size = [obj.content sizeWithFont:[UIFont systemFontOfSize:[PostFrameObject postContentFont]] andSize:CGSizeMake(([PostFrameObject widthPostCell] -11), 999)];
            if(size.height>20) height = height+20;
            else height = height +size.height;
            [heights addObject:[NSNumber numberWithDouble:height]];
        }];
        _myPostItemHeights = [heights copy];
    }
    return _myPostItemHeights;
}
+ (CGFloat)widthPostCell{
    return (kDeviceWidth-30)/2;
}
+ (CGFloat)postContentFont{
    return 10;
}
+(CGFloat)placeDescribFont{
    return 12;
}
@end
