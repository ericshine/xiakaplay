//
//  MapObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapObject : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *content;
@property(nonatomic) BOOL isFollow;
@property(nonatomic,copy) NSString *placeId;
+ (MapObject*)mapObject;
@end
