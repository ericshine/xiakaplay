

#import <UIKit/UIKit.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height
#define kFixTop (44)//在此修正sectionheader停留的位置

@class BJMapsListCollectionLayout;

@protocol BJMapsListCollectionDelegate <NSObject>

/**
 *  返回所在section的每个item的width（一个section只有一个width）
 *
 */
- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
    widthForItemInSection:( NSInteger )section;
/**
 *  返回所在indexPath的每个item的height
 *
 */
- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
 heightForItemAtIndexPath:(nonnull NSIndexPath *)indexPath;

@optional
/**
 *  返回所在indexPath的header的height
 *
 */
- (CGFloat) collectionView:(nonnull UICollectionView *)collectionView
                    layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
heightForHeaderAtIndexPath:(nonnull NSIndexPath *)indexPath;
/**
 *  返回所在section与上一个section的间距(表达的可能不够准确，但是你们都懂的)
 *
 */
- (CGFloat) collectionView:(nonnull UICollectionView *)collectionView
                    layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
              topInSection:(NSInteger )section;
/**
 *  返回所在section与下一个section的间距(表达的可能不够准确，但是你们都懂的)
 *
 */
- (CGFloat) collectionView:(nonnull UICollectionView *)collectionView
                    layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
           bottomInSection:( NSInteger)section;
/**
 *  返回所在section的header停留时与顶部的距离（如果设置isTopForHeader ＝ yes ，则距离会叠加）
 *
 */
- (CGFloat) collectionView:(nonnull UICollectionView *)collectionView
                    layout:(nonnull BJMapsListCollectionLayout *)collectionViewLayout
      headerToTopInSection:( NSInteger)section;
@end

@interface BJMapsListCollectionLayout : UICollectionViewLayout

@property (nullable,nonatomic, assign)  id<BJMapsListCollectionDelegate> delegate;
//在此修正sectionheader停留的位置,默认64
@property (nonatomic,assign) CGFloat fixTop;

//是否设置sectionHeader停留,默认YES
@property (nonatomic) BOOL isStickyHeader;
//section停留的位置是否包括原来设置的top，默认NO
@property (nonatomic) BOOL isTopForHeader;

@end


// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com