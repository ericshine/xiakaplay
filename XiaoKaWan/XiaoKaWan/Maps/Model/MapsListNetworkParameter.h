//
//  MapsListNetworkParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapsListNetworkParameter : NSObject
/**
 *  默认0
 */
@property(nonatomic) NSInteger last_id;
/**
 *  earlier / latest
 */
@property(nonatomic,copy) NSString *get_type;
/**
 *  可用范围10～100
 */
@property(nonatomic,copy) NSString *count;
@end
