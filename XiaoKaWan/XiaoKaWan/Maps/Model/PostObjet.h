//
//  PostObjet.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJImageObject.h"
#import "User.h"
#import "Place.h"
#import <MJExtension.h>
#import "UserPost.h"
@interface PostObjet : NSObject
@property(nonatomic) NSInteger id;
@property(nonatomic,copy) NSString *content;
@property(nonatomic) NSInteger like_count;
@property(nonatomic) NSInteger comment_count;
@property(nonatomic) NSInteger image_count;
@property(nonatomic,strong) BJImageObject *latest_image;
@property(nonatomic,strong) BJImageObject *latestPostFristImage;
@property(nonatomic,strong) NSArray<BJImageObject*> *latest_images;
@property(nonatomic) NSInteger place_id;
@property(nonatomic,copy) NSString *address;
@property(nonatomic,strong) User *post_user;
@property(nonatomic,strong) Place *placeInfo;
+ (PostObjet *)coreDataToObject:(UserPost *)post;
+ (NSMutableArray *)dicToObjectWithArray:(NSArray *)array;
@end
