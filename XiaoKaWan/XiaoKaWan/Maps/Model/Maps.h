//
//  Maps.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MapsUser;

NS_ASSUME_NONNULL_BEGIN

@interface Maps : NSManagedObject
+ (void)saveMapsData:(NSArray *)mapsList;
// Insert code here to declare functionality of your managed object subclass
+ (NSArray<Maps*>*)getMapsList;

@end

NS_ASSUME_NONNULL_END

#import "Maps+CoreDataProperties.h"
