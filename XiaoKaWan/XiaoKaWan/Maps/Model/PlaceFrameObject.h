//
//  PlaceFrameObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJImageObject.h"
@interface PlaceFrameObject : NSObject
@property(nonatomic,strong) NSArray<NSArray*> *placeImageWidths;//动态cell的高度array
- (instancetype)initWithPlaceList:(NSArray*)placeList;
+ (CGFloat)heightimageCollectionView;
+ (CGFloat)heightCellShowMore:(BOOL)more content:(NSString*)text;
+ (CGFloat)reasonLabelFont;
@end
