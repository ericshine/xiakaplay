//
//  MapsUser.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
@class  Maps;
@interface MapsUser : NSManagedObject
+ (void)saveMapUser:(NSArray*)array withMaps:(Maps*)maps;
// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MapsUser+CoreDataProperties.h"
