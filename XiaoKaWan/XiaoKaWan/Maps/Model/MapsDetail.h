//
//  MapsDetail.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJImageObject.h"
#import "PostObjet.h"
#import "Place.h"
@interface MapsDetail : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *content;
@property(nonatomic) NSInteger follow_count;
@property(nonatomic) NSInteger read_count;
@property(nonatomic) NSInteger place_count;
@property(nonatomic) NSInteger post_count;
@property(nonatomic) NSInteger user_count;
@property(nonatomic) NSInteger id;
@property(nonatomic) NSInteger createUserId;
@property(nonatomic) BOOL isFollowed;
@property(nonatomic,strong) NSArray<BJImageObject*> *latest_photos;
@property(nonatomic,strong) NSArray<PostObjet*> *latest_post; // 基于动态的数据
@property(nonatomic,strong) NSArray<Place*> *place_list; // 基于地点的数据
@property(nonatomic,strong) NSArray<User*> *latest_users;
@property(nonatomic,copy) NSString *addReason;
+ (NSMutableArray *)dicToObject:(NSArray *)array;
@end
