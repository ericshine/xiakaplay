//
//  PostObjet.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostObjet.h"
#import "MyPlaceObject.h"
@implementation PostObjet
- (instancetype)init{
    self = [super init];
    if(self){
        [PostObjet setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"latest_image":[BJImageObject class],
                      @"latest_images":[BJImageObject class],
                     @"placeInfo":[Place class],
                     @"latestPostFristImage":[BJImageObject class]
                     };
        }];
        [PostObjet setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"placeInfo":@"place_info",
                     @"latestPostFristImage":@"latest_post_frist_image"
                     };
        }];
    }
    return self;
}
+ (PostObjet *)coreDataToObject:(UserPost *)post{
    PostObjet *postObj = [[PostObjet alloc] init];
    postObj.id = post.idValue;
    postObj.like_count = post.like_countValue;
    postObj.comment_count = post.comment_countValue;
    postObj.content = post.content;
    postObj.image_count = post.image_countValue;
    postObj.address = post.place.name;
    Place *place = [[Place alloc] init];
    place.address = post.place.address;
    place.name = post.place.name;
    postObj.placeInfo = place;
    if(post.images !=nil){
        NSDictionary *imageDic =[NSJSONSerialization JSONObjectWithData:post.images options:NSJSONReadingAllowFragments error:nil];
         BJImageObject *imageObj = [BJImageObject objectWithKeyValues:imageDic];
        postObj.latestPostFristImage = imageObj;
    }
    return postObj;
    
}
+ (NSMutableArray *)dicToObjectWithArray:(NSArray *)array{
    NSMutableArray *objects = [[NSMutableArray alloc] initWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PostObjet *object = [PostObjet objectWithKeyValues:obj];
        [objects addObject:object];
    }];
    return objects;
}
@end
