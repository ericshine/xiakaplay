//
//  PostFrameObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostListObj.h"
@interface PostFrameObject : NSObject
@property(nonatomic,strong) NSArray *postItemHeights;//动态cell的高度array
@property(nonatomic,strong) NSArray *myPostItemHeights;
@property(nonatomic,strong)Place *place;
- (instancetype)initWithPostList:(NSArray*)postist;
- (CGFloat)headViewHeight;
+ (CGFloat)widthPostCell;
+ (CGFloat)postContentFont;
@end
