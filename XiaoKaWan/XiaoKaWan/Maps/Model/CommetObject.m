//
//  CommetObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommetObject.h"
#import <MJExtension.h>
#import "NSString+Cagetory.h"
@implementation CommetObject
- (instancetype)init{
    self = [super init];
    if(self){
    [CommetObject setupReplacedKeyFromPropertyName:^NSDictionary *{
        return @{
                 @"postId":@"post_id",
                 @"userId":@"user_id",
                 @"gmtCreated":@"gmt_created",
                 @"gmtCreatedH":@"gmt_created_h",
                 @"loginUserlLike":@"login_user_like"
                 };
    }];
        [CommetObject setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"user":[User class],
                     };
        }];
    }
    return self;
}
- (CGFloat)commentHeight{
    CGFloat height = 50;
    CGSize size = [self.content sizeWithFont:[UIFont systemFontOfSize:12] andSize:CGSizeMake(kDeviceWidth-70, 999999)];
    height +=size.height;
    if(height<100) return 100;
    else return height;
}
@end
