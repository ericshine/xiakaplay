//
//  MapsListNetworkParameter.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsListNetworkParameter.h"

@implementation MapsListNetworkParameter
- (instancetype)init{
    self = [super init];
    if(self){
        self.count = @"10";//默认10条
        self.get_type = @"earlier";
        self.last_id = 0;
    }
    return self;
}
@end
