//
//  MapsUser.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsUser.h"
#import "MapsModelManage.h"
#import <MJExtension.h>
#import "Maps.h"
@implementation MapsUser
+ (void)saveMapUser:(NSArray *)array withMaps:(nonnull Maps *)maps{
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *mapsUser = [MapsUser getMapsWithId:[obj[@"uid"] integerValue]];
        if(mapsUser == nil){
            mapsUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        mapsUser.maps = maps;
        [mapsUser setKeyValues:obj];
    }];
//    NSLog(@"%lu",maps.mapsUser.count);
}
+ (MapsUser *)getMapsWithId:(NSInteger)uid{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"uid = %i",uid];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
// Insert code here to add functionality to your managed object subclass

@end
