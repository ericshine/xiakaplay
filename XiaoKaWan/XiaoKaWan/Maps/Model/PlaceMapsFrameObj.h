//
//  PlaceMapsFrameObj.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PlaceMapsFrameObj : NSObject
@property(nonatomic,strong) NSArray *mapsCellHeight;
- (instancetype)initWithMapsArray:(NSArray*)mapsArray;
+ (CGFloat)reasonLabelFont;
@end
