//
//  MapsPlaceLayout.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MapsPlaceLayout;
@protocol MapsPlaceLayoutDelegate <NSObject>
@required
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(MapsPlaceLayout *)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath *)indexPath;
@end
@interface MapsPlaceLayout : UICollectionViewLayout
@property(nonatomic,assign) id<MapsPlaceLayoutDelegate>delegate;
@end
