//
//  PlaceFrameObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceFrameObject.h"
#import "Place.h"
#import "NSString+Cagetory.h"
@implementation PlaceFrameObject{
    NSArray *placeLists;
}
- (instancetype)initWithPlaceList:(NSArray *)placeList{
    self = [super init];
    if(self){
        placeLists = placeList;
    }
    return self;
}
- (NSArray<NSArray *>*)placeImageWidths{
    if(_placeImageWidths == nil ||_placeImageWidths.count<=0){
        NSMutableArray *placeImagesHs =[[NSMutableArray alloc] initWithCapacity:placeLists.count];
        [placeLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            Place *place = (Place*)obj;
            NSMutableArray *imageHeights = [[NSMutableArray alloc] initWithCapacity:place.latestImages.count];
            [place.latestImages enumerateObjectsUsingBlock:^(BJImageObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                BJImageObject *imageObj = obj;
                CGFloat h = imageObj.height;
                CGFloat w = imageObj.width;
                CGFloat imageH = [PlaceFrameObject heightimageCollectionView];
                CGFloat imageW = (imageH*w)/h;
                [imageHeights addObject:[NSNumber numberWithFloat:imageW]];
            }];
            [placeImagesHs addObject:imageHeights];
        }];
    return [placeImagesHs copy];
    }else{
        return _placeImageWidths;
    }
}
+ (CGFloat)heightCellShowMore:(BOOL)more content:(NSString *)text{
     CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:[PlaceFrameObject reasonLabelFont]] andSize:CGSizeMake(kDeviceWidth-70, 9999)];
    if(more){
        return 290+40+size.height;
    }
    if(text.length<=0)return 290+40;
    if(size.height>20) return 290+40+20;
    return 290+40+size.height;
}
+ (CGFloat)heightimageCollectionView{
    return 145;
}
+(CGFloat)reasonLabelFont{
    return 11;
}
@end
