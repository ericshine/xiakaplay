//
//  MapDetailNetworkParameter.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension.h>
@interface MapDetailNetworkParameter : NSObject
@property(nonatomic,copy) NSString *maps_id;
@property(nonatomic,copy) NSString *last_id;
@property(nonatomic,copy) NSString *count;
@property(nonatomic,copy) NSString *get_type;
@property(nonatomic,copy) NSString *_t;
@end
