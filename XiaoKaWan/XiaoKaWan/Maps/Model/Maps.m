//
//  Maps.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "Maps.h"
#import "MapsUser.h"
#import "MapsModelManage.h"
#import <MJExtension.h>
@implementation Maps
+ (void)saveMapsData:(NSArray *)mapsList{
    [mapsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Maps *maps = nil; //[Maps getMapsWithId:[obj[@"id"] integerValue]];
        if(maps == nil){
            maps = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[MapsModelManage sharedInstance].context];
        }
        NSData *photoData = [NSJSONSerialization dataWithJSONObject:obj[@"latestPhotos"] options:NSJSONWritingPrettyPrinted error:nil];
        maps.photos = photoData;
        [maps setKeyValues:obj];
        [MapsUser saveMapUser:obj[@"users"] withMaps:maps];
    }];
    [[MapsModelManage sharedInstance] saveContext];
}
+ (Maps *)getMapsWithId:(NSInteger)mapsId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate =[NSPredicate predicateWithFormat:@"id = %i",mapsId];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result[0];
}
+ (NSArray<Maps*>*)getMapsList{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSArray *result = [[MapsModelManage sharedInstance].context executeFetchRequest:fetchRequest error:nil];
    if(!result.count){
        return nil;
    }
    return result;
}
// Insert code here to add functionality to your managed object subclass

@end
