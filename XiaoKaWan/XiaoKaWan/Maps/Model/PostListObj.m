//
//  PostListObj.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PostListObj.h"
#import <MJExtension.h>
@implementation PostListObj
- (instancetype)init{
    self = [super init];
    if(self){
        [PostListObj setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                        @"postCount":@"post_count",
                        @"maspCount":@"maps_count",
                        @"postList":@"post_list",
                        @"beenUsers":@"been_users",
                        @"wantUsers":@"want_users",
                        @"beenCount":@"been_count",
                        @"wantCount":@"want_count"
                     };
        }];
        [PostListObj setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"postList":[PostObjet class],
                     @"beenUsers":[User class],
                     @"wantUsers":[User class],
                     @"maps":[MapsDetail class]
                     };
        }];
    }
    return self;
}
@end
