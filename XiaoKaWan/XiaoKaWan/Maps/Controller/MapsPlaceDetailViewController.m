//
//  MapsPlaceDetailViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceDetailViewController.h"
#import "MapsPlaceDetailViewModel.h"
#import "UIColor+colorWithHex.h"
#import "AddPlaceToMapsView.h"
#import "MapsUserListViewController.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "ThirdpartyNavigation.h"
#import "BJShareTool.h"
#import "AddPostPopView.h"
#import "BJNavigationController.h"
#import <AVFoundation/AVFoundation.h>
#import "PhotoAlbumViewController.h"
#import "AddPostVViewController.h"
#import "PhotoObject.h"
#import "CreatLoactionObj.h"
@interface MapsPlaceDetailViewController ()<AddPostPopViewDelegate,MapsPlaceDetailViewModelDelegate,BMKLocationServiceDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property(nonatomic,strong)MapsPlaceDetailViewModel *viewModel;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)AddPlaceToMapsView *addPlaceToMapsPopView;
@property(nonatomic,strong) BMKLocationService *locationService;
@property(nonatomic,strong) AddPostPopView *addPostPopView;
@property(nonatomic,strong) UIButton *addPost;
@end

@implementation MapsPlaceDetailViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.locationService startUserLocationService];
    self.locationService.delegate = self;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.locationService stopUserLocationService];
    self.locationService.delegate = nil;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self collectionView];
    [self addPost];
    [self rightButtonWithColor:0xff0000 title:@"加入榜单"];
    [self rightButtonWithIconfont:@"\U0000e622" color1:0x000000 title1:@"\U0000e623" color2:0x000000];
    _collectionView.delegate = self.viewModel;
    _collectionView.dataSource = self.viewModel;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPost) name:BJAddPostNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPost) name:BJDeletePostNotification object:nil];
    // Do any additional setup after loading the view.
}
- (void)reloadPost{
    self.viewModel.place.postCount = [NSString stringWithFormat:@"%ld",([self.viewModel.place.postCount integerValue] +1)];
    [self.viewModel getPlaceData:YES];
}
- (void)rightClick{
   [self.addPlaceToMapsPopView showViewInSupperView:self.tabBarController.view];
    _addPlaceToMapsPopView.place = self.place;
}
- (void)rightClick1{
    [BJShareTool shareAddressDetailWithViewController:self placeID:@"1"];
    
    
}
- (BMKLocationService *)locationService{
    if(_locationService == nil){
        _locationService = [[BMKLocationService alloc] init];
        
    }
    return _locationService;
}
- (UIButton *)addPost{
    if(_addPost == nil){
        _addPost = [[UIButton alloc] initWithFrame:CGRectMake(kDeviceWidth-60, kDeviceHeight-128, 50, 50)];
        [_addPost setBackgroundImage:[UIImage imageNamed:@"post_button1"] forState:UIControlStateNormal];
        [self.view addSubview:_addPost];
        
        [_addPost addTarget:self action:@selector(addPostAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addPost;
}
- (void)addPostAction{
    if([UserConfig sharedInstance].user_token.length) [self.addPostPopView showInSuperView:self.tabBarController.view buttonPoint:_addPost.frame.origin buttonInView:self.view];
    else{
        [self toLoginVcClearData:NO];
    }
}
- (AddPostPopView *)addPostPopView{
    if(_addPostPopView == nil){
        _addPostPopView = [[AddPostPopView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        [_addPostPopView addItems:@[@"paot_camera",@"post_photo",@"post_text"]];
        _addPostPopView.delegate = self;
        
    }
    return _addPostPopView;
}
- (AddPlaceToMapsView *)addPlaceToMapsPopView{
    if(_addPlaceToMapsPopView == nil){
        _addPlaceToMapsPopView = [[AddPlaceToMapsView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        __weak typeof(self) weakSelf = self;
        _addPlaceToMapsPopView.isAddToMaps = ^(BOOL isAddToMaps){
            if(isAddToMaps){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            NSInteger mapsCount = [strongSelf.viewModel.place.mapsCount integerValue] +1;
            strongSelf.viewModel.place.mapsCount = [NSString stringWithFormat:@"%li",(long)mapsCount];
                [strongSelf.viewModel getPlaceData:YES];
                
            }
        };
    }
    return _addPlaceToMapsPopView;
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}
- (MapsPlaceDetailViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[MapsPlaceDetailViewModel alloc] initWithModel:self.place controller:self collectionView:_collectionView];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
- (void)toUserList:(Place *)place{
    MapObject *placeObj = [[MapObject alloc] init];
    placeObj.placeId = place.placeId;
    MapsUserListViewController *userListVc = [[MapsUserListViewController alloc] init];
    userListVc.model = placeObj;
    [self push:userListVc];
}
- (void)thirdpartyNavigation{
   UIAlertController *alterVc = [[ThirdpartyNavigation sharedInstance] navigationAlertControllerFromPlace:self.locationService.userLocation.location.coordinate toPlace:self.place];
    [self presentViewController:alterVc animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    PhotoObject *photo = [[PhotoObject alloc] init];
    photo.image = image;
    photo.thumbImage = image;
    [self addPostToPlace:@[photo]];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AddPostPopViewDelegate
- (void)selectAtIndex:(NSInteger)index{
    [self.addPostPopView disMiss];
    switch (index) {
        case 0:
        {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if(status == AVAuthorizationStatusDenied){
                BJAlertController *alertVc = [[BJAlertController alloc] initWithTitle:@"小咖玩想要使用您的相机" message:nil preferredStyle:UIAlertControllerStyleAlert];
                [alertVc addActionWithTitle:@"设置" handler:^{
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }];
                [alertVc addCancleActionHandler:nil];
                
                [self presentViewController:alertVc animated:YES completion:nil];
            }else{
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePicker.delegate = self;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }break;
        case 1:
        {
            PhotoAlbumViewController *photoAlbum = [[PhotoAlbumViewController alloc] init];
            photoAlbum.limitCount = 9;
            [self presentViewController:[[BJNavigationController alloc] initWithRootViewController:photoAlbum] animated:YES completion:nil];
            __weak typeof(self) weakSelf = self;
            photoAlbum.seleltImages = ^(NSArray *images){
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf addPostToPlace:images];
            };
        }break;
        case 2:
        {
            MapSearchResult *location = [[MapSearchResult alloc] init];
            location.name = self.place.name;
            location.place_id = [NSString stringWithFormat:@"%li",(long)self.place.id];
            AddPostVViewController *addPost = [[AddPostVViewController alloc] init];
            addPost.postType = POST_TEXT;
            addPost.location = location;
            addPost.fromeVc = FromeVc_place;
            [self push:addPost];
            
        }break;
            
        default:
            break;
    }
}
- (void)addPostToPlace:(NSArray *)images{
    MapSearchResult *location = [[MapSearchResult alloc] init];
    location.name = self.place.name;
    location.place_id = [NSString stringWithFormat:@"%li",(long)self.place.id];
    AddPostVViewController *addPost = [[AddPostVViewController alloc] init];
    addPost.photoObjs = images;
    addPost.postType = POST_IMAGE;
    addPost.location = location;
    addPost.fromeVc = FromeVc_place;
    [self push:addPost];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
