//
//  MapDetailListViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "MapObject.h"
@interface MapDetailListViewController : BJSuperViewController
@property(nonatomic,strong)MapObject *mapObject;
@end
