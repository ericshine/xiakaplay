//
//  PlacePostViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostViewController.h"
#import "PlacePostViewModel.h"
@interface PlacePostViewController ()
@property(nonatomic,strong)PlacePostViewModel *viewModel;
@property(nonatomic,strong) UICollectionView *collectionView;
@end

@implementation PlacePostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self collectionView];
    // Do any additional setup after loading the view.
}
- (UICollectionView *)collectionView
{
    if(_collectionView == nil){
        UICollectionViewLayout *layOut = [[UICollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) collectionViewLayout:layOut];
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_collectionView];
        _collectionView.delegate = self.viewModel;
        _collectionView.dataSource = self.viewModel;
        
    }
    return _collectionView;
}
- (PlacePostViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[PlacePostViewModel alloc] initWithModel:self.mapsObject controller:self collectionView:_collectionView];
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
