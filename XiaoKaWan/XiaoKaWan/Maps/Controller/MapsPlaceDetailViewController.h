//
//  MapsPlaceDetailViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "Place.h"
@interface MapsPlaceDetailViewController : BJSuperViewController
@property(nonatomic,strong)Place *place;
@end
