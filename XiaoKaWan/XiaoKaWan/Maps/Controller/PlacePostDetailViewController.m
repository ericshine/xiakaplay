//
//  PlacePostDetailViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailViewController.h"
#import "CommentAndLikeView.h"
#import "CommentLikeViewModel.h"
#import "PlacePostDetailViewModel.h"
#import "MapsUserListViewController.h"
#import "PopSheetView.h"
#import "MapsNetworkManage.h"
#import "MeHomeViewController.h"
#import "MapsPlaceDetailViewController.h"
#import "BJShareTool.h"
@interface PlacePostDetailViewController ()<CommentLikeViewModelDelegate>
@property(nonatomic,strong)CommentAndLikeView *commentView;
@property(nonatomic,strong)CommentLikeViewModel *commentViewModel;
@property(nonatomic,strong)PlacePostDetailViewModel *detailViewModel;
@property(nonatomic,strong)PopSheetView *popView;
@end

@implementation PlacePostDetailViewController{
    BOOL isCurrentUser;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self.commentViewModel selector:@selector(keyBoardViewShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.commentViewModel selector:@selector(keyBoardHidden:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.commentViewModel selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self.commentViewModel];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.edgesForExtendedLayout=UIRectEdgeNone;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadedPostData:) name:@"kLoadedPostData" object:nil];
    self.tableView.delegate = self.detailViewModel;
    self.tableView.dataSource = self.detailViewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, commentViewHeight, 0);
    [self commentViewModel];
     _commentView.delegate = self.commentViewModel;
    // Do any additional setup after loading the view.
}
- (void)rightClick{
    if(isCurrentUser){
        [self deletePost];
    }else{
        /**
         *  分享
         *
         */
        [BJShareTool shareDynamicDetailWithTypeViewController:self postID:self.parameter.post_id];
        
    }
}
- (void)rightClick1{
    [self sharePost];
}
- (void)loadedPostData:(NSNotification *)info{
    PostDetailObject * post = (PostDetailObject*)info.object;
    NSInteger currentId = [[UserConfig sharedInstance].userId integerValue];
    NSInteger userId = [post.userId integerValue];
    if(userId == currentId){
        isCurrentUser = YES;
//        [self rightButtonWithIconfont:@"\U0000e620"];
        [self rightButtonWithIconfont:@"\U0000e62d" color1:0x000000 title1:@"\U0000e623" color2:0x000000];
    }else{
    [self rightButtonWithIconfont:@"\U0000e623"];
    }
    
}
//- (PopSheetView *)popView{
//    if(_popView == nil){
//        _popView = [[PopSheetView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
//        __weak typeof(self) weakSelf = self;
//        _popView.selectAtIndex = ^(NSInteger index){
//            __strong typeof(weakSelf) strongSelf = weakSelf;
//            [strongSelf sheetActionWithIndex:index];
//        };
//    }
//    return _popView;
//}
- (PlacePostDetailViewModel*)detailViewModel{
    if(_detailViewModel == nil){
         __weak typeof(self) weakSelf = self;
        _detailViewModel = [[PlacePostDetailViewModel alloc] initWithModel:self.parameter controller:self table:self.tableView];
        _detailViewModel.delegate = self.commentViewModel;
        _detailViewModel.scrollDidScroll = ^(){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.commentView.commentTextView resignFirstResponder];
        };
        _detailViewModel.toUserList = ^(PostDetailObject *object){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf toUserList:object];
        };
        _detailViewModel.toUserDetail = ^(User *user){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
            MeHomeViewController *userInfor = [[MeHomeViewController alloc] init];
            userInfor.currentUser = user;
            userInfor.userInforType = USERINFOR_OTHER;
            [strongSelf push:userInfor];
        };
        _detailViewModel.toPlaceDetail = ^(Place *place){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
            MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
            placeDetail.place = place;
            [strongSelf push:placeDetail];
        };
    }
    return _detailViewModel;
}
- (CommentAndLikeView *)commentView{
    if(_commentView == nil){
        _commentView = [[CommentAndLikeView alloc] init];
        _commentView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_commentView];
    }
    return _commentView;
}
- (CommentLikeViewModel *)commentViewModel{
    if(_commentViewModel == nil){
        __weak typeof(self) weakSelf = self;
        _commentViewModel = [[CommentLikeViewModel alloc] initWithView:
                             self.commentView andSuperView:self.view];
        _commentViewModel.keyboardShow = ^(CGFloat height){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.tableView.contentInset = UIEdgeInsetsMake(0, 0, height, 0);
        };
        _commentViewModel.delegate = self;
        PostParatemer *parameter=  [PostParatemer parameter];
        parameter.post_id =self.parameter.post_id;
        _commentViewModel.paratemer = parameter;
    }
    return _commentViewModel;
}

- (void)sharePost{
    [BJShareTool shareDynamicDetailWithTypeViewController:self postID:self.parameter.post_id];
}
- (void)deletePost{
    [self presentViewController:[[BJAlertController alloc] initWithTitle:nil message:@"你要删除这条动态吗" preferredStyle:UIAlertControllerStyleAlert withTitles:@[@"删除"] handler:^(NSInteger index) {
        self.parameter._t = [UserConfig sharedInstance].user_token;
        [[MapsNetworkManage sharedInstance] deletePostWithParameter:self.parameter complete:^(BOOL succeed, id obj) {
            if(succeed) {
                [[NSNotificationCenter defaultCenter] postNotificationName:BJDeletePostNotification object:nil];
                [self popViewController];
            }
            else [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }];
    }] animated:YES completion:nil];
}
#pragma mark -CommentLikeViewModelDelegate
- (void)commentSuccess:(CommetObject *)commentObj{
    [self.detailViewModel commentSuccess:commentObj];
}
- (void)likePostCuccess:(NSString *)likeState{
    [self.detailViewModel likePostSuccess:likeState];
}
- (void)toUserList:(PostDetailObject *)object{
    MapObject *placeObj = [[MapObject alloc] init];
    placeObj.placeId = object.placeId;
    MapsUserListViewController *userListVc = [[MapsUserListViewController alloc] init];
    userListVc.model = placeObj;
    [self push:userListVc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
