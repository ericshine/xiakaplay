//
//  MapsUserListViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "MapsUserListViewModel.h"
#import "MapObject.h"
@interface MapsUserListViewController : BJSuperViewController
@property(nonatomic,strong)NSArray *dataArray;
@property(nonatomic)UserListType userListTyper;
@property(nonatomic,strong)MapObject *model;
@end
