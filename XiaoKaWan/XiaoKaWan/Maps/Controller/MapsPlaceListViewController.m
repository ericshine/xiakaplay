//
//  MapsPlaceListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceListViewController.h"
#import "MapsPlaceDetailViewController.h"
#import "MapsPlaceListViewModel.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface MapsPlaceListViewController ()<MapsPlaceListViewModelDelegate>
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) MapsPlaceListViewModel *viewModel;
@end

@implementation MapsPlaceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self collectionView];
//    self.edgesForExtendedLayout = UIRectEdgeTop;
    // Do any additional setup after loading the view.
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(kDeviceWidth, 100);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self.viewModel;
        _collectionView.dataSource = self.viewModel;
        [self.view addSubview:_collectionView];
    }
    
    return _collectionView;
}
- (MapsPlaceListViewModel *)viewModel{
    if(_viewModel== nil){
        _viewModel = [[MapsPlaceListViewModel alloc] initWithView:_collectionView];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
#pragma mark - MapsPlaceListViewModelDelegate
- (void)didSelectItem{
    MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
    [self push:placeDetail];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
