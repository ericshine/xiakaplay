//
//  ShowImageViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ShowImageViewController.h"
#import <Masonry.h>
#import "BJUIImageView.h"
#import "UIColor+colorWithHex.h"
@interface ShowImageViewController ()

@end

@implementation ShowImageViewController
{
    BJUIImageView *imageView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    imageView = [[BJUIImageView alloc] init];
    imageView.backgroundColor = [UIColor p_colorWithHex:0xefefef];
    [imageView setImageWithUrlString:[ImageSizeUrl image750_url:self.imageObject.url]];
//    imageView.image = [UIImage imageNamed:@"image2"];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
