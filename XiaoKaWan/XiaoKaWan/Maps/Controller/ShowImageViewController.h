//
//  ShowImageViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJImageObject.h"
@interface ShowImageViewController : UIViewController
@property(nonatomic)NSInteger pageIndex;
@property(nonatomic,strong) BJImageObject *imageObject;
@end
