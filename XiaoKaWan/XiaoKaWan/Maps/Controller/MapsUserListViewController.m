//
//  MapsUserListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsUserListViewController.h"
#import "MapsUserListModel.h"
#import "UserListHeadView.h"
#import "MeHomeViewController.h"
@interface MapsUserListViewController ()<MapsUserListViewModelDelegate>
@property(nonatomic,strong)MapsUserListViewModel *beenViewModel;
@property(nonatomic,strong)MapsUserListViewModel *wantViewModel;
@property(nonatomic,strong) UserListHeadView *headView;
@end

@implementation MapsUserListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self.beenViewModel;
    self.tableView.dataSource = self.beenViewModel;
    self.tableView.separatorStyle = 0;
    self.navigationItem.titleView = self.headView;
    [[NSNotificationCenter defaultCenter] addObserver:self.beenViewModel selector:@selector(reloadData) name:BJNeedReloadUserInformationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.wantViewModel selector:@selector(reloadData) name:BJNeedReloadUserInformationNotification object:nil];
    
    // Do any additional setup after loading the view.
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self.beenViewModel];
    [[NSNotificationCenter defaultCenter] removeObserver:self.wantViewModel];
}
- (MapsUserListViewModel *)beenViewModel{
    if(_beenViewModel == nil){
        _beenViewModel = [[MapsUserListViewModel alloc] initWithModel:self.model listType:USERLISTTYPE_BEEN table:self.tableView];
        _beenViewModel.delegate = self;
    }
    return _beenViewModel;
}
- (MapsUserListViewModel *)wantViewModel{
    if(_wantViewModel == nil){
        _wantViewModel = [[MapsUserListViewModel alloc] initWithModel:self.model listType:USERLISTTYPE_WANT table:self.tableView];
        _wantViewModel.delegate = self;
    }
    return _wantViewModel;
}
- (UserListHeadView *)headView{
    if(_headView == nil){
        _headView = [[UserListHeadView alloc] initWithFrame:CGRectMake(0, 0, 130, 44)];
        __weak typeof(self)weakSelf = self;
        _headView.selectAtIndex = ^(NSInteger index){
            __strong typeof(weakSelf) strongSelf= weakSelf;
            if(index == 0){
                strongSelf.tableView.delegate = strongSelf.beenViewModel;
                strongSelf.tableView.dataSource = strongSelf.beenViewModel;
            }else{
                strongSelf.tableView.delegate = strongSelf.wantViewModel;
                strongSelf.tableView.dataSource = strongSelf.wantViewModel;
            }
            [strongSelf.tableView reloadData];
        };
    }
    return _headView;
}
#pragma mark - MapsUserListViewModelDelegate
- (void)toUserInfor:(User *)user{
    MeHomeViewController *userInfor = [[MeHomeViewController alloc] init];
    userInfor.currentUser = user;
    userInfor.userInforType = USERINFOR_OTHER;
    [self push:userInfor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
