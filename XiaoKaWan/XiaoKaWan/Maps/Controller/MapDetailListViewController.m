//
//  MapDetailListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapDetailListViewController.h"
#import "MapsDetailListViewModel.h"
#import "BJMapsListCollectionLayout.h"
#import <MJRefresh.h>
#import "MapsUserListViewController.h"
#import "MapsPlaceListViewController.h"
#import "MapsNetworkManage.h"
#import "PlacePostViewController.h"
#import "BJNotificationConfig.h"
#import "PlacePostDetailViewController.h"
#import "UIColor+colorWithHex.h"
#import "MapsPlaceDetailViewController.h"
#import "BJShareTool.h"
#import "BJAlertController.h"
@interface MapDetailListViewController ()<MapsDetailListViewModelDelegate>
@property(nonatomic,strong) MapsDetailListViewModel *modelView;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong) BJMapsListCollectionLayout *collectionLayout;
@end

@implementation MapDetailListViewController{
    MapsDetail *mapsDetail;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:_modelView selector:@selector(reloadData) name:BJMapsDetailNeedReloadDataNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:_modelView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self collectionView];
}
- (void)rightClick{
    NSInteger currentId = [[UserConfig sharedInstance].userId integerValue];
    NSInteger userId = mapsDetail.createUserId;
    if(userId == currentId){
        [self shareMaps];
    }else{
        [self followMaps];
    }
   
}

-(void)rightClick1
{
    [self shareMaps];
}
- (void)shareMaps{
   
    [BJShareTool shareAlbumWithTypeViewController:self.navigationController mapsID:self.mapObject.id];
}
- (void)followMaps{
    NSString *title;
    NSString *butonTitle;
    if(self.mapObject.isFollow){
        title = @"取消关注此榜单";
        butonTitle = @"取消关注";
    }else{
        title = @"关注此榜单";
        butonTitle = @"关注";
    }
    BJAlertController *alertVc = [BJAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertVc addDestructiveActionWithTitle:butonTitle handler:^{
        [[MapsNetworkManage sharedInstance] followWithParameter:self.mapObject complete:^(BOOL succeed, id obj) {
            if(succeed ){
                [[NSNotificationCenter defaultCenter] postNotificationName:BJMapsChangedNotification object:nil];
                mapsDetail.isFollowed = !mapsDetail.isFollowed;
                [self isFollow:mapsDetail];
                [[NSNotificationCenter defaultCenter] postNotificationName:BJMapsDetailNeedReloadDataNotification object:nil];
            }else{
                [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
            }
        }];
    }];
    [alertVc addActionWithTitle:@"取消" handler:nil];
    [self presentViewController:alertVc animated:YES completion:nil];
}
- (MapsDetailListViewModel *)modelView{
    if(_modelView==nil){
        _modelView = [[MapsDetailListViewModel alloc] initWithModel:self.mapObject andCollectionView:_collectionView];
        _modelView.delegate = self;
    }
    return _modelView;
}
- (BJMapsListCollectionLayout *)collectionLayout
{
    if(_collectionLayout == nil){
        _collectionLayout = [[BJMapsListCollectionLayout alloc] init];
        _collectionLayout.delegate = self.modelView;
        _collectionLayout.isStickyHeader = YES;
    }
    return _collectionLayout;
}
- (UICollectionView *)collectionView
{
    if(_collectionView == nil){
        UICollectionViewLayout *layOut = [[UICollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64) collectionViewLayout:layOut];
        _collectionView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self.view addSubview:_collectionView];
        _collectionView.collectionViewLayout = self.collectionLayout;
        _collectionView.delegate = self.modelView;
        _collectionView.dataSource = self.modelView;

    }
    return _collectionView;
}
- (void)dealloc
{
    _collectionLayout = nil;
}
#pragma mark - MapsDetailListViewModelDelegate
- (void)gotoBeenUserListWithData:(Place *)place{
//    [self gotoUersList:USERLISTTYPE_BEEN place:place];
}
- (void)gotoWantUserListWithData:(Place *)place{
//    [self gotoUersList:USERLISTTYPE_WANT place:place];
}
- (void)toUserList:(Place *)place{
    MapObject *mapsObj = [MapObject mapObject];
    mapsObj.placeId = place.placeId;
    MapsUserListViewController *userListVc = [[MapsUserListViewController alloc] init];
    userListVc.model = mapsObj;
    [self push:userListVc];
}

- (void)gotoPlace{
    MapsPlaceListViewController *mapsPlaceVc = [[MapsPlaceListViewController alloc] init];
    [self push:mapsPlaceVc];
}
- (void)gotoPost{
    PlacePostViewController *postVc = [[PlacePostViewController alloc] init];
    postVc.mapsObject = self.mapObject;
    [self push:postVc];
}
- (void)isFollow:(MapsDetail*)mapsDetail1{
    NSInteger currentId = [[UserConfig sharedInstance].userId integerValue];
    NSInteger userId = mapsDetail1.createUserId;
    mapsDetail = mapsDetail1;
    if(userId == currentId){
        [self rightButtonWithIconfont:@"\U0000e623"];
    }else{
    self.mapObject.isFollow = mapsDetail1.isFollowed;
    if(mapsDetail.isFollowed){
        [self rightButtonWithIconfont:@"\U0000e626" color1:0xCB2929 title1:@"\U0000e623" color2:0x000000];
    
    }else [self rightButtonWithIconfont:@"\U0000e626" color1:0x000000 title1:@"\U0000e623" color2:0x000000];
    }
}
- (void)toPostDetail:(PostObjet *)postObject{
    PostParatemer *parameter = [PostParatemer parameter];
    parameter.post_id = [NSString stringWithFormat:@"%li",(long)postObject.id];
    PlacePostDetailViewController *postDetailVc = [[PlacePostDetailViewController alloc] init];
    postDetailVc.parameter = parameter;
    [self push:postDetailVc];
}
- (void)toPlaceDetail:(Place *)place{
    MapsPlaceDetailViewController *placeDetail = [[MapsPlaceDetailViewController alloc] init];
    placeDetail.place = place;
    [self push:placeDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
