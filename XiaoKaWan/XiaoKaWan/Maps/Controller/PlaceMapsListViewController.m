//
//  PlaceMapsListViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceMapsListViewController.h"
#import "PlaceMapsViewModel.h"
@interface PlaceMapsListViewController ()
@property(nonatomic,strong)PlaceMapsViewModel *viewModel;
@end

@implementation PlaceMapsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tableView.separatorStyle = 0;
    // Do any additional setup after loading the view.
}
- (PlaceMapsViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[PlaceMapsViewModel alloc] initWithModel:nil controller:self];
    }
    return _viewModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
