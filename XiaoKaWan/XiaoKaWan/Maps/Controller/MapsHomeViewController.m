//
//  MapsHomeViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsHomeViewController.h"
#import "MapsHomeViewModel.h"
#import "MapDetailListViewController.h"
//#import "BJNavigationController.h"
@interface MapsHomeViewController ()<MapsHomeViewModelDelegate>
@property(nonatomic,strong) MapsHomeViewModel *viewModel;
@end

@implementation MapsHomeViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.hidesBottomBarWhenPushed = NO;
//    [self.tabBarController.tabBar setHidden:NO];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//   [self.tabBarController.tabBar setHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//     self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = NSLocalizedString(@"mainPage_maps", nil);
    _viewModel = [[MapsHomeViewModel alloc] initWithModel:nil andTabel:self.tableView];
    _viewModel.delegate = self;
    self.tableView.delegate =_viewModel;
    self.tableView.dataSource = _viewModel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark -MapsHomeViewModelDelegate
- (void)touchOneMapsWithModel:(id)model
{
    MapDetailListViewController *detailList = [[MapDetailListViewController alloc] init];
    detailList.mapObject = model;
    [self push:detailList];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
