//
//  PlacePostViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "MapObject.h"
@interface PlacePostViewController : BJSuperViewController
@property(nonatomic,strong) MapObject *mapsObject;
@end
