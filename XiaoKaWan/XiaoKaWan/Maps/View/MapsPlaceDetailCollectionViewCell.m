//
//  MapsPlaceDetailCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceDetailCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import "PlaceCategory.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@implementation MapsPlaceDetailCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    
//        [self lineView];
        self.backgroundColor = [UIColor whiteColor];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    self.beenWantView.place = place;
    self.titleLabel.text = place.name;
    self.addressLabel.text = place.addressDetail;
    NSString *content;
    if(self.place.content.length>50) content = [NSString stringWithFormat:@"%@...",[self.place.content substringToIndex:50]];
    else content = self.place.content;
    self.contentLab.text = content;
    self.categoryLb.text = place.category_name;
    NSString *categoryId;
    if(place.sub_category_id.length)categoryId = place.sub_category_id;
    else categoryId = place.category_id;
    [self.cagegoryIcon setImageWithUrlString:[NSString stringWithFormat:@"%@%@.png",[PlaceCategory iconUrlConfig],categoryId]];
    [self.segmentView setNumber1:place.postCount];
    [self.segmentView setNumber2:place.mapsCount];

}
//-(void)setObject:(PostListObj *)object{
//}
- (void)updateConstraints{
    [super updateConstraints];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(20);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.width.equalTo(@300);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.width.lessThanOrEqualTo(@(kDeviceWidth-60));
    }];
    [self.addressIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.addressLabel.mas_right).offset(10);
//        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
       
    }];
    [self.cagegoryIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.top.equalTo(self.addressIcon.mas_bottom).offset(10);
        make.height.equalTo(@14);
        make.width.equalTo(@14);
    }];
    [self.categoryLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cagegoryIcon.mas_right).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.top.equalTo(self.addressIcon.mas_bottom).offset(11);
    }];
    
    [self.contentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-130);
        make.top.equalTo(self.cagegoryIcon.mas_bottom).offset(10);
    }];
   
    [self.beenWantView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLab.mas_bottom).offset(20);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@30);
    }];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.beenWantView.mas_bottom).offset(30);
        make.left.equalTo(self.mas_left).offset(20);
        make.width.equalTo(@SEGMENTWIDTH);
        make.height.equalTo(@SEGMENTHEIGHT);
    }];
}

- (UILabel*)titleLabel{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:30 color:0x555555];
        _titleLabel.text = @"猫屎咖啡";
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)addressLabel{
    if(_addressLabel==nil){
        _addressLabel = [UILabel creatLabelWithFont:11 color:0xB7B7B7];
        _addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_addressLabel];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAddress:)];
        [_addressLabel addGestureRecognizer:tap];
        _addressLabel.userInteractionEnabled = YES;
    }
    return _addressLabel;
}
- (void)tapAddress:(UITapGestureRecognizer *)tapGesture{
    [self.delegate mapNavigation];
}
- (UILabel *)addressIcon{
    if(_addressIcon == nil){
        _addressIcon = [UILabel creatLabelWithFont:11 color:0xB7B7B7];
        _addressIcon.font = [UIFont fontWithName:@"iconfont" size:11];
        [self.contentView addSubview:_addressIcon];
        _addressIcon.text = @"\U0000e62a";
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAddress:)];
        [_addressIcon addGestureRecognizer:tap];
        _addressIcon.userInteractionEnabled = YES;
    }
    return _addressIcon;
}
- (UILabel *)contentLab{
    if(_contentLab == nil){
        _contentLab = [UILabel creatLabelWithFont:12 color:0x666666];
        _contentLab.numberOfLines = 0;
        [self.contentView addSubview:_contentLab];
    }
    return _contentLab;
}
- (BJUIImageView *)cagegoryIcon{
    if(_cagegoryIcon == nil){
        _cagegoryIcon = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_cagegoryIcon];
    }
    return _cagegoryIcon;
}
- (UILabel *)categoryLb{
    if(_categoryLb == nil){
        _categoryLb = [UILabel creatLabelWithFont:11 color:0x202020];
        [self.contentView addSubview:_categoryLb];
    }
    return _categoryLb;
}
- (BeenWantView*)beenWantView
{
    if(_beenWantView==nil){
        _beenWantView = [[BeenWantView alloc] init];
        _beenWantView.delegate = self;
        [self.contentView addSubview:_beenWantView];
        _beenWantView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userListGesture:)];
        [_beenWantView addGestureRecognizer:tapGesture];
    }
    return _beenWantView;
}
- (void)userListGesture:(UIGestureRecognizer *)gesture{
    [self.delegate userList:self.place];
}
- (void)moreDetail:(UIButton *)button{
    [self.delegate moreDetailContent:_place.content];
}
- (MapsPlaceSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView = [[MapsPlaceSegmentView alloc] init];
        __weak typeof(self) weakSelf = self;
        _segmentView.segmentIndex = ^(NSInteger index){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate selectAtIndex:index];
        };
        [self addSubview:_segmentView];
    }
    return _segmentView;
}
- (void)setTitles:(NSArray *)titles{
    self.segmentView.titles =titles;
}
#pragma mark - BeenWantViewDelegate

- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate wantGoAction:place callBack:place1];
}



@end
