//
//  MapsHomeCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsHomeCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "MapsUser.h"
@implementation MapsHomeCell
+ (instancetype)initCellWithTabel:(UITableView *)tableView
{
    static NSString *cellId = @"MapsHomeCellId";
    MapsHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell){
        cell = [[MapsHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self collectView];
    }
    return self;
}
- (void)setIndexPath:(NSIndexPath *)indexPath
{
//    [_collectView reloadData];
    _indexPath = indexPath;
}
- (void)setMaps:(Maps *)maps
{
    [self setNeedsUpdateConstraints];
    self.titleLabel.text = maps.name;
    [self setTitle:@"\U0000e600" withLabel:self.followNumberIcon];
    [self setTitle:@"\U0000e602" withLabel:self.placeNumberIcon];
    [self setTitle:@"\U0000e607" withLabel:self.dynamicNumberIcon];
    self.followNumberLabel.text =[NSString stringWithFormat:@"%@",maps.follow_count];
    self.placeNumberLabel.text =[NSString stringWithFormat:@"%@",maps.place_count];
    self.dynamicNumberLabel.text =[NSString stringWithFormat:@"%@",maps.post_count];
    [self collectView];
    self.modelView.indexPath = self.indexPath;
    CollectionObject *collectionObject = [[CollectionObject alloc] init];
    collectionObject.id = [NSString stringWithFormat:@"%@",maps.id];
    collectionObject.photoData = maps.photos;
    self.modelView.collectionObject = collectionObject;
    [self intHeadViewData:maps];
}
- (void)intHeadViewData:(Maps *)maps{
//    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"uid" ascending:YES];
    NSArray *usersArray  = [MapsUser mapsUserMapsId:maps.idValue];//[maps.mapsUserSet sortedArrayUsingDescriptors:@[sort]];
    NSMutableArray *userHeadUrl  = [NSMutableArray arrayWithCapacity:0];
    [usersArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MapsUser *user = (MapsUser *)obj;
        NSLog(@"name:%@,url:%@",user.nick,user.avatar);
        [userHeadUrl addObject:user.avatar==nil?@" ":user.avatar];
    }];
   
    [self.groupHeadView setImageUrlArray:userHeadUrl];
//    self.groupHeadView.backgroundColor = [UIColor redColor];
    self.groupHeadView.countLimit = 4;
    self.groupHeadView.totalNumber = [maps.user_count integerValue];
    self.groupHeadView.headViewType = HEADVIEWTYPE_SHOWNUMBER;
    NSInteger imageCount =[maps.user_count integerValue]; //userHeadUrl.count;
    if(imageCount>4) imageCount= 4;
    CGFloat with = imageCount*30+8*(imageCount-1);
    self.groupHeadView.frame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-(with+10), 34, with, 30);

}
- (void)updateConstraints
{
    [super updateConstraints];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(10);
    }];
    [self.followNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.height.equalTo(@15);
    }];
    [self.followNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.followNumberIcon.mas_right);
        make.height.equalTo(@15);
    }];
    [self.placeNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.followNumberLabel.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.placeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.placeNumberIcon.mas_right);
        make.height.equalTo(@15);
    }];
    [self.dynamicNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.placeNumberLabel.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.dynamicNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.dynamicNumberIcon.mas_right);
        make.height.equalTo(@15);
    }];
//    [self.groupHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView.mas_bottom).offset(34);
//        make.right.equalTo(self.contentView.mas_right).offset(-10);
//        make.width.equalTo(@150);
//        make.height.equalTo(@30);
//    }];
//    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.loactionNumberLabel.mas_bottom).offset(10);
//        make.left.equalTo(self.contentView.mas_left).offset(0);
//        make.right.equalTo(self.contentView.mas_right).offset(0);
//        make.height.equalTo(@0.5);
//    }];
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dynamicNumberLabel.mas_bottom).offset(25);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-73);
    }];
    
}
- (void)setTitle:(NSString *)title withLabel:(UIButton *)label{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:title];
    [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:15],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xb2b2b2]} range:NSMakeRange(0, title.length)];
    [label setAttributedTitle:attStr forState:UIControlStateNormal];
    //    [self.placeNumberLabel setImage:image forState:UIControlStateNormal];
//    [label setTitleColor:[UIColor p_colorWithHex:0x737373] forState:UIControlStateNormal];
}


- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:17 color:0x4c4c4c];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)placeNumberLabel
{
    if(_placeNumberLabel==nil){
        _placeNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
        [self.contentView addSubview:_placeNumberLabel];
    }
    return _placeNumberLabel;
}
- (UILabel*)followNumberLabel
{
    if(_followNumberLabel==nil){
        _followNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
        [self.contentView addSubview:_followNumberLabel];
    }
    return _followNumberLabel;
}
- (UILabel*)dynamicNumberLabel
{
    if(_dynamicNumberLabel==nil){
        _dynamicNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
        [self.contentView addSubview:_dynamicNumberLabel];
    }
    return _dynamicNumberLabel;
}
- (UIButton*)placeNumberIcon
{
    if(_placeNumberIcon==nil){
        _placeNumberIcon = [[UIButton alloc] init];
        _placeNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
        _placeNumberIcon.selected = NO;
        [self.contentView addSubview:_placeNumberIcon];
        
    }
    return _placeNumberIcon;
}
- (UIButton *)dynamicNumberIcon
{
    if(_dynamicNumberIcon == nil){
        _dynamicNumberIcon = [[UIButton alloc] init];
        _dynamicNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
        _dynamicNumberIcon.selected = NO;
        [self.contentView addSubview:_dynamicNumberIcon];
    }
    return _dynamicNumberIcon;
}
- (UIButton*)followNumberIcon
{
    if(_followNumberIcon==nil){
        _followNumberIcon = [[UIButton alloc] init];
        _followNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
        _followNumberIcon.selected = NO;
        [self.contentView addSubview:_followNumberIcon];
    }
    return _followNumberIcon;
}
- (BJGroupHeadImageView*)groupHeadView
{
    if(_groupHeadView==nil){
        _groupHeadView = [[BJGroupHeadImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-160, 34, 150, 30)];
        [self.contentView addSubview:_groupHeadView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchUserGroup:)];
        [_groupHeadView addGestureRecognizer:tapGesture];
    }
    return _groupHeadView;
}
- (UIView*)line
{
    if(_line==nil){
        _line = [[UIView alloc] init];
        _line.backgroundColor =[UIColor blackColor];
        [self.contentView addSubview:_line];
    }
    return _line;
}
- (UICollectionView*)collectView
{
    if(_collectView==nil){
        UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc] init];
//       viewLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:viewLayout];
        _collectView.scrollEnabled = NO;
        _collectView.delegate = self.modelView;
        _collectView.dataSource = self.modelView;
        _collectView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.layer.cornerRadius = 10;
        _collectView.layer.masksToBounds = YES;
        [self.contentView addSubview:_collectView];
    }
    return _collectView;
}
- (MapsHomeCellViewModel *)modelView
{
    if(_modelView == nil){
        _modelView = [[MapsHomeCellViewModel alloc] initWithModel:nil andCollcetionView:_collectView];
        _modelView.delegate = self;
    }
    
    return _modelView;
}
- (void)touchUserGroup:(UITapGestureRecognizer *)gesture{
    
}
#pragma mark- MapsHomeCellViewModelDelegate
- (void)touchCollectionWithModel:(id)model
{
    [self.delegate touchMapsWithModel:model];
}
#pragma mark - touch 
//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    [UIView animateWithDuration:0.2 animations:^{
//        self.transform = CGAffineTransformMakeScale(0.9, 0.9);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.1 animations:^{
//            self.transform = CGAffineTransformIdentity;
//        } completion:^(BOOL finished) {
////            [self touchCollectionWithModel:self.maps];
//        }];
//    }];
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
