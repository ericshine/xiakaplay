//
//  PlacePostDetailHeadFootView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailHeadFootView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@implementation PlacePostDetailHeadFootView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self commentButton];
        [self likeButton];
//        [self setNeedsUpdateConstraints];
    }
    return self;
}
//- (void)updateConstraints{
//    [super updateConstraints];
//    [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(10);
//        make.top.equalTo(self.mas_top).offset(0);
//    }];
//    [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.commentButton.mas_right).offset(10);
//         make.top.equalTo(self.mas_top).offset(0);
//    }];
//}
- (void)setPostDetail:(PostDetailObject *)postDetail{
    NSString *coment;
    NSString *like;
    if([postDetail.commentCount integerValue]>1000)coment = @"评论 999+";
    else coment = [NSString stringWithFormat:@"评论 %@",postDetail.commentCount];
    if([postDetail.likeCount integerValue]>1000)like = @"点赞 999+";
    else like = [NSString stringWithFormat:@"点赞 %@",postDetail.likeCount];
    
    [_commentButton setTitle:coment forState:UIControlStateNormal];
    [_likeButton setTitle:like forState:UIControlStateNormal];
}
- (UIButton *)commentButton{
    if(_commentButton == nil){
        _commentButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, 60,44)];
        _commentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _commentButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_commentButton setTitleColor:[UIColor p_colorWithHex:0x393939] forState:UIControlStateNormal];
//        [_commentButton setTitle:@"评论 999+" forState:UIControlStateNormal];
        [_commentButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        _commentButton.tag = 60;
        [self addSubview:_commentButton];
    }
    return _likeButton;
}
- (UIButton *)likeButton{
    if(_likeButton == nil){
        _likeButton = [[UIButton alloc] initWithFrame:CGRectMake(70, 0, 60, CGRectGetHeight(self.frame))];
        _likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _likeButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_likeButton setTitleColor:[UIColor p_colorWithHex:0x393939] forState:UIControlStateNormal];
//        [_likeButton setTitle:@"赞 999+" forState:UIControlStateNormal];
        _likeButton.tag = 61;
        [_likeButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_likeButton];
    }
    return _likeButton;
}
- (void)buttonAction:(UIButton *)button{
    [self.delegate selelctSegmentType:(button.tag==60)?COMMENT_TYPE:LIKE_TYPE];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
