//
//  MapsPlaceDetailCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/7/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
#import "BJUIImageView.h"
#import "BeenWantView.h"
#import "Place.h"
#import "MapsPlaceSegmentView.h"
#import "PostListObj.h"
#import "BJUIImageView.h"
@protocol MapsPlaceDetailCollectionViewCellDelegate <NSObject>

- (void)wantGoAction:(Place *)place callBack:(void(^)(Place *place))place1;
- (void)beenAction:(Place *)place callBack:(void(^)(Place *place))place1;
- (void)moreDetailContent:(NSString*)content;
- (void)userList:(Place*)place;

- (void)selectAtIndex:(NSInteger)index;
- (void)mapNavigation;
@end
@interface MapsPlaceDetailCollectionViewCell : UICollectionViewCell<BeenWantViewDelegate>
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *addressLabel;
@property(nonatomic,strong) UILabel *addressIcon;
//@property(nonatomic,strong) UIButton *moreBUtton;

@property(nonatomic,strong) UILabel *contentLab;
@property(nonatomic,strong) BJUIImageView *cagegoryIcon;
@property(nonatomic,strong) UILabel *categoryLb;
@property(nonatomic,strong) BeenWantView *beenWantView;
@property(nonatomic,strong) MapsPlaceSegmentView *segmentView;
@property(nonatomic,assign) id<MapsPlaceDetailCollectionViewCellDelegate>delegate;
@property(nonatomic,strong) Place *place;
@property(nonatomic,strong) PostListObj *object;
@property(nonatomic,strong) NSArray *titles;
@end

