//
//  UserListTableViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
#import "User.h"
@protocol UserListTableViewCellDelegate <NSObject>

- (void)followUser:(User*)user success:(void(^)(User *user))user;

@end
@interface UserListTableViewCell : UITableViewCell
@property(nonatomic,assign) id<UserListTableViewCellDelegate>delegate;
@property(nonatomic,strong) BJUIImageView *headImageView;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIButton *followBtn;
@property(nonatomic,strong) UIView *lineView;
@property(nonatomic,strong) User *user;
@end

