//
//  MapsDetailPlaceHeadCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
#import "MapsPlaceSegmentView.h"
#import "BJTitleNumberButton.h"
#import "MapsDetail.h"
@protocol MapsDetailPlaceHeadCollectionViewCellDelegate <NSObject>

- (void)placeTouchAction;
- (void)postTouchAction;

@end
@interface MapsDetailPlaceHeadCollectionViewCell : UICollectionViewCell
@property(nonatomic,assign) id<MapsDetailPlaceHeadCollectionViewCellDelegate> delegate;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIButton *scanIcon;
@property(nonatomic,strong)UIButton *followIcon;
@property(nonatomic,strong)UILabel *scanLabel;
@property(nonatomic,strong)UILabel *followLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)BJGroupHeadImageView *headGroupView;
@property(nonatomic,strong)UILabel *placeNumberLb;
//@property(nonatomic,strong)MapsPlaceSegmentView *segmentView;
@property(nonatomic,strong) MapsDetail *mapsObject;
@end
