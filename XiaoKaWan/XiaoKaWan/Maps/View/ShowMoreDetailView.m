//
//  ShowMoreDetailView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ShowMoreDetailView.h"
#import "NSString+Cagetory.h"
#import "UIColor+colorWithHex.h"
@interface ShowMoreDetailView()
@property(nonatomic,strong)UITextView *textView;
@property(nonatomic,strong)UIView *whiteBg;
@end
@implementation ShowMoreDetailView{
    UIView *bgView;
    
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        
        [self addSubview:bgView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
        [bgView addGestureRecognizer:tapGesture];
    }
    return self;
}
+ (ShowMoreDetailView *)showViewInView:(UIView *)view point:(CGRect)rect{
     ShowMoreDetailView *selfView = [[ShowMoreDetailView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    [view addSubview:selfView];
    return selfView;
}
- (void)gestureAction:(UITapGestureRecognizer*)tap{
   [self dissMiss];
}
- (UIView*)whiteBg{
    if(_whiteBg == nil){
        _whiteBg = [[UIView alloc] init];
        _whiteBg.layer.cornerRadius = 7;
        _whiteBg.backgroundColor = [UIColor whiteColor];
        [bgView addSubview:_whiteBg];
    }
    return _whiteBg;
}
- (UITextView *)textView{
    if(_textView == nil){
        _textView = [[UITextView alloc] init];
        _textView.layer.cornerRadius = 7;
        _textView.layer.masksToBounds = YES;
        _textView.font = [UIFont systemFontOfSize:14];
        _textView.textColor = [UIColor p_colorWithHex:0x434343];
        _textView.editable = NO;
        [bgView addSubview:_textView];
    }
    return _textView;
}
- (void)setContentString:(NSString *)contentString{
     self.textView.text = contentString;
    CGSize size = [contentString sizeWithFont:self.textView.font andSize:CGSizeMake(kDeviceWidth-82, 99999)];
    if(size.height>400) size.height = 400;
    else if(size.height<200) size.height = 200;
    [UIView animateWithDuration:0 animations:^{
        self.whiteBg.frame = CGRectMake((kDeviceWidth-size.width)/2, 141, size.width, size.height);
        self.whiteBg.center = CGPointMake(20, 150);
        self.whiteBg.transform = CGAffineTransformMakeScale(0, 0);
         [_whiteBg addSubview:self.textView];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
             bgView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.7];
            self.whiteBg.center = self.center;
            self.whiteBg.transform = CGAffineTransformMakeScale(1, 1);
            self.textView.frame = CGRectMake(0, 0, size.width, size.height);
        } completion:^(BOOL finished) {
            
           
        }];
    }];
    
    
//    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
//        
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
//            
//        } completion:^(BOOL finished) {
//            
//        }];
//    }];
//    [UIView animateWithDuration:0.3 animations:^{
//        CGPoint point = [self.textView convertPoint:CGPointMake(150, -150) toView:self];
//        self.textView.frame = CGRectMake(point.x, point.y, size.height, size.height);
//    }];
//     self.textView.frame = CGRectMake((kDeviceWidth-size.width)/2, 141, size.width, size.height);
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)dissMiss{
    [self removeFromSuperview];
}
@end
