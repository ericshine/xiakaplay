//
//  HeadSegmentCollectionView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "HeadSegmentCollectionView.h"
#import <Masonry.h>
@implementation HeadSegmentCollectionView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left).offset(20);
        make.width.equalTo(@SEGMENTWIDTH);
        make.height.equalTo(@SEGMENTHEIGHT);
    }];
}
- (void)setPostObject:(PostListObj *)postObject{
    self.segmentView.number1 = postObject.postCount;
    self.segmentView.number2 = postObject.maspCount;
}
- (void)setTitles:(NSArray *)titles{
    self.segmentView.titles =titles;
}
- (MapsPlaceSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView = [[MapsPlaceSegmentView alloc] init];
        __weak typeof(self) weakSelf = self;
        _segmentView.segmentIndex = ^(NSInteger index){
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate selectAtIndex:index];
        };
        [self addSubview:_segmentView];
    }
    return _segmentView;
}
@end
