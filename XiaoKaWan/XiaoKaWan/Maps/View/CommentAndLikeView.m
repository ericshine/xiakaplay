//
//  CommentAndLikeView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "CommentAndLikeView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@implementation CommentAndLikeView
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@1);
        
    }];
    [self.commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.mas_left).offset(15);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        
    }];
    [self.likeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.commentTextView.mas_right);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.right.equalTo(self.mas_right).offset(-5);
        make.width.equalTo(@50);
    }];
}
- (BJPlaceholderTextView*)commentTextView
{
    if(_commentTextView==nil){
        _commentTextView = [[BJPlaceholderTextView alloc] init];
        _commentTextView.layer.cornerRadius = 15;
        _commentTextView.layer.masksToBounds = YES;
        _commentTextView.layer.borderColor = [UIColor p_colorWithHex:0xcbcbcb].CGColor;
        _commentTextView.layer.borderWidth = 1;
        _commentTextView.returnKeyType = UIReturnKeySend;
        _commentTextView.delegate = self;
         _commentTextView.font = [UIFont systemFontOfSize:12];
        _commentTextView.placeHolder = @"写评论:";
        [self addSubview:_commentTextView];
    }
    return _commentTextView;
}
- (BJIconfontButton*)likeView
{
    if(_likeView==nil){
        _likeView = [[BJIconfontButton alloc] init];
        [_likeView setTitle:@"点赞" forState:UIControlStateNormal];
        [_likeView setTitle:@"已赞" forState:UIControlStateSelected];
        [_likeView setTitleColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
        [_likeView setIcon:@"\U0000e611" forState:UIControlStateNormal];
        [_likeView setIcon:@"\U0000e601" forState:UIControlStateSelected];
        [_likeView setIconColor:[UIColor mainColor] forState:UIControlStateSelected];
        [_likeView setIconColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
        [_likeView addTartget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_likeView];
    }
    return _likeView;
}
- (UIView*)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor =[UIColor p_colorWithHex:0xcbcbcb];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (void)likeAction:(UIButton*)button{
    [self.delegate likeAction:^(BOOL success) {
        if(success){
            _likeView.titleButton.selected = !_likeView.titleButton.selected;
            _likeView.iconButton.selected = !_likeView.iconButton.selected;
        }
    }];
}
- (void)setLikeState:(BOOL)likeState{
    _likeView.titleButton.selected = likeState;
    _likeView.iconButton.selected = likeState;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [self.commentTextView resignFirstResponder];
        [self.delegate commentWithContent:self.commentTextView.text];
    }
    return YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
