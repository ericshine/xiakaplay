//
//  MapsDetailDynamicCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetailDynamicCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import <UIImageView+WebCache.h>
#import "UIColor+colorWithHex.h"
#import "PostFrameObject.h"
static NSInteger const KheadImagecornerRadius = 15;
@interface MapsDetailDynamicCollectionViewCell ()
@property(nonatomic,strong)UIView *bgView;
@end
@implementation MapsDetailDynamicCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self bgView];
        [self updateFrame];
     self.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
    }
    return self;
}
- (void)setPostObject:(PostObjet *)postObject{
    
    [self.dynamicImageView setImageWithUrlString:[ImageSizeUrl imagef480_url:postObject.latest_image.url]];
    self.nameLabel.text = postObject.post_user.nick;
    [self.headImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar60:postObject.post_user.avatar]];
//    self.addressLabel.text = postObject.placeInfo.name;
    self.contentLabel.text = postObject.content;
    [self.likeLabel setAttributedTitle:[self getAttString:[NSString stringWithFormat:@"\U0000e611 %li",(long)postObject.like_count]] forState:UIControlStateNormal];
 [self.commitLabel setAttributedTitle:[self getAttString:[NSString stringWithFormat:@"\U0000e609 %li",(long)postObject.like_count]] forState:UIControlStateNormal];
    if(postObject.image_count>1) self.multiImageLb.text = @"多图";
    else self.multiImageLb.text = @"";
}
- (NSMutableAttributedString*)getAttString:(NSString*)string{
  NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:string];
    [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:9],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x94999a]} range:NSMakeRange(0, 1)];
//    [attStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x94999a]} range:NSMakeRange(1, attStr.length)];
    return attStr;
}
-(void)updateFrame
{
    
//    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self.contentView.mas_bottom).offset(-1);
//        make.left.equalTo(self.contentView.mas_left).offset(0);
//        make.right.equalTo(self.contentView.mas_right);
//        make.height.equalTo(@0.5);
//    }];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.top.equalTo(self.contentView.mas_top).offset(20);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
//    [self.addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self.bgView.mas_bottom).offset(-10);
//        make.left.equalTo(self.bgView.mas_left).offset(10);
//        make.right.equalTo(self.bgView.mas_right).offset(-10);
//        
//    }];
    
    [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-10);
        make.left.equalTo(self.bgView.mas_left).offset(10);
         make.right.equalTo(self.bgView.mas_right).offset(-10);
    }];
    
    [self.headImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentLabel.mas_top).offset(-10);
        make.left.equalTo(self.bgView.mas_left).offset(10);
        make.height.equalTo(@(KheadImagecornerRadius*2));
        make.width.equalTo(@(KheadImagecornerRadius*2));
        }];
    
    [self.likeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headImageView.mas_bottom).offset(5);
        make.left.equalTo(self.headImageView.mas_right);
        
    }];
    [self.commitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headImageView.mas_bottom).offset(5);
        make.left.equalTo(self.likeLabel.mas_right);
        
    }];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.likeLabel.mas_top);
        make.left.equalTo(self.headImageView.mas_right).offset(5);
    }];
        [self.dynamicImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.bgView.mas_top);
                make.left.equalTo(self.bgView.mas_left);
                make.right.equalTo(self.bgView.mas_right);
                make.bottom.equalTo(self.headImageView.mas_top).offset(-7);
            make.top.equalTo(self.bgView.mas_top);
            }];
    [self.multiImageLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.dynamicImageView.mas_right);
        make.bottom.equalTo(self.dynamicImageView.mas_bottom);
        //        make.height.equalTo(@14);
        //        make.width.equalTo(@31);
    }];
    
}
- (UIView*)bgView{
    if(_bgView == nil){
        _bgView = [[UIView alloc] init];
        _bgView.layer.cornerRadius = 5;
        _bgView.layer.masksToBounds = YES;
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}
- (BJUIImageView*)dynamicImageView
{
    if(_dynamicImageView==nil){
        _dynamicImageView = [[BJUIImageView alloc] init];
        //_dynamicImageView.layer.masksToBounds = NO;
//        _dynamicImageView.image = [UIImage imageNamed:@"image2"];
//        _dynamicImageView.layer.cornerRadius = 6;
//        _dynamicImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_dynamicImageView];
    }
    return _dynamicImageView;
}
- (BJUIImageView*)headImageView
{
    if(_headImageView==nil){
        _headImageView = [[BJUIImageView alloc] init];
        [self.bgView addSubview:_headImageView];
    }
    return _headImageView;
}
- (UILabel*)nameLabel
{
    if(_nameLabel==nil){
        _nameLabel = [UILabel creatLabelWithFont:10 color:0x424242];
//        _nameLabel.text = @"韩鹿";
        [self.bgView addSubview:_nameLabel];
    }
    return _nameLabel;
}
- (UILabel*)addressLabel
{
    if(_addressLabel==nil){
        _addressLabel = [UILabel creatLabelWithFont:9 color:0xd1d1d1];
//        _addressLabel.text = @"南京东路南京东路南京东路南京东路南京东路";
        [self.bgView addSubview:_addressLabel];
    }
    return _addressLabel;
}
- (UIButton*)likeLabel
{
    if(_likeLabel==nil){
        _likeLabel = [[UIButton alloc] init];
        _likeLabel.titleLabel.font = [UIFont fontWithName:@"iconfont" size:9];
        [_likeLabel setTitleColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
//        [_likeLabel setTitle:@"\U0000e61f9K+" forState:UIControlStateNormal];
        [self.bgView addSubview:_likeLabel];
    }
    return _likeLabel;
}
- (UIButton*)commitLabel
{
    if(_commitLabel==nil){
        _commitLabel = [[UIButton alloc] init];
        _commitLabel.titleLabel.font = [UIFont fontWithName:@"iconfont" size:9];
        [_commitLabel setTitleColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
//        [_commitLabel setTitle:@"12K+" forState:UIControlStateNormal];
        [self.bgView addSubview:_commitLabel];
    }
    return _commitLabel;
}
- (UILabel*)contentLabel
{
    if(_contentLabel==nil){
        _contentLabel = [UILabel creatLabelWithFont:[PostFrameObject postContentFont] color:0x737373];
        _contentLabel.numberOfLines = 2;
        [self.bgView addSubview:_contentLabel];
    }
    return _contentLabel;
}
- (UIView*)lineView
{
    if(_lineView==nil){
        _lineView = [[UIView alloc] init];
//        [self.contentView addSubview:_lineView];
        _lineView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:0.5];
    }
    return _lineView;
}
- (UILabel *)multiImageLb{
    if(_multiImageLb == nil){
        _multiImageLb = [UILabel creatLabelWithFont:10 color:0xffffff];
        _multiImageLb.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _multiImageLb.textAlignment = NSTextAlignmentCenter;
        _multiImageLb.layer.cornerRadius = 3;
        _multiImageLb.layer.masksToBounds = YES;
        [self.dynamicImageView addSubview:_multiImageLb];
    }
    return _multiImageLb;
}

@end
