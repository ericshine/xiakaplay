//
//  ShowMoreDetailView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowMoreDetailView : UIView
@property(nonatomic,strong) NSString *contentString;
+ (ShowMoreDetailView *)showViewInView:(UIView *)view point:(CGRect)rect;
- (void)dissMiss;
@end
