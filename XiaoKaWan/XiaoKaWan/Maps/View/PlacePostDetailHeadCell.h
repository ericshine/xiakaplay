//
//  PlacePostDetailHeadCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlacePostLocationView.h"
#import "PlaceFlagButton.h"
#import "PostDetailObject.h"
#import "BJUIImageView.h"
#import "BeenWantView.h"
#import "Place.h"
@protocol PlacePostDetailHeadCellDelegate <NSObject>

- (void)beenAction:(Place*)place callBack:(void (^)(Place *place))place1;
- (void)wantGoAction:(Place *)place callBack:(void(^)(Place* place))place1;
- (void)toUserList:(PostDetailObject *)object;
- (void)touserDetail:(User *)user;
- (void)toPlaceDetail:(Place *)place;
@end
@interface PlacePostDetailHeadCell : UITableViewCell
@property(nonatomic,strong)id<PlacePostDetailHeadCellDelegate>delegate;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)UIPageViewController *bigImageView;//展示大图
@property(nonatomic,strong)UICollectionView *smallImageView;//展示小图
@property(nonatomic,strong)UICollectionView *tagView;
@property(nonatomic,strong)PlacePostLocationView *addressView;
//@property(nonatomic,strong)PlaceFlagButton *beenFlagView;
//@property(nonatomic,strong)PlaceFlagButton *wantFlagView;
@property(nonatomic,strong)BeenWantView *beenWantView;
@property(nonatomic,strong)PostDetailObject *postObject;
//@property(nonatomic,strong)UILabel *noteLabel;
@end

