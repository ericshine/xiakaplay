//
//  UserListHeadView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserListHeadView.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"

@implementation UserListHeadView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self segmentView];
            }
    return self;
}
- (BJSegmentView *)segmentView{
    if(_segmentView == nil){
        _segmentView = [[BJSegmentView alloc] initWithItmes:@[@"去过",@"想去"] withAnimation:NO];
        [self addSubview:_segmentView];
    }
    __weak typeof(self) weakSelf = self;
    _segmentView.selectAtIndex = ^(NSInteger index){
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if(strongSelf.selectAtIndex)strongSelf.selectAtIndex(index);
    };
//    _segmentView.selectAtIndex = self.selectAtIndex;
    return _segmentView;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
