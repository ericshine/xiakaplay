//
//  PopSheetView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PopSheetView.h"
#define arror_w 10  // 顶部便偏移量
#define leftOffset 10  //箭头相对左边的偏移量
#define height 20
@implementation PopSheetView
{
    UIView *bgView;
    PopBgView *whiteView;
    
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        bgView =[[UIView alloc]initWithFrame:frame];
        [self addSubview:bgView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMiss)];
        [bgView addGestureRecognizer:tap];
        
        whiteView = [[PopBgView alloc] init];
        whiteView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        whiteView.selectAtIndex = ^(NSInteger index){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf disMiss];
            if(strongSelf.selectAtIndex)strongSelf.selectAtIndex(index);
        };
        [self addSubview:whiteView];
        
    }
    return self;
}
- (void)showInView:(UIView *)view fromPoint:(CGPoint)point withItems:(NSArray *)items{
    
    [view addSubview:self];
//    whiteView.frame = CGRectMake(point.x, point.y, 100, 10);
    CGFloat view_h = arror_w+5+items.count*(height+6);
    whiteView.frame = CGRectMake(point.x+20, point.y-20, 80, view_h);
     whiteView.items = items;
     whiteView.transform = CGAffineTransformMakeScale(0.3, 0.3);
    [UIView animateWithDuration:0.2 animations:^{
        bgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        whiteView.transform =  CGAffineTransformIdentity;
         whiteView.frame = CGRectMake(point.x, point.y, 80, view_h);
    }];

    
}
- (void)disMiss{
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

@implementation PopBgView{
    NSMutableArray *buttons;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        buttons = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}
- (void)setItems:(NSArray *)items{
    [buttons enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    [self creatItems:items];
}
- (void)creatItems:(NSArray *)items{
    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, arror_w+5+idx*(height+6), CGRectGetWidth(self.frame), height)];
        button.titleLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:button];
        button.tag = 40+idx;
        [button addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:obj forState:0];
        [button setTitleColor:[UIColor p_colorWithHex:0xc4c4c4] forState:0];
        [buttons addObject:button];
        if(idx<items.count-1){
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(button.frame)+3,CGRectGetWidth(self.frame),0.5)];
        line.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        [self addSubview:line];
        [buttons addObject:line];
        }
    }];
}
- (void)selectAction:(UIButton *)button{
    if(self.selectAtIndex)self.selectAtIndex(button.tag-40);
}
- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    CGContextRef contenxt  = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(contenxt, 1.0);
    CGContextSetFillColorWithColor(contenxt, [UIColor whiteColor].CGColor);
    CGContextSetStrokeColorWithColor(contenxt, [UIColor colorWithWhite:0.8 alpha:0.8].CGColor);
  
    CGRect r_rect = self.bounds;
    CGFloat radius = 6;
    CGFloat min_x = CGRectGetMinX(r_rect);
    CGFloat mid_x = CGRectGetMidX(r_rect);
    CGFloat max_x = CGRectGetMaxX(r_rect);
    CGFloat min_y = CGRectGetMinY(r_rect)+arror_w;
    CGFloat max_y = CGRectGetMaxY(r_rect);
    
    CGContextMoveToPoint(contenxt, max_x-leftOffset, min_y);
    CGContextAddLineToPoint(contenxt, max_x-leftOffset-arror_w, min_y-arror_w);
    CGContextAddLineToPoint(contenxt, max_x-leftOffset-2*arror_w, min_y);
    
    CGContextAddArcToPoint(contenxt, min_x, min_y, min_x, max_y, radius);
    CGContextAddArcToPoint(contenxt, min_x, max_y, max_x, max_y, radius);
    CGContextAddArcToPoint(contenxt, max_x, max_y, max_x, min_y, radius);
    CGContextAddArcToPoint(contenxt, max_x, min_y, mid_x, min_y, radius);
    CGContextClosePath(contenxt);
    CGContextStrokePath(contenxt);
    CGContextFillPath(contenxt);
    
}


@end
