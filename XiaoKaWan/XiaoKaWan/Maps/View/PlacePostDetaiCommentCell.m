//
//  PlacePostDetaiCommentCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetaiCommentCell.h"

#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#define headWith 45
@implementation PlacePostDetaiCommentCell
- (void)setCommentObject:(CommetObject *)commentObject{
    _commentObject = commentObject;
    User *user = commentObject.user;
    [self.headView setAvatarImageWithUrlString:[ImageSizeUrl avatar120:user.avatar]];
    self.titleLabel.text = user.nick;
    self.timeLabel.text = commentObject.gmtCreatedH;
    self.contentLabel.text = commentObject.content;
    if([self.commentObject.loginUserlLike integerValue])
    {
        self.likeButton.iconButton.selected = YES;
    }else self.likeButton.iconButton.selected = NO;
    [self.likeButton setTitle:commentObject.likes forState:UIControlStateNormal];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(23);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.width.equalTo(@(headWith));
        make.height.equalTo(@(headWith));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(22);
        make.left.equalTo(self.headView.mas_right).offset(10);
        make.width.equalTo(@150);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.equalTo(self.headView.mas_right).offset(10);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.headView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(22);
        make.right.equalTo(self.contentView.mas_right).offset(-22);
        make.width.equalTo(@40);
        make.height.equalTo(@30);
    }];
}
- (BJUIImageView *)headView{
    if(_headView == nil){
        _headView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_headView];
    }
    return _headView;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:13 color:0x393939];
        _titleLabel.text = @"Eric";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)timeLabel
{
    if(_timeLabel==nil){
        _timeLabel = [UILabel creatLabelWithFont:9 color:0x393939];
        _timeLabel.text = @"1小时前";
        [self.contentView addSubview:_timeLabel];
    }
    return _timeLabel;
}
- (UILabel*)contentLabel
{
    if(_contentLabel==nil){
        _contentLabel = [UILabel creatLabelWithFont:12 color:0x919191];
        _contentLabel.numberOfLines = 0;
        //_contentLabel.text = @"钓鱼钓鱼据搜房卡简单啊觉得看哈电话啊觉得胡恶搞没收到关键是打开姐夫看开点";
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}
- (BJIconfontButton *)likeButton{
    if(_likeButton == nil){
        _likeButton = [[BJIconfontButton alloc] init];
       
        [_likeButton setTitleColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
        [_likeButton setIconColor:[UIColor mainColor] forState:UIControlStateSelected];
        [_likeButton setIconColor:[UIColor p_colorWithHex:0x94999a] forState:UIControlStateNormal];
        [_likeButton setIcon:@"\U0000e611" forState:UIControlStateNormal];
        [_likeButton setIcon:@"\U0000e601" forState:UIControlStateSelected];
        [_likeButton addTartget:self action:@selector(likeAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_likeButton];
    }
    return _likeButton;
}
- (void)likeAction{
    [self.delelgate likeComment:self.commentObject commentObj:^(CommetObject *commentObj) {
        [self setCommentObject:commentObj];
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
