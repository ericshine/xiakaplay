//
//  UserListHeadView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJSegmentView.h"
@interface UserListHeadView : UIView
@property(nonatomic,copy) void(^selectAtIndex)(NSInteger index);
@property(nonatomic,strong)BJSegmentView *segmentView;
@end
