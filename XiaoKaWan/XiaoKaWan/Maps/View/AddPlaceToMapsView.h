//
//  AddPlaceToMapsView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"
#import "Place.h"
@interface AddPlaceToMapsView : BJView
@property(nonatomic,strong)Place *place;
@property(nonatomic,copy) void(^isAddToMaps)(BOOL addToMaps);
- (void)showViewInSupperView:(UIView*)view;
- (void)disMiss;
@end
