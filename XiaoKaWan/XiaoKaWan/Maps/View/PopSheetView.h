//
//  PopSheetView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface PopSheetView : BJView
@property(nonatomic,copy) void(^selectAtIndex)(NSInteger index);
- (instancetype)initWithFrame:(CGRect)frame;
- (void)showInView:(UIView *)view fromPoint:(CGPoint)point withItems:(NSArray *)items;
- (void)disMiss;
@end

@interface PopBgView : BJView
@property(nonatomic,copy) void(^selectAtIndex)(NSInteger index);
@property(nonatomic,strong)NSArray *items;
- (instancetype)initWithFrame:(CGRect)frame;
@end