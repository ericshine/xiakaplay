//
//  PlaceTitleNumberButton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceTitleNumberButton : UIButton
@property(nonatomic,strong)UILabel *titleLab;
@property(nonatomic,strong)UILabel *numberLab;
@property(nonatomic,copy) NSString *titleStr;
@property(nonatomic,copy) NSString *numnerStr;
@end
