//
//  PlacePostLocationView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostLocationView.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@implementation PlacePostLocationView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}
- (void)tapGesture:(UITapGestureRecognizer*)gesture{
    if(self.touchAction)self.touchAction();
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.iconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.width.equalTo(@20);
    }];
    [self.addressNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.iconLabel.mas_right);
        make.right.equalTo(self.mas_right).offset(-20);
        
    }];
    [self.addressLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressNameLb.mas_bottom);
        make.left.equalTo(self.iconLabel.mas_right);
        make.right.equalTo(self.mas_right).offset(-20);
        
    }];
}
- (UILabel*)iconLabel
{
    if(_iconLabel==nil){
        _iconLabel = [UILabel creatLabelWithFont:18 color:0xffde43];
        _iconLabel.font = [UIFont fontWithName:@"iconfont" size:18];
        _iconLabel.text = @"\U0000e602";
        [self addSubview:_iconLabel];
    }
    return _iconLabel;
}
- (UILabel*)addressNameLb{
    if(_addressNameLb==nil){
        _addressNameLb = [UILabel creatLabelWithFont:16 color:0x333333];
        _addressNameLb.text = @"西溪湿地汝拉小镇";
        [self addSubview:_addressNameLb];
    }
    return _addressNameLb;
}
- (UILabel*)addressLb{
    if(_addressLb==nil){
        _addressLb = [UILabel creatLabelWithFont:10 color:0x666666];
        _addressLb.text = @"西溪湿地北门1314号";
        [self addSubview:_addressLb];
    }
    return _addressLb;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
