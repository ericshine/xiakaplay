//
//  UserListTableViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserListTableViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "ImageSizeUrl.h"
#import "UserConfig.h"
#import "UIColor+colorWithHex.h"
#define cellHeight 72
#define headHeight 45
@implementation UserListTableViewCell
- (void)setUser:(User *)user{
    _user = user;
    if(user.uid == [[UserConfig sharedInstance].userId integerValue]){
        [self.followBtn setHidden:YES];
    }else{
        [self.followBtn setHidden:NO];
        if(user.bi_followed){
            [_followBtn setTitleColor:[UIColor p_colorWithHex:0xA8A8A8] forState:UIControlStateNormal];
            [_followBtn setTitle:@"\U0000e629" forState:UIControlStateNormal];
        }else{
            if(user.followed){
                 [_followBtn setTitleColor:[UIColor p_colorWithHex:0xA8A8A8] forState:UIControlStateNormal];
                [_followBtn setTitle:@"\U0000e61f" forState:UIControlStateNormal];
            }else{
                [_followBtn setTitleColor:[UIColor p_colorWithHex:0xffcd00] forState:UIControlStateNormal];
                [_followBtn setTitle:@"\U0000e61e" forState:UIControlStateNormal];
            }
        }
//        if(user.userType == UserType_MyFans){
//            if(user.bi_followed){
//                 [_followBtn setTitle:@"\U0000e629" forState:UIControlStateNormal];
//            }else [_followBtn setTitle:@"\U0000e61e" forState:UIControlStateNormal];
//        
//        }else if(user.userType == UserType_MyFollows){
//            if(user.bi_followed){
//            [_followBtn setTitle:@"\U0000e629" forState:UIControlStateNormal];
//            }else [_followBtn setTitle:@"\U0000e61f" forState:UIControlStateNormal];
//        }else{
//            if(user.bi_followed){
//                [_followBtn setTitle:@"\U0000e629" forState:UIControlStateNormal];
//            }else{
//                if(user.followed){
//                    [_followBtn setTitle:@"\U0000e61f" forState:UIControlStateNormal];
//                }else{
//                    [_followBtn setTitle:@"\U0000e61e" forState:UIControlStateNormal];
//                }
//            }
//        }
    }
    self.titleLabel.text = user.nick;
    
    [self.headImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar120:user.avatar]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(headHeight));
        make.width.equalTo(@(headHeight));
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(15);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@20);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.headImageView.mas_right).offset(10);
        make.width.equalTo(@200);
    }];
    [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.right.equalTo(self.contentView.mas_right).offset(-30);
        make.height.equalTo(@40);
        make.width.equalTo(@40);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@0.5);
    }];
}
- (BJUIImageView*)headImageView
{
    if(_headImageView==nil){
        _headImageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
    }
    return _headImageView;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:15 color:0x6c6c6c];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIButton*)followBtn
{
    if(_followBtn==nil){
        _followBtn = [[UIButton alloc] init];
        _followBtn.titleLabel.font = [UIFont fontWithName:@"iconfont" size:35];
//        [_followBtn setTitleColor:[UIColor p_colorWithHex:0xa8a8a8] forState:UIControlStateDisabled];
        [_followBtn addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_followBtn];
        
    }
    return _followBtn;
}
- (UIView*)lineView
{
    if(_lineView==nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
- (void)followAction:(UIButton *)button{
    [self.delegate followUser:_user success:^(User *user) {
        self.user = user;
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
