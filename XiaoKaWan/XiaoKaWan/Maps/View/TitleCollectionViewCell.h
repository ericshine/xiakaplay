//
//  TitleCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/31/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Description
 */
typedef NS_ENUM(NSInteger,MoreType) {
    /**
     *  Description
     */
    MORE_TYPE = 0,
    /**
     *  更多地点
     */
    MORE_PLACE,
    /**
     *  更多动态
     */
    MORE_POST
};
@protocol TitleCollectionViewCellDelegate <NSObject>

- (void)moreAction:(MoreType)moreType;

@end
@interface TitleCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong) UIButton *moreButton;
@property(nonatomic,strong) UIImageView *lineView;
@property(nonatomic,assign) id<TitleCollectionViewCellDelegate> delegate;
@property(nonatomic) MoreType moreType;
@end
