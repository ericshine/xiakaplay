//
//  CommentAndLikeView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJIconfontButton.h"
#import "BJPlaceholderTextView.h"
@protocol CommentAndLikeViewDelegate <NSObject>

- (void)commentWithContent:(NSString*)content;
- (void)likeAction:(void(^)(BOOL success))success;

@end
@interface CommentAndLikeView : UIView<UITextViewDelegate>
@property(nonatomic,strong) id<CommentAndLikeViewDelegate>delegate;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)BJPlaceholderTextView *commentTextView;
@property(nonatomic,strong)BJIconfontButton *likeView;
@property(nonatomic) BOOL likeState;
@end

