//
//  MapsCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "MapsUser.h"
#import "PlaceMapsFrameObj.h"
@interface MapsCollectionViewCell()
@end
@implementation MapsCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
    [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setMapsObj:(MapsDetail *)mapsObj
{
    _mapsObj = mapsObj;
    self.titleLabel.text = mapsObj.name;
    [self setTitle:@"\U0000E600" withLabel:self.followNumberIcon];
    [self setTitle:@"\U0000E602" withLabel:self.placeNumberIcon];
    [self setTitle:@"\U0000E607" withLabel:self.dynamicNumberIcon];
    self.followNumberLabel.text =[NSString stringWithFormat:@"%ld",(long)mapsObj.follow_count];
    self.placeNumberLabel.text =[NSString stringWithFormat:@"%ld",(long)mapsObj.place_count];
//    self.dynamicNumberLabel.text =[NSString stringWithFormat:@"%ld",(long)mapsObj.post_count];
    self.reasonLabel.text = mapsObj.addReason;
    [self collectView];

    self.modelView.imageArray = mapsObj.latest_photos;
    self.modelView.mapsObject = mapsObj;
    [self intHeadViewData:mapsObj];
}
- (void)intHeadViewData:(MapsDetail *)maps{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"uid" ascending:YES];
    NSArray *usersArray = [maps.latest_users sortedArrayUsingDescriptors:@[sort]];
    NSMutableArray *userHeadUrl  = [NSMutableArray arrayWithCapacity:0];
    [usersArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        User *user = (User *)obj;
//        NSLog(@"avatar:%@",user.avatar);
        [userHeadUrl addObject:user.avatar==nil?@" ":user.avatar];
    }];
    
    [self.groupHeadView setImageUrlArray:userHeadUrl];
    self.groupHeadView.countLimit = 4;
    self.groupHeadView.totalNumber = MAX(maps.user_count, userHeadUrl.count);
    self.groupHeadView.headViewType = HEADVIEWTYPE_SHOWNUMBER;
    NSInteger imageCount = userHeadUrl.count;
    if(imageCount>4) imageCount= 4;
    CGFloat with = imageCount*30+8*(imageCount-1);
    self.groupHeadView.frame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-(with+10), 34, with, 30);
    
}
- (void)setTitle:(NSString *)title withLabel:(UIButton *)label{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:title];
    [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:15],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xb2b2b2]} range:NSMakeRange(0, title.length)];
    [label setAttributedTitle:attStr forState:UIControlStateNormal];
    //    [self.placeNumberLabel setImage:image forState:UIControlStateNormal];
    //    [label setTitleColor:[UIColor p_colorWithHex:0x737373] forState:UIControlStateNormal];
}

- (void)updateConstraints
{
    [super updateConstraints];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(10);
//        make.width.equalTo(@100);
//        make.height.equalTo(@30);
    }];
    [self.followNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.height.equalTo(@15);
    }];
    [self.followNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.followNumberIcon.mas_right);
        make.height.equalTo(@15);
    }];
    [self.placeNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.followNumberLabel.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.placeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
        make.left.equalTo(self.placeNumberIcon.mas_right);
        make.height.equalTo(@15);
    }];
//    [self.dynamicNumberIcon mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
//        make.left.equalTo(self.placeNumberLabel.mas_right).offset(0);
//        make.height.equalTo(@15);
//    }];
//    [self.dynamicNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
//        make.left.equalTo(self.dynamicNumberIcon.mas_right);
//        make.height.equalTo(@15);
//    }];
 
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.placeNumberLabel.mas_bottom).offset(25);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
//        make.bottom.equalTo(self.contentView.mas_bottom).offset(-73);
    }];
    [self.reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectView.mas_bottom).offset(24);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-73);
    }];
}

- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:17 color:0x4c4c4c];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
//- (UILabel*)titleLabel
//{
//    if(_titleLabel==nil){
//        _titleLabel = [UILabel creatLabelWithFont:17 color:0x4c4c4c];
//        [self.contentView addSubview:_titleLabel];
//    }
//    return _titleLabel;
//}
- (UILabel*)placeNumberLabel
{
    if(_placeNumberLabel==nil){
        _placeNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
        [self.contentView addSubview:_placeNumberLabel];
    }
    return _placeNumberLabel;
}
- (UILabel*)followNumberLabel
{
    if(_followNumberLabel==nil){
        _followNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
        [self.contentView addSubview:_followNumberLabel];
    }
    return _followNumberLabel;
}
//- (UILabel*)dynamicNumberLabel
//{
//    if(_dynamicNumberLabel==nil){
//        _dynamicNumberLabel = [UILabel creatLabelWithFont:12 color:0xb2b2b2];
//        [self.contentView addSubview:_dynamicNumberLabel];
//    }
//    return _dynamicNumberLabel;
//}
- (UIButton*)placeNumberIcon
{
    if(_placeNumberIcon==nil){
        _placeNumberIcon = [[UIButton alloc] init];
        _placeNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
        _placeNumberIcon.selected = NO;
        [self.contentView addSubview:_placeNumberIcon];
        
    }
    return _placeNumberIcon;
}
//- (UIButton *)dynamicNumberIcon
//{
//    if(_dynamicNumberIcon == nil){
//        _dynamicNumberIcon = [[UIButton alloc] init];
//        _dynamicNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
//        _dynamicNumberIcon.selected = NO;
//        [self.contentView addSubview:_dynamicNumberIcon];
//    }
//    return _dynamicNumberIcon;
//}
- (UIButton*)followNumberIcon
{
    if(_followNumberIcon==nil){
        _followNumberIcon = [[UIButton alloc] init];
        _followNumberIcon.titleLabel.font = [UIFont systemFontOfSize:15];
        _followNumberIcon.selected = NO;
        [self.contentView addSubview:_followNumberIcon];
    }
    return _followNumberIcon;
}
- (BJGroupHeadImageView*)groupHeadView
{
    if(_groupHeadView==nil){
        _groupHeadView = [[BJGroupHeadImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-160, 34, 150, 30) andGroups:nil];
        [self.groupHeadView setUserInteractionEnabled:YES];
        [self.contentView addSubview:_groupHeadView];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gesture:)];
//        [_groupHeadView addGestureRecognizer:tap];
    }
    return _groupHeadView;
}
- (void)gesture:(UITapGestureRecognizer *)gesture{

}
- (UIView*)line
{
    if(_line==nil){
        _line = [[UIView alloc] init];
        _line.backgroundColor =[UIColor blackColor];
        [self.contentView addSubview:_line];
    }
    return _line;
}
- (UICollectionView*)collectView
{
    if(_collectView==nil){
        UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc] init];
        //       viewLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:viewLayout];
        _collectView.scrollEnabled = NO;
        _collectView.delegate = self.modelView;
        _collectView.dataSource = self.modelView;
        _collectView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.layer.cornerRadius = 10;
        _collectView.layer.masksToBounds = YES;
        [self.contentView addSubview:_collectView];
    }
    return _collectView;
}
- (UILabel*)reasonLabel
{
    if(_reasonLabel==nil){
        _reasonLabel = [UILabel creatLabelWithFont:[PlaceMapsFrameObj reasonLabelFont] color:0x777777];
        _reasonLabel.numberOfLines = 0;
        [self.contentView addSubview:_reasonLabel];
    }
    return _reasonLabel;
}
- (PlaceMapsMultipleImageViewModel *)modelView
{
    if(_modelView == nil){
        _modelView = [[PlaceMapsMultipleImageViewModel alloc] initWithModel:nil andCollcetionView:_collectView];
        _modelView.delegate = self;
    }
    
    return _modelView;
}
#pragma mark- PlaceMapsMultipleImageViewModelDelegate
- (void)touchCollectionWithModel:(id)model
{
    [self.delegate touchMapsWithModel:self.mapsObj];
}
//#pragma mark - touch
//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    [UIView animateWithDuration:0.2 animations:^{
//        self.transform = CGAffineTransformMakeScale(0.9, 0.9);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.1 animations:^{
//            self.transform = CGAffineTransformIdentity;
//        } completion:^(BOOL finished) {
////            [self touchCollectionWithModel:];
//        }];
//    }];
//}

@end
