//
//  PlacePostDetailTitleCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacePostDetailTitleCell : UITableViewCell
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIImageView *leftView;
@property(nonatomic,strong) UIImageView *rightView;
@end
