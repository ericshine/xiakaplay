//
//  UserSelectCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsSelectCell.h"
#define cellHeight 72
#define headHeight 45
@implementation MapsSelectCell
- (void)layoutSubviews{
    [super layoutSubviews];
//    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(@(headHeight));
//        make.width.equalTo(@(headHeight));
//        make.centerY.equalTo(self.contentView.mas_centerY);
//        make.left.equalTo(self.contentView.mas_left).offset(15);
//    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@20);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.width.equalTo(@200);
    }];
    [self.selectLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [self showLineWithLeft:0 rightOffset:0];
}
- (BJUIImageView*)headImageView
{
    if(_headImageView==nil){
        _headImageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
    }
    return _headImageView;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:15 color:0x6c6c6c];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel *)selectLb{
    if(_selectLb == nil){
        _selectLb = [UILabel creatLabelWithFont:20 color:0xffda44];
        _selectLb.font = [UIFont fontWithName:@"iconfont" size:20];
        [self.contentView addSubview:_selectLb];
    }
    return _selectLb;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
