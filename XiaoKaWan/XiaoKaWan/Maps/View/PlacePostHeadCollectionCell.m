//
//  PlacePostHeadCollectionCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostHeadCollectionCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@implementation PlacePostHeadCollectionCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    self.typeString = @"动态";//default
    }
    return self;
}
- (void)setPostListObject:(PostListObj *)postListObject{
    _postListObject = postListObject;
    self.titleLabel.text = postListObject.name;
    self.detailLabel.text = [NSString stringWithFormat:@"%@ %@",_typeString,postListObject.postCount];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(15);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-100);
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        make.left.equalTo(self.contentView.mas_left).offset(20);
    }];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:20 color:0x555555];
//        _titleLabel.text = @"猫屎咖啡  猫屎咖啡  猫屎咖啡";
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:15 color:0xb5b5b5];
//        _detailLabel.text = @"榜单 666";
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
@end
