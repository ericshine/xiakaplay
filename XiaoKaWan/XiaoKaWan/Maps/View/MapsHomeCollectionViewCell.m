//
//  MapsHomeCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsHomeCollectionViewCell.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "ImageSizeUrl.h"
#import <UIImageView+WebCache.h>
#import "UIImage+Category.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@implementation MapsHomeCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        self.imageView = [[BJUIImageView alloc] init];
        self.imageView.backgroundColor = [UIColor p_colorWithHex:0xefefef];
        self.imageView.layer.masksToBounds = YES;
        [self.contentView addSubview:self.imageView];
        self.contentView.layer.masksToBounds = YES;
    }
    return self;
}
- (void)setImageObj:(BJImageObject *)imageObj{
    _imageObj = imageObj;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[ImageSizeUrl imagef480_url:imageObj.url]]];
     [self setImageFrame];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformMakeScale(0.95, 0.95);
    } completion:^(BOOL finished) {
       
    }];
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self touchEnd];
    
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [self touchEnd];
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    [self touchEnd];
}
- (void)touchEnd{
    [UIView animateWithDuration:0.1 animations:^{
        self.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        //            [self.delegate touchItemWithModel:nil];
    }];
   
}
- (void)setImageFrame
{
    CGFloat w = _imageObj.width;
    CGFloat h = _imageObj.height;
    if(w==0 || h==0) return;
    CGFloat imageW = self.frame.size.width;
    CGFloat imageH = h*(imageW/w);
    if(imageH <self.frame.size.height){
        imageH = self.frame.size.height;
        imageW = w*(self.frame.size.height/h);
        if(imageW<self.frame.size.width){
            imageW = self.frame.size.width;
            imageH = h*(self.frame.size.width/w);
        }
    }
    self.imageView.frame = CGRectMake(0, 0, imageW, imageH);
}
@end
