//
//  PlacePostLocationView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacePostLocationView : UIView
@property(nonatomic,strong)UILabel *iconLabel;
@property(nonatomic,strong)UILabel *addressNameLb;
@property(nonatomic,strong)UILabel *addressLb;
@property(nonatomic,copy) void(^touchAction)();
@end
