//
//  MapsPlaceSegmentView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJTitleNumberButton.h"
#define SEGMENTWIDTH 150
#define SEGMENTHEIGHT 60
typedef void (^SegmentAtIndex)(NSInteger index);
@interface MapsPlaceSegmentView : UIView
- (instancetype)initWithFrame:(CGRect)frame WithTitles:(NSArray *)titles;
@property(nonatomic,strong)BJTitleNumberButton *segment1;
@property(nonatomic,strong)BJTitleNumberButton *segment2;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,copy)SegmentAtIndex segmentIndex;
@property(nonatomic,copy) NSString *number1;
@property(nonatomic,copy) NSString *number2;
@property(nonatomic,strong) NSArray *titles;
@end
