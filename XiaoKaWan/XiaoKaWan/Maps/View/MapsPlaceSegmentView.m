//
//  MapsPlaceSegmentView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceSegmentView.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define WIDTH 60
@implementation MapsPlaceSegmentView
- (instancetype)initWithFrame:(CGRect)frame WithTitles:(NSArray *)titles;
{
    self = [super initWithFrame:frame];
    if(self){
        self.segment1.titleLab.text = titles[0];
        self.segment2.titleLab.text = titles[1];

        [self setNeedsUpdateConstraints];
//        [self segment1];
    }
    return self;
}
- (void)setNumber1:(NSString *)number1{
    if(number1 == nil || number1.length<=0) number1 = @"0";
    self.segment1.numberLab.text = number1;
}
- (void)setNumber2:(NSString *)number2{
    NSLog(@"fff:%@",number2);
    if(number2 == nil || number2.length<=0|| number2==NULL ||[number2 isEqualToString:@"(null)"]) number2 = @"0";
    self.segment2.numberLab.text = number2;
}
- (void)setTitles:(NSArray *)titles{
    self.segment1.titleLab.text = titles[0];
    self.segment2.titleLab.text = titles[1];
}
- (void)updateConstraints
{
    [super updateConstraints];
    [self.segment1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.height.equalTo(@SEGMENTHEIGHT);
        make.width.equalTo(@WIDTH);
    }];
    [self.segment2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(@SEGMENTHEIGHT);
        make.width.equalTo(@WIDTH);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segment1.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.height.equalTo(@1.5);
        make.width.equalTo(@WIDTH);
    }];
}
- (BJTitleNumberButton*)segment1
{
    if(_segment1 == nil){
        _segment1 = [[BJTitleNumberButton alloc] init];
        _segment1.tag = 10;
        [_segment1 addTarget:self action:@selector(butSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_segment1];
    }
    return _segment1;

}
- (BJTitleNumberButton*)segment2
{
    if(_segment2 == nil){
        _segment2 = [[BJTitleNumberButton alloc] init];
        _segment2.tag = 11;
        [_segment2 addTarget:self action:@selector(butSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_segment2];
    }
    return _segment2;

}
- (UIView*)lineView
{
    if(_lineView==nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0xffe028];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (void)butSelectAction:(BJTitleNumberButton *)button
{
    [self animationSelect:button];
    button.selected = YES;
    if(button.tag ==10) {
        _segment2.selected = !button.selected;
    }
    else {
        _segment1.selected = !button.selected;
    }
    if(self.segmentIndex)self.segmentIndex((button.tag-10));
}
- (void)animationSelect:(BJTitleNumberButton *)button{
//    [UIView animateWithDuration:0.3 animations:^{
//        button.transform = CGAffineTransformMakeScale(0.8, 0.8);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3 animations:^{
//            button.transform = CGAffineTransformMakeScale(1, 1);
//        }];
//    }];
    if(button.tag == 10){
        [UIView animateWithDuration:0.3 animations:^{
            self.lineView.transform = CGAffineTransformIdentity;
           // self.lineView.transform = CGAffineTransformMakeScale(0.8, 1);

        } completion:^(BOOL finished) {
            //self.lineView.transform = CGAffineTransformMakeScale(1, 1);
        }];
    }else {
        [UIView animateWithDuration:0.3 animations:^{
            self.lineView.transform = CGAffineTransformMakeTranslation(90, 0);
//            button.transform = CGAffineTransformMakeScale(0.8, 0.8);

        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.3 animations:^{
////                button.transform = CGAffineTransformMakeScale(1, 1);
//                
//            }];
        }];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
