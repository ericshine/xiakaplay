//
//  MapsDetailPlaceCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
@interface MapsDetailPlaceCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *addressLabel;
@property(nonatomic,strong)UILabel *havedGoLabel;
@property(nonatomic,strong)UILabel *wantGoLabel;
@property(nonatomic,strong)BJGroupHeadImageView *groupHeadView;
@property(nonatomic,strong)UIImageView *iconView;
@property(nonatomic,strong)UILabel *contentLabel;
@property(nonatomic,strong)NSMutableArray *imageArray;
@end
