//
//  MapsDetailDynamicCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostObjet.h"
#import "BJUIImageView.h"
@interface MapsDetailDynamicCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)BJUIImageView *dynamicImageView;
@property(nonatomic,strong)BJUIImageView *headImageView;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *addressLabel;
@property(nonatomic,strong)UIButton *likeLabel;
@property(nonatomic,strong)UIButton *commitLabel;
@property(nonatomic,strong)UILabel *contentLabel;
@property(nonatomic,strong)PostObjet *postObject;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)UILabel *multiImageLb;
@end
