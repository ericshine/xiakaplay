//
//  MapsPlaceNormalCollectionCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceNormalCollectionCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import "MapsPlaceListCellViewModel.h"
#import <Masonry.h>
#import "PlaceFrameObject.h"
#import "NSString+Cagetory.h"
#define collectionViewHeight 145
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface MapsPlaceNormalCollectionCell()<MapsPlaceListCellViewModelDelegate>
@property(nonatomic,strong)MapsPlaceListCellViewModel *viewModel;
@property(nonatomic,strong)UIView *bgView;
@end
@implementation MapsPlaceNormalCollectionCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self bgView];
        [self collectionView];
        self.contentView.backgroundColor = [UIColor p_colorWithHex:0xf0f0f0];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    self.titleLabel.text = place.name;
    self.detailLabel.text = place.addReason;
    CGSize size = [place.addReason sizeWithFont:self.detailLabel.font andSize:CGSizeMake(kDeviceWidth-75, 999)];
    if(size.height>30) _moreButton.hidden = NO;
    else _moreButton.hidden = YES;
    if(place.address.length)
    _viewModel.place = place;
    self.beenWantView.place = place;
    [self tagLabel];
}
- (void)setIsShowMore:(BOOL)isShowMore{
    self.moreButton.selected = isShowMore;
    if(isShowMore){
        self.detailLabel.numberOfLines = 0;
    }else{
        self.detailLabel.numberOfLines = 2;
    }
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(25);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(5);
        make.width.equalTo(@50);
        make.height.equalTo(@50);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(14);
        make.left.equalTo(self.bgView.mas_left).offset(15);
        make.right.equalTo(self.bgView.mas_right).offset(15);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(31);
        make.left.equalTo(self.bgView.mas_left).offset(10);
        make.right.equalTo(self.bgView.mas_right).offset(-10);
        make.height.equalTo(@(collectionViewHeight));
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.left.equalTo(self.bgView.mas_left).offset(21);
        make.right.equalTo(self.bgView.mas_right).offset(-21);
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom);
        //        make.left.equalTo(self.contentView.mas_left).offset(120);
        make.centerX.equalTo(self.bgView.mas_centerX);
        make.width.equalTo(@60);
        make.height.equalTo(@30);
    }];
    [self.beenWantView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-20);
        make.left.equalTo(self.bgView.mas_left).offset(5);
        make.right.equalTo(self.bgView.mas_right).offset(-5);
        make.height.equalTo(@30);
    }];
    
}

- (MapsPlaceListCellViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[MapsPlaceListCellViewModel alloc] initWithView:_collectionView];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
- (UIView *)bgView{
    if(_bgView == nil){
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 5;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}
- (UIImageView*)tagLabel
{
    if(_tagLabel==nil){
        _tagLabel = [[UIImageView alloc] init];
        _tagLabel.image = [UIImage imageNamed:@"bangdanIcon"];
        [self.contentView addSubview:_tagLabel];
    }
    return _tagLabel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:18 color:0x545454];
        //        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000e612未名湖湖畔"];
        //        [attStr setAttributes:@{NSForegroundColorAttributeName:[UIColor p_colorWithHex:0xffdc43],NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:20]} range:NSMakeRange(0, 1)];
        //        _titleLabel.attributedText = attStr;
        [self.bgView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(100, collectionViewHeight);
        layout.minimumInteritemSpacing = 6;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, collectionViewHeight) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self.viewModel;
        _collectionView.dataSource = self.viewModel;
        [self.bgView addSubview:_collectionView];
    }
    return _collectionView;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:[PlaceFrameObject reasonLabelFont] color:0x5f5f5f];
        _detailLabel.text = @"dfjg是对方打电话更加深度开发高科技的感觉对方更加好看分手的减肥高考的健康的风格和空间的分开过地方艰苦对方给对方感觉的就发给就是对方";
        _detailLabel.numberOfLines = 2;
        [self.bgView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UIButton*)moreButton
{
    if(_moreButton==nil){
        _moreButton = [[UIButton alloc] init];
        _moreButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:15];
        _moreButton.hidden = YES;
        [_moreButton setTitleColor:[UIColor p_colorWithHex:0x878787] forState:UIControlStateNormal];
        [_moreButton setTitle:@"\U0000e615" forState:UIControlStateNormal];
        [_moreButton setTitle:@"\U0000e616" forState:UIControlStateSelected];
        [_moreButton addTarget:self action:@selector(showMoreData:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_moreButton];
    }
    return _moreButton;
}
- (BeenWantView*)beenWantView
{
    if(_beenWantView==nil){
        _beenWantView = [[BeenWantView alloc] init];
        _beenWantView.delegate = self;
        [self.bgView addSubview:_beenWantView];
        _beenWantView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [_beenWantView addGestureRecognizer:tap];
    }
    return _beenWantView;
}
- (void)tapGesture:(UITapGestureRecognizer *)tap{
    [self.delegate toUserList:_place];
}
- (void)showMoreData:(UIButton *)button{
    button.selected = !button.selected;
    if(button.selected){
        self.detailLabel.numberOfLines = 0;
    }else{
        self.detailLabel.numberOfLines = 2;
    }
    [self.delegate showMoreReason:button.selected atIndexPath:self.indexPath];
}
#pragma mark - MapsPlaceListCellViewModelDelegate
- (void)selelctItem:(Place *)place{
    if([self.delegate respondsToSelector:@selector(selectImageViewWithPlace:)]){
        [self.delegate selectImageViewWithPlace:place];
    }
}
#pragma mark - BeenWantViewDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate wantAction:place callBack:place1];
}
@end
