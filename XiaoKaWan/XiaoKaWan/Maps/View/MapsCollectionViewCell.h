//
//  MapsCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
#import "PlaceMapsMultipleImageViewModel.h"
#import "MapsDetail.h"
@protocol MapsCollectionViewCellDelegate <NSObject>
- (void)touchMapsWithModel:(id)model;
@end
@interface MapsCollectionViewCell : UICollectionViewCell<PlaceMapsMultipleImageViewModelDelegate>
@property(nonatomic,assign)id<MapsCollectionViewCellDelegate>delegate;
@property(nonatomic,strong)UILabel *titleLabel;
//@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIButton *placeNumberIcon;
@property(nonatomic,strong)UIButton *dynamicNumberIcon;
@property(nonatomic,strong)UIButton *followNumberIcon;
@property(nonatomic,strong)UILabel *placeNumberLabel;
@property(nonatomic,strong)UILabel *dynamicNumberLabel;
@property(nonatomic,strong)UILabel *followNumberLabel;
@property(nonatomic,strong)BJGroupHeadImageView *groupHeadView;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UICollectionView *collectView;
@property(nonatomic,strong)UILabel *reasonLabel;
@property(nonatomic,strong)PlaceMapsMultipleImageViewModel *modelView;
@property(nonatomic,strong)MapsDetail *mapsObj;

@end

