//
//  MapsHomeCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJImageObject.h"
#import "BJUIImageView.h"
@protocol MapsHomeCollectionViewCellDelegate <NSObject>

-(void)touchItemWithModel:(id)model;

@end
@interface MapsHomeCollectionViewCell : UICollectionViewCell
@property(nonatomic,assign) id<MapsHomeCollectionViewCellDelegate>delegate;
@property(nonatomic,strong) BJUIImageView *imageView;
@property(nonatomic,strong) BJImageObject*imageObj;
@end
