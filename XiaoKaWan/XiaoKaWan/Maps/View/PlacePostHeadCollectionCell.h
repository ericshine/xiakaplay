//
//  PlacePostHeadCollectionCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/8/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListObj.h"
@interface PlacePostHeadCollectionCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,copy) NSString *typeString; // default 动态 （榜单）
@property(nonatomic,strong)PostListObj *postListObject;
@end
