//
//  HeadSegmentCollectionView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/24/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapsPlaceSegmentView.h"
#import "PostListObj.h"
@protocol HeadSegmentCollectionViewDelegate <NSObject>

- (void)selectAtIndex:(NSInteger)index;

@end
@interface HeadSegmentCollectionView : UICollectionReusableView
@property(nonatomic,strong) id<HeadSegmentCollectionViewDelegate>delegate;
@property(nonatomic,strong)MapsPlaceSegmentView *segmentView;
@property(nonatomic,strong)NSArray *titles;
@property(nonatomic,strong)PostListObj *postObject;
@end

