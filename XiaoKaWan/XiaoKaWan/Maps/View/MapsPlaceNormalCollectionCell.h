//
//  MapsPlaceNormalCollectionCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "BeenWantView.h"
@protocol MapsPlaceNormalCollectionCellDelegate <NSObject>
@optional
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1;
- (void)selectImageViewWithPlace:(Place*)place;
- (void)showMoreReason:(BOOL)show atIndexPath:(NSIndexPath*)indexPath;
- (void)toUserList:(Place *)place;
@end
@interface MapsPlaceNormalCollectionCell : UICollectionViewCell<BeenWantViewDelegate>
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)UIButton *moreButton;
@property(nonatomic,strong)UIImageView *tagLabel;
@property(nonatomic,strong)BeenWantView *beenWantView;
@property(nonatomic,assign)id<MapsPlaceNormalCollectionCellDelegate>delegate;
@property(nonatomic,strong)Place *place;
@property(nonatomic,strong)NSIndexPath *indexPath;
@property(nonatomic) BOOL isShowMore;
@end
