//
//  MapsPlaceSimpleCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/31/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
typedef void(^CallBack)(BOOL success);
@class Place;
@protocol MapsPlaceSimpleCollectionViewCellDelegate <NSObject>

- (void)showWantUserListWithData:(Place *)place;
- (void)showBeenUserListWithData:(Place *)place;
- (void)beenButtonActionWithPlace:(Place*)place callBack:(CallBack)callback;
- (void)wangButtonActionWithPlace:(Place*)place callBack:(CallBack)callback;
@end

@interface MapsPlaceSimpleCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) BJGroupHeadImageView *groupBeenHeadView;
@property(nonatomic,strong) BJGroupHeadImageView *groupWantHeadView;
@property(nonatomic,strong) UIButton *beenButton;
@property(nonatomic,strong) UIButton *wantButton;
@property(nonatomic,strong) Place *place;
@property(nonatomic,strong) UILabel*flagLabelIcon;
@property(nonatomic,strong) UILabel *wantLabelIcon;
@property(nonatomic,assign) id<MapsPlaceSimpleCollectionViewCellDelegate> delegate;
@end
