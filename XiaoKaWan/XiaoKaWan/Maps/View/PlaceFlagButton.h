//
//  PlaceFlagButton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
@protocol PlaceFlagButtonDelegate <NSObject>
- (void)usersList;
- (void)flagTouchAction;
@end
@class PlaceFlageObj;
@interface PlaceFlagButton : UIView
@property(nonatomic,strong) id<PlaceFlagButtonDelegate>delegate;
/**
 *  头像
 */
@property(nonatomic,strong) BJGroupHeadImageView *groupHeadView;
/**
 *  按钮
 */
@property(nonatomic,strong) UIButton *actionButton;
/**
 * 需要传的数据
 */
@property(nonatomic,strong) PlaceFlageObj *model;
/**
 *  按钮标题
 */
@property(nonatomic,strong) NSString *title;
/**
 *  选择之后的标签
 */
@property(nonatomic,strong) UILabel*flagLabelIcon;
/**
 *  标签样式
 */
@property(nonatomic,strong) NSString *iconText;
/**
 *  颜色
 */
@property(nonatomic,strong) UIColor *textColor;
@end

/**
 *  需要传的数据
 */
@interface PlaceFlageObj : NSObject
@property(nonatomic) NSInteger totalNumber;
@property(nonatomic,strong) NSArray *arrayUrl;
@end