//
//  MapsDetailPlaceHeadCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetailPlaceHeadCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
//static NSInteger const leftSpace = 20; // equal to rightSpace
@implementation MapsDetailPlaceHeadCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setNeedsUpdateConstraints];
       
    }
    return self;
}
- (void)setMapsObject:(MapsDetail *)mapsObject{
    self.titleLabel.text = mapsObject.name;
    self.detailLabel.text = mapsObject.content;
    self.scanLabel.text = [NSString stringWithFormat:@"%li",(long)mapsObject.read_count+1];
    self.followLabel.text = [NSString stringWithFormat:@"%li",(long)mapsObject.follow_count];
    NSString *placeNubText = [NSString stringWithFormat:@"%li 地点",(long)mapsObject.place_count];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:placeNubText];
    [attStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} range:NSMakeRange(placeNubText.length-2, 2)];
    self.placeNumberLb.attributedText = attStr;
    [self setHeadViewData:mapsObject];
}
- (void)setHeadViewData:(MapsDetail*)mapsObject{
    NSMutableArray *imageUrls = [NSMutableArray arrayWithCapacity:0];
    [mapsObject.latest_users enumerateObjectsUsingBlock:^(User * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj != nil && obj.avatar !=nil) [imageUrls addObject:obj.avatar];
        else [imageUrls addObject:@""];
    }];
    [self.headGroupView setImageUrlArray:imageUrls];
    self.headGroupView.countLimit = 4;
    self.headGroupView.totalNumber = mapsObject.user_count;
    self.headGroupView.headViewType = HEADVIEWTYPE_SHOWNUMBER;
   
}
- (void)updateConstraints
{
    [super updateConstraints];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(32);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-134);
        make.height.equalTo(@20);
    }];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(28);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-134);
    }];
    [self.scanIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(28);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.height.equalTo(@15);
    }];
    [self.scanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(28);
        make.left.equalTo(self.scanIcon.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.followIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(28);
        make.left.equalTo(self.scanLabel.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.followLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(28);
        make.left.equalTo(self.followIcon.mas_right).offset(0);
        make.height.equalTo(@15);
    }];
    [self.headGroupView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@30);
        make.width.equalTo(@100);
    }];
    [self.placeNumberLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.followLabel.mas_top).offset(40);
        make.left.equalTo(self.mas_left).offset(20);
    }];
    
    
//  [self headGroupView];

}
//- (MapsPlaceSegmentView*)segmentView
//{
//    if(_segmentView == nil){
//        _segmentView = [[MapsPlaceSegmentView alloc] initWithFrame:CGRectZero WithTitles:@[@"地点",@"动态"]];
//        __weak typeof(self)weakSelf = self;
//        _segmentView.segmentIndex = ^(NSInteger index){
//            __strong __typeof(weakSelf) strongSelf = weakSelf;
//           if(index == 0) [strongSelf.delegate placeTouchAction];
//           else [strongSelf.delegate postTouchAction];
//        };
//        [self.contentView addSubview:_segmentView];
//    }
//    return _segmentView;
//}
- (BJGroupHeadImageView *)headGroupView
{
    if(_headGroupView == nil){
        _headGroupView = [[BJGroupHeadImageView alloc]init];
        [self.contentView addSubview:_headGroupView];
    }
    return _headGroupView;
}
- (UILabel *)placeNumberLb{
    if(_placeNumberLb == nil){
        _placeNumberLb = [UILabel creatLabelWithFont:34 color:0x555555];
        [self.contentView addSubview:_placeNumberLb];
    }
    return _placeNumberLb;
}
- (UIButton*)scanIcon
{
    if(_scanIcon==nil){
        _scanIcon = [[UIButton alloc] init];
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000E603"];
        [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:15],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x737373]} range:NSMakeRange(0, 1)];
        [_scanIcon setAttributedTitle:attStr forState:UIControlStateNormal];
        [self.contentView addSubview:_scanIcon];
    }
    return _scanIcon;
}
- (UIButton*)followIcon
{
    if(_followIcon==nil){
        _followIcon = [[UIButton alloc] init];
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"\U0000E600"];
        [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:15],NSForegroundColorAttributeName:[UIColor p_colorWithHex:0x737373]} range:NSMakeRange(0, 1)];
        [_followIcon setAttributedTitle:attStr forState:UIControlStateNormal];
//         _followIcon.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_followIcon];
    }
    return _followIcon;
}
- (UILabel*)scanLabel{
    if(_scanLabel == nil){
        _scanLabel = [UILabel creatLabelWithFont:12 color:0x737373];
        _scanLabel.text=@"1900";
        [self.contentView addSubview:_scanLabel];
    }
    return _scanLabel;
}
- (UILabel*)followLabel{
    if(_followLabel == nil){
        _followLabel = [UILabel creatLabelWithFont:12 color:0x737373];
         _followLabel.text=@"1900";
        [self.contentView addSubview:_followLabel];
    }
    return _followLabel;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:17 color:0x494949];
        _detailLabel.numberOfLines = 0;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:25 color:0x323232];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (void)buttonAction:(BJTitleNumberButton*)button{
    NSInteger tag = button.tag ;
    switch (tag) {
        case 30:
            if([self.delegate respondsToSelector:@selector(placeTouchAction)]){
                [self.delegate placeTouchAction];
            }
            break;
        case 31:
            if([self.delegate respondsToSelector:@selector(postTouchAction)]){
                [self.delegate postTouchAction];
            }
            break;
    
        default:
            break;
    }
}
@end
