//
//  PlacePostDetailHeadFootView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDetailObject.h"
typedef NS_ENUM(NSInteger,CommentLikeType) {
    COMMENT_TYPE,
    LIKE_TYPE
};
@protocol PlacePostDetailHeadFootViewDelegate <NSObject>

- (void)selelctSegmentType:(CommentLikeType)type;

@end
@interface PlacePostDetailHeadFootView : UIView
@property(nonatomic,assign) id<PlacePostDetailHeadFootViewDelegate>delegate;
@property(nonatomic,strong) UIButton *commentButton;
@property(nonatomic,strong) UIButton *likeButton;
@property(nonatomic,strong) PostDetailObject*postDetail;
@end
