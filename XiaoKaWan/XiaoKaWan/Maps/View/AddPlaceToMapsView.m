//
//  AddPlaceToMapsView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/27/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AddPlaceToMapsView.h"
#import "NSString+Cagetory.h"
#import "UIColor+colorWithHex.h"
#import "AddPlaceToMapsViewModel.h"
#import "BJPlaceholderTextView.h"
#import <Masonry.h>
#import "QAlertView.h"
#import "MapsNetworkManage.h"
#import "UILabel+initLabel.h"
@interface AddPlaceToMapsView ()<AddPlaceToMapsViewModelDelegate,UITextViewDelegate>
@property(nonatomic,strong) UITableView *table;
@property(nonatomic,strong) AddPlaceToMapsViewModel *viewModel;
@property(nonatomic,strong) BJPlaceholderTextView *textView;
@property(nonatomic,strong) UIButton *saveButton;
@property(nonatomic,strong)  UIView *whiteBgView;
@property(nonatomic,strong) Maps *maps;
@property(nonatomic,strong) UILabel *titleLb;
@property(nonatomic,strong) UILabel *addressLb;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UIView *line1;
@property(nonatomic,strong) UIButton *buttonClose;
@property(nonatomic,strong) UILabel *noteLb;
@end
@implementation AddPlaceToMapsView{
    UIView *bgView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
        bgView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:0.8];
        [self addSubview:bgView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
        [bgView addGestureRecognizer:tapGesture];
        [self whiteBgView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
-(void)setPlace:(Place *)place{
    _place = place;
    self.titleLb.text = place.name;
    self.addressLb.text = place.addressDetail;
    
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.whiteBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(60);
        make.bottom.equalTo(self.mas_bottom).offset(-150);
        make.left.equalTo(self.mas_left).offset(30);
        make.right.equalTo(self.mas_right).offset(-30);
    }];
    [self.buttonClose mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView.mas_top).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left).offset(10);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    [self.titleLb mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(self.whiteBgView.mas_top).offset(10);
        make.left.equalTo(self.buttonClose.mas_right).offset(10);
        make.right.equalTo(self.whiteBgView.mas_right).offset(-50);
    }];
    
    [self.addressLb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left).offset(20);
        make.right.equalTo(self.whiteBgView.mas_right).offset(-20);
    }];
    [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLb.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left);
        make.right.equalTo(self.whiteBgView.mas_right);
        make.height.equalTo(@0.5);
    }];
    [self.textView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left).offset(10);
        make.right.equalTo(self.whiteBgView.mas_right).offset(-10);
        make.height.equalTo(@100);
    }];
    [self.noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left).offset(10);
    }];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom);
        make.right.equalTo(self.whiteBgView.mas_right).offset(-10);
        make.width.equalTo(@70);
    }];
    [self.line1 mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.saveButton.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView.mas_left);
        make.right.equalTo(self.whiteBgView.mas_right);
        make.height.equalTo(@0.5);
    }];
    [self.table mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line1.mas_bottom);
        make.left.equalTo(self.whiteBgView.mas_left).offset(0);
        make.right.equalTo(self.whiteBgView.mas_right).offset(0);
        make.bottom.equalTo(self.whiteBgView.mas_bottom).offset(-10);
    }];
}
- (AddPlaceToMapsViewModel *)viewModel{
    if(_viewModel == nil){
        _viewModel = [[AddPlaceToMapsViewModel alloc] initWithTable:_table];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
- (UIView *)whiteBgView{
    if(_whiteBgView == nil){
        _whiteBgView = [[UIView alloc] init];
        [self addSubview:_whiteBgView];
        _whiteBgView.layer.cornerRadius = 8;
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}
- (UIButton *)buttonClose{
    if(_buttonClose == nil){
        _buttonClose = [[UIButton alloc] init];
        _buttonClose.titleLabel.font = [UIFont fontWithName:@"iconfont" size:17];
        [_buttonClose setTitle:@"\U0000e632" forState:UIControlStateNormal];
        [_buttonClose setTitleColor:[UIColor p_colorWithHex:0x999999] forState:UIControlStateNormal];
        [_buttonClose addTarget:self action:@selector(disMiss) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteBgView addSubview:_buttonClose];
    }
    return _buttonClose;
}
- (UILabel *)titleLb{
    if(_titleLb == nil){
        _titleLb = [UILabel creatLabelWithFont:15 color:0x444444];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        [self.whiteBgView addSubview:_titleLb];
    }
    return _titleLb;
}
- (UILabel *)addressLb{
    if(_addressLb == nil){
        _addressLb = [UILabel creatLabelWithFont:11 color:0x999999];
        _addressLb.textAlignment = NSTextAlignmentCenter;
        [self.whiteBgView addSubview:_addressLb];
    }
    return _addressLb;
}
- (UIView *)line{
    if(_line == nil){
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        [self.whiteBgView addSubview:_line];
    }
    return _line;
}
- (UIView *)line1{
    if(_line1 == nil){
        _line1 = [[UIView alloc] init];
        _line1.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        [self.whiteBgView addSubview:_line1];
    }
    return _line1;
}
- (BJPlaceholderTextView *)textView{
    if(_textView == nil){
        _textView = [[BJPlaceholderTextView alloc] init];
        _textView.font = [UIFont systemFontOfSize:12];
        _textView.placeHolder = @"说说您收藏的理由....";
        _textView.returnKeyType = UIReturnKeyDone;
        _textView.delegate = self;
        [self.whiteBgView addSubview:_textView];
    }
    return _textView;
}
- (UILabel *)noteLb{
    if(_noteLb == nil){
        _noteLb = [UILabel creatLabelWithFont:11 color:0x999999];
        _noteLb.text = @"选择专辑";
        [self.whiteBgView addSubview:_noteLb];
    }
    return _noteLb;
}
- (UIButton *)saveButton{
    if(_saveButton == nil){
        _saveButton = [[UIButton alloc] init];
        [_saveButton setBackgroundColor:[UIColor redColor]];
        [_saveButton setTitle:@"保存" forState:UIControlStateNormal];
        [_saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveButton.layer.cornerRadius = 5;
        _saveButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_saveButton addTarget:self action:@selector(addPlaceToMaps) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteBgView addSubview:_saveButton];
    }
    return _saveButton;
}
- (UITableView *)table{
    if(_table == nil){
        _table = [[UITableView alloc]init];
        _table.delegate = self.viewModel;
        _table.dataSource = self.viewModel;
        [_whiteBgView addSubview:_table];
        _table.separatorStyle = 0;
    }
    return _table;
}
- (void)gestureAction:(UITapGestureRecognizer*)tap{
    [self disMiss];
}
- (void)showViewInSupperView:(UIView *)view{
    [view addSubview:self];
}
- (void)disMiss{
    [self removeFromSuperview];
}
- (void)addPlaceToMaps{
    [self.textView resignFirstResponder];
//    if(self.textView.text.length<=0){
//        [[QAlertView sharedInstance] showAlertText:@"请填写添加理由" fadeTime:2];
//        return;
//    }
    if(self.maps == nil){
        [[QAlertView sharedInstance] showAlertText:@"请选择一个榜单" fadeTime:2];
        return;
    }
    PlaceParameter *placeParameter = [[PlaceParameter alloc] init];
    placeParameter.place_id = self.place.placeId;
    placeParameter.maps_id = [NSString stringWithFormat:@"%@",self.maps.id];
    placeParameter.add_reason = self.textView.text;
    [[MapsNetworkManage sharedInstance] addPlaceToMapsWithParameter:placeParameter complete:^(BOOL succeed, id obj) {
         if(self.isAddToMaps)self.isAddToMaps(YES);
        if(succeed){
            [[NSNotificationCenter defaultCenter] postNotificationName:BJMapsChangedNotification object:nil];
            [[QAlertView sharedInstance] showAlertText:@"添加成功" fadeTime:2];
            self.textView.text =@"";
            [self disMiss];
        }else{
            [[QAlertView sharedInstance] showAlertText:obj fadeTime:2];
        }
    }];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [self.textView resignFirstResponder];
    }
    return YES;
}
#pragma mark - AddPlaceToMapsViewModelDelegate
- (void)selectMaps:(Maps *)maps{
    _maps = maps;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
