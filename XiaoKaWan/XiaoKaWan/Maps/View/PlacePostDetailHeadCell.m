//
//  PlacePostDetailHeadCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailHeadCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "PlacePostDetailBigImageViewModel.h"
#import "PlacePostDetailSmallImageViewModel.h"
#import "ImageSizeUrl.h"
#import "PostTagsViewModel.h"
#define headViewHeight 45
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@interface PlacePostDetailHeadCell ()<PlacePostDetailSmallImageViewModelDelegate,BeenWantViewDelegate>
@property(nonatomic,strong) PlacePostDetailBigImageViewModel *bigImageViewModel;
@property(nonatomic,strong) PlacePostDetailSmallImageViewModel *smallImageViewModel;
@property(nonatomic,strong) PostTagsViewModel *tagViewModel;
@end
@implementation PlacePostDetailHeadCell{
   
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self smallImageView];
        [self tagView];
        [self bigImageView];
    }
    return self;
}
- (void)setPostObject:(PostDetailObject *)postObject{
    _postObject = postObject;
    postObject.placeInfo.placeId = [NSString stringWithFormat:@"%ld",(long)postObject.placeInfo.id];
    [self.headImageView setAvatarImageWithUrlString:[ImageSizeUrl avatar120:postObject.user.avatar]];
    self.titleLabel.text = postObject.user.nick;
    self.timeLabel.text = postObject.gmtCreated;
    self.detailLabel.text = postObject.content;
    _smallImageViewModel.imageList = postObject.photos;
    _bigImageViewModel.imageArray = postObject.photos;
    Place *palce = postObject.placeInfo;
    self.addressView.addressNameLb.text = palce.name;
    self.addressView.addressLb.text = palce.address;
    self.beenWantView.place = postObject.placeInfo;
    [self setNeedsUpdateConstraints];
    
    self.tagViewModel.tagList = _postObject.tags;
}
- (void)updateConstraints{
    [super updateConstraints];
    CGFloat width;
    CGFloat height;
    CGFloat tagViewHeight;
    if(_postObject.tags.count>0) tagViewHeight = 30;
    else tagViewHeight = 0;
    if(_postObject.photos.count>0){
        width = 355;
        height = 50;
    }
    else {
        height = 0;
        width = 0;
    }
    [self.headImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(21);
        make.left.equalTo(self.contentView.mas_left).offset(14);
        make.height.equalTo(@headViewHeight);
        make.width.equalTo(@headViewHeight);
    }];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(21);
        make.left.equalTo(self.headImageView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(7);
        make.left.equalTo(self.headImageView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLabel.mas_bottom).offset(15);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
    [self.bigImageView.view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLabel.mas_bottom).offset(30);
        make.left.equalTo(self.contentView.mas_left).offset((kDeviceWidth-355)/2);
        make.height.equalTo(@(width));
        make.width.equalTo(@(width));
    }];
    [self.smallImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bigImageView.view.mas_bottom).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset((kDeviceWidth-355)/2);
        make.height.equalTo(@(height));
        make.width.equalTo(@(width));
    }];
    [self.tagView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.smallImageView.mas_bottom).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset((kDeviceWidth-355)/2);
        make.height.equalTo(@(tagViewHeight));
        make.width.equalTo(@(355));
    }];
    [self.addressView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tagView.mas_bottom).offset(9);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@40);
    }];
    [self.beenWantView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressView.mas_bottom).offset(30);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@30);
        
    }];
}
- (BJUIImageView*)headImageView{
    if(_headImageView == nil){
        _headImageView =[[BJUIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGstureToUser:)];
        [_headImageView addGestureRecognizer:tap];
        [_headImageView setUserInteractionEnabled:YES];
    }
    return _headImageView;
}
- (void)tapGstureToUser:(UITapGestureRecognizer *)tap{
    [self.delegate touserDetail:_postObject.user];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:16 color:0x333333];
       
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)timeLabel
{
    if(_timeLabel==nil){
        _timeLabel = [UILabel creatLabelWithFont:11 color:0x939393];
        [self.contentView addSubview:_timeLabel];
    }
    return _timeLabel;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:15 color:0x393939];
//        _detailLabel.text = @"s看看都没法看到了父母过都快某个时刻老地方动力方面各类贷款买房";
        _detailLabel.numberOfLines = 0;
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UIPageViewController *)bigImageView{
    if(_bigImageView == nil){
        NSDictionary *option = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:5] forKey:UIPageViewControllerOptionInterPageSpacingKey];
        _bigImageView = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:option];
        _bigImageView.delegate = self.bigImageViewModel;
        _bigImageView.dataSource = self.bigImageViewModel;
        [self.contentView addSubview:_bigImageView.view];
    }
    return _bigImageView;
}
- (PlacePostDetailBigImageViewModel *)bigImageViewModel{
    if(_bigImageViewModel ==nil){
        _bigImageViewModel = [[PlacePostDetailBigImageViewModel alloc] initWithPageController:self.bigImageView];
        _bigImageViewModel.delegate  = self.smallImageViewModel;
    }
    return _bigImageViewModel;
}
- (UICollectionView *)smallImageView{
    if(_smallImageView == nil){
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _smallImageView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
        _smallImageView.delegate = self.smallImageViewModel;
        _smallImageView.dataSource = self.smallImageViewModel;
        _smallImageView.backgroundColor = [UIColor whiteColor];
        _smallImageView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_smallImageView];
    }
    return _smallImageView;

}
- (UICollectionView *)tagView{
    if(_tagView == nil){
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _tagView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
        _tagView.delegate = self.tagViewModel;
        _tagView.dataSource = self.tagViewModel;
        _tagView.backgroundColor = [UIColor whiteColor];
        _tagView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_tagView];
    }
    return _tagView;
}
- (PostTagsViewModel *)tagViewModel{
    if(_tagViewModel == nil){
        _tagViewModel = [[PostTagsViewModel alloc] initWithTags:_postObject.tags collectView:_tagView];
    }
    return _tagViewModel;
}
- (PlacePostDetailSmallImageViewModel *)smallImageViewModel{
    if(_smallImageViewModel == nil){
        _smallImageViewModel = [[PlacePostDetailSmallImageViewModel alloc] initWithModel:nil collectionView:_smallImageView];
        _smallImageViewModel.delegate = self;
    }
    return _smallImageViewModel;
}
- (PlacePostLocationView*)addressView
{
    if(_addressView==nil){
        _addressView = [[PlacePostLocationView alloc] init];
        __weak typeof(self) weakSelf = self;
        _addressView.touchAction = ^(){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.delegate toPlaceDetail:strongSelf.postObject.placeInfo];
        };
        [self.contentView addSubview:_addressView];
    }
    return _addressView;
}
- (BeenWantView *)beenWantView{
    if(_beenWantView == nil){
        _beenWantView = [[BeenWantView alloc] init];
        _beenWantView.delegate = self;
        [self.contentView addSubview:_beenWantView];
        _beenWantView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [_beenWantView addGestureRecognizer:tapGesture];
    }
    return _beenWantView;
}

- (void)tapGesture:(UITapGestureRecognizer *)gesture{
    [self.delegate toUserList:self.postObject];
}
//- (PlaceFlagButton*)beenFlagView{
//    if(_beenFlagView == nil){
//        _beenFlagView = [[PlaceFlagButton alloc] init];
//        _beenFlagView.title = @"去过";
//        _beenFlagView.iconText = @"\U0000e604";
//        _beenFlagView.textColor = [UIColor redColor];
//        [self.contentView addSubview:_beenFlagView];
//    }
//    return _beenFlagView;
//}
//- (PlaceFlagButton*)wantFlagView{
//    if(_wantFlagView == nil){
//        _wantFlagView = [[PlaceFlagButton alloc] init];
//        _wantFlagView.title = @"想去";
//        _wantFlagView.iconText = @"\U0000e60a";
//        _wantFlagView.textColor = [UIColor p_colorWithHex:0x66ccff];
//        [self.contentView addSubview:_wantFlagView];
//    }
//    return _wantFlagView;
//}
#pragma mark - PlacePostDetailSmallImageViewModelDelegate
- (void)selectItemtAtIndex:(NSInteger)index{
    self.bigImageViewModel.selectAtIndex = index;
}
#pragma mark - BeenWantViewDelegate
- (void)beenAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate beenAction:place callBack:place1];
}
- (void)wantAction:(Place *)place callBack:(void (^)(Place *))place1{
    [self.delegate wantGoAction:place callBack:place1];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
