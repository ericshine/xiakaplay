//
//  PlaceFlagButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceFlagButton.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#import "UILabel+initLabel.h"
@implementation PlaceFlagButton
- (void)setModel:(PlaceFlageObj *)model{
    self.groupHeadView.totalNumber = model.totalNumber;
    [self.groupHeadView setImageUrlArray:model.arrayUrl];
}
- (void)setTitle:(NSString *)title{
     [self.actionButton setTitle:title forState:UIControlStateNormal];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.groupHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@30);
        
    }];
    [self.actionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.groupHeadView.mas_bottom).offset(6);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@31);
    }];
    [self.flagLabelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.actionButton.mas_bottom).offset(-10);
        make.left.equalTo(self.actionButton.mas_left);
    }];

}
- (BJGroupHeadImageView*)groupHeadView
{
    if(_groupHeadView==nil){
        _groupHeadView = [[BJGroupHeadImageView alloc] init];
        _groupHeadView.userInteractionEnabled = YES;
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gesture:)];
        [_groupHeadView addGestureRecognizer:gesture];
        _groupHeadView.countLimit = 3;
        _groupHeadView.headViewType = HEADVIEWTYPE_SHOWNUMBER;
        [self addSubview:_groupHeadView];
    }
    return _groupHeadView;
}
- (UILabel*)flagLabelIcon
{
    if(_flagLabelIcon==nil){
        _flagLabelIcon = [UILabel creatLabelWithFont:40 color:0xffffff];
//        _flagLabelIcon.textColor = [UIColor redColor];
        _flagLabelIcon.font = [UIFont fontWithName:@"iconfont" size:45];
        _flagLabelIcon.userInteractionEnabled = NO;
        //        _flagLabelIcon.text = @"\U0000e604";
        [self addSubview:_flagLabelIcon];
    }
    return _flagLabelIcon;
}
- (UIButton*)actionButton
{
    if(_actionButton==nil){
        _actionButton = [self creatButton];
        _actionButton.tag = 40;
        [self addSubview:_actionButton];
    }
    return _actionButton;
}
- (UIButton *)creatButton{
    UIButton *button = [[UIButton alloc] init];
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor p_colorWithHex:0xbbbbbb].CGColor;
    [button setTitleColor:[UIColor p_colorWithHex:0x545454] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    button.layer.cornerRadius = 5;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
- (void)buttonAction:(UIButton *)button{
    
        if(button.selected){
            [self disSelectAnimation:self.flagLabelIcon];
        }else{
            self.flagLabelIcon.text = self.iconText;
            self.flagLabelIcon.textColor = self.textColor;
            [self selectAnimation:self.flagLabelIcon];
        }
   
    button.selected=!button.selected;
}
- (void)gesture:(UITapGestureRecognizer*)tap{
    if([self.delegate respondsToSelector:@selector(usersList)]){
        [self.delegate usersList];
    }
}
- (void)selectAnimation:(UILabel *)label{
    [UIView animateWithDuration:0.3 animations:^{
        label.hidden = NO;
        label.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            label.transform = CGAffineTransformIdentity;
        }];
        
    }];
    
}
- (void)disSelectAnimation:(UILabel *)label{
    [UIView animateWithDuration:0.3 animations:^{
        label.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            label.transform = CGAffineTransformMakeScale(0.2, 0.2);
            label.hidden = YES;
        }];
        
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
@implementation PlaceFlageObj

@end
