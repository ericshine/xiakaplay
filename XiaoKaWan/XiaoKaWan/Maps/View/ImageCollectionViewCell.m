//
//  ImageCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ImageCollectionViewCell.h"
#import <Masonry.h>
@implementation ImageCollectionViewCell
-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
- (BJUIImageView*)imageView{
    if(_imageView==nil){
        _imageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}
@end
