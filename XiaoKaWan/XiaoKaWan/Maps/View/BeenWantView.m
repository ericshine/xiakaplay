//
//  BeenWantView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BeenWantView.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation BeenWantView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setPlace:(Place *)place{
    _place = place;
    self.headGroupView.countLimit = 6;
    NSInteger totalNumner = 0;
    NSMutableArray *users = [[NSMutableArray alloc] initWithCapacity:0];
    /**
     *  业务逻辑：我去过的 但不想去只显示去过的，没去过但是想去只显示想去的，即去过也想去显示全部的
     *
     */
    if((place.isBeen == YES && place.isWant == YES)||(place.isBeen == NO && place.isWant == NO)){
        [users addObjectsFromArray:place.beenUsers];
        [users addObjectsFromArray:place.wantUsers];
        totalNumner = place.beenCount + place.wantCount;
    }
    if(place.isWant ==YES && place.isBeen == NO) {
        totalNumner = place.wantCount;
        [users addObjectsFromArray:place.wantUsers];
    }
    if(place.isWant == NO && place.isBeen == YES){
        totalNumner = place.beenCount;
        [users addObjectsFromArray:place.beenUsers];
    }
    self.headGroupView.totalNumber = totalNumner;
    NSMutableArray *urls = [[NSMutableArray alloc] initWithCapacity:users.count];
    [users enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        User *user = obj;
        NSString *urlStr = (user.avatar == nil)?@"":user.avatar;
        if(![urls containsObject:urlStr]) [urls addObject:urlStr];
    }];
    [self.headGroupView setImageUrlArray:[urls copy]];
    
    [self setButton:self.beenButton withBool:place.isBeen];
    [self setButton:self.wantButton withBool:place.isWant];
    self.wantNumber = place.wantCount;
    self.beenNumber = place.beenCount;
}
- (void)setIsBeen:(BOOL)isBeen{
    [self setButton:_beenButton withBool:isBeen];
}
- (void)setIsWant:(BOOL)isWant{
    [self setButton:_wantButton withBool:isWant];
}
- (void)setButton:(UIButton *)button withBool:(BOOL)isTrue{
    if(isTrue){
        button.layer.borderWidth = 0;
        button.layer.borderColor = [UIColor p_colorWithHex:0xffda00].CGColor;
        [button setBackgroundColor:[UIColor p_colorWithHex:0xffda00]];
    }else{
        button.layer.borderWidth = 1;
        button.layer.borderColor = [UIColor p_colorWithHex:0x707070].CGColor;
        [button setBackgroundColor:[UIColor whiteColor]];
    }
}
- (void)setWantNumber:(NSInteger)wantNumber{
    if(wantNumber<=0){
        [_wantButton setTitle:@"想去" forState:UIControlStateNormal];
    }else{
        [_wantButton setTitle:[NSString stringWithFormat:@"想去\n%li",(long)wantNumber] forState:UIControlStateNormal];
    }
}
- (void)setBeenNumber:(NSInteger)beenNumber{
    if(beenNumber<=0){
        [_beenButton setTitle:@"去过" forState:UIControlStateNormal];
    }else{
        [_beenButton setTitle:[NSString stringWithFormat:@"去过\n%li",(long)beenNumber] forState:UIControlStateNormal];
    }

}
- (void)updateConstraints{
    [super updateConstraints];
    
    [self.wantButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.width.equalTo(@52);
        make.bottom.equalTo(self.mas_bottom);
    }];
    [self.beenButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.wantButton.mas_left).offset(-5);
        make.width.equalTo(@52);
        make.bottom.equalTo(self.mas_bottom);
    }];
    [self.headGroupView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.beenButton.mas_left).offset(-10);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (BJGroupHeadImageView*)headGroupView
{
    if(_headGroupView==nil){
        _headGroupView = [[BJGroupHeadImageView alloc] init];
        _headGroupView.headViewType = HEADVIEWTYPE_SHOWNUMBER;
        [self addSubview:_headGroupView];
    }
    return _headGroupView;
}
- (UIButton*)beenButton
{
    if(_beenButton==nil){
        _beenButton = [[UIButton alloc] init];
        _beenButton.titleLabel.font =[UIFont systemFontOfSize:11];
        _beenButton.titleLabel.numberOfLines = 2;
        _beenButton.titleLabel.textAlignment = NSTextAlignmentCenter;
         _beenButton.layer.cornerRadius = 5;
        [_beenButton setTitle:@"去过" forState:UIControlStateNormal];
        [_beenButton setTitleColor:[UIColor p_colorWithHex:0x1c1c1c] forState:UIControlStateNormal];
        [_beenButton addTarget:self action:@selector(beenAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_beenButton];
    }
    return _beenButton;
}
- (UIButton*)wantButton
{
    if(_wantButton==nil){
        _wantButton = [[UIButton alloc] init];
         _wantButton.layer.cornerRadius = 5;
        _wantButton.titleLabel.font =[UIFont systemFontOfSize:11];
        _wantButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _wantButton.titleLabel.numberOfLines = 2;
        [_wantButton setTitleColor:[UIColor p_colorWithHex:0x1c1c1c] forState:UIControlStateNormal];
        [_wantButton setTitle:@"想去" forState:UIControlStateNormal];
        [_wantButton addTarget:self action:@selector(wantAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_wantButton];
    }
    return _wantButton;
}
- (void)beenAction:(UIButton *)button{
    [self.delegate beenAction:self.place callBack:^(Place *place) {
        self.place = place;
    }];
}
- (void)wantAction:(UIButton *)button{
    [self.delegate wantAction:self.place callBack:^(Place *place) {
        self.place = place;
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
