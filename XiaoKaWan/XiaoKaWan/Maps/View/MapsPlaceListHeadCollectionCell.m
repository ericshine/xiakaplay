//
//  MapsPlaceListHeadCollectionCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsPlaceListHeadCollectionCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@interface MapsPlaceListHeadCollectionCell()
@property(nonatomic,strong) UIButton *changeButton;
@end
@implementation MapsPlaceListHeadCollectionCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(37);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-134);//134
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(23);
        make.left.equalTo(self.contentView.mas_left).offset(20);
    }];
    [self.changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-17);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
        make.width.equalTo(@54);
        make.height.equalTo(@54);
    }];
    
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:24 color:0x555555];
        _titleLabel.numberOfLines = 2;
       // _titleLabel.text = @"大家可适当降分关键是对方今生的开发规划设计看到符合公司觉得好";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)detailLabel
{
    if(_detailLabel==nil){
        _detailLabel = [UILabel creatLabelWithFont:18 color:0xb5b5b5];
        _detailLabel.text = @"地点  45";
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}
- (UIButton *)changeButton{
    if(_changeButton == nil){
        _changeButton = [[UIButton alloc] init];
        _changeButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:30];
        [_changeButton setTitle:@"\U0000e608" forState:UIControlStateNormal];
        [_changeButton setTitle:@"\U0000e606" forState:UIControlStateSelected];
        [_changeButton setTitleColor:[UIColor p_colorWithHex:0xfbe47a] forState:UIControlStateNormal];
                [_changeButton setTitleColor:[UIColor p_colorWithHex:0xfbe47a] forState:UIControlStateSelected];
        [_changeButton addTarget:self action:@selector(changButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_changeButton];
    }
    return _changeButton;
}
- (void)changButtonAction:(UIButton *)button{
    button.selected = !button.selected;
    if(button.selected){
        [self.delegate changeModel:PLACEtYPE_DETAIL];
    }else [self.delegate changeModel:PLACETYPE_SIMPLE];
}
@end
