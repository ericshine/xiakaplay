//
//  ImageCollectionViewCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
@interface ImageCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)BJUIImageView *imageView;
@end
