//
//  PlacePostDetailTitleCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlacePostDetailTitleCell.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define titleWidth 60
@implementation PlacePostDetailTitleCell
- (void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.left.equalTo(self.contentView.mas_left);
        make.height.equalTo(@2);
        make.width.equalTo(@((kDeviceWidth-titleWidth)/2));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.leftView.mas_right);
        make.width.equalTo(@titleWidth);

    }];
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.right.equalTo(self.contentView.mas_right);
        make.height.equalTo(@2);
        make.width.equalTo(@((kDeviceWidth-titleWidth)/2));
    }];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:11 color:0xc6c6c6];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UIImageView*)leftView
{
    if(_leftView==nil){
        _leftView = [[UIImageView alloc] init];
        _leftView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_leftView];
    }
    return _leftView;
}
- (UIImageView*)rightView
{
    if(_rightView==nil){
        _rightView = [[UIImageView alloc] init];
        _rightView.image = [UIImage imageNamed:@"mapsLine"];
        [self.contentView addSubview:_rightView];
    }
    return _rightView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
