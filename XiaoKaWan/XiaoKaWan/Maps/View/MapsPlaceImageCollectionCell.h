//
//  MapsPlaceImageCollectionCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
@interface MapsPlaceImageCollectionCell : UICollectionViewCell
@property(nonatomic,strong) BJUIImageView *imageView;
@end
