//
//  MapsPlaceListHeadCollectionCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/2/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,PlaceType){
    PLACETYPE_SIMPLE = 0,
    PLACEtYPE_DETAIL
};
@protocol MapsPlaceListHeadCollectionCellDelegate <NSObject>

- (void)changeModel:(PlaceType)placeType;

@end
@interface MapsPlaceListHeadCollectionCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,assign)id<MapsPlaceListHeadCollectionCellDelegate>delegate;
@end

