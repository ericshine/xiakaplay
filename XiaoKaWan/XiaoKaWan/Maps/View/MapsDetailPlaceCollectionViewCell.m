//
//  MapsDetailPlaceCollectionViewCell.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/19/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "MapsDetailPlaceCollectionViewCell.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
@implementation MapsDetailPlaceCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
        _imageArray = [NSMutableArray arrayWithCapacity:0];
//       
//        for (int i =0; i<4; i++) {
//            UIImageView *imageView = [[UIImageView alloc] init];
//            imageView.image = [UIImage imageNamed:@"image7"];
//            [self.contentView addSubview:imageView];
//            [_imageArray addObject:imageView];
//        }
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@20);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-80);
        
    }];
    [self.havedGoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLabel.mas_bottom);
        make.left.equalTo(self.contentView.mas_left).offset(10);
    }];
    [self.wantGoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLabel.mas_bottom);
        make.left.equalTo(self.havedGoLabel.mas_right);
    }];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.wantGoLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.height.equalTo(@40);
        make.width.equalTo(@40);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.wantGoLabel.mas_bottom).offset(10);
        make.left.equalTo(self.iconView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    NSInteger number = 4;
    CGFloat widht = 70;
   // CGFloat space = (kDeviceWidth - number*widht)/(number+1);
//    [_imageArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        UIImageView *imageView = (UIImageView *)obj;
//       [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//           make.left.equalTo(self.contentView.mas_left).offset(space + (widht +space)*idx);
//           make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
//           make.width.equalTo(@(widht));
//           make.height.equalTo(@(widht));
//       }];
//    }];
}
- (UILabel*)titleLabel
{
    if(_titleLabel==nil){
        _titleLabel = [UILabel creatLabelWithFont:16 color:0x444444];
        _titleLabel.textColor = [UIColor blueColor];
        _titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        _titleLabel.text = @"为名湖畔";
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
- (UILabel*)addressLabel
{
    if(_addressLabel==nil){
        _addressLabel = [UILabel creatLabelWithFont:14 color:0x4c4c4c];
         _addressLabel.text = @"上海市东大街";
        [self.contentView addSubview:_addressLabel];
    }
    return _addressLabel;
}
- (UILabel*)havedGoLabel
{
    if(_havedGoLabel==nil){
        _havedGoLabel = [UILabel creatLabelWithFont:14 color:0x4c4c4c];
        _havedGoLabel.text = @"99人去过";
        [self.contentView addSubview:_havedGoLabel];
    }
    return _havedGoLabel;
}
- (UILabel*)wantGoLabel
{
    if(_wantGoLabel ==nil){
        _wantGoLabel = [UILabel creatLabelWithFont:14 color:0x4c4c4c];
        _wantGoLabel.text = @"120人想去";
        [self.contentView addSubview:_wantGoLabel];
    }
    return _wantGoLabel;
}
- (UIImageView *)iconView
{
    if(_iconView == nil){
        _iconView = [[UIImageView alloc] init];
        _iconView.image = [UIImage imageNamed:@"shuaxin2"];
        [self.contentView addSubview:_iconView];
    }
    return _iconView;
}
- (UILabel*)contentLabel
{
    if(_contentLabel==nil){
        _contentLabel = [UILabel creatLabelWithFont:14 color:0x4c4c4c];
        _contentLabel.numberOfLines = 2;
        _contentLabel.text = @"十足的家常饭馆，十足的家常饭馆，十足的家常饭馆，十足的家常饭馆，十足的家常饭馆，十足的家常饭馆";
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}
- (BJGroupHeadImageView *)groupHeadView
{
    if(_groupHeadView == nil){
        _groupHeadView = [[BJGroupHeadImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-70, 10, 60, 60) andGroups:@[@"http://vimg3.ws.126.net/image/snapshot/2016/5/5/S/VBLI6CF5S.jpg",@"http://vimg3.ws.126.net/image/snapshot/2016/5/5/S/VBLI6CF5S.jpg"]];
        [self.contentView addSubview:_groupHeadView];
    }
    return _groupHeadView;
}
@end
