//
//  MapsHomeCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"
#import "BJGroupHeadImageView.h"
#import "MapsHomeCellViewModel.h"
#import "Maps.h"
@protocol MapsHomeCellDelegate <NSObject>

- (void)touchMapsWithModel:(id)model;

@end
@interface MapsHomeCell : BJTableViewCell<MapsHomeCellViewModelDelegate>
@property(nonatomic,assign)id<MapsHomeCellDelegate>delegate;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIButton *placeNumberIcon;
@property(nonatomic,strong)UIButton *dynamicNumberIcon;
@property(nonatomic,strong)UIButton *followNumberIcon;
@property(nonatomic,strong)UILabel *placeNumberLabel;
@property(nonatomic,strong)UILabel *dynamicNumberLabel;
@property(nonatomic,strong)UILabel *followNumberLabel;
@property(nonatomic,strong)BJGroupHeadImageView *groupHeadView;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UICollectionView *collectView;
@property(nonatomic,strong)MapsHomeCellViewModel *modelView;
@property(nonatomic,strong) Maps *maps;
@property(nonatomic,strong) NSIndexPath *indexPath;
+ (instancetype)initCellWithTabel:(UITableView*)tableView;
@end
