//
//  UserSelectCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 8/4/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTableViewCell.h"

@interface MapsSelectCell : BJTableViewCell
@property(nonatomic,strong) BJUIImageView *headImageView;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong)UILabel *selectLb;
@end
