//
//  PlaceTitleNumberButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "PlaceTitleNumberButton.h"
#import "UILabel+initLabel.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>
@implementation PlaceTitleNumberButton
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setTitleStr:(NSString *)titleStr{
    self.titleLab.text = titleStr;
}
- (void)setNumnerStr:(NSString *)numnerStr{
    self.numberLab.text = numnerStr;
}
- (void)updateConstraints{
    [super updateConstraints];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@15);
    }];
    [self.numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@15);
    }];
}
- (UILabel*)titleLab
{
    if(_titleLab==nil){
        _titleLab = [UILabel creatLabelWithFont:11 color:0x1c1c1c];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLab];
    }
    return _titleLab;
}
- (UILabel*)numberLab
{
    if(_numberLab==nil){
        _numberLab = [UILabel creatLabelWithFont:10 color:0x1c1c1c];
        _numberLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_numberLab];
    }
    return _numberLab;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
