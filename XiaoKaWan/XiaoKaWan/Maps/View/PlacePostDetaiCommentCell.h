//
//  PlacePostDetaiCommentCell.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJIconfontButton.h"
#import "CommetObject.h"
#import "BJUIImageView.h"
@protocol PlacePostDetaiCommentCellDelegate <NSObject>

- (void)likeComment:(CommetObject*)commentObj commentObj:(void(^)(CommetObject*commentObj))commentObj;

@end
@interface PlacePostDetaiCommentCell : UITableViewCell
@property(nonatomic,strong)id<PlacePostDetaiCommentCellDelegate>delelgate;
@property(nonatomic,strong)BJUIImageView *headView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *contentLabel;
@property(nonatomic,strong)BJIconfontButton *likeButton;
@property(nonatomic,strong)CommetObject *commentObject;
@end

