//
//  BeenWantView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/23/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJGroupHeadImageView.h"
#import "Place.h"
@protocol BeenWantViewDelegate <NSObject>

- (void)beenAction:(Place *)place callBack:(void(^)(Place *place))place1;
- (void)wantAction:(Place *)place callBack:(void(^)(Place *place))place1;

@end
@interface BeenWantView : UIView
@property(nonatomic,assign) id<BeenWantViewDelegate>delegate;
@property(nonatomic,strong) BJGroupHeadImageView *headGroupView;
@property(nonatomic,strong) UIButton *beenButton;
@property(nonatomic,strong) UIButton *wantButton;
@property(nonatomic) BOOL isWant;
@property(nonatomic) BOOL isBeen;
@property(nonatomic) NSInteger beenNumber;
@property(nonatomic) NSInteger wantNumber;
@property(nonatomic,strong)Place *place;
@end
