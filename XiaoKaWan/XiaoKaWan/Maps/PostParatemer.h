//
//  PostParatemer.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/21/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostParatemer : NSObject
@property(nonatomic,copy) NSString *post_id;
@property(nonatomic,copy) NSString *last_id;
@property(nonatomic,copy) NSString *count;
@property(nonatomic,copy) NSString *get_type;
@property(nonatomic,copy) NSString *hot_count; //热门评论数
@property(nonatomic,copy) NSString *content;// 评论
@property(nonatomic,copy) NSString *_t;
@property(nonatomic,copy) NSString *comment_id;
@property(nonatomic,copy) NSString *is_remove;
+ (PostParatemer *)parameter;
+ (PostParatemer *)parameterComment;
@end
