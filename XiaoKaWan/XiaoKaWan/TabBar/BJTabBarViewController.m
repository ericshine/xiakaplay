//
//  BJTabBarViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTabBarViewController.h"
#import "BJNavigationController.h"
#import "MeHomeViewController.h"
#import "MapsHomeViewController.h"
#import "NewsCenterViewController.h"
#import "BJNotificationVC.h"
@interface BJTabBarViewController ()<UITabBarControllerDelegate>

@property (nonatomic,assign)NSInteger lastIndex;

@end

@implementation BJTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBarController];
    self.delegate=self;
    // Do any additional setup after loading the view.
}
#pragma mark - set tabBarItems
- (void)initBarController
{
    NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"TabBarList" ofType:@"plist"];
    NSArray *controllers = [[NSArray alloc] initWithContentsOfFile:dataPath];
    [controllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIViewController *controller = [[NSClassFromString(obj[@"controller"]) alloc] init];
        
        BJNavigationController *nav = [[BJNavigationController alloc] initWithRootViewController:controller];
//        nav.tabBarItem.title = NSLocalizedString(obj[@"title"], nil);
        [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[UIFont systemFontOfSize:16]} forState:UIControlStateNormal];
        [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:16]} forState:UIControlStateSelected];
        [nav.tabBarItem setImage:[[UIImage imageNamed:obj[@"icon_n"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        nav.tabBarItem.selectedImage = [[UIImage imageNamed:obj[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
     //   nav.tabBarItem.imageInsets = UIEdgeInsetsMake(0, 0, -5, 0);
        [self addChildViewController:nav];
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 控制底部红点提醒
 **/
-(void)tabBarController:(BJTabBarViewController *)tabBarController didSelectViewController:(nonnull UIViewController *)viewController
{
    NewsCenterViewController *newCenterVC= ((BJNavigationController *)tabBarController.childViewControllers[1]).childViewControllers[0];
    
    BJNotificationVC *NotificationVC;
    
    NSInteger currentIndex=self.selectedIndex;
    
    if ((currentIndex==1)&&(self.lastIndex!=currentIndex)) {
        if (newCenterVC.pageIndex==1) {
            NotificationVC= newCenterVC.childViewControllers[1];
            [NotificationVC showOnScreen];
        }
    }
    if (self.lastIndex==1&&currentIndex!=self.lastIndex) {
        if (newCenterVC.pageIndex==1) {
            NotificationVC= newCenterVC.childViewControllers[1];
            [NotificationVC dismissOnScreen];
        }
    }
    
    self.lastIndex=currentIndex;
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
