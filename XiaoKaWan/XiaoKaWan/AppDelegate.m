//
//  AppDelegate.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "AppDelegate.h"
#import "BJTabBarViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "LanchView.h"
#import "LoginViewController.h"
#import "BJNavigationController.h"
#import <Bugtags/Bugtags.h>
#import <UMSocial.h>
#import <UMSocialSinaSSOHandler.h>
#import "UMSocialQQHandler.h"
#import "UMSocialWechatHandler.h"
#import "UMMobClick/MobClick.h"
#import "GeTuiSdk.h"
#import <RHAddressBook/AddressBook.h>
#import "UserConfig.h"
#import "UMessage.h"
#import "BJNotificationTool.h"
#import <UIImageView+WebCache.h>
#import "BJNotificationDBTool.h"
#import "BJDBToolParameter.h"
#import "BJNotificationModel.h"

#define baiDuKey @"nH4c5GK40k768rRhnkiUj1a0nIFEbztp"

#define bugsKey @"15a7e57c8003d71fb8a698681238bc89"

#define UmengAppKey @"575d5600e0f55a80c90013cc"
#define sinaAppKey @"4099019807"

//#define kGtAppId           @"dScGk0gXEY86L3xiqvws02"
//#define kGtAppKey          @"9wvmdc5g6m7KNRKkcGfu15"
//#define kGtAppSecret       @"MiAu8517G46ZiB4dC607j1"
BMKMapManager* _mapManager;
@interface AppDelegate ()<GeTuiSdkDelegate,UIApplicationDelegate>

@end

@implementation AppDelegate



-(void)addressBookChang
{
    NSLog(@"++%s--",__func__);
}

-(void)test
{
    UIViewController *VC=[[UIViewController alloc]init];
    UIWebView *webView=[[UIWebView alloc]initWithFrame:VC.view.bounds];
    [VC.view addSubview:webView];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.baidu.com"]]];
    
    [((BJTabBarViewController *)self.window.rootViewController).viewControllers[0] pushViewController:VC animated:YES];
    NSLog(@"%@",((BJTabBarViewController *)self.window.rootViewController).viewControllers[0]);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(addressBookChang) name:RHAddressBookExternalChangeNotification object:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor=[UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    /**
     *  设置友盟
     */
    [self setupUmeng];
    /**
     *  设置友盟推送
     */
    [self setupUmengPushWith:launchOptions];
    /**
     *  添加网络监测
     */
    [self startMonitorNetNetwork];
    /**
     *  注册地图apiKey
     */
    [self registMapAppKey];
//# ifdef RELEASE
    /**
     *  bugs
     */
    [self startBugtags];
//# endif
   
    /**
     *  主页面
     */
    [self mainTaBarController];
    
    /**
     *  收到通知后做些什么
     */
    if ([launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]) {
        [BJNotificationTool DealWithNotification:[launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]];
    }
    /**
     *  拉去通知数据
     */
    [self pullNotificationData];
    
    return YES;
}
- (void)mainTaBarController{
        BJTabBarViewController *tabBarController = [[BJTabBarViewController alloc]init];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];

}
- (void)loginViewController{
    LoginViewController *loginViewController = [[LoginViewController alloc]init];
    self.window.rootViewController = [[BJNavigationController alloc] initWithRootViewController:loginViewController];
    [self.window makeKeyAndVisible];
}
- (void)launchView{
//    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil] instantiateViewControllerWithIdentifier:@"LaunchScreen"];
    UIWindow *mainWindow = [UIApplication sharedApplication].keyWindow;
     LanchView *launchView = [[LanchView alloc] initWithFrame:mainWindow.frame];
    [mainWindow addSubview:launchView];
}
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    SDWebImageManager *imageManage = [SDWebImageManager sharedManager];
    [imageManage cancelAll];
    [imageManage.imageCache clearMemory];
}
#pragma mark - regist AMap
- (void)registMapAppKey{
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:baiDuKey generalDelegate:self];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
}
#pragma mark -BMKGeneralDelegate
- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        NSLog(@"baidu_联网成功");
    }
    else{
        NSLog(@"baidu_onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        NSLog(@"baidu_授权成功");
    }
    else {
        NSLog(@"baidu_onGetPermissionState %d",iError);
    }
}
#pragma mark -- NetNetwork
/**
 *  network monitor
 */
- (void)startMonitorNetNetwork{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        /**
         *  网络改变回调
         */
    }];
}


/**
 *  bugs 监控
 *
 *  @param application application description
 */
- (void)startBugtags{
    BugtagsOptions *options = [[BugtagsOptions alloc] init];
    options.trackingCrashes = YES;        // 是否收集闪退，联机 Debug 状态下默认 NO，其它情况默认 YES
    options.trackingUserSteps = YES;      // 是否跟踪用户操作步骤，默认 YES
    options.trackingConsoleLog = YES;     // 是否收集控制台日志，默认 YES
    options.trackingUserLocation = YES;   // 是否获取位置，默认 YES
    options.trackingNetwork = YES;
    options.crashWithScreenshot = YES;    // 收集闪退是否附带截图，默认 YES
    options.ignorePIPESignalCrash = YES;  // 是否忽略 PIPE Signal (SIGPIPE) 闪退，默认 NO
//    options.version = @"1.0.10";          // 自定义版本名称，默认自动获取应用的版本号
//    options.build = @"10";                // 自定义 build，默认自动获取应用的 build
    [Bugtags startWithAppKey:bugsKey invocationEvent:BTGInvocationEventShake options:options];
}





# pragma mark 集成友盟

-(void)setupUmeng
{
     [UMSocialData setAppKey:UmengAppKey];
    
    
    //打开新浪微博的SSO开关，设置新浪微博回调地址，这里必须要和你在新浪微博后台设置的回调地址一致。需要 #import "UMSocialSinaSSOHandler.h"
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:sinaAppKey
                                              secret:@"febec6ab8ff5413c7bba3b0d694e860f"
                                         RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
    [UMSocialQQHandler setQQWithAppId:@"1105549736" appKey:@"01QgLJpSQuHuvWaI" url:@"http://www.umeng.com/social"];
    
     [UMSocialWechatHandler setWXAppId:@"wx955941592b4b2e7f" appSecret:@"e5b6437546b35bf8c5654c8f585ea01e" url:@"http://www.umeng.com/social"];
    
    UMConfigInstance.appKey = UmengAppKey;
    UMConfigInstance.channelId = @"App Store";

    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    
}


-(void)setupUmengPushWith:(NSDictionary *)launchOptions
{
    //设置 AppKey 及 LaunchOptions
    [UMessage startWithAppkey:UmengAppKey launchOptions:launchOptions];
    
    //1.3.0版本开始简化初始化过程。如不需要交互式的通知，下面用下面一句话注册通知即可。
    [UMessage registerForRemoteNotifications];
    
    /**  如果你期望使用交互式(只有iOS 8.0及以上有)的通知，请参考下面注释部分的初始化代码
     //register remoteNotification types （iOS 8.0及其以上版本）
     UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
     action1.identifier = @"action1_identifier";
     action1.title=@"Accept";
     action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
     
     UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
     action2.identifier = @"action2_identifier";
     action2.title=@"Reject";
     action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
     action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
     action2.destructive = YES;
     
     UIMutableUserNotificationCategory *actionCategory = [[UIMutableUserNotificationCategory alloc] init];
     actionCategory.identifier = @"category1";//这组动作的唯一标示
     [actionCategory setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
     
     NSSet *categories = [NSSet setWithObject:actionCategory];
     
     //如果默认使用角标，文字和声音全部打开，请用下面的方法
     [UMessage registerForRemoteNotifications:categories];
     
     //如果对角标，文字和声音的取舍，请用下面的方法
     //UIRemoteNotificationType types7 = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
     //UIUserNotificationType types8 = UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge;
     //[UMessage registerForRemoteNotifications:categories withTypesForIos7:types7 withTypesForIos8:types8];
     */
    
    //for log
    [UMessage setLogEnabled:YES];
    
   
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //关闭友盟自带的弹出框
      [UMessage setAutoAlert:NO];
    
    [UMessage didReceiveRemoteNotification:userInfo];
    
    [BJNotificationTool DealWithNotification:userInfo];
   
    
    
//    for (int i=0; i<100000; i++) {
//        
//        NSLog(@"fdsafdsafdsaf");
//    }
    
    //    self.userInfo = userInfo;
    //    //定制自定的的弹出框
    //    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    //    {
    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"标题"
    //                                                            message:@"Test On ApplicationStateActive"
    //                                                           delegate:self
    //                                                  cancelButtonTitle:@"确定"
    //                                                  otherButtonTitles:nil];
    //
//            [alertView show];
    //
    //    }
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [UMSocialSnsService handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
    }
    return result;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    return YES;
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


//#pragma mark - 远程通知(推送)回调
//
///** 远程通知注册成功委托 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"\n>>>[DeviceToken Success]:%@\n\n", token);
    
    if ([UserConfig sharedInstance].userId.length) {
        [UMessage setAlias:[UserConfig sharedInstance].userId type:@"UID" response:^(id responseObject, NSError *error) {
        }];
    }
    
}
//
///** 远程通知注册失败委托 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"\n>>>[DeviceToken Error]:%@\n\n", error.description);
}

-(void)pullNotificationData
{
    [BJNotificationDBTool newNotificationListWithParam:[[BJDBToolParameter alloc]init] success:^(NSArray *notificationModelArray) {
        for (BJNotificationModel *model in notificationModelArray) {
            if (!model.NewViewHide) {
                self.window.rootViewController.childViewControllers[1].tabBarItem.badgeValue=@"";
            }
        }
        
    }];
}


@end

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end
