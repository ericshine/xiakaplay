//
//  BJDynamicCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/15.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJNotificationModel.h"

@interface BJDynamicCell : UITableViewCell

typedef void(^mainAction)(BJNotificationModel *notificationModel);
typedef void(^subAction)(BJNotificationModel *notificationModel);

@property (nonatomic,strong)BJNotificationModel *DynamicModel;

@property (nonatomic,copy)mainAction mainActionBlock;

@property (nonatomic,copy)subAction subActionBlock;

@end
