//
//  BJImagesView.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/15.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJImagesView.h"
#import "BJpch.h"
#import "BJImageObject.h"
#import "ImageSizeUrl.h"

@implementation BJImagesView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addsubs];
    }
    return self;
}


-(void)setImageArray:(NSArray *)imageArray
{
    _imageArray=imageArray;
    for (int i=0; i<3; i++) {
       UIImageView * imageView=self.subviews[i];
        
        if (i<imageArray.count) {
            imageView.hidden=NO;
            
            BJImageObject *imageObj=imageArray[i];
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:[ImageSizeUrl image750_url:imageObj.url]] placeholderImage:[UIImage imageFromColor:GrayColor(199)]];
            
        }else
        {
            imageView.hidden=YES;
        }
    }
    
    if (imageArray.count>=4) {
        self.subviews[3].hidden=NO;
    }else{
        self.subviews[3].hidden=YES;
    }
}

-(void)addsubs
{
    CGFloat margin=7;
    CGFloat imageWH=((ScreenW-74)-3*margin)/4;
    for (int i=0; i<4;i++) {
        
        UIImageView *imageVeiw=[[UIImageView alloc]init];
        [self addSubview:imageVeiw];
//        imageVeiw.image=[UIImage imageFromColor:[UIColor RandomColor]];
        imageVeiw.tag=i;
        if (i ==3){
            imageVeiw.image=[UIImage imageNamed:@"more"];
            imageVeiw.contentMode=UIViewContentModeCenter;
        }
        
        imageVeiw.frame=CGRectMake(i*(margin+imageWH), 0, imageWH, imageWH);
        
        //对省略号那张图片特殊处理
        if (i==3) imageVeiw.frame=CGRectMake(i*(margin+imageWH)-8, 0, imageWH, imageWH);
    }
}




@end
