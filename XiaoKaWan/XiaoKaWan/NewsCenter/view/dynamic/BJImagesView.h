//
//  BJImagesView.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/15.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJImagesView : UIView

@property (nonatomic,strong)NSArray *imageArray;

@end
