//
//  BJDynamicCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/15.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJDynamicCell.h"
#import "BJpch.h"
#import "BJImagesView.h"

@interface BJDynamicCell()

@property (nonatomic,weak)UIImageView *iconView;

@property (nonatomic,weak)UILabel *nameLable;

@property (nonatomic,weak)UILabel *timeLable;

@property (nonatomic,weak)UILabel *contentLable;

@property (nonatomic,strong)BJImagesView *imagesView;

@property (nonatomic,weak)UILabel *DescribeLable;


@end

@implementation BJDynamicCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
//        [self test];
    }
    return self;
}

-(void)setDynamicModel:(BJNotificationModel *)DynamicModel
{
    _DynamicModel=DynamicModel;
    
    if ([DynamicModel.type isEqualToString:@"followed_post"]) {
        self.DescribeLable.hidden=YES;
        
    }else
    {
        self.DescribeLable.hidden=NO;
    }
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:DynamicModel.icon] placeholderImage:[UIImage imageNamed:@"defaultHeadImage"]];
    self.nameLable.text=DynamicModel.notice_title;
    self.timeLable.text=DynamicModel.gmt_created;
    self.contentLable.text=DynamicModel.notice_content;
    self.DescribeLable.text=DynamicModel.sub_title;
    
    self.imagesView.imageArray=DynamicModel.extend.post_images;
//    NSLog(@"%@",DynamicModel.extend.post_images);
    
    DynamicModel.actionModel;
    
}



-(UIImageView *)setupImageView
{
    UIImageView *imageView=[[UIImageView alloc]init];
    [self.contentView addSubview:imageView];
    
    
    return imageView;
}

-(UIView *)setupView
{
    UIView *View=[[UIView alloc]init];
    [self.contentView addSubview:View];
    
    
    return View;
}

-(UILabel *)setupLable
{
    UILabel *lable=[[UILabel alloc]init];
    [self.contentView addSubview:lable];
    
    
    return lable;
}

-(UIButton*)setupBtn
{
    UIButton *Btn=[[UIButton alloc]init];
    [self.contentView addSubview:Btn];
    
    
    return Btn;
}


-(void)addsubs
{
    UITapGestureRecognizer *iconTap=[[UITapGestureRecognizer alloc]init];
    @weakify(self)
    [[iconTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self)
        
        if (self.DynamicModel.main_action) {
            if (self.mainActionBlock) {
                self.mainActionBlock(self.DynamicModel);
            }
        }
        
    }];
    UITapGestureRecognizer *nameTap=[[UITapGestureRecognizer alloc]init];
    [[nameTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self)
        
        if (self.DynamicModel.main_action) {
            if (self.mainActionBlock) {
                self.mainActionBlock(self.DynamicModel);
            }
        }
    }];
    
    UITapGestureRecognizer *descriTap=[[UITapGestureRecognizer alloc]init];
    [[descriTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self)
        
        if (self.DynamicModel.sub_action) {
            if (self.subActionBlock) {
                self.subActionBlock(self.DynamicModel);
            }
        }
        
    }];
    
    self.iconView=[self setupImageView];
    self.iconView.layer.cornerRadius=22.5;
    self.iconView.layer.masksToBounds=YES;
    self.iconView.userInteractionEnabled=YES;
    [self.iconView addGestureRecognizer:iconTap];
    
    self.nameLable=[self setupLable];
    self.nameLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:14];
    self.nameLable.textColor=GrayColor(52);
    self.nameLable.userInteractionEnabled=YES;
    [self.nameLable addGestureRecognizer:nameTap];
    
    self.timeLable=[self setupLable];
    self.timeLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:9];
    self.timeLable.textColor=GrayColor(147);
    
    self.contentLable=[self setupLable];
    self.contentLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:14];
    self.contentLable.textColor=GrayColor(50);
    
    self.imagesView=[[BJImagesView alloc]init];
    [self.contentView addSubview:self.imagesView];
    
    
    self.DescribeLable=[self setupLable];
    self.DescribeLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:13];
    self.DescribeLable.textColor=GrayColor(102);
    self.DescribeLable.userInteractionEnabled=YES;
    [self.DescribeLable addGestureRecognizer:descriTap];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.iconView.frame=CGRectMake(20, 12.5, 45, 45);
    
    self.nameLable.frame=CGRectMake(76, 22.5, ScreenW-74, 14);
    
    self.timeLable.frame=CGRectMake(75, 42, ScreenW-75, 8);
    
    self.contentLable.frame=CGRectMake(75, 63, ScreenW-23-75, 16);
    
    if (self.DynamicModel.notice_content.length) {
        self.imagesView.frame=CGRectMake(75, 99.5, ScreenW-23.5-75, ((ScreenW-23-75)-3*5)/4);
        
    }else
    {
        self.imagesView.frame=CGRectMake(75, 67.5, ScreenW-23.5-75, ((ScreenW-23-75)-3*5)/4);
    }
    
    @weakify(self)
    [self.DescribeLable mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).with.offset(-17.5);
        make.bottom.equalTo(self).with.offset(-10.5);
        
        
    }];
    
    
}

@end
