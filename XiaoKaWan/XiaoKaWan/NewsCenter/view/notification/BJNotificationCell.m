//
//  BJNotificationCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationCell.h"
#import "BJpch.h"
#import "BJJumpTool.h"
#import "ImageSizeUrl.h"

#define iconfontJiaGuanzhu @"\U0000e61e"

@interface BJNotificationCell()
@property (nonatomic,weak)UIImageView *RedView;

@property (nonatomic,weak)UIImageView *iconView;

@property (nonatomic,weak)UILabel *nameLable;

@property (nonatomic,weak)UILabel *contentLable;

@property (nonatomic,weak)UILabel *timeLable;

@property (nonatomic,weak)UIImageView *ContentImageView;

@property (nonatomic,weak)UIButton *AddAttentionBtn;

@end

@implementation BJNotificationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
        
//        [self test];
    }
    return self;
}
-(void)setNotificationModel:(BJNotificationModel *)NotificationModel
{
    _NotificationModel=NotificationModel;
    
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:NotificationModel.icon] placeholderImage:[UIImage imageNamed:@"defaultHeadImage"]];
    
    self.nameLable.text=NotificationModel.notice_title;
    
    if (NotificationModel.AttributeNotice_content) {
        self.contentLable.attributedText=NotificationModel.AttributeNotice_content;
        
    }else
    {
        self.contentLable.text=NotificationModel.notice_content;
    }
    
    
    self.timeLable.text=NotificationModel.gmt_created;
   
    
    [self.ContentImageView sd_setImageWithURL:[NSURL URLWithString: [ImageSizeUrl image750_url:NotificationModel.extend.icon.url]] placeholderImage:[UIImage imageFromColor:GrayColor(199)]];
    
    if ([NotificationModel.type isEqualToString:@"follow"]) {
        self.AddAttentionBtn.hidden=NO;
        self.AddAttentionBtn.hidden=NotificationModel.extend.followed;
        
    }else
    {
        self.AddAttentionBtn.hidden=YES;
    }
    if (NotificationModel.extend.icon) {
        self.ContentImageView.hidden=NO;
    }else
    {
        self.ContentImageView.hidden=YES;
    }
    
    self.RedView.hidden=NotificationModel.NewViewHide;
}

-(void)test
{
    self.iconView.image=[UIImage imageFromColor:[UIColor RandomColor]];
    self.nameLable.text=@"薛之懵逼的死忠粉";
    
    self.contentLable.text=@"赞了这条动态";
    
    self.timeLable.text=@"30分钟前";
    
    self.ContentImageView.image= [UIImage imageFromColor:[UIColor RandomColor]];
    
    [self.AddAttentionBtn setTitleColor:GrayColor(168) forState:UIControlStateNormal];
    [self.AddAttentionBtn setTitle:iconfontJiaGuanzhu forState:UIControlStateNormal];
    
}

-(UIImageView *)setupImageView
{
    UIImageView *imageView=[[UIImageView alloc]init];
    [self.contentView addSubview:imageView];
    
    
    return imageView;
}

-(UIView *)setupView
{
    UIView *View=[[UIView alloc]init];
    [self.contentView addSubview:View];
    
    
    return View;
}

-(UILabel *)setupLable
{
    UILabel *lable=[[UILabel alloc]init];
    [self.contentView addSubview:lable];
    
    
    return lable;
}

-(UIButton*)setupBtn
{
    UIButton *Btn=[[UIButton alloc]init];
    [self.contentView addSubview:Btn];
    
    
    return Btn;
}


-(void)fofds
{
    NSLog(@"++%s--",__func__);
}

-(void)addsubs
{
    UITapGestureRecognizer *iconTap=[[UITapGestureRecognizer alloc]init];
    @weakify(self)
    [[iconTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self)
        
        if (self.NotificationModel.main_action) {
            if (self.mainActionBlock) {
                self.mainActionBlock(self.NotificationModel);
            }
        }
        
    }];
    UITapGestureRecognizer *nameTap=[[UITapGestureRecognizer alloc]init];
    [[nameTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self)
        
        if (self.NotificationModel.main_action) {
            if (self.mainActionBlock) {
                self.mainActionBlock(self.NotificationModel);
            }
        }
    }];
    
    
    self.RedView=[self setupImageView];
    self.RedView.image=[UIImage imageNamed:@"reddian"];
    
    self.iconView=[self setupImageView];
    self.iconView.layer.cornerRadius=22.5;
    self.iconView.layer.masksToBounds=YES;
    self.iconView.userInteractionEnabled=YES;
    [self.iconView addGestureRecognizer:iconTap];
    
    self.nameLable=[self setupLable];
    self.nameLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:14];
    self.nameLable.textColor=GrayColor(52);
    self.nameLable.userInteractionEnabled=YES;
    [self.nameLable addGestureRecognizer:nameTap];
    
    self.contentLable=[self setupLable];
    self.contentLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:14];
    self.contentLable.textColor=GrayColor(145);
    
    self.timeLable=[self setupLable];
    self.timeLable.font=[UIFont fontWithName:@"STHeitiSC-Light" size:11];
    self.timeLable.textColor=GrayColor(52);
    
    self.ContentImageView=[self setupImageView];
    
    self.AddAttentionBtn=[self setupBtn];
    self.AddAttentionBtn.titleLabel.font=[UIFont fontWithName:@"iconfont" size:34];
    [self.AddAttentionBtn setTitleColor:GrayColor(168) forState:UIControlStateNormal];
    [self.AddAttentionBtn setTitle:iconfontJiaGuanzhu forState:UIControlStateNormal];
    [self.AddAttentionBtn addTarget:self action:@selector(addAttentionClick) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addAttentionClick
{
    if (self.addAttentionBlock) {
        self.addAttentionBlock(self.NotificationModel);
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.RedView.frame=CGRectMake(13, 11.5, 9, 9);
    
    self.iconView.frame=CGRectMake(22, 11.5, 45, 45);
    
    self.nameLable.frame=CGRectMake(77.5, 11.5, ScreenW-77.5-70, 14);
    
    self.contentLable.frame=CGRectMake(77.5, 34.5, ScreenW-77.5-65, 14);
    
    self.timeLable.frame=CGRectMake(77.5, 61.5, ScreenW-77.5-70, 11);
    
    @weakify(self)
    [self.ContentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).with.offset(-10);
        make.top.equalTo(self).with.offset(10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(60);
        
    }];
    
    [self.AddAttentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).with.offset(-13.5);
        make.top.equalTo(self).with.offset(26);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(32);
        
    }];
    
}



@end
