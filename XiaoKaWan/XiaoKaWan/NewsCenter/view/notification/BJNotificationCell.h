//
//  BJNotificationCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJNotificationModel.h"

typedef void(^addAttention)(BJNotificationModel *notificationModel);
typedef void(^mainAction)(BJNotificationModel *notificationModel);

@interface BJNotificationCell : UITableViewCell

@property (nonatomic,strong)BJNotificationModel *NotificationModel;

@property (nonatomic,copy)addAttention addAttentionBlock;

@property (nonatomic,copy)mainAction mainActionBlock;

@end
