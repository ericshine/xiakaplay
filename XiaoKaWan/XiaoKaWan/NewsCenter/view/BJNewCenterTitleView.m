//
//  BJNewCenterTitleView.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNewCenterTitleView.h"
#import <Masonry.h>
#import "BJTitleViewBtn.h"
#import <ReactiveCocoa.h>

@interface BJNewCenterTitleView()

@property (nonatomic,strong)NSArray *titleArray;


@end

@implementation BJNewCenterTitleView

-(NSArray *)titleArray
{
    if (_titleArray==nil) {
        _titleArray=@[@"好友",@"通知",@"动态"];
    }
    return _titleArray;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addsubs ];
        
    }
    return self;
}

-(void)addsubs
{
    for (NSString * title in self.titleArray) {
        NSInteger index=[self.titleArray indexOfObject:title];
        BJTitleViewBtn *titleBtn=[[BJTitleViewBtn alloc]init];
        [titleBtn setTitle:title forState:UIControlStateNormal];
        [self addSubview:titleBtn];
        titleBtn.tag=index;
        @weakify(self)
        [[titleBtn rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(BJTitleViewBtn *titleBtn) {
            @strongify(self)
            self.titleClickBlock(titleBtn.tag);
            [self selectIndex:titleBtn.tag];
            
        }];
        if (index==0) {
            titleBtn.selected=YES;
        }else
        {
            titleBtn.selected=NO;
        }
    }
}

-(void)layoutSubviews
{
    for (BJTitleViewBtn *titleBtn in self.subviews) {
        titleBtn.frame=CGRectMake(titleBtn.tag*55, 0, 46, 46);
        
    }
}


-(void)selectIndex:(NSInteger)index
{
    NSLog(@"fdsafdsafdsafdsa");
    for (BJTitleViewBtn *titleBtn in self.subviews) {
        if (index==titleBtn.tag) {
            
            titleBtn.selected=YES;
        }else
        {
            titleBtn.selected=NO;
        }
        
    }
}





@end
