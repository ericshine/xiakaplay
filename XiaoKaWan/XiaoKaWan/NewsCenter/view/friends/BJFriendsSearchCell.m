//
//  BJFriendsSearchCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJFriendsSearchCell.h"
#import "BJpch.h"
#import "UserConfig.h"

#define iconfontJiaGuanZhu @"\U0000e61e"

#define iconfontYiGuanZhu @"\U0000e61f"




@interface BJFriendsSearchCell()

@property (nonatomic,weak)UIImageView *iconView;

@property (nonatomic,weak)UILabel *nickLable;

@property (nonatomic,weak)UILabel *fensiCountLable;

@property (nonatomic,weak)UIButton *addAttentionBtn;


@end

@implementation BJFriendsSearchCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
    }
    return self;
}

-(void)setUser:(User *)User
{
    _User=User;
    
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:User.avatar] placeholderImage:[UIImage imageNamed:@"defaultHeadImage"]];
    
    self.nickLable.text=User.nick;
    
    self.fensiCountLable.text=[NSString stringWithFormat:@"粉丝：%@",User.following_count];
    
    
    if (User.followed) {
        
        self.addAttentionBtn.enabled=NO;
        
    }else
    {
        self.addAttentionBtn.enabled=YES;
        
    }
    if ([[UserConfig sharedInstance].userId integerValue]==User.uid) {
        self.addAttentionBtn.hidden=YES;
    }else
    {
        self.addAttentionBtn.hidden=NO;
    }
}

-(UIImageView *)setupImageView
{
    UIImageView *imageView=[[UIImageView alloc]init];
    [self.contentView addSubview:imageView];
    
    
    return imageView;
}

-(UIView *)setupView
{
    UIView *View=[[UIView alloc]init];
    [self.contentView addSubview:View];
    
    
    return View;
}

-(UILabel *)setupLable
{
    UILabel *lable=[[UILabel alloc]init];
    [self.contentView addSubview:lable];
    lable.textColor=GrayColor(52);
    
    
    return lable;
}

-(UIButton*)setupBtn
{
    UIButton *Btn=[[UIButton alloc]init];
    [self.contentView addSubview:Btn];
    
    
    return Btn;
}


-(void)addsubs
{
    self.iconView=[self setupImageView];
    self.iconView.layer.cornerRadius=22.5;
    self.iconView.layer.masksToBounds=YES;
    
    self.nickLable=[self setupLable];
    self.nickLable.font =[UIFont systemFontOfSize:14];
    
    self.fensiCountLable=[self setupLable];
    self.fensiCountLable.font=[UIFont systemFontOfSize:11];
    
    self.addAttentionBtn=[self setupBtn];
    self.addAttentionBtn.titleLabel.font=[UIFont fontWithName:@"iconfont" size:34];
    [self.addAttentionBtn addTarget:self action:@selector(AddAttentionClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.addAttentionBtn setTitle:iconfontYiGuanZhu forState:UIControlStateDisabled];
    [self.addAttentionBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.addAttentionBtn setTitle:iconfontJiaGuanZhu forState:UIControlStateNormal];
    [self.addAttentionBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    
    
}

-(void)AddAttentionClick:(UIButton *)btn
{
    if (self.addAttentionBlock) {
        self.addAttentionBlock(self.User);
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.iconView.frame=CGRectMake(21, 19, 45, 45);
    
    self.nickLable.frame=CGRectMake(76, 23.5, ScreenW-72, 14);
    
    self.fensiCountLable.frame=CGRectMake(76, 48, ScreenW-76, 10);
    
    @weakify(self)
    [self.addAttentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).with.offset(-25);
        make.centerY.equalTo(self);
        
        
    }];
    
    
}


@end
