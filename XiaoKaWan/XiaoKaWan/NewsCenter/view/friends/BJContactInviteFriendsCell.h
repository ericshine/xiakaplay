//
//  BJContactInviteFriendsCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/16.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJContactModel.h"

typedef void(^InviteHimClick)(BJContactModel *contactModel);

typedef void(^AddAttentionClick)(BJContactModel *contactModel);


@interface BJContactInviteFriendsCell : UITableViewCell

@property (nonatomic,strong)BJContactModel *ContactModel;

@property (nonatomic,copy)InviteHimClick InviteHimClickBlock;

@property (nonatomic,copy)AddAttentionClick AddAttentionClickBlock;

@end
