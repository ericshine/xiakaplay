//
//  BJAllAttentionCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJAllAttentionCell.h"
#import "UIColor+colorWithHex.h"
#import <ReactiveCocoa.h>
#import <Masonry.h>
#import "UIImage+Category.h"
#import <UIImageView+WebCache.h>

#define iconfontHuxiangGuanzhu @"\U0000e629"
#define iconfontYiGuanzhu @"\U0000e61f"

@interface BJAllAttentionCell()

@property (nonatomic, weak) UIImageView *iconView;

@property (nonatomic,weak)UILabel *nameLable;

@property (nonatomic,weak) UIButton *attentionBtn;



@end


@implementation BJAllAttentionCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
        
//        [self test];
    }
    return self;
}

-(void)setUserModel:(User *)userModel
{
    _userModel=userModel;
    
    self.nameLable.text=userModel.nick;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageNamed:@"defaultHeadImage"]];
    
    self.attentionBtn.selected=userModel.bi_followed;
}

-(void)addsubs
{
    UIImageView *iconView=[[UIImageView alloc]init];
    self.iconView=iconView;
    [self.contentView addSubview:iconView];
    iconView.layer.cornerRadius=22.5;
    iconView.layer.masksToBounds=YES;
    
    UILabel *nameLable=[[UILabel alloc]init];
    self.nameLable=nameLable;
    [self.contentView addSubview:nameLable];
    nameLable.textColor=GrayColor(108);
    nameLable.font=[UIFont systemFontOfSize:15.3];
    
    UIButton *attentionBtn=[[UIButton alloc]init];
    self.attentionBtn=attentionBtn;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn.titleLabel.font=[UIFont fontWithName:@"iconfont" size:34];
    [self.attentionBtn setTitle:iconfontHuxiangGuanzhu forState:UIControlStateSelected];
    [self.attentionBtn setTitleColor:[UIColor p_colorWithHex:0xA8A8A8] forState:UIControlStateSelected];
    [self.attentionBtn setTitle:iconfontYiGuanzhu forState:UIControlStateNormal];
    [self.attentionBtn setTitleColor:[UIColor p_colorWithHex:0xA8A8A8] forState:UIControlStateNormal];
    [self.attentionBtn addTarget:self action:@selector(cancleAttentionClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)cancleAttentionClick
{
    if (self.CancleAttentionClickBlock) {
        self.CancleAttentionClickBlock(self.userModel);
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.iconView.frame=CGRectMake(15, 15, 45, 45);
    
    self.nameLable.frame=CGRectMake(74, 31, 100, 15);
    
    @weakify(self)
    [self.attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).with.offset(-14);
        make.top.equalTo(self).with.offset(20);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(32);
        
    }];
    
    
}

@end
