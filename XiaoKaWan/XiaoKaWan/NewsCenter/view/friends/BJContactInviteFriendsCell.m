//
//  BJContactInviteFriendsCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/16.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJContactInviteFriendsCell.h"
#import "BJpch.h"


#define iconfontJiaGuanZhu @"\U0000e61e"
#define iconfontYaoQingTa @"\U0000e627"
#define iconfontYiGuanZhu @"\U0000e61f"

@interface BJContactInviteFriendsCell()

@property (nonatomic,weak)UIImageView *iconView;

@property (nonatomic,weak)UILabel *UserNameLable;

@property (nonatomic,weak)UILabel *ContactNameLable;

@property (nonatomic,weak)UILabel *DescribeLable;

@property (nonatomic,weak) UIButton *attentionBtn;


@end

@implementation BJContactInviteFriendsCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
        
//        [self test];
    }
    return self;
}

-(void)setContactModel:(BJContactModel *)ContactModel
{
    _ContactModel=ContactModel;
    
    self.UserNameLable.text=ContactModel.NickName;
    self.ContactNameLable.text=ContactModel.contactName;
    self.DescribeLable.text=[NSString stringWithFormat:@"手机联系人：%@",ContactModel.contactName];
    if (![ContactModel.imgUrl isEqualToString:@"(null)"]) {
        [self.iconView sd_setImageWithURL:[NSURL URLWithString:ContactModel.imgUrl] placeholderImage:[UIImage imageNamed:@"defaultHeadImage"]];
    }else
    {
        self.iconView.image=[UIImage imageNamed:@"defaultHeadImage"];
    }
    
         //不是小咖玩用户
    if (ContactModel.Status==0) {
        [self.attentionBtn setTitle:iconfontYaoQingTa forState:UIControlStateNormal];
        [self.attentionBtn setTitleColor:rgbColor(39, 38, 54, 1) forState:UIControlStateNormal];
        self.UserNameLable.hidden=YES;
        self.ContactNameLable.hidden=NO;
        self.DescribeLable.hidden=YES;
        
        //是小咖玩用户，但我还没关注他
    }else if (ContactModel.Status==1)
    {
        [self.attentionBtn setTitle:iconfontJiaGuanZhu forState:UIControlStateNormal];
        [self.attentionBtn setTitleColor:rgbColor(255, 218, 68, 1) forState:UIControlStateNormal];
        self.UserNameLable.hidden=NO;
        self.ContactNameLable.hidden=YES;
        self.DescribeLable.hidden=NO;

        
        //是小咖玩用户，我已经关注了他
    }else if (ContactModel.Status==2)
    {
        [self.attentionBtn setTitle:iconfontYiGuanZhu forState:UIControlStateNormal];
        [self.attentionBtn setTitleColor:GrayColor(168) forState:UIControlStateNormal];
        self.UserNameLable.hidden=NO;
        self.ContactNameLable.hidden=YES;
        self.DescribeLable.hidden=NO;

    }
}


-(UIImageView *)setupImageView
{
    UIImageView *imageView=[[UIImageView alloc]init];
    [self.contentView addSubview:imageView];
    
    
    return imageView;
}

-(UIView *)setupView
{
    UIView *View=[[UIView alloc]init];
    [self.contentView addSubview:View];
    
    
    return View;
}

-(UILabel *)setupLable
{
    UILabel *lable=[[UILabel alloc]init];
    [self.contentView addSubview:lable];
    
    
    return lable;
}

-(UIButton*)setupBtn
{
    UIButton *Btn=[[UIButton alloc]init];
    [self.contentView addSubview:Btn];
    
    
    return Btn;
}


-(void)addsubs
{
    UIImageView *iconView=[[UIImageView alloc]init];
    self.iconView=iconView;
    [self.contentView addSubview:iconView];
    iconView.layer.cornerRadius=22.5;
    iconView.layer.masksToBounds=YES;
    self.iconView.image=[UIImage imageNamed:@"defaultHeadImage"];
    
    UILabel *UserNameLable=[[UILabel alloc]init];
    UserNameLable.textAlignment=NSTextAlignmentLeft;
    self.UserNameLable=UserNameLable;
    [self.contentView addSubview:UserNameLable];
    UserNameLable.textColor=GrayColor(51);
    UserNameLable.font=[UIFont systemFontOfSize:14];
    
    UILabel *ContactNameLable=[[UILabel alloc]init];
    self.ContactNameLable=ContactNameLable;
    [self.contentView addSubview:ContactNameLable];
    ContactNameLable.textColor=GrayColor(51);
    ContactNameLable.font=[UIFont systemFontOfSize:14];
    UserNameLable.textAlignment=NSTextAlignmentLeft;
    
    UILabel *DescribeLable=[[UILabel alloc]init];
    self.DescribeLable=DescribeLable;
    [self.contentView addSubview:DescribeLable];
    DescribeLable.textColor=GrayColor(102);
    DescribeLable.font=[UIFont systemFontOfSize:13];
    UserNameLable.textAlignment=NSTextAlignmentLeft;
    
    UIButton *attentionBtn=[[UIButton alloc]init];
    self.attentionBtn=attentionBtn;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn.titleLabel.font=[UIFont fontWithName:@"iconfont" size:34];
    [self.attentionBtn addTarget:self action:@selector(AddAttentionClick:) forControlEvents:UIControlEventTouchUpInside];
    UserNameLable.textAlignment=NSTextAlignmentLeft;
}

-(void)AddAttentionClick:(UIButton *)attentionBtn
{
    if (self.ContactModel.Status==0) {
        if (self.InviteHimClickBlock) {
            self.InviteHimClickBlock(self.ContactModel);
        }
        
    }else if (self.ContactModel.Status==1)
    {
        if (self.AddAttentionClickBlock) {
            self.AddAttentionClickBlock(self.ContactModel);
        }
        
    }else if (self.ContactModel.Status==2)
    {
        
    }
    
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.iconView.frame=CGRectMake(15, 15, 45, 45);
    
    self.UserNameLable.frame=CGRectMake(78, 20, ScreenW-2*78, 14);
    
    self.ContactNameLable.frame=CGRectMake(78, 32, ScreenW-2*78, 14);
    
    self.DescribeLable.frame=CGRectMake(78, 43, ScreenW-2*78, 13);
    
    @weakify(self)
    [self.attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self);
        make.top.equalTo(self);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(80);
        
    }];
    
}


@end
