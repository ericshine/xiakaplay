//
//  BJinviteFriendsCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^wechatClick)();

typedef void(^ContactsClick)();

typedef void(^MicroBlogClick)();

typedef void(^QQClick)();
@interface BJinviteFriendsCell : UITableViewCell

@property (nonatomic,copy) wechatClick wechatClickBlock;

@property (nonatomic,copy) ContactsClick ContactsClickBlock;

@property (nonatomic,copy) MicroBlogClick MicroBlogClickBlock;

@property (nonatomic,copy) QQClick QQClickBlock;

@end
