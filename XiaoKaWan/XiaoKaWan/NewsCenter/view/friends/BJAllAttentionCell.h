//
//  BJAllAttentionCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

typedef void(^CancleAttentionClick)(User *UserModel);

@interface BJAllAttentionCell : UITableViewCell

@property (nonatomic,strong)User *userModel;

@property (nonatomic,copy)CancleAttentionClick CancleAttentionClickBlock;

@end
