//
//  BJFriendsSearchCell.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

typedef void(^addAttention)(User *user);

@interface BJFriendsSearchCell : UITableViewCell

@property (nonatomic,strong)User *User;

@property (nonatomic,copy) addAttention addAttentionBlock;

@end
