//
//  BJinviteFriendsCell.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJinviteFriendsCell.h"
#import "BJTextUnderImgBtn.h"
#import <Masonry.h>
#import <ReactiveCocoa.h>
#import "UIColor+colorWithHex.h"

@interface BJinviteFriendsCell()

@property (nonatomic ,strong)NSArray *titleArray;

@property (nonatomic,strong)NSArray *iconArry;

@property (nonatomic,weak)UIView *lineView;

@end

@implementation BJinviteFriendsCell

-(NSArray *)titleArray
{
    if (_titleArray==nil) {
        _titleArray=@[@"微信",@"通讯录",@"微博",@"QQ"];
    }
    return _titleArray;
}

-(NSArray *)iconArry
{
    if (_iconArry==nil) {
        _iconArry=@[@"wechatLogo",@"contactLogo",@"sinaLogo",@"qqLogo"];
    }
    return _iconArry;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addsubs];
        
        
    }
    return self;
}


-(void)addsubs
{
    for (int i=0; i<4; i++) {
        NSString *title=self.titleArray[i];
        BJTextUnderImgBtn *btn=[[BJTextUnderImgBtn alloc]init];
        btn.tag=i;
        [btn setTitle:title forState:UIControlStateNormal];
        [self.contentView addSubview:btn];
        [btn setImage:[UIImage imageNamed:self.iconArry[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside ];
        
    }
    
    UIView *lineView=[[UIView alloc]init];
    self.lineView=lineView;
    [self.contentView addSubview:lineView];
    lineView.backgroundColor=GrayColor(240);
    
}

-(void)btnClick:(BJTextUnderImgBtn *)btn
{
    if (btn.tag==0) {
        if (self.wechatClickBlock) {
            self.wechatClickBlock();
        }
        
    }else if (btn.tag==1)
    {
        if (self.ContactsClickBlock) {
            self.ContactsClickBlock();
        }
        
    }else if (btn.tag==2)
    {
        if (self.MicroBlogClickBlock) {
            self.MicroBlogClickBlock();
        }
        
    }else if (btn.tag==3)
    {
        if (self.QQClickBlock) {
            self.QQClickBlock();
        }
        
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    @weakify(self)
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.bottom.equalTo(self);
       
        make.height.mas_equalTo(0.5);
        
    }];
    
    
    
    CGFloat margin=([UIScreen mainScreen].bounds.size.width-45.5*4)/5;
    
    for (UIView *btn in self.contentView.subviews) {
        if ([btn isKindOfClass:[BJTextUnderImgBtn class]]) {
            @weakify(self)
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                @strongify(self)
                make.left.equalTo(self).with.offset(btn.tag*(45.5+margin)+margin);
                make.bottom.equalTo(self).with.offset(-11.5);
                make.width.mas_equalTo(45.5);
                make.height.mas_equalTo(45.5+7+11.5);
                
            }];
            
            
        }
    }
    
}

@end
