//
//  BJTitleViewBtn.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJTitleViewBtn.h"
#import "UIColor+colorWithHex.h"
#import <Masonry.h>

@interface BJTitleViewBtn()

@property (nonatomic,weak)UIView *lineView;

@end

@implementation BJTitleViewBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor colorWithWhite:0 alpha:0];
        
        [self addLineView];
        
       
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.titleLabel.font=[UIFont systemFontOfSize:17];
        self.titleLabel.textAlignment=NSTextAlignmentCenter;
        
    }
    return self;
}



-(void)addLineView
{
    UIView *lineView=[[UIView alloc]init];
    [self addSubview:lineView];
    self.lineView=lineView;
    lineView.backgroundColor=[UIColor p_colorWithHex:0x333333];
}

-(void)layoutSubviews
{
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(4);
        make.width.mas_equalTo(33.5);
        
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.lineView);
        make.top.equalTo(self);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(50);
    }];
    
}

-(void)setSelected:(BOOL)selected
{
    if (selected) {
        self.lineView.hidden=NO;
    }else
    {
        self.lineView.hidden=YES;
    }
    
}


@end
