//
//  BJNewCenterTitleView.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^titleClick)(NSInteger index);

@interface BJNewCenterTitleView : UIView

@property (nonatomic,copy) titleClick titleClickBlock;

-(void)selectIndex:(NSInteger)index;

@end
