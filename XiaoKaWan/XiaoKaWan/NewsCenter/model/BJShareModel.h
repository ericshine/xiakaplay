//
//  BJShareModel.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/27.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJShareTool.h"

//typedef NS_ENUM(NSInteger ,SDKType){
//    
//    weixin=0,
//    weibo=1,
//    qq=2
//    
//};

@interface BJShareModel : NSObject



/*
 {
	icon : http://static.uu19.com/images/share/logo.png,
	title : 来小咖玩跟我一起跟上流行的脚步吧!,
	url : http://www.uu19.com/index.php?m=default&c=share&a=invite&uid=1,
	sub_title : 不知道哪里好吃哪里好玩？纳尼！快来小咖玩，怎么流行怎么玩！
 }
 **/

@property (nonatomic,copy)NSString *icon;

@property (nonatomic,copy)NSString *title;

@property (nonatomic,copy)NSString *url;

@property (nonatomic,copy)NSString *sub_title;

@property (nonatomic,assign)SDKType SDKtype;

@property (nonatomic,copy)NSString *type;

@end
