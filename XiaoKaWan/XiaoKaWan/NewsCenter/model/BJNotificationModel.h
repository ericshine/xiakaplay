//
//  BJNotificationModel.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/26.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJNotificationExtendModel.h"
#import "BJNotificationActionModel.h"

@interface BJNotificationModel : NSObject

@property (nonatomic,strong)BJNotificationExtendModel *extend;

@property (nonatomic,copy)NSString *notice_title;

@property (nonatomic,copy)NSString *action;

@property (nonatomic,copy)NSString *main_action;

@property (nonatomic,copy)NSString *sub_action;

@property (nonatomic,copy)NSString *gmt_created;

@property (nonatomic,copy)NSString *notice_content;

@property (nonatomic,copy)NSMutableAttributedString *AttributeNotice_content;

@property (nonatomic,copy)NSString *type;

@property (nonatomic,assign)NSInteger notice_id;

@property (nonatomic,copy)NSString *icon;

@property (nonatomic,copy)NSString *sub_title;

@property (nonatomic,copy)NSString *notice_time;

@property (nonatomic,copy)NSString *user_id;

@property(nonatomic,assign)BOOL NewViewHide;

@property (nonatomic,strong)BJNotificationActionModel *actionModel;

@property (nonatomic,assign)CGFloat DynamicCellHieght;



/*
 {
	msg : success,
	data : [
	{
	extend : {
	post_id : 21,
	icon : {
	src : http://img.uu19.com/post/2016-06-13/1eca4aebUQTXAQ,
	w : 330,
	h : 220
 }
 },
	notice_title : 唐小三,
	action : come.xiaokaplay://?address=post_vc&id=21,
	gmt_created : 2016-07-21 17:02:14,
	notice_content : hehehe @eric hihi,
	type : post_comment,
	notice_id : 7,
	icon : http://img.uu19.com/avatar/26/deda83f899b3e2cbca5cef68a1cbb75b
 },
	{
	extend : {
	post_id : 21,
	icon : {
	src : http://img.uu19.com/post/2016-06-13/1eca4aebUQTXAQ,
	w : 330,
	h : 220
 }
 },
	notice_title : 唐小三,
	action : come.xiaokaplay://?address=post_vc&id=21,
	gmt_created : 2016-07-21 17:00:12,
	notice_content : hehehe,
	type : post_comment,
	notice_id : 6,
	icon : http://img.uu19.com/avatar/26/deda83f899b3e2cbca5cef68a1cbb75b
 },
	{
	extend : {
	post_id : 21,
	icon : {
	src : http://img.uu19.com/post/2016-06-13/1eca4aebUQTXAQ,
	w : 330,
	h : 220
 }
 },
	notice_title : 唐小三,
	action : come.xiaokaplay://?address=post_vc&id=21,
	gmt_created : 2016-07-21 16:27:57,
	notice_content : 赞了这条动态,
	user_id : 26,
	type : post_like,
	notice_id : 5,
	icon : http://img.uu19.com/avatar/26/deda83f899b3e2cbca5cef68a1cbb75b
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:04:31,
	notice_content : fdsadfsd,
	type : system,
	notice_id : 17,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:04:50,
	notice_content : fdsfasdf,
	type : system,
	notice_id : 18,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:04:56,
	notice_content : fasdfasdf,
	type : system,
	notice_id : 19,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:05:00,
	notice_content : fasdfsadf,
	type : system,
	notice_id : 20,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:05:07,
	notice_content : fsdafsadfsadf,
	type : system,
	notice_id : 21,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:05:13,
	notice_content : fasdfasdfasdf,
	type : system,
	notice_id : 22,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:05:19,
	notice_content : fasdfasdfasd,
	type : system,
	notice_id : 23,
	icon : default_notice_icon
 },
	{
	extend : [
 ],
	notice_title : 小咖玩官方,
	gmt_created : 2016-07-23 10:05:31,
	notice_content : fasdf,
	type : system,
	notice_id : 24,
	icon : default_notice_icon
 }
 ],
	code : 0
 }
 **/

@end
