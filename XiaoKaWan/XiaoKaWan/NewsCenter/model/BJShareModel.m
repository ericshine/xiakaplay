//
//  BJShareModel.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/27.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJShareModel.h"

@implementation BJShareModel

-(SDKType)SDKtype
{
    if ([self.type isEqualToString:@"wxsession"]) {
        return wxsession;
    }else if ([self.type isEqualToString:@"sina"])
    {
        return sina;
    }else if ([self.type isEqualToString:@"qq"])
    {
        return qq;
    }else if ([self.type isEqualToString:@"wxtimeline"])
    {
        return wxtimeline;
    }else if ([self.type isEqualToString:@"qzone"])
    {
        return qzone;
    }else
    {
        return 0;
    }
    
    
}

@end
