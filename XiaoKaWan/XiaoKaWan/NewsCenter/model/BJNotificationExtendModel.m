//
//  BJNotificationExtendModel.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/26.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationExtendModel.h"
#import <MJExtension.h>
@class BJImageObject;

@implementation BJNotificationExtendModel

+(NSDictionary *)objectClassInArray{
    return @{@"post_images":[BJImageObject class]};
    
}

@end
