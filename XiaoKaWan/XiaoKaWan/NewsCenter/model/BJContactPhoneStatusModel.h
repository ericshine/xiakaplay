//
//  BJContactPhoneStatusModel.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/22.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJContactPhoneStatusModel : NSObject

/*
 {
	status : 0,
	number : 13870993894,
	uid : 0,
	nickname :
    nick : http://img.uu19.com/avatar/11/334c2eb9040dfd11152a045a52f13a7a
 },
 **/

@property (nonatomic,assign)NSInteger status;

@property (nonatomic,copy)NSString *number;

@property (nonatomic,assign)NSInteger uid;

@property (nonatomic,copy)NSString *nickname;

@property (nonatomic,copy)NSString *nick;

@end
