//
//  BJContactModel.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/22.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJContactModel : NSObject

@property (nonatomic,copy)NSString *contactName;

@property (nonatomic,copy)NSString *PhoneStr;

@property (nonatomic,copy)NSString *NickName;

@property (nonatomic,copy)NSString *imgUrl;

@property (nonatomic,assign)NSInteger Status;

@property (nonatomic,assign)NSInteger uid;

@property (nonatomic,assign)NSInteger id;

@end
