//
//  BJNotificationExtendModel.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/26.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJImageObject.h"

@interface BJNotificationExtendModel : NSObject

/*
 {
	extend : {
	post_id : 21,
	icon : {
	src : http://img.uu19.com/post/2016-06-13/1eca4aebUQTXAQ,
	w : 330,
	h : 220
 }
 **/

@property (nonatomic,copy)NSString *post_id;

@property (nonatomic,strong)BJImageObject *icon;

@property (nonatomic,strong)NSArray *post_images;

@property (nonatomic,copy )NSString *uid;

@property (nonatomic,assign)BOOL followed;



@end
