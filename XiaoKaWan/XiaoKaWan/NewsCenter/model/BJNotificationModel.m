//
//  BJNotificationModel.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/26.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationModel.h"
#import "BJpch.h"

@implementation BJNotificationModel

-(NSMutableAttributedString *)AttributeNotice_content
{
    NSMutableAttributedString *AttriStr=[[NSMutableAttributedString alloc]init];
    
    if ([self remindOther]) {
        
        NSRange symbolStrRange=[self.notice_content rangeOfString:@"[@:"];
        
        NSString *beforeText= [self.notice_content substringToIndex:symbolStrRange.location];
        
        NSString *afterSymbolStr=[self.notice_content substringFromIndex:symbolStrRange.location+symbolStrRange.length];
        
        NSRange secondColonRange= [afterSymbolStr rangeOfString:@":"];
        
        NSString *nameStr=[afterSymbolStr substringToIndex:secondColonRange.location];
        
        NSRange secondbracketsRange=[afterSymbolStr rangeOfString:@"]"];
        
        NSString *afterText= [afterSymbolStr substringFromIndex:secondbracketsRange.location+secondbracketsRange.length];
        
        NSString *alltext=[NSString stringWithFormat:@"%@@%@%@",beforeText,nameStr,afterText];
        
        NSMutableAttributedString *AttributeAlltext=[[NSMutableAttributedString alloc]initWithString:alltext];
        
        [AttributeAlltext addAttribute:NSForegroundColorAttributeName value:rgbColor(70, 117, 217, 1) range:NSMakeRange(beforeText.length, nameStr.length+1)];
        
        AttriStr=AttributeAlltext;
        
        return AttriStr;
        
    }else
    {
        return nil;
    }
    
    return nil;
    
}

-(BOOL)remindOther
{
    if ([self.notice_content rangeOfString:@"[@:"].location!=NSNotFound) {
        
        return YES;
    }else
    {
        return NO;
    }
}

-(BJNotificationActionModel *)actionModel
{
    if (_actionModel==nil) {
        _actionModel=[[BJNotificationActionModel alloc]init];
    }
    
   NSRange addressRemindRange=[self.action rangeOfString:@"address="];
    
    NSRange symbolRange=[self.action rangeOfString:@"&"];
    
    NSRange idRemindRange=[self.action rangeOfString:@"id="];
    
    NSString *address=[self.action substringWithRange:NSMakeRange(addressRemindRange.location+addressRemindRange.length, symbolRange.location-(addressRemindRange.location+addressRemindRange.length))];
    
    NSString *id=[self.action substringFromIndex:idRemindRange.location+idRemindRange.length];
    

    _actionModel.address=address;
    _actionModel.id=id;
    
    return _actionModel;
}

@end
