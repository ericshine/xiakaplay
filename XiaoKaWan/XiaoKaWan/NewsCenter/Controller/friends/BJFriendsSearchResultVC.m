//
//  BJFriendsSearchResultVC.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJFriendsSearchResultVC.h"
#import "ClientNetwork.h"
#import "BJEmptyView.h"
#import "BJFriendsSearchCell.h"
#import "BJpch.h"
#import "BJRBITool.h"
#import "BJTabBarViewController.h"
#import "BJNavigationController.h"
#import "MeHomeViewController.h"

@interface BJFriendsSearchResultVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)User *resultUser;

@property (nonatomic,strong)BJEmptyView *emptyView;

@property (nonatomic,strong)UIView *WhiteView;

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)BJNavigationController *PresentVC;



@end

@implementation BJFriendsSearchResultVC
-(BJNavigationController *)PresentVC
{
    if (_PresentVC==nil) {
        
        MeHomeViewController *VC=[[MeHomeViewController alloc]init];
        VC.currentUser=self.resultUser;
        VC.userInforType=USERINFOR_OTHER;
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        button.titleLabel.font = [UIFont fontWithName:@"iconfont" size:20];
        [button setTitle:@"\U0000e613" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        // [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
        [button addTarget:self action:@selector(dismissPresentVC) forControlEvents:UIControlEventTouchUpInside];
        VC.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        _PresentVC= [[BJNavigationController alloc] initWithRootViewController:VC];
       
    }
    return _PresentVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.hidesBottomBarWhenPushed=YES;
    
    [self addtableView];


}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    ((BJTabBarViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController]).tabBar.hidden=NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ((BJTabBarViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController]).tabBar.hidden=YES;
}

-(void)addtableView
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, -60, ScreenW, ScreenH+60) style:UITableViewStyleGrouped];
    [self.view addSubview:tableView];
    tableView.delegate=self ;
    tableView.dataSource=self;
    self.tableView=tableView;
    tableView.rowHeight=82.75;
//    tableView.contentInset=UIEdgeInsetsMake(200, 0, 0, 0);
    
    BJEmptyView *empytView=[[BJEmptyView alloc]initWithFrame:tableView.bounds titel:@"暂无此人" imageName:@"searchEmptyIcon"];
    self.emptyView=empytView;
    [tableView addSubview:empytView];
    
    UIView *WhiteView=[[UIView alloc]initWithFrame:tableView.bounds];
    WhiteView.backgroundColor=rgbColor(239, 239, 239, 1);
    self.WhiteView=WhiteView;
    [tableView addSubview:WhiteView];
    WhiteView.hidden=NO;
    
}


-(void)setupEmptyView
{
    if (self.resultUser) {
        self.emptyView.hidden=YES;
    }else
    {
        self.emptyView.hidden=NO;
    }
}

#pragma mark - tableView Delegate && dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify=@"identify";
    BJFriendsSearchCell *cell=[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell==nil) {
        cell=[[BJFriendsSearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    
    cell.User=self.resultUser;
    
    @weakify(self)
    cell.addAttentionBlock=^(User *userModel){
        @strongify(self)
        [BJRBITool newCentureSearchResultAddAttentionOrCancleAttentionClick];
        [self addAttentionWithUser:userModel];
    
    };
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    

    [self presentViewController:self.PresentVC animated:YES completion:nil];
    
}

-(void)dismissPresentVC
{

    [self.PresentVC dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark searchController delegate
-(void)didDismissSearchController:(UISearchController *)searchController
{

}

-(void)willDismissSearchController:(UISearchController *)searchController
{

    
}

#pragma mark SearchBar delegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [BJRBITool newCentureSearchFriendsClick];
    return YES;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{

    [self searchWithText:searchBar.text];
    
     self.WhiteView.hidden=YES;
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.WhiteView.hidden=NO;
    self.resultUser=nil;
}

#pragma mark 网络操作

-(void)searchWithText:(NSString *)text
{
    NSDictionary *parameter=@{@"keyword":text, @"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] userFindApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@",obj);
        
        if (succeed) self.resultUser=  [User objectWithKeyValues:obj];
        [self.tableView reloadData];
        [self setupEmptyView];
        
        
    }];
}

-(void)addAttentionWithUser:(User *)user
{
    NSDictionary *parameter=@{@"uid":@(user.uid), @"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] followAdddApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        if (succeed){
            self.resultUser.followed=1;
            [self.tableView reloadData];
            [self setupEmptyView];
            
            if (self.addAttentionBlock) {
                self.addAttentionBlock(user);
            }
        }
        
        
    }];
}


@end
