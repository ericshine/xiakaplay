//
//  BJFriendsSearchResultVC.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"
#import "User.h"


typedef void(^addAttention)(User *UserModel);

@interface BJFriendsSearchResultVC : UIViewController<UISearchControllerDelegate,UISearchBarDelegate>

@property (nonatomic,copy)addAttention addAttentionBlock;


@end
