//
//  BJContactInviteFriendsVC.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/16.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJContactInviteFriendsVC.h"
#import "BJpch.h"
#import <RHAddressBook/AddressBook.h>
#import "BJUITools.h"
#import "BJContactInviteFriendsCell.h"
#import <FMDB.h>
#import <MJExtension.h>
#import "ClientNetwork.h"
#import "BJContactTool.h"
#import "BJContactPhoneStatusModel.h"
#import "BJPregressHUD.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import "BJRBITool.h"
#import "BJRefreshGitHeader.h"
#import "ClientNetwork.h"

@interface BJContactInviteFriendsVC ()<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>

@property (nonatomic,strong)BJContactTool *contactTool;

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)NSArray *contactArray;

@end

@implementation BJContactInviteFriendsVC

-(NSArray *)contactArray
{
    if (_contactArray==nil) {
        _contactArray=[NSArray array];
    }
    return _contactArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"邀请好友";
    
   
    
    
    [self setupContactTool];
    
    
    [self addtableView];
    
    [BJPregressHUD showWithStatus:nil];
    [BJContactTool contactModelArraySuccess:^(NSArray *contactModelArray) {
        self.contactArray=contactModelArray;
        [self.tableView reloadData];
        
        [BJPregressHUD dissMiss];
        
    }];
}
#pragma mark 设置通讯录工具
-(void)setupContactTool
{
    BJContactTool *contactTool=[[BJContactTool alloc]init];
    
    self.contactTool=contactTool;
    @weakify(self)
    contactTool.RefreshContactBlock=^(){
        @strongify(self)
        
        [self.tableView reloadData];
//        [BJPregressHUD dissMiss];
        
    };
    
//    //不是第一次打开通讯录功能，以前读取过系统的通讯录到数据库
//    if ([contactTool hasSavedContact]) {
//        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
//        NSDate *lastSendDate= [userDefault objectForKey:@"sendContactTime"];
//        NSTimeInterval timeIntervalSinceLastSend=[[NSDate date]timeIntervalSinceDate:lastSendDate];
//        //查看离上一次发送给服务器有多久
//        if (timeIntervalSinceLastSend>60*60*24) {
//            [BJPregressHUD showWithStatus:nil];
//            dispatch_async(dispatch_get_global_queue(0, 0), ^{
//                [contactTool clearDB];
//                [contactTool saveContact];
//                
//                [contactTool sendContactToServerWithTelArray:[contactTool readAllContactTel]];
//                
//            });
//        }
////        if (timeIntervalSinceLastSend>2) [contactTool sendContactToServerWithTelArray:[contactTool readAllContactTel]];
//        //第一次打开通讯录功能
//    }else
//    {
//        [BJPregressHUD showWithStatus:nil];
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            [contactTool saveContact];
//            
//            [contactTool sendContactToServerWithTelArray:[contactTool readAllContactTel]];
//            
//        });
//        
//    }
}

-(void)addtableView
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView=tableView;
    [self.view addSubview:tableView];
    tableView.delegate=self ;
    tableView.dataSource=self;
    tableView.rowHeight=80;
    
    @weakify(self)
    tableView.header=[BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        @strongify(self)
        [self.tableView.header endRefreshing];

        [BJPregressHUD showWithStatus:nil];
        [BJContactTool newContactModelArraySuccess:^(NSArray *contactModelArray) {
            
            self.contactArray=contactModelArray;
            [self.tableView reloadData];
            
            [BJPregressHUD dissMiss];
        }];
        
        
        
    }];
}

#pragma mark cell 各种点击效果

-(void)InviteContactWith:(BJContactModel *)contactModel
{
    [BJRBITool newCentureContactInviteHimClick];
    
    [self loadInviteMessageData:^(NSString *messageContent) {
        
        NSArray *PhoneArray= @[contactModel.PhoneStr];
        
        MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
        smsController.messageComposeDelegate = self;
        
        smsController.recipients=PhoneArray;
        
        [smsController setBody:messageContent];//可以写一个方法专门组复杂的字符串在这调用即可
        
        [self presentViewController:smsController animated:YES completion:nil];
    }];
}

-(void)AddAttentionWith:(BJContactModel *)contactModel
{
    [BJRBITool newCentureContactAddAttention];
    
    NSDictionary *parameter=@{@"uid":@(contactModel.uid), @"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] followAdddApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)

        if (succeed){
            contactModel.Status=2;
//            [self.contactTool updateContactAttentionWith:contactModel];
            
            if (self.addattentionBlock) {
                self.addattentionBlock(contactModel);
            }
            [BJPregressHUD showSuccessWithStatus:@"关注成功"];
            
        }else
        {
            [BJPregressHUD showErrorWithStatus:obj];
        }
        
        
    }];
}

#pragma mark - tableView Delegate && dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contactArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify=@"identify";
    BJContactInviteFriendsCell *cell=[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell==nil) {
        cell=[[BJContactInviteFriendsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(BJContactInviteFriendsCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [BJUITools tableviewSepratorLineLeftInsetWith:cell Suojin:0 ];
    
    cell.ContactModel=self.contactArray[indexPath.row];
    
    @weakify(self)
    cell.InviteHimClickBlock=^(BJContactModel *contactModel){
        @strongify(self)
        
        [self InviteContactWith:contactModel];
        
    };
    cell.AddAttentionClickBlock=^(BJContactModel *contactModel){
        @strongify(self)
        
        [self AddAttentionWith:contactModel];
        
    };
}



#pragma mark - 短信发完以后代理
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            # ifdef DEBUG
             NSLog(@"Result: canceled");
            # endif
            break;
        case MessageComposeResultSent:
            # ifdef DEBUG
            NSLog(@"Result: Sent");
             # endif
            [BJPregressHUD showSuccessWithStatus:@"发送成功"];
            break;
        case MessageComposeResultFailed:
            # ifdef DEBUG
            if (DEBUG) NSLog(@"Result: Failed");
            # endif
            
            [BJPregressHUD showErrorWithStatus:@"发送失败"];
            break;
        default:
            break;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [BJPregressHUD dissMiss];
    });
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self ];
}

#pragma mark -网络操作
/*
 {
	url : http://112.124.39.140/index.php?m=default&c=share&a=invite&uid=1,
	icon : http://static.uu19.com/images/share/logo.png,
	title : 全世界好吃好玩的地方都在这里，和我一起来跟上流行吧！查看详情http://112.124.39.140/index.php?m=default&c=share&a=invite&uid=1
 }
 **/
-(void)loadInviteMessageData:(void(^)(NSString *messageContent))success
{
    NSDictionary * parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"share_type":@"message"};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] shareInviteApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@===%d",obj,succeed);
        
        if (succeed) {
           
            if (success) {
                success(obj[@"title"]);
            }
        }
        
    }];
}



@end
