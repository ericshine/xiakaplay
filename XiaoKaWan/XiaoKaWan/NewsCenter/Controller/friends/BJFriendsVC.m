//
//  BJFriendsVC.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJFriendsVC.h"
#import "UIImage+Category.h"
#import <Masonry.h>
#import "BJinviteFriendsCell.h"
#import <ReactiveCocoa.h>
#import "BJAllAttentionCell.h"
#import <UMengSocial/UMSocial.h>
#import <UMSocialSnsData.h>
#import "BJContactInviteFriendsVC.h"
#import "BJUITools.h"
#import "BJFriendsSearchResultVC.h"
#import "ClientNetwork.h"
#import "User.h"
#import "UMMobClick/MobClick.h"
#import "BJContactModel.h"
#import "BJContactTool.h"
#import "BJRBITool.h"
#import "BJShareModel.h"
#import "BJShareTool.h"
#import "MeHomeViewController.h"
#import "User.h"
#import <MJExtension.h>
#import "BJRefreshGitHeader.h"
#import "BJTabBarViewController.h"
#import <AFNetworking.h>
#import "BJEmptyView.h"
#import "BJpch.h"
#import "LoginViewController.h"
#import "BJNavigationController.h"

@interface BJFriendsVC ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UMSocialUIDelegate,UISearchControllerDelegate,UISearchBarDelegate,NSURLSessionDelegate>

@property (nonatomic,strong)UITableView *tableVeiw;

@property (nonatomic,strong)UISearchController *searchController;

@property (nonatomic,strong)NSMutableArray *allAttentionUserArray;

@property (nonatomic,strong)User *WillCancleUserModel;

@property (nonatomic,weak)BJEmptyView *emptyView;

@end

@implementation BJFriendsVC

-(NSMutableArray *)allAttentionUserArray
{
    if (_allAttentionUserArray==nil) {
        _allAttentionUserArray=[NSMutableArray array];
    }
    return _allAttentionUserArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor=[UIColor RandomColor];
    
    [self addtableview];
    
    [self addSearchControll];
    
    [self loadAllAttentionData];
    
    [self adnotification];
    
    [self addemptyview];
    
}

-(void)addemptyview
{
    BJEmptyView *emptyView=[[BJEmptyView alloc]initWithFrame:self.view.bounds titel:@"好友信息需登录查看" imageName:@"unLoginImage"];
    self.emptyView=emptyView;
    [self.view addSubview:emptyView];
    
    UIButton *loginBtn=[[UIButton alloc]init];
    [loginBtn setTitle:@"立即登录" forState:UIControlStateNormal];
    [loginBtn setBackgroundColor:rgbColor(255, 216, 68,1)];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBtn.layer.cornerRadius=5;
    loginBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [emptyView addSubview:loginBtn];
    [loginBtn addTarget:self action:@selector(loginClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLable;
    for (UIView *subView in emptyView.subviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            titleLable=(UILabel *)subView;
        }
    }
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(emptyView);
        make.top.equalTo(titleLable.mas_bottom).with.offset(50);
        make.width.mas_equalTo(125);
        make.height.mas_equalTo(44);
        
    }];
    
    [self setupEmptyView];
    
}

-(void)loginClick
{
    LoginViewController *loginVc = [[LoginViewController alloc] init];
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform t = CGAffineTransformMakeTranslation(0, 100);
        self.tabBarController.view.transform = CGAffineTransformScale(t, 0.9, 0.9);
        [UIView animateWithDuration:0.1 animations:^{
            
            [self presentViewController:[[BJNavigationController alloc] initWithRootViewController:loginVc] animated:YES completion:nil];
        } completion:^(BOOL finished) {
            
        }];
    } completion:^(BOOL finished) {
        self.tabBarController.view.transform = CGAffineTransformIdentity;
    }];
}

-(void)setupEmptyView
{
    
    if ([UserConfig sharedInstance].user_token) {
        
        self.emptyView.hidden=YES;
    }else
    {
        self.emptyView.hidden=NO;
    }
}

-(void)adnotification
{
    @weakify(self)
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLogOutNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableVeiw.header beginRefreshing];
        [self setupEmptyView];
        
    }];
    
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLoginNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableVeiw.header beginRefreshing];
        [self setupEmptyView];
        
    }];
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJNeedReloadUserInformationNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableVeiw.header beginRefreshing];
        
    }];
    
    
}

#pragma mark 埋点的测试
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [MobClick event:@"loadAllAttentionData"];
    [MobClick event:@"test2" attributes:@{@"kkkkk":@"vvvv"}];
    
}

-(void)addSearchControll
{
    BJFriendsSearchResultVC *resultVC=[[BJFriendsSearchResultVC alloc]init];
    @weakify(self)
    resultVC.addAttentionBlock=^(User *UserModel){
        @strongify(self)
        [self.tableVeiw.header beginRefreshing];
    };

    UISearchController *searchController=[[UISearchController alloc]initWithSearchResultsController:resultVC];
    self.searchController=searchController;
    searchController.delegate=resultVC;
    searchController.searchBar.delegate=resultVC;
    
    self.searchController.searchBar.backgroundImage=[UIImage imageFromColor:GrayColor(240)];

    self.searchController.searchBar.barTintColor=[UIColor p_colorWithHex:0xffda44];
    self.searchController.searchBar.tintColor=[UIColor blackColor];
    self.searchController.searchBar.placeholder=@"输入昵称或手机号，关注好友";
    self.searchController.dimsBackgroundDuringPresentation=1;
    self.searchController.obscuresBackgroundDuringPresentation=1;
    self.searchController.hidesNavigationBarDuringPresentation=1;
    
    self.tableVeiw.tableHeaderView=self.searchController.searchBar;
    [self.searchController.searchBar sizeToFit];
    
}


-(void)addtableview
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:tableView];
    self.tableVeiw=tableView;
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 130, 0);
    tableView.showsVerticalScrollIndicator=0;
    tableView.separatorColor=GrayColor(240);
    
    @weakify(self)
    tableView.header=[BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        @strongify(self)
        
        [ self loadAllAttentionData];
        
    }];
    

}

#pragma mark - tableViewDelegate,datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }else if (section==1)
    {
        return self.allAttentionUserArray.count;
    }else{
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 76;
    }else
    {
        return 72;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identify=[NSString stringWithFormat:@"friends%ld",indexPath.section];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if(!cell){
        if (indexPath.section==0){
            cell = [[BJinviteFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
            
        }
        if (indexPath.section==1)  cell = [[BJAllAttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    
    
    
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    [BJUITools tableviewSepratorLineLeftInsetWith:cell Suojin:0];
    
    if (indexPath.section==0) {
        
        @weakify(self)
        ((BJinviteFriendsCell*)cell).wechatClickBlock=^(){
            @strongify(self)
            [self wechatClick];
            
        };
        ((BJinviteFriendsCell*)cell).ContactsClickBlock=^(){
            @strongify(self)
            [BJRBITool newCentureContactInviteFriends];
            [self ContactClick];
            
        };
        ((BJinviteFriendsCell*)cell).MicroBlogClickBlock=^(){
            @strongify(self)
            [self MicroBolgClick];
            
        };
        ((BJinviteFriendsCell*)cell).QQClickBlock=^(){
            @strongify(self)
            [self QQClick];
            
        };
    }
    
    if (indexPath.section==1) {
        ((BJAllAttentionCell*)cell).userModel=self.allAttentionUserArray[indexPath.row];
        ((BJAllAttentionCell *)cell).CancleAttentionClickBlock=^(User *userModel){
            [BJRBITool newCentureCancleAttentionClick];
            self.WillCancleUserModel=userModel;
            UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"是否取消关注" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"不再关注" otherButtonTitles: nil];
            [actionSheet showInView:self.view];
        };
    }
    
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [self cancleAttentionWithUser:self.WillCancleUserModel];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor whiteColor];
    UILabel *titleLable=[[UILabel alloc]init];
    titleLable.font=[UIFont systemFontOfSize:14];
    titleLable.textColor=[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
    [view addSubview:titleLable];
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).with.offset(10);
        make.bottom.equalTo(view).with.offset(-10);
        
    }];
    
    if (section==0) {
        titleLable.text=@"邀请好友";
    }else
    {
        titleLable.text=@"全部关注";
    }
    
    
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==1) {
        MeHomeViewController *VC=[[MeHomeViewController alloc]init];
        VC.userInforType=USERINFOR_OTHER;
        User  *user=[[User alloc]init];
        user.uid=((User *)self.allAttentionUserArray[indexPath.row]).uid;
        VC.currentUser=user;
        VC.hidesBottomBarWhenPushed=YES;
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }
    
}

-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData
{

    NSLog(@"%@=====%@",platformName,socialData);
    
    socialData.title=@"dsafdsafdsf";
    socialData.extConfig.title=@"fdsfdsa";
}


#pragma mark 各种社交分享方法

-(void)wechatClick
{
    
    [BJShareTool shareInviteFriendsWithType:wxsession viewController:self];
}

-(void)ContactClick
{
    BJContactInviteFriendsVC *InviteVC=[[BJContactInviteFriendsVC alloc]init];
    @weakify(self)
    InviteVC.addattentionBlock=^(BJContactModel *contactModel){
        @strongify(self)
           [self loadAllAttentionData];
        
    };
    InviteVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:InviteVC animated:YES];
    
    
    
}

-(void)MicroBolgClick
{
    [BJShareTool shareInviteFriendsWithType:sina viewController:self];
}

-(void)QQClick
{
    [BJShareTool shareInviteFriendsWithType:qq viewController:self];

}



#pragma mark 网络操作

-(void)loadAllAttentionData
{
    if (![UserConfig sharedInstance].user_token)
    {
        [self.tableVeiw.header endRefreshing];
        [self.allAttentionUserArray removeAllObjects];
        [self.tableVeiw reloadData];
        return;
    }
    
    NSDictionary *parameter=@{ @"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] followFollowingsApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@==%d",obj,succeed);
        if (succeed) {
            self.allAttentionUserArray=[User objectArrayWithKeyValuesArray:obj];
            [self.tableVeiw reloadData];
            [self.tableVeiw.header endRefreshing ];
            
        }else
        {
            [self.tableVeiw.header endRefreshing];
        }
        
        
    }];
    
}

-(void)cancleAttentionWithUser:(User *)UserModel
{
    if (![UserConfig sharedInstance].user_token) return;
    NSDictionary *parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"uid":@(UserModel.uid)};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] followDeleteApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@",obj);
        if (succeed) {
            
            [self.allAttentionUserArray removeObject:UserModel];
            [self.tableVeiw reloadData];
            
//            [[[BJContactTool alloc]init] updateContactCancleAttentionWith:UserModel.uid];
        }
        
        
        
    }];
}



@end
