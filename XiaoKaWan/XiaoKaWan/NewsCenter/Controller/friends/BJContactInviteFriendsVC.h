//
//  BJContactInviteFriendsVC.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/16.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJContactModel.h"

typedef void(^addattention)(BJContactModel *comtactModel);

@interface BJContactInviteFriendsVC : UIViewController

@property (nonatomic,copy)addattention addattentionBlock;

@end
