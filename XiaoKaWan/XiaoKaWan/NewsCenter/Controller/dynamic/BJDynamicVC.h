//
//  BJDynamicVC.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"

@interface BJDynamicVC : UIViewController

@property (nonatomic,strong)NSMutableArray *dynamicArray;

@end
