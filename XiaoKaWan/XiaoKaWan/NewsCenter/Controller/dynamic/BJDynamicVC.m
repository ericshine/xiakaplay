//
//  BJDynamicVC.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJDynamicVC.h"
#import "BJDynamicCell.h"
#import "BJUITools.h"
#import "BJEmptyView.h"
#import "BJRefreshGitHeader.h"
#import "BJRefreshGitFooter.h"
#import "ClientNetwork.h"
#import <MJExtension.h>
#import "BJNotificationModel.h"
#import "BJpch.h"
#import "BJDynamicDBTool.h"
#import "BJDBToolParameter.h"
#import "MapDetailListViewController.h"
#import "MapObject.h"
#import "PlacePostDetailViewController.h"
#import "PostParatemer.h"
#import "MeHomeViewController.h"
#import "User.h"
#import "MapsPlaceDetailViewController.h"
#import "Place.h"
#import "BJJumpTool.h"


@interface BJDynamicVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)BJEmptyView *emptyView;

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,assign)NSInteger currentIndex;


@end

@implementation BJDynamicVC

-(NSMutableArray *)dynamicArray
{
    if (_dynamicArray==nil) {
        _dynamicArray=[NSMutableArray array];
    }
    return _dynamicArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor RandomColor];
    
    [self addtableView];
    
    [self.tableView.header beginRefreshing];
    
    [self addnotification];
 
    
}

-(void)addnotification
{
    @weakify(self)
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLogOutNotification object:nil] subscribeNext:^(id x) {
      @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLoginNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];
    
}

-(void)addtableView
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [self.view addSubview:tableView];
    tableView.delegate=self ;
    tableView.dataSource=self;
    tableView.contentInset = UIEdgeInsetsMake(-18, 0, 130, 0);
    self.tableView=tableView;
    tableView.separatorColor=GrayColor(240);
    
    BJEmptyView *emptyView=[[BJEmptyView alloc]initWithFrame:tableView.bounds];
    self.emptyView=emptyView;
    [tableView addSubview:emptyView];
    
    @weakify(self)
    tableView.header=[BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        @strongify(self)
//        [self loadDynamicData];
        BJDBToolParameter *parameter=[[BJDBToolParameter alloc]init];
        parameter.count=10;
        parameter.isMoreType=NO;
        [BJDynamicDBTool dynamicListWithParameter:parameter success:^(NSArray *dynamicModelArray) {
            [self.tableView.header endRefreshing];
            self.dynamicArray=[NSMutableArray arrayWithArray:dynamicModelArray];
            self.currentIndex=[((BJNotificationModel *)self.dynamicArray.lastObject) notice_id];
            [self.tableView reloadData];
            [self setupEmptyView];
            
        }];
    }];
    
    tableView.footer=[BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        @strongify(self)
        BJDBToolParameter *parameter=[[BJDBToolParameter alloc]init];
        parameter.count=10;
        parameter.isMoreType=1;
        parameter.MaxID=self.currentIndex;
        [BJDynamicDBTool dynamicListWithParameter:parameter success:^(NSArray *dynamicModelArray) {
            [self.tableView.footer endRefreshing];
            [self.dynamicArray addObjectsFromArray:dynamicModelArray];
            self.currentIndex=((BJNotificationModel *)self.dynamicArray.lastObject).notice_id;
            [self.tableView reloadData];
            [self setupEmptyView];
        }];
        
    }];
    
}
-(void)setupEmptyView
{
    if (self.dynamicArray.count>0) {
        self.emptyView.hidden=YES;
    }else
    {
        self.emptyView.hidden=NO;
    }
    
}

#pragma mark - tableView Delegate && dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dynamicArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify=@"identify";
    BJDynamicCell *cell=[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell==nil) {
        cell=[[BJDynamicCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
//        cell.textLabel.text=[NSString stringWithFormat:@"++--%ld",indexPath.row];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BJNotificationModel *dynamicModel=self.dynamicArray[indexPath.row   ];

    return dynamicModel.DynamicCellHieght;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BJNotificationModel *dynamicModel= self.dynamicArray[indexPath.row];
    
    [BJJumpTool JumpWithActionStr:dynamicModel.action navigationVC:self.navigationController];

}
-(void)tableView:(UITableView *)tableView willDisplayCell:(BJDynamicCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [BJUITools tableviewSepratorLineLeftInsetWith:cell Suojin:0 ];
    cell.DynamicModel=self.dynamicArray[indexPath.row   ];
    
    @weakify(self)
    cell.mainActionBlock=^(BJNotificationModel *dynamicModel){
        @strongify(self)
        [BJJumpTool JumpWithActionStr:dynamicModel.main_action navigationVC:self.navigationController];
        
    };
    cell.subActionBlock=^(BJNotificationModel *dynamickModel){
        @strongify(self)
        [BJJumpTool JumpWithActionStr:dynamickModel.sub_action navigationVC:self.navigationController];
    };
    
}




@end
