//
//  NewsCenterViewController.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "NewsCenterViewController.h"
#import <Masonry.h>
#import "BJFriendsVC.h"
#import "BJNotificationVC.h"
#import "BJDynamicVC.h"
#import "BJNewCenterTitleView.h"
#import "BJpch.h"
#import "BJNotificationConfig.h"
#import "BJTabBarViewController.h"


@interface NewsCenterViewController ()<UIScrollViewDelegate>

@property (nonatomic,strong)NSArray *childVCClassArray;

@property (nonatomic,strong)UIScrollView *scrollView;

@property (nonatomic,strong)BJNewCenterTitleView *titleView;

@property (nonatomic,assign)NSInteger LastIndex;

@end

@implementation NewsCenterViewController{
    UIImageView *navBarHairlineImageView;
}

-(NSArray *)childVCClassArray
{
    if (_childVCClassArray==nil) {
        _childVCClassArray=@[[BJFriendsVC class],[BJNotificationVC class],[BJDynamicVC class]];
    }
    return _childVCClassArray;
}

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    [self setupViewUnderNavigationBar];
    
    [self addTitleItem];
    
    [self addScrollView];
    
    [self addChildVC];
    
    [self addNotification];
    
    
    // Do any additional setup after loading the view.
}

-(void)addNotification
{
    @weakify(self)
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"BJUploadNotification" object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.scrollView setContentOffset:CGPointMake(1*kDeviceWidth, 0) animated:YES];
        
    }];
}


-(void)setupViewUnderNavigationBar
{
    self.edgesForExtendedLayout=UIRectEdgeNone;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    navBarHairlineImageView.hidden = YES;
    
}
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:rgbColor(255, 213, 37, 1)];
//    self.tabBarController.tabBar.hidden=NO;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
//    self.tabBarController.tabBar.hidden=YES;
    
}

-(void)addChildVC
{
    for (Class VCclass in self.childVCClassArray) {
        NSInteger index=[self.childVCClassArray indexOfObject:VCclass];
        UIViewController *ChildVC=[[VCclass alloc]init];
        [self addChildViewController:ChildVC];
        [self.scrollView addSubview:ChildVC.view];
        ChildVC.view.frame=CGRectMake(kDeviceWidth*index, 0, kDeviceWidth,self.scrollView.bounds.size.height);
        
    }
    
}


-(void)addScrollView
{
    UIScrollView *ScrollView=[[UIScrollView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:ScrollView];
    self.scrollView=ScrollView;
    ScrollView.showsHorizontalScrollIndicator=NO;
    ScrollView.pagingEnabled=YES;
    ScrollView.contentSize=CGSizeMake(kDeviceWidth*3, 0);
    ScrollView.delegate=self;
}

-(void)addTitleItem
{
   
    BJNewCenterTitleView *view=[[BJNewCenterTitleView alloc]initWithFrame:CGRectMake(0, 0, 155, 46)];
    self.titleView=view;
    view.backgroundColor=[UIColor clearColor];
    self.navigationItem.titleView=view  ;
    view.titleClickBlock=^(NSInteger index){
        [self.scrollView setContentOffset:CGPointMake(index*kDeviceWidth, 0) animated:YES];
        
    };
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    CGPoint point=scrollView.contentOffset;
    NSInteger CurrentIndex=(NSInteger)(point.x/kDeviceWidth);
    [self.titleView selectIndex:CurrentIndex];
    
    BJNotificationVC *notificationVC= self.childViewControllers[1];
    //没赋值之前 LastIndex 代表上一次的索引,这一句是显示的控制器从通知跳到别的时执行的语句
    if (self.LastIndex==1&&self.LastIndex!=CurrentIndex) {
        [notificationVC dismissOnScreen];
    }
    if (CurrentIndex==1&&self.LastIndex!=CurrentIndex) {
        [notificationVC showOnScreen];
    }
    
    //赋值之后 LastIndex 代表本次的索引
    self.LastIndex=CurrentIndex;
    
    self.pageIndex=CurrentIndex;
    
    NSLog(@"%s===%ld",__func__,CurrentIndex);
}




-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    CGPoint point=scrollView.contentOffset;
    NSInteger CurrentIndex=(NSInteger)(point.x/kDeviceWidth);
    [self.titleView selectIndex:CurrentIndex];
    
    BJNotificationVC *notificationVC= self.childViewControllers[1];
    //没赋值之前 LastIndex 代表上一次的索引,这一句是显示的控制器从通知跳到别的时执行的语句
    if (self.LastIndex==1&&self.LastIndex!=CurrentIndex) {
        [notificationVC dismissOnScreen];
    }
    
    //赋值之后 LastIndex 代表本次的索引
    self.LastIndex=CurrentIndex;
    
    self.pageIndex=CurrentIndex;
}




@end
