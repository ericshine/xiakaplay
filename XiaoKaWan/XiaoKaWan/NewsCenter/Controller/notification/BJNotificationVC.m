//
//  BJNotificationVC.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/13.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationVC.h"
#import "BJNotificationCell.h"
#import "BJUITools.h"
#import "BJpch.h"
#import "BJRefreshGitHeader.h"
#import "BJRefreshGitFooter.h"
#import "BJEmptyView.h"
#import "ClientNetwork.h"
#import <MJExtension.h>
#import "BJNotificationModel.h"
#import "BJPregressHUD.h"
#import "BJNotificationDBTool.h"
#import "BJDBToolParameter.h"
#import "MapDetailListViewController.h"
#import "MapObject.h"
#import "PlacePostDetailViewController.h"
#import "PostParatemer.h"
#import "MeHomeViewController.h"
#import "User.h"
#import "BJNotificationConfig.h"
#import "MapsPlaceDetailViewController.h"
#import "Place.h"
#import "BJJumpTool.h"


@interface BJNotificationVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonnull,strong)UITableView *tableView;

@property (nonatomic,strong)BJEmptyView *emptyView;

@property (nonatomic,assign)NSInteger currentLastIndex;

@end

@implementation BJNotificationVC



-(NSMutableArray *)notificationArray
{
    if (_notificationArray==nil) {
        _notificationArray=[NSMutableArray array];
        
       
    }
    return _notificationArray   ;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor RandomColor];
    
    [self addtableView];
    
    [self.tableView.header beginRefreshing];
    
    [self addNotification];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"haveNewNotification"]) {
//        
//        [self.tableView.header beginRefreshing];
//    }

}

-(void)addNotification
{
    @weakify(self)
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"BJUploadNotification" object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];
    
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:@"haveNewNotification" object:nil] subscribeNext:^(id x) {
       @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];
    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLogOutNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];

    [[[NSNotificationCenter defaultCenter]rac_addObserverForName:BJLoginNotification object:nil] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView.header beginRefreshing];
        
    }];
    
}

-(void)addtableView
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [self.view addSubview:tableView];
    tableView.delegate=self ;
    tableView.dataSource=self;
    tableView.contentInset = UIEdgeInsetsMake(-18, 0, 130, 0);
    self.tableView=tableView;
    tableView.separatorColor=GrayColor(240);
    
    BJEmptyView *emptyView=[[BJEmptyView alloc]initWithFrame:tableView.bounds];
    self.emptyView=emptyView;
    [tableView addSubview:emptyView];
    @weakify(self)
    tableView.header=[BJRefreshGitHeader selfHeaderWithRefreshingBlock:^{
        @strongify(self)

//        self.navigationController.tabBarItem.badgeValue=@"";
        
        BJDBToolParameter *parameter=  [[BJDBToolParameter alloc]init];
        parameter.count=10;
        [BJNotificationDBTool newNotificationListWithParam:parameter success:^(NSArray *notificationModelArray) {
            [self.tableView.header endRefreshing];
            self.notificationArray=[NSMutableArray arrayWithArray:notificationModelArray];
            self.currentLastIndex=((BJNotificationModel*)self.notificationArray.lastObject).notice_id;
            [self.tableView reloadData];
            [self setupEmptyView];
            
        }];
        
    }];
    
    tableView.footer=[BJRefreshGitFooter selfFooterWithRefreshingBlock:^{
        @strongify(self)
        
        BJDBToolParameter *parameter=[[BJDBToolParameter alloc]init];
        parameter.MaxID=self.currentLastIndex;
        parameter.count=10;
        parameter.isMoreType=1;
        [BJNotificationDBTool notificationListWithParam:parameter success:^(NSArray *notificationModelArray) {
            [self.tableView.footer endRefreshing];
            [self.notificationArray addObjectsFromArray:notificationModelArray];
            self.currentLastIndex=((BJNotificationModel*)self.notificationArray.lastObject).notice_id;
            [self.tableView reloadData];
            [self setupEmptyView];
            
        }];
        
    }];
    
}
#pragma mark 控制器显示在屏幕上和从屏幕消失时调用的方法

-(void)dismissOnScreen
{
    for (BJNotificationModel *notiMofel in self.notificationArray) {
        notiMofel.NewViewHide=1;
    }
    [self.tableView reloadData];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [BJNotificationDBTool haveScanNotificactions];
    });
    self.navigationController.tabBarItem.badgeValue=nil;
}

-(void)showOnScreen
{
    self.navigationController.tabBarItem.badgeValue=nil;
}

-(void)hideRedNewViewWith:(BJNotificationModel *)notificationModel
{
    notificationModel.NewViewHide=1;
    [self.tableView reloadData];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [BJNotificationDBTool haveScanNotificactionWithNotificationModel:notificationModel];
    });
}

-(void)setupEmptyView
{
    if (self.notificationArray.count>0) {
        self.emptyView.hidden=YES;
    }else
    {
        self.emptyView.hidden=NO;
    }

}


#pragma mark - tableView Delegate && dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notificationArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify=@"identify";
    BJNotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell==nil) {
        cell=[[BJNotificationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
//    cell.textLabel.text=[NSString stringWithFormat:@"++--%ld",indexPath.row];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 83;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BJNotificationModel *notificationModel= self.notificationArray[indexPath.row];
    
    [BJJumpTool JumpWithActionStr:notificationModel.action navigationVC:self.navigationController];
    
    [self hideRedNewViewWith:notificationModel];
    

}
-(void)tableView:(UITableView *)tableView willDisplayCell:(BJNotificationCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [BJUITools tableviewSepratorLineLeftInsetWith:cell Suojin:0 ];
    
    cell.NotificationModel=self.notificationArray[indexPath.row];
    
    @weakify(self)
    cell.addAttentionBlock=^(BJNotificationModel *NotificationModel){
        @strongify(self)
        [self addAttentionWithNotificationModel:NotificationModel];
    };
    cell.mainActionBlock=^(BJNotificationModel *notificationModel){
        @strongify(self)
        [BJJumpTool JumpWithActionStr:notificationModel.main_action navigationVC:self.navigationController];
    };
}

#pragma mark 网络操作


-(void)addAttentionWithNotificationModel:(BJNotificationModel  *)notificationModel
{
    NSLog(@"%@----%@",notificationModel.extend.uid,[UserConfig sharedInstance].user_token);
    
    NSDictionary *parameter=@{@"uid":notificationModel.extend.uid, @"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] followAdddApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@===%d",obj,succeed);
        
        if (succeed){
            [BJPregressHUD showSuccessWithStatus:@"关注成功"];
            notificationModel.extend.followed=1;
            [self.tableView reloadData];
            [BJNotificationDBTool updateNotificationArray:@[notificationModel]];
            
        }
        
        
    }];
}


@end
