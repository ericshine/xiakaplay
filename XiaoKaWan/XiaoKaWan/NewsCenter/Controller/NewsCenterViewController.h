//
//  NewsCenterViewController.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/9/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSuperViewController.h"

@interface NewsCenterViewController : BJSuperViewController

@property (nonatomic,assign)NSInteger pageIndex;

@end
