//
//  APIConfig.m
//  Share
//
//  Created by apple_Eric on 4/27/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "APIConfig.h"
#define urlCommon @"api.php?method="
@implementation APIConfig
SINGLETON_M(APIConfig)
- (NSString *)getApiWithMethod:(NSString *)url{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",urlCommon,url];
    return urlString;
}
- (NSString*)registerVerfy{
    return [self getApiWithMethod:@"user.captcha"];
}
- (NSString*)registerAndLogin{
    return [self getApiWithMethod:@"user.register"];
//    return @"api.php?method=user.register";
}
-(NSString *)loginApi{
    return [self getApiWithMethod:@"user.login"];
//    return @"api.php?method=user.login";
}
- (NSString *)forgetPassworkApi{
    return [self getApiWithMethod:@"user.resetpassword"];
//    return @"api.php?method=user.resetpassword";
}
- (NSString *)changeUserInformationApi{
    return [self getApiWithMethod:@"user.setprofile"];
//    return @"api.php?method=user.setprofile";
}
- (NSString *)mapsList{
    return [self getApiWithMethod:@"maps.recommend"];
//    return @"api.php?method=maps.recommend";
}
- (NSString *)mapsDetail{
    return [self getApiWithMethod:@"maps.detail"];
//    return @"api.php?method=maps.detail";
}
- (NSString *)mapsPlacelist{
    return [self getApiWithMethod:@"maps.placelist"];
}
- (NSString*)readMapsDetailApi{
    return [self getApiWithMethod:@"maps.read"];
}
- (NSString *)mapsFollowApi{
    return [self getApiWithMethod:@"maps.follow"];
}
- (NSString *)mapsPlaceWantGoApi{
    return [self getApiWithMethod:@"place.want"];
}
- (NSString *)mapsPlaceBeenApi{
    return [self getApiWithMethod:@"place.been"];
}
- (NSString *)mapsPostListApi{
    return [self getApiWithMethod:@"post.list"];
}
- (NSString *)wantUserListApi{
    return [self getApiWithMethod:@"place.wantlist"];
}
- (NSString *)beenUserListApi{
    return [self getApiWithMethod:@"place.beenlist"];
}
- (NSString *)mapsPostDetailApi{
    return [self getApiWithMethod:@"post.detail"];
}
- (NSString *)commentPostApi{
    return [self getApiWithMethod:@"post.comment"];
}
- (NSString *)postLikeApi{
    return [self getApiWithMethod:@"post.like"];
}
- (NSString *)commentLikeApi{
    return [self getApiWithMethod:@"comment.like"];
}
- (NSString *)placeDetailApi{
    return [self getApiWithMethod:@"place.detail"];
}
- (NSString *)placeMapsListApi{
    return [self getApiWithMethod:@"place.mapslist"];
}
- (NSString *)placePostListApi{
    return [self getApiWithMethod:@"place.postlist"];
}
- (NSString *)deletePostApi{
    return [self getApiWithMethod:@"post.delete"];
}
#pragma mark - 我
- (NSString *)userInforApi{
    return [self getApiWithMethod:@"user.userinfo"];
}
- (NSString *)myMapsListApi{
     return [self getApiWithMethod:@"my.maps"];
}

- (NSString *)myPlacelistApi{
    return [self getApiWithMethod:@"my.placelist"];
}

- (NSString *)placeAddToMapsApi{
    return [self getApiWithMethod:@"place.addtomaps"];
}
- (NSString *)myFansApi{
    return [self getApiWithMethod:@"follow.followers"];
}
- (NSString *)myPostListApi{
    return [self getApiWithMethod:@"my.postlist"];
}
#pragma mark - newCenture
-(NSString *)userFindApi
{
    return [self getApiWithMethod:@"user.find"];
}

-(NSString *)followAdddApi
{
    return [self getApiWithMethod:@"follow.add"];
}

-(NSString *)followFollowingsApi
{
    return [self getApiWithMethod:@"follow.followings"];
}

-(NSString *)followDeleteApi
{
    return [self getApiWithMethod:@"follow.delete"];
}

-(NSString *)contactsSyncApi
{
    return [self getApiWithMethod:@"contacts.sync"];
}
-(NSString *)noticeListApi
{
    return [self getApiWithMethod:@"notice.list"];
}
-(NSString *)shareInviteApi
{
    return [self getApiWithMethod:@"share.invite"];
}
-(NSString *)sharePlaceApi
{
    return [self getApiWithMethod:@"share.place"];
}
-(NSString *)shareMapsApi
{
    return [self getApiWithMethod:@"share.maps"];
}
-(NSString *)shareUserApi
{
    return [self getApiWithMethod:@"share.user"];
}
-(NSString *)sharePostApi
{
    return [self getApiWithMethod:@"share.post"];
}

#pragma mark - 地图
- (NSString *)placeCategoryApi{
    return [self getApiWithMethod:@"place.category"];
}

- (NSString *)placeCommendCategoryApi{
    return [self getApiWithMethod:@"place.recommendcategory"];
}
- (NSString *)addPlaceApi{
    return [self getApiWithMethod:@"place.add"];
}
- (NSString *)placeNearbyApi{
    return [self getApiWithMethod:@"place.nearby"];
}
- (NSString *)searchNearbyUserPlaceApi{
    return [self getApiWithMethod:@"place.nearbyuserplace"];
}
- (NSString *)keyWordSearchUserPlaceApi{
    return [self getApiWithMethod:@"place.searchuserplace"];
}
#pragma mark - 添加地图
- (NSString *)upLoadImageApi{
    return [self getApiWithMethod:@"post.upload"];
}
- (NSString *)postTagApi{
    return [self getApiWithMethod:@"post.tags"];
}
- (NSString *)addPostApi{
    return [self getApiWithMethod:@"post.add"];
}


@end
