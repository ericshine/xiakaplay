//
//  BJNotificationConfig.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//


#import <UIKit/UIKit.h>
/**
 *  修改个人资料
 */
UIKIT_EXTERN NSString* const BJChangeUserInformationNotification;
/**
 *  登陆成功
 */
UIKIT_EXTERN NSString* const BJLoginNotification;
/**
 *  退出登录
 */
UIKIT_EXTERN NSString* const BJLogOutNotification;
/**
 *  需要用户登录  跳转登录页面的通知
 */
UIKIT_EXTERN NSString* const BJNeedUserLoginNotification;
/**
 *  maps 详情刷新数据
 */
UIKIT_EXTERN NSString* const BJMapsDetailNeedReloadDataNotification;
/**
 *  隐藏键盘
 */
UIKIT_EXTERN NSString* const BJHiddenKeyboardNotification;

#pragma mark- 发布动态
/**
 *  发布动态中 所有图片上传完成通知
 */
UIKIT_EXTERN NSString* const BJUploadAllImageNotification;
/**
 *  删除动态
 */
UIKIT_EXTERN NSString* const BJDeletePostNotification;
/**
 *  发布动态
 */
UIKIT_EXTERN NSString* const BJAddPostNotification;
/**
 *  点击去过想去
 */
//UIKIT_EXTERN NSString* const BJBeenOrWantBeenActionNotification;

UIKIT_EXTERN NSString* const BJWantActionNotification;

UIKIT_EXTERN NSString* const BJBeenActionNotification;

/**
 *  专辑发生变化
 */
UIKIT_EXTERN NSString* const BJMapsChangedNotification;
/**
 *  收到通知后打开通知页面并刷新数据
 */
UIKIT_EXTERN NSString* const BJUploadNotification111;
/**
 *   scrollViewDidScroll
 */
UIKIT_EXTERN NSString* const BJscrollViewDidScrollNotification;
/**
 *  选中tableView
 */
UIKIT_EXTERN NSString* const BJDidSelectTableViewNotification;

#pragma mark - 个人信息

UIKIT_EXTERN NSString* const BJNeedReloadUserInformationNotification;





























