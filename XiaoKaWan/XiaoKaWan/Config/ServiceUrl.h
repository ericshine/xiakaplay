//
//  ServiceUrl.h
//  Share
//
//  Created by apple_Eric on 4/27/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
typedef enum {
    SERVICE = 0,
    DevelopmentService = 1,
    ProductionService
}ServiceState;

@interface ServiceUrl : NSObject
@property(nonatomic,copy)NSString *baseUrl;
SINGLETON_H

- (NSString*)serviceUrl;
- (void)changeServiceWithIndex:(ServiceState)index;
@end
