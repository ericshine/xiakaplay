//
//  APIConfig.h
//  Share
//
//  Created by apple_Eric on 4/27/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"

@interface APIConfig : NSObject
SINGLETON_H
#pragma mark - 登录
/**
 *  登陆注册
 *
 *  @return api
 */

- (NSString *)registerVerfy;
/**
 *  注册并登陆
 *
 *  @return api
 */
- (NSString *)registerAndLogin;
/**
 *  登陆
 *
 *  @return api
 */
- (NSString *)loginApi;
/**
 *  maps
 *
 *  @return @return api
 */
/**
 *  忘记密码
 *
 *  @return api
 */
- (NSString *)forgetPassworkApi;
/**
 *  修改个人资料
 *
 *  @return api
 */
- (NSString *)changeUserInformationApi;
/**
 *  获取首页maps
 *
 *  @return return value description
 */




#pragma mark - maps


- (NSString *)mapsList;
/**
 *  获取maps 详情
 *
 *  @return return value description
 */
- (NSString *)mapsDetail;
/**
 *  maps 地点列表
 *
 *  @return return value description
 */
- (NSString *)mapsPlacelist;
/**
 *  maps 阅读统计
 *
 *  @return return value description
 */
- (NSString *)readMapsDetailApi;
/**
 *  maps 详情页面的 follow
 *
 *  @return return value description
 */
- (NSString *)mapsFollowApi;
/**
 *  maps 地点想去api
 *
 *  @return return value description
 */
- (NSString *)mapsPlaceWantGoApi;
/**
 *  maps 地点已去过api
 *
 *  @return return value description
 */
- (NSString *)mapsPlaceBeenApi;
/**
 *  maps 动态列表api
 *
 *  @return return value description
 */
- (NSString *)mapsPostListApi;
/**
 *  想去的人列表
 *
 *  @return return value description
 */
- (NSString *)wantUserListApi;
/**
 *  去过的人列表
 *
 *  @return return value description
 */
- (NSString *)beenUserListApi;
/**
 *  动态详情
 *
 *  @return return value description
 */
- (NSString *)mapsPostDetailApi;
/**
 *  评论动态
 *
 *  @return return value description
 */
- (NSString *)commentPostApi;
/**
 *  动态点赞
 *
 *  @return return value description
 */
- (NSString *)postLikeApi;
/**
 *  评论点赞
 *
 *  @return return value description
 */
- (NSString *)commentLikeApi;
/**
 *  地点详情
 *
 *  @return return value description
 */
- (NSString *)placeDetailApi;
/**
 *  地点maps
 *
 *  @return return value description
 */
- (NSString *)placeMapsListApi;
/**
 *  地点动态
 *
 *  @return return value description
 */
- (NSString *)placePostListApi;
/**
 *  删除动态
 *
 *  @return return value description
 */
- (NSString *)deletePostApi;

#pragma mark - 我

- (NSString *)userInforApi;
/**
 *  获取我的maps
 *
 *  @return return value description
 */
- (NSString *)myMapsListApi;
/**
 *  获取我的地点列表
 *
 *  @return return value description
 */
- (NSString *)myPlacelistApi;
/**
 *  地点加入榜单api
 *
 *  @return return value description
 */
- (NSString *)placeAddToMapsApi;
/**
 *  我的粉丝
 *
 *  @return return value description
 */
- (NSString *)myFansApi;
/**
 *  我的动态列表
 *
 *  @return return value description
 */
- (NSString *)myPostListApi;

#pragma mark - newCenture
/**
 *  查找用户
 *
 *  @return return value description
 */
-(NSString *)userFindApi;
/**
 *  关注用户
 *
 *  @return return value description
 */
-(NSString *)followAdddApi;
/**
 *  关注列表
 *
 *  @return return value description
 */
-(NSString *)followFollowingsApi;
/**
 *  取消关注某人
 *
 *  @return return value description
 */
-(NSString *)followDeleteApi;
/**
 *  同步通讯录
 *
 *  @return return value description
 */
-(NSString *)contactsSyncApi;
/**
 *  通知列表
 *
 *  @return return value description
 */
-(NSString *)noticeListApi;
/**
 *  分享给好友
 *
 *  @return return value description
 */
-(NSString *)shareInviteApi;
/**
 *  分享地点
 *
 *  @return return value description
 */
-(NSString *)sharePlaceApi;
/**
 *  分享专辑
 *
 *  @return return value description
 */
-(NSString *)shareMapsApi;
/**
 *  分享个人主页
 *
 *  @return return value description
 */
-(NSString *)shareUserApi;
/**
 *  分享动态
 *
 *  @return return value description
 */
-(NSString *)sharePostApi;


#pragma mark - 地图
/**
 *  获取地点的分类
 *
 *  @return return value description
 */
- (NSString *)placeCategoryApi;

- (NSString *)placeCommendCategoryApi;
/**
 *  添加地点
 *
 *  @return return value description
 */
- (NSString *)addPlaceApi;
/**
 *  周边
 *
 *  @return return value description
 */
- (NSString *)placeNearbyApi;
/**
 *  搜索用户创建的附近点
 *
 *  @return return value description
 */
- (NSString *)searchNearbyUserPlaceApi;
/**
 *  /
 *
 *  @return 搜索自有poi
 */
- (NSString *)keyWordSearchUserPlaceApi;

#pragma mark  添加动态
- (NSString *)upLoadImageApi;
/**
 *  获取标签
 *
 *  @return return value description
 */
- (NSString *)postTagApi;
/**
 *  添加动态
 *
 *  @return return value description
 */
- (NSString *)addPostApi;
@end
