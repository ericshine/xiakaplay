//
//  ServiceUrl.m
//  Share
//
//  Created by apple_Eric on 4/27/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "ServiceUrl.h"
#import "ClientNetwork.h"
#define DEVELOPMENTSERVICEURL @"http://112.124.39.140/"
#define PRODUCTIONSERVICEURL @"https://api.uu19.com/"
static NSString *const KbaseUrl = @"KbaseUrlKey";
@implementation ServiceUrl
SINGLETON_M(ServiceUrl);
- (NSString *)serviceUrl
{
    if(self.baseUrl == nil){
# ifdef DEBUG
      return DEVELOPMENTSERVICEURL;
# else
        return DEVELOPMENTSERVICEURL;
# endif
        
    }
    return self.baseUrl;
}
- (void)changeServiceWithIndex:(ServiceState)index
{
    [ClientNetwork sharedInstance].sessionManage = nil;
    switch (index) {
        case ProductionService:
            self.baseUrl = PRODUCTIONSERVICEURL;
            break;
        case DevelopmentService:
            self.baseUrl = DEVELOPMENTSERVICEURL;
            break;
            
        default:
            break;
    }
}
- (void)setBaseUrl:(NSString *)baseUrl
{
    [[NSUserDefaults standardUserDefaults] setValue:baseUrl forKey:KbaseUrl];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)baseUrl
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:KbaseUrl];
}
@end
