//
//  BJNotificationConfig.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJNotificationConfig.h"

 NSString *const BJChangeUserInformationNotification = @"BJChangeUserInformationNotification";

NSString *const BJLoginNotification = @"BJLoginNotification";

NSString *const BJLogOutNotification = @"BJLogOutNotification";

NSString *const BJNeedUserLoginNotification = @"BJNeedUserLoginNotification";

NSString *const BJMapsDetailNeedReloadDataNotification = @"BJMapsDetailNeedReloadDataNotification";

NSString *const BJHiddenKeyboardNotification = @"BJHiddenKeyboardNotification";

NSString *const BJUploadAllImageNotification = @"BJUploadAllImageNotification";

NSString* const BJDeletePostNotification  = @"BJDeletePostNotification";

NSString* const BJAddPostNotification = @"BJAddPostNotification";

NSString* const BJWantActionNotification = @"BJWantActionNotification";
NSString* const BJBeenActionNotification = @"BJBeenActionNotification";

NSString* const BJMapsChangedNotification = @"BJMapsChangedNotification";

NSString* const BJscrollViewDidScrollNotification = @"BJscrollViewDidScrollNotification";

NSString* const BJDidSelectTableViewNotification = @"BJDidSelectTableViewNotification";

#pragma mark - user`s information

NSString* const BJNeedReloadUserInformationNotification = @"BJNeedReloadUserInformationNotification";
