//
//  HeadFileConfig.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/14/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#ifndef HeadFileConfig_h
#define HeadFileConfig_h
#import "Singleton.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
#import "BJNotificationConfig.h"
#import "BJUIImageView.h"
#import "NSString+Cagetory.h"
#import "QAlertView.h"
#import "NSDate+BJDateFormat.h"
#import "UIImage+Category.h"
#import "BJPregressHUD.h"
#import "UserConfig.h"
#import "BJAlertController.h"
#import "UINavigationController+Category.h"
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height


#endif /* HeadFileConfig_h */
