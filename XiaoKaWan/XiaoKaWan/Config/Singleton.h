//
//  Singleton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#ifndef Singleton_h
#define Singleton_h
#define SINGLETON_H +(instancetype)sharedInstance;
#define SINGLETON_M(classname)\
+(instancetype)sharedInstance\
{     \
static classname *instance = nil;\
if(instance == nil)\
{         \
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
instance = [[classname alloc] init];\
});\
}   \
return instance;\
}

#endif /* Singleton_h */

