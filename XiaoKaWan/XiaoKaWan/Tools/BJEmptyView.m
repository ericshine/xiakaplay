//
//  BJEmptyView.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJEmptyView.h"
#import "BJpch.h"

@interface BJEmptyView()

@property (nonatomic,weak)UIImageView *emptyImageView;

@property (nonatomic,weak)UILabel *titleLable ;

@property (nonatomic,weak)UIView *topGrayView;

@end

@implementation BJEmptyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
        [self addsubs];
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame titel:(NSString *)text imageName:(NSString *)imageName
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
        [self addsubs];
        
        self.titleLable.text=text;
        
        self.emptyImageView.image=[UIImage imageNamed:imageName];
        
    }
    return self;
}

-(void)addsubs
{
    UIImageView *emptyImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"emptyIcon"]];
    self.emptyImageView=emptyImageView;
    [self addSubview:emptyImageView];
    emptyImageView.contentMode=UIViewContentModeCenter;
    
    UILabel *titleLable=[[UILabel alloc]init];
    self.titleLable=titleLable;
    [self addSubview:titleLable];
    titleLable.text=@"暂无消息";
    titleLable.font=[UIFont systemFontOfSize:17];
    titleLable.textColor=GrayColor(165);
    titleLable.textAlignment=NSTextAlignmentCenter;
    
    UIView *topGrayView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 18 )];
    topGrayView.backgroundColor=rgbColor(239, 239, 244, 1);
    [self addSubview:topGrayView];
    self.topGrayView=topGrayView;
   
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    
    @weakify(self)
    [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.center.equalTo(self);
        
    }];
    
   
    [self.emptyImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.titleLable.mas_top).with.offset(-20);
        make.width.mas_equalTo(182);
        make.height.mas_equalTo(170);
        
    }];
    
}

@end
