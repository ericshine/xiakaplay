//
//  BJPlaceholderTextView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJPlaceholderTextView.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@interface BJPlaceholderTextView()
@property(nonatomic,strong)UILabel *placeholderLabel;
@end
@implementation BJPlaceholderTextView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)textChanged:(NSNotification*)notification{
    if(self.text.length>0) _placeholderLabel.hidden = YES;
    else _placeholderLabel.hidden = NO;
}
- (void)setPlaceHolder:(NSString *)placeHolder{
    self.placeholderLabel.text = placeHolder;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.left.equalTo(self.mas_left).offset(10);
    }];
}
- (UILabel*)placeholderLabel
{
    if(_placeholderLabel==nil){
        _placeholderLabel = [UILabel creatLabelWithFont:self.font.pointSize color:0x919191];
        [self addSubview:_placeholderLabel];
    }
    return _placeholderLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
