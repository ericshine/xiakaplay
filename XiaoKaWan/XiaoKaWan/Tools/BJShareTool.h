//
//  BJShareTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/29.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UMengSocial/UMSocial.h>
#import <UMSocialSnsData.h>

typedef NS_ENUM(NSInteger ,SDKType){
    
    wxsession=0,
    sina=1,
    qq=2,
    wxtimeline=3,
    qzone=4
    
};

@interface BJShareTool : NSObject<UMSocialUIDelegate>

+(void)shareInviteFriendsWithType:(SDKType )type viewController:(UIViewController *__weak)VC;

+(void)shareAddressDetailWithViewController:(UIViewController *__weak)VC placeID:(NSString *)placeID;

+(void)shareAlbumWithTypeViewController:(UIViewController *__weak)VC mapsID:(NSString *)mapsID;

+(void)shareHomePageWithTypeViewController:(UIViewController *__weak)VC UID:(NSString *)uid;

+(void)shareDynamicDetailWithTypeViewController:(UIViewController *__weak)VC postID:(NSString *)postID;

+(void)shareDynamicDetailWithType:(SDKType)type viewController:(UIViewController *__weak)VC postID:(NSString *)postID;


@end
