//
//  BJPregressHUD.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJPregressHUD.h"
#import <SVProgressHUD.h>
@implementation BJPregressHUD
+ (void)showWithStatus:(NSString*)status
{
    [SVProgressHUD showWithStatus:status];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
}
+ (void)dissMiss{
    [SVProgressHUD dismiss];
}

+(void)showSuccessWithStatus:(NSString *)status
{
    [SVProgressHUD showSuccessWithStatus:status];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss] ;
    });
}
+(void)showErrorWithStatus:(NSString *)status
{
    [SVProgressHUD showErrorWithStatus:status];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss] ;
    });
}
@end
