//
//  BJSegmentView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJView.h"

@interface BJSegmentView : BJView
- (instancetype)initWithItmes:(NSArray *)items withAnimation:(BOOL)animation;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,copy)void(^selectAtIndex)(NSInteger index);
@end
