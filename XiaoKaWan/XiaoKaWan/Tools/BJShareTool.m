//
//  BJShareTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/29.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJShareTool.h"
#import "ClientNetwork.h"
#import "BJpch.h"
#import "BJShareModel.h"

static NSArray *shareModelArray=nil;
static BJShareTool *sharetool=nil;

@implementation BJShareTool

/*
 [
	{
	title : 发的发生大,
	icon : http://static.uu19.com/images/share/logo.png,
	type : qq,
	sub_title : 来自小咖玩的地点分享,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 },
	{
	title : 发的发生大,
	icon : http://static.uu19.com/images/share/logo.png,
	type : qzone,
	sub_title : 来自小咖玩的地点分享,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 },
	{
	title : 发的发生大,
	type : wxsession,
	sub_title : 来自小咖玩的地点分享,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 },
	{
	title : 发的发生大,
	type : wxtimeline,
	sub_title : 来自小咖玩的地点分享,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 },
	{
	type : sina,
	title : 最流行的地方——发的发生大 http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1自小咖玩的地点分享,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 },
	{
	type : sina,
	title : 最流行的地方——发的发生大 http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1\345分享自 @小咖玩,
	url : http://112.124.39.140/index.php?m=default&c=share&a=place&uid=&place_id=1
 }
 ]
 **/


+(void)shareInviteFriendsWithType:(SDKType)type viewController:(UIViewController *__weak)VC
{
    NSString *typeStr;
    if (type==wxsession) {
        typeStr=@"wxsession";
    }else if (type==sina)
    {
        typeStr=@"sina";
    }else if (type==qq)
    {
        typeStr=@"qq";
    }
    
    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"share_type":typeStr};
    }else
    {
        parameter=@{@"share_type":typeStr};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] shareInviteApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@===%d",obj,succeed);
        
        if (succeed) {
            BJShareModel *shareModel=[BJShareModel objectWithKeyValues:obj];
            
            [self shareWithSDKtype:type ShareModel:shareModel ViewController:VC];
            
        }
        
    }];
}


+(void)shareAddressDetailWithViewController:(UIViewController *__weak)VC placeID:(NSString *)placeID
{

    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"place_id":placeID,@"share_type":@"all"};
    }else
    {
        parameter=@{@"place_id":placeID,@"share_type":@"all"};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] sharePlaceApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@",obj);

        if (succeed) {
            shareModelArray=[BJShareModel objectArrayWithKeyValuesArray:obj];
            
            [self shareCommenWith:shareModelArray ViewController:VC];
        }
        

        
    }];
}

+(void)shareAlbumWithTypeViewController:(UIViewController *__weak)VC mapsID:(NSString *)mapsID
{

    
    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"maps_id":mapsID,@"share_type":@"all"};
    }else
    {
        parameter=@{@"maps_id":mapsID,@"share_type":@"all"};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] shareMapsApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@",obj);
        
        if (succeed) {
            shareModelArray=[BJShareModel objectArrayWithKeyValuesArray:obj];
            
            [self shareCommenWith:shareModelArray ViewController:VC];
        }
        
        
        
    }];
    
}

+(void)shareHomePageWithTypeViewController:(UIViewController *__weak)VC UID:(NSString *)uid
{

    
    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"to_uid":uid,@"share_type":@"all"};
    }else
    {
        parameter=@{@"to_uid":uid,@"share_type":@"all"};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] shareUserApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@",obj);
        
        if (succeed) {
            shareModelArray=[BJShareModel objectArrayWithKeyValuesArray:obj];
            
            [self shareCommenWith:shareModelArray ViewController:VC];
        }
        
        
        
    }];
    
}

+(void)shareDynamicDetailWithTypeViewController:(UIViewController *__weak)VC postID:(NSString *)postID
{

    
    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"post_id":postID,@"share_type":@"all"};
    }else
    {
        parameter=@{@"post_id":postID,@"share_type":@"all"};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] sharePostApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@",obj);
        
        if (succeed) {
            shareModelArray=[BJShareModel objectArrayWithKeyValuesArray:obj];
            
            [self shareCommenWith:shareModelArray ViewController:VC];
        }
        
        
        
    }];
    
}

+(void)shareDynamicDetailWithType:(SDKType)type viewController:(UIViewController *__weak)VC postID:(NSString *)postID
{
    NSDictionary *parameter;
    if ([UserConfig sharedInstance].user_token.length) {
        parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"post_id":postID,@"share_type":@"all"};
    }else
    {
        parameter=@{@"post_id":postID,@"share_type":@"all"};
    }
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] sharePostApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@",obj);
        
        if (succeed) {
            shareModelArray=[BJShareModel objectArrayWithKeyValuesArray:obj];
            
            for (BJShareModel *shareModel in shareModelArray) {
                
                if (shareModel.SDKtype==type) {
                    [self shareWithSDKtype:type ShareModel:shareModel ViewController:VC];
                }
            }
            
        }
        
        
        
    }];
}


+(void)shareCommenWith:(NSArray *)shareModelArray ViewController:(UIViewController *)VC
{
    sharetool=[[BJShareTool alloc]init];
    BJShareModel *shareModel=[sharetool shareModelInArrayWith:@"qq"];
    
    [UMSocialData defaultData].extConfig.title =shareModel.title;
    [UMSocialData defaultData].extConfig.qqData.url = shareModel.url;
    [UMSocialSnsService presentSnsIconSheetView:VC
                                         appKey:@"575d5600e0f55a80c90013cc"
                                      shareText:shareModel.sub_title
                                     shareImage:nil
                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone]
                                       delegate:sharetool];
}

+(void)shareWithSDKtype:(SDKType )sdktype ShareModel:(BJShareModel *)shareModel ViewController:(UIViewController *)VC
{
    if (sdktype==wxsession) {
        [UMSocialData defaultData].extConfig.wechatSessionData.title = shareModel.title;
        [UMSocialData defaultData].extConfig.wechatSessionData.url = shareModel.url;
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                            shareModel.icon];
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:shareModel.sub_title image:nil location:nil urlResource:urlResource presentedController:VC completion:^(UMSocialResponseEntity *shareResponse){
            if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                NSLog(@"分享成功！");
            }
        }];
        
    }else if (sdktype==sina)
    {
        [UMSocialData defaultData].extConfig.title = shareModel.title;
        
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:shareModel.icon];
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToSina] content:[NSString stringWithFormat:@"%@ %@",shareModel.title,shareModel.url] image:nil location:nil urlResource:urlResource presentedController:VC completion:^(UMSocialResponseEntity *shareResponse){
            if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                NSLog(@"分享成功！");
            }
        }];
        
    }else if (sdktype==qq)
    {
        [UMSocialData defaultData].extConfig.title = shareModel.title;
        [UMSocialData defaultData].extConfig.qqData.url = shareModel.url;
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                            shareModel.icon];
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToQQ] content:shareModel.sub_title image:nil location:nil urlResource:urlResource presentedController:VC completion:^(UMSocialResponseEntity *shareResponse){
            if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                NSLog(@"分享成功！");
            }
        }];
        
    }else if (sdktype==wxtimeline)
    {
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = shareModel.title;
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = shareModel.url;
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                            shareModel.icon];
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:shareModel.sub_title image:nil location:nil urlResource:urlResource presentedController:VC completion:^(UMSocialResponseEntity *shareResponse){
            if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                NSLog(@"分享成功！");
            }
        }];
        
    }else if (sdktype==qzone)
    {
        [UMSocialData defaultData].extConfig.title = shareModel.title;
        [UMSocialData defaultData].extConfig.qzoneData.url = shareModel.url;
        [UMSocialData defaultData].extConfig.qzoneData.shareText = shareModel.sub_title;
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                            shareModel.icon];
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToQzone] content:shareModel.sub_title image:nil location:nil urlResource:urlResource presentedController:VC completion:^(UMSocialResponseEntity *shareResponse){
            if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                NSLog(@"分享成功！");
            }
        }];
    }
}



-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData
{
    

    BJShareModel *shareModel=[self shareModelInArrayWith:platformName];
    
    if ([platformName isEqualToString:@"wxsession"]) {
        
        socialData.extConfig.wechatSessionData.title = shareModel.title;
        socialData.extConfig.wechatSessionData.shareText=shareModel.sub_title;
        socialData.extConfig.wechatSessionData.url = shareModel.url;
        socialData.extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
        socialData.urlResource=[[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                shareModel.icon];
        
        
        
    }else if ([platformName isEqualToString:@"wxtimeline"])
    {
        
        socialData.extConfig.wechatTimelineData.title = shareModel.title;
        socialData.extConfig.wechatTimelineData.shareText=shareModel.title;
        socialData.extConfig.wechatTimelineData.url = shareModel.url;
        socialData.extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
        socialData.urlResource=[[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                shareModel.icon];
        
    }else if ([platformName isEqualToString:@"sina"])
    {
        
        socialData.shareText=[NSString stringWithFormat:@"%@ ",shareModel.title];
        
        socialData.urlResource= [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:shareModel.icon];
        
    }else if ([platformName isEqualToString:@"qq"])
    {
        socialData.extConfig.qqData.title = shareModel.title;
        socialData.extConfig.qqData.shareText=shareModel.sub_title;
        socialData.extConfig.qqData.url = shareModel.url;
        socialData.urlResource=[[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                shareModel.icon];
        
        
    }else if ([platformName isEqualToString:@"qzone"])
    {
        socialData.extConfig.qzoneData.title = shareModel.title;
        socialData.extConfig.qzoneData.shareText=shareModel.sub_title;
        socialData.extConfig.qzoneData.url = shareModel.url;
        socialData.urlResource=[[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                shareModel.icon];
    }
    
    
}

-(BJShareModel *)shareModelInArrayWith:(NSString *)str
{
    for (BJShareModel *shareModel in shareModelArray) {
        if ([shareModel.type isEqualToString:str]) {
            
            return shareModel;
        }
    }
    return nil;
}



@end
