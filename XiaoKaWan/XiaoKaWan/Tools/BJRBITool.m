//
//  BJRBITool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/25.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJRBITool.h"
#import "UMMobClick/MobClick.h"


@implementation BJRBITool

+(void)UmengMobWithKeyStr:(NSString *)str
{
    [MobClick event:str];
}

+(void)UmengMobWithKeyStr:(NSString *)str Attribute:(NSDictionary *)dic
{
    [MobClick event:str attributes:dic];
}

/*
 1 首页－点击搜索框
 **/
+(void)searchBarClick{
    [self UmengMobWithKeyStr:@"searchBarClick"];
}
/*
 2首页－搜索不到地点－点击创建地点
 **/
+(void)searchAddressNoFoundAndCreatAddress{
    [self UmengMobWithKeyStr:@"searchAddressNoFoundAndCreatAddress"];
}
/*
 3首页－点击不同的分类进行切换     key1=分类的位置
 **/
+(void)diffrentCategorySwitchWith:(NSInteger )position{
   
}
/*
 4首页－点击有内容的地点
 **/
+(void)addressWithContentClick{
    [self UmengMobWithKeyStr:@"addressWithContentClick"];
}
/*
 5首页－点击地点弹窗的导航
 **/
+(void)addressPOPViewClick{
    [self UmengMobWithKeyStr:@"addressPOPViewClick"];
}
/*
 6首页－点击弹窗中查看详情进入地点详情页
 **/
+(void)addressPOPViewAddressDetailClick{
    [self UmengMobWithKeyStr:@"addressPOPViewAddressDetailClick"];
}
/*
 7地点详情页－点击加入专辑
 **/
+(void)addressDetailJoinAlbumsClick{
    [self UmengMobWithKeyStr:@"addressDetailJoinAlbumsClick"];
}
/*
 8地点详情页－点击去过按钮
 **/
+(void)addressDetailHaveGoneClick{
    [self UmengMobWithKeyStr:@"addressDetailHaveGoneClick"];
}
/*
 9地点详情页－点击想去按钮
 **/
+(void)addressDetailWantGoClick{
    [self UmengMobWithKeyStr:@"addressDetailWantGoClick"];
}
/*
 10地点详情页－点击专辑的tab
 **/
+(void)addressDetailAlbumTabClick{
    [self UmengMobWithKeyStr:@"addressDetailAlbumTabClick"];
}
/*
 11地点详情页－点击动态的tab
 **/
+(void)addressDetailDynamicTabClick{
    [self UmengMobWithKeyStr:@"addressDetailDynamicTabClick"];
}
/*
 12地点详情页－点击去过（想去）的人的list
 **/
+(void)addressDetailHaveGoneListClick{
    [self UmengMobWithKeyStr:@"addressDetailHaveGoneListClick"];
}
/*
 13地点详情页－点击进入动态详情页
 **/
+(void)addressDetailIntoDynamickDetailClick{
    [self UmengMobWithKeyStr:@"addressDetailIntoDynamickDetailClick"];
}
/*
 14地点详情页－点击进入地图详情页
 **/
+(void)addressDetailIntoMapDetailClick{
    [self UmengMobWithKeyStr:@"addressDetailIntoMapDetailClick"];
}
/*
 15地点详情页－点击进行导航
 **/
+(void)addressDetailNavigationClick{
    [self UmengMobWithKeyStr:@"addressDetailNavigationClick"];
}
/*
 16地点详情页－点击分享按钮      key1=分享的内容（动态/地点详情/专辑主页/个人主页）
 **/
+(void)addressDetailShareClickWith:(NSInteger )content{
    
}
/*
 17地点详情页－点击分享给第三方（用参数区分）      key1＝分享的第三方源
 **/
+(void)addressDetailShareToOtherSDK:(NSInteger)type{
    
}
/*
 18地图详情页－点击关注
 **/
+(void)mapDetailAttentionClick{
    [self UmengMobWithKeyStr:@"mapDetailAttentionClick"];
}
/*
 19地图详情页－点击取消关注
 **/
+(void)mapDetailCancleAttentionClick{
    [self UmengMobWithKeyStr:@"mapDetailCancleAttentionClick"];
}
/*
 20地图详情页－点击动态tab
 **/
+(void)mapDetailDynamicTabClick{
    [self UmengMobWithKeyStr:@"mapDetailDynamicTabClick"];
}
/*
 21地图详情页－点击地点tab
 **/
+(void)mapDetailAddressTabClick{
    [self UmengMobWithKeyStr:@"mapDetailAddressTabClick"];
}
/*
 22地图详情页－点击进入动态详情页
 **/
+(void)mapDetailIntoDynamicDetailClick{
    [self UmengMobWithKeyStr:@"mapDetailIntoDynamicDetailClick"];
}
/*
 23地图详情页－点击进入地点详情页
 **/
+(void)mapDetailIntoAddressDetailClick{
    [self UmengMobWithKeyStr:@"mapDetailIntoAddressDetailClick"];
}
/*
 24地图详情页－点击去过(想去)的人的list
 **/
+(void)mapDetailWantGoAndHaveGonePeopleListClick{
    [self UmengMobWithKeyStr:@"mapDetailWantGoAndHaveGonePeopleListClick"];
}
/*
 25地图详情页－点击想去按钮
 **/
+(void)mapDetailWantGoBtnClick{
    [self UmengMobWithKeyStr:@"mapDetailWantGoBtnClick"];
}
/*
 26地图详情页－点击去过按钮
 **/
+(void)mapDetailHaveGoneBtnClick{
    [self UmengMobWithKeyStr:@"mapDetailHaveGoneBtnClick"];
}
/*
 27动态详情页－点击去过按钮
 **/
+(void)dynamicDetailHaveGoneClick{
    [self UmengMobWithKeyStr:@"dynamicDetailHaveGoneClick"];
}
/*
 28动态详情页－点击想去按钮
 **/
+(void)dynamicDetailWantGoBtnClick{
    [self UmengMobWithKeyStr:@"dynamicDetailWantGoBtnClick"];
}
/*
 29动态详情页－点击查看大图
 **/
+(void)dynamicDetailScanBigImgClick{
    [self UmengMobWithKeyStr:@"dynamicDetailScanBigImgClick"];
}
/*
 30动态详情页－对他人的评论点赞
 **/
+(void)dynamicDetailZanToOthersRemarkClick{
    [self UmengMobWithKeyStr:@"dynamicDetailZanToOthersRemarkClick"];
}
/*
 31动态详情页－对动态点赞
 **/
+(void)dynamicDetailZanToDynamicClick{
    [self UmengMobWithKeyStr:@"dynamicDetailZanToDynamicClick"];
}
/*
 32动态详情页－点击输入框进行评论
 **/
+(void)dynamicDetailRemarkClick{
    [self UmengMobWithKeyStr:@"dynamicDetailRemarkClick"];
}
/*
 33动态详情页－点击进入地点详情页
 **/
+(void)dynamicDetailIntoAddressDetailClick{
    [self UmengMobWithKeyStr:@"dynamicDetailIntoAddressDetailClick"];
}
/*
 34动态详情页－点击想去（去过）的人的list
 **/
+(void)dynamicDetailWantGoAndHaveGoneClick{
    [self UmengMobWithKeyStr:@"dynamicDetailWantGoAndHaveGoneClick"];
}
/*
 35动态详情页－点击删除动态
 **/
+(void)dynamicDetailDynamicDeleteClick{
    [self UmengMobWithKeyStr:@"dynamicDetailDynamicDeleteClick"];
}
/*
 36动态详情页－点击确认删除动态
 **/
+(void)dynamicDetailDynamicDeleteConfirmClick{
    [self UmengMobWithKeyStr:@"dynamicDetailDynamicDeleteConfirmClick"];
}
/*
 37个人主页－点击“关注”
 **/
+(void)homePageAttentionClick{
    [self UmengMobWithKeyStr:@"homePageAttentionClick"];
}
/*
 38个人主页－点击“粉丝”
 **/
+(void)homePageFansClick{
    [self UmengMobWithKeyStr:@"homePageFansClick"];
}
/*
 39个人主页－点击关注他人
 **/
+(void)homePageOtherAttentionClick{
    [self UmengMobWithKeyStr:@"homePageOtherAttentionClick"];
}
/*
 40个人主页－点击取消关注他人（控件里的取消关注）
 **/
+(void)homePageOtherAttentionCancleClick{
    [self UmengMobWithKeyStr:@"homePageOtherAttentionCancleClick"];
}
/*
 41个人主页－点击设置
 **/
+(void)homePageSettingClick{
    [self UmengMobWithKeyStr:@"homePageSettingClick"];
}
/*
 42个人主页－点击动态tab
 **/
+(void)homePageDynamicTabClick{
    [self UmengMobWithKeyStr:@"homePageDynamicTabClick"];
}
/*
 43个人主页－点击去过tab
 **/
+(void)homePageHaveGoneTabClick{
    [self UmengMobWithKeyStr:@"homePageHaveGoneTabClick"];
}
/*
 44个人主页－点击想去tab
 **/
+(void)homePageWantGoTabClick{
    [self UmengMobWithKeyStr:@"homePageWantGoTabClick"];
}
/*
 45个人主页－点击专辑tab
 **/
+(void)homePageAlbumTabClick{
    [self UmengMobWithKeyStr:@"homePageAlbumTabClick"];
}
/*
 46个人主页－点击具体某个专辑进入到专辑页
 **/
+(void)homePageAlbumClick{
    [self UmengMobWithKeyStr:@"homePageAlbumClick"];
}
/*
 47个人主页－点击具体动态，进入动态详情页
 **/
+(void)homePageDynamicClick{
    [self UmengMobWithKeyStr:@"homePageDynamicClick"];
}
/*
 48个人主页－点击具体地点，进入地点的详情页
 **/
+(void)homePageAddressClick{
    [self UmengMobWithKeyStr:@"homePageAddressClick"];
}
/*
 49个人主页－点击查看去过（想去）的人的列表
 **/
+(void)homePageScanWantGoAndHaveGonePeopleListClick{
    [self UmengMobWithKeyStr:@"homePageScanWantGoAndHaveGonePeopleListClick"];
}
/*
 50一级tab－点击地图主页
 **/
+(void)level1TabMapHomePageClick{
    [self UmengMobWithKeyStr:@"level1TabMapHomePageClick"];
}
/*
 51一级tab－点击消息中心
 **/
+(void)level1TabNewsCentureClick{
    [self UmengMobWithKeyStr:@"level1TabNewsCentureClick"];
}
/*
 52一级tab－点击个人中心
 **/
+(void)level1PersonalCentureClick{
    [self UmengMobWithKeyStr:@"level1PersonalCentureClick"];
}
/*
 53发布－点击相机发布
 **/
+(void)PublishCameraClick{
    [self UmengMobWithKeyStr:@"PublishCameraClick"];
}
/*
 54发布－点击相册发布
 **/
+(void)PublishPhotoGalleryClick{
    [self UmengMobWithKeyStr:@"PublishPhotoGalleryClick"];
}
/*
 55发布－点击文字发布
 **/
+(void)PublishTextClick{
    [self UmengMobWithKeyStr:@"PublishTextClick"];
}
/*
 56发布－从相机进入，点击拍照
 **/
+(void)PublishTakePhotoClick{
    [self UmengMobWithKeyStr:@"PublishTakePhotoClick"];
}
/*
 57发布－从相册进入，点击下一步
 **/
+(void)PublishPhotoGalleryAndNextClick{
    [self UmengMobWithKeyStr:@"PublishPhotoGalleryAndNextClick"];
}
/*
 58发布主页－点击添加标签
 **/
+(void)PublishHomePageAddLableClick{
    [self UmengMobWithKeyStr:@"PublishHomePageAddLableClick"];
}
/*
 59发布主页－点击选择位置
 **/
+(void)PublishHomePageChoosePositionClick{
    [self UmengMobWithKeyStr:@"PublishHomePageChoosePositionClick"];
}
/*
 60发布主页－点击编辑更多
 **/
+(void)PublishHomePageEditMoreClick{
    [self UmengMobWithKeyStr:@"PublishHomePageEditMoreClick"];
}
/*
 61发布主页－点击时间选择
 **/
+(void)PublishHomePageChooseTimeClick{
    [self UmengMobWithKeyStr:@"PublishHomePageChooseTimeClick"];
}
/*
 62发布主页－点击选择榜单
 **/
+(void)PublishHomePageChooseListClick{
    [self UmengMobWithKeyStr:@"PublishHomePageChooseListClick"];
}
/*
 63发布主页－选择推荐榜单并确定
 **/
+(void)PublishHomePageRecommendListAndConfirmClick{
    [self UmengMobWithKeyStr:@"PublishHomePageRecommendListAndConfirmClick"];
}
/*
 64发布主页－选择位置中点击新建地址
 **/
+(void)PublishHomePageChoosePositionAndAddNewAddress{
    [self UmengMobWithKeyStr:@"PublishHomePageChoosePositionAndAddNewAddress"];
}
/*
 65发布主页－点击发布并成功发布
 **/
+(void)PublishHomePageSuccessPublishClick{
    [self UmengMobWithKeyStr:@"PublishHomePageSuccessPublishClick"];
}
/*
 66发布主页－点击添加更多图片
 **/
+(void)PublishHomePageAddMoreImgClick{
    [self UmengMobWithKeyStr:@"PublishHomePageAddMoreImgClick"];
}
/*
 67发布成功－点击查看动态
 **/
+(void)PublishSuccessScanDynamicClick{
    [self UmengMobWithKeyStr:@"PublishSuccessScanDynamicClick"];
}
/*
 68发布成功－点击再发一条动态
 **/
+(void)PublishSuccessPublicOneMoreDynamicClick{
    [self UmengMobWithKeyStr:@"PublishSuccessPublicOneMoreDynamicClick"];
}
/*
 69创建地点主页－下一步
 **/
+(void)addAddressHomePageAndNextClick{
    [self UmengMobWithKeyStr:@"addAddressHomePageAndNextClick"];
}
/*
 70创建地点－点击成功创建
 **/
+(void)addAddressAddSuccess{
    [self UmengMobWithKeyStr:@"addAddressAddSuccess"];
}
/*
 71消息中心－点击搜索好友
 **/
+(void)newCentureSearchFriendsClick{
    [self UmengMobWithKeyStr:@"newCentureSearchFriendsClick"];
}
/*
 72消息中心－从搜索出来的好友中点击关注或取关
 **/
+(void)newCentureSearchResultAddAttentionOrCancleAttentionClick{
    [self UmengMobWithKeyStr:@"newCentureSearchResultAddAttentionOrCancleAttentionClick"];
}
/*
 73消息中心－点击关注人的操作按钮——取关
 **/
+(void)newCentureCancleAttentionClick{
    [self UmengMobWithKeyStr:@"newCentureCancleAttentionClick"];
}
/*
 74消息中心－点击邀请好友（微信/微博/朋友圈）    key1=邀请入口
 **/
+(void)newCentureInviteFriendsClickWith:(NSInteger )OtherSDK{
    
}
/*
 75消息中心－点击查看关注者的主页
 **/
+(void)newCentureScanAttentionPersonsHomePage{
    [self UmengMobWithKeyStr:@"newCentureScanAttentionPersonsHomePage"];
}
/*
 76消息中心－点击通讯录邀请好友
 **/
+(void)newCentureContactInviteFriends{
    [self UmengMobWithKeyStr:@"newCentureContactInviteFriends"];
}
/*
 77消息中心－点击通讯录好友中的加关注
 **/
+(void)newCentureContactAddAttention{
    [self UmengMobWithKeyStr:@"newCentureContactAddAttention"];
}
/*
 78消息中心－点击通讯录好友中的“邀请他”
 **/
+(void)newCentureContactInviteHimClick{
    [self UmengMobWithKeyStr:@"newCentureContactInviteHimClick"];
}
/*
 79消息中心－查看通知的消息     key1=通知消息的分类（点赞、评论、@、官方）
 **/
+(void)newCentureScanPushNewsWith:(NSInteger )type{
    
}
/*
 80消息中心－对关注我的人的快捷关注操作
 **/
+(void)newCentureAttentionPersonWhoAttentionMe{
    [self UmengMobWithKeyStr:@"newCentureAttentionPersonWhoAttentionMe"];
}
/*
 81消息中心－查看动态的消息   key1＝动态消息的分类（新建动态、新建地址）
 **/
+(void)newCentureScanDynamicNewsWith:(NSInteger )type{
   
}
/*
 82设置页－查看头像
 **/
+(void)settingPageScanHeadPortrait{
    [self UmengMobWithKeyStr:@"settingPageScanHeadPortrait"];
}
/*
 83设置页－点击更换头像
 **/
+(void)settingPageReplaceHeadPortrait{
    [self UmengMobWithKeyStr:@"settingPageReplaceHeadPortrait"];
}
/*
 84设置页－更换头像－拍照
 **/
+(void)settingPageReplaceHeadPortraitFromTakePhoto{
    [self UmengMobWithKeyStr:@"settingPageReplaceHeadPortraitFromTakePhoto"];
}
/*
 85设置页－更换头像－从相册选择更换
 **/
+(void)settingPageScanHeadPortraitFromPhotoGallery{
    [self UmengMobWithKeyStr:@"settingPageScanHeadPortraitFromPhotoGallery"];
}
/*
 86设置页－更换头像－取消
 **/
+(void)settingPageScanHeadPortraitCancle{
    [self UmengMobWithKeyStr:@"settingPageScanHeadPortraitCancle"];
}
/*
 87设置页－查看昵称
 **/
+(void)settingPageScanNickName{
    [self UmengMobWithKeyStr:@"settingPageScanNickName"];
}
/*
 88设置页－查看昵称－保存
 **/
+(void)settingPageScanNickNameAndSave{
    [self UmengMobWithKeyStr:@"settingPageScanNickNameAndSave"];
}
/*
 89设置页－查看个人签名
 **/
+(void)settingPageScanPersonalSignature{
    [self UmengMobWithKeyStr:@"settingPageScanPersonalSignature"];
}
/*
 90设置页－查看个人签名－保存
 **/
+(void)settingPageScanPersonalSignatureAndSave{
    [self UmengMobWithKeyStr:@"settingPageScanPersonalSignatureAndSave"];
}
/*
 91设置页－修改密码
 **/
+(void)settingPageChangePassword{
    [self UmengMobWithKeyStr:@"settingPageChangePassword"];
}
/*
 92设置页－修改密码－获取验证码
 **/
+(void)settingPageChangePasswordGetCode{
    [self UmengMobWithKeyStr:@"settingPageChangePasswordGetCode"];
}
/*
 93设置页－修改密码－保存
 **/
+(void)settingPageChangePasswordSave{
    [self UmengMobWithKeyStr:@"settingPageChangePasswordSave"];
}
/*
 94登录－手机号快速注册
 **/
+(void)LogInMobileNumberRegister{
    [self UmengMobWithKeyStr:@"LogInMobileNumberRegister"];
}
/*
 95登录－找回密码
 **/
+(void)LogInForgetPassword{
    [self UmengMobWithKeyStr:@"LogInForgetPassword"];
}
/*
 96登录－点击登录
 **/
+(void)LogInClick{
    [self UmengMobWithKeyStr:@"LogInClick"];
}
/*
 97注册－获取验证码
 **/
+(void)registerGetCode{
    [self UmengMobWithKeyStr:@"registerGetCode"];
}
/*
 98注册－点击注册并登录
 **/
+(void)registerAndLogIn{
    [self UmengMobWithKeyStr:@"registerAndLogIn"];
}
/*
 99注册－点击小咖玩用户注册协议
 **/
+(void)registerAggreementClick{
    [self UmengMobWithKeyStr:@"registerAggreementClick"];
}

@end
