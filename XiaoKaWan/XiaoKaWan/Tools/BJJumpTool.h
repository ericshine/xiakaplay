//
//  BJJumpTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/8/4.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BJJumpParameter.h"

@interface BJJumpTool : NSObject

+(void)JumpWithActionStr:(NSString *)actionStr navigationVC:(UINavigationController *)navigationVC;

@end
