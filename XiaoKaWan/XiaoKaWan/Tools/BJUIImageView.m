//
//  BJUIImageView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJUIImageView.h"
#import "UIImageView+WebCache.h"
static CGFloat const circleRadius = 20;
@interface BJUIImageView ()

@end
@implementation BJUIImageView
{
    CAShapeLayer *shapeLayer;
}

- (void)setAvatarImageWithUrlString:(NSString *)urlString
{
    [self setImageWithUrlString:urlString placeholderImage:[UIImage imageNamed:@"defaultHeadImage"] withProgress:NO];
}
- (void)setImageWithUrlString:(NSString *)urlString{
    [self setImageWithUrlString:urlString placeholderImage:nil withProgress:NO];
}
- (void)setImageWithUrlString:(NSString *)urlString placeholderImage:(UIImage*)image withProgress:(BOOL)progress
{
    NSURL *image_url = [NSURL URLWithString:urlString];
    if(progress) [self addPregresss];
    [self sd_setImageWithURL:image_url placeholderImage:image options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        CGFloat progress = (CGFloat)receivedSize/(CGFloat)expectedSize;
        if(progress) [self setProgress:progress];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(progress && cacheType ==SDImageCacheTypeNone)[self coverLayerWithAnimation];
    }];
}

- (void)addPregresss{
    shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.frame = self.bounds;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.strokeColor = [UIColor redColor].CGColor;
    shapeLayer.lineWidth = 2;
    [self.layer addSublayer:shapeLayer];
}
- (CGRect)circleFrame{
    CGRect circleFrame = CGRectMake(0, 0, circleRadius*2, circleRadius*2);
    circleFrame.origin.x = CGRectGetMidX(shapeLayer.frame) - CGRectGetMidX(circleFrame);
    circleFrame.origin.y = CGRectGetMidY(shapeLayer.frame) - CGRectGetMidY(circleFrame);
    return circleFrame;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
   
    shapeLayer.frame = self.bounds;
    shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:[self circleFrame]].CGPath;
}
- (void)setProgress:(CGFloat)progress{
    if(progress>=1){
        shapeLayer.strokeEnd = 1.0;
    }
    else if (progress<=0){
        shapeLayer.strokeEnd = 0.0;
    }else{
        shapeLayer.strokeEnd = progress;
    }
}
#pragma mark - cover layer
- (void)coverLayerWithAnimation{
    
    self.backgroundColor = [UIColor clearColor];
    [shapeLayer removeAnimationForKey:@"strokeEnd"];
    [shapeLayer removeFromSuperlayer];
    self.layer.mask = shapeLayer;
    CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat finalRadius = sqrt(center.x*center.x +center.y*center.y);
    CGFloat radiusInset = finalRadius-circleRadius;
    CGRect outerRect = CGRectInset([self circleFrame], -radiusInset, -radiusInset);
    CGPathRef toPath = [UIBezierPath bezierPathWithOvalInRect:outerRect].CGPath;
    CGPathRef fromPath = shapeLayer.path;
    CGFloat fromLineWidth = shapeLayer.lineWidth;
    [CATransaction begin];
    //    [CATransaction setValue:(id)kCFBooleanFalse
    //                     forKey:kCATransactionDisableActions];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    shapeLayer.lineWidth = 2 *finalRadius;
    shapeLayer.path = toPath;
    [CATransaction commit];
    
    CABasicAnimation *lineWidthAnimation = [CABasicAnimation animationWithKeyPath:@"lineWidth"];
    lineWidthAnimation.fromValue = [NSNumber numberWithFloat:fromLineWidth];
    lineWidthAnimation.toValue = [NSNumber numberWithFloat:2*finalRadius];
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    pathAnimation.fromValue = (__bridge id _Nullable)(fromPath);
    pathAnimation.toValue = (__bridge id _Nullable)(toPath);
    
    CAAnimationGroup *groupNaumations = [[CAAnimationGroup alloc] init];
    groupNaumations.duration = 3;
    groupNaumations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    groupNaumations.animations = @[lineWidthAnimation,pathAnimation];
    groupNaumations.delegate = self;
    [shapeLayer addAnimation:groupNaumations forKey:@"strokeWidth"];
    
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [shapeLayer removeFromSuperlayer];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
