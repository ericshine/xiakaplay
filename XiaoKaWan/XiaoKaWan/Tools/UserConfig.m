//
//  UserConfig.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "UserConfig.h"
#import "BJKeychain.h"
#import <UIKit/UIKit.h>
static NSString* const kuser_Token = @"kuser_Token";
static NSString* const kUserId = @"kUserId";
static NSString* const kAccount = @"kAccount";
@implementation UserConfig
SINGLETON_M(UserConfig)
- (NSString*)deviceId{
    //CFBundleIdentifier
    NSString *bundleIdentifier  = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    NSString *deviceIdStr = @"";
    deviceIdStr = [BJKeychain load:bundleIdentifier];
    if(deviceIdStr.length<=0){
        NSString *myuuidStr = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [BJKeychain save:bundleIdentifier data:myuuidStr];
        deviceIdStr = myuuidStr;
    }
    return deviceIdStr;
}
- (NSString *)apiVersion{
    return @"1.0";
}
- (NSString*)user_token{
  NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kuser_Token];
    return token;
}
- (void)setUser_token:(NSString *)user_token{
    [[NSUserDefaults standardUserDefaults] setObject:user_token forKey:kuser_Token];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)userId{
    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:kUserId];
    return uid;
}
- (void)setUserId:(NSString *)userId{
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:kUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (void)setAccount:(NSString *)account{
    [[NSUserDefaults standardUserDefaults] setObject:account forKey:kAccount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString*)account{
    NSString *account = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
    return account;
}
@end
