//
//  ImageSizeUrl.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  图片尺寸配置
 */
@interface ImageSizeUrl : NSObject
/**
 *  圆角头像 60px 120px 280px
 *
 *  @param urlString urlString description
 *
 *  @return return value description
 */
+ (NSString*)avatar60:(NSString*)urlString;
+ (NSString*)avatar120:(NSString*)urlString;
+ (NSString*)avatar280:(NSString*)urlString;
/**
 *  不裁剪 正方形 60*60 90*90 ......
 *
 *  @param urlString urlString description
 *
 *  @return return value description
 */
+ (NSString*)image60_url:(NSString*)urlString;
+ (NSString*)image90_url:(NSString*)urlString;
//+ (NSString*)image176_url:(NSString*)urlString;
//+ (NSString*)image272_url:(NSString*)urlString;
+ (NSString*)image750_url:(NSString*)urlString;
/**
 *  裁剪过的图
 *
 *  @param urlString urlString description
 *
 *  @return return value description
 */
+ (NSString*)image568x320_url:(NSString*)urlString;
+ (NSString*)image852x480_url:(NSString*)urlString;
+ (NSString*)image1704x960_url:(NSString*)urlString;
/**
 *  定宽 裁剪 w1:320  w2:480 w3:960
 *
 *  @param urlString urlString description
 *
 *  @return return value description
 */
+ (NSString*)imagef320_url:(NSString*)urlString;
+ (NSString*)imagef480_url:(NSString*)urlString;
+ (NSString*)imagef960_url:(NSString*)urlString;
/**
 *  定高 裁剪 300
 *
 *  @param urlString urlString description
 *
 *  @return return value description
 */
+ (NSString*)imageFH300_url:(NSString*)urlString;
+ (NSString*)imageW4_url:(NSString*)urlString;
@end
