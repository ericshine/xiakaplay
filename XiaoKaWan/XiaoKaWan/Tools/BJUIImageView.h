//
//  BJUIImageView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageSizeUrl.h"
@interface BJUIImageView : UIImageView
//@property(nonatomic,copy) NSString *imageUrl;
- (void)setAvatarImageWithUrlString:(NSString *)urlString;
/**
 *  Description
 *
 *  @param urlString image url
 *  @param progress  progress default is show  progress
 */
- (void)setImageWithUrlString:(NSString *)urlString;
- (void)setImageWithUrlString:(NSString *)urlString placeholderImage:(UIImage*)image withProgress:(BOOL)progress;
@end

