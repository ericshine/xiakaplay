//
//  BJPlaceholderTextView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJPlaceholderTextView : UITextView
@property(nonatomic,copy) NSString *placeHolder;

@end
