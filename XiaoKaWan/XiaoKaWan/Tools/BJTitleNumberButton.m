//
//  BJTitleNumberButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/31/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJTitleNumberButton.h"
#import "UILabel+initLabel.h"
#import <Masonry.h>
@implementation BJTitleNumberButton
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
//        make.bottom.equalTo(self.titleLab.mas_top);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.numberLab.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (UILabel*)titleLab
{
    if(_titleLab==nil){
        _titleLab = [UILabel creatLabelWithFont:10 color:0x555555];
        _titleLab.text = @"地点";
        _titleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLab];
    }
    return _titleLab;
}
- (UILabel*)numberLab
{
    if(_numberLab==nil){
        _numberLab = [UILabel creatLabelWithFont:24 color:0x555555];
        _numberLab.text = @"0";
        _numberLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_numberLab];
    }
    return _numberLab;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
