//
//  BJTitleNumberButton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/31/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJTitleNumberButton : UIButton
@property(nonatomic,strong)UILabel *numberLab;
@property(nonatomic,strong)UILabel *titleLab;
@end
