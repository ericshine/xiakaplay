//
//  BJTextUnderImgBtn.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJTextUnderImgBtn.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"

@implementation BJTextUnderImgBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.titleLabel.textAlignment=NSTextAlignmentCenter;
        self.imageView.contentMode=UIViewContentModeCenter;
        self.titleLabel.font=[UIFont systemFontOfSize:12];
        [self setTitleColor:GrayColor(51) forState:UIControlStateNormal];
        
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    

    
    self.imageView.frame=CGRectMake(0, 0, 45.5, 45.5);
    
    self.titleLabel.frame=CGRectMake(0, 45.5, self.bounds.size.width, 20);
}


@end
