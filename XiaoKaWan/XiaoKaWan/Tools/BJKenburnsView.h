//
//  BJKenburnsImageView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BJKenburnsImageViewState) {
    BJKenburnsImageViewStateAnimating,
    BJKenburnsImageViewStatePausing
};
typedef NS_ENUM(NSInteger, BJKenburnsImageViewZoomCourse) {
    BJKenburnsImageViewZoomCourseRandom                = 0,
    BJKenburnsImageViewZoomCourseUpperLeftToLowerRight = 1,
    BJKenburnsImageViewZoomCourseUpperRightToLowerLeft = 2,
    BJKenburnsImageViewZoomCourseLowerLeftToUpperRight = 3,
    BJKenburnsImageViewZoomCourseLowerRightToUpperLeft = 4
};
typedef NS_ENUM(NSInteger, BJKenburnsImageViewZoomPoint) {
    BJKenburnsImageViewZoomPointLowerLeft  = 0,
    BJKenburnsImageViewZoomPointLowerRight = 1,
    BJKenburnsImageViewZoomPointUpperLeft  = 2,
    BJKenburnsImageViewZoomPointUpperRight = 3
};

@interface BJKenburnsImageView : UIImageView

@end

@interface BJKenburnsView : UIView
@property (nonatomic, strong) BJKenburnsImageView * imageView;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, assign) CGFloat animationDuration;  //default is 13.f
@property (nonatomic, assign) CGFloat zoomRatio; // default 0.1  0 ~ 1 not working
@property (nonatomic, assign) CGFloat endZoomRate; // default 1.2
@property (nonatomic, assign) CGFloat startZoomRate; // default 1.3
@property (nonatomic, assign) UIEdgeInsets padding; // default UIEdgeInsetsMake(10, 10, 10, 10);
@property (nonatomic, assign) BJKenburnsImageViewZoomCourse course; // default is 0

@property (nonatomic, assign) BJKenburnsImageViewState state;

- (void)restartMotion;
- (void)showWholeImage;
- (void)zoomAndRestartAnimation;
- (void)zoomAndRestartAnimationWithCompletion:(void(^)(BOOL finished))completion;
@end
