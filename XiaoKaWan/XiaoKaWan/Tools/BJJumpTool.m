//
//  BJJumpTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/8/4.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJJumpTool.h"
#import "MapDetailListViewController.h"
#import "MapObject.h"
#import "PlacePostDetailViewController.h"
#import "PostParatemer.h"
#import "MeHomeViewController.h"
#import "User.h"
#import "BJNotificationConfig.h"
#import "MapsPlaceDetailViewController.h"
#import "Place.h"


@implementation BJJumpTool

+(void)JumpWithActionStr:(NSString *)actionStr navigationVC:(UINavigationController *)navigationVC;
{
    
    if (actionStr==nil||navigationVC==nil) return;
    
    NSRange addressRemindRange=[actionStr rangeOfString:@"address="];
    
    NSRange symbolRange=[actionStr rangeOfString:@"&"];
    
    NSRange idRemindRange=[actionStr rangeOfString:@"id="];
    
    NSString *address=[actionStr substringWithRange:NSMakeRange(addressRemindRange.location+addressRemindRange.length, symbolRange.location-(addressRemindRange.location+addressRemindRange.length))];
    
    NSString *id=[actionStr substringFromIndex:idRemindRange.location+idRemindRange.length];
    
    
    if (address.length&&id.length) {
        
        BJJumpParameter *parameter=[[BJJumpParameter alloc]init];
        parameter.id=id;
        parameter.address=address;
        
        [self jumpWithNavigationVC:navigationVC parameter:parameter];
        
    }
    
}

+(void)jumpWithNavigationVC:(UINavigationController *)navigationVC parameter:(BJJumpParameter *)parameter
{
    
    if ([parameter.address isEqualToString:@"maps_vc"]) {
        MapDetailListViewController *VC=[[MapDetailListViewController alloc]init];
        MapObject *object=[[MapObject alloc]init];
        object.id=parameter.id;
        VC.mapObject=object;
        VC.hidesBottomBarWhenPushed=YES;
        [navigationVC pushViewController:VC animated:YES];
        
        
    }else if ([parameter.address isEqualToString:@"post_vc"])
    {
        PlacePostDetailViewController *VC=[[PlacePostDetailViewController alloc]init];
        PostParatemer *PostParameter=[[PostParatemer alloc]init];
        PostParameter.post_id=parameter.id;
        VC.parameter=PostParameter;
        VC.hidesBottomBarWhenPushed=YES;
        [navigationVC pushViewController:VC animated:YES];
        
    }else if ([parameter.address isEqualToString:@"place_vc"])
    {
        MapsPlaceDetailViewController *VC=[[MapsPlaceDetailViewController alloc]init];
        Place *place=[[Place alloc]init];
        place.id=[parameter.id integerValue];
        VC.place=place;
        VC.hidesBottomBarWhenPushed=YES;
        [navigationVC pushViewController:VC  animated:YES];
        
    }else if ([parameter.address isEqualToString:@"userInfor_vc"])
    {
        MeHomeViewController *VC=[[MeHomeViewController alloc]init];
        User *user=[[User alloc]init];
        user.uid=[parameter.id integerValue];
        VC.userInforType=USERINFOR_OTHER;
        VC.currentUser=user;
        VC.hidesBottomBarWhenPushed=YES;
        [navigationVC pushViewController:VC animated:YES];
        
    }else if ([parameter.address isEqualToString:@"notice_vc"])
    {
        
    }
    
    
}


@end
