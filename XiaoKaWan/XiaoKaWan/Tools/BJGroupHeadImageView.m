//
//  BJGroupHeadImageView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJGroupHeadImageView.h"
#import <UIImageView+WebCache.h>
#import "UIImage+Category.h"
#import <Masonry.h>
#import "UILabel+initLabel.h"
#import "ImageSizeUrl.h"
//#import "HeadViewCollectionViewCell.h"
static NSString* const cellIdentify = @"headViewcellIdentify";
static NSInteger const headImageWidth = 30;
@interface BJGroupHeadImageView()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)UICollectionView *collectionView;
@end
@implementation BJGroupHeadImageView
{
//    NSArray *imageUrls;
    NSArray *placeholderImage;
}
- (instancetype)initWithFrame:(CGRect)frame andGroups:(NSArray *)urls andPlaceholderImages:(NSArray *)images
{
    self=[super initWithFrame:frame];
    if(self){
//        if(imageUrls == nil)imageUrls =[NSArray arrayWithArray:urls];
        if(placeholderImage ==nil)placeholderImage = [NSArray arrayWithArray:images];
//        [self getImageFromeGroup];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame andGroups:(NSArray *)urls
{
    self=[super initWithFrame:frame];
    if(self){
//        if(imageUrls == nil)imageUrls =[NSArray arrayWithArray:urls];
//        [self getImageFromeGroup];
    }
    return self;
}
- (void)updateConstraints{
    [super updateConstraints];
    NSInteger imageCount =_imageUrlArray.count;
    if(imageCount>4) imageCount= 4;
    CGFloat with = imageCount*30+8*(imageCount-1);
    self.collectionView.frame = CGRectMake(self.frame.size.width-with, 0, with, 30);
//    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top);
//        make.bottom.equalTo(self.mas_bottom);
//        make.right.equalTo(self.mas_right);
//        make.width.equalTo(@(with));
//    }];
}
- (UICollectionView *)collectionView{
    if(_collectionView == nil){
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(headImageWidth, headImageWidth);
        layout.minimumInteritemSpacing = 5;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.frame collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_collectionView];
        [_collectionView registerClass:[HeadViewCollectionViewCell class] forCellWithReuseIdentifier:cellIdentify];
    }
    return _collectionView;
}
- (void)setImageUrlArray:(NSArray *)imageUrlArray
{
    _imageUrlArray = imageUrlArray;
//    if(imageUrls == nil)imageUrls =[NSArray arrayWithArray:imageUrlArray];
//    [self getImageFromeGroup];
    [self setNeedsUpdateConstraints];
    [self.collectionView reloadData];
    
}
#pragma mark - delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    return  MIN(self.countLimit, _imageUrlArray.count);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *urlStr = _imageUrlArray[indexPath.row];
    HeadViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentify forIndexPath:indexPath];
    if(self.headViewType==HEADVIEWTYPE_SHOWNUMBER && indexPath.row>=self.countLimit-1){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *placeHolderImage = [UIImage imageFromColor:[UIColor p_colorWithHex:0xa6a6a6]];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = placeHolderImage;
                cell.imageView.layer.cornerRadius = 15;
                cell.imageView.layer.masksToBounds = YES;
            });
        });
        if(self.totalNumber>99) {
            cell.numberLabel.text = @"99+";
            cell.numberLabel.font = [UIFont systemFontOfSize:12];
        }
        else cell.numberLabel.text = [NSString stringWithFormat:@"%li",(long)self.totalNumber];
    }else{
    [cell removeNumber];
   [cell.imageView setAvatarImageWithUrlString:[ImageSizeUrl avatar60:urlStr]];
    }
    cell.headViewType = self.headViewType;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

@implementation HeadViewCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self imageView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right);
    }];
    if(self.headViewType == HEADVIEWTYPE_SHOWNUMBER){
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right);
    }];
    }
}
- (BJUIImageView *)imageView{
    if(_imageView == nil){
        _imageView = [[BJUIImageView alloc] init];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}
- (UILabel*)numberLabel
{
    if(_numberLabel==nil){
        _numberLabel = [UILabel creatLabelWithFont:18 color:0xffffff];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_numberLabel];
    }
//    _numberLabel.hidden = NO;
    return _numberLabel;
}
- (void)removeNumber{
    if(_numberLabel) _numberLabel.text = @"";
}
@end