//
//  BJRefreshGitHeader.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJRefreshGitHeader.h"

@implementation BJRefreshGitHeader
+(instancetype)selfHeaderWithRefreshingBlock:(BJRefreshComponentRefreshingBlock)refreshingBlock
{
    BJRefreshGitHeader * header= [BJRefreshGitHeader headerWithRefreshingBlock:refreshingBlock];
    [header setTitle:@"start" forState:MJRefreshStatePulling];
    [header setTitle:@"loading" forState:MJRefreshStateRefreshing];
    [header setTitle:@" " forState:MJRefreshStateIdle];
    [header setTitle:@"" forState:MJRefreshStateNoMoreData];
    UIImage *imgR1 = [UIImage imageNamed:@""];
    UIImage *imgR2 = [UIImage imageNamed:@""];
    
    NSArray *reFreshone = [NSArray arrayWithObjects:imgR1, nil];
    NSArray *reFreshtwo = [NSArray arrayWithObjects:imgR2, nil];
    NSArray *reFreshthree = [NSArray arrayWithObjects:imgR1,imgR2, nil];
    
    [header setImages:reFreshone forState:MJRefreshStatePulling];
    [header setImages:reFreshthree forState:MJRefreshStateRefreshing];
    [header setImages:reFreshtwo forState:MJRefreshStateIdle];
    
//    header.stateLabel.text = @"";
//    header.lastUpdatedTimeLabel.text = @"";
    header.lastUpdatedTimeLabel.hidden  = YES;
    return header;
}

- (void)beginRefreshing
{
    [super beginRefreshing];
}
- (void)endRefreshing
{
    [super endRefreshing];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
