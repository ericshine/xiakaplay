//
//  BJDynamicDBTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/28.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BJDBToolParameter.h"


@interface BJDynamicDBTool : NSObject

+(void)dynamicListWithParameter:(BJDBToolParameter *)parameter success:(void (^)(NSArray *dynamicModelArray))success;

+(void)clearDB;

@end
