//
//  BJNotificationDBTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/28.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJDBToolParameter.h"
#import "BJNotificationModel.h"


@interface BJNotificationDBTool : NSObject

+(void)notificationListWithParam:(BJDBToolParameter *)parameter success:(void(^)(NSArray *notificationModelArray) )success;

+(void)newNotificationListWithParam:(BJDBToolParameter *)parameter success:(void(^)(NSArray *notificationModelArray) )success;

//+(void)haveScanNotificactions:(NSArray *)notificationArray;

+(void)haveScanNotificactions;

+(void)haveScanNotificactionWithNotificationModel:(BJNotificationModel *)notificationModel;

+(void)updateNotificationArray:(NSArray *)notificationModelArray;

+(void)clearDB;

@end
