//
//  BJDBToolParameter.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/28.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJDBToolParameter : NSObject

@property (nonatomic,assign) NSInteger count;

@property (nonatomic,assign) NSInteger SinceID;

@property (nonatomic,assign) NSInteger MaxID;

@property (nonatomic,assign)BOOL isMoreType;

@end
