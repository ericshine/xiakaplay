//
//  BJDynamicDBTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/28.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJDynamicDBTool.h"
#import <FMDB.h>
#import "BJDBToolParameter.h"
#import "BJNotificationModel.h"
#import "ClientNetwork.h"
#import "BJpch.h"

@implementation BJDynamicDBTool

static FMDatabase *_db;

+ (void)initialize
{

    NSString *doc=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *DBName=[NSString stringWithFormat:@"%@_notiifcation.sqlite",[UserConfig sharedInstance].userId];
    NSString *filePath=[doc stringByAppendingPathComponent:DBName];
    
    _db =[FMDatabase databaseWithPath:filePath];
    
    if ([_db open]) {
        
        BOOL createResult=[_db executeUpdate:@"create table if not exists dynamicListTable (id integer primary key autoincrement,dynamicModelDic blob,notificationID integer);"];
        
        if (createResult) {
            NSLog(@"创建动态列表数据库成功");
        }else{
            NSLog(@"创建动态列表数据库失败");
            
        }
    }

}

+(void)dynamicListWithParameter:(BJDBToolParameter *)parameter success:(void (^)(NSArray *))success
{
    if (parameter.isMoreType==0) {
       // 下拉刷新
        
        //先拉去网络数据
        if (![UserConfig sharedInstance].user_token.length)
        {
            if (success) {
                success(nil);
            }
            
            return;
        }

        NSDictionary *RequestParameter=@{@"notice_type":@"post",@"_t":[UserConfig sharedInstance].user_token};
        
        @weakify(self)
        [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] noticeListApi] parameter:RequestParameter complete:^(BOOL succeed, id obj) {
            @strongify(self)
            
            NSLog(@"%@===%d",obj,succeed);
            
            if (succeed) {
                //成功获取到最新的数据
                [self saveDynamicList:obj];
                
            }
            if (success) {
                success([self readDyanmicListWithParameter:parameter]);
            }
            
        }];
        
        
    }else
    {
        // 上拉刷新
       NSArray *MoreDynamicModelArray= [self readDyanmicListWithParameter:parameter];
        if (MoreDynamicModelArray.count>0) {
            //数据库里读出了数据
            if (success) {
                success(MoreDynamicModelArray);
            }
            
        }else
        {
            //数据库里没有读出数据
            if (success) {
                success(nil);
            }
            
        }
    }
}

+(NSArray *)readDyanmicListWithParameter:(BJDBToolParameter *)parameter
{
    NSMutableArray *dynamicArray=[NSMutableArray array];
    
    FMResultSet *resultSet=nil;
    
    if (parameter.SinceID)   {
        resultSet=[_db executeQueryWithFormat:@"select * from dynamicListTable where notificationID>%ld order by notificationID desc limit %ld;",parameter.SinceID,parameter.count];
    }else if (parameter.MaxID)
    {
        resultSet =[_db executeQueryWithFormat:@"select * from dynamicListTable where notificationID<%ld order by notificationID desc limit %ld;",parameter.MaxID,parameter.count];
    }else
    {
        resultSet =[_db executeQueryWithFormat:@"select * from dynamicListTable order by notificationID desc limit %ld",parameter.count];
    }
    
    while (resultSet.next) {
        NSData *dynamicDicData=[resultSet objectForColumnName:@"dynamicModelDic"];
        NSDictionary * dynamicDic=[NSKeyedUnarchiver unarchiveObjectWithData:dynamicDicData];
        BJNotificationModel *dynamicModel=[BJNotificationModel objectWithKeyValues:dynamicDic];
        [self kindOfCalculationWith:dynamicModel];
        [dynamicArray addObject:dynamicModel];
    }
    
    return dynamicArray;
}

+(void)saveDynamicList:(NSArray *)dynamicListDicArray
{
    
    for (NSDictionary *dynamicDic in dynamicListDicArray) {
        NSData *dynamicDicData=[NSKeyedArchiver archivedDataWithRootObject:dynamicDic];
        
        //去重
        FMResultSet *resultSet=[_db executeQueryWithFormat:@"select * from notificationListTable where notificationID = %@",[dynamicDic objectForKey:@"notice_id"]];
        if (resultSet.next) continue;
        
        [_db executeUpdateWithFormat:@"insert into dynamicListTable (dynamicModelDic,notificationID) values(%@,%@);",dynamicDicData,[dynamicDic objectForKey:@"notice_id"]];
        
    }
    
}

+(void)clearDB
{
    if ([_db open]) {
        
        [_db executeUpdateWithFormat:@"delete from dynamicListTable"];
    }
}

+(void)kindOfCalculationWith:(BJNotificationModel *)dynamicModel
{
    CGFloat margin=7;
    CGFloat imageWH=((ScreenW-74)-3*margin)/4;
    
    if (dynamicModel.extend.post_images.count) {
        //有图片
        if (dynamicModel.notice_content.length) {
            //有文字
            dynamicModel.DynamicCellHieght=imageWH+99.5+19.5;
        }else
        {
            //无文字
            dynamicModel.DynamicCellHieght=imageWH+67.5+19.5;
        }
        
        
    }else
    {
        //没图片
        if (dynamicModel.sub_title.length) {
            //有副标题
            dynamicModel.DynamicCellHieght=128;
            
        }else
        {
            //没有副标题
            dynamicModel.DynamicCellHieght=93.5;
        }
        
    }
    
}


@end
