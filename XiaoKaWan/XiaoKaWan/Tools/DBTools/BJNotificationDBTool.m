//
//  BJNotificationDBTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/28.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationDBTool.h"
#import <FMDB.h>
#import "BJDBToolParameter.h"
#import "ClientNetwork.h"
#import "BJpch.h"

@implementation BJNotificationDBTool

static FMDatabase *_db;

+ (void)initialize
{
    
    NSString *doc=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
     NSString *DBName=[NSString stringWithFormat:@"%@_notiifcation.sqlite",[UserConfig sharedInstance].userId];
    NSString *filePath=[doc stringByAppendingPathComponent:DBName];
    
    _db=[FMDatabase databaseWithPath:filePath];
    
    if ([_db open]) {
        
      BOOL createResult=  [_db executeUpdate:@"create table if not exists notificationListTable (id integer primary key autoincrement,notificationModelDic blob,notificationID integer,haveSeen integer);"];
        
        if (createResult) {
            
            NSLog(@"创建通知列表数据库成功");
            
        }else{
            NSLog(@"创建通知列表数据库失败");
            
        }
    }
    
}

+(void)newNotificationListWithParam:(BJDBToolParameter *)parameter success:(void (^)(NSArray *))success
{
    if (parameter.count==0) parameter.count=10;
    
    if (![UserConfig sharedInstance].user_token.length)
    {
        if (success) {
            success(nil);
        }
        
        return;
    }
    
    NSDictionary *requestParameter=@{@"_t":[UserConfig sharedInstance].user_token};
    
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] noticeListApi] parameter:requestParameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        
        NSLog(@"%@===%d",obj,succeed);
        
        if (succeed) {
            NSArray * notificatioArray=[BJNotificationModel objectArrayWithKeyValuesArray:obj];
            
            [self SaveNotificationListWith:[BJNotificationModel keyValuesArrayWithObjectArray:notificatioArray] ];
        }
        NSArray * notificationModelArray= [self readNotificationListWithParam:parameter];
        if (success) {
            success(notificationModelArray);
        }
        
        
    }];
}


+(void)notificationListWithParam:(BJDBToolParameter *)parameter success:(void(^)(NSArray *))success
{
    if (parameter.count==0) parameter.count=10;
    
    if (![UserConfig sharedInstance].user_token.length)
    {
        if (success) {
            success(nil);
        }
        
        return;
    }

    
    NSArray *notificationModelArray=[self readNotificationListWithParam:parameter];
    
    if (notificationModelArray.count>0) {
        //数据库里有数据
        if (success) {
            success(notificationModelArray);
        }
    }else{
        //数据库里无数据
        if (parameter.isMoreType==1) {
            if (success) {
                success(nil);
            }
            return;
        }
        
        NSDictionary *requestParameter=@{@"_t":[UserConfig sharedInstance].user_token};
        
        @weakify(self)
        [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] noticeListApi] parameter:requestParameter complete:^(BOOL succeed, id obj) {
            @strongify(self)
            
            NSLog(@"%@===%d",obj,succeed);
            
            if (succeed) {
               NSArray * notificatioArray=[BJNotificationModel objectArrayWithKeyValuesArray:obj];
                
                [self SaveNotificationListWith:[BJNotificationModel keyValuesArrayWithObjectArray:notificatioArray] ];
                if (success) {
                    success(notificatioArray);
                }
                
                
                
            }
            
            
        }];
        
    }
    
}




//读取数据库里的数据
+(NSArray *)readNotificationListWithParam:(BJDBToolParameter *)parameter
{
    NSMutableArray * notificationArray=[NSMutableArray array];
    
    FMResultSet *resultSet=nil;
    
    if (parameter.SinceID) {
        resultSet =[_db executeQueryWithFormat:@"select * from notificationListTable where notificationID > %ld order by  notificationID desc limit %ld",parameter.SinceID,parameter.count];
    }else if (parameter.MaxID)
    {
        resultSet =[_db executeQueryWithFormat:@"select * from notificationListTable where notificationID < %ld order by  notificationID desc limit %ld",parameter.MaxID,parameter.count];
    }else
    {
        resultSet =[_db executeQueryWithFormat:@"select * from notificationListTable order by  notificationID desc limit %ld",parameter.count];
    }
    
    while (resultSet.next) {
        NSData *notificationModelData=[resultSet objectForColumnName:@"notificationModelDic"];
        int haveSeen=[resultSet intForColumn:@"haveSeen"];
        NSDictionary *notificationModelDic=[NSKeyedUnarchiver unarchiveObjectWithData:notificationModelData];
        BJNotificationModel *notificationModel=[BJNotificationModel objectWithKeyValues:notificationModelDic];
        notificationModel.NewViewHide=haveSeen;
        [notificationArray addObject:notificationModel];
    }
    
    
    return notificationArray;
}

//保存到数据库里数据
+(void)SaveNotificationListWith:(NSArray *)notificationListDicArray
{
    for (NSDictionary *notificationDic in notificationListDicArray) {
        //去重
        FMResultSet *resultSet=[_db executeQueryWithFormat:@"select * from notificationListTable where notificationID = %@",[notificationDic objectForKey:@"notice_id"]];
        if (resultSet.next) continue;
        
        NSData *notificationListDicData =[NSKeyedArchiver archivedDataWithRootObject:notificationDic];
        [_db executeUpdateWithFormat:@"insert into notificationListTable (notificationModelDic,notificationID,haveSeen) values (%@,%@,%d)",notificationListDicData,[notificationDic objectForKey:@"notice_id"],0];
        
    }
}



+(void)haveScanNotificactions
{
//    [_db open];
    if ([_db open]) {
         [_db executeUpdateWithFormat:@"update notificationListTable set haveSeen = 1"];
    }
    
}

+(void)haveScanNotificactionWithNotificationModel:(BJNotificationModel *)notificationModel
{
    if ([_db open]) {
        [_db executeUpdateWithFormat:@"update notificationListTable set haveSeen = 1 where notificationID =%ld",notificationModel.notice_id];
    }
}

+(void)updateNotificationArray:(NSArray *)notificationModelArray
{
    if ([_db open]) {
        
        for (BJNotificationModel *notificationModel in notificationModelArray) {
            NSData *notificationModelData=[NSKeyedArchiver archivedDataWithRootObject:[notificationModel keyValues] ];
            
            [_db executeUpdateWithFormat:@"update notificationListTable set notificationModelDic = %@ where notificationID =%ld",notificationModelData,notificationModel.notice_id];
            
        }
        
    }
    
}

+(void)clearDB
{
    if ([_db open]) {
        [_db executeUpdateWithFormat:@"delete from notificationListTable"];
    }
    
}








@end
