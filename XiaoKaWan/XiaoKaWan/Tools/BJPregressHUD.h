//
//  BJPregressHUD.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJPregressHUD : NSObject
+ (void)showWithStatus:(NSString*)status;
+(void)showSuccessWithStatus:(NSString *)status;
+(void)showErrorWithStatus:(NSString *)status;
+ (void)dissMiss;
@end
