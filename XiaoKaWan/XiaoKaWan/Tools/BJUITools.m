//
//  BJUITools.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/16.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJUITools.h"

@implementation BJUITools

+(void)tableviewSepratorLineLeftInsetWith:(UITableViewCell *)cell Suojin:(CGFloat)LeftSuojin
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, LeftSuojin, 0, 0)];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

@end
