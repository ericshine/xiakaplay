//
//  BJNotificationTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/8/2.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJNotificationTool.h"
#import "BJTabBarViewController.h"
#import "BJNavigationController.h"
#import "BJNotificationConfig.h"
#import "BJNotificationDBTool.h"
#import "BJDBToolParameter.h"

@implementation BJNotificationTool

+(void)DealWithNotification:(NSDictionary *)NotificationDic
{
    
    
//    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"fdsf" message:[NSString stringWithFormat:@"==%@==",NotificationDic] delegate:nil cancelButtonTitle:@"" otherButtonTitles:@"", nil];
//    [alertView show];
    if ([NotificationDic objectForKey:@"Message"]) {
        //是透传消息，根据信号，拉去对应的通知网络数据
        UIWindow *window=[[[UIApplication sharedApplication] delegate ] window];
        BJTabBarViewController *tabVC=((BJTabBarViewController*)window.rootViewController);
        tabVC.childViewControllers[1].tabBarItem.badgeValue=@"";
        
//        [[NSUserDefaults standardUserDefaults] setBool:1 forKey:@"haveNewNotification"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"haveNewNotification" object:nil];
        
        
    }else if ([NotificationDic objectForKey:@"Notification"])
    {
        //是通知消息，打开某个应用内功能页，或者打开某个网页
        if ([UIApplication sharedApplication].applicationState==0) return;
        
        NSString * ValueStr=[NotificationDic objectForKey:@"Notification"];
        NSRange headRange= [ValueStr rangeOfString:@"CallFunc="];
        NSString *temStr=[ValueStr substringFromIndex:headRange.length];
        NSRange andSymbolRange= [temStr rangeOfString:@"&"];
        NSString *methodStr=[temStr substringToIndex:andSymbolRange.location];
        NSRange actionSymbolRange=[ValueStr rangeOfString:@"Action=\""];
        NSString *actionStr=[ValueStr substringFromIndex:(actionSymbolRange.location+actionSymbolRange.length)];
        actionStr=[actionStr substringWithRange:NSMakeRange(0, actionStr.length-1)];
        
        NSLog(@"%@",methodStr);
        
        if ([methodStr isEqualToString:@"openVC"]) {
            //打开某个具体程序页面
            NSLog(@"%@",actionStr);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                UIWindow *window=[[[UIApplication sharedApplication] delegate ] window];
                BJTabBarViewController *tabVC=((BJTabBarViewController*)window.rootViewController);
                [tabVC setSelectedIndex:1];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"BJUploadNotification" object:nil];
                });
                
            });
            
            
        }else if ([methodStr isEqualToString:@"openWeb"])
        {
            //打开某个网页
            NSLog(@"%@",actionStr);
            UIViewController *VC=[[UIViewController alloc]init];
            UIWebView *webView=[[UIWebView alloc]initWithFrame:VC.view.bounds];
            [VC.view addSubview:webView];
            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:actionStr]]];
            
            
            UIWindow *window=[[[UIApplication sharedApplication] delegate ] window];
            BJTabBarViewController *tabVC=((BJTabBarViewController*)window.rootViewController);
            [tabVC setSelectedIndex:0];
            NSLog(@"%@",tabVC.viewControllers[0]);
            [((BJNavigationController *)tabVC.viewControllers[0]) pushViewController:VC animated:YES];
            

            
        }
        
    }
    
    
    
}

@end
