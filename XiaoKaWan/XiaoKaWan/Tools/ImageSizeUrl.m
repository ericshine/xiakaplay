//
//  ImageSizeUrl.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ImageSizeUrl.h"

@implementation ImageSizeUrl
+ (NSString*)avatar60:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!avatar1"];
}
+ (NSString*)avatar120:(NSString*)urlString{
    if(!urlString.length) return @"";
 return [NSString stringWithFormat:@"%@%@",urlString,@"@!avatar2"];
}
+ (NSString*)avatar280:(NSString*)urlString{
    if(!urlString.length) return @"";
 return [NSString stringWithFormat:@"%@%@",urlString,@"@!avatar3"];
}
+ (NSString*)image60_url:(NSString *)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!h1"];
}
+ (NSString*)image90_url:(NSString *)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!h2"];
}
+ (NSString*)image750_url:(NSString *)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!h5"];
}
//+ (NSString*)image272_url:(NSString *)urlString{
//    return [NSString stringWithFormat:@"%@%@",urlString,@"@!h1"];
//}
+ (NSString*)image568x320_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!c1"];
}
+ (NSString*)image852x480_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!c2"];
}
+ (NSString*)image1704x960_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!c3"];
}
+ (NSString*)imagef320_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!f1"];
}
+ (NSString*)imagef480_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!f2"];
}
+ (NSString*)imagef960_url:(NSString*)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!f3"];
}
+ (NSString*)imageFH300_url:(NSString *)urlString{
    if(!urlString.length) return @"";
    return [NSString stringWithFormat:@"%@%@",urlString,@"@!fh2"];
}
+ (NSString*)imageW4_url:(NSString*)urlString{
    if(!urlString.length) return @"";
     return [NSString stringWithFormat:@"%@%@",urlString,@"@!w4"];
}
@end
