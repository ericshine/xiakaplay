//
//  BJRefreshGitHeader.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>
typedef void (^BJRefreshComponentRefreshingBlock)();
@interface BJRefreshGitHeader : MJRefreshGifHeader
+ (instancetype)selfHeaderWithRefreshingBlock:(BJRefreshComponentRefreshingBlock)refreshingBlock;

@end
