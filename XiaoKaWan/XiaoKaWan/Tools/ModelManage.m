//
//  ModelManage.m
//  Share
//
//  Created by apple_Eric on 5/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "ModelManage.h"
#import "UserConfig.h"
@implementation ModelManage
SINGLETON_M(ModelManage)
//- (id)init
//{
//    self =[super init];
//    if(self){
//    
//    }
//    return self;
//}
- (NSManagedObjectModel *)model
{
    if(_model ==nil){
        NSURL *libBundleUrl = [[NSBundle mainBundle] URLForResource:self.modeldStr withExtension:@"momd"];
            _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:libBundleUrl];
    }
    return _model;
}
-(NSString *)modeldStr
{
    return @"";
}
-(void)saveContext
{
    NSError *error;
    if(![self.context save:&error]){
        NSLog(@"contextSaveError:%@",[error localizedDescription]);
        abort();
    }
}
- (void)clearContext
{
    [self.context reset];
    self.context = nil;
    self.coordinator = nil;
    self.model = nil;
    self.storeUrl = nil;
}
- (void)deleteContext{
    [self clearContext];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtURL:self.storeUrl error:&error];
    NSLog(@"delete coreData error:%@",[error localizedDescription]);
}
- (NSManagedObjectContext*)context
{
    if(_context == nil){
        _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_context setPersistentStoreCoordinator:self.coordinator];
    }
    return _context;
}

- (NSPersistentStoreCoordinator *)coordinator
{
    if(_coordinator == nil){
        _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
            NSDictionary *option = @{NSMigratePersistentStoresAutomaticallyOption:[NSNumber numberWithBool:YES],NSInferMappingModelAutomaticallyOption:[NSNumber numberWithBool:YES],NSPersistentStoreFileProtectionKey:NSFileProtectionNone};
        
        NSError *error = nil;
        if(![_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration: nil URL:self.storeUrl options:option error:&error]){
            NSLog(@"%@,error:%@",error,error.userInfo);
            // Move Incompatible Store
            NSFileManager *fm = [NSFileManager defaultManager];
            [fm removeItemAtURL:_storeUrl error:nil];
            _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
            
            NSError *error = nil;
            if(![_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration: nil URL:self.storeUrl options:option error:&error]){
            }
        };
        NSDictionary *fileAttributes = [NSDictionary dictionaryWithObject:NSFileProtectionNone forKey:NSFileProtectionKey];
        if (![[NSFileManager defaultManager] setAttributes:fileAttributes ofItemAtPath:[_storeUrl path] error:&error]) {
            // Deal with the error
            NSLog( @"encrypt database (when the device is locked or booting) fail!");
        }
    }
    return _coordinator;
}
- (NSURL *)storeUrl
{
    if(_storeUrl == nil){
        NSString *token = [UserConfig sharedInstance].user_token;
        
        NSString *filePath;
        if(token.length && (token != nil)){
            filePath = [self storePathWith:[NSString stringWithFormat:@"%@_%@.db",[UserConfig sharedInstance].userId,self.modeldStr]];
        }else{
            filePath = [self storePathWith:[NSString stringWithFormat:@"%@.db",self.modeldStr]];
        }
        _storeUrl = [NSURL fileURLWithPath:filePath isDirectory:YES];
    }
    NSLog(@"path:%@",_storeUrl);
    return _storeUrl;
}

- (NSString *)storePathWith:(NSString *)pathId
{
    NSString *storePath = [[[self applicationDocumentsDirectory] path] stringByAppendingPathComponent:pathId];
    return storePath;
}

- (NSURL *)applicationDocumentsDirectory
{
    //   NSArray *array1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSArray *array2 = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return array2.lastObject;
}

@end
