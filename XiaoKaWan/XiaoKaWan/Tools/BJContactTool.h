//
//  BJContactTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/21.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BJContactModel.h"


typedef void(^RefreshContact)();

@interface BJContactTool : NSObject

@property (nonatomic,copy)RefreshContact RefreshContactBlock;

@property (nonatomic,strong)NSMutableArray *ContactModelArray;


/*
 获取通讯录数据，自动判断是否需要同步获取最新的通讯录数据
 **/
+(void)contactModelArraySuccess:(void(^)(NSArray *contactModelArray))success;


/*
 获取通讯录数据，强制同步最新的通讯录数据
 **/
+(void)newContactModelArraySuccess:(void(^)(NSArray *contactModelArray))success;

@end
