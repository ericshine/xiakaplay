//
//  ModelManage.h
//  Share
//
//  Created by apple_Eric on 5/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import <CoreData/CoreData.h>
@interface ModelManage : NSObject
@property(nonatomic,strong) NSManagedObjectModel *model;
@property(nonatomic,strong) NSManagedObjectContext *context;
@property(nonatomic,strong) NSPersistentStoreCoordinator *coordinator;
@property(nonatomic,strong) NSURL *storeUrl;
//@property(nonatomic,strong) NSString *modeldStr;
SINGLETON_H
-(NSString *)modeldStr;
-(void)saveContext;
- (void)clearContext;
- (void)deleteContext;
@end
