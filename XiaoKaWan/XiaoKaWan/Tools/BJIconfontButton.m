//
//  BJIconfontButton.m
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJIconfontButton.h"
#import <Masonry.h>
#import "UIColor+colorWithHex.h"
@implementation BJIconfontButton
- (void)setTitle:(NSString *)title forState:(UIControlState)state{
    [self.titleButton setTitle:title forState:state];
}
- (void)setIcon:(NSString *)icon forState:(UIControlState)state{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:icon];
    [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"iconfont" size:15]} range:NSMakeRange(0, attStr.length)];
//    [self.iconButton setAttributedTitle:attStr forState:state];
    self.iconButton.titleLabel.font = [UIFont fontWithName:@"iconfont" size:15];
    [self.iconButton setTitle:icon forState:state];
}
- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state{
    [self.titleButton setTitleColor:color forState:state];
}
- (void)setIconColor:(UIColor *)color forState:(UIControlState)state{
//    self.iconButton.titleLabel.textColor = color;
    [self.iconButton setTitleColor:color forState:state];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.iconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
    }];
    [self.titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.iconButton.mas_right).offset(-10);
        make.bottom.equalTo(self.mas_bottom);
    }];
}
- (UIButton*)iconButton{
    if(_iconButton == nil){
        _iconButton =[[UIButton alloc] init];
        [self addSubview:_iconButton];
    }
    return _iconButton;
}
- (UIButton*)titleButton{
    if(_titleButton == nil){
        _titleButton =[[UIButton alloc] init];
        _titleButton.titleLabel.font = [UIFont systemFontOfSize:11];
        [self addSubview:_titleButton];
    }
    return _titleButton;
}
- (void)addTartget:(id)target action:(SEL)selector forControlEvents:(UIControlEvents)UIControlEvents{
    [_titleButton addTarget:target action:selector forControlEvents:UIControlEvents];
    [_iconButton addTarget:target action:selector forControlEvents:UIControlEvents];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
