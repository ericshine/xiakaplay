//
//  BJGroupHeadImageView.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/12/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJUIImageView.h"
typedef NS_ENUM(NSInteger,HeadViewType){
    HEADVIEWTYPE=0,
    HEADVIEWTYPE_NORMAL,
    HEADVIEWTYPE_SHOWNUMBER
};
@interface BJGroupHeadImageView : UIImageView
@property(nonatomic) NSInteger countLimit;
@property(nonatomic) NSInteger totalNumber;
@property(nonatomic,strong) NSArray *imageUrlArray;
@property(nonatomic)HeadViewType headViewType;
- (instancetype)initWithFrame:(CGRect)frame andGroups:(NSArray*)urls andPlaceholderImages:(NSArray*)images;
- (instancetype)initWithFrame:(CGRect)frame andGroups:(NSArray*)urls;

@end
@interface HeadViewCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)BJUIImageView *imageView;
@property(nonatomic,strong)UILabel *numberLabel;
@property(nonatomic)HeadViewType headViewType;
- (void)removeNumber;
@end