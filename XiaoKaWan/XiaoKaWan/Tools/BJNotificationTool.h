//
//  BJNotificationTool.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/8/2.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BJNotificationTool : NSObject

+(void)DealWithNotification:(NSDictionary *)NotificationDic;

@end
