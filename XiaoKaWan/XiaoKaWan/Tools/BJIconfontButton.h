//
//  BJIconfontButton.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/13/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJIconfontButton : UIView

@property(nonatomic,strong)UIButton *iconButton;
@property(nonatomic,strong)UIButton *titleButton;
/**
 *  Description
 *
 *  @param title title description
 *  @param state state description
 */
- (void)setTitle:(NSString*)title forState:(UIControlState)state;
- (void)setTitleColor:(UIColor*)color forState:(UIControlState)state;

/**
 *  Description
 *
 *  @param icon  icon description
 *  @param state state description
 */
- (void)setIcon:(NSString *)icon forState:(UIControlState)state;
- (void)setIconColor:(UIColor *)color forState:(UIControlState)state;
/**
 *  Description
 *
 *  @param target          target description
 *  @param selector        selector description
 *  @param UIControlEvents UIControlEvents description
 */
- (void)addTartget:(id)target action:(SEL)selector forControlEvents:(UIControlEvents)UIControlEvents;
@end
