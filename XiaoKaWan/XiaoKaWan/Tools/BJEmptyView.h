//
//  BJEmptyView.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/19.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJEmptyView : UIView

-(instancetype)initWithFrame:(CGRect)frame titel:(NSString *)text imageName:(NSString *)imageName;

@end
