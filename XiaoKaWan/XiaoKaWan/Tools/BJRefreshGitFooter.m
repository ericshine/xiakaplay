//
//  BJRefreshGitFooter.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJRefreshGitFooter.h"

@implementation BJRefreshGitFooter
+ (instancetype)selfFooterWithRefreshingBlock:(BJRefreshComponentRefreshingBlock)refreshingBlock
{
    BJRefreshGitFooter *footer = [BJRefreshGitFooter footerWithRefreshingBlock:refreshingBlock];
    [footer setTitle:@"start" forState:MJRefreshStatePulling];
    [footer setTitle:@"loading" forState:MJRefreshStateRefreshing];
    [footer setTitle:@" " forState:MJRefreshStateIdle];
    [footer setTitle:@"" forState:MJRefreshStateNoMoreData];
    UIImage *imgR1 = [UIImage imageNamed:@""];
    UIImage *imgR2 = [UIImage imageNamed:@""];
    
    NSArray *reFreshone = [NSArray arrayWithObjects:imgR1, nil];
    NSArray *reFreshtwo = [NSArray arrayWithObjects:imgR2, nil];
    NSArray *reFreshthree = [NSArray arrayWithObjects:imgR1,imgR2, nil];
    
    [footer setImages:reFreshone forState:MJRefreshStatePulling];
    [footer setImages:reFreshthree forState:MJRefreshStateRefreshing];
    [footer setImages:reFreshtwo forState:MJRefreshStateIdle];
    
       return footer;
}
//- (void)beginRefreshing
//{
//    [super beginRefreshing];
//}
//- (void)endRefreshing
//{
//    [super endRefreshing];
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
