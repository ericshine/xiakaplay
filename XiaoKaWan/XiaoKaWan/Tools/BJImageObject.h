//
//  BJImageObject.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension.h>
#import <UIKit/UIKit.h>
@interface BJImageObject : NSObject
@property(nonatomic,strong)NSString *url; // 原图
@property(nonatomic) CGFloat height;
@property(nonatomic) CGFloat width;

@end
