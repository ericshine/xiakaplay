//
//  BJSegmentView.m
//  XiaoKaWan
//
//  Created by apple_Eric on 7/20/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJSegmentView.h"
#define titleFont [UIFont systemFontOfSize:17]
#define itemsSpace 10
#define itemsHeight 44
#define offset 20
@implementation BJSegmentView{
    CGFloat itemWidth;
    BOOL isAnimation;
}
- (instancetype)initWithItmes:(NSArray *)items withAnimation:(BOOL)animation{
    self = [super initWithFrame:CGRectZero];
    if(self){
        isAnimation = animation;
        [self creatItems:items];
        
    }
    return self;
}
- (void)creatItems:(NSArray *)items{
    __block CGFloat width;
    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *text = obj;
        CGSize size = [text sizeWithFont:titleFont andSize:CGSizeMake(kDeviceWidth, 30)];
        width  = size.width+offset;
        itemWidth = size.width;
        UIButton *titleButton = [[UIButton alloc]initWithFrame:CGRectMake(itemsSpace +idx*(width+itemsSpace), 0, width, itemsHeight)];
        titleButton.tag = 30+idx;
        [titleButton setTitle:obj forState:UIControlStateNormal];
        titleButton.titleLabel.font = titleFont;
        [titleButton setTitleColor:[UIColor p_colorWithHex:0x333333] forState:UIControlStateNormal];
        [titleButton addTarget:self action:@selector(selectNameButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:titleButton];
    }];
    self.frame = CGRectMake(0, 0, width*items.count + itemsSpace*(items.count+1), itemsHeight);
    [self lineView];
}
- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(itemsSpace+offset/2, itemsHeight-3, itemWidth, 3)];
        _lineView.backgroundColor = [UIColor p_colorWithHex:0x333333];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (void)selectNameButton:(UIButton *)button{
    NSInteger index = button.tag - 30;
    if(self.selectAtIndex)self.selectAtIndex(index);
    if(isAnimation){
    [UIView animateWithDuration:0.2 animations:^{
        self.lineView.transform = CGAffineTransformMakeTranslation(index*(itemWidth+itemsSpace+offset), 0);
    }];
    }else self.lineView.transform = CGAffineTransformMakeTranslation(index*(itemWidth+itemsSpace+offset), 0);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
