//
//  BJImageObject.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/30/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "BJImageObject.h"

@implementation BJImageObject
- (instancetype)init{
    self =[super init];
    if(self){
        [BJImageObject setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"url":@"src",
                     @"height":@"h",
                     @"width":@"w"
                     };
        }];
    }
    return self;
}
@end
