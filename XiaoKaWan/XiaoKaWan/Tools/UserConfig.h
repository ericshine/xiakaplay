//
//  UserConfig.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/15/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
@interface UserConfig : NSObject
SINGLETON_H
@property(nonatomic,copy) NSString *deviceId;
@property(nonatomic,copy) NSString *apiVersion;
@property(nonatomic,copy) NSString *user_token;
@property(nonatomic,copy) NSString *userId;
@property(nonatomic,copy) NSString *account;
@end
