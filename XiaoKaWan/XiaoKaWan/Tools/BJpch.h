//
//  BJpch.h
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/14.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+colorWithHex.h"
#import <ReactiveCocoa.h>
#import <Masonry.h>
#import "UIImage+Category.h"
#import <UIImageView+WebCache.h>

#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height

//#define MainFontName @"STHeitiTC-Light"

@interface BJpch : NSObject

@end
