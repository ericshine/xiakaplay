//
//  BJAvatarBrower.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/18/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJAvatarBrower : UIViewController
/**
 *  Description
 *
 *  @param avatarImageView 头像imageView
 */
+(void)showImage:(UIImageView*)avatarImageView;
@end
