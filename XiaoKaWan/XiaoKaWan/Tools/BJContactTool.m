//
//  BJContactTool.m
//  XiaoKaWan
//
//  Created by 常鑫亮 on 16/7/21.
//  Copyright © 2016年 不匠科技. All rights reserved.
//

#import "BJContactTool.h"
#import <FMDB.h>
#import <FMDatabaseQueue.h>
#import <RHAddressBook/AddressBook.h>
#import "BJpch.h"
#import <MJExtension.h>
#import "ClientNetwork.h"
#import "BJContactPhoneStatusModel.h"
#import "BJPregressHUD.h"


@implementation BJContactTool

/*
 用来在内存中存储通讯录，key为第一个电话号码，Value为通讯录模型
 **/
static NSMutableDictionary *_ContactArrayDic;
/*
 数据库对象
 **/
static FMDatabase *_db;
/*
 数据库安全队列
 **/
static FMDatabaseQueue *_DBQueue ;


+(void)initialize
{
    NSString *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *DBName=[NSString stringWithFormat:@"%@_Contact.sqlite",[UserConfig sharedInstance].userId];
    NSString *Contactfilepath=[directoryPath stringByAppendingPathComponent:DBName];
    _db=[FMDatabase databaseWithPath:Contactfilepath];
    
    if ([_db open]) {
        
        NSString *CreateTableSql=@"create table if not exists Contact (id integer primary key autoincrement, contactName text, Phone text, NickName text, imgUrl text,Status integer,uid integer);";
        
        BOOL create= [_db executeUpdate:CreateTableSql];
        if (create) {
            NSLog(@"创建通讯录列表数据库成功");
        }else
        {
            NSLog(@"创建通讯录列表数据库失败");
        }
    }
    
    _ContactArrayDic=[NSMutableDictionary dictionary];
    
    _DBQueue = [FMDatabaseQueue databaseQueueWithPath:Contactfilepath];
   
}

+(void)contactModelArraySuccess:(void (^)(NSArray *))success
{
    //如果用户未登陆，直接返回空数组
    if (![UserConfig sharedInstance].user_token.length)
    {
        if (success) {
            success(nil);
        }
        
        return;
    }
    
    if ([self hasSavedContact]) {
        //数据库中有数据
        if ([self contactDataIsTimeOut]) {
            //数据库中的数据已经过期，需要获取最新的通讯录数据
            [self newContactModelArraySuccess:^(NSArray *contactModelArray) {
                if (success) {
                    success(contactModelArray);
                }
            }];
        }else
        {
            //数据库中的数据已经是最新数据，直接读取数据库数据
            if (success) {
                success([self ReadContactArrayFromDB]);
                
            }
        }
    }else
    {
        //数据库中无数据，第一次获取通讯录数据，直接获取最新的数据
        [self newContactModelArraySuccess:^(NSArray *contactModelArray) {
            if (success) {
                success(contactModelArray);
            }
            
        }];
        
    }
    
    
}

+(void)newContactModelArraySuccess:(void (^)(NSArray *))success
{
    
    //将通讯录数据从系统通讯录读取到内存，用字典来存储，key为电话号码，value为通讯录模型
   @weakify(self)
   [self readContactToRAMSuccess:^{
       @strongify(self)
       //将内存中的数据发送到服务器进行比较
       [self sendContactToServierSuccess:^{
           if (success) {
               success([self seqenceContact]);
           }
           
       }];
       
   }];
    
    
    
}
 //将内存中的数据发送到服务器进行比较
+(void)sendContactToServierSuccess:(void(^)())success
{
    NSArray *ContactModelArray= [_ContactArrayDic allValues];
    NSMutableArray *allphoneArray=[NSMutableArray array];
    //便利一遍把所有电话号码放到一个数组里
    for (BJContactModel *contactModel in ContactModelArray) {
        [allphoneArray addObject:contactModel.PhoneStr];
    }
    //再便利一遍把所有电话号码变成一个字典再放到一个数组里
    NSMutableArray *PhoneDicArray=[NSMutableArray array];
    for (NSString *str in allphoneArray) {
        [PhoneDicArray addObject:@{@"phone_number":str}];
        
    }
    
    NSString *str=[PhoneDicArray JSONString];
    NSDictionary *parameter=@{ @"_t":[UserConfig sharedInstance].user_token,@"data":str};
    
//    NSString *bodyStr=[parameter JSONString];
    //    NSLog(@"%@",bodyStr);
    @weakify(self)
    [[ClientNetwork sharedInstance]postWithPath:[[APIConfig sharedInstance] contactsSyncApi] parameter:parameter complete:^(BOOL succeed, id obj) {
        @strongify(self)
        NSLog(@"%@===%d",obj,succeed);
        if (succeed) {
            //存储同步时间
            [self saveCurrentTime];
            
            NSArray *PhoneStatusArray=[BJContactPhoneStatusModel objectArrayWithKeyValuesArray:[obj objectForKey:@"contacts"]];
            
            [self updateRAMContactWithServerData:PhoneStatusArray success:^{
                if (success) {
                    success();
                }
            }];

        }else
        {
            [BJPregressHUD dissMiss];
        }
        
        
    }];
    
}

+(void)updateRAMContactWithServerData:(NSArray *)PhoneStatusArray success:(void(^)())success
{
    for (BJContactPhoneStatusModel *ContactPhoneStatusModel in PhoneStatusArray) {
       BJContactModel *contactModel=[_ContactArrayDic objectForKey:ContactPhoneStatusModel.number];
        contactModel.Status=ContactPhoneStatusModel.status;
        contactModel.uid=ContactPhoneStatusModel.uid;
        contactModel.NickName=ContactPhoneStatusModel.nickname;
        contactModel.imgUrl=ContactPhoneStatusModel.nick;
    }

    if (success) {
        success();
    }
    
    
    
    @weakify(self)
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        @strongify(self)
        //存储到数据库
        [self saveToDBFromRAM];
        
    });
    
}

//内存中的字典取出数组排序
+(NSArray *)seqenceContact
{
    NSComparator cmptr = ^(BJContactModel *obj1, BJContactModel *obj2){
        if (obj1.Status < obj2.Status) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if (obj1.Status > obj2.Status) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
   NSArray *sortedArray= [[_ContactArrayDic allValues] sortedArrayUsingComparator:cmptr];
    
    return sortedArray;
}

+(void)clearDB
{
//    NSString *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    
//    NSString *hisfilepath=[directoryPath stringByAppendingPathComponent:@"Contact.sqlite"];
//    
//    FMDatabase *db=[FMDatabase databaseWithPath:hisfilepath];
    if (![_db open]) {
        
        NSLog(@"db open fail");
        return ;
    }
    
    
    BOOL res = [_db executeUpdate:@"delete from Contact"];
    if (!res) {
        NSLog(@"error when clear db table");
    } else {
    }
    [_db close];
}

+(void)saveToDBFromRAM
{
    [_DBQueue inDatabase:^(FMDatabase *_db) {
        [self clearDB];
        
        NSArray *contactModelArray=[_ContactArrayDic allValues];
        if ([_db open]) {
            
            for (BJContactModel *contactModel in contactModelArray) {
                
                BOOL insert= [_db executeUpdateWithFormat:@"insert into Contact (contactName,Phone,NickName,imgUrl,Status,uid) values (%@,%@,%@,%@,%ld,%ld)",contactModel.contactName,contactModel.PhoneStr,contactModel.NickName,contactModel.imgUrl,contactModel.Status,contactModel.uid];
                
                if (insert) {
                    
//                    NSLog(@"cha ru chenggong");
                }else
                {
//                    NSLog(@"cha ru shibai");
                }
                
            }
            
        }
        
    }];
    
}

//将通讯录数据从系统读取到内存，用字典来存储，key为电话号码，value为通讯录模型
+(void)readContactToRAMSuccess:(void(^)())success
{
    //注册通讯录权限
    RHAddressBook *addressBook = [[RHAddressBook alloc] init];
    
    RHAuthorizationStatus status = [RHAddressBook authorizationStatus];
    
    if (status == RHAuthorizationStatusNotDetermined){
        @weakify(self)
        [addressBook requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
            @ strongify(self)
            if (granted==YES){
                [self ReadAddressBookDataToRAMWith:addressBook success:^{
                    if (success) {
                        success();
                    }
                    
                }];
            }
        }];
    }else if (status == RHAuthorizationStatusAuthorized)
    {
        [self ReadAddressBookDataToRAMWith:addressBook success:^{
            if (success) {
                success();
            }
            
        }];
        
    }else{
        return;
    }
}
+(void)ReadAddressBookDataToRAMWith:(RHAddressBook *)addressBook success:(void(^)())success
{
    // 4.获取所有的联系人
    NSArray *peopleArray = addressBook.people;
    
    // 5.遍历所有的联系人
    for (RHPerson *person in peopleArray) {
        [self SaveContactWith:person];
        
    }
    
    if (success) {
        success();
    }
    
}

+(void)SaveContactWith:(RHPerson *)person
{
    if (person.lastName ==nil) {
        person.lastName=@"";
    }
    if (person.firstName==nil) {
        person.firstName=@"";
    }
    
    NSString *ContactName=[NSString stringWithFormat:@"%@%@",person.lastName,person.firstName];
    
    NSMutableArray *PhoneArray=[NSMutableArray array];
    RHMultiValue *phones = person.phoneNumbers;
    
    for (int i = 0; i < phones.count; i++) {
        
        NSString *phoneValue = [phones valueAtIndex:i];
        
        phoneValue= [self dealWithPhoneStr:phoneValue];
        if ([[UserConfig sharedInstance].account isEqualToString:phoneValue]) continue;
        
        if ([self isMobile:phoneValue]){
            
            BJContactModel *contactModel=[[BJContactModel alloc]init];
            contactModel.contactName=ContactName;
            contactModel.PhoneStr=phoneValue;
            
            [_ContactArrayDic setObject:contactModel forKey:phoneValue];
        }
        
    }
    
    
    
}
+(NSString *)dealWithPhoneStr:(NSString *)PhoneStr
{
    //去除数字以外字符
    NSCharacterSet *setToRemove =
    [[ NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
     invertedSet ];
    
    NSString *newString =
    [[PhoneStr componentsSeparatedByCharactersInSet:setToRemove]
     componentsJoinedByString:@""];
    
    return newString;
}


/*
 将现有的数据库里的数据转化成通讯录模型数组
 **/
+(NSArray *)ReadContactArrayFromDB
{
    
//    NSString *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    
//    NSString *hisfilepath=[directoryPath stringByAppendingPathComponent:@"Contact.sqlite"];
//    
//    FMDatabase *db=[FMDatabase databaseWithPath:hisfilepath];
    if (![_db open]) {
        
        NSLog(@"db open fail");
        return nil;
    }
    
    
    FMResultSet *resultSet=[_db executeQuery:@"select * from Contact ORDER BY Status DESC"];
    
    while ([resultSet next]) {
        
        BJContactModel *ContactModel=[[BJContactModel alloc]init];
        
        ContactModel.contactName=[resultSet stringForColumn:@"contactName"];
        
        ContactModel.PhoneStr=[resultSet stringForColumn:@"Phone"];
        
        ContactModel.NickName=[resultSet stringForColumn:@"NickName"];
        
        ContactModel.imgUrl=[resultSet stringForColumn:@"imgUrl"];
        
        ContactModel.Status=[resultSet intForColumn:@"Status"];
        
        ContactModel.uid=[resultSet intForColumn:@"uid"];
        
        ContactModel.id=[resultSet intForColumn:@"id"];

        [_ContactArrayDic setObject:ContactModel forKey:ContactModel.PhoneStr];
    }
    
    return [self seqenceContact];
}

+(BOOL)hasSavedContact
{
    //判断一下通讯录文件是否存在来决定通讯录是否保存到本地过
    NSString *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *DBName=[NSString stringWithFormat:@"%@_Contact.sqlite",[UserConfig sharedInstance].userId];
    NSString *Contactfilepath=[directoryPath stringByAppendingPathComponent:DBName];
//    NSString *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    
//    NSString *Contactfilepath=[directoryPath stringByAppendingPathComponent:@"Contact.sqlite"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:Contactfilepath])
    {
        return YES;
    }else
    {
        return NO;
    }
    
    
}

/**
 通讯录是否过期
 */
+(BOOL)contactDataIsTimeOut
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSDate *lastSendDate= [userDefault objectForKey:@"sendContactTime"];
    NSTimeInterval timeIntervalSinceLastSend=[[NSDate date]timeIntervalSinceDate:lastSendDate];
    
    if (timeIntervalSinceLastSend>60*60*24) {
        return YES;
    }else
    {
        return NO;
    }
}

//保存本次和服务器同步通讯录数据的时间
+(void)saveCurrentTime
{
    NSUserDefaults *userFefault= [NSUserDefaults standardUserDefaults];
    [userFefault setObject:[NSDate date] forKey:@"sendContactTime"];
}



+(BOOL) isMobile:(NSString *)mobileNumbel{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189,181(增加)
     */
    NSString * MOBIL = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189,181(增加)
     22         */
    NSString * CT = @"^1((33|53|8[019])[0-9]|349)\\d{7}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBIL];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNumbel]
         || [regextestcm evaluateWithObject:mobileNumbel]
         || [regextestct evaluateWithObject:mobileNumbel]
         || [regextestcu evaluateWithObject:mobileNumbel])) {
        return YES;
    }
    
    return NO;
}



@end
