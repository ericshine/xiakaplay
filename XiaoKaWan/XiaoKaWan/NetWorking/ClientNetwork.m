//
//  ClientNetwork.m
//  XiaoKaWan
//
//  Created by apple_Eric on 5/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "ClientNetwork.h"
#import "QAlertView.h"
#import "UserConfig.h"
#import "BJUploadFileConfig.h"
#import "NSDate+BJDateFormat.h"
#import "BJNotificationConfig.h"
//#import <SystemConfiguration/SystemConfiguration.h>
//#import <MobileCoreServices/MobileCoreServices.h>
//#define AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES
@interface ClientNetwork ()
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,strong)NSURLSessionTask *uploadTask;
@end
@implementation ClientNetwork{
    NSProgress *uploadProgress;
}
SINGLETON_M(ClientNetwork)
-(AFHTTPSessionManager *)sessionManage
{
    if(_sessionManage == nil){
    _sessionManage = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:[ServiceUrl sharedInstance].serviceUrl]];
        
    [_sessionManage setSecurityPolicy:[ClientNetwork customSecurityPolicy]];
    }
    return _sessionManage;
}
+ (AFSecurityPolicy *)customSecurityPolicy
{
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"apiuu19com" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    
    //AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    //allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    //如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    securityPolicy.validatesDomainName = NO;
    NSSet *set = [[NSSet alloc] initWithObjects:certData, nil];
    securityPolicy.pinnedCertificates = set;
    
    return securityPolicy;
}
- (void)getWithPath:(NSString *)path parameter:(NSDictionary *)parameter complete:(requestComplete)complete
{
    if(![self isNetWorking]) {
        complete(NO,NSLocalizedString(@"networkFail", nil));
        return;
    };
    NSMutableDictionary *parameterDic = [self getParameter:parameter];
    [self.sessionManage GET:path parameters:parameterDic progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject[@"code"] integerValue]==0){
            complete(YES,responseObject[@"data"]);
        }else if([responseObject[@"code"] integerValue]==101){
            [[NSNotificationCenter defaultCenter] postNotificationName:BJNeedUserLoginNotification object:nil];
        }else{
            complete(NO,responseObject[@"msg"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NO,[error localizedDescription]);
        
    }];
}

- (void)postWithPath:(NSString *)path parameter:(NSDictionary *)parameter complete:(requestComplete)complete
{         
    if(![self isNetWorking]) {
        complete(NO,NSLocalizedString(@"networkFail", nil));
        return;
    };
    NSMutableDictionary *parameterDic = [self getParameter:parameter];
    
    [self.sessionManage POST:path parameters:parameterDic progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject[@"code"] integerValue]==0){
            complete(YES,responseObject[@"data"]);
        }else{
            complete(NO,responseObject[@"msg"]);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NO,[error localizedDescription]);
        
        NSLog(@"%@",task.response);
    }];
}
- (void)uploadImageWithPath:(NSString *)path image:(UIImage *)image imageName:(NSString *)imageName complete:(requestComplete)complete{
    [self uploadMultipleImagePath:path images:@[image] imageNmaes:@[imageName] complete:complete pregress:nil];
}
- (void)uploadImageWithPath:(NSString *)path image:(UIImage *)image imageName:(NSString *)imageName complete:(requestComplete)complete pregress:(pregress)pregress{
    [self uploadMultipleImagePath:path images:@[image] imageNmaes:@[imageName] complete:complete pregress:pregress];
}
- (void)uploadMultipleImagePath:(NSString *)path images:(NSArray<UIImage *> *)images imageNmaes:(NSArray<NSString *> *)imageNames complete:(requestComplete)complete pregress:(pregress)pregress{
    NSArray *filesParameter;
    NSMutableArray *fileConfigs = [[NSMutableArray alloc] initWithCapacity:images.count];
    for(int i=0;i<images.count;i++){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [[NSDate date] dateToStringWithFormat:@"yyyyMMddHHmmss"];
        BJUploadFileConfig *fileConfig = [[BJUploadFileConfig alloc] init];
        fileConfig.fileData = UIImageJPEGRepresentation(images[i],0.1);
        fileConfig.name = imageNames[i];
        fileConfig.fileName = [NSString stringWithFormat:@"%@.jpeg",str];
        fileConfig.mimeType = @"image/jpeg";
        [fileConfigs addObject:fileConfig];
//        NSLog(@"image:%f",[fileConfig.fileData length]/1000.0);
    }
    filesParameter = [fileConfigs copy];
    [self uploadMultipleFilesPath:path files:filesParameter complete:complete pregress:pregress];
}
- (void)uploadFileWithPath:(NSString *)path filesParameter:(BJUploadFileConfig *)fileParameter complete:(requestComplete)complete{
    [self uploadMultipleFilesPath:path files:@[fileParameter] complete:complete pregress:nil];
}
-(void)uploadMultipleFilesPath:(NSString *)path files:(NSArray<BJUploadFileConfig *> *)filesparameters complete:(requestComplete)complete pregress:(pregress)pregress{
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:[UserConfig sharedInstance].user_token forKey:@"_t"];
    parameter = [self getParameter:parameter];
    
//    NSError *error;
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",[ServiceUrl sharedInstance].serviceUrl,path] parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        for(BJUploadFileConfig *fileConfig in filesparameters){
//            [formData appendPartWithFileData:fileConfig.fileData name:fileConfig.name fileName:fileConfig.fileName mimeType:fileConfig.mimeType];
//        }
//    } error:&error];
//    NSLog(@"error:%@",error);
//    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
////    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    _uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
//                    NSLog(@"Progress:%f",uploadProgress.fractionCompleted);
//                      dispatch_async(dispatch_get_main_queue(), ^{
//                          //Update the progress view
////                          [progressView setProgress:uploadProgress.fractionCompleted];
//                      });
//                  } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                      if (error) {
//                          NSLog(@"Error: %@", error);
//                      } else {
//                          NSLog(@"%@ %@", response, responseObject);
//                      }
//                  }];
//   
//    uploadProgress = [manager uploadProgressForTask:_uploadTask];
//    [uploadProgress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionNew context:nil];
//    NSProgress *downProgress = [manager downloadProgressForTask:_uploadTask];
//      [downProgress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionNew context:nil];
//     [_uploadTask resume];
//     return;
//    /**
//     *  old
//     */
    [self.sessionManage POST:path parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for(BJUploadFileConfig *fileConfig in filesparameters){
        [formData appendPartWithFileData:fileConfig.fileData name:fileConfig.name fileName:fileConfig.fileName mimeType:fileConfig.mimeType];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"Progress:%f",uploadProgress.fractionCompleted);
//            if(pregress)pregress(uploadProgress.fractionCompleted);
//        });
       
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject[@"code"] integerValue]==0){
            complete(YES,responseObject[@"data"]);
        }else{
            complete(NO,responseObject[@"msg"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        NSLog(@"%li",(long)response.statusCode);
        if(complete)complete(NO,[error localizedDescription]);
    }];
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
//    NSProgress *progress = object;
//    NSLog(@"%i",progress.fractionCompleted);
//}
- (NSMutableDictionary *)getParameter:(NSDictionary*)parameter
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:parameter];
    [dictionary setObject:[UserConfig sharedInstance].deviceId forKey:@"_dt"];
    [dictionary setObject:[UserConfig sharedInstance].apiVersion forKey:@"_api"];
    return dictionary;
}
/**
 *  判断是否网络打开
 *
 *  @return return value description
 */
- (BOOL)isNetWorking{
    if([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
       // [[QAlertView sharedInstance] showAlertText:NSLocalizedString(@"networkFail", nil) fadeTime:2];
        return NO;
    }
    return YES;
}
@end

