//
//  BJUploadFileConfig.h
//  XiaoKaWan
//
//  Created by apple_Eric on 6/17/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJUploadFileConfig : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *fileName;
@property(nonatomic,copy) NSString *mimeType;
@property(nonatomic,copy) NSData *fileData;
@end
