//
//  ClientNetwork.h
//  XiaoKaWan
//
//  Created by apple_Eric on 5/16/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "ServiceUrl.h"
#import "APIConfig.h"
#import "AFNetworking.h"
#import "UserConfig.h"
#import <MJExtension.h>
typedef void(^requestComplete)(BOOL succeed,id obj);
typedef void(^pregress)(CGFloat pregress);
@class BJUploadFileConfig;
@interface ClientNetwork : NSObject
@property(nonatomic,strong)AFHTTPSessionManager *sessionManage;
SINGLETON_H
/**
 *  get method
 *
 *  @param path     request url
 *  @param headers  request header
 *  @param complete request result
 */
- (void)getWithPath:(NSString *)path parameter:(NSDictionary *)parameter complete:(requestComplete)complete;

/**
 *  post method
 *
 *  @param path      request url
 *  @param parameter request  parameter
 *  @param complete  request result
 */
- (void)postWithPath:(NSString *)path parameter:(NSDictionary *)parameter complete:(requestComplete)complete;
/**
 *  上传单张图
 *
 *  @param path     url
 *  @param image    image
 *  @param complete complete description
 */
- (void)uploadImageWithPath:(NSString *)path image:(UIImage*)image imageName:(NSString *)imageName complete:(requestComplete)complete;
- (void)uploadImageWithPath:(NSString *)path image:(UIImage*)image imageName:(NSString *)imageName complete:(requestComplete)complete  pregress:(pregress)pregress;
/**
 *  上传多张图
 *
 *  @param path     url
 *  @param images   images ,array of images
 *  @param complete complete description
 */
//- (void)uploadMultipleImagePath:(NSString *)path images:(NSArray<UIImage*> *)images imageNmaes:(NSArray<NSString*>*)imageNames complete:(requestComplete)complete;
- (void)uploadMultipleImagePath:(NSString *)path images:(NSArray<UIImage *> *)images imageNmaes:(NSArray<NSString *> *)imageNames complete:(requestComplete)complete pregress:(pregress)pregress;
/**
 *  上传单个文件
 *
 *  @param path     url
 *  @param fileData fileData description
 *  @param complete complete description
 */
- (void)uploadFileWithPath:(NSString *)path filesParameter:(BJUploadFileConfig*)fileParameter complete:(requestComplete)complete;
/**
 *  上传多个文件
 *
 *  @param path      url
 *  @param filesData filesData description
 *  @param complete  complete description
 */
- (void)uploadMultipleFilesPath:(NSString*)path files:(NSArray<BJUploadFileConfig*>*)filesparameters complete:(requestComplete)complete pregress:(pregress)pregress;
@end
