//
//  TodayViewController.m
//  XKWExtensionToday
//
//  Created by apple_Eric on 5/31/16.
//  Copyright © 2016 不匠科技. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
//#import "Maps.h"
@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSArray *array = [Maps getMapsList];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = [UIImage imageNamed:@"slowstreet"];
//    imageView.backgroundColor = [UIColor redColor];
    imageView.userInteractionEnabled= YES;
    [self.view addSubview:imageView];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame)+10, 120, 30)];
    [button setTitle:@"进入小咖玩" forState:0];
    [button setTitleColor:[UIColor whiteColor] forState:0];
    [button setTitleColor:[UIColor yellowColor] forState:UIControlStateHighlighted];
   // button.backgroundColor = [UIColor redColor];
    button.layer.cornerRadius = 5;
    
    [self.view addSubview:button];
    [button addTarget:self action:@selector(openApp:) forControlEvents:UIControlEventTouchUpInside];
    self.preferredContentSize = CGSizeMake(320, 300);
    self.view.userInteractionEnabled = YES;

    // Do any additional setup after loading the view from its nib.
}
- (void)openApp:(UIButton *)button{
    NSURL *appUrl = [NSURL URLWithString:@"come.xiaokaplay://xiaokawan?name:eric"];
    [self.extensionContext openURL:appUrl completionHandler:^(BOOL success) {
    }];
}
//- (void)receivedAdditionalContent {
//    self.preferredContentSize = [self sizeNeededToShowAdditionalContent];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets{
    return UIEdgeInsetsMake(0, 16, 0, 0);
}
- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

@end
